import React from 'react';
import {
	Alert, AsyncStorage, Image, ImageBackground, Keyboard, KeyboardAvoidingView, Platform, StatusBar, Text, TextInput,
	TouchableOpacity, TouchableWithoutFeedback, View
} from 'react-native';
import styles from './styles';
import Spinner from 'react-native-loading-spinner-overlay';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Modal from 'react-native-modal';


let moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class login extends React.Component {
	static navigationOptions = {
		header: null
	};
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible, dismiss: 0
		});
	};
	handleCorreo = text => {
		this.setState({correo: text});
	};
	_fetchLoginData = (mail, pass, spinner) => {
		if (mail === '' || pass === '') {
			Alert.alert('Campos vacíos', 'Es necesario llenar los campos de usuario y contraseña para realizar esta operación.');
		} else {
			spinner._changeWheelState();
			return fetch(this.state.uri + '/api/auth/login', {
				method: 'POST', headers: {
					Accept: 'application/json', 'Content-Type': 'application/json'
				}, body: JSON.stringify({
					email: mail, password: pass
				})
			})
				.then(response => response.json())
				.then(responseJson => {
					if (responseJson.token === undefined) {
						switch (responseJson.error.status_code) {
							case 422:
								Alert.alert('Formato inválido', 'Es necesario poner un email válido.', [{
									text: 'OK', onPress: () => spinner._changeWheelState()
								}]);
								break;
							case undefined:
								Alert.alert('Usuario o contraseña Incorrectos', 'Ingresa los datos correctos', [{
									text: 'OK', onPress: () => spinner._changeWheelState()
								}]);
								break;
							case 403:
								Alert.alert('Usuario o contraseña incorrectos', 'Si el error persiste solicite ayuda.', [{
									text: 'OK', onPress: () => spinner._changeWheelState()
								}]);
								break;
							case 500:
								Alert.alert('Error interno', 'Por el momento no se puede procesar su solicitud, intente más tarde.', [{
									text: 'OK', onPress: () => spinner._changeWheelState()
								}]);
								break;
							default:
								alert('Error:' + responseJson.error.status_code, 'Solicite ayuda por favor', [{
									text: 'OK', onPress: () => spinner._changeWheelState()
								}]);
						}
					} else {
						this.saveItem('token', responseJson.token);
						this.saveItem('id', responseJson.id.toString());
						this.saveItem('role', responseJson.role);
						this.saveItem('puesto', responseJson.puesto);
						this.saveItem('grado', responseJson.grado);
						this.saveItem('grupo', responseJson.grupo);
						this.saveItem('name', responseJson.name);
						this.getUserData();
					}
				})
				.catch(error => {
					Alert.alert('ERROR DE CONEXIÓN', 'Revise su conexión a internet. Si el problema persiste solicite ayuda.', [{
						text: 'OK', onPress: () => spinner._changeWheelState()
					}]);
				});
		}

	};
	handleEmail = text => {
		this.setState({email: text});
	};
	handlePassword = text => {
		this.setState({password: text});
	};

	constructor(props) {
		super(props);
		this._fetchLoginData = this._fetchLoginData.bind(this);
		this.state = {
			email: '',
			password: '',
			uri: '',
			escuelasDatos: [],
			visible: false,
			textInputValue: '',
			cososPicker: [],
			datos: [],
			aux: 0
		};
	}

	static cambiarEscuela() {
		AsyncStorage.clear();
		Actions.replace('Bienvenida', {type: 'reset'});
	}


	async getUserData() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data', {
			method: 'GET', headers: {Authorization: 'Bearer ' + token}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({datos: responseJson});
				}
			});
		{
			this.state.datos.passwordUpdated !== '0' || this.state.datos.passwordUpadated !== 0
				? await Actions.replace('drawer', {key: Math.random()})
				: await Actions.PwdRest()
		}
	}

	async componentWillMount() {
		await this.getURL();
		await this.getSchoolData();
	}


	async getSchoolData() {
		let scid = await AsyncStorage.getItem('schoolId');
		let schoolData = await fetch('https://controlescolar.pro/api/get/escuelas/datos/' + scid);
		let scData = await schoolData.json();
		this.setState({escuelasDatos: scData[0]});
	}

	async getURL() {
		this.setState({
			uri: await AsyncStorage.getItem('uri'),
			mainColor: await AsyncStorage.getItem('mainColor'),
			secondColor: await AsyncStorage.getItem('secondColor'),
			thirdColor: await AsyncStorage.getItem('thirdColor'),
			fourthColor: await AsyncStorage.getItem('fourthColor')
		});
		await this.changeProp()
	}

	async changeProp() {
		this.state.email = this.props.email_user;
		await this.setState({aux: 0});
	}

	async openModal() {
		await this.setState({isModalComent: true});
	}

	async closeModal() {
		await this.setState({isModalComent: false});
	}

	render() {
		return (
			<ImageBackground
				style={styles.container}
				source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
			>
				<KeyboardAvoidingView behavior='position' style={[styles.container, {backgroundColor: 'transparent'}]}>
					<StatusBar
						backgroundColor={this.state.mainColor}
						barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
					/>
					<View
						style={{
							alignItems: 'center',
							marginTop: responsiveHeight(1)
						}}>
						<Image
							style={[styles.logo,{height:responsiveHeight(35)}]}
							source={{
								uri: 'https://controlescolar.pro/images/' + this.state.escuelasDatos.nombre_corto + '.png'
							}}
						/>
					</View>
					<View style={[styles.login]}>
						<Text style={[styles.jelow, {
							color: this.state.mainColor,
							marginTop: Platform.OS === 'ios' ? 10 : 0
						}]}>
							¡Bienvenido!
						</Text>
						<Spinner visible={this.state.visible} textContent='Verificando...'/>
						{Platform.OS === 'ios' ?
							(<View style={[styles.textInputV2, styles.row, {
								borderColor: this.state.secondColor,
								backgroundColor: 'transparent'
							}]}>
								<TextInput
									ref='email'
									placeholder='Email'
									onChangeText={this.handleEmail}
									keyboardType='email-address'
									returnKeyType='next'
									underlineColorAndroid='transparent'
									autoCapitalize='none'
									autoCorrect={false}
									defaultValue={this.state.email}
									style={[styles.inputDismmis, {backgroundColor: 'transparent'}]}
									onSubmitEditing={() => this.refs.password.focus()}
								/>
								<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
									<MaterialCommunityIcons color={'grey'} name='keyboard-close' size={25}/>
								</TouchableWithoutFeedback>

							</View>) : (<TextInput
								ref='email'
								placeholder='Email'
								onChangeText={this.handleEmail}
								keyboardType='email-address'
								returnKeyType='next'
								defaultValue={this.state.email}
								underlineColorAndroid='transparent'
								autoCapitalize='none'
								autoCorrect={false}
								style={[styles.textInput, {
									borderColor: this.state.secondColor,
									backgroundColor: 'transparent'
								}]}
								onSubmitEditing={() => this.refs.password.focus()}
							/>)}
						<TextInput
							ref='password'
							placeholder='Contraseña'
							secureTextEntry={true}
							underlineColorAndroid='transparent'
							onChangeText={this.handlePassword}
							returnKeyType='go'
							autoCapitalize='none'
							style={[styles.textInput, {
								borderColor: this.state.secondColor,
								backgroundColor: 'transparent'
							}]}
							onSubmitEditing={() => this._fetchLoginData(this.state.email, this.state.password, this)}
						/>
						<TouchableOpacity
							onPress={() => this._fetchLoginData(this.state.email, this.state.password, this)}
							style={[styles.bigButtonLogin, {
								backgroundColor: this.state.secondColor, borderColor: this.state.secondColor
							}]}
						>
							<Text style={styles.textButton}>Iniciar Sesión</Text>
						</TouchableOpacity>
						<View style={[styles.row, {marginTop: responsiveHeight(3)}]}>
							<TouchableOpacity style={{width: responsiveWidth(42), paddingVertical: responsiveHeight(1)}}
											  onPress={() => login.cambiarEscuela()}>
								<Text style={[styles.noEscuela, {
									textAlign: 'center',
									fontSize: responsiveFontSize(1.5)
								}]}>Cambiar
									Escuela</Text>
							</TouchableOpacity>
							<TouchableOpacity style={{width: responsiveWidth(52), paddingVertical: responsiveHeight(1)}}
											  onPress={() => this.openModal()}>
								<Text style={[styles.noEscuela, {
									textAlign: 'center',
									fontSize: responsiveFontSize(1.5)
								}]}>¿Olvidó su contraseña?</Text>
							</TouchableOpacity>
						</View>
						<View style={[styles.row, {marginTop: responsiveHeight(2)}]}>
							<TouchableOpacity style={{width: responsiveWidth(52), paddingVertical: responsiveHeight(1)}}
											  onPress={() => Actions.preinscrip()}>
								<Text style={[styles.noEscuela, {
									textAlign: 'center',
									fontSize: responsiveFontSize(1.5)
								}]}>Iniciar preinscripción</Text>
							</TouchableOpacity>
							<TouchableOpacity style={{width: responsiveWidth(52), paddingVertical: responsiveHeight(1)}}
											  onPress={() => Actions.loginPreinscrip()}>
								<Text style={[styles.noEscuela, {
									textAlign: 'center',
									fontSize: responsiveFontSize(1.5)
								}]}>Continuar preinscripción</Text>
							</TouchableOpacity>
						</View>
						<Modal
							isVisible={this.state.isModalComent}
							backdropOpacity={0.5}
							animationIn={'bounceIn'}
							animationOut={'bounceOut'}
							animationInTiming={1000}
							animationOutTiming={1000}
						>
							<KeyboardAvoidingView behavior='position'>
								<View style={{height: responsiveHeight(40), alignItems: 'center'}}>
									<View style={[styles.container, {
										width: responsiveWidth(94),
										borderRadius: 10,
										alignItems: 'center'
									}]}>
										<Text
											style={[styles.main_title, styles.modalWidth, {
												color: this.state.thirdColor,
												textAlign: 'center'
											}]}>
											Ingrese el correo con el que se dio de alta
										</Text>

										<View style={{height: responsiveHeight(4)}}/>
										<TextInput
											ref='correo'
											placeholder='Correo'
											underlineColorAndroid='transparent'
											onChangeText={this.handleCorreo}
											returnKeyType='go'
											autoCapitalize='none'
											style={[styles.textInput, styles.modalWidth, {borderColor: this.state.secondColor}]}
										/>
										<TouchableOpacity
											onPress={() => this.requestCorreo(this.state.correo)}
											style={[styles.bigButtonLogin, styles.modalWidth, {
												backgroundColor: this.state.secondColor,
												borderColor: this.state.secondColor
											}]}>
											<Text style={styles.textButton}>Restablecer contraseña</Text>
										</TouchableOpacity>
										<TouchableOpacity onPress={() => this.closeModal()}
														  style={[styles.textButton, {marginTop: responsiveHeight(2)}]}><Text>Cancelar</Text></TouchableOpacity>
									</View>
								</View>
							</KeyboardAvoidingView>
						</Modal>
					</View>
					<View style={{
						position: 'relative',
						bottom: 0,
						height: Platform.OS === 'ios' ? 55 : 40,
						justifyContent: 'flex-end'
					}}>
						<Text style={styles.footer}>Todos los derechos reservados.</Text>
						<Text style={styles.footersito}>
							Control Escolar Profesional &copy;2017 - {moment().year()}
						</Text>
					</View>
				</KeyboardAvoidingView>
			</ImageBackground>
		);
	}

	async requestCorreo(c) {
		await fetch(this.state.uri + '/api/get/enviarCorreo/resetPassword/' + c,
			{
				method: 'get',
				headers: {
					Accept: 'application/json, text/plain, */*',
					'Content-Type': 'application/json'
				}
			})
			.then(response => {
				return response.json();

			}).then(responseJson => {
				this.closeModal();
				Alert.alert(
					'Restablecer Contraseña',
					'Se le ha enviado un correo con una contraseña temporal.'
				);
			});
	}

	async saveItem(item, selectedValue) {
		try {
			await AsyncStorage.setItem(item, selectedValue);
		} catch (error) {
			console.error('AsyncStorage error: ' + error.message);
		}
	}
}
