import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
} from "react-native";
import {Actions} from "react-native-router-flux";
import {RadioButton, RadioGroup} from "react-native-flexi-radio-button";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import Ionicons from "react-native-vector-icons/Ionicons";

export default class aplicEnc extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: "",
            solicitudes: [],
            pregunta: [
                {id: "1", grupo: "Categoría 1"},
                {id: "2", grupo: "Categoría 2"},
                {id: "3", grupo: "Categoría 3"}
            ],
            sigCategoria: 0
        };
    }

    async onSelect(index, value) {
        await this.setState({role: value});
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    renderExamen(item, index) {
        return (
            <View>
                {this.state.sigCategoria === index ? (
                    <View key={index}>
                        <Text style={styles.main_title}>{item}</Text>
                        <Text
                            style={{
                                marginTop: 5,
                                alignItems: "flex-start",
                                width: responsiveWidth(94),
                                fontSize: responsiveFontSize(2)
                            }}>
                            Esta es una pregunta de evaluación de la categoría 1
                        </Text>
                        <View
                            style={{
                                marginTop: 10,
                                width: responsiveWidth(94),
                                alignItems: "center"
                            }}>
                            <RadioGroup
                                style={{
                                    height: responsiveHeight(5)
                                }}
                                onSelect={(index, value) => this.onSelect(index, value)}>
                                <RadioButton value={"Siempre"}>
                                    <Text>Siempre</Text>
                                </RadioButton>
                                <RadioButton value={"Casisiempre"}>
                                    <Text>Casi siempre</Text>
                                </RadioButton>
                                <RadioButton value={"Regularmente"}>
                                    <Text>Regularmente</Text>
                                </RadioButton>
                                <RadioButton value={"Casinunca"}>
                                    <Text>Casi nunca</Text>
                                </RadioButton>
                                <RadioButton value={"Todos"}>
                                    <Text>Nunca</Text>
                                </RadioButton>
                            </RadioGroup>
                        </View>
                    </View>
                ) : null}
            </View>
        );
    }

    renderExamenes() {
        let buttons = [];
        this.state.pregunta.forEach((item, index) => {
            buttons.push(this.renderExamen(item.grupo, index));
        });
        return buttons;
    }

    renderClass() {
        return (
            <View style={styles.centered_RT}>
                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor,
                            marginTop: 10
                        }
                    ]}>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={{fontWeight: "800"}}>Maestro: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={styles.textCenter}>Joaquin Guzman Loera</Text>
                    </View>
                </View>
                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor
                        }
                    ]}>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={{fontWeight: "800"}}>Materia: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>Ciencias</Text>
                    </View>
                </View>

                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor,
                            marginBottom: 10
                        }
                    ]}>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={{fontWeight: "800"}}>Grado y grupo:</Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={styles.textCenter}>1C</Text>
                    </View>
                </View>
            </View>
        );
    }

    categoria() {
        if (this.state.sigCategoria < this.state.pregunta.length - 1) {
            this.setState({
                sigCategoria: this.state.sigCategoria + 1
            });
        } else {
            this.setState({
                sigCategoria: this.state.pregunta.length - 1
            });
        }
    }

    categoriaR() {
        if (this.state.sigCategoria <= 0) {
            this.setState({
                sigCategoria: 0
            });
        } else {
            this.setState({
                sigCategoria: this.state.sigCategoria - 1
            });
        }
    }

    render(item, index) {
        const pregunta = this.renderExamenes();
        const maestro = this.renderClass();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView>
                    <Text style={styles.main_title}>Encuesta de evaluacion 1</Text>
                    {maestro}
                    {pregunta}
                </ScrollView>
                <View style={[styles.rowsCalif, {width: responsiveWidth(75)}]}>
                    <TouchableOpacity onPress={() => this.categoriaR()}>
                        <Ionicons
                            name="ios-arrow-dropleft-outline"
                            size={70}
                            color={this.state.thirdColor}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.categoria()}>
                        <Ionicons
                            name="ios-arrow-dropright"
                            size={70}
                            color={this.state.thirdColor}
                        />
                    </TouchableOpacity>
                </View>
                <View>
                    {this.state.sigCategoria == 2 ? (
                        <View>
                            <TouchableOpacity
                                style={[
                                    styles.bigButton,
                                    {backgroundColor: this.state.secondColor}
                                ]}
                                onPress={() => Actions.aplicEnc()}>
                                <Text style={styles.textButton}>Guardar y finalizar</Text>
                            </TouchableOpacity>
                        </View>
                    ) : null}
                </View>
            </View>
        );
    }
}
