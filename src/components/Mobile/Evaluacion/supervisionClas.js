import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableHighlight,
    TouchableOpacity,
    ScrollView,
    TextInput
} from "react-native";
import {Actions} from "react-native-router-flux";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import StepIndicator from "rn-step-indicator";
import Modal from "react-native-modal";
import Entypo from "react-native-vector-icons/Entypo";
import styles from "../../styles";
import GradoyGrupo from "../Globales/GradoyGrupo";
import ModalSelector from "react-native-modal-selector";
import MultiBotonRow from "../Globales/MultiBotonRow";

const encParam = [
    "Añadir categoría",
    "Añadir preguntas",
    "Estilo de respuestas"
];
const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class supervisionClas extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            isDateTimePickerVisible: false,
            botonSelected: "Crear encuesta",
            currentPosition: 0,
            temporal: false,
            calidad: false,
            temporal1: false,
            calidad1: false,
            fecha: [],
            fecha1: "",
            fechadb: "",
            data: "",
            tipo: "",
            value: true,
            selectedIndexGrados: -1,
            grado: "",
            selectedIndexGrupos: -1,
            grupo: "",
            sigPaso: 0
        };
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    async onSelect(index, value) {
        await this.setState({role: value});
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            selected: option.id
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    onPageChange(position) {
        this.setState({
            currentPosition: position
        });
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado
        });
    }

    renderClass() {
        return (
            <View style={styles.centered_RT}>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    El maestro asignado a esta clase es:
                </Text>
                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor,
                            marginTop: 10
                        }
                    ]}>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={{fontWeight: "800"}}>Maestro: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={styles.textCenter}>Joaquin Guzman Loera</Text>
                    </View>
                </View>
                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor
                        }
                    ]}>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={{fontWeight: "800"}}>Materia: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>Ciencias</Text>
                    </View>
                </View>

                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor,
                            marginBottom: 10
                        }
                    ]}>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={{fontWeight: "800"}}>Grado y grupo: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={styles.textCenter}>1C</Text>
                    </View>
                </View>
            </View>
        );
    }

    pasoSig() {
        if (this.state.currentPosition <= 1) {
            this.setState({
                currentPosition: this.state.currentPosition + 1,
                temporal: false,
                calidad: false
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition + 0,
                temporal: false,
                calidad: false
            });
        }
    }

    pasoAnt() {
        if (this.state.currentPosition <= 0) {
            this.setState({
                currentPosition: 0,
                temporal: false,
                calidad: false
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition - 1,
                temporal: false,
                calidad: false
            });
        }
    }

    editEnc() {
        let customStyles = {
            stepIndicatorSize: 25,
            currentStepIndicatorSize: 30,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: "#777777",
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: "#777777",
            stepStrokeUnFinishedColor: "#aaaaaa",
            separatorFinishedColor: "#777777",
            separatorUnFinishedColor: "#aaaaaa",
            stepIndicatorFinishedColor: "#777777",
            stepIndicatorUnFinishedColor: "#ffffff",
            stepIndicatorCurrentColor: "#ffffff",
            stepIndicatorLabelFontSize: 13,
            currentStepIndicatorLabelFontSize: 13,
            stepIndicatorLabelCurrentColor: "#777777",
            stepIndicatorLabelFinishedColor: "#ffffff",
            stepIndicatorLabelUnFinishedColor: "#aaaaaa",
            labelColor: "#aaaaaa",
            labelSize: responsiveFontSize(1.3),
            currentStepLabelColor: "#777777"
        };
        let texto = [
            styles.textoN,
            {fontSize: responsiveFontSize(1.7), fontWeight: "700"}
        ];
        let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.calidad === true) {
            btnCal.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7),
                fontWeight: "700"
            });
        }
        let texto1 = [
            styles.textoN,
            {fontSize: responsiveFontSize(1.7), fontWeight: "700"}
        ];
        let btnTem = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.temporal === true) {
            btnTem.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto1.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7),
                fontWeight: "700"
            });
        }
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View
                        style={{
                            width: responsiveWidth(94),
                            marginTop: 10,
                            marginBottom: 8
                        }}>
                        <StepIndicator
                            customStyles={customStyles}
                            stepCount={3}
                            currentPosition={this.state.currentPosition}
                            labels={encParam}
                        />
                        {this.state.currentPosition === 0 ? (
                            <View>
                                <Text
                                    style={[styles.main_title, {color: this.state.thirdColor}]}>
                                    Agregue las categorías
                                </Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({isModalCategoria: true})}>
                                    <View style={styles.miniRowPedN}>
                                        <Entypo name="plus" size={20} color="black"/>
                                        <Text>Categoría 1</Text>
                                    </View>
                                </TouchableOpacity>
                                <Modal
                                    isVisible={this.state.isModalCategoria}
                                    backdropOpacity={0.5}
                                    animationIn={"bounceIn"}
                                    animationOut={"bounceOut"}
                                    animationInTiming={1000}
                                    animationOutTiming={1000}>
                                    <View style={[styles.container, {borderRadius: 6}]}>
                                        <View style={styles.centered_RT}>
                                            <Text style={[styles.main_title, styles.modalWidth]}>
                                                Agrega una categoría
                                            </Text>
                                        </View>
                                        <TextInput
                                            keyboardType="default"
                                            maxLength={256}
                                            multiline={true}
                                            returnKeyType="next"
                                            underlineColorAndroid="transparent"
                                            style={[
                                                styles.inputComentarios,
                                                {borderColor: this.state.secondColor}
                                            ]}
                                        />
                                        <TouchableOpacity
                                            style={[
                                                styles.bigButton,
                                                styles.modalWidth,
                                                {
                                                    backgroundColor: this.state.mainColor,
                                                    borderColor: this.state.mainColor
                                                }
                                            ]}
                                            onPress={() =>
                                                this.setState({isModalCategoria: false})
                                            }>
                                            <Text style={styles.textButton}>Guardar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Modal>
                            </View>
                        ) : null}
                        {this.state.currentPosition === 1 ? (
                            <View>
                                <Text
                                    style={[styles.main_title, {color: this.state.thirdColor}]}>
                                    Agregue las Preguntas
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: "700",
                                        fontSize: responsiveFontSize(2),
                                        marginTop: 5
                                    }}>
                                    1er categoría
                                </Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({isModalPreg: true})}>
                                    <View style={styles.miniRowPedN}>
                                        <Entypo name="plus" size={20} color="black"/>
                                        <Text>Pregunta 1</Text>
                                    </View>
                                </TouchableOpacity>
                                <Modal
                                    isVisible={this.state.isModalPreg}
                                    backdropOpacity={0.5}
                                    animationIn={"bounceIn"}
                                    animationOut={"bounceOut"}
                                    animationInTiming={1000}
                                    animationOutTiming={1000}>
                                    <View style={[styles.container, {borderRadius: 6}]}>
                                        <View style={styles.centered_RT}>
                                            <Text style={[styles.main_title, styles.modalWidth]}>
                                                Agrega una pregunta
                                            </Text>
                                        </View>
                                        <TextInput
                                            keyboardType="default"
                                            maxLength={256}
                                            multiline={true}
                                            returnKeyType="next"
                                            underlineColorAndroid="transparent"
                                            style={[
                                                styles.inputComentarios,
                                                {borderColor: this.state.secondColor}
                                            ]}
                                        />
                                        <TouchableOpacity
                                            style={[
                                                styles.bigButton,
                                                styles.modalWidth,
                                                {
                                                    backgroundColor: this.state.mainColor,
                                                    borderColor: this.state.mainColor
                                                }
                                            ]}
                                            onPress={() => this.setState({isModalPreg: false})}>
                                            <Text style={styles.textButton}>Guardar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Modal>
                            </View>
                        ) : null}
                        {this.state.currentPosition === 2 ? (
                            <View style={{alignItems: "center", marginTop: 10}}>
                                <Text
                                    style={[styles.main_title, {color: this.state.thirdColor}]}>
                                    Seleccione tipo de respuesta
                                </Text>
                                <View
                                    style={[
                                        styles.rowsCalif,
                                        {
                                            width: responsiveWidth(62.5),
                                            marginTop: 5
                                        }
                                    ]}>
                                    <TouchableHighlight
                                        underlayColor={this.state.thirdColor}
                                        style={btnTem}
                                        onPress={() => {
                                            if (this.state.temporal === false) {
                                                this.setState({temporal: true, calidad: false});
                                            } else {
                                                this.setState({temporal: true, calidad: false});
                                            }
                                        }}>
                                        <Text style={texto1}>Temporales</Text>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        underlayColor={this.state.thirdColor}
                                        style={btnCal}
                                        onPress={() => {
                                            if (this.state.calidad === false) {
                                                this.setState({
                                                    calidad: true,
                                                    temporal: false
                                                });
                                            } else {
                                                this.setState({
                                                    calidad: true,
                                                    temporal: false
                                                });
                                            }
                                        }}>
                                        <Text style={texto}>Calidad</Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                        ) : null}
                    </View>
                    {this.state.temporal == true ? (
                        <View
                            style={{
                                height: responsiveHeight(30),
                                width: responsiveWidth(94),
                                marginTop: 10,
                                alignItems: "center"
                            }}>
                            <View style={[styles.rowEnc, {width: responsiveWidth(50)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Muy ocasionalmente</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(50)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Ocasionalmente</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(50)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>A veces</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(50)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Frecuentemente</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(50)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Muy frecuentemente</Text>
                            </View>
                        </View>
                    ) : null}
                    {this.state.calidad == true ? (
                        <View
                            style={{
                                marginTop: 10,
                                height: responsiveHeight(30),
                                width: responsiveWidth(94),
                                alignItems: "center"
                            }}>
                            <View style={[styles.rowEnc, {width: responsiveWidth(35)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Muy bueno</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(35)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Bueno</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(35)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Regular</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(35)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Malo</Text>
                            </View>
                            <View style={[styles.rowEnc, {width: responsiveWidth(35)}]}>
                                <View
                                    style={[
                                        styles.btncitoEnc,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <Text style={{textAlign: "center"}}>Muy malo</Text>
                            </View>
                        </View>
                    ) : null}
                </ScrollView>
                <View
                    style={[
                        styles.rowsCalif,
                        {width: responsiveWidth(100), marginTop: 20}
                    ]}>
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {
                                backgroundColor: this.state.secondColor
                            }
                        ]}
                        onPress={() => this.pasoAnt()}>
                        <Text style={styles.textButton}>Paso anterior</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {
                                backgroundColor: this.state.secondColor,
                                borderLeftWidth: 1,
                                borderColor: "white"
                            }
                        ]}
                        onPress={() => this.pasoSig()}>
                        <Text style={styles.textButton}>Paso Siguiente</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    apliEnc() {
        const clase = this.renderClass();
        let index = 0;
        const data = [
            {key: index++, label: "7:00"},
            {key: index++, label: "8:00"},
            {key: index++, label: "9:00"},
            {key: index++, label: "10:00"},
            {key: index++, label: "11:00"}
        ];
        const data1 = [
            {key: index++, label: "10min"},
            {key: index++, label: "15min"},
            {key: index++, label: "20min"},
            {key: index++, label: "25min"}
        ];
        const dataEnc = [
            {key: index++, label: "Encuesta de evaluación 1"},
            {key: index++, label: "Encuesta de evaluación 2"},
            {key: index++, label: "Encuesta de evaluación 3"}
        ];
        return (
            <View style={styles.container}>
                <ScrollView>
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione la materia
                    </Text>
                    <ModalSelector
                        cancelText="Cancelar"
                        initValue="Seleccione la materia"
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor, marginTop: 1}
                        ]}
                    />
                    {clase}
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione la encuesta a aplicar
                    </Text>
                    <ModalSelector
                        data={dataEnc}
                        selectStyle={{
                            width: responsiveWidth(94),
                            borderColor: this.state.secondColor
                        }}
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        initValue="Seleccione la encuesta"
                        onChange={option => this.onChange(option)}
                    />
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}
                    onPress={() => Actions.aplicEnc()}>
                    <Text style={styles.textButton}>Supervisar esta clase</Text>
                </TouchableOpacity>
            </View>
        );
    }

    verAcum() {
        let index = 0;
        const data = [
            {key: index++, label: "Encuesta de evaluación 1"},
            {key: index++, label: "Encuesta de evaluación 2"},
            {key: index++, label: "Encuesta de evaluación 3"}
        ];
        let texto = [
            styles.textoN,
            {fontSize: responsiveFontSize(1.7), fontWeight: "700"}
        ];
        let btnPer = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.porPer === true) {
            btnPer.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7),
                fontWeight: "700"
            });
        }
        let texto1 = [
            styles.textoN,
            {fontSize: responsiveFontSize(1.7), fontWeight: "700"}
        ];
        let btnMae = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.porMae === true) {
            btnMae.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto1.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7),
                fontWeight: "700"
            });
        }
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione la encuesta a revisar
                    </Text>
                    <ModalSelector
                        data={data}
                        selectStyle={{
                            width: responsiveWidth(94),
                            borderColor: this.state.secondColor
                        }}
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        initValue="Seleccione la encuesta"
                        onChange={option => this.onChange(option)}
                    />
                    <View style={{alignItems: "center", marginTop: 10}}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione tipo de resultados
                        </Text>
                        <View
                            style={[
                                styles.rowsCalif,
                                {width: responsiveWidth(62.5), marginTop: 5}
                            ]}>
                            <TouchableHighlight
                                underlayColor={this.state.thirdColor}
                                style={btnPer}
                                onPress={() => {
                                    if (this.state.porPer === false) {
                                        this.setState({porPer: true, porMae: false});
                                    } else {
                                        this.setState({porPer: true, porMae: false});
                                    }
                                }}>
                                <Text style={texto}>Por periodo</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={this.state.thirdColor}
                                style={btnMae}
                                onPress={() => {
                                    if (this.state.porMae === false) {
                                        this.setState({porMae: true, porPer: false});
                                    } else {
                                        this.setState({porMae: true, porPer: false});
                                    }
                                }}>
                                <Text style={texto1}>Por maestro</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Grafica de resultados
                    </Text>
                </ScrollView>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <MultiBotonRow
                    itemBtns={["Crear encuesta", "Aplicar encuesta", "Ver resultados"]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={3}
                />
                {this.state.botonSelected === "Crear encuesta" ? this.editEnc() : null}
                {this.state.botonSelected === "Aplicar encuesta"
                    ? this.apliEnc()
                    : null}
                {this.state.botonSelected === "Ver resultados" ? this.verAcum() : null}
            </View>
        );
    }
}
