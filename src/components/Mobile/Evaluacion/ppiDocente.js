import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity
} from "react-native";
import {
    responsiveFontSize,
    responsiveWidth
} from "react-native-responsive-dimensions";
import Ionicons from "react-native-vector-icons/Ionicons";
import styles from "../../styles";

export default class ppiDocentes extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            parametro: false,
            indicador: false,
            parametro2: false,
            indicador2: false
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    indicadores() {
        return (
            <View style={{width: responsiveWidth(94)}}>
                {this.state.indicador === true ? (
                    <View
                        style={{
                            width: responsiveWidth(94),
                            marginTop: 10,
                            alignItems: "flex-end",
                            marginLeft: 20
                        }}>
                        <View style={{flexDirection: "row", width: responsiveWidth(85)}}>
                            <Text>1.1.1 </Text>
                            <Text style={{width: responsiveWidth(65)}}>
                                Identifica los procesos de desarrollo y aprendizaje de los
                                adolescentes como referentes para conocer a los alumnos.
                            </Text>
                        </View>
                    </View>
                ) : null}
                {this.state.indicador2 === true ? (
                    <View
                        style={{
                            width: responsiveWidth(94),
                            marginTop: 10,
                            alignItems: "flex-end",
                            marginLeft: 20
                        }}>
                        <View style={{flexDirection: "row", width: responsiveWidth(85)}}>
                            <Text>2.1.1 </Text>
                            <Text style={{width: responsiveWidth(65)}}>
                                Identifica las características del entorno escolar para la
                                organización de su intervención docente.
                            </Text>
                        </View>
                    </View>
                ) : null}
                {this.state.indicador3 === true ? (
                    <View
                        style={{
                            width: responsiveWidth(94),
                            marginTop: 10,
                            alignItems: "flex-end",
                            marginLeft: 20
                        }}>
                        <View style={{flexDirection: "row", width: responsiveWidth(85)}}>
                            <Text>3.1.1 </Text>
                            <Text style={{width: responsiveWidth(66)}}>
                                Identifica los aspectos a mejorar en su función docente como
                                resultado del análisis de las evidencias de su práctica.
                            </Text>
                        </View>
                    </View>
                ) : null}
                {this.state.indicador4 === true ? (
                    <View
                        style={{
                            width: responsiveWidth(94),
                            marginTop: 10,
                            alignItems: "flex-end",
                            marginLeft: 20
                        }}>
                        <View style={{flexDirection: "row", width: responsiveWidth(85)}}>
                            <Text>4.1.1 </Text>
                            <Text style={{width: responsiveWidth(63)}}>
                                Desarrolla su función docente con apego a los principios
                                filosóficos establecidos en el artículo tercero constitucional.
                            </Text>
                        </View>
                    </View>
                ) : null}
                {this.state.indicador5 === true ? (
                    <View
                        style={{
                            width: responsiveWidth(94),
                            marginTop: 24,
                            alignItems: "flex-end",
                            marginLeft: 20
                        }}>
                        <View style={{flexDirection: "row", width: responsiveWidth(85)}}>
                            <Text>5.1.1 </Text>
                            <Text style={{width: responsiveWidth(63)}}>
                                Participa con el colectivo docente en la elaboración del
                                diagnóstico escolar, para diseñar estrategias que permitan
                                cumplir con los propósitos educativos.
                            </Text>
                        </View>
                    </View>
                ) : null}
            </View>
        );
    }

    parametros() {
        return (
            <View>
                {this.state.parametro === true ? (
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.indicador === false) {
                                this.setState({
                                    indicador: true,
                                    parametro2: false,
                                    indicador2: false
                                });
                            } else {
                                this.setState({indicador: false});
                            }
                        }}>
                        <View
                            style={[
                                styles.rowPPI,
                                {
                                    justifyContent: "space-between",
                                    marginTop: 1,
                                    alignItems: "flex-start"
                                }
                            ]}>
                            <Text
                                style={{fontWeight: "700", fontSize: responsiveFontSize(2)}}>
                                1.1
                            </Text>
                            <Text
                                style={{fontWeight: "700", fontSize: responsiveFontSize(2)}}>
                                Conoce los procesos de desarrollo y de aprendizaje de los
                                adolescentes.
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                ) : null}
                {this.state.parametro2 === true ? (
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.indicador2 === false) {
                                this.setState({
                                    indicador2: true,
                                    indicador: false,
                                    parametro: false
                                });
                            } else {
                                this.setState({indicador2: false});
                            }
                        }}>
                        <View
                            style={[
                                styles.rowPPI,
                                {
                                    marginTop: 1,
                                    justifyContent: "space-between",
                                    alignItems: "flex-start"
                                }
                            ]}>
                            <Text
                                style={{fontWeight: "700", fontSize: responsiveFontSize(2)}}>
                                2.1
                            </Text>
                            <Text
                                style={{
                                    fontWeight: "700",
                                    fontSize: responsiveFontSize(2),
                                    width: responsiveWidth(75)
                                }}>
                                Organiza su intervención docente para el aprendizaje de sus
                                alumnos.
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                ) : null}
                {this.state.parametro3 === true ? (
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.indicador3 === false) {
                                this.setState({
                                    indicador3: true,
                                    indicador: false,
                                    parametro: false
                                });
                            } else {
                                this.setState({indicador3: false});
                            }
                        }}>
                        <View
                            style={[
                                styles.rowPPI,
                                {
                                    marginTop: 1,
                                    marginBottom: 15,
                                    justifyContent: "space-between",
                                    alignItems: "flex-start"
                                }
                            ]}>
                            <Text
                                style={{fontWeight: "700", fontSize: responsiveFontSize(2)}}>
                                3.1
                            </Text>
                            <Text
                                style={{
                                    fontWeight: "700",
                                    fontSize: responsiveFontSize(2),
                                    width: responsiveWidth(75)
                                }}>
                                Reflexiona sistemáticamente sobre{"\n"}su práctica docente como
                                medio para mejorarla.
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                ) : null}
                {this.state.parametro4 === true ? (
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.indicador4 === false) {
                                this.setState({
                                    indicador4: true,
                                    indicador: false,
                                    parametro: false
                                });
                            } else {
                                this.setState({indicador4: false});
                            }
                        }}>
                        <View
                            style={[
                                styles.rowPPI,
                                {
                                    alignItems: "flex-start",
                                    marginTop: 1,
                                    marginBottom: 50,
                                    justifyContent: "space-between"
                                }
                            ]}>
                            <Text
                                style={{fontWeight: "700", fontSize: responsiveFontSize(2)}}>
                                4.1
                            </Text>
                            <Text
                                style={{
                                    fontWeight: "700",
                                    fontSize: responsiveFontSize(2),
                                    width: responsiveWidth(75)
                                }}>
                                Considera los principios filosóficos, los fundamentos legales y
                                las finalidades de la educación pública mexicana en el ejercicio{
                                "\n"
                            }de su función docente.
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                ) : null}
                {this.state.parametro5 === true ? (
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.indicador5 === false) {
                                this.setState({
                                    indicador5: true,
                                    indicador: false,
                                    parametro: false
                                });
                            } else {
                                this.setState({indicador5: false});
                            }
                        }}>
                        <View
                            style={[
                                styles.rowPPI,
                                {
                                    marginTop: 1,
                                    justifyContent: "space-between",
                                    alignItems: "flex-start"
                                }
                            ]}>
                            <Text
                                style={{fontWeight: "700", fontSize: responsiveFontSize(2)}}>
                                5.1
                            </Text>
                            <Text
                                style={{
                                    fontWeight: "700",
                                    fontSize: responsiveFontSize(2),
                                    width: responsiveWidth(75)
                                }}>
                                Realiza acciones en la gestión escolar para contribuir a la
                                calidad de los resultados educativos.
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                ) : null}
            </View>
        );
    }

    render() {
        const indicador = this.indicadores();
        const parametro = this.parametros();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <TouchableOpacity
                    onPress={() => {
                        if (this.state.parametro === false) {
                            this.setState({
                                parametro: true,
                                parametro5: false,
                                indicador5: false,
                                parametro2: false,
                                parametro4: false,
                                indicador4: false,
                                indicador2: false,
                                parametro3: false,
                                indicador3: false
                            });
                        } else {
                            this.setState({parametro: false, indicador: false});
                        }
                    }}>
                    <View
                        style={[
                            styles.rowPPI,
                            {
                                justifyContent: "space-between",
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                            Dimension 1
                        </Text>
                        <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                    </View>
                </TouchableOpacity>
                {this.state.parametro === true ? parametro : null}
                {this.state.indicador === true ? indicador : null}
                <TouchableOpacity
                    onPress={() => {
                        if (this.state.parametro2 === false) {
                            this.setState({
                                parametro2: true,
                                parametro4: false,
                                indicador5: false,
                                parametro5: false,
                                indicador4: false,
                                indicador3: false,
                                parametro: false,
                                parametro3: false,
                                indicador: false
                            });
                        } else {
                            this.setState({parametro2: false, indicador2: false});
                        }
                    }}>
                    <View
                        style={[
                            styles.rowPPI,
                            {
                                justifyContent: "space-between",
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                            Dimension 2
                        </Text>
                        <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                    </View>
                </TouchableOpacity>
                {this.state.parametro2 === true ? parametro : null}
                {this.state.indicador2 === true ? indicador : null}

                <TouchableOpacity
                    onPress={() => {
                        if (this.state.parametro3 === false) {
                            this.setState({
                                parametro3: true,
                                parametro4: false,
                                parametro5: false,
                                indicador5: false,
                                indicador4: false,
                                parametro2: false,
                                indicador2: false,
                                parametro: false,
                                indicador: false
                            });
                        } else {
                            this.setState({parametro3: false, indicador3: false});
                        }
                    }}>
                    <View
                        style={[
                            styles.rowPPI,
                            {
                                justifyContent: "space-between",
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                            Dimension 3
                        </Text>
                        <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                    </View>
                </TouchableOpacity>
                {this.state.parametro3 === true ? parametro : null}
                {this.state.indicador3 === true ? indicador : null}
                <TouchableOpacity
                    onPress={() => {
                        if (this.state.parametro4 === false) {
                            this.setState({
                                parametro4: true,
                                parametro3: false,
                                parametro2: false,
                                indicador3: false,
                                indicador2: false,
                                parametro5: false,
                                indicador5: false,
                                parametro: false,
                                indicador: false
                            });
                        } else {
                            this.setState({parametro4: false, indicador4: false});
                        }
                    }}>
                    <View
                        style={[
                            styles.rowPPI,
                            {
                                justifyContent: "space-between",
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                            Dimension 4
                        </Text>
                        <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                    </View>
                </TouchableOpacity>
                {this.state.parametro4 === true ? parametro : null}
                {this.state.indicador4 === true ? indicador : null}
                <TouchableOpacity
                    onPress={() => {
                        if (this.state.parametro5 === false) {
                            this.setState({
                                parametro5: true,
                                parametro4: false,
                                indicador4: false,
                                parametro3: false,
                                parametro2: false,
                                indicador3: false,
                                indicador2: false,
                                parametro: false,
                                indicador: false
                            });
                        } else {
                            this.setState({parametro5: false, indicador5: false});
                        }
                    }}>
                    <View
                        style={[
                            styles.rowPPI,
                            {
                                justifyContent: "space-between",
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                            Dimension 5
                        </Text>
                        <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                    </View>
                </TouchableOpacity>
                {this.state.parametro5 === true ? parametro : null}
                {this.state.indicador5 === true ? indicador : null}
            </View>
        );
    }
}
