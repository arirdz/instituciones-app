import React from 'react';
import {
	Alert, AsyncStorage, Image, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';

export default class pagoEfectivoP extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			laSolicitud: '',
			solicitudes: [],
			recibo: []
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}


	async pagoTienda(tutor) {
		let formData = new FormData();
		formData.append('parameters', JSON.stringify({
			method: 'store',
			amount: Number(this.props.montoInscrip),
			description: 'Pago de preinscripcion liceo',
			customer: {
				name: tutor.nombre,
				last_name: tutor.apellidos,
				phone_number: tutor.telefono_contacto,
				email: tutor.email
			}
		}));
		fetch(this.state.uri + '/api/pago/tienda/openpay/preinscrip', {
			method: 'POST',
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		}).then(response => response.json())
			.then(responseJson => {
				this.setState({recibo: responseJson.result});
				if (responseJson.response !== 'error') {
					this.updateAspirante(this.props.elAspirante, responseJson.result.id);
					Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
						text: 'Gracias',
						onPress: () => Actions.reciboTienda({
							reciboTienda: responseJson.result.serializableData,
							transaction_id: responseJson.transaction_id,
							user: tutor.email
						})
					}]);
					this.setState({recibo: responseJson});
				} else {
					if (responseJson.error.code === 3001) {
						Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3002) {
						Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirada', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3003) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3004) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3005) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
							text: 'Entendido'
						}]);

					} else if (responseJson.error.code === 1006) {
						Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
							text: 'Entendido'
						}]);

					} else {
						this.updateAspirante(this.props.elAspirante, responseJson.result.id);
						Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
							text: 'Entendido' , onPress: () => Actions.reciboTienda({
								reciboTienda: responseJson.result.serializableData,
								transaction_id: responseJson.transaction_id,
								user: tutor.email
							})
						}]);
					}
				}
			});

	}

	async updateAspirante(aspirante, charge_id) {
		if (this.props.tipo === 'preinscrip') {
			let formData = new FormData();
			formData.append('update', JSON.stringify({
				id: aspirante.id,
				paso_registro: '3.1',
				pago_preinscrip: charge_id
			}));
			await fetch(this.state.uri + '/api/cita/aspirantes', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data'
				}, body: formData
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al actualizar aspirante',
							'Ha ocurrido un error ' + responseJson.error.status_code + ' ' +
							'si el error continua pónganse en contacto con soporte (Pago en tarjeta)', [
								{
									text: 'Entendido',
									onPress: () => Actions.replace('aspirantesTutor',{key:Math.random()})
								}
							]);
					}
				});
		} else if (this.props.tipo === 'inscrip') {
			let formData = new FormData();
			formData.append('update', JSON.stringify({
				id: aspirante.id,
				paso_registro: '5.1',
				pago_inscrip: charge_id
			}));
			await fetch(this.state.uri + '/api/cita/aspirantes', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data'
				}, body: formData
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al actualizar aspirante',
							'Ha ocurrido un error ' + responseJson.error.status_code + ' ' +
							'si el error continua pónganse en contacto con soporte (Pago en tarjeta)', [
								{
									text: 'Entendido',
									onPress: () => Actions.replace('aspirantesTutor',{key:Math.random()})
								}
							]);
					}
				});
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione una tienda para generar el cargo
				</Text>
				<ScrollView style={styles.menu} showsVerticalScrollIndicator={false}>
					<View style={styles.row_M}>
						<TouchableOpacity onPress={() => this.pagoTienda(this.props.elAspirante.tutor)}
										  style={styles.btnContainer_M}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/superama.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.props.elAspirante.tutor)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/walmart.png')}/>
						</TouchableOpacity>
					</View>
					<View style={styles.row_M}>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.props.elAspirante.tutor)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/fa.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.props.elAspirante.tutor)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/sams.png')}/>
						</TouchableOpacity>
					</View>
					<View style={styles.row_M}>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.props.elAspirante.tutor)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/fg.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.props.elAspirante.tutor)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(9), width: responsiveWidth(35)
								}]}
								source={require('../../../images/waldos.png')}/>
						</TouchableOpacity>
					</View>
					<View style={styles.row_M}>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.props.dato_aspirante.tutor)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(9), width: responsiveWidth(35)
								}]}
								source={require('../../../images/prenda.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.props.elAspirante.tutor)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/aurrera.png')}/>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</View>
		);
	}
}
