import React from 'react';
import {
	Alert, AsyncStorage, Image, Keyboard, KeyboardAvoidingView, Platform, ScrollView, StatusBar, Text, TextInput,
	TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../../styles';
import Modal from 'react-native-modal';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export default class loginPreinscrip extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible,
			isModalComent: false,
			dismiss: 0
		});
	};
	handleCorreo = text => {
		this.setState({correo: text});
	};
	loginPreinsc = (mail, password, spinner) => {
		if (mail === '' || password === '') {
			Alert.alert(
				'Campos vacíos',
				'Es necesario llenar los campos de usuario y contraseña para realizar esta operación.'
			);
		} else {
			let mc1 = this.state.mainColor;
			let sc1 = this.state.secondColor;
			let tc1 = this.state.thirdColor;
			let fc1 = this.state.fourthColor;
			spinner._changeWheelState();
			fetch(this.state.uri + '/api/login/preinscripcion', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: mail,
					password: password
				})
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== 'Unauthorized') {
						if (responseJson.estatus !== 'alta') {
							this.saveItem('idTutor',responseJson.toString());
							this.saveItem('mainColor',mc1);
							this.saveItem('secondColor',sc1);
							this.saveItem('thirdColor',tc1);
							this.saveItem('fourthColor',fc1);
							this.saveItem('uri',this.state.uri);
							Actions.aspirantesTutor({aspirantes: responseJson});
							spinner._changeWheelState();
						} else {
							Alert.alert('Usuario registrado',
								'Ya has sido dado de alta, puedes continuar el proceso de los demas aspirantes en el ' +
								'apartado de inscripciones en el menu de la aplicación', [
									{text: 'Enterado', onPress: () => spinner._changeWheelState()}
								])
						}
					} else {
						switch (responseJson.error.status_code) {
							case 422:
								Alert.alert(
									'Formato inválido',
									'Es necesario poner un email válido.',
									[
										{
											text: 'OK',
											onPress: () => spinner._changeWheelState()
										}
									]
								);
								break;
							case undefined:
								Alert.alert(
									'Usuario o contraseña Incorrectos',
									'Ingresa los datos correctos',
									[
										{
											text: 'OK',
											onPress: () => spinner._changeWheelState()
										}
									]
								);
								break;
							case 403:
								Alert.alert(
									'Usuario o contraseña incorrectos',
									'Si el error persiste solicite ayuda.',
									[
										{
											text: 'OK',
											onPress: () => spinner._changeWheelState()
										}
									]
								);
								break;
							case 500:
								Alert.alert(
									'Error interno',
									'Por el momento no se puede procesar su solicitud, intente más tarde.',
									[
										{
											text: 'OK',
											onPress: () => spinner._changeWheelState()
										}
									]
								);
								break;
							default:
								alert(
									'Error:' + responseJson.error.status_code,
									'Solicite ayuda por favor',
									[
										{
											text: 'OK',
											onPress: () => spinner._changeWheelState()
										}
									]
								);
						}
					}
				}).catch(error => {
				Alert.alert(
					'ERROR DE CONEXIÓN',
					'Revisa tu conexión a internet. Si el problema persiste solicita ayuda.',
					[
						{
							text: 'OK',
							onPress: () => spinner._changeWheelState()
						}
					]
				);
			});
		}
	};

	handleEmail = text => {
		this.setState({correo: text});
	};

	handlePassword = text => {
		this.setState({password: text});
	};

	constructor(props) {
		super(props);
		this.loginPreinsc = this.loginPreinsc.bind(this);
		this.state = {
			data: [],
			visible: false,
			password: '',
			mainColor: '#ffff',
			secondColor: '#ffff',
			thirdColor: '#ffff',
			fourthColor: '#ffff',
			uri: 'https://controlescolar.pro',
			correo: '',
			escuelasDatos: []
		};
	}

	async componentWillMount() {
		await this.globalConfigs();
		await this.getSchoolData();
	}

	async saveItem(key,item){
		await AsyncStorage.setItem(key, item.toString());
	}


	async getSchoolData() {
		let scid = await AsyncStorage.getItem('schoolId');
		let schoolData = await fetch('https://controlescolar.pro/api/get/escuelas/datos/' + this.props.idschool);
		let scData = await schoolData.json();
		this.setState({escuelasDatos: scData[0]});
	}

	async globalConfigs() {
		let getConfig = await fetch(this.state.uri + '/api/global/config/values');
		let configSave = await getConfig.json();
		this.setState({
			mainColor: configSave[0].second_option.toString(),
			secondColor: configSave[1].second_option.toString(),
			thirdColor: configSave[2].second_option.toString(),
			fourthColor: configSave[3].second_option.toString()
		});
	}

	async requestCorreo(c) {
		await fetch(this.state.uri + '/api/recuperar/nip/' + c,
			{
				method: 'GET',
				headers: {
					Accept: 'application/json, text/plain, */*',
					'Content-Type': 'application/json'
				}
			})
			.then(response => {
				return response;
			}).then(responseJson => {
				Alert.alert(
					'Restablecer NIP',
					'Se le ha enviado un correo con un nuevo NIP.', [
						{text: 'Enterado', onPress: () => this.closeModal()}
					]
				);
			});
	}

	async openModal() {
		await this.setState({isModalComent: true})
	}

	async closeModal() {
		await this.setState({isModalComent: false})
	}

	render() {
		return (
			<KeyboardAvoidingView
				behavior={Platform.OS === 'ios' ? 'position' : 'position'}
				keyboardVerticalOffset={Platform.OS === 'ios' ? 30 : 65}
				style={styles.container}
			>
				<Spinner visible={this.state.visible} textContent='Verificando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				{Platform.OS === 'ios' ?
					<Image
						style={{width: responsiveWidth(100), height: responsiveHeight(30)}}
						source={require('../../../images/salon7.jpg')}
					/> :
					<Image
						style={{width: responsiveWidth(100), height: responsiveHeight(25)}}
						source={require('../../../images/salon7.jpg')}
					/>}
				<View style={{width: responsiveWidth(100), alignItems: 'center'}}>
					<Text
						style={{
							textAlign: 'center',
							color: this.state.secondColor,
							marginTop: Platform.OS === 'ios' ? 20 : 10,
							backgroundColor: 'transparent',
							fontWeight: '900',
							fontSize: responsiveFontSize(3.3)
						}}>
						Continuar proceso
					</Text>
					<Text style={[{width: responsiveWidth(94), marginBottom: 20, textAlign: 'center'}]}>
						Ingrese el correo con el que inició el registro seguido de la clave que se envió al mismo
					</Text>
					{Platform.OS === 'ios' ? (
						<View style={[styles.textInputV2, styles.row, {borderColor: this.state.secondColor}]}>
							<TextInput
								ref='email'
								placeholder='Email'
								onChangeText={this.handleEmail}
								keyboardType='email-address'
								returnKeyType='next'
								underlineColorAndroid='transparent'
								autoCapitalize='none'
								autoCorrect={false}
								style={[styles.inputDismmis, {textAlign: 'left'}]}
								onSubmitEditing={() => this.refs.password.focus()}
							/>
							<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
								<MaterialCommunityIcons color={'grey'} name='keyboard-close' size={25}/>
							</TouchableWithoutFeedback>
						</View>) : (<TextInput
						ref='email'
						placeholder='Email'
						onChangeText={this.handleEmail}
						keyboardType='email-address'
						returnKeyType='next'
						underlineColorAndroid='transparent'
						autoCapitalize='none'
						autoCorrect={false}
						style={[styles.textInput, {borderColor: this.state.secondColor, textAlign: 'left'}]}
						onSubmitEditing={() => this.refs.password.focus()}
					/>)}
					<TextInput
						ref='password'
						placeholder='Clave'
						secureTextEntry={true}
						underlineColorAndroid='transparent'
						onChangeText={this.handlePassword}
						returnKeyType='go'
						autoCapitalize='none'
						textAlign={'center'}
						style={[
							styles.textInput,
							{borderColor: this.state.secondColor, marginTop: 5, textAlign: 'left'}
						]}
						onSubmitEditing={() =>
							this.loginPreinsc(
								this.state.correo,
								this.state.password,
								this
							)
						}
					/>
					<TouchableOpacity
						onPress={() =>
							this.loginPreinsc(this.state.correo, this.state.password, this)
						}
						style={[
							styles.bigButtonLogin,
							{
								backgroundColor: this.state.mainColor,
								borderColor: this.state.mainColor,
								...Platform.select({android: {marginBottom: 15}})
							}
						]}
					>
						<Text style={styles.textButton}>
							Continuar registro
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{width: responsiveWidth(52), paddingVertical: responsiveHeight(1)}}
									  onPress={() => this.openModal()}>
						<Text style={[styles.noEscuela, {
							textAlign: 'center',
							marginVertical: Platform.OS === 'ios' ? 15 : -10,
							fontSize: responsiveFontSize(1.5)
						}]}>¿Olvidó su NIP?</Text>
					</TouchableOpacity>
				</View>
				<Modal
					isVisible={this.state.isModalComent}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<KeyboardAvoidingView behavior='position'>
						<View style={{height: responsiveHeight(40), alignItems: 'center'}}>
							<View style={[styles.container, {
								width: responsiveWidth(94),
								borderRadius: 10,
								alignItems: 'center'
							}]}>
								<Text
									style={[styles.main_title, styles.modalWidth, {
										color: this.state.thirdColor,
										textAlign: 'center'
									}]}>
									Ingrese el correo con el que se dio de alta
								</Text>

								<View style={{height: responsiveHeight(4)}}/>
								<TextInput
									ref='correo'
									placeholder='Correo'
									underlineColorAndroid='transparent'
									onChangeText={this.handleCorreo}
									returnKeyType='go'
									autoCapitalize='none'
									style={[styles.textInput, styles.modalWidth, {borderColor: this.state.secondColor}]}
								/>
								<TouchableOpacity
									onPress={() => this.requestCorreo(this.state.correo)}
									style={[styles.bigButtonLogin, styles.modalWidth, {
										backgroundColor: this.state.secondColor,
										borderColor: this.state.secondColor
									}]}>
									<Text style={styles.textButton}>Restablecer NIP</Text>
								</TouchableOpacity>
								<TouchableOpacity onPress={() => this.closeModal()}
												  style={[styles.textButton, {marginTop: responsiveHeight(2)}]}><Text>Cancelar</Text></TouchableOpacity>
							</View>
						</View>
					</KeyboardAvoidingView>
				</Modal>
			</KeyboardAvoidingView>
		);
	}
}
