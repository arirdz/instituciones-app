import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, StatusBar, Text, TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Fumi} from 'react-native-textinput-effects';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../../styles';
import Spinner from 'react-native-loading-spinner-overlay';

export default class recomendar extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			maincolor: '#fff',
			secondColor: '#fff',
			thirdColor: '#fff',
			fourthColor: '#fff',
			datosV2: [],
			visible: false,
			recomendados: [0],
			correo1: [],
			numero_tel: [],
			indxPersona: 0,
			nombre1: []
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.getUserdataV2();
	}

	async getUserdataV2() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				Authorization: 'Bearer ' + token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos',
						'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 4)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({datosV2: responseJson});
				}
			});
	}

	async alertRecomendacion() {
		let found1 = false;
		let found2 = false;
		let found2B = false;
		let found3 = false;
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		for (let i = 0; i < this.state.recomendados.length; i++) {
			let num_tel = this.state.numero_tel[i];
			if (this.state.nombre1[i] === '' || this.state.nombre1[i] === undefined) {
				found1 = true;
				break;
			} else if (this.state.correo1[i] === '' || this.state.correo1[i] === undefined) {
				found2 = true;
				break;
			} else if (reg.test(this.state.correo1[i]) === false) {
				found2B = true;
				break;
			} else if (num_tel === '' || num_tel === undefined || num_tel.length < 10) {
				found3 = true;
				break;
			}
		}
		if (found1 === true) {
			Alert.alert('Campo nombre vacío',
				'Asegurese de llenar todos los datos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else if (found2 === true) {
			Alert.alert('Campo correo vacío',
				'Asegurese de llenar todos los datos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else if (found2B === true) {
			Alert.alert('Correo invalido',
				'Asegurese de capturar correos validos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else if (found3 === true) {
			Alert.alert('Campo telefono vacío o incompleto',
				'Asegurese de llenar todos los datos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else {
			await this.requestMultiPart(this.state.nombre1, this.state.correo1, this.state.numero_tel);
		}
	}


	async requestMultiPart(nombre, correo, telefono) {
		this._changeWheelState();
		let request = [];
		for (let i = 0; i < this.state.recomendados.length; i++) {
			let formData = new FormData();
			formData.append('new', JSON.stringify({
				nombre: nombre[i],
				correo: correo[i],
				telefono: telefono[i],
				id_user: this.state.datosV2.id
			}));
			await fetch(this.state.uri + '/api/post/guardar/recomendados', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json'
				}, body: formData
			}).then(res => res)
				.then(responseJson => {
					request.push(responseJson);
				});
		}
		if (request.error !== undefined) {
			Alert.alert(
				'Error al enviar recomendacion',
				'Ha ocurrido un error al tratar de enviar el correo si el error continua pónganse en contacto con soporte (Enviar recomendación)',
				[{text: 'Entendido', onPress: () => this.setState({visible: false})}]
			);
		} else {
			await this.enviarCorreo();
		}
	}

	async enviarCorreo() {
		let request = [];
		for (let i = 0; i < this.state.recomendados.length; i++) {
			await fetch(this.state.uri + '/api/send/recomendacion', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json'
				}, body: JSON.stringify({
					id: this.state.datosV2.id.toString(),
					correo: this.state.correo1[i]
				})
			}).then(res => res)
				.then(responseJson => {
					request.push(responseJson);
				});
		}
		if (request.error !== undefined) {
			Alert.alert(
				'Error al cargar datos',
				'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Enviar recomendación)',
				[{text: 'Entendido', onPress: () => this.setState({visible: false})}]
			);
		} else {
			Alert.alert(
				'Recomendaciones enviadas',
				'Se han enviado las recomendaciones al los correos exitosamente ¡Gracias por tu apoyo!',
				[{
					text: 'Entendido',
					onPress: () => [this.setState({visible: false}), Actions.pop({refresh: {key: 'drawer'}})]
				}]
			);
		}
	}

	//+-+-+-+-+-+-+-+-+-+-+--+-+-+-+-+

	async validacionRec() {
		let found1 = false;
		let found2 = false;
		let found2B = false;
		let found3 = false;
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		for (let i = 0; i < this.state.recomendados.length; i++) {
			let num_tel = this.state.numero_tel[i];
			if (this.state.nombre1[i] === '' || this.state.nombre1[i] === undefined) {
				found1 = true;
				break;
			} else if (this.state.correo1[i] === '' || this.state.correo1[i] === undefined) {
				found2 = true;
				break;
			} else if (reg.test(this.state.correo1[i]) === false) {
				found2B = true;
				break;
			} else if (num_tel === '' || num_tel === undefined || num_tel.length < 10) {
				found3 = true;
				break;
			}
		}
		if (found1 === true) {
			Alert.alert('Campo nombre vacío', 'Asegurese de llenar todos los datos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else if (found2 === true) {
			Alert.alert('Campo correo vacío', 'Asegurese de llenar todos los datos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else if (found2B === true) {
			Alert.alert('Correo invalido', 'Asegurese de capturar correos validos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else if (found3 === true) {
			Alert.alert('Campo telefono vacío o incompleto', 'Asegurese de llenar todos los datos para poder agregar a otra persona',
				[{text: 'Enterado'}]);
		} else {
			await this.agregarAsp();
		}
	}

	async agregarAsp() {
		this.state.recomendados.push(0);
		this.state.nombre1.push('');
		this.state.correo1.push('');
		this.state.numero_tel.push('');
		await this.setState({aux: 0});
	}

	rndAspiranteBtns() {
		let btnAspirante = [];
		if (this.state.recomendados.length !== 0) {
			this.state.recomendados.forEach((it, i) => {
				btnAspirante.push(
					<TouchableHighlight
						key={i + 'btnRec'}
						underlayColor={'transparent'}
						style={{
							borderRadius: 6,
							paddingVertical: 4,
							paddingHorizontal: 10,
							borderWidth: 1,
							marginHorizontal: 3,
							borderColor: this.state.secondColor,
							backgroundColor: this.state.indxPersona === i ? this.state.secondColor : '#fff'
						}}
						onPress={() => this.setState({indxPersona: i})}
					>
						<Text
							style={{color: this.state.indxPersona === i ? '#fff' : '#000'}}>
							Recomendado {i + 1}
						</Text>
					</TouchableHighlight>
				)
			});
		}
		return btnAspirante;
	}

//+-+-+-+-+-+-+-+-+-+-+--+-+-+--+-+-+
	async deleteAspirante(i) {
		this.state.recomendados.splice(i, 1);
		this.state.nombre1.splice(i, 1);
		this.state.correo1.splice(i, 1);
		this.state.numero_tel.splice(i, 1);
		this.state.indxPersona[i] = -1;
		await this.setState({aux: 0});
	}

	rndInputs() {
		let inputs = [];
		if (this.state.recomendados.length !== 0) {
			this.state.recomendados.forEach((it, ix) => {
				if (this.state.indxPersona === ix) {
					inputs.push(
						<View key={ix + 'rec'}>
							<View style={[styles.row, styles.btn3T, {justifyContent: 'space-between', marginTop: 10}]}>
								<Text style={[styles.textW]}>Persona {ix + 1}</Text>
								{this.state.indxPersona !== 0 ? <Entypo
									name='circle-with-minus' size={18} color='red'
									onPress={() => this.deleteAspirante(ix)}
								/> : null}
							</View>
							<Fumi
								label={'Nombre(s)'}
								iconClass={Ionicons}
								style={[styles.inputDato, {borderColor: this.state.secondColor}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'md-person'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								onChangeText={text => (this.state.nombre1[ix] = text)}
								keyboardType='default'
								value={this.state.nombre1[ix]}
								returnKeyType='next'
								autoCapitalize='none'
								autoCorrect={false}
								// onSubmitEditing={() => this.refs.apellidoPat.focus()}
							/>
							<Fumi
								// ref='emailTut'
								label={'Email 1'}
								iconClass={Entypo}
								style={[styles.inputDato, {borderColor: this.state.secondColor, marginTop: 10}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'email'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								onChangeText={text => (this.state.correo1[ix] = text)}
								value={this.state.correo1[ix]}
								keyboardType='email-address'
								returnKeyType='next'
								autoCapitalize='none'
								autoCorrect={false}
							/>
							<Fumi
								ref='numeroTel'
								label={'Número telefónico'}
								iconClass={Ionicons}
								style={[styles.inputDato, {borderColor: this.state.secondColor, marginTop: 10}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'md-phone-portrait'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								maxLength={10}
								keyboardType='phone-pad'
								onChangeText={text => (this.state.numero_tel[ix] = text)}
								value={this.state.numero_tel[ix]}
								returnKeyType='next'
								autoCapitalize='none'
								autoCorrect={false}
								onSubmitEditing={() => this.refs.emailTut.focus()}
							/>
						</View>
					)
				}
			});
		}
		return inputs;
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Enviando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Agregue aquí los nombres y correos de hasta 2 personas a los que le recomendaría la escuela:
				</Text>
				<View style={[styles.row, styles.widthall, {justifyContent: 'flex-start', marginTop: 10}]}>
					{this.rndAspiranteBtns()}
					{this.state.recomendados.length === 3 ? null : (<Entypo
						name='circle-with-plus' size={30} color='#43d551' onPress={() => this.validacionRec()}
					/>)}
				</View>
				<ScrollView>
					{this.rndInputs()}
				</ScrollView>
				<TouchableOpacity
					style={[
						styles.bigButton,
						{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
					]}
					onPress={() => this.alertRecomendacion()}
				>
					<Text style={[styles.textButton]}>Enviar recomendación</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
