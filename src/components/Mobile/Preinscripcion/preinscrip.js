import React from 'react';
import {
	Alert, AsyncStorage, Keyboard, KeyboardAvoidingView, Platform, ScrollView, StatusBar, Text, TouchableHighlight,
	TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import ModalSelector from 'react-native-modal-selector';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Fumi} from 'react-native-textinput-effects';
import Spinner from 'react-native-loading-spinner-overlay';
import {TextInputMask} from 'react-native-masked-text';
import StepIndicator from 'rn-step-indicator';

const labels_estForm = [
	'Solicitar\ninfo',
	'Agendar\ncita',
	'Subir\ndocumentos',
	'Acreditar\npreinscripción',
	'Día de\ninmersión',
	'Acreditar\ninscripción'
];

const customStyles = {
	stepIndicatorSize: 20,
	currentStepIndicatorSize: 25,
	separatorStrokeWidth: 1,
	currentStepStrokeWidth: 2,
	stepStrokeCurrentColor: '#777777',
	separatorFinishedColor: '#777777',
	separatorUnFinishedColor: '#aaaaaa',
	stepIndicatorFinishedColor: '#777777',
	stepIndicatorUnFinishedColor: '#aaaaaa',
	stepIndicatorCurrentColor: '#ffffff',
	stepIndicatorLabelFontSize: responsiveFontSize(1.3),
	currentStepIndicatorLabelFontSize: responsiveFontSize(1.3),
	stepIndicatorLabelCurrentColor: '#000000',
	stepIndicatorLabelFinishedColor: '#ffffff',
	stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
	labelColor: '#666666',
	labelSize: responsiveFontSize(1),
	currentStepLabelColor: '#777777'
};

export default class preinscrip extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			maincolor: '#fff',
			secondColor: '#fff',
			thirdColor: '#fff',
			fourthColor: '#fff',
			visible: false,
			elGenero: [],
			tipoAlta: ['ingreso'],
			currentPosition: 0,
			addAspirant: false,
			losAspirantes: [{
				'nombre_aspitante': '',
				'grado_asp': '',
				'escuela_procendente': '',
				'estado_escuela': '',
				'promedio_gral': '',
				'curp_aspirante': ''
			}],
			nombreAlumn: [],
			aspirantes: [1],
			apellidMatAlumn: [],
			elGeneroTut: '',
			apellidPatAlumn: [],
			CurpAlumn: [],
			grados: [],
			elGrado: ['1'],
			selectedIndexGrados: [],
			elPromedio: [],
			escuela: [],
			escuelaInt: '',
			nombre: '',
			apellido_materno: '',
			apellido_paterno: '',
			numero_tel: '',
			elCurp: [],
			email: '',
			indexAspirante: 0,
			elRequest: [],
			tutorID: '',
			elEstado: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		this.state.losAspirantes.forEach(() => {
			this.state.grados.push('6');
			this.state.selectedIndexGrados.push(-1);
		});
		await this.setState({aux: 0});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async pressAspiranteBtn(it, i) {
		await this.setState({indexAspirante: i});
	}

	rndAspiranteBtns() {
		let btnAspirante = [];
		if (this.state.aspirantes.length !== 0) {
			this.state.aspirantes.forEach((it, i) => {
				btnAspirante.push(
					<TouchableHighlight
						key={i + 'btnAsp'}
						underlayColor={'transparent'}
						style={{
							borderRadius: 6,
							paddingVertical: 4,
							paddingHorizontal: 10,
							borderWidth: 1,
							marginHorizontal: 3,
							borderColor: this.state.secondColor,
							backgroundColor: this.state.indexAspirante === i ? this.state.secondColor : '#fff'
						}}
						onPress={() => this.pressAspiranteBtn(it, i)}
					>
						<Text
							style={{color: this.state.indexAspirante === i ? '#fff' : '#000'}}>
							Aspirante {i + 1}
						</Text>
					</TouchableHighlight>
				)
			});
		}
		return btnAspirante;
	}


	async alertSolicitudEnviar() {
		let found1 = false;
		let found2 = false;
		let found3 = false;
		let found4 = false;
		let found5 = false;
		let found6 = false;
		let found7 = false;
		let found8 = false;
		let found9 = false;
		for (let i = 0; i < this.state.losAspirantes.length; i++) {
			let elCurp = this.state.CurpAlumn[i];
			let laEscuela = this.state.escuela[i];
			if (this.state.tipoAlta[i] === '' || this.state.tipoAlta[i] === undefined) {
				found5 = true;
				break;
			} else if (this.state.nombreAlumn[i] === '' || this.state.nombreAlumn[i] === undefined) {
				found1 = true;
				break;
			} else if (this.state.apellidPatAlumn[i] === '' || this.state.apellidPatAlumn[i] === undefined) {
				found2 = true;
				break;
			} else if (this.state.apellidMatAlumn[i] === '' || this.state.apellidMatAlumn[i] === undefined) {
				found3 = true;
				break;
			} else if (elCurp === '' || elCurp === undefined || elCurp.length < 18) {
				found4 = true;
				break;
			} else if (this.state.elGenero[i] === '' || this.state.elGenero[i] === undefined) {
				found9 = true;
				break;
			} else if (this.state.selectedIndexGrados[i] === -1 && this.state.elGrado[i] === undefined) {
				found6 = true;
				break;
			} else if (laEscuela === '' || laEscuela === undefined || laEscuela.length < 10) {
				found7 = true;
				break;
			} else if (this.state.elPromedio[i] === '' || this.state.elPromedio[i] === undefined || this.state.elPromedio[i] > 10) {
				found8 = true;
				break;
			}
		}
		if (found1 === true) {
			Alert.alert('Campo nombre vacío',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found2 === true) {
			Alert.alert('Campo apellido paterno vacío',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found3 === true) {
			Alert.alert('Campo apellido materno vacío',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found4 === true) {
			Alert.alert('Curp indefinido o incompleto',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found5 === true) {
			Alert.alert('Campo tipo de solicitud indefinido',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found6 === true) {
			Alert.alert('Campo grado de interes indefinido',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found7 === true) {
			Alert.alert('CCT de la escuela vacío o incompleto',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found8 === true) {
			Alert.alert('Campo ultimo promedio general vacío',
				'Asegurese de llenar todos correctamente este campo que no sea mayor a 10 o menor a 0 para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found9 === true) {
			Alert.alert('Campo genero del aspirante vacío',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else {
			Alert.alert('Enviar solicitud', 'Está a punto de enviar una solicitud de informes\n¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.createTutor()}, {text: 'No'}
			])
		}
	}

	async createTutor() {
		await this._changeWheelState();
		await fetch(this.state.uri + '/api/post/nuevo/tutor', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}, body: JSON.stringify({
				nombre: this.state.nombre,
				apellidos: this.state.apellido_paterno + ' ' + this.state.apellido_materno,
				genero: this.state.elGeneroTut,
				telefono_contacto: this.state.numero_tel,
				email: this.state.email,
				password: '',
				PushNotificationsID: 'asd'
			})
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar la solicitud', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de enviar la solicitud de infores, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{
							text: 'Entendido',
							onPress: () => [this.setState({visible: false}), Actions.pop({refresh: {key: 'Drawer'}})]
						}]);
				} else {
					this.requestMultipart(responseJson.id);
				}
			});
	}

	async saveitem(key, value) {
		try {
			await AsyncStorage.setItem(key, value);
		} catch (error) {
			console.error('AsyncStorage error: ' + error.message);
			console.log('AsyncStorage error: ', error.message);
		}
	}

	async requestMultipart(id) {
		await this.setState({tutorID: id});
		for (let i = 0; i < this.state.losAspirantes.length; ++i) {
			await fetch(this.state.uri + '/api/nueva/solicitud/info', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json'
				}, body: JSON.stringify({
					id_tutor: id,
					nombre_aspirante: this.state.nombreAlumn[i] + ' ' + this.state.apellidPatAlumn[i] + ' ' + this.state.apellidMatAlumn[i],
					curp_aspirante: this.state.CurpAlumn[i],
					genero: this.state.elGenero[i],
					grado_interes: this.state.elGrado[i],
					estado_escuela: '0',
					escuela_procedente: this.state.escuela[i],
					promedio_gral: this.state.elPromedio[i],
					paso_registro: '1',
					fecha_cita: '',
					hora_cita: '',
					tipo_alta: this.state.tipoAlta[i],
					estatus_alta: 'no'
				})
			}).then(res => res.json())
				.then(responseJson => {
					this.setState({elRequest: responseJson});
				});
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al enviar la solicitud', 'Ha ocurrido un error ' + this.state.elRequest.error.status_code +
				' al tratar de enviar la solicitud de infores, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
				[{
					text: 'Entendido',
					onPress: () => [this.setState({visible: false}), Actions.pop({refresh: {key: 'Drawer'}})]
				}]);
		} else {
			await this.correoPassword()
		}
	}


	async correoPassword() {
		await this.saveitem('idTutor', this.state.tutorID.toString());
		await fetch(this.state.uri + '/api/get/usuario/preincripcion/' + this.state.tutorID).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar la solicitud', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de enviar la solicitud de informes, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{
							text: 'Entendido',
							onPress: () => [this.setState({visible: false}), Actions.pop({refresh: {key: 'Drawer'}})]
						}]);
				} else {
					Alert.alert(
						'¡Gracias!',
						'Hemos recibido su solicitud y enviado a su correo una clave de acceso con la cual podrá acceder para continuar el registro de los aspirantes que registró a su nombre',
						[
							{
								text: 'Entendido',
								onPress: () => [this.setState({visible: false}), Actions.replace('aspirantesTutor', {key: Math.random()})]
							}
						]
					)
				}
			});
	}

	//+-++-+-+-+-+-+-+-+-+-+-+-+--+ Datos padre
	async validarPadre() {
		if (this.state.nombre === '') {
			Alert.alert('Campo nombre vacío', 'Falta ingresar nombre')
		} else if (this.state.apellido_paterno === '') {
			Alert.alert('Campo apellido paterno vacío', 'Falta ingresar apellido paterno')
		} else if (this.state.apellido_materno === '') {
			Alert.alert('Campo apellido materno vacío', 'Falta ingresar apellido materno')
		} else if (this.state.elGeneroTut === '') {
			Alert.alert('Campo genero vacío', 'Falta ingresar el genero del tutor')
		} else if (this.state.numero_tel === '') {
			Alert.alert('Campo numero telefonico vacío', 'Falta ingresar número telefonico')
		} else if (this.state.numero_tel.length < 10) {
			Alert.alert('Campo numero telefonico incompleto', 'El numero telefonico debe ser de 10 digitos')
		} else if (this.state.email === '') {
			Alert.alert('Campo email vacío', 'Falta ingresar email')
		} else {
			let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if (reg.test(this.state.email) === false) {
				Alert.alert('Email invalido', 'ingresa un email valido');
				return false;
			}
			else {
				await this.verificateCorreo(this.state.email);
			}
		}
	}

	async verificateCorreo(email) {
		await fetch(this.state.uri + '/api/verificar/correo', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}, body: JSON.stringify({email: email})
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error === 'Unauthorized') {
					Alert.alert('Ya existe este correo', 'Este corre ya esta siendo utilizado',
						[{text: 'Entendido'}]);
				} else {
					this.setState({addAspirant: true});
					Keyboard.dismiss();
				}
			});
	}

	preinscrip() {
		let index = 0;
		const data = [
			{key: index++, label: 'Masculino', activo: '1'},
			{key: index++, label: 'Femenino', activo: '0'}
		];
		return (
			<View>
				{this.state.currentPosition === 0 ? (
					<View style={[styles.container]}>
						<Text style={[styles.main_title, {
							color: this.state.thirdColor, ...Platform.select({
								ios: {
									marginTop: 10
								},
								android: {
									marginTop: 2
								}
							})
						}]}>
							Datos del padre o tutor
						</Text>
						<ScrollView>
							<Fumi
								label={'Nombre(s)'}
								iconClass={Ionicons}
								style={[styles.inputDato, {borderColor: this.state.secondColor}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'md-person'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								onChangeText={text => (this.state.nombre = text)}
								value={this.state.nombre}
								keyboardType='default'
								returnKeyType='next'
								autoCorrect={false}
								onSubmitEditing={() => this.refs.apellidoPat.focus()}
							/>
							<Fumi
								ref='apellidoPat'
								label={'Apellido paterno'}
								iconClass={Ionicons}
								style={[styles.inputDato, {borderColor: this.state.secondColor}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'md-person'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								onChangeText={text => (this.state.apellido_paterno = text)}
								value={this.state.apellido_paterno}
								keyboardType='default'
								returnKeyType='next'
								autoCorrect={false}
								onSubmitEditing={() => this.refs.apellidoMat.focus()}
							/>
							<Fumi
								ref='apellidoMat'
								label={'Apellido materno'}
								iconClass={Ionicons}
								style={[styles.inputDato, {borderColor: this.state.secondColor}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'md-person'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								onChangeText={text => (this.state.apellido_materno = text)}
								value={this.state.apellido_materno}
								keyboardType='default'
								returnKeyType='next'
								autoCorrect={false}
								onSubmitEditing={() => this.refs.numeroTel.focus()}
							/>
							<ModalSelector
								data={data}
								selectStyle={[
									styles.inputPicker,
									{borderColor: this.state.secondColor, marginTop: 3}
								]}
								cancelText='Cancelar'
								optionTextStyle={{color: this.state.thirdColor}}
								initValue='Género'
								onChange={option => this.onChangeGeneroTut(option)}
							/>
							<Fumi
								ref='numeroTel'
								label={'Número telefónico'}
								iconClass={Ionicons}
								style={[styles.inputDato, {borderColor: this.state.secondColor}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'md-phone-portrait'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								maxLength={10}
								keyboardType='phone-pad'
								onChangeText={text => (this.state.numero_tel = text)}
								value={this.state.numero_tel}
								returnKeyType='next'
								autoCapitalize='none'
								autoCorrect={false}
								onSubmitEditing={() => this.refs.emailTut.focus()}
							/>
							<Fumi
								ref='emailTut'
								label={'Email'}
								iconClass={Entypo}
								style={[styles.inputDato, {borderColor: this.state.secondColor}]}
								labelStyle={{color: this.state.mainColor}}
								iconName={'email'}
								inputStyle={{color: this.state.secondColor}}
								iconColor={this.state.mainColor}
								iconSize={20}
								onChangeText={text => (this.state.email = text)}
								value={this.state.email}
								keyboardType='email-address'
								returnKeyType='next'
								autoCapitalize='none'
								autoCorrect={false}
								onSubmitEditing={() => Keyboard.dismiss()}
							/>
							<TouchableOpacity
								style={[styles.bigButton, styles.widthall, {
									flexDirection: 'row',
									backgroundColor: this.state.secondColor,
									borderRadius: 7
								}]}
								onPress={() => this.validarPadre()}
							>
								<Text style={styles.textButton}>Añadir aspirante(s)</Text>
								<View style={{paddingHorizontal: 10}}>
									<Feather name='user-plus' size={20} color='#fff'/>
								</View>
							</TouchableOpacity>
						</ScrollView>
					</View>
				) : null}
			</View>
		);
	}

	//+-+-+-+-+-++-+-+-+-+-Grados
	rndGrados(i) {
		let buttons = [];
		for (let j = 0; j < this.state.grados[i]; ++j) {
			buttons.push(this.rndGrado(i, j));
		}
		return buttons;
	}

	async onListItemPressedGrado(indxGrado, j) {
		this.state.selectedIndexGrados[indxGrado] = j;
		this.state.elGrado[indxGrado] = j + 1;
		await this.setState({aux: 0});
	}

	rndGrado(indx, j) {
		let smallButtonStyles = [
			styles.listButton,
			{
				borderColor: this.state.secondColor,
				height: responsiveHeight(4),
				width: responsiveWidth(11)
			}
		];
		let texto = [styles.textoN, {fontSize: responsiveFontSize(1.5)}];

		if (this.state.selectedIndexGrados[indx] === j) {
			smallButtonStyles.push(styles.listButtonSelected, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB, {fontSize: responsiveFontSize(1.5)});
		}
		return (
			<TouchableHighlight
				key={j + 'gdo'}
				underlayColor={this.state.secondColor}
				style={smallButtonStyles}
				onPress={() => this.onListItemPressedGrado(indx, j)}>
				<View style={styles.listItem}>
					<Text style={texto}>{j + 1}</Text>
				</View>
			</TouchableHighlight>
		);
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ AddAspirantes

	async validacionAlumno() {
		let found1 = false;
		let found2 = false;
		let found3 = false;
		let found4 = false;
		let found5 = false;
		let found6 = false;
		let found7 = false;
		let found8 = false;
		let found9 = false;
		for (let i = 0; i < this.state.losAspirantes.length; i++) {
			let elCurp = this.state.CurpAlumn[i];
			let laEscuela = this.state.escuela[i];
			if (this.state.nombreAlumn[i] === '' || this.state.nombreAlumn[i] === undefined) {
				found1 = true;
				break;
			} else if (this.state.apellidPatAlumn[i] === '' || this.state.apellidPatAlumn[i] === undefined) {
				found2 = true;
				break;
			} else if (this.state.apellidMatAlumn[i] === '' || this.state.apellidMatAlumn[i] === undefined) {
				found3 = true;
				break;
			} else if (elCurp === '' || elCurp === undefined || elCurp.length < 18) {
				found4 = true;
				break;
			} else if (this.state.elGenero[i] === '' || this.state.elGenero[i] === undefined) {
				found9 = true;
				break;
			} else if (this.state.tipoAlta[i] === '' || this.state.tipoAlta[i] === undefined) {
				found5 = true;
				break;
			} else if (this.state.selectedIndexGrados[i] === -1 && this.state.elGrado[i] === undefined) {
				found6 = true;
				break;
			} else if (laEscuela === '' || laEscuela === undefined || laEscuela.length < 10) {
				found7 = true;
				break;
			} else if (this.state.elPromedio[i] === '' || this.state.elPromedio[i] === undefined || this.state.elPromedio[i] > 10) {
				found8 = true;
				break;
			}
		}
		if (found1 === true) {
			Alert.alert('Campo nombre vacío',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found2 === true) {
			Alert.alert('Campo apellido paterno vacío',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found3 === true) {
			Alert.alert('Campo apellido materno vacío',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found4 === true) {
			Alert.alert('Curp indefinido o incompleto',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found5 === true) {
			Alert.alert('Campo tipo de solicitud indefinido',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found6 === true) {
			Alert.alert('Campo grado de interes indefinido',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found7 === true) {
			Alert.alert('CCT de la escuela vacío o incompleto',
				'Asegurese de llenar todos los datos para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found8 === true) {
			Alert.alert('Campo ultimo promedio general vacío',
				'Asegurese de llenar todos correctamente este campo que no sea mayor a 10 o menor a 0 para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else if (found9 === true) {
			Alert.alert('Campo genero del aspirante vacío',
				'Asegurese de llenar todos correctamente este campo que no sea mayor a 10 o menor a 0 para agregar otro aspirante',
				[{text: 'Enterado'}]);
		} else {
			await this.agregarAsp();
		}
	}

	async agregarAsp() {
		this.state.losAspirantes.push({
			nombre_aspitante: this.state.nombreAlumn + ' ' + this.state.apellidPatAlumn + ' ' + this.state.apellidMatAlumn,
			curp_aspirante: this.state.CurpAlumn,
			grado_asp: this.state.elGrado,
			estado_escuela: this.state.elEstado,
			escuela_procedente: this.state.escuela,
			promedio_gral: this.state.elPromedio,
			tipo_alta: this.state.tipoAlta,
			genero: this.state.elGenero
		});
		this.state.aspirantes.push(0);
		this.state.losAspirantes.forEach(() => {
			this.state.grados.push('6');
			this.state.tipoAlta.push('ingreso')
		});
		await this.setState({aux: 0});
	}

	async deleteAspirante(i) {
		this.state.losAspirantes.splice(i, 1);
		this.state.aspirantes.splice(i, 1);
		this.state.nombreAlumn.splice(i, 1);
		this.state.apellidPatAlumn.splice(i, 1);
		this.state.apellidMatAlumn.splice(i, 1);
		this.state.elGrado.splice(i, 1);
		this.state.tipoAlta.splice(i, 1);
		this.state.elGenero.splice(i, 1);
		this.state.elEstado.splice(i, 1);
		this.state.escuela.splice(i, 1);
		this.state.elPromedio.splice(i, 1);
		this.state.CurpAlumn.splice(i, 1);
		this.state.selectedIndexGrados[i] = -1;
		await this.setState({aux: 0});
	}


	async onChangeTipo(option, i) {
		this.state.tipoAlta[i] = option;
		if (this.state.tipoAlta[i] === 'ingreso') {
			this.state.selectedIndexGrados[i] = 0;
			this.state.elGrado[i] = '1';
		} else {
			this.state.selectedIndexGrados[i] = -1;
			this.state.elGrado[i] = undefined;
		}
		await this.setState({aux: 0});
	}

	async onChangeGenero(option, ix) {
		this.state.elGenero[ix] = option.label;
		await this.setState({aux: 0});
	}

	async onChangeGeneroTut(option) {
		await this.setState({elGeneroTut: option.label});
	}

	async cct(text, i) {
		let texto = text;
		this.state.escuela[i] = texto.toUpperCase();
		await this.setState({aux: 0});
	}

	async curp(text, i) {
		let texto = text;
		this.state.CurpAlumn[i] = texto.toUpperCase();
		await this.setState({aux: 0});
	}

	rndAspirante(indx) {
		let tipo_alta = this.state.tipoAlta[indx];
		if (this.state.indexAspirante === indx) {
			let index = 0;
			const data = [
				{key: index++, label: 'Masculino', activo: '1'},
				{key: index++, label: 'Femenino', activo: '0'}
			];
			return (
				<View key={indx + 'addAsp'} style={{marginTop: 20}}>
					<View style={[styles.row, styles.btn2_3, {justifyContent: 'space-between'}]}>
						<Text style={[styles.textW, {color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}]}>
							Capture los datos del aspirante {indx + 1}
						</Text>
						{this.state.indexAspirante !== 0 ? <Entypo
							name='circle-with-minus' size={18} color='red' onPress={() => this.deleteAspirante(indx)}
						/> : null}
					</View>
					<Text style={[styles.main_title, {color: '#000', fontSize: responsiveFontSize(1.6)}]}>
						1) Seleccione una opción
					</Text>
					<View style={[styles.row, {marginTop: 5}]}>
						<TouchableOpacity
							style={[styles.btn2, {
								borderRadius: 6,
								padding: 3,
								backgroundColor: tipo_alta === 'ingreso' ? this.state.mainColor : this.state.fourthColor
							}]}
							onPress={() => this.onChangeTipo('ingreso', indx)}
						>
							<Text style={[styles.textW, {
								textAlign: 'center',
								color: tipo_alta === 'ingreso' ? '#fff' : '#000',
								fontSize: responsiveFontSize(2),
								marginVertical: 3
							}]}>
								Nuevo ingreso
							</Text>
							<Text
								style={{
									textAlign: 'center',
									color: tipo_alta === 'ingreso' ? '#fff' : '#000',
									fontSize: responsiveFontSize(1.3),
									lineHeight: responsiveHeight(1.5),
									marginBottom: 4
								}}
							>
								Por primera vez{'\n'}a primer grado
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[styles.btn2, {
								borderRadius: 6,
								padding: 3,
								backgroundColor: tipo_alta === 'traslado' ? this.state.mainColor : this.state.fourthColor
							}]}
							onPress={() => this.onChangeTipo('traslado', indx)}
						>
							<Text style={[styles.textW, {
								textAlign: 'center',
								color: tipo_alta === 'traslado' ? '#fff' : '#000',
								fontSize: responsiveFontSize(2),
								marginVertical: 3
							}]}>
								Traslado
							</Text>
							<Text
								style={{
									textAlign: 'center',
									color: tipo_alta === 'traslado' ? '#fff' : '#000',
									fontSize: responsiveFontSize(1.3),
									lineHeight: responsiveHeight(1.5),
									marginBottom: 4
								}}
							>
								A cualquier grado{'\n'}despues de iniciar el ciclo
							</Text>
						</TouchableOpacity>
					</View>
					<Text style={[styles.main_title, {color: '#000', fontSize: responsiveFontSize(1.6)}]}>
						2) Complete la información
					</Text>
					<Fumi
						label={'Nombre(s)'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.nombreAlumn[indx] = text)}
						value={this.state.nombreAlumn[indx]}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.apellidoPAsp.focus()}
					/>
					<Fumi
						ref='apellidoPAsp'
						label={'Apellido paterno'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.apellidPatAlumn[indx] = text)}
						value={this.state.apellidPatAlumn[indx]}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.apellidoMAsp.focus()}
					/>
					<Fumi
						ref='apellidoMAsp'
						label={'Apellido materno'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.apellidMatAlumn[indx] = text)}
						value={this.state.apellidMatAlumn[indx]}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.curpAsp.focus()}
					/>
					<Fumi
						ref='curpAsp'
						label={'C.U.R.P del aspirante'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-paper'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						maxLength={18}
						onChangeText={text => this.curp(text, indx)}
						value={this.state.CurpAlumn[indx]}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.cctAsp.focus()}
					/>
					<ModalSelector
						data={data}
						selectStyle={[
							styles.inputPicker,
							{borderColor: this.state.secondColor, marginTop: 3}
						]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor}}
						initValue='Género del aspirante'
						onChange={option => this.onChangeGenero(option, indx)}
					/>
					{this.state.tipoAlta[indx] === 'traslado' ?
						<View style={[styles.widthall, {alignItems: 'center', marginVertical: 8}]}>
							<Text
								style={[styles.subTitulo, styles.widthall, {
									textAlign: 'center',
									color: this.state.mainColor
								}]}>
								Seleccione un grado de interés
							</Text>
							<View style={[styles.buttonsRow, {marginVertical: 5}]}>{this.rndGrados(indx)}</View>
						</View> : null}
					<Fumi
						ref='cctAsp'
						label={'CCT de la escuela de procedencia (Clave)'}
						iconClass={FontAwesome}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'building-o'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						maxLength={10}
						onChangeText={text => this.cct(text, indx)}
						value={this.state.escuela[indx]}
						keyboardType='default'
						returnKeyType='next'
						autoCapitalize='none'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.promedioAsp.focus()}
					/>
					<Fumi
						ref='promedioAsp'
						label={'Último promedio general'}
						iconClass={MaterialCommunityIcons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'school'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={22}
						maxLength={4}
						onChangeText={text => (this.state.elPromedio[indx] = text)}
						value={this.state.elPromedio[indx]}
						keyboardType='numeric'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => Keyboard.dismiss()}
					/>
				</View>
			);
		}
	}

	rndAspirantes() {
		let btnAspirante = [];
		for (let i = 0; i < this.state.losAspirantes.length; ++i) {
			btnAspirante.push(this.rndAspirante(i))
		}
		return btnAspirante;
	}

	rndAddAspir() {
		return (
			<View style={[styles.container]}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Añadir aspirantes
				</Text>
				<View style={[styles.row, styles.widthall, {justifyContent: 'flex-start'}]}>
					{this.rndAspiranteBtns()}
					{this.state.losAspirantes.length === 3 ? null : (<Entypo
						name='circle-with-plus' size={30} color='#43d551' onPress={() => this.validacionAlumno()}
					/>)}
				</View>
				{this.rndAspirantes()}
				{this.state.addAspirant === true ? (
					<TouchableOpacity
						style={[
							styles.bigButton,
							{
								backgroundColor: this.state.secondColor,
								borderColor: this.state.secondColor
							}
						]}
						onPress={() => [Keyboard.dismiss(), this.alertSolicitudEnviar()]}
					>
						<Text style={styles.textButton}>Enviar solicitud de informes</Text>
					</TouchableOpacity>
				) : null}
			</View>
		);
	}

	render() {
		return (
			<KeyboardAvoidingView
				style={[styles.container]}
				behavior={Platform.OS === 'ios' ? 'padding' : null}
				keyboardVerticalOffset={Platform.OS === 'ios' ? 10 : 0}
				enabled
			>
				<Spinner visible={this.state.visible} textContent='Enviando solicitud...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {marginTop: 5}]}>
					Este es el procedimiento a seguir:
				</Text>
				<View
					style={[styles.widthall, {marginTop: 5}]}>
					<StepIndicator
						customStyles={customStyles}
						stepCount={6}
						currentPosition={this.state.currentPosition}
						labels={labels_estForm}
					/>
				</View>
				<ScrollView
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
				>
					{this.state.addAspirant === false ? this.preinscrip() : this.rndAddAspir()}
				</ScrollView>
			</KeyboardAvoidingView>
		);
	}
}
