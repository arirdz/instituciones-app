import React from 'react';
import {
	Alert, AsyncStorage, FlatList, KeyboardAvoidingView, Linking, Platform, Text, TouchableOpacity, View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {CreditCardInput} from 'rn-credit-card-view';
import styles from '../../styles';
import openpay from 'react-native-openpay';
import Spinner from 'react-native-loading-spinner-overlay';

export default class pagoTarjetaP extends React.Component {
	_onChange = formData => {
		let card = formData.values.number;
		let cardJunto = card.split(' ');
		let expire = formData.values.expiry;
		let expiraSeparado = expire.split('/');
		this.setState({
			cardNumber: cardJunto[0] + cardJunto[1] + cardJunto[2] + cardJunto[3],
			holderName: formData.values.name,
			cvv2: formData.values.cvc,
			month: expiraSeparado[0],
			year: expiraSeparado[1]
		});
	};


	constructor(props) {
		super(props);
		this.state = {
			Numero: [],
			data: [],
			values: [],
			datos: [],
			cardData: [],
			source_id: '',
			singleCard: [],
			visible: false
		};
	}


	async componentWillMount() {
		await this.getURL();
	}

	async _changeWheelState(state) {
		await this.setState({
			visible: state
		});
	};

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.getUserdata();
	}


	async createCardTokenGG() {
		Alert.alert('Realizar pago',
			'Está a punto de realizar el pago con tarjeta ¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.pagarAhora()}, {text: 'No'}
			])
	}

	async pagarAhora() {
		//recordar cambiar api key
		await this._changeWheelState(true);
		if (this.state.cardNumber.length === 16) {
			if (this.state.cvv2.length) {
				await openpay.setup('mpb6xcs65mhmahdjv935', 'pk_1c63493c1ce241319bfe713ece85fa26');
				await openpay.createCardToken({
					holder_name: this.state.holderName,
					card_number: this.state.cardNumber,
					cvv2: this.state.cvv2,
					expiration_month: this.state.month,
					expiration_year: this.state.year,
					address: {
						line1: this.state.datos.calle,
						line2: this.state.datos.colonia,
						line3: '',
						state: this.state.datos.estado,
						city: this.state.datos.ciudad,
						postal_code: this.state.datos.cp,
						country_code: 'MX'
					}
				}).then(token => {
					this.setState({cardToken: token});
					openpay.getDeviceSessionId().then(sessionId => {
						this.setState({session_id: sessionId});
						this.pagoCardToken(this.props.elAspirante.tutor);
					});

				}).catch(e => {
					Alert.alert('Tarjeta invalida', e.message, [{
						text: 'Entendido', onPress: () => this._changeWheelState(false)
					}]);
				});

			} else {
				Alert.alert('Tarjeta invalida', 'El número del ccv debe ser de 3 a 4 dígitos', [{
					text: 'Entendido', onPress: () => this._changeWheelState(false)
				}]);
			}
		} else {
			Alert.alert('Tarjeta invalida', 'El número de la tarjeta debe ser de 16  dígitos', [{
				text: 'Entendido', onPress: () => this._changeWheelState(false)
			}]);
		}

	}

	async pagoCardToken(tutor) {
		let formData = new FormData();
		formData.append('parameters', JSON.stringify({
			method: 'card',
			source_id: this.state.cardToken,
			amount: Number(this.props.montoInscrip),
			description: 'Pago de preinscripcion Liceo Ànimas',
			device_session_id: this.state.session_id,
			use_3d_secure: true,
			redirect_url: this.state.uri + '/Pago',
			customer: {
				name: tutor.nombre,
				last_name: tutor.apellidos,
				phone_number: tutor.telefono_contacto,
				email: tutor.email
			}
		}));
		await fetch(this.state.uri + '/api/pago/tarjeta/openpay/preinscrip', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(response => response.json())
			.then(responseJson => {
				if (responseJson.response !== 'error') {
					Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
						text: 'Gracias',
						onPress: () => this.updateAspirante(this.props.elAspirante, responseJson.result.id)
					}]);
					Linking.openURL(responseJson.result.serializableData.payment_method.url);
					this.setState({recibo: responseJson});
				} else {
					if (responseJson.error.code === 3001) {
						Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3002) {
						Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirado', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3003) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3004) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3005) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
							text: 'Entendido'
						}]);

					} else if (responseJson.error.code === 1006) {
						Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
							text: 'Entendido'
						}]);

					} else {
						Linking.openURL(responseJson.result.payment_method.url);
						Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
							text: 'Gracias',
							onPress: () => this.updateAspirante(this.props.elAspirante, responseJson.result.id)
						}]);
					}
				}
			}).catch(error => {
				Alert.alert('Error', 'No se pudo generar el cargo, intente más tarde', [{
					text: 'Gracias', onPress: () => Actions.pop({refresh: {key: 'aspirantesTutor'}})
				}]);
			});
	}

	async getUserdata() {
		let admin = await fetch(this.state.uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let adminId = await admin.json();
		await this.setState({data: adminId});
	}

	async updateAspirante(aspirante, id_charge) {
		if (this.props.tipo === 'preinscrip') {
			let formData = new FormData();
			formData.append('update', JSON.stringify({
				id: aspirante.id,
				paso_registro: '3.1',
				pago_preinscrip: id_charge
			}));
			await fetch(this.state.uri + '/api/cita/aspirantes', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data'
				}, body: formData
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al actualizar aspirante',
							'Ha ocurrido un error ' + responseJson.error.status_code + ' ' +
							'si el error continua pónganse en contacto con soporte (Pago en tarjeta)', [
								{
									text: 'Entendido',
									onPress: () => [this._changeWheelState(false), Actions.replace('aspirantesTutor', {key: Math.random()})]
								}
							]);
					} else {
						if (this.state.token === null) {
							this._changeWheelState(false);
							Actions.replace('aspirantesTutor', {key: Math.random()})
						} else {
							this._changeWheelState(false);
							Actions.replace('aspirantePendiente', {key: Math.random()})
						}
					}
				});
		} else if (this.props.tipo === 'inscrip') {
			let formData = new FormData();
			formData.append('update', JSON.stringify({
				id: aspirante.id,
				paso_registro: '5.1',
				pago_inscrip: id_charge
			}));
			await fetch(this.state.uri + '/api/cita/aspirantes', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data'
				}, body: formData
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al actualizar aspirante',
							'Ha ocurrido un error ' + responseJson.error.status_code + ' ' +
							'si el error continua pónganse en contacto con soporte (Pago en tarjeta)', [
								{
									text: 'Entendido',
									onPress: () => [this._changeWheelState(false), Actions.replace('aspirantesTutor', {key: Math.random()})]
								}
							]);
					} else {
						if (this.state.token === null) {
							this._changeWheelState(false);
							Actions.replace('aspirantesTutor', {key: Math.random()})
						} else {
							this._changeWheelState(false);
							Actions.replace('aspirantePendiente', {key: Math.random()})
						}
					}
				});
		}
	}


	registrarTarjetas() {
		return (<KeyboardAvoidingView
			behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
			keyboardVerticalOffset={Platform.OS === 'ios' ? 15 : 20}>
			<View
				style={{
					width: responsiveWidth(95), height: responsiveHeight(63), paddingTop: Platform.OS === 'ios' ? 17 : 8
				}}>
				<CreditCardInput
					autoFocus
					requiresName
					requiresCVC
					allowScroll={true}
					labels={{
						number: 'Número de Tarjeta', expiry: 'Expira', cvc: 'CVC/CCV', name: 'Nombre'
					}}
					placeholders={{
						number: '8899 6677 4433 1122', expiry: 'MM/YY', cvc: 'CVC', name: 'Nombre Completo'
					}}
					validColor={'green'}
					invalidColor={'red'}
					placeholderColor={'darkgray'}
					onChange={this._onChange}
				/>
			</View>
		</KeyboardAvoidingView>);
	}

	render() {
		return (
			<View style={styles.container}
			>
				<Spinner visible={this.state.visible} textContent='Verificando...'/>
				{this.registrarTarjetas()}
				<TouchableOpacity
					style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
					onPress={() => this.createCardTokenGG(this.props.elAspirante)}
				>
					<Text style={styles.textButton}>Pagar ahora</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
