import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, SectionList, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class diaInmersion extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			losDias: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getDiasInmersion();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getDiasInmersion() {
		await fetch(this.state.uri + '/api/get/dias/inmersion', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(response => response.json()).then(responseJson => {
			if (responseJson.error === null) {
				Alert.alert('Error al carrgar los dias, Sí el error continua pónganse en contacto con soporte (Dia de inmersión)', [
					{text: 'Enterado', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})}
				]);
			} else {
				this.setState({losDias: responseJson[0]});
			}
		});
		await this.setState({aux: 0});
	}

	render() {
		let diaInmers = this.state.losDias;
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					La fecha del día de la inmersión será:
				</Text>
				<View
					style={[
						styles.cardHoras,
						styles.widthall,
						{backgroundColor: this.state.fourthColor, paddingVertical: 15}
					]}
				>

					<Text
						style={{
							textAlign: 'center',
							marginTop: 2,
							fontSize: responsiveFontSize(2.5),
							fontWeight: '700'
						}}
					>
						{moment(diaInmers.fecha).format('LL')}
					</Text>
				</View>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Indicaciones especiales:
				</Text>
				<View
					style={[
						styles.cardHoras,
						styles.widthall,
						{backgroundColor: this.state.fourthColor, paddingVertical: 15}
					]}
				>
					<SectionList
						style={{height:responsiveHeight(34)}}
						overScrollMode='always'
						showsVerticalScrollIndicator={false}
						renderSectionHeader={({section: {title}}) => (
							<Text style={{
								fontWeight: 'bold',
								backgroundColor: this.state.fourthColor,
								marginTop: 5
							}}>{title}</Text>
						)}
						renderItem={({item, index, section}) =>
							<Text key={index}
								  style={{backgroundColor: this.state.fourthColor, marginBottom: 5}}>{item}</Text>}
						sections={[
							{
								title: 'Horario',
								data: ['El alumno deberá asistir en un horario de 7:50 am a 2:30 pm el día designado en la parte superior. La escuela le informará el tipo de vestimenta que deberá llevar.']
							},
							{
								title: 'Materiales',
								data: ['El alumno deberá traer una libreta de cualquier tipo y lapicera con materiales personales (lápiz, goma, colores, tijeras, etc)']
							},
							{
								title: 'Lunch',
								data: ['El alumno podrá traer un lunch o comprar en la tiendita de la escuela. El horario de receso es de 10:30 a 11:00 am.']
							},
							{
								title: 'Seguimiento',
								data: ['En los días posteriores al día de inmersión nos pondremos en contacto con usted para dar seguimiento a su proceso de inscripción.']
							}
						]}
						keyExtractor={(item, index) => item + index}
					/>
				</View>
				<TouchableOpacity
					style={[
						styles.bigButton,
						{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
					]}
					onPress={() => Actions.pop({refresh: {key: 'Drawer'}})}
				>
					<Text style={[styles.textButton]}>Enterado, ¡Gracias!</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
