import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize} from 'react-native-responsive-dimensions';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class citaPreinscrip extends React.Component {
	_changeWheelState = (state) => {
		this.setState({visible: state});
	};

	constructor(props) {
		super(props);
		this.state = {
			elAspirante: [],
			horas: [],
			visible: false,
			selectedIndexHora: -1,
			laHora: '',
			laHora1: '',
			fecha: '',
			isDateTimePickerVisible: false
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.setState({elAspirante: this.props.elAspirante});
		await this.getHorasTodas();
		await this.setState({aux: 0});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let schoolId = await AsyncStorage.getItem('schoolId');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			schoolId: schoolId
		});
	}

//+-+-+--+-+-+--+-++-+Post citas
	async alertCita() {
		if (this.state.fecha === '') {
			Alert.alert('Campo fecha vacío', 'Llene todos los campos para poder continuar', [
				{text: 'Entendido'}
			]);
		} else if (this.state.laHora1 === '') {
			Alert.alert('Campo hora vacío', 'Llene todos los campos para poder continuar', [
				{text: 'Entendido'}
			])
		} else {
			Alert.alert('Enviar cita', 'Está a punto de agendar una cita\n¿Seguro que desea continuar?', [
				{
					text: 'Sí',
					onPress: () => this.createCita(this.state.fecha, this.state.laHora1, this.props.elAspirante)
				}, {text: 'No'}
			])
		}
	}

	async createCita(fecha, hora, aspirante) {
		await this._changeWheelState(true);
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			id_tutor: aspirante.tutor.id,
			id_aspirante: aspirante.id,
			fecha_cita: fecha,
			hora_cita: hora,
			aprobada: 0
		}));
		await fetch(this.state.uri + '/api/create/cita/asp', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita aspirante)', [{
							text: 'Entendido',
							onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					if (aspirante.estatus_alta !== 'rechazado') {
						this.requestMultiPart(aspirante, '2')
					} else {
						this.requestMultiPart2(fecha, hora, aspirante, '7.1')
					}
				}
			});
	}

	async requestMultiPart(aspirante, paso) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: aspirante.id,
			paso_registro: paso
		}));
		await fetch(this.state.uri + '/api/cita/aspirantes', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita aspirante)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					this.notificarAdmin(aspirante);
				}
			});
	}

	async requestMultiPart2(fecha, hora, aspirante, paso) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: aspirante.id,
			paso_registro: paso,
			fecha_cita: fecha,
			hora_cita: hora
		}));
		await fetch(this.state.uri + '/api/cita/aspirantes', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita aspirante)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					this.notificarAdmin(aspirante);
				}
			});
	}

	async notificarAdmin(aspirante) {
		let mensaje = '';
		if (aspirante.estatus_alta !== 'rechazado') {
			mensaje = 'El Sr(a) ' + aspirante.tutor.nombre + ' ha agendado una cita para hablar sobre la inscripción del aspirante ' + aspirante.nombre_aspirante + '';
		} else {
			mensaje = 'El Sr(a) ' + aspirante.tutor.nombre + ' ha agendado una cita para hablar sobre el motivo de rechazo del aspirante ' + aspirante.nombre_aspirante + '';
		}
		await fetch(this.state.uri + '/api/notificar/admins/' + mensaje).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita aspirante)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					Alert.alert('Cita enviada', 'Su cita ha sido agendada correctamente, en breve nos comunicaremos contigo.', [{
						text: 'Enterado',
						onPress: () => [this._changeWheelState(false), Actions.pop({refresh: {key: Math.random()}})]
					}]);
				}
			});
	}

//+-+-+-+-+-++-+-+-get horario escolar
	async getHorasTodas() {
		await fetch(this.state.uri + '/api/get/horarios', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en horas de atencion',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Agendar cita preinscripcion)');
				} else {
					this.setState({horas: responseJson});
				}
			});
	}

	async onListItemPressedHora(index, hora, hora1) {
		await this.setState({
			selectedIndexHora: index,
			laHora: hora,
			laHora1: moment(hora1, 'HH:mm:ss').format('HH:mm')
		});
	}

	renderHoras() {
		let lasHoras = [];
		this.state.horas.forEach((it, ix) => {
			let a = this.state.horas.length - 1;
			let tabRow = [styles.rowTabla, styles.modalWidth];
			if (ix !== a) {
				tabRow.push({borderColor: this.state.secondColor});
			}
			let smallButtonStyles = [styles.rowTabla, {borderColor: this.state.secondColor}];
			let texto = [styles.textoN];
			if (this.state.selectedIndexHora === ix) {
				smallButtonStyles.push(styles.rowTabla, {
					backgroundColor: this.state.secondColor
				});
				texto.push(styles.textoB);
			}
			lasHoras.push(<TouchableHighlight
				key={ix + 'hrs'}
				underlayColor={this.state.secondColor}
				style={[tabRow, smallButtonStyles]}
				onPress={() => this.onListItemPressedHora(ix, it.horas, it.inicio)}
			>
				<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
					<Text style={[texto]}>{it.horas}</Text>
				</View>
			</TouchableHighlight>);
		});
		return lasHoras;
	}

//+-+-+-+-+-+-+--+-+
	async _handleDatePicked(date) {
		let fecha1 = moment(date).format('YYYY-MM-DD');
		this.state.fecha = fecha1;
		this.setState({aux: 0});
		this._hideDateTimePicker();
	};

	//++++++++++timepicker
	async _hideDateTimePicker(i) {
		this.state.isDateTimePickerVisible = false;
		await this.setState({aux: 0});
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>Seleccione una fecha</Text>
				<TouchableOpacity
					style={[styles.inputPicker, {borderColor: this.state.secondColor}]}
					onPress={() => this.setState({isDateTimePickerVisible: true})}
				>
					{this.state.fecha === '' ? (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							Seleccione fecha
						</Text>
					) : (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							{moment(this.state.fecha, 'YYYY-MM-DD').format('DD/MM/YYYY')}
						</Text>
					)}
				</TouchableOpacity>
				<DateTimePicker
					locale={'es'}
					isVisible={this.state.isDateTimePickerVisible}
					onConfirm={(date) => this._handleDatePicked(date)}
					onCancel={() => this._hideDateTimePicker()}
					titleIOS={'Seleccione una fecha'}
					confirmTextIOS={'Seleccionar'}
					cancelTextIOS={'Cancelar'}
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>Seleccione una hora</Text>
				<View
					style={[
						styles.tabla,
						{borderColor: this.state.secondColor, marginTop: 5}
					]}
				>
					{this.renderHoras()}
				</View>
				<TouchableOpacity
					style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
					onPress={() => this.alertCita()}
				>
					<Text style={styles.textButton}>Agendar cita</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
