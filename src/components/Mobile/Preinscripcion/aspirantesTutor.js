import React from 'react';
import {
	Alert, AsyncStorage, BackHandler, KeyBoard, KeyboardAvoidingView, Linking, Platform, RefreshControl, ScrollView,
	StatusBar, Text, TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {Actions} from 'react-native-router-flux';
import Modal from 'react-native-modal';
import StepIndicator from 'rn-step-indicator';
import Spinner from 'react-native-loading-spinner-overlay';
import OneSignal from 'react-native-onesignal';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const labels_estForm = [
	'Solicitar\ninfo',
	'Agendar\ncita',
	'Subir\ndocumentos',
	'Acreditar\npreinscripción',
	'Día de\ninmersión',
	'Acreditar\ninscripción'
];

const customStyles = {
	stepIndicatorSize: 20,
	currentStepIndicatorSize: 25,
	separatorStrokeWidth: 1,
	currentStepStrokeWidth: 2,
	stepStrokeCurrentColor: '#777777',
	separatorFinishedColor: '#16a900',
	separatorUnFinishedColor: '#aaaaaa',
	stepIndicatorFinishedColor: '#16a900',
	stepIndicatorUnFinishedColor: '#aaaaaa',
	stepIndicatorCurrentColor: '#ffffff',
	stepIndicatorLabelFontSize: responsiveFontSize(1.3),
	currentStepIndicatorLabelFontSize: responsiveFontSize(1.3),
	stepIndicatorLabelCurrentColor: '#000000',
	stepIndicatorLabelFinishedColor: '#ffffff',
	stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
	labelColor: '#666',
	labelSize: responsiveFontSize(1),
	currentStepLabelColor: '#777777'
};

export default class aspirantesTutor extends React.Component {
	_onRefresh = () => {
		this.setState({refreshing: true});
		fetch(this.state.uri + '/api/get/aspirante/' + this.state.idTutor).then(res => res.json())
			.then(responseJson => {
				this.setState({refreshing: false});
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{text: 'Entendido'}]);
				} else {
					this.setState({aspirantes: responseJson});
					if (this.state.aspirantes.length !== 0) {
						this.state.aspirantes.forEach(() => {
							this.state.currentPosition.push(0);
							this.state.modalAspirante.push(false);
						});
					}
				}
			});
	};

	constructor(props) {
		super(props);
		this.state = {
			aspirantes: [],
			id_tutor: '',
			stepsLabel: [],
			visible: false,
			modalAspirante: [],
			currentStape: [],
			currentPosition: [],
			elComentario: '',
			idAlumno: '',
			refreshing: false,
			pago: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getAspirantes(this.state.idTutor);
		OneSignal.init('0361dfc7-e393-468e-879d-7dfde0531c2a');
		OneSignal.addEventListener('received', this.onReceived);
		OneSignal.addEventListener('opened', this.onOpened);
		OneSignal.addEventListener('ids', this.onIds);
		Linking.addEventListener('url', this.handleOpenURL);
		OneSignal.inFocusDisplaying(2);
		OneSignal.configure();
	}

	componentWillUnmount() {
		Linking.removeEventListener('url', this.handleOpenURL);
		OneSignal.removeEventListener('received', this.onReceived);
		OneSignal.removeEventListener('opened', this.onOpened);
		OneSignal.removeEventListener('ids', this.onIds);
		BackHandler.removeEventListener('hardwareBackPress');
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let tutor = await AsyncStorage.getItem('tutor');
		let idTutor = await AsyncStorage.getItem('idTutor');
		await this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			tutor: tutor,
			idTutor: idTutor
		});
	}

	async spinner(state) {
		await this.setState({visible: state});
	}

	async onIds(device) {
		let uri = await AsyncStorage.getItem('uri');
		let idTutor = await AsyncStorage.getItem('idTutor');
		let formData = new FormData();
		formData.append(
			'PNToken',
			JSON.stringify({
				id: idTutor,
				PushNotificationsID: device.userId
			})
		);
		await fetch(uri + '/api/aspirante/notification', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
			});
	}


//-+-+-+-+-++---+-+-+-+-+get aspirantes
	async getAspirantes(id) {
		await fetch(this.state.uri + '/api/get/aspirante/' + id).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{text: 'Entendido'}]);
				} else {
					this.setState({aspirantes: responseJson});
					if (this.state.aspirantes.length !== 0) {
						this.state.aspirantes.forEach(() => {
							this.state.currentPosition.push(0);
							this.state.modalAspirante.push(false);
						});
					}
				}
			});
	}

	//+-+-+-+-+-+-+-+

	//-+-+-+-+-++-++enviar indicacion
	async enviarIndicacion(id, indicacion, i) {
		if (this.state.elComentario === '') {
			Alert.alert(
				'Campo indicaciones vacío', 'Escriba las indicaciones para poder continuar de lo contrario presione cancelar',
				[
					{text: 'Entendido'}
				]
			)
		} else {
			await fetch(this.state.uri + '/api/indicacion/especial/' + id + '/' + indicacion).then(res => res)
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al enviar la solicitud', 'Ha ocurrido un error ' + responseJson.error.status_code +
							' al tratar de enviar la solicitud de informes, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
							[{
								text: 'Entendido',
								onPress: () => [this.closeModal(i)]
							}]);
					} else {
						Alert.alert(
							'¡Gracias!',
							'Hemos recibido sus indicaciones',
							[
								{text: 'Entendido', onPress: () => [this.closeModal(indicacion, i)]}
							]
						)
					}
				});
		}

	}

	async openModal(aspirante, i) {
		this.state.modalAspirante[i] = true;
		this.state.elComentario = aspirante.indicacion_especial;
		this.state.idAlumno = aspirante.id;
		await this.setState({aux: 0});
	}

	async closeModal(indicacion, i) {
		this.state.aspirantes[i].indicacion_especial = indicacion;
		this.state.modalAspirante[i] = false;
		await this.setState({aux: 0});
	}

	async pagoAgain(aspirante) {
		if (aspirante.paso_registro === '3.1') {
			Actions.pagoPreinscrip({elAspirante: aspirante})
		} else if (aspirante.paso_registro === '5.1') {
			Actions.pagoInscrip({elAspirante: aspirante})
		}
		await this.setState({aux: 0});
	}

	async revisarPago(aspirante, tipo_pago, paso) {
		await this.spinner(true);
		await fetch(this.state.uri + '/api/get/estatus/pago/' + aspirante.id + '/' + tipo_pago, {
			method: 'GET', headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{text: 'Entendido'}]);
				} else {
					if (responseJson.result.status === 'in_progress') {
						Alert.alert('Su pago está en proceso',
							'Su pago está siendo procesado,\neste será reflejado en un lapso de 24 a 48 horas despues pagado\n' +
							'¿Desea crear el cargo de nuevo?', [
								{
									text: 'Sí',
									onPress: () => [this.spinner(false), this.pagoAgain(aspirante)]
								}, {text: 'No', onPress: () => this.spinner(false)}
							]);
					} else if (responseJson.result.status === 'completed') {
						if (paso === '6') {
							Alert.alert('Su pago ha sido acreditado',
								'Su pago ha sido acreditado. Ahora podrá continuar con el proceso de inscripción', [
									{
										text: 'Entendido',
										onPress: () => [this.spinner(false), this.updateAspirante(aspirante, paso, 'alta')]
									}
								]);
						} else {
							Alert.alert('Su pago ha sido acreditado',
								'Su pago ha sido acreditado. Ahora podrá continuar con el proceso de inscripción', [
									{
										text: 'Entendido',
										onPress: () => [this.spinner(false), this.updateAspirante(aspirante, paso, 'en proceso')]
									}
								]);
						}
					}
				}
			});
	}

	async updateAspirante(aspirante, paso, estatus) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: aspirante.id,
			paso_registro: paso,
			estatus_alta: estatus
		}));
		await fetch(this.state.uri + '/api/cita/aspirantes', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Pago en efectivo)', [
							{text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})}
						]);
				} else {
					if (paso === '6') {
						Alert.alert('¡FELICIDADES!',
							'Ahora eres parte de la comunidad escolar, ' +
							'se enviará a tu correo una contraseña con la cúal podrá ingresar como usuario\n\n' +
							'Nota: Si tiene aspirantes por terminar de registrar podrá continuar con el proceso desde el menú en "Inscripciones"', [
								{
									text: 'Enterado ¡Gracias!',
									onPress: () => this.updateTutor(aspirante, estatus)
								},{text:'Esperar'}
							])
					} else {
						this.getAspirantes(aspirante.id_tutor)
					}
				}
			});
	}

	async updateTutor(aspirante, estatus) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: aspirante.id_tutor,
			estatus: estatus
		}));
		await fetch(this.state.uri + '/api/post/update/tutor', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Pago en efectivo)', [
							{text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})}
						]);
				} else {
					this.createPadre(aspirante);
				}
			});
	}

	async createPadre(aspirante) {
		await this.spinner(true);
		await fetch(this.state.uri + '/api/alta/tutor/aspirante', {
			method: 'POST',
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: aspirante.tutor.nombre + ' ' + aspirante.tutor.apellidos,
				email: aspirante.tutor.email,
				password: 'asdasd',
				role: 'Padre',
				activo: '0',
				estatus: '',
				puesto: 'Padre',
				PushNotificationsID: '',
				escuela_id: '0',
				grado: '0',
				grupo: '0',
				padre_id: '0',
				id_registro: aspirante.id_tutor,
				genero: 'Masculino'
			})
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'Ha ocurrido un error '
						+
						responseJson.error.status_code
						+
						' si el error continua pónganse en contacto con soporte'
						+
						'(Alta de aspirante)',
						[{
							text: 'Entendido',
							onPress: () => [this.spinner(false)/*, Actions.pop({refresh: {key: 'Drawer'}})*/]
						}]);
				} else {
					this.sendCorreo(aspirante.tutor);
					this.createAlumno(aspirante, responseJson.id);
				}
			});
	}

	async createAlumno(aspirante, id) {
		await fetch(this.state.uri + '/api/alta/tutor/aspirante', {
			method: 'POST',
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: aspirante.nombre_aspirante,
				email: 'alumno' + id + Math.floor((Math.random() * 100) + 1) + '@ce.pro',
				password: 'asdasd',
				role: 'Alumno',
				activo: '0',
				estatus: aspirante.tipo_alta,
				puesto: 'Alumno',
				PushNotificationsID: '',
				escuela_id: '0',
				grado: aspirante.grado_interes,
				grupo: '0',
				padre_id: id,
				id_registro: aspirante.id,
				genero: aspirante.genero
			})
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'Ha ocurrido un error '
						+
						responseJson.error.status_code
						+
						' si el error continua pónganse en contacto con soporte'
						+
						'(Alta de aspirante)',
						[{
							text: 'Entendido',
							onPress: () => [this.spinner(false), Actions.pop({refresh: {key: 'Drawer'}})]
						}]);
				} else {
					this.spinner(false);
					Actions.Authentication();
				}
			});
	}

	async sendCorreo(tutor) {
		await fetch(this.state.uri + '/api/get/enviarCorreo/nuevoMaestro/' + tutor.email, {
			method: 'get', headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			}
		}).then(res => res.json())
			.then(responseJson => console.log(responseJson));

	}

	async pasoSiguiente(aspirante) {
		if (aspirante.paso_registro === '1') {
			Actions.citaPreinscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '2') {
			Actions.subirDoctos({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '2.1') {
			Alert.alert('Documentos en revisión',
				'Los documentos están siendo revisados, se le notificará cuándo pueda continuar con el proceso', [
					{text: 'Enterado ¡Gracias!'}
				]);
		} else if (aspirante.paso_registro === '3') {
			Actions.pagoPreinscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '3.1') {
			this.revisarPago(aspirante, 'preinscripcion', '4');
		} else if (aspirante.paso_registro === '4') {
			Actions.diaInmersion({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '5') {
			Actions.pagoInscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '5.1') {
			this.revisarPago(aspirante, 'inscripcion', '6');
		} else if (aspirante.paso_registro === '6') {
			this.updateAspirante(aspirante, '6', 'alta');
		} else if (aspirante.paso_registro === '7') {
			Actions.citaPreinscrip({elAspirante: aspirante});
		}
	}


	rndAspirantes() {
		let aspirante = [];
		this.state.aspirantes.forEach((it, ix) => {
			let txtBtn = 'Ir a siguiente paso';
			if (it.paso_registro === '1') {
				txtBtn = 'Solicitar cita';
			} else if (it.paso_registro === '2') {
				txtBtn = 'Subir documentos';
			} else if (it.paso_registro === '2.1') {
				txtBtn = 'Documentos en revisión';
			} else if (it.paso_registro === '3') {
				txtBtn = 'Pago de preinscripción';
			} else if (it.paso_registro === '3.1') {
				txtBtn = 'Estatus de pago';
			} else if (it.paso_registro === '4') {
				txtBtn = 'Ir a día de inmersión'
			} else if (it.paso_registro === '5') {
				txtBtn = 'Pago de inscripción';
			} else if (it.paso_registro === '5.1') {
				txtBtn = 'Estatus de pago';
			}
			if (it.paso_registro === '6') {
				txtBtn = 'Dar de alta';
			} else if (it.paso_registro === '7') {
				txtBtn = 'Solicitar cita';
			}
			aspirante.push(
				<View
					key={ix + 'aspCard'}
					style={[
						styles.cardHoras,
						styles.widthall,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 5,
							padding: 10,
							alignItems: 'center'
						}
					]}
				>
					<Modal
						isVisible={this.state.modalAspirante[ix]}
						backdropOpacity={0.8}
						animationIn={'bounceIn'}
						animationOut={'bounceOut'}
						animationInTiming={1000}
						animationOutTiming={1000}
					>
						<KeyboardAvoidingView
							style={[styles.container, {borderRadius: 6, flex: 0}]}
							keyboardVerticalOffset={Platform.OS === 'ios' ? 150 : 10} behavior='padding'
						>
							<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
								Escriba la información:
							</Text>
							<TextInput
								keyboardType='default'
								returnKeyType='next'
								multiline={true}
								placeholder='Si existe cualquier información relevante sobre el estado de salud de su hijo o cualquier diagnóstico médico o psicológico relevante, favor de escribirlo aquí.'
								underlineColorAndroid='transparent'
								onChangeText={text => (this.state.elComentario = text)}
								defaultValue={this.state.elComentario}
								style={[
									styles.inputContenido,
									styles.modalWidth,
									{borderColor: this.state.secondColor, padding: 5, height: responsiveHeight(20)}
								]}
							/>
							<View style={[styles.modalWidth, styles.row]}>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: '#fff', borderColor: this.state.secondColor}
									]}
									onPress={() => [(this.state.modalAspirante[ix] = false), this.setState({aux: 0})]}
								>
									<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{
											backgroundColor: this.state.secondColor,
											borderColor: this.state.secondColor
										}
									]}
									onPress={() => this.enviarIndicacion(this.state.idAlumno, this.state.elComentario, ix)}
								>
									<Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
								</TouchableOpacity>
							</View>
						</KeyboardAvoidingView>
					</Modal>
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'flex-start',
						paddingHorizontal: 10
					}]}>
						<Text>Nombre del aspirante: </Text>
						<Text style={[styles.textW]}> {it.nombre_aspirante}</Text>
					</View>
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'flex-start',
						paddingHorizontal: 10
					}]}>
						<Text>Escuela procedente: </Text>
						<Text style={[styles.textW]}> {it.escuela_procedente}</Text>
					</View>
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'flex-start',
						paddingHorizontal: 10
					}]}>
						<Text>Ultimo promedio gral: </Text>
						<Text style={[styles.textW]}> {it.promedio_gral}</Text>
					</View>
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'flex-start',
						paddingHorizontal: 10
					}]}>
						<Text>Grado al que aplica: </Text>
						<Text style={[styles.textW]}> {it.grado_interes}</Text>
					</View>
					<View style={[styles.btn1, {marginTop: 20, marginBottom: 10}]}>
						<StepIndicator
							customStyles={customStyles}
							stepCount={6}
							currentPosition={Number(this.state.aspirantes[ix].paso_registro)}
							labels={labels_estForm}
						/>
					</View>
					<View style={[styles.modalWidth, styles.row]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.openModal(this.state.aspirantes[ix], ix)}
						>
							<Text style={[styles.textW, {
								color: this.state.secondColor,
								fontSize: responsiveFontSize(1.5)
							}]}>
								Indicaciones especiales
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{
									backgroundColor: this.state.secondColor,
									borderColor: this.state.secondColor
								}
							]}
							onPress={() => this.pasoSiguiente(this.state.aspirantes[ix], ix)}
						>
							<Text style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
								{txtBtn}
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			)
		});
		return aspirante;
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando, por favor espere...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Datos de los aspirantes
				</Text>
				<ScrollView
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{...ifIphoneX({marginBottom: 35}, {marginBottom: 20})}}
				>
					{this.rndAspirantes()}
				</ScrollView>
			</View>
		);
	}
}
