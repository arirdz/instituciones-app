import React from 'react';
import {
	Alert, AsyncStorage, CameraRoll, Image, Modal, PermissionsAndroid, Platform, ScrollView, StatusBar, Text, TextInput,
	TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class subirDoctos extends React.Component {
	_handleButtonPress = () => {
		CameraRoll.getPhotos({
			first: 100,
			assetType: 'All'
		}).then(r => {
			this.setState({photos: r.edges});
			this.setState({aux: 0});
		}).catch((err) => {
			//Error Loading Images
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			elAspirante: this.props.elAspirante,
			esperar1: false,
			documentCard: 0,
			aux: 0,
			visible: false,
			modalVisible1: false,
			photos: [],
			laImagen: '',
			elActa: '',
			laCurp: '',
			laBoleta: '',
			fotoAspirante: '',
			indexSelectedImage: -1,
			pictures: [],
			idTutor: '',
			idAspirante: '',
			loadImage: true,
			error: false,
			validarActa: '',
			validarCurp: '',
			validarBoleta: '',
			validarFotos: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getDocumentosAspirante(this.props.elAspirante);
	}

	async componentDidMount() {
		if (Platform.OS !== 'ios') {
			await this.requestPhotosPermission();
		} else {
			await this._handleButtonPress();
		}
	}

	async requestPhotosPermission() {
		try {
			const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				await this._handleButtonPress()
			} else {
				console.log('Photos permission denied')
			}
		} catch (err) {
			console.warn(err)
		}
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let schoolId = await AsyncStorage.getItem('schoolId');
		await this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			schoolId:schoolId
		});
	}

	async uploadActa(imagen) {
		await this.setState({esperar1: true, modalVisible1: false, loadImage: true});
		const fd = new FormData();
		fd.append('id_tutor', this.props.elAspirante.id_tutor);
		fd.append('id_aspirante', this.props.elAspirante.id);
		fd.append('image',
			{
				uri: imagen,
				type: 'image/jpeg',
				name: 'photo'
			});

		fetch(this.state.uri + '/api/post/subir/acta', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: fd
		}).then(res => {
			if (res.error !== undefined) {
				Alert.alert('Error al subir documento', 'Ha ocurrido un error ' + res.error.status_code +
					' al tratar de subir el documento, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido',
						onPress: () => this.setState({laImagen: '', indexSelectedImage: -1})
					}]);
			} else {
				this.getDocumentosAspirante(this.props.elAspirante);
			}
		});
		await this.setState({aux: 0});
	}

	async uploadCurp(imagen) {
		await this.setState({laCurp: '', esperar1: true, modalVisible1: false});
		const fd = new FormData();
		fd.append('id_tutor', this.props.elAspirante.id_tutor);
		fd.append('id_aspirante', this.props.elAspirante.id);
		fd.append('image',
			{
				uri: imagen,
				type: 'image/jpeg',
				name: 'photo'
			});

		fetch(this.state.uri + '/api/post/subir/curp', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: fd
		}).then(res => {
			if (res.error !== undefined) {
				Alert.alert('Error al subir documento', 'Ha ocurrido un error ' + res.error.status_code +
					' al tratar de subir el documento, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido',
						onPress: () => this.setState({laImagen: '', indexSelectedImage: -1})
					}]);
			} else {
				this.getDocumentosAspirante(this.props.elAspirante);
			}
		});
		await this.setState({aux: 0});
	}

	async uploadBoleta(imagen) {
		await this.setState({laBoleta: '', esperar1: true, modalVisible1: false});
		const fd = new FormData();
		fd.append('id_tutor', this.props.elAspirante.id_tutor);
		fd.append('id_aspirante', this.props.elAspirante.id);
		fd.append('image',
			{
				uri: imagen,
				type: 'image/jpeg',
				name: 'photo'
			});

		fetch(this.state.uri + '/api/post/subir/boleta', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: fd
		}).then(res => {
			if (res.error !== undefined) {
				Alert.alert('Error al subir documento', 'Ha ocurrido un error ' + res.error.status_code +
					' al tratar de subir el documento, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido',
						onPress: () => this.setState({laImagen: '', indexSelectedImage: -1})
					}]);
			} else {
				this.getDocumentosAspirante(this.props.elAspirante);
			}
		});
		await this.setState({aux: 0});
	}

	async uploadFoto(imagen) {
		await this.setState({fotoAspirante: '', esperar1: true, modalVisible1: false});
		const fd = new FormData();
		fd.append('id_tutor', this.props.elAspirante.id_tutor);
		fd.append('id_aspirante', this.props.elAspirante.id);
		fd.append('image', {uri: imagen, type: 'image/jpeg', name: 'photo'});
		fetch(this.state.uri + '/api/post/subir/foto', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: fd
		}).then(res => {
			if (res.error !== undefined) {
				Alert.alert('Error al subir documento', 'Ha ocurrido un error ' + res.error.status_code +
					' al tratar de subir el documento, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido',
						onPress: () => this.setState({laImagen: '', indexSelectedImage: -1})
					}]);
			} else {
				this.getDocumentosAspirante(this.props.elAspirante);
			}
		});
		await this.setState({aux: 0});
	}

	async onListSelectedImage(it, ix) {
		await this.setState({laImagen: it, indexSelectedImage: ix});
		await this.setState({aux: 0});
	}

	galeriaPicker() {
		let img = [];
		for (let i = 0; i < this.state.photos.length; i = i + 2) {
			let select = null;
			let selected = null;
			let select2 = null;
			let selected2 = null;
			if (this.state.indexSelectedImage === i) {
				select = [{backgroundColor: '#113fff'}];
				selected = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'cover',
					opacity: .6
				}];
			} else {
				select = [{padding: 0, backgroundColor: '#fff'}];
				selected = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'cover'
				}];
			}
			if (this.state.indexSelectedImage === i + 1) {
				select2 = [{backgroundColor: '#113fff'}];
				selected2 = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'cover',
					opacity: .6
				}];
			} else {
				select2 = [{padding: 0, backgroundColor: '#fff'}];
				selected2 = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'cover'
				}];
			}
			img.push(
				<View
					key={i + 'photopicker'} style={styles.row}>
					<TouchableHighlight
						style={select}
						onPress={() => this.onListSelectedImage(this.state.photos[i].node.image.uri, i)}>
						<Image
							style={selected}
							defaultSource={require('../../../images/giphy.gif')}
							source={{uri: this.state.photos[i].node.image.uri}}
						/>
					</TouchableHighlight>
					{this.state.photos.length !== i + 1 ?
						<TouchableHighlight
							style={select2}
							onPress={() => this.onListSelectedImage(this.state.photos[i + 1].node.image.uri, i + 1)}>
							<Image
								style={selected2}
								defaultSource={require('../../../images/giphy.gif')}
								source={{uri: this.state.photos[i + 1].node.image.uri}}
							/>
						</TouchableHighlight> : null}
				</View>
			);
		}
		return img;
	}

	async getDocumentosAspirante(aspirante) {
		await fetch(this.state.uri + '/api/documentos/aspirante/' + aspirante.id_tutor + '/' + aspirante.id, {
			method: 'GET', headers: {
				'Content-Type': 'application/json'
			}
		}).then(response => response.json()).then(responseJson => {
			if (responseJson.error === null) {
				Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Galeria)', [
					{text: 'Enterado', onPress: () => Actions.pop({refresh: {key: 'drawer'}})}
				]);
			} else {
				this.setState({pictures: responseJson});
				this.state.elActa = this.state.pictures.acta_nacimiento;
				this.state.laCurp = this.state.pictures.curp_aspirante;
				this.state.laBoleta = this.state.pictures.boleta_aspirante;
				this.state.fotoAspirante = this.state.pictures.foto_aspirante;
				this.state.validarActa = this.state.pictures.validar_acta;
				this.state.validarCurp = this.state.pictures.validar_curp;
				this.state.validarBoleta = this.state.pictures.validar_boleta;
				this.state.validarFotos = this.state.pictures.validar_foto;
				this.state.esperar1 = false;
			}
		});
		await this.setState({aux: 0});
	}

	async closeModal(imagen) {
		if (this.state.laImagen === '') {
			Alert.alert('Sin imagen seleccionada', 'Seleccione una imagen para poder continuar');
		} else {
			if (this.state.documentCard === 1) {
				await this.uploadActa(imagen);
			} else if (this.state.documentCard === 2) {
				await this.uploadCurp(imagen);
			} else if (this.state.documentCard === 3) {
				await this.uploadBoleta(imagen);
			} else if (this.state.documentCard === 4) {
				await this.uploadFoto(imagen);
			}
		}
		await this.setState({aux: 0, indexSelectedImage: -1});
	}

	async alertDocumentosEnviados() {
		Alert.alert('Enviar documentos', 'Está a punto de enviar la documentación del aspirante\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.documentosEnviados(this.props.elAspirante)}, {text: 'No'}
		]);
	}

	async documentosEnviados(aspirante) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: aspirante.id,
			paso_registro: '2.1'
		}));
		await fetch(this.state.uri + '/api/cita/aspirantes', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar los documentos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Documentos aspirante)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					this.notificarAdmin(aspirante)
				}
			});

	}

	async notificarAdmin(aspirante) {
		let mensaje = 'El Sr(a) ' + aspirante.tutor.nombre + ' ' + aspirante.tutor.apellidos + ' ha subido la documentación del aspirante ' + aspirante.nombre_aspirante + ' para su pronta revisión';
		await fetch(this.state.uri + '/api/notificar/admins/' + mensaje).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar los documentos',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Enviar documentos)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					Alert.alert(
						'¡Documentación enviada con éxito!',
						'Hemos recibido sus documentos y serán revisados en breve. Recuerde que deberá traer los originales y fotocopia de cada uno para cotejo en la fecha del día de inmersión.',
						[{text: 'Enterado', onPress: () => Actions.pop({refresh: {key: Math.random()}})}]
					);
				}
			});
	}

	cards() {
		let p = Platform.OS === 'ios';
		let heightActa = [{...ifIphoneX({height: responsiveHeight(17)}, {height: p ? responsiveHeight(18.5) : responsiveHeight(19.5)})}];
		let heightBoleta = [{...ifIphoneX({height: responsiveHeight(17)}, {height: p ? responsiveHeight(19) : responsiveHeight(19.5)})}];
		let heightFoto = [{...ifIphoneX({height: responsiveHeight(17)}, {height: p ? responsiveHeight(22.2) : responsiveHeight(22.4)})}];
		let main = this.state.mainColor;
		let second = this.state.secondColor;
		const gif = this.state.loadImage === true ? require('../../../images/giphy.gif') : {uri: this.state.elActa};
		return (
			<View style={[styles.widthall]}>
				{/*-+-+Acta de nacimiento+-+-*/}
				<View
					style={[
						styles.widthall,
						styles.row,
						styles.carDocumentos,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 5,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<View>
						{this.state.indexSelectedImage === -1 && this.state.elActa === '' ?
							<View
								style={[heightActa, {
									width: responsiveWidth(30),
									backgroundColor: '#f6f6f6',
									alignItems: 'center',
									justifyContent: 'center'
								}]}
							>
								<Text>No hay foto</Text>
							</View> : this.state.elActa !== '' || this.state.documentCard === 1 ?
								<Image
									style={[heightActa, {
										width: responsiveWidth(30),
										resizeMode: 'cover'
									}]}
									source={{uri: this.state.elActa}}
									// defaultSource={require('../../../images/giphy.gif')}
									onLoadEnd={() => this.setState({loadImage: false})}
								/> : null}
						{this.state.esperar1 === true || this.state.validarActa === '2' ? (
							<View
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: responsiveHeight(4),
										backgroundColor: 'grey',
										borderColor: 'grey',
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.validarActa === '2' ? 'Validado' : 'Espere'}
								</Text>
							</View>
						) : (
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: responsiveHeight(4),
										backgroundColor: this.state.elActa !== '' ? main : second,
										borderColor: this.state.elActa !== '' ? main : second,
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
								onPress={() => [this.setState({
									modalVisible1: true,
									laImagen: '',
									indexSelectedImage: -1,
									documentCard: 1
								})]}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.elActa !== '' ? 'Modificar' : 'Subir documento'}
								</Text>
							</TouchableOpacity>
						)}
					</View>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{color: this.state.thirdColor, width: responsiveWidth(37), textAlign: 'left'}
							]}
						>
							Acta de nacimiento
						</Text>
						<Text style={{fontSize: responsiveFontSize(1.45), textAlign: 'left'}}>
							Debe subir la foto del acta de nacimiento más reciente que tenga disponible. La foto
							debe ser lo más clara posible y del documento completo.{'\n'}
						</Text><Text
						style={{
							fontSize: responsiveFontSize(1.45),
							marginBottom: 10,
							fontWeight: '700'
						}}>
						Recuerde que deberá llevar el acta de
						nacimiento en original y fotocopia para cotejo en la fecha del día de inmersión.
					</Text>
					</View>
				</View>
				{/*C.U.R.P*/}
				<View
					style={[
						styles.widthall,
						styles.row,
						styles.carDocumentos,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 5,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<View>
						{this.state.indexSelectedImage === -1 && this.state.laCurp === '' ?
							<View
								style={{
									width: responsiveWidth(30),
									height: Platform.OS === 'ios' ? responsiveHeight(17) : responsiveHeight(19.5),
									backgroundColor: '#f6f6f6',
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<Text>No hay foto</Text>
							</View>
							: this.state.laCurp !== '' || this.state.documentCard === 2 ?
								<Image
									style={{
										width: responsiveWidth(30),
										height: Platform.OS === 'ios' ? responsiveHeight(17) : responsiveHeight(19.5),
										resizeMode: 'cover'
									}}
									onLoadEnd={() => this.setState({loadImage: false})}
									onError={() => this.setState({error: true})}
									defaultSource={require('../../../images/giphy.gif')}
									source={this.state.loadImage === true ? require('../../../images/giphy.gif') : {uri: this.state.laCurp}}
								/> : null}
						{this.state.esperar1 === true || this.state.validarCurp === '2' ? (
							<View
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: responsiveHeight(4),
										backgroundColor: 'grey',
										borderColor: 'grey',
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.validarCurp === '2' ? 'Validado' : 'Espere'}
								</Text>
							</View>
						) : (
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: responsiveHeight(4),
										backgroundColor: this.state.laCurp !== '' ? main : second,
										borderColor: this.state.laCurp !== '' ? main : second,
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
								onPress={() => [this.setState({
									modalVisible1: true,
									laImagen: '',
									indexSelectedImage: -1,
									documentCard: 2
								})]}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.laCurp !== '' ? 'Modificar' : 'Subir documento'}
								</Text>
							</TouchableOpacity>
						)}
					</View>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{color: this.state.thirdColor, width: responsiveWidth(64), textAlign: 'left'}
							]}
						>
							C.U.R.P
						</Text>
						<Text style={{fontSize: responsiveFontSize(1.45), textAlign: 'left'}}>
							Debe subir la C.U.R.P más reciente que tenga disponible. La foto debe
							ser lo más clara posible y del documento completo.
						</Text><Text
						style={{
							fontSize: responsiveFontSize(1.45),
							marginBottom: 10,
							marginTop: 5,
							fontWeight: '700'
						}}>
						Recuerde que deberá llevar la C.U.R.P en original y fotocopia para cotejo en la fecha
						del día de
						inmersión.
					</Text>
					</View>
				</View>
				{/*Ultima boleta de calificaciones*/}
				<View
					style={[
						styles.widthall,
						styles.row,
						styles.carDocumentos,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 5,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<View>
						{this.state.indexSelectedImage === -1 && this.state.laBoleta === '' ?
							<View
								style={[heightBoleta, {
									width: responsiveWidth(30),
									backgroundColor: '#f6f6f6',
									alignItems: 'center',
									justifyContent: 'center'
								}]}
							>
								<Text>No hay foto</Text>
							</View>
							: this.state.laBoleta !== '' || this.state.documentCard === 3 ?
								<Image
									style={[heightBoleta, {
										width: responsiveWidth(30),
										resizeMode: 'cover'
									}]}
									onLoadEnd={() => this.setState({loadImage: false})}
									onError={() => this.setState({error: true})}
									source={this.state.loadImage=== true ? require('../../../images/giphy.gif') : {uri: this.state.laBoleta}}
								/> : null}
						{this.state.esperar1 === true || this.state.validarBoleta === '2' ? (
							<View
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: responsiveHeight(4),
										backgroundColor: 'grey',
										borderColor: 'grey',
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.validarBoleta === '2' ? 'Validado' : 'Espere'}
								</Text>
							</View>
						) : (
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: responsiveHeight(4),
										backgroundColor: this.state.laBoleta !== '' ? main : second,
										borderColor: this.state.laBoleta !== '' ? main : second,
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
								onPress={() => this.setState({
									modalVisible1: true,
									laImagen: '',
									indexSelectedImage: -1,
									documentCard: 3
								})}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.laBoleta !== '' ? 'Modificar' : 'Subir documentos'}
								</Text>
							</TouchableOpacity>
						)}
					</View>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{color: this.state.thirdColor, width: responsiveWidth(64), textAlign: 'left'}
							]}
						>
							Última boleta de calificaciones
						</Text>
						<Text style={{fontSize: responsiveFontSize(1.45), textAlign: 'left'}}>
							Debe subir la foto de la última boleta de calificaciones más reciente que tenga
							disponible.
							La foto debe
							ser lo más clara posible y del documento completo.
						</Text><Text
						style={{
							fontSize: responsiveFontSize(1.45),
							marginBottom: 10,
							marginTop: 5,
							fontWeight: '700'
						}}>
						Recuerde que deberá llevar la última boleta de calificaciones en original y fotocopia
						para
						cotejo en la fecha del día de inmersión.
					</Text>
					</View>
				</View>
				{/*foto del aspirante*/}
				<View
					style={[
						styles.widthall,
						styles.row,
						styles.carDocumentos,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 8,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<View>
						{this.state.indexSelectedImage === -1 && this.state.fotoAspirante === '' ?
							<View
								style={[heightFoto, {
									width: responsiveWidth(30),
									backgroundColor: '#f6f6f6',
									alignItems: 'center',
									justifyContent: 'center'
								}]}
							>
								<Text>No hay foto</Text>
							</View> : this.state.fotoAspirante !== '' || this.state.documentCard === 4 ?
								<Image
									style={[heightFoto, {
										width: responsiveWidth(30),
										resizeMode: 'cover'
									}]}
									onLoadEnd={() => this.setState({loadImage: false})}
									onError={() => this.setState({error: true})}
									source={this.state.loadImage=== true ? require('../../../images/giphy.gif') : {uri: this.state.fotoAspirante}}
								/> : null}
						{this.state.esperar1 === true || this.state.validarFotos === '2' ? (
							<View
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: Platform.OS === 'ios' ? responsiveHeight(4) : responsiveHeight(5),
										backgroundColor: 'grey',
										borderColor: 'grey',
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.validarFotos === '2' ? 'Validado' : 'Espere'}
								</Text>
							</View>
						) : (
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{
										width: responsiveWidth(30),
										height: Platform.OS === 'ios' ? responsiveHeight(4) : responsiveHeight(5),
										backgroundColor: this.state.fotoAspirante !== '' ? main : second,
										borderColor: this.state.fotoAspirante !== '' ? main : second,
										marginVertical: 0,
										borderBottomLeftRadius: 10,
										borderRadius: 0
									}
								]}
								onPress={() => [this.setState({
									modalVisible1: true,
									laImagen: '',
									indexSelectedImage: -1,
									documentCard: 4
								})]}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.fotoAspirante !== '' ? 'Modificar' : 'Subir documentos'}
								</Text>
							</TouchableOpacity>
						)}
					</View>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{color: this.state.thirdColor, width: responsiveWidth(64), textAlign: 'left'}
							]}
						>
							Foto del aspirante
						</Text>
						<Text style={{fontSize: responsiveFontSize(1.45), textAlign: 'left'}}>
							Debe subir una foto tomada de frente y en primer plano(de los hombros para arriba).
							La foto debe ser con blusa o camisa blanca, con fondo blanco. Los niños cabello corto y las
							niñas con cabello recogido y sin maquillaje.{'\n'}
						</Text><Text
						style={{
							fontSize: responsiveFontSize(1.45),
							marginBottom: 10,
							fontWeight: '700'
						}}>
						Recuerde que deberá llevar 2 fotografias tamaño infantil en blanco y negro en la fecha
						del día
						de inmersión.
					</Text>
					</View>
				</View>
			</View>
		)
	}

	render() {
		return (
			<View style={styles.container}>
				<Modal
					animationType='slide'
					transparent={false}
					visible={this.state.modalVisible1}
				>
					<View style={[styles.container]}>
						{ifIphoneX ? (
							<View
								style={{...ifIphoneX({height: responsiveHeight(4)}, {height: responsiveHeight(3)})}}
							/>
						) : null}
						<Text
							style={[styles.main_title, {color: this.state.thirdColor}]}>
							Agregar foto de documento
						</Text>
						<View style={{marginTop: responsiveHeight(1)}}/>
						<ScrollView>
							{this.galeriaPicker()}
						</ScrollView>
						<View style={[styles.modalWidth, styles.row, {...ifIphoneX({marginBottom: 20})}]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => [this.setState({
									modalVisible1: false,
									laImagen: '',
									indexSelectedImage: -1
								}), this.getDocumentosAspirante(this.props.elAspirante)]}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.closeModal(this.state.laImagen)}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Subir documentos
				</Text>
				<ScrollView showsVerticalScrollIndicator={false}>
					{this.cards()}
					{this.state.elActa !== '' && this.state.laCurp !== '' && this.state.laBoleta !== '' && this.state.fotoAspirante !== '' ? (
						<TouchableOpacity
							style={[
								styles.bigButton,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.alertDocumentosEnviados()}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Enviar documentos</Text>
						</TouchableOpacity>
					) : <View
						style={{height: responsiveHeight(6)}}
					/>}
				</ScrollView>
			</View>
		);
	}
}
