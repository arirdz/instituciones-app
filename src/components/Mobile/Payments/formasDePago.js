import React from 'react';
import {Alert, AsyncStorage, Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class formadepagoquenofunciona extends React.Component {
    getFeed = () => {
        let formData = new FormData();
        formData.append('parameters', JSON.stringify({
            method: 'store',
            amount: this.props.FinalPrice,
            description: this.state.itemDescripcion,
            order_id: 'CE-0' + (this.state.itempay) + '-' + this.state.hijo + '-' + this.state.intento,
            due_date: moment.utc(this.state.item_due_date)
        }));
        fetch(this.state.url + "/api/pasarelas/v1/openpay/customer/" + this.state.userID + "/store/charge", {
            method: "POST",
            headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'multipart/form-data',
                Authorization: "Bearer " + this.state.token
            },
            body: formData
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState({recibo: responseJson.result});
                if (responseJson.response !== 'error') {
                    Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
                        text: 'Gracias',
                        onPress: () => Actions.reciboTienda({
                            reciboTienda: responseJson.result,
                            transaction_id: responseJson.transaction_id,
                            user: this.state.email
                        })
                    }]);
                    this.setState({recibo: responseJson});
                } else {
                    if (responseJson.error.code === 3001) {
                        Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3002) {
                        Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirada', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3003) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3004) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3005) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
                            text: 'Entendido'
                        }]);

                    } else if (responseJson.error.code === 1006) {
                        Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
                            text: 'Entendido'
                        }]);

                    } else {
                        Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
                            text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
                        }]);
                    }
                }
            });
    };

    getFeedTransfer = () => {
        let formData = new FormData();
        formData.append('parameters', JSON.stringify({
            method: 'bank_account',
            amount: this.props.FinalPrice,
            description: this.state.itemDescripcion,
            order_id: 'CE-0' + (this.state.itempay) + '-' + this.state.hijo + '-' + this.state.intento,
            due_date: moment.utc(this.state.item_due_date)
        }));
        fetch(this.state.url + "/api/pasarelas/v1/openpay/customer/" + this.state.userID + "/spei/charge", {
            method: "POST",
            headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'multipart/form-data',
                Authorization: "Bearer " + this.state.token
            },
            body: formData
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState({recibo: responseJson.result});
                if (responseJson.response !== 'error') {
                    this.setState({recibo: responseJson.result});
                    Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
                        text: 'Gracias',
                        onPress: () => Actions.reciboTransferencia({
                            reciboTienda: responseJson.result,
                            transaction_id: responseJson.transaction_id,
                            user: this.state.email
                        })
                    }]);
                } else {
                    if (responseJson.error.code === 3001) {
                        Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3002) {
                        Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirada', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3003) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3004) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3005) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
                            text: 'Entendido'
                        }]);

                    } else if (responseJson.error.code === 1006) {
                        Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
                            text: 'Entendido'
                        }]);

                    } else {
                        Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
                            text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
                        }]);
                    }
                }
            });
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [], url: '', recibo: [], intento: 0
        };
    }

    async comprobar(i) {
        let r1;
        let r2;
        if (i === 1) {
            r1 = '¿Está seguro que desea generar el pago con transferencia?';
            r2 = '¿Está seguro que desea generar de nuevo el pago con transferencia?';
        } else {
            r1 = '¿Está seguro que desea generar el pago con referencia en tienda?';
            r2 = '¿Está seguro que desea generar de nuevo el pago con referencia en tienda?';
        }
        await fetch(this.state.url + '/api/payment/dos/get/exist/charge/' + this.state.hijo + '/' + this.state.itempay, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({intento: responseJson});
                    if (responseJson === 1) {
                        Alert.alert(
                            'Generar pago',
                            r1,
                            [{
                                text: 'Si',
                                onPress: () => {
                                    i === 1 ? this.getFeedTransfer() : this.getFeed()
                                }
                            },
                                {text: 'No'}]);
                    }
                    else {
                        Alert.alert(
                            'Este cargo ya ha sido generado',
                            r2,
                            [{
                                text: 'Si',
                                onPress: () => {
                                    i === 1 ? this.getFeedTransfer() : this.getFeed()
                                }
                            },
                                {text: 'No'}]);
                    }
                }
            });
    }

    async componentWillMount() {
        await this.getURL()
    }

    async getURL() {
        let token = await AsyncStorage.getItem('token');
        let url = await AsyncStorage.getItem('uri');
        let aydi = await AsyncStorage.getItem('itemid');
        let itemTotalToPay = await AsyncStorage.getItem('itemTotalToPay');
        let itemDescripcion = await AsyncStorage.getItem('itemDescripcion');
        let item_due_date = await AsyncStorage.getItem('item_due_date');
        let user_id = await AsyncStorage.getItem('userID');
        let hijo = await AsyncStorage.getItem('itemHijoId');
        let email = await AsyncStorage.getItem('email');
        await this.setState({
            itempay: aydi,
            itemTotalToPay: itemTotalToPay,
            itemDescripcion: itemDescripcion,
            item_due_date: item_due_date,
            userID: user_id,
            url: url,
            token: token,
            hijo: hijo,
            email: email
        });
    }

    async pagarTarjeta() {
        await fetch(this.state.url + '/api/payment/dos/get/exist/charge/' + this.state.hijo + '/' + this.state.itempay, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({intento: responseJson});
                    if (responseJson === 1) {
                        Alert.alert(
                            'Generar pago',
                            '¿Está seguro que desea generar el pago con tarjeta?',
                            [{
                                text: 'Si',
                                onPress: () => {
                                    Actions.pagoTarjetas({
                                        FinalPrice: this.props.FinalPrice,
                                        intento: this.state.intento,
                                        user: this.state.email
                                    })
                                }
                            },
                                {text: 'No'}]);
                    }
                    else {
                        Alert.alert(
                            'Este cargo ya ha sido generado',
                            '¿Está seguro que desea generar de nuevo el pago con tarjeta?',
                            [{
                                text: 'Si',
                                onPress: () => {
                                    Actions.pagoTarjetas({
                                        FinalPrice: this.props.FinalPrice,
                                        intento: this.state.intento,
                                        user: this.state.email
                                    })
                                }
                            },
                                {text: 'No'}]);
                    }
                }
            });

    }

    render() {
        return (<View style={styles.container}>
            <ScrollView style={styles.menu} showsVerticalScrollIndicator={false}>
                <View style={styles.row_M}>
                    <TouchableOpacity
                        onPress={() => this.comprobar(1)}
                        style={styles.btnContainer_M}>
                        <Text
                            style={{
                                color: 'black',
                                fontSize: responsiveFontSize(1.8),
                                textAlign: 'center',
                                paddingTop: 13,
                                width: responsiveWidth(35)
                            }}>
                            <Text style={{
                                fontWeight: '700'
                            }}>Transferencia {'\n'}</Text>
                            ELECTRONICA
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.btnContainer_M}
                        onPress={() => this.pagarTarjeta()}>
                        <Text
                            style={{
                                color: 'black',
                                fontSize: responsiveFontSize(1.8),
                                textAlign: 'center',
                                paddingTop: 13,
                                width: responsiveWidth(35)
                            }}>
                            Tarjeta de{'\n'}
                            <Text style={{
                                fontWeight: '700'
                            }}>CREDITO/DEBITO</Text>
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.row_M}>
                    <TouchableOpacity onPress={() => this.comprobar(2)} style={styles.btnContainer_M}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(10), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/superama.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnContainer_M} onPress={() => this.comprobar(2)}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(10), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/walmart.png')}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.row_M}>
                    <TouchableOpacity style={styles.btnContainer_M} onPress={() => this.comprobar(2)}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(10), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/fa.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnContainer_M} onPress={() => this.comprobar(2)}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(10), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/sams.png')}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.row_M}>
                    <TouchableOpacity style={styles.btnContainer_M} onPress={() => this.comprobar(2)}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(10), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/fg.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnContainer_M} onPress={() => this.comprobar(2)}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(9), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/waldos.png')}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.row_M}>
                    <TouchableOpacity style={styles.btnContainer_M} onPress={() => this.comprobar(2)}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(9), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/prenda.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnContainer_M} onPress={() => this.comprobar(2)}>
                        <Image
                            style={[styles.imagen, {
                                height: responsiveHeight(10), width: responsiveWidth(35)
                            }]}
                            source={require('../../../images/aurrera.png')}/>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>);
    }
}
