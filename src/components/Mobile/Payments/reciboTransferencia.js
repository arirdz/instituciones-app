import React from 'react';
import {AsyncStorage, Image, ScrollView, Text, View} from 'react-native';
import styles from '../../styles';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';

let moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class reciboTransferencia extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reciboTienda: []
        };
    }

    async componentWillMount() {
        await this.getUri();
        await this.sendMail();
    }

    async getUri() {
        let token = await AsyncStorage.getItem('token');
        let url = await AsyncStorage.getItem('uri');
        await this.setState({uri: url, token: token});
    }

	async sendMail() {
		if (this.state.token !== null || this.state.token !== undefined || this.state.token !== '') {
			let formData = new FormData();
			formData.append('data', JSON.stringify({
				method: 'spei',
				transaction_id: this.props.transaction_id,
				email: this.props.user
			}));
			fetch(this.state.uri + '/api/metodo/pago', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				},
				body: formData
			}).then(response => response.json())
				.then(responseJson => {
				});
		} else {
			let formData = new FormData();
			formData.append('data', JSON.stringify({
				method: 'spei',
				transaction_id: this.props.transaction_id,
				email: this.props.user
			}));
			fetch(this.state.uri + '/api/metodo/pago/preinscrip', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				},
				body: formData
			}).then(response => response.json())
				.then(responseJson => {
				});
		}
	}

    render() {
        return (<View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={true}>
                <View style={[styles.rowPago, {backgroundColor: '#F8A919', borderColor: '#F8A919'}]}>
                    <Text style={styles.tituloPago}>Total a pagar
                    </Text>
                    <Text style={styles.infoPago}>
                        ${' ' + this.props.reciboTienda.amount}
                        <Text style={styles.infoPagoP}>{' MXN'}</Text>
                    </Text>
                </View>

                <View style={[styles.row_rT, {marginTop: 20}]}>
                    <View style={styles.row}>
                        <View style={{width: responsiveWidth(10)}}>
                            <View style={{backgroundColor: '#F8A919', width: 30, height: 40}}/>
                        </View>
                        <View style={{width: responsiveWidth(84), height: 35, justifyContent: 'flex-end'}}>
                            <Text style={[styles.info, {fontWeight: '500'}]}>Fecha límite de pago</Text>
                        </View>
                    </View>

                    <View style={[styles.row, {marginTop: 10}]}>
                        <View style={{width: responsiveWidth(10)}}>
                        </View>
                        <View style={{width: responsiveWidth(84)}}>
                            <Text style={[styles.info, {
                                fontSize: responsiveFontSize(1.6), fontWeight: '500'
                            }]}>{moment(this.props.reciboTienda.due_date).format('lll') + '\n'}</Text>
                        </View>
                    </View>
                </View>
                <View style={[styles.row_rT, {marginTop: 0}]}>
                    <View style={styles.row}>
                        <View style={{width: responsiveWidth(10)}}>
                        </View>
                        <View style={{width: responsiveWidth(84), height: 35, justifyContent: 'flex-end'}}>
                            <Text style={[styles.info, {fontWeight: '500'}]}>Beneficiario</Text>
                        </View>
                    </View>

                    <View style={[styles.row, {marginTop: 10}]}>
                        <View style={{width: responsiveWidth(10)}}>
                        </View>
                        <View style={{width: responsiveWidth(84)}}>
                            <Text style={[styles.info, {
                                fontSize: responsiveFontSize(1.6), fontWeight: '500'
                            }]}>{'Control Escolar' + '\n'}</Text>
                        </View>
                    </View>
                </View>

                <View style={[styles.row_rT, {marginTop: 30}]}>
                    <View style={styles.row}>
                        <View style={{width: responsiveWidth(10)}}>
                            <View style={{backgroundColor: '#F8A919', width: 30, height: 40}}/>
                        </View>
                        <View style={{width: responsiveWidth(84), height: 35, justifyContent: 'flex-end'}}>
                            <Text style={[styles.info, {fontWeight: '500'}]}>Detalles de la compra</Text>
                        </View>
                    </View>

                    <View style={[styles.row, {marginTop: 10}]}>
                        <View style={{
                            width: responsiveWidth(38), padding: 10, paddingLeft: 15, backgroundColor: '#F1F1F1'
                        }}><Text>Descripción</Text></View>
                        <View style={{
                            width: responsiveWidth(56), padding: 10, paddingLeft: 15, backgroundColor: '#F1F1F1'
                        }}><Text>{this.props.reciboTienda.description}</Text></View>
                    </View>
                    <View style={[styles.row, {marginTop: 0}]}>
                        <View style={{
                            width: responsiveWidth(38), padding: 10, paddingLeft: 15, backgroundColor: '#E8E8E8'
                        }}><Text>Fecha y hora</Text></View>
                        <View style={{
                            width: responsiveWidth(56), padding: 10, paddingLeft: 15, backgroundColor: '#E8E8E8'
                        }}><Text>{moment(this.props.reciboTienda.due_date).format('lll')}</Text></View>
                    </View>
                </View>

                <View style={[styles.row_rT, {marginTop: 30}]}>
                    <View style={styles.row}>
                        <View style={{width: responsiveWidth(10)}}>
                            <View style={{backgroundColor: '#F8A919', width: 30, height: 40}}/>
                        </View>
                        <View style={{width: responsiveWidth(84), height: 35, justifyContent: 'flex-end'}}>
                            <Text style={[styles.info, {fontWeight: '500'}]}>Pasos para realizar el pago</Text>
                        </View>
                    </View>

                    <View style={[styles.row, {marginTop: 10}]}>
                        <View style={{width: responsiveWidth(94)}}>
                            <View style={{
                                padding: 15, backgroundColor: '#F1F1F1'
                            }}>
                                <View style={styles.row}><Text style={[styles.description, {fontWeight: '500'}]}>Desde
                                    BBVA
                                    Bancomer</Text></View>
                                <View style={[styles.row, {marginTop: 8}]}><Text
                                    style={[styles.description, {width: responsiveWidth(84)}]}>1. Dentro del menú
                                    de "Pagar"
                                    seleccione la opción
                                    "De Servicios" e
                                    ingrese el siguiente
                                    "Número de
                                    convenio CIE"</Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Número de convenio CIE: <Text
                                    style={styles.description}>{this.props.reciboTienda.payment_method.agreement}</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {width: responsiveWidth(84)}]}>2. Ingrese los
                                    datos de registro
                                    para concluir con
                                    la
                                    operación.</Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Referencia: <Text
                                    style={styles.description}>{this.props.reciboTienda.payment_method.name}</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Importe: <Text
                                    style={styles.description}>{'$ ' + this.props.reciboTienda.amount + ' MXN'}</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Concepto: <Text
                                    style={styles.description}>{this.props.reciboTienda.description}</Text></Text></View>
                            </View>
                        </View>
                    </View>

                    <View style={[styles.row, {marginTop: 10}]}>
                        <View style={{width: responsiveWidth(94)}}>
                            <View style={{
                                padding: 15, backgroundColor: '#F1F1F1',
                            }}>
                                <View style={styles.row}><Text style={[styles.description, {fontWeight: '500'}]}>Desde
                                    cualquier otro banco</Text></View>
                                <View style={[styles.row, {marginTop: 8,}]}><Text
                                    style={[styles.description, {width: responsiveWidth(84)}]}>1. Ingresa a la sección
                                    de transferencias y pagos o
                                    pagos a otros bancos y proporciona los datos de
                                    la transferencia:</Text></View>

                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Beneficiario: <Text
                                    style={styles.description}>{this.props.reciboTienda.payment_method.name}</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Banco destino: <Text
                                    style={styles.description}>BBVA Bancomer</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Clabe: <Text
                                    style={styles.description}>{this.props.reciboTienda.payment_method.clabe}</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Concepto de pago: <Text
                                    style={styles.description}>{this.props.reciboTienda.payment_method.name}</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Referencia: <Text
                                    style={styles.description}>{this.props.reciboTienda.payment_method.agreement}</Text></Text></View>
                                <View style={[styles.row, {marginTop: 13}]}><Text
                                    style={[styles.description, {fontWeight: '500'}]}>Importe: <Text
                                    style={styles.description}>{'$ ' + this.props.reciboTienda.amount + ' MXN'}</Text></Text></View>
                            </View>
                        </View>
                    </View>
                </View>


                <View style={[styles.row, {alignItems: 'center'}]}>
                    <Image
                        style={{
                            width: responsiveWidth(50), height: responsiveHeight(5), resizeMode: 'contain'
                        }}
                        source={require('../../../images/bancos/BANCOMER.png')}/>
                </View>

                <View style={[styles.row, {justifyContent: 'space-between', alignItems: 'center'}]}>
                    <View><Image
                        style={{
                            width: responsiveWidth(25), height: responsiveHeight(5), resizeMode: 'contain'
                        }}
                        source={require('../../../images/bancos/CITIBANAMEX.png')}/></View>

                    <View><Image
                        style={{
                            width: responsiveWidth(25), height: responsiveHeight(5), resizeMode: 'contain'
                        }}
                        source={require('../../../images/bancos/BANORTE.png')}/></View>
                    <View><Image
                        style={{
                            width: responsiveWidth(25), height: responsiveHeight(5), resizeMode: 'contain'
                        }}
                        source={require('../../../images/bancos/SANTANDER.png')}/></View>
                </View>


            </ScrollView>
        </View>);
    }
}
