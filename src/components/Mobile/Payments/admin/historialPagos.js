import React from 'react';
import {
    AsyncStorage,
    ScrollView,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import Modal from 'react-native-modal';
import GradoyGrupo from '../../Globales/GradoyGrupo';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class pagosVencidos extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            alumnos: [],
            cargos: [],
            cososPicker: [],
            elAlumno: '',
            elGrado: '',
            elGrupo: '',
            elHijo: '',
            grados: [],
            grupos: [],
            isDateTimePickerVisible: false,
            mes: moment().month() + 1,
            selectedIndex: -1,
            selectedIndexAlumno: -1,
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selectedIndexPago: -1,
            selItem: '',
            textInputValue: ''
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getMeses();
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            elGrupo: grupo
        });
        await this.getAlumnos();
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            elGrado: grado
        });
    }

    //+++++++++++++++++++++++++++++++++++++++ MESES ++++++++++++++++++++++++
    async onChange(option) {
        await this.setState({textInputValue: option.label, mes: option.mes});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    // +++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++

    async getAlumnos() {
        let alumnoList = await fetch(
            this.state.uri +
            '/api/user/alumnos/' +
            this.state.elGrado +
            '/' +
            this.state.elGrupo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        alumnosList = await alumnoList.json();
        this.setState({alumnos: alumnosList});
    }

    async onListPressedAlumno(indexAlumno, alumno, id) {
        await this.setState({
            selectedIndexAlumno: indexAlumno,
            elAlumno: alumno,
            selectedIndexPago: -1,
            selItem: '',
            elHijo: id
        });
        await this.getCargos();
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [
            styles.rowTabla,
            styles.modalWidth,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno == indexAlumno) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={[tabRow, smallButtonStyles]}
                onPress={() =>
                    this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.id)
                }>
                <View>
                    {this.state.isModalalumno === true
                        ? this.isValor(itemAlumno, indexAlumno)
                        : null}
                </View>
            </TouchableHighlight>
        );
    }

    isValor(itemAlumno, indexAlumno) {
        return (
            <View style={[styles.campoTablaG, {alignItems: 'center'}]}>
                <View>
                    <Text style={{marginRight: 25}}>{itemAlumno.name}</Text>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async getCargos() {
        let request = await fetch(
            this.state.uri +
            '/api/payments/historial/customer/charges/' +
            this.state.elHijo +
            '/' +
            this.state.mes,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let data = await request.json();
        await this.setState({cargos: data});
    }

    async onListItemPressedPago(index, item) {
        await this.setState({selectedIndexPago: index, selItem: item});
    }

    renderPago(itemPago, indexPago) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            styles.row,
            {backgroundColor: '#f2f2f2', borderWidth: 0}
        ];

        if (this.state.selectedIndexPago == indexPago) {
            bigButtonStyles.push(
                styles.listButton,
                styles.listButtonBig,
                styles.row,
                {backgroundColor: '#ddd', borderWidth: 0}
            );
        }
        return (
            <TouchableOpacity
                key={indexPago}
                style={bigButtonStyles}
                underlayColor={'ransparent'}
                onPress={() => this.onListItemPressedPago(indexPago, itemPago)}>
                <View style={[styles.btn8, {marginLeft: 9}]}>
                    <Text numberOfLines={1} style={{textAlign: 'left'}}>
                        {itemPago.local.descripcion}
                    </Text>
                </View>
                <View
                    style={[
                        styles.btn4_lT,
                        {
                            height: responsiveHeight(3.75),
                            justifyContent: 'center'
                        }
                    ]}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(1.5),
                            textAlign: 'right'
                        }}>
                        ${Number(itemPago.local.importe_a_pagar).toLocaleString('en-US')}
                    </Text>
                </View>
                <View
                    style={[
                        styles.btn3,
                        {height: responsiveHeight(3.75), justifyContent: 'center'}
                    ]}>
                    <Text
                        style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>
                        {itemPago.openpay.status}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    renderPagos() {
        let buttonsPagos = [];
        this.state.cargos.forEach((itemPago, indexPago) => {
            if (this.state.cargos[0].response !== 'Empty') {
                buttonsPagos.push(this.renderPago(itemPago, indexPago));
            }
        });
        return buttonsPagos;
    }

    render() {
        const listaPagos = this.renderPagos();
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        const alumnos = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.isModalalumno}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de alumnos
                            </Text>
                            <View
                                style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}
                            />
                            <View
                                style={[
                                    styles.tabla,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}>
                                {alumnos}
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor
                                }
                            ]}
                            onPress={() => this.setState({isModalalumno: false})}>
                            <Text style={styles.textButton}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text
                        style={[
                            styles.main_title,
                            {color: this.state.thirdColor, marginBottom: 1}
                        ]}>
                        Elija el periodo a consultar
                    </Text>
                    <ModalSelector
                        data={data}
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[
                            [styles.inputPicker, {borderColor: this.state.secondColor}],
                            {marginTop: 1}
                        ]}
                        initValue={mess}
                        onChange={option => this.onChange(option)}
                    />
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el alumno
                    </Text>
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.btnLista,
                                {borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.setState({isModalalumno: true})}>
                            <Text style={[styles.textButton, {color: 'black'}]}>
                                Ver lista
                            </Text>
                        </TouchableOpacity>
                        <Text style={{marginTop: 10}}>Alumno seleccionado</Text>
                        <Text style={[styles.textButton, {color: 'black'}]}>
                            {this.state.elAlumno}
                        </Text>
                    </View>
                    <View style={{width: responsiveWidth(95)}}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Lista de pagos requeridos
                        </Text>
                    </View>
                    <View>
                        <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                            <View style={styles.btn4}>
                                <Text style={{fontWeight: '700', textAlign: 'center'}}>
                                    Concepto
                                </Text>
                            </View>
                            <View style={styles.btn2_5}>
                                <Text style={{fontWeight: '700', textAlign: 'right'}}>
                                    importe
                                </Text>
                            </View>
                            <View style={styles.btn3}>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        textAlign: 'center',
                                        marginLeft: 5
                                    }}>
                                    Estado
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                height: responsiveHeight(23),
                                borderBottomWidth: 1,
                                borderTopWidth: 1,
                                marginBottom: 5
                            }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {listaPagos}
                            </ScrollView>
                        </View>
                    </View>
                    <View
                        style={{
                            alignItems: 'flex-start',
                            width: responsiveWidth(95),
                            height: responsiveHeight(5)
                        }}>
                        <Text
                            style={[
                                styles.main_title,

                                {color: this.state.thirdColor, marginBottom: 1}
                            ]}>
                            Detalle del pago
                        </Text>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(2),
                                fontWeight: '300',
                                color: this.state.thirdColor
                            }}>
                            Descripción:
                        </Text>
                    </View>
                    {this.state.selectedIndexPago !== -1 ? (
                        <View
                            style={{
                                backgroundColor: '#ddd',
                                width: responsiveWidth(95),
                                height: responsiveHeight(20),
                                marginTop: 25,
                                borderRadius: 6,
                                marginBottom: 30
                            }}>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    marginTop: 10,
                                    marginHorizontal: 10,
                                    textAlign: 'left'
                                }}>
                                El pago fue realizado el dia:{' '}
                                <Text
                                    style={{
                                        fontWeight: '600',
                                        marginLeft: 20
                                    }}>
                                    {moment(this.state.selItem.openpay.creation_date).format(
                                        'DD/MM/YY'
                                    ) + '\n\n'}
                                </Text>
                                Fue realizado por medio de:{' '}
                                <Text
                                    style={{
                                        fontWeight: '600',
                                        marginLeft: 20
                                    }}>
                                    {this.state.selItem.openpay.method}
                                </Text>
                            </Text>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    marginTop: 30,
                                    marginHorizontal: 10,
                                    textAlign: 'center',
                                    fontWeight: '600'
                                }}>
                                {this.state.selItem.local.descripcion}
                            </Text>
                        </View>
                    ) : null}
                </ScrollView>
            </View>
        );
    }
}
