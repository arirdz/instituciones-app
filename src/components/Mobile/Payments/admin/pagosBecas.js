import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    AsyncStorage,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');
export default class pagosBecas extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            textInputValue: '',
            elMes: '',
            cososPicker: [],
            grados: [],
            grupos: [],
            elGrado: '',
            mes: moment().month() + 1,
            elGrupo: '',
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexGrupo: -1,
            selectedIndexPago: -1,
            selectedIndex: -1,
            elAlumno: '',
            alumnos: []
        };

        this.renderList = this.renderList.bind(this);
        this.renderGrupos = this.renderGrupos.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
        this.onListItemPressedGrupo = this.onListItemPressedGrupo.bind(this);
        this.renderPagos = this.renderPagos.bind(this);
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getMeses();
        this.getGrados();
        this.getGrupos();
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ MESES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async onChange(option) {
        await this.setState({textInputValue: option.label, elMes: option.mes});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ GRADOS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getGrados() {
        let gradpicker = await fetch(this.state.uri + '/api/get/grados');
        gradospicker = await gradpicker.json();
        this.setState({
            grados: gradospicker.map(item => item)
        });
    }

    async onListItemPressed(index, grado) {
        await this.setState({
            selectedIndex: index,
            elGrado: grado,
            selectedIndexGrupo: -1,
            selectedIndexAlumno: -1
        });
    }

    renderItem(listItem, index) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            {borderColor: this.state.secondColor}
        ];
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndex == index) {
            bigButtonStyles.push(styles.listButtonSelected);
            smallButtonStyles.push(styles.listButtonSelected);
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={index}
                underlayColor={secondColor}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressed(index, listItem.grado)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{listItem.grado}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderList() {
        let buttons = [];
        this.state.grados.forEach((item, index) => {
            buttons.push(this.renderItem(item, index));
        });
        return buttons;
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ GRUPOS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async getGrupos() {
        let grupicker = await fetch(this.state.uri + '/api/get/grupos');
        grupospicker = await grupicker.json();
        this.setState({
            grupos: grupospicker.map(item => item)
        });
    }

    async onListItemPressedGrupo(index, grupo) {
        await this.setState({
            selectedIndexGrupo: index,
            elGrupo: grupo,
            selectedIndexAlumno: -1
        });
        await this.getAlumnos();
    }

    renderGrupo(itemGrupo, indexGrupo) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            {borderColor: this.state.secondColor}
        ];
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexGrupo == indexGrupo) {
            bigButtonStyles.push(styles.listButtonSelected);
            smallButtonStyles.push(styles.listButtonSelected);
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexGrupo}
                underlayColor={secondColor}
                style={smallButtonStyles}
                onPress={() =>
                    this.onListItemPressedGrupo(indexGrupo, itemGrupo.grupo)
                }>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async getAlumnos() {
        let alumnoList = await fetch(
            this.state.uri +
            '/api/user/alumnos/' +
            this.state.elGrado +
            '/' +
            this.state.elGrupo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        alumnosList = await alumnoList.json();
        this.setState({alumnos: alumnosList});
    }

    onListPressedAlumno(indexAlumno, alumno) {
        this.setState({selectedIndexAlumno: indexAlumno, elAlumno: alumno});
        this.getCargos();
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let smallButtonStyles = [
            styles.listButtonAsunto,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexAlumno == indexAlumno) {
            smallButtonStyles.push(styles.listButtonSelected);
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexAlumno}
                underlayColor={secondColor}
                style={smallButtonStyles}
                onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.id)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemAlumno.name}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async getCargos() {
        let request = await fetch(
            this.state.uri + '/api/payments/charge/return/all/' + this.state.mes,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let data = await request.json();
        this.setState({cargos: data});
    }

    async onListItemPressedPago(index, item) {
        await this.setState({selectedIndexPago: index, selItem: item});
    }

    renderPago(itemPago, indexPago) {
        let bigButtonStyles = [
            styles.listButtonBott,
            styles.listButtonBig,
            {borderColor: this.state.secondColor}
        ];
        let smallButtonStyles = [
            styles.listButtonBott,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let smallButtonStylesRight = [
            styles.listButtonBott,
            styles.listButtonSmallRight,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexPago == indexPago) {
            bigButtonStyles.push(styles.listButtonSelectedDesc);
            smallButtonStyles.push(styles.listButtonSelectedCenter);
            smallButtonStylesRight.push(styles.listButtonSelectedRight);
            texto.push(styles.textoB);
        }

        return (
            <TouchableOpacity
                key={indexPago}
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: responsiveWidth(90),
                    marginTop: 10
                }}
                underlayColor={secondColor}
                onPress={() => this.onListItemPressedPago(indexPago, itemPago)}>
                <Text style={bigButtonStyles}>{itemPago.descripcion}</Text>
                <Text style={smallButtonStyles}>${Number(itemPago.importe_a_pagar).toLocaleString('en-US')}</Text>
                <Text style={smallButtonStylesRight}>$320</Text>
            </TouchableOpacity>
        );
    }

    renderPagos() {
        let buttonsPagos = [];
        this.state.cargos.forEach((itemPago, indexPago) => {
            buttonsPagos.push(this.renderPago(itemPago, indexPago));
        });
        return buttonsPagos;
    }

    render() {
        const listaPagos = this.renderPagos();

        let indexA = 0;
        const porcentajes = [
            {key: indexA++, label: '10%'},
            {key: indexA++, label: '20%'},
            {key: indexA++, label: '30%'},
            {key: indexA++, label: '40%'},
            {key: indexA++, label: '50%'},
            {key: indexA++, label: '60%'},
            {key: indexA++, label: '70%'},
            {key: indexA++, label: '80%'},
            {key: indexA++, label: '90%'},
            {key: indexA++, label: '100%'}
        ];
        let indexB = 0;
        const tipos = [
            {key: indexB++, label: 'Academica'},
            {key: indexB++, label: 'Deportiva'},
            {key: indexB++, label: 'Institucional'}
        ];
        const grupos = this.renderGrupos();
        const list = this.renderList();
        const alumnos = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={styles.title}>
                        Defina los parametros de la consulta:
                    </Text>
                    <Text style={styles.title}>Grado</Text>
                    <View style={styles.botones}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                            {list}
                        </ScrollView>
                    </View>
                    <Text style={styles.title}>Grupo</Text>
                    <View style={styles.botones}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                            {grupos}
                        </ScrollView>
                    </View>
                    <Text style={styles.title}>SELECCIONE EL ALUMNO</Text>
                    <View style={styles.contAlumnos}>
                        <ScrollView
                            style={[
                                styles.titulosContainer,
                                {borderColor: this.state.secondColor}
                            ]}
                            horizontal={false}
                            showsVerticalScrollIndicator={false}>
                            {alumnos}
                        </ScrollView>
                    </View>
                    <View style={styles.row}>
                        <View>
                            <Text style={{textAlign: 'center'}}>Beca asignada</Text>
                            <ModalSelector
                                data={porcentajes}
                                initValue="10%"
                                cancelText="Cancelar"
                                style={{width: responsiveWidth(45), marginVertical: 10}}
                                onChange={option => {
                                    Alert.alert(`${option.label} (${option.key})`);
                                }}
                            />
                        </View>
                        <View>
                            <Text style={{textAlign: 'center'}}>Beca asignada</Text>
                            <ModalSelector
                                data={tipos}
                                initValue="Academica"
                                cancelText="Cancelar"
                                style={{width: responsiveWidth(45), marginVertical: 10}}
                                onChange={option => {
                                    Alert.alert('Felicidades', 'Se ha asignado una beca ');
                                }}
                            />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col1}>
                            <Text style={styles.centeredTxt}>Total de alumnos becados</Text>
                            <TouchableHighlight style={styles.requerido}>
                                <Text style={styles.rounded}>18</Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.col1}>
                            <Text style={styles.centeredTxt}>
                                Porcentaje {'\n'}total de {'\n'}beca asignado
                            </Text>
                            <TouchableHighlight style={styles.realizado}>
                                <Text style={styles.rounded}>2600%</Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.col2}>
                            <Text style={styles.centeredTxt}>
                                Media {'\n'}de beca asignada
                            </Text>
                            <TouchableHighlight style={styles.pendiente}>
                                <Text style={styles.rounded}>15%</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={{width: responsiveWidth(95)}}>
                        <Text style={styles.titleBottom}>Estadisticas de becas</Text>
                    </View>
                    <View>
                        <View style={styles.cargos}>
                            <Text style={styles.subTitleMain}>Concepto</Text>
                            <Text
                                style={{marginLeft: 90, marginTop: 20, fontWeight: '700'}}>
                                Importe
                            </Text>
                            <Text
                                style={{
                                    marginLeft: 70,
                                    marginTop: 20,
                                    fontWeight: '700',
                                    textAlign: 'center'
                                }}>
                                Estado
                            </Text>
                        </View>
                        <View
                            style={{
                                height: responsiveHeight(23),
                                borderBottomWidth: 1,
                                borderTopWidth: 1,
                                marginBottom: 5
                            }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {listaPagos}
                            </ScrollView>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    titulo: {
        width: responsiveWidth(94),
        height: 20,
        marginTop: 5,
        marginBottom: 15
    },
    inputPicker: {
        backgroundColor: '#fff',
        width: responsiveWidth(94),
        padding: 15,
        height: 50,
        borderRadius: 6,
        borderColor: '#ccc',
        borderWidth: 1
    },
    subTitulo: {
        fontSize: responsiveFontSize(1.8),
        width: responsiveWidth(90),
        marginHorizontal: 10,
        color: 'black',
        marginTop: 5,
        fontWeight: '600'
    },
    botones: {
        alignItems: 'stretch',
        flexDirection: 'row',
        width: responsiveWidth(95)
    },
    listItem: {
        flexDirection: 'row',
        borderRadius: 6
    },
    listButton: {
        padding: 10,
        margin: 2,
        borderWidth: 1,
        borderRadius: 6,
        borderColor: 'lightgray',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    listButtonSmall: {
        backgroundColor: 'white',
        borderColor: 'lightgray',
        paddingVertical: 5,
        paddingHorizontal: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listButtonBott: {
        flexDirection: 'row',
        width: responsiveWidth(30),
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center'
    },
    listButtonSelected: {
        backgroundColor: '#3fa9f5',
        padding: 10,
        margin: 2,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 6,
        marginHorizontal: 1.5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listButtonSelectedDesc: {
        backgroundColor: '#3fa9f5',
        flexDirection: 'row',
        height: responsiveHeight(4),
        width: responsiveWidth(33),
        paddingTop: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listButtonSelectedCenter: {
        backgroundColor: '#3fa9f5',
        flexDirection: 'row',
        height: responsiveHeight(4),
        textAlign: 'center',
        width: responsiveWidth(33),
        paddingTop: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listButtonSelectedRight: {
        backgroundColor: '#3fa9f5',
        flexDirection: 'row',
        height: responsiveHeight(4),
        textAlign: 'left',
        width: responsiveWidth(33),
        paddingTop: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textoB: {
        fontSize: responsiveFontSize(1.6),
        color: 'white'
    },
    textoN: {
        fontSize: responsiveFontSize(1.6),
        color: 'black'
    },
    listButtonAsunto: {
        width: responsiveWidth(96),
        paddingTop: 10,
        paddingBottom: 4,
        borderRadius: 6,
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        borderColor: 'lightgray',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleBottom: {
        fontSize: responsiveFontSize(2.5),
        color: 'red',
        fontWeight: '600',
        paddingTop: 15,
        width: responsiveWidth(94),
        textAlign: 'left',
        alignItems: 'center'
    },
    title: {
        fontSize: responsiveFontSize(2.5),
        color: '#263238',
        fontWeight: '600',
        paddingTop: 15,
        width: responsiveWidth(94),
        textAlign: 'left',
        alignItems: 'center'
    },
    titulosContainer: {
        width: responsiveWidth(94),
        borderWidth: 1,
        borderRadius: 6,
        height: responsiveHeight(20),
        padding: 5
    },
    contAlumnos: {
        height: responsiveHeight(20),
        width: responsiveWidth(94)
    },
    row: {
        paddingTop: 10,
        flexDirection: 'row',
        width: responsiveWidth(95),
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    col1: {
        width: responsiveWidth(30)
    },
    col2: {
        width: responsiveWidth(30)
    },
    centeredTxt: {
        textAlign: 'center',
        fontWeight: '700'
    },
    rounded: {
        color: 'black',
        textAlign: 'center',
        fontWeight: '700'
    },
    requerido: {
        backgroundColor: 'white',
        borderRadius: 6,
        height: 30,
        width: responsiveWidth(30),
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        marginTop: 10
    },
    realizado: {
        backgroundColor: 'white',
        borderRadius: 6,
        height: 30,
        borderWidth: 1,
        marginTop: 10,
        width: responsiveWidth(30),

        justifyContent: 'center',
        alignItems: 'center'
    },
    pendiente: {
        backgroundColor: 'white',
        borderRadius: 6,
        height: 30,
        width: responsiveWidth(30),
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        marginTop: 10
    },
    cargos: {
        flexDirection: 'row',
        width: responsiveWidth(90),
        alignItems: 'stretch'
    },
    subTitleMain: {
        marginTop: 20,
        marginBottom: 10,
        fontWeight: '700'
    },
    listButtonSmallRight: {
        backgroundColor: 'white',
        borderColor: 'lightgray',
        paddingVertical: 5,
        paddingHorizontal: 25,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
