import React from "react";
import {
    AsyncStorage,
    ScrollView,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from "react-native";
import ModalSelector from "react-native-modal-selector";
import {
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../../../styles";
import Modal from "react-native-modal";
import GradoyGrupo from "../../Globales/GradoyGrupo";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class pagosVencidos extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            textInputValue: "",
            isDateTimePickerVisible: false,
            elMes: "",
            cososPicker: [],
            grados: [],
            grupos: [],
            elGrado: "",
            mes: moment().month() + 1,
            elGrupo: "",
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selectedIndex: -1,
            elAlumno: "",
            alumnos: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getMeses();
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            elGrupo: grupo
        });
        await this.getAlumnos();
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            elGrado: grado
        });
    }

    //+++++++++++++++++++++++++++++ MESES ++++++++++++++++++++++++++++++++++++++
    async onChange(option) {
        await this.setState({textInputValue: option.label, elMes: option.mes});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + "/api/config/extra/meses/");
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    // ++++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++++++
    async getAlumnos() {
        let alumnoList = await fetch(
            this.state.uri +
            "/api/user/alumnos/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        alumnosList = await alumnoList.json();
        this.setState({alumnos: alumnosList});
    }

    onListPressedAlumno(indexAlumno, alumno) {
        this.setState({selectedIndexAlumno: indexAlumno, elAlumno: alumno});
        this.getCargos();
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [
            styles.rowTabla,
            styles.modalWidth,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno == indexAlumno) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={[tabRow, smallButtonStyles]}
                onPress={() =>
                    this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.id)
                }
            >
                <View>
                    {this.state.isModalalumno === true
                        ? this.isValor(itemAlumno, indexAlumno)
                        : null}
                </View>
            </TouchableHighlight>
        );
    }

    isValor(itemAlumno, indexAlumno) {
        return (
            <View style={[styles.campoTablaG, {alignItems: "center"}]}>
                <View>
                    <Text style={{marginRight: 25}}>{itemAlumno.name}</Text>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async getCargos() {
        let request = await fetch(
            this.state.uri + "/api/payments/charge/return/all/" + this.state.mes,
            {
                method: "GET",
                headers: {
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let data = await request.json();
        this.setState({cargos: data});
    }

    async onListItemPressedPago(index, item) {
        await this.setState({selectedIndexGrupo: index, selItem: item});
    }

    render() {
        var data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        const alumnos = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.isModalalumno}
                    backdropOpacity={0.5}
                    animationIn={"bounceIn"}
                    animationOut={"bounceOut"}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de alumnos
                            </Text>
                            <View
                                style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}
                            />
                            <View
                                style={[
                                    styles.tabla,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}
                            >
                                {alumnos}
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor
                                }
                            ]}
                            onPress={() => this.setState({isModalalumno: false})}
                        >
                            <Text style={styles.textButton}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text
                        style={[
                            styles.main_title,
                            {color: this.state.thirdColor, marginBottom: 1}
                        ]}
                    >
                        Elija el periodo a consultar
                    </Text>
                    <ModalSelector
                        data={data}
                        selectStyle={[
                            [styles.inputPicker, {borderColor: this.state.secondColor}],
                            {marginTop: 2}
                        ]}
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        initValue={mess}
                        onChange={option => this.onChange(option)}
                    />
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <View style={{alignItems: "center"}}>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.btnLista,
                                {borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.setState({isModalalumno: true})}
                        >
                            <Text style={[styles.textButton, {color: "black"}]}>
                                Ver lista
                            </Text>
                        </TouchableOpacity>
                        <Text style={{marginTop: 10}}>Alumno seleccionado</Text>
                        <Text style={[styles.textButton, {color: "black"}]}>
                            {this.state.elAlumno}
                        </Text>
                    </View>
                    <View style={{width: responsiveWidth(95)}}>
                        <Text
                            style={[
                                styles.main_title,
                                {color: this.state.thirdColor, marginTop: 10}
                            ]}
                        >
                            Lista de facturas generadas
                        </Text>
                    </View>
                    <View>
                        <View style={styles.cargos}>
                            <Text style={{marginLeft: 5, marginTop: 5, fontWeight: "700"}}>
                                Concepto
                            </Text>
                            <Text style={{marginLeft: 90, marginTop: 5, fontWeight: "700"}}>
                                Importe
                            </Text>
                            <Text
                                style={{
                                    marginLeft: 70,
                                    marginTop: 5,
                                    fontWeight: "700",
                                    textAlign: "center"
                                }}
                            >
                                Recargo
                            </Text>
                        </View>
                        <View
                            style={{
                                height: responsiveHeight(23),
                                borderBottomWidth: 1,
                                borderTopWidth: 1,
                                marginBottom: 5
                            }}
                        >
                            <ScrollView showsVerticalScrollIndicator={false}/>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
