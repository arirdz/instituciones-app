import React from 'react';
import {
    AsyncStorage, ScrollView, Text, TouchableOpacity, View
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {
    responsiveHeight, responsiveWidth, responsiveFontSize
} from 'react-native-responsive-dimensions';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import GradoyGrupo from '../../Globales/GradoyGrupo';
import styles from '../../../styles';
import Modal from 'react-native-modal';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class consultarSaldos extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            alumnos: [],
            cargos: [],
            cososPicker: [],
            elAlumno: '',
            elCargo: '',
            elGrado: '',
            elGrupo: '',
            elMes: '',
            grados: [],
            grupos: [],
            isDateTimePickerVisible: false,
            mes: moment().month() + 1,
            selectedIndex: -1,
            selectedIndexAlumno: -1,
            selectedIndexGrado: -1,
            selectedIndexGrupo: -1,
            selectedIndexGrupos: -1,
            selectedIndexLista: -1,
            elTaller: "",
            textInputValue: ''
        };
        // this.renderPagos = this.renderPagos.bind(this);
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getMeses();
    }

    async onListItemPressedLista(indexLista, taller) {
        await this.setState({
            selectedIndexLista: indexLista,
            elTaller: taller,
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo, elGrupo: grupo
        });

        await this.getCargos();
    }

    async onListItemPressedGrado(indexGrado, grado) {
        this.setState({
            selectedIndexGrado: indexGrado, elGrado: grado
        });
        if (this.state.selectedIndexGrupos !== -1) {
            await this.getCargos();
        }
    }

    //++++++++++++++++++++++++ MESES +++++++++++++++++++++
    async onChange(option) {
        await this.setState({textInputValue: option.label, elMes: option.mes});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async getCargos() {
        let request = await fetch(this.state.uri + '/api/payments/saldos/' + this.state.elGrado + '/' + this.state.elGrupo + '/' + this.state.mes, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let data = await request.json();
        await this.setState({cargos: data});
    }

    async onListItemPressedPago(indexPago, itemPago) {
        await this.setState({
            selectedIndexGrupo: index, selItem: itemPago
        });
    }

    async sendNotification(item) {
        try {
            let formData = new FormData();
            formData.append('new', JSON.stringify({
                data: {
                    cargo: item.charge.descripcion,
                    fecha: moment(item.charge.fecha_limite_pago).format('DD/MM/YY'),
                    importe: item.charge.importe_a_pagar,
                    id: item.faltantes
                }
            }));
            let request = await fetch(this.state.uri + '/api/recordatorio/pagos', {
                method: 'POST', headers: {
                    'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
                }, body: formData
            });
        } catch (e) {
        }
    }

    renderPago(itemPago, indexPago) {
        let alumnos = [];

        itemPago.faltantes.forEach((item, index) => {
            alumnos.push((<View>
                <View
                    style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}
                ><Text>{item.nombre}</Text></View>
                <View
                    style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}
                />
            </View>));
        });
        return (<View
            key={indexPago}
            style={[styles.row, {width: responsiveWidth(94), marginTop: 4}]}>
            <Modal
                isVisible={this.state.isModalalumno}
                backdropOpacity={0.5}
                animationIn={'bounceIn'}
                animationOut={'bounceOut'}
                animationInTiming={1000}
                animationOutTiming={1000}>
                <View style={[styles.container, {borderRadius: 6}]}>
                    <ScrollView>
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>
                            Concepto del pago:
                        </Text>
                        <Text
                            style={{
                                textAlign: 'center', marginTop: 10, fontWeight: '600', fontSize: responsiveFontSize(2.5)
                            }}>
                            {this.state.elCargo}
                        </Text>
                        <Text style={[styles.main_title, styles.modalWidth]}>
                            Lista de alumnos:
                        </Text>
                        {alumnos}

                    </ScrollView>
                    <TouchableOpacity
                        style={[styles.btn2, styles.row, {
                            height: responsiveHeight(5),
                            backgroundColor: 'red',
                            borderRadius: 6,
                            marginTop: 3,
                            justifyContent: 'flex-end',
                            alignItems: 'center'
                        }]}
                        onPress={() => this.sendNotification(itemPago)}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(2), marginRight: 3, color: 'white'
                            }}>
                            Notificar pendientes
                        </Text>
                        <View style={{marginRight: 5}}>
                            <MaterialCommunityIcons
                                name="alert-circle-outline"
                                size={15}
                                color="white"
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.bigButton, styles.btn9, {
                            borderBottomLeftRadius: 6,
                            borderBottomRightRadius: 6,
                            backgroundColor: this.state.mainColor,
                            borderColor: this.state.mainColor
                        }]}
                        onPress={() => this.setState({
                            isModalalumno: false
                        })}>
                        <Text style={styles.textButton}>Cerrar</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
            <View style={styles.btn2}>
                <Text numberOfLines={1}>{itemPago.charge.descripcion}</Text>
            </View>
            <TouchableOpacity
                style={[styles.btn3_5_l, styles.row, {
                    height: responsiveHeight(3.75),
                    backgroundColor: 'red',
                    borderRadius: 6,
                    marginTop: 3,
                    justifyContent: 'flex-end',
                    alignItems: 'center'
                }]}
                onPress={() => this.setState({
                    isModalalumno: true, elCargo: itemPago.charge.descripcion
                })}>
                <Text
                    style={{
                        fontSize: responsiveFontSize(1.25), marginRight: 3, color: 'white'
                    }}>
                    ${Number(itemPago.pendiente).toLocaleString('en-US')}
                </Text>
                <View style={{marginRight: 5}}>
                    <MaterialCommunityIcons
                        name="alert-circle-outline"
                        size={14}
                        color="white"
                    />
                </View>
            </TouchableOpacity>
        </View>);
    }

    renderPagos() {
        let buttonsPagos = [];
        this.state.cargos.forEach((itemPago, indexPago) => {
            buttonsPagos.push(this.renderPago(itemPago, indexPago));
        });
        return buttonsPagos;
    }

    render() {
        const listaPagos = this.renderPagos();
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (<View style={styles.container}>
            <Text
                style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>
                Elija el periodo a consultar
            </Text>
            <ModalSelector
                data={data}
                selectStyle={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 2}]}
                cancelText="Cancelar"
                optionTextStyle={{color: this.state.thirdColor}}
                initValue={mess}
                onChange={option => this.onChange(option)}
            />
            <GradoyGrupo
                onListItemPressedGrupos={(indexGrupo, grupo) =>
                    this.onListItemPressedGrupos(indexGrupo, grupo)
                }
                onListItemPressedGrado={(indexGrado, grado) =>
                    this.onListItemPressedGrado(indexGrado, grado)
                }
                onListItemPressedLista={(indexLista, taller) =>
                    this.onListItemPressedLista(indexLista, taller)
                }
                listaVar={true}
                todos={"1"}
            />
            <View style={{width: responsiveWidth(94)}}>
                <Text
                    style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 5, marginTop: 6}]}>
                    Seleccione un cargo a consultar
                </Text>
            </View>
            <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                <View style={styles.btn8}>
                    <Text style={{fontWeight: '700'}}>Concepto</Text>
                </View>
                <View style={styles.btn6}>
                    <Text style={{fontWeight: '700', marginLeft: 24}}> </Text>
                </View>
                <View style={styles.btn3_5_l}>
                    <Text style={{fontWeight: '700', marginLeft: 15}}>Pendiente</Text>
                </View>
            </View>
            <View
                style={{
                    paddingHorizontal: 10,
                    height: responsiveHeight(30),
                    borderBottomWidth: 1,
                    borderTopWidth: 1,
                    marginBottom: 5,
                    borderColor: this.state.fourthColor
                }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {listaPagos}
                </ScrollView>
            </View>
        </View>);
    }
}
