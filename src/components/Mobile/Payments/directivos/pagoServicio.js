import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import {Actions} from 'react-native-router-flux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';

const moment = require('moment');

require('moment/locale/es');
moment.locale('es');


export default class pagoServicio extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			tipoPago: [
				{'tipo_pago': 'tarjeta', 'titulo': 'Pago con tarjeta', 'descripcion': 'Visa, Mastercard o Amex.'},
				{'tipo_pago': 'tienda', 'titulo': 'Pago en efectivo', 'descripcion': 'En tiendas de conveniencia.'},
				{
					'tipo_pago': 'transfer',
					'titulo': 'Pago con transferencia electronica',
					'descripcion': 'Desde el portal de su banco.'
				}
			],
			elPago: '',
			indexSelectedPago: -1,
			montos: [],
			inscrip: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
		console.log(this.props.user_data)
        console.log(this.props.pago)
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async onSelectedPressPago(it, ix) {
		await this.setState({
			elPago: it,
			indexSelectedPago: ix
		});
        AsyncStorage.setItem('itemid', this.props.pago.id.toString());
        AsyncStorage.setItem('itemTotalToPay', this.props.pago.monto.toString());
        AsyncStorage.setItem('itemHijoId', 'xxx');
        AsyncStorage.setItem('itemDescripcion', 'Pago de servicio control escolar '+ moment(this.props.pago.mes).format("MMMM YYYY"));
        AsyncStorage.setItem('item_due_date', this.props.pago.mes);
        AsyncStorage.setItem('userID', this.props.user_data.id.toString());
		if (ix === 0) {
			Alert.alert('Realizar cargo',
				'Esta a punto de generar un cargo por pago con tarjeta\n¿Seguro que desea continuar?', [
					{
						text: 'Sí',
						onPress: () => Actions.pagoTarjetaServicio({
                            FinalPrice: this.props.pago.monto
						})
					},
					{text: 'No'}
				]);
		} else if (ix === 1) {
			Alert.alert('Realizar cargo',
				'Esta a punto de generar un cargo por pago en tienda\n¿Seguro que desea continuar?', [
					{
						text: 'Sí',
						onPress: () => Actions.pagoEfectivoP({
							elAspirante: aspirante,
							montoInscrip: monto,
							paso: '5',
							tipo: 'inscrip'
						})
					}, {text: 'No'}
				]);
		} else if (ix === 2) {
			Alert.alert('Realizar cargo',
				'Esta a punto de generar un cargo por transferencia electronica\n¿Seguro que desea continuar?', [
					{text: 'Sí', onPress: () => this.alertTransfer()}, {text: 'No'}
				]);
		}
		await this.setState({aux: 0});
	}

	async alertTransfer(aspirante) {
		Alert.alert('Pago por transferencia electronica', 'A continuacion se mostrara las instrucciones para la transferencia bancaria en el pago ', [
			{text: 'Continuar', onPress: () => this.transferencia(this.props.elAspirante.tutor)}, {text: 'Cancelar'}
		]);
	}


	async transferencia(tutor) {
		let formData = new FormData();
		formData.append('parameters', JSON.stringify({
			method: 'bank_account',
			amount: Number(this.state.inscrip),
			description: 'Pago de preinscripción Liceo Animas',
			customer: {
				name: tutor.nombre,
				last_name: tutor.apellidos,
				phone_number: tutor.telefono_contacto,
				email: tutor.email
			}
		}));
		await fetch(this.state.uri + '/api/pago/transfer/openpay/preinscrip', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data',
			}, body: formData
		}).then(response => response.json())
			.then(responseJson => {
				if (responseJson.response !== 'error') {
					this.updateAspirante(this.props.elAspirante);
					Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
						text: 'Gracias',
						onPress: () => Actions.reciboTransferencia({
							reciboTienda: responseJson.result.serializableData,
							transaction_id: responseJson.transaction_id,
							user: tutor.email
						})
					}]);
					this.setState({recibo: responseJson});
				} else {
					if (responseJson.error.code === 3001) {
						Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3002) {
						Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirado', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3003) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3004) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3005) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 1006) {
						Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
							text: 'Entendido'
						}]);
					} else {
						this.updateAspirante(this.props.elAspirante);
						Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
							text: 'Entendido', onPress: () => Actions.reciboTransferencia({
								reciboTienda: responseJson.result,
								transaction_id: responseJson.transaction_id,
								user: tutor.email
							})
						}]);
					}
				}
			}).catch(error => {
				Alert.alert('¡Ups!', 'No se pudo generar el cargo, intente más tarde', [{
					text: 'Gracias'
				}]);
			});
	}


	//updatepago

	rndTipoPagos() {
		let pagos = [];
		if (this.state.tipoPago.length !== 0) {
			this.state.tipoPago.forEach((it, ix) => {
				pagos.push(
					<TouchableOpacity
						key={ix+'ass'}
						style={[styles.widthall, styles.carDocumentos, styles.row,
							{
								backgroundColor: this.state.indexSelectedPago === ix ? this.state.secondColor : this.state.fourthColor,
								paddingHorizontal: 5,
								marginVertical: 5,
								borderRadius: 10
							}]}
						onPress={() => this.onSelectedPressPago(it, ix, this.props.elAspirante, this.state.inscrip)}
					>
						<View
							style={{
								borderRightWidth: 1,
								borderColor: 'lightgray',
								alignItems: 'center',
								paddingVertical: 15,
								width: responsiveWidth(18),
								paddingHorizontal: 5
							}}
						>
							{it.tipo_pago === 'tarjeta' ?
								<FontAwesome name='credit-card' size={35}
											 color={this.state.indexSelectedPago === ix ? '#fff' : '#000'}/> :
								it.tipo_pago === 'tienda' ?
									<FontAwesome5 name='store' size={31}
												  color={this.state.indexSelectedPago === ix ? '#fff' : '#000'}/> :
									it.tipo_pago === 'transfer' ?
										<Entypo name='laptop' size={37}
												color={this.state.indexSelectedPago === ix ? '#fff' : '#000'}/> : null}
						</View>
						<View style={{width: responsiveWidth(82), alignItems: 'flex-start', paddingLeft: 10}}>
							<Text
								style={[styles.textW, {color: this.state.indexSelectedPago === ix ? '#fff' : '#000'}]}
							>
								{it.titulo}
							</Text>
							<Text
								style={{color: this.state.indexSelectedPago === ix ? '#fff' : '#000'}}>{it.descripcion}</Text>
						</View>
					</TouchableOpacity>
				)
			});
		}
		return pagos;
	}


	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Datos del pago
				</Text>
				<View style={{marginVertical: 20}}>
					<View style={[styles.row, styles.modalWidth, {marginTop: 10}]}>
						<Text style={[styles.btn8, {textAlign: 'center', fontWeight: '700'}]}>Pago de Servicio Control Escolar</Text>
						<Text style={[styles.btn8, {textAlign: 'center', fontWeight: '700'}]}>Importe</Text>
					</View>
					<View style={[styles.row, styles.modalWidth]}>
						<View style={[
							styles.modalBigBtn,
							{
								borderWidth: 0,
								backgroundColor: this.state.fourthColor,
								marginTop: 3,
								marginVertical: 0
							}
						]}
						>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								lineHeight: 12,
								textAlign: 'center',
                                textTransform: 'uppercase'
							}}>
                                {moment(this.props.pago.mes).format("MMMM YYYY")}
							</Text>
						</View>
						<View style={[styles.modalBigBtn, {
							borderWidth: 0,
							backgroundColor: this.state.fourthColor,
							marginTop: 3,
							marginVertical: 0
						}]}
						>
							<Text>${this.props.pago.monto}</Text>
						</View>
					</View>
				</View>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione un metodo de pago
				</Text>
				{this.rndTipoPagos()}
			</View>
		);
	}
}
