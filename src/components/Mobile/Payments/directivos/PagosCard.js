import React from "react";
import {
    AsyncStorage,
    Text,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    ScrollView,
    View
} from "react-native";
import {
    responsiveHeight,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import styles from "../../../styles";
import Swipeable from "react-native-swipeable";
import Modal from "react-native-modal";

export default class PagosCard extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            enterados: "",
            isDateTimePickerVisible: false,
            laCard: [
                {
                    nombre: "Mario Ricardo Vazquez Petronilo",
                    concepto: "Colegiatura de Enero",
                    pago: "8,400.00",
                    metodo: "card",
                    tienda: "Banamex"
                },
                {
                    nombre: "Luis Fernando Gutierrez Avelardo",
                    concepto: "Uniforme deportivo",
                    pago: "7,010.00",
                    metodo: "store",
                    tienda: "Farmacias del ahorro"
                },
                {
                    nombre: "Laura Josefina Calandria Ibañez",
                    concepto: "Excurcion a six-flag",
                    pago: "1,234.00",
                    metodo: "store",
                    tienda: "Aurrera"
                },
                {
                    nombre: "Pedro Infante Zamora Santos",
                    concepto: "Tardeada para los graduados",
                    pago: ",600.00",
                    metodo: "card",
                    tienda: "Banorte"
                },
                {
                    nombre: "Olaf Partida Luna",
                    concepto: "remodelacion del salon",
                    pago: "1,500.00",
                    metodo: "shop",
                    tienda: "Farmacias Guadalajara"
                },
                {
                    nombre: "Ana Karen Lozada Carmona",
                    concepto: "Material didactico",
                    pago: "300.00",
                    metodo: "card",
                    tienda: "Santander"
                },
                {
                    nombre: "Inez de la Cruz Montero Perez",
                    concepto: "Pago de uniforme de gala",
                    pago: "6,000.00",
                    metodo: "card",
                    tienda: "Bancomer"
                },
                {
                    nombre: "Sandra Fernandez Hurtado",
                    concepto: "Compra de equipo para excurcion",
                    pago: "3,000.00",
                    metodo: "shop",
                    tienda: "Miscelanea"
                }
            ]
        };
    }

    async getURL() {
        let auth = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: auth,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.rendercards();
    }

    rendercard(item, index) {
        const rightButtons = [
            <TouchableHighlight
                underlayColor={"lightgray"}
                style={[styles.btnSwip, {backgroundColor: this.state.fourthColor}]}
                onPress={() =>
                    this.setState({
                        isModalalumno: true,
                        modPago: item
                    })
                }
            >
                <Text
                    style={{
                        color: this.state.thirdColor,
                        fontSize: responsiveFontSize(1.5)
                    }}
                >
                    Detalles
                </Text>
            </TouchableHighlight>,
            <TouchableHighlight
                style={[
                    styles.btnSwip,
                    {
                        backgroundColor: this.state.fourthColor,
                        borderColor: "lightgray",
                        borderLeftWidth: 1
                    }
                ]}
            >
                <Text
                    style={{
                        color: this.state.thirdColor,
                        fontSize: responsiveFontSize(1.5)
                    }}
                >
                    Archivar
                </Text>
            </TouchableHighlight>
        ];
        return (
            <View key={index} style={styles.widthall}>
                <Swipeable rightButtons={rightButtons}>
                    <View style={{marginTop: 5}}>
                        <Text style={styles.agoText}>hace 45 min</Text>
                    </View>
                    <View
                        style={[styles.cardPago, {borderColor: this.state.secondColor}]}
                    >
                        <View style={styles.rowsCalif}>
                            <View style={styles.contIcon}>
                                {this.state.laCard[index].metodo === "card" ? (
                                    <FontAwesome
                                        name="credit-card"
                                        size={20}
                                        color={this.state.secondColor}
                                    />
                                ) : (
                                    <Entypo
                                        name="shop"
                                        size={20}
                                        color={this.state.secondColor}
                                    />
                                )}
                            </View>
                            <View>
                                <View style={[styles.row, styles.contPago, {marginTop: 2}]}>
                                    <View>
                                        <Text style={styles.textGris}>Nombre del alumno</Text>
                                        <Text style={styles.textPago}>{item.nombre}</Text>
                                    </View>
                                    <View style={styles.gradoPago}>
                                        <Text style={styles.textGris}>Grado y Grupo</Text>
                                        <Text style={styles.textPago}>3 C</Text>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        styles.row,
                                        styles.contPago,
                                        {marginBottom: 1, marginTop: -4}
                                    ]}
                                >
                                    <View>
                                        <Text style={styles.textGris}>Concepto del pago</Text>
                                        <Text style={styles.textPago}>Colegiatura de Febrero</Text>
                                    </View>
                                    <View style={styles.gradoPago}>
                                        <Text style={styles.textGris}>Importe</Text>
                                        <Text style={styles.textPago}>${item.pago}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </Swipeable>
            </View>
        );
    }

    renderCards() {
        let btnCard = [];
        this.state.laCard.forEach((item, index) => {
            btnCard.push(this.rendercard(item, index));
        });
        return btnCard;
    }

    render() {
        const card = this.renderCards();
        return (
            <View>
                <Modal
                    isVisible={this.state.isModalalumno}
                    backdropOpacity={0.5}
                    animationIn={"bounceIn"}
                    animationOut={"bounceOut"}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={{height: responsiveHeight(57)}}>
                        <View style={[styles.container, {borderRadius: 6}]}>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Detalles del pago
                            </Text>
                            <Text
                                style={{
                                    textAlign: "center",
                                    fontSize: responsiveFontSize(1.25),
                                    marginTop: 10
                                }}
                            >
                                Pago recibido por concepto de:
                            </Text>
                            <Text style={{textAlign: "center", marginTop: 2}}>
                                {this.state.modPago.concepto}
                            </Text>
                            <Text
                                style={{
                                    textAlign: "center",
                                    fontSize: responsiveFontSize(1.25),
                                    marginTop: 13
                                }}
                            >
                                Correspondiente al alumno:
                            </Text>
                            <Text
                                style={{textAlign: "center", fontWeight: "700", marginTop: 2}}
                            >
                                {this.state.modPago.nombre}
                            </Text>
                            <Text
                                style={{
                                    textAlign: "center",
                                    fontSize: responsiveFontSize(1.25),
                                    marginTop: 13
                                }}
                            >
                                El importe de pago fue de:
                            </Text>
                            <Text style={{textAlign: "center", marginTop: 2}}>
                                ${this.state.modPago.pago}
                            </Text>
                            <Text
                                style={{
                                    textAlign: "center",
                                    fontSize: responsiveFontSize(1.25),
                                    marginTop: 13
                                }}
                            >
                                El pago fue realizado por el usuario:
                            </Text>
                            <Text style={{textAlign: "center", marginTop: 2}}>
                                email_email@hotmail.com
                            </Text>
                            <Text
                                style={{
                                    textAlign: "center",
                                    fontSize: responsiveFontSize(1.25),
                                    marginTop: 13
                                }}
                            >
                                El pago fue realizado por el usuario:
                            </Text>
                            <Text style={{textAlign: "center", marginTop: 2}}>
                                21/01/2018 16:34hrs
                            </Text>
                            <Text
                                style={{
                                    textAlign: "center",
                                    fontSize: responsiveFontSize(1.25),
                                    marginTop: 13
                                }}
                            >
                                Lugar y metodo de pago:
                            </Text>
                            <View
                                style={[
                                    styles.btn3,
                                    {
                                        height: responsiveHeight(7),
                                        borderRadius: 6,
                                        marginTop: 2,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }
                                ]}
                            >
                                <Image
                                    style={[styles.imagen, {height: 200, width: 100}]}
                                    source={require("../../../../images/bancos/CITIBANAMEX.png")}
                                />
                            </View>
                            <TouchableOpacity
                                style={[
                                    styles.bigButton,
                                    styles.btn9,
                                    {
                                        marginRight: 1,
                                        borderBottomLeftRadius: 6,
                                        borderBottomRightRadius: 6,
                                        backgroundColor: this.state.mainColor,
                                        borderColor: this.state.mainColor
                                    }
                                ]}
                                onPress={() =>
                                    this.setState({
                                        isModalalumno: false
                                    })
                                }
                            >
                                <Text style={styles.textButton}>Cerrar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <ScrollView style={{marginBottom: 100}}>{card}</ScrollView>
            </View>
        );
    }
}
