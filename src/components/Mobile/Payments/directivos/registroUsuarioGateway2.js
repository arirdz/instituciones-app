import React from 'react';
import {
    Alert, AsyncStorage, KeyboardAvoidingView, Platform, ScrollView, StatusBar, Text, TouchableOpacity, View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../../styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Fumi} from 'react-native-textinput-effects';
import MultiBotonRow from '../../Globales/MultiBotonRow';
import Modal from 'react-native-modal';
import Feather from 'react-native-vector-icons/Feather';
import {responsiveHeight} from 'react-native-responsive-dimensions';

export default class registroDeUsuarioGateway2 extends React.Component {
    _showModal = () => this.setState({isModalVisible: true});

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            cososPicker: [],
            datos: [],
            isModalVisible: false,
            botonSelected: 'Generales',
            maincolor: '#fff',
            secondColor: '#fff',
            thirdColor: '#fff',
            fourthColor: '#fff',
            datoFaltantes: []
        };
    }

    async requestMultipart() {
        let formData = new FormData();
        formData.append('parameters', JSON.stringify({
            external_id: this.state.datos.user_id,
            name: this.state.datos.nombre,
            last_name: this.state.datos.apellido_paterno + ' ' + this.state.datos.apellido_materno,
            email: this.state.datos.email_tutor,
            requires_account: false,
            address: {
                line1: this.state.datos.calle + '' + this.state.datos.numero,
                line2: this.state.datos.colonia,
                state: this.state.datos.estado,
                city: this.state.datos.ciudad,
                postal_code: this.state.datos.cp,
                country_code: 'MX'
            }
        }));
        await fetch(this.state.uri + '/api/gateways/v1/openpay/customer', {
            method: 'POST', headers: {
                'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(response => response.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte', [{
                        text: 'Entendido', onPress: () => Actions.refresh()
                    }]);
                } else {
                    this.setState({data: responseJson});
                    Alert.alert('¡Felicidades!',
                        'Se ha modificado tus datos personales',
                        [{
                            text: 'Entendido', onPress: () => Actions.reset('drawer')
                        }]);

                }
            });

    }

    async getURL() {
        this.setState({
            uri: await AsyncStorage.getItem('uri'),
            token: await AsyncStorage.getItem('token'),
            mainColor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor')
        });
        await this.getRoles();
        await this.getUserdata();
        await this.getUserdataV2();
    }

    async componentWillMount() {
        await this.getURL();
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected, indexSelected: indexSelected
        });
    }

    async getUserdataV2() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        await fetch(uri + '/api/user/data/v2', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error ' +
                        // responseJson.error.status_code +
                        ' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 4)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({datosV2: responseJson});
                }
            });
    }


    async requestMultipart1() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let formData = new FormData();
        formData.append(
            'update',
            JSON.stringify({
                user_id: this.state.datosV2.id,
                nombre: this.state.datos.nombre,
                apellido_paterno: this.state.datos.apellido_paterno,
                apellido_materno: this.state.datos.apellido_materno,
                parentesco: this.state.datos.parentesco,
                calle: this.state.datos.calle,
                numero: this.state.datos.numero,
                colonia: this.state.datos.colonia,
                ciudad: this.state.datos.ciudad,
                estado: this.state.datos.estado,
                cp: this.state.datos.cp
            })
        );
        let reg1 = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let reg2 = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.datos.nombre === null || this.state.datos.nombre === '' || this.state.datos.nombre === undefined) {
            this.state.datoFaltantes.push('Nombre');
        }
        if (this.state.datos.apellido_paterno === null || this.state.datos.apellido_paterno === '') {
            this.state.datoFaltantes.push('Apellido Paterno');
        }
        if (this.state.datos.apellido_materno === null || this.state.datos.apellido_materno === '') {
            this.state.datoFaltantes.push('Apellido Materno');
        }
        if (this.state.datos.calle === null || this.state.datos.calle === '') {
            this.state.datoFaltantes.push('Calle');
        }
        if (this.state.datos.numero === null || this.state.datos.numero === '') {
            this.state.datoFaltantes.push('Número de Calle');
        }
        if (this.state.datos.colonia === null || this.state.datos.colonia === '') {
            this.state.datoFaltantes.push('Colonia');
        }
        if (this.state.datos.ciudad === null || this.state.datos.ciudad === '') {
            this.state.datoFaltantes.push('Ciudad');
        }
        if (this.state.datos.estado === null || this.state.datos.estado === '') {
            this.state.datoFaltantes.push('Estado');
        }
        if (this.state.datos.cp === null || this.state.datos.cp === '') {
            this.state.datoFaltantes.push('Código Postal');
        } else if (this.state.datos.cp.length < 5) {
            this.state.datoFaltantes.push('Código Postal incompleto');
        }
        if (this.state.datoFaltantes.length > 0) {
            Alert.alert('Datos incompletos',
                'Aún faltan datos que son necesarios tales como, ' + this.state.datoFaltantes,
                [{
                    text: 'Entendido', onPress: () => this.setState({datoFaltantes: []})
                }]);
        } else {
            await fetch(uri + '/api/user/update', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: 'Bearer ' + token
                },
                body: formData
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert('¡Ups!',
                            'Ha ocurrido un error '
                            +
                            responseJson.error.status_code
                            +
                            ' si el error continua pónganse en contacto con soporte (Cod. 5)',
                            [{
                                text: 'Entendido'
                            }]);
                    } else {
                        this.requestMultipart();

                    }
                });
        }
    }

    async getRoles() {
        let uri = await AsyncStorage.getItem('uri');
        await fetch(uri + '/api/get/parentezcos').then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 6)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({cososPicker: responseJson});
                }
            });
    }

    async getUserdata() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        await fetch(uri + '/api/user/data', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos',
                        'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 7)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({datos: responseJson});
                }
            });
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    datosPersonales() {
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, id: item.id, label: item.parentezco};
        });
        return (<View style={styles.datosBasicos}>
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                Datos personales
            </Text>
            <Fumi
                label={'Nombre(s)'}
                iconClass={Ionicons}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'md-person'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.nombre = text)}
                keyboardType='default'
                value={this.state.datos.nombre}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onSubmitEditing={() => this.refs.apellidoPat.focus()}
            />
            <Fumi
                ref='apellidoPat'
                label={'Apellido paterno'}
                iconClass={Ionicons}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'md-person'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.apellido_paterno = text)}
                keyboardType='default'
                value={this.state.datos.apellido_paterno}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onSubmitEditing={() => this.refs.apellidoMat.focus()}
            />
            <Fumi
                ref='apellidoMat'
                label={'Apellido materno'}
                iconClass={Ionicons}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'md-person'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.apellido_materno = text)}
                keyboardType='default'
                value={this.state.datos.apellido_materno}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
            />
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                Domicilio
            </Text>
            <Fumi
                label={'Calle/Avenida'}
                iconClass={Entypo}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'address'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.calle = text)}
                keyboardType='default'
                value={this.state.datos.calle}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onSubmitEditing={() => this.refs.numeroCasa.focus()}
            />
            <Fumi
                ref='numeroCasa'
                label={'Número'}
                iconClass={FontAwesome}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'hashtag'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.numero = text)}
                keyboardType='numeric'
                value={this.state.datos.numero ? this.state.datos.numero.toString() : this.state.datos.numero}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onSubmitEditing={() => this.refs.colonia.focus()}
            />
            <Fumi
                ref='colonia'
                label={'Colonia'}
                iconClass={MaterialIcons}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'my-location'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.colonia = text)}
                keyboardType='default'
                value={this.state.datos.colonia}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onSubmitEditing={() => this.refs.ciudad.focus()}
            />
            <Fumi
                ref='ciudad'
                label={'Ciudad'}
                iconClass={MaterialIcons}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'location-city'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.ciudad = text)}
                keyboardType='default'
                value={this.state.datos.ciudad}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onSubmitEditing={() => this.refs.estado.focus()}
            />
            <Fumi
                ref='estado'
                label={'Estado'}
                iconClass={Entypo}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'location'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.estado = text)}
                keyboardType='default'
                value={this.state.datos.estado}
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onSubmitEditing={() => this.refs.codigoPost.focus()}
            />
            <Fumi
                ref='codigoPost'
                label={'Código Postal'}
                iconClass={FontAwesome}
                style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                labelStyle={{color: this.state.mainColor}}
                iconName={'location-arrow'}
                inputStyle={{color: this.state.secondColor}}
                iconColor={this.state.mainColor}
                iconSize={20}
                onChangeText={text => (this.state.datos.cp = text)}
                keyboardType='numeric'
                value={this.state.datos.cp ? this.state.datos.cp.toString() : this.state.datos.cp}
                returnKeyType='next'
                maxLength={5}
                autoCapitalize='none'
                autoCorrect={false}
            />
        </View>);
    }

    async onChange1(option) {
        await this.setState({
            parentesco_alternativo1: option.label
        });
    }

    async onChange2(option2) {
        await this.setState({
            parentesco_alternativo2: option2.label
        });
    }

    onGoFocus() {
        this._myTextInputMask.getElement().focus()
    }


    render() {
        return (<KeyboardAvoidingView
            style={styles.container}
            behavior={Platform.OS === 'ios' ? 'padding' : null}>
            {Platform.OS === 'ios' ?
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='dark-content'
                /> : <StatusBar
                    backgroundColor={this.state.secondColor}
                    barStyle='light-content'
                />}
            <View style={[styles.container, {marginTop: responsiveHeight(5)}]}>
                <Modal
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={styles.containerMod}>
                        <View style={styles.modalClaus}>
                            <Text style={styles.titleClausula}>
                                CLÁUSULA DE RESPONSABILIDAD PARA LA ACTUALIZACIÓN DE MIS
                                DATOS DE CONTACTO
                            </Text>
                            <Text style={styles.textClaus}>
                                Entiendo y acepto que es mi total responsabilidad mantener
                                mis datos de contacto actualizados. Deslindo a la
                                institución de cualquier responsabilidad derivada de la
                                imposibilidad de localizarme en el caso de una emergencia
                                debido a que no hubiese mantenido actualizada mi información
                                de contacto. Al guardar sus datos estará aceptando la
                                presente cláusula de responsabilidad.
                            </Text>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({isModalVisible: false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Enterado,
                                    ¡Gracias!</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <View style={[styles.row, styles.widthall, {marginTop: 15}]}>
                    <Feather style={{marginLeft: 360}} name='alert-circle' size={20} onPress={this._showModal}/>
                </View>
                {/*<MultiBotonRow*/}
                    {/*itemBtns={['Generales', 'Contacto', 'Alternativo']}*/}
                    {/*onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}*/}
                    {/*cantidad={3}/>*/}
                <ScrollView showsVerticalScrollIndicator={false}>
                    {this.datosPersonales()}
                    {/*{this.state.botonSelected === 'Generales' ? this.datosPersonales() : null}*/}
                    {/*{this.state.botonSelected === 'Contacto' ? this.contacto() : null}*/}
                    {/*{this.state.botonSelected === 'Alternativo' ? this.alternativo() : null}*/}
                    <TouchableOpacity
                        style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
                        onPress={() => this.requestMultipart1()}>
                        <Text
                            style={styles.textButton}>{this.state.datos.length > 0 ? 'Guardar' : 'Actualizar'}</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>

        </KeyboardAvoidingView>);
    }
}



