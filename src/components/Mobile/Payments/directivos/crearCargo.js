import React from 'react';
import {
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import {responsiveWidth} from 'react-native-responsive-dimensions';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../../styles';
import GradoyGrupo from '../../Globales/GradoyGrupo';
import {TextInputMask} from 'react-native-masked-text';
import Spinner from "react-native-loading-spinner-overlay";
import {Actions} from "react-native-router-flux";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class crearCargo extends React.Component {
    _showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});
    _hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});
    _handleDatePicked = date => {
        let fecha = moment(date).format('LL');
        let fechadb = moment(date).format('YYYY-MM-DD HH:mm:ss');
        this.setState({fecha1: fecha, datee: fechadb});
        this._hideDateTimePicker();
    };
    _keyExtractor = item => item.id;

    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            botonSelected: 'Crear cargo',
            cargoNuevo: false,
            cososPicker: [],
            data: [],
            date: '',
            datee: '',
            fecha: [],
            fecha1: '',
            fechadb: '',
            grado: '',
            descripcion: '',
            grados: [],
            grupo: '',
            grupos: [],
            isDateTimePickerVisible: false,
            itemName: '',
            selectedIndexLista: -1,
            elTaller: '',
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selectedIndexPayment: [],
            selectedIndexPeriodo: -1,
            verCargosGenerados: false,
            elImporte: '',
            visible: false,
            tipo: 'Colegiatura', selectedTipo: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let mainColor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: mainColor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getCargos();
        await this.getMeses();
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo, grupo: grupo
        });
    }

    async onListItemPressedLista(indexLista, taller) {
        await this.setState({
            selectedIndexLista: indexLista, elTaller: taller
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado, grado: grado
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected, indexSelected: indexSelected
        });

    }

    async enviarCargo() {
        await this._changeWheelState();
        if (this.state.datee === '') {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta seleccionar la fecha', [{
                text: 'Entendido'
            }]);
        }
        else if (this.state.elImporte === '') {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta agregar el importe', [{
                text: 'Entendido'
            }]);
        }
        else if (this.state.descripcion === '') {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta agregar descripcion', [{
                text: 'Entendido'
            }]);
        }
        else if (this.state.grado === '') {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta seleccionar grado', [{
                text: 'Entendido'
            }]);
        }
        else if (this.state.grupo === '') {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta seleccionar grupo', [{
                text: 'Entendido'
            }]);
        }
        else {
            await fetch(this.state.uri + '/api/payment/dos/crear/cargo', {
                method: 'POST', headers: {
                    Accept: 'application/json', 'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }, body: JSON.stringify({
                    tipo_pago: this.state.tipo,
                    fecha_limite_pago: this.state.datee,
                    importe_a_pagar: this.state.elImporte,
                    descripcion: this.state.descripcion,
                    grado: this.state.grado,
                    grupo: this.state.grupo
                })
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Crear Cargo)', [{
                            text: 'Entendido', onPress: () => [this.setState({visible: false}), Actions.HomePage()]
                        }]);
                    } else {
                        this.setState({idNoticia: responseJson.id});
                        Alert.alert('¡Felicidades!', 'Se ha publicado el cargo con éxito', [{
                            text: 'Entendido', onPress: () => [this.setState({visible: false}), Actions.HomePage()]
                        }]);
                    }

                });
        }
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, mes: option.mes});
        await this.getCargos();
    }

    async getCargos() {
        let request = await fetch(this.state.uri + '/api/payments/charge/return/all/' + this.state.mes, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let data = await request.json();
        this.setState({data: data});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async ponerImport(value) {
        let patron = /,/gi;
        let nuevoPatron = '';
        let importe1 = value.toString().replace(patron, nuevoPatron);
        let nuevoPatron1 = '';
        let importe2 = importe1.toString().replace('$', nuevoPatron1);
        await this.setState({elImporte: importe2});
    }

    onChange(option) {
        this.setState({tipo: option.value, selectedTipo: option.label})
    }

    render() {
        let i = 0;
        const data = [
            {key: i++, label: "Colegiatura", value: 'Colegiatura'},
            {key: i++, label: "Otro", value: 'Pago'}
        ];
        return (<View style={styles.container} behavior='position'>
            <StatusBar
                backgroundColor={this.state.mainColor}
                barStyle='light-content'
            />
            <KeyboardAvoidingView style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Spinner visible={this.state.visible} textContent='Procesando...'/>
                    <View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Fecha límite de pago
                        </Text>
                        <TouchableOpacity
                            style={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 3}]}
                            onPress={this._showDateTimePicker}>
                            {this.state.fecha1 === '' ? (<Text>Seleccione una fecha</Text>) : (
                                <Text>{this.state.fecha1}</Text>)}
                        </TouchableOpacity>
                    </View>
                    <DateTimePicker
                        locale={'es'}
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        titleIOS={'Seleccione una fecha'}
                        confirmTextIOS={'Seleccionar'}
                        cancelTextIOS={'Cancelar'}
                    />
                    <Text
                        style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Importe total a pagar
                    </Text>
                    <TextInputMask
                        type={'money'}
                        options={{
                            unit: '$',
                            separator: '.',
                            delimiter: ','
                        }}
                        style={{
                            paddingVertical: 14,
                            width: responsiveWidth(94),
                            borderColor: this.state.secondColor,
                            borderRadius: 6,
                            textAlign: 'center',
                            borderWidth: 1,
                            fontWeight: '600'
                        }}
                        placeholder={'1000'}
                        onChangeText={text => this.ponerImport(text)}
                        underlineColorAndroid={'transparent'}
                        value={this.state.elImporte}
                        keyboardType={'numeric'}
                        maxLength={10}
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Concepto
                    </Text>
                    <TextInput
                        style={{
                            width: responsiveWidth(94),
                            textAlign: 'auto',
                            paddingLeft: 5,
                            paddingVertical: 14,
                            borderWidth: 1,
                            backgroundColor: '#fff',
                            borderRadius: 6,
                            borderColor: this.state.secondColor,
                            textAlignVertical: 'top',
                        }}
                        placeholder={'Colegiatura del mes...'}
                        onChangeText={text => this.setState({descripcion: text})}
                        underlineColorAndroid={'transparent'}
                        maxLength={35}
                        numberOfLines={10}
                        value={this.state.descripcion}
                    />
                    <Text
                        style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>
                        Indique el tipo de cargo </Text>
                    <ModalSelector
                        data={data}
                        selectStyle={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 2}]}
                        cancelText='Cancelar'
                        optionTextStyle={{color: this.state.thirdColor}}
                        initValue='Colegiatura'
                        onChange={option => this.onChange(option)}
                    />
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexGrupo, grupo) => this.onListItemPressedGrupos(indexGrupo, grupo)}
                        onListItemPressedGrado={(indexGrado, grado) => this.onListItemPressedGrado(indexGrado, grado)}
                        onListItemPressedLista={(indexLista, taller) => this.onListItemPressedLista(indexLista, taller)}
                        listaVar={false}
                        todos={'1'}
                    />
                    <TouchableHighlight
                        underlayColor={this.state.mainColor}
                        style={[styles.bigButton, {
                            backgroundColor: this.state.secondColor, borderColor: this.state.secondColor
                        }]}
                        onPress={() => this.enviarCargo()}>
                        <Text style={styles.textButton}>Publicar cargo ahora</Text>
                    </TouchableHighlight>
                </ScrollView>
            </KeyboardAvoidingView>
        </View>);
    }
}
