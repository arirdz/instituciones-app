import React from 'react';
import {AsyncStorage, ScrollView, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';
import styles from '../../../styles';
import GradoyGrupo from '../../Globales/GradoyGrupo';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class pagosVencidosDir extends React.Component {
   constructor(props) {
	  super(props);
	  this.state = {
		 isDateTimePickerVisible: false,
		 textInputValue: '',
		 elMes: '',
		 cososPicker: [],
		 grados: [],
		 grupos: [],
		 elGrado: '',
		 mes: moment().month() + 1,
		 elGrupo: '',
		 cargos: [],
		 selectedIndexAlumno: -1,
		 selectedIndexGrados: -1,
		 selectedIndexGrupos: -1,
		 selectedIndex: -1,
		 elAlumno: '',
		 alumnos: []
	  };
	  this.renderPagos = this.renderPagos.bind(this);
   }

   async componentWillMount() {
	  await this.getURL();
   }

   async getURL() {
	  await this.setState({
		 uri: await AsyncStorage.getItem('uri'),
		 token: await AsyncStorage.getItem('token'),
		 mainColor: await AsyncStorage.getItem('mainColor'),
		 secondColor: await AsyncStorage.getItem('secondColor'),
		 thirdColor: await AsyncStorage.getItem('thirdColor'),
		 fourthColor: await AsyncStorage.getItem('fourthColor')
	  });
	  await this.getMeses();
   }

   async onListItemPressedGrupos(indexGrupo, grupo) {
	  await this.setState({
		 selectedIndexGrupos: indexGrupo,
		 elGrupo: grupo,
		 selectedIndexAlumno: -1,
		 selectedIndexPago: -1,
		 selItem: '',
		 elAlumno: ''
	  });
	  await this.getAlumnos();
   }

   async onListItemPressedGrado(indexGrado, grado) {
	  await this.setState({
		 selectedIndexGrados: indexGrado,
		 elGrado: grado,
		 selectedIndexAlumno: -1,
		 selectedIndexPago: -1,
		 selItem: '',
		 elAlumno: ''
	  });
   }

   //+++++++++++++++++++++++++++++++++++ MESES +++++++++++++++++++++++++++++++++
   async onChange(option) {
	  await this.setState({
		 textInputValue: option.label, elMes: option.mes, selectedIndexPago: -1, selItem: '', elAlumno: ''
	  });
   }

   async getMeses() {
	  let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
	  datosPicker = await dataPicker.json();
	  this.setState({cososPicker: datosPicker});
   }

   // ++++++++++++++++++++++++++++++ALUMNOS+++++++++++++++++++++++++++++++++++++++
   async getAlumnos() {
	  let alumnoList = await fetch(this.state.uri + '/api/user/alumnos/' + this.state.elGrado + '/' + this.state.elGrupo, {
		 method: 'GET', headers: {
			'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
		 }
	  });
	  alumnosList = await alumnoList.json();
	  this.setState({alumnos: alumnosList});
   }

   async onListPressedAlumno(indexAlumno, alumno) {
	  await this.setState({selectedIndexAlumno: indexAlumno, elAlumno: alumno});
	  await this.getCargos();
   }

   renderAlumno(itemAlumno, indexAlumno) {
	  let a = this.state.alumnos.length - 1;
	  let tabRow = [styles.rowTabla, styles.modalWidth];
	  if (indexAlumno !== a) {
		 tabRow.push({borderColor: this.state.secondColor});
	  }
	  let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
	  let texto = [styles.textoN];
	  if (this.state.selectedIndexAlumno === indexAlumno) {
		 smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
			backgroundColor: this.state.secondColor
		 });
		 texto.push(styles.textoB);
	  }
	  return (<TouchableHighlight
		key={indexAlumno}
		underlayColor={this.state.secondColor}
		style={[tabRow, smallButtonStyles]}
		onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.id)}
	  >
		 <View>
			{this.state.isModalalumno === true ? this.isValor(itemAlumno, indexAlumno) : null}
		 </View>
	  </TouchableHighlight>);
   }

   isValor(itemAlumno, indexAlumno) {
	  return (<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
		 <View>
			<Text style={{marginRight: 25}}>{itemAlumno.name}</Text>
		 </View>
	  </View>);
   }

   renderAlumnos() {
	  let buttonsAlumnos = [];
	  this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
		 buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
	  });
	  return buttonsAlumnos;
   }

   async getCargos() {
	  let request = await fetch(this.state.uri + '/api/payments/charge/return/all/' + this.state.mes, {
		 method: 'GET', headers: {
			Authorization: 'Bearer ' + this.state.token
		 }
	  });
	  let data = await request.json();
	  this.setState({cargos: data});
   }

   async onListItemPressedPago(index, item) {
	  await this.setState({selectedIndexGrupo: index, selItem: item});
   }

   renderPago(itemPago, indexPago) {
	  let bigButtonStyles = [styles.listButton, styles.listButtonBig, {borderColor: 'transparent'}];
	  let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn5, {borderColor: 'transparent'}];
	  let smallButtonStylesRight = [styles.listButton, styles.listButtonSmallRight, {borderColor: 'transparent'}];
	  let texto = [styles.textoN];

	  if (this.state.selectedIndexGrupo == indexPago) {
		 bigButtonStyles.push(styles.listButtonSelected, {
			backgroundColor: this.state.secondColor, height: responsiveHeight(4), width: responsiveWidth(30)
		 });
		 smallButtonStyles.push(styles.listButtonSelectedCenter, {
			backgroundColor: this.state.secondColor, width: responsiveWidth(35), height: responsiveHeight(4)
		 });
		 smallButtonStylesRight.push(styles.listButtonSelectedRight, {
			backgroundColor: this.state.secondColor, height: responsiveHeight(4), width: responsiveWidth(30)
		 });
		 texto.push(styles.textoB);
	  }

	  return (<TouchableOpacity
		key={indexPago}
		style={{
		   flexDirection: 'row', alignItems: 'center', width: responsiveWidth(95)
		}}
		underlayColor={this.state.secondColor}
		onPress={() => this.onListItemPressedPago(indexPago, itemPago)}
	  >
		 <View
		   style={[styles.row, {width: responsiveWidth(94), marginTop: 0}]}
		 >
			<View style={bigButtonStyles}>
			   <Text numberOfLines={1} style={[texto, {marginLeft: 5}]}>
				  {itemPago.descripcion}
			   </Text>
			</View>
			<View style={smallButtonStyles}>
			   <Text style={[texto, {marginLeft: 45}]}>
				  $ {itemPago.importe_a_pagar}
			   </Text>
			</View>
			<View style={smallButtonStylesRight}>
			   <Text style={[texto, {marginLeft: 15}]}>$320</Text>
			</View>
		 </View>
	  </TouchableOpacity>);
   }

   renderPagos() {
	  let buttonsPagos = [];
	  this.state.cargos.forEach((itemPago, indexPago) => {
		 buttonsPagos.push(this.renderPago(itemPago, indexPago));
	  });
	  return buttonsPagos;
   }

   render() {
	  const listaPagos = this.renderPagos();
	  let data = this.state.cososPicker.map((item, i) => {
		 return {key: i, mes: item.mes, label: item.nombre_mes};
	  });
       let mess = 'Enero';
       if (this.state.cososPicker !== ''){
           if (this.state.cososPicker.length !== 0){
               mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
           }
       }
	  const alumnos = this.renderAlumnos();
	  return (<View style={styles.container}>
		 <Modal
		   isVisible={this.state.isModalalumno}
		   backdropOpacity={0.5}
		   animationIn={'bounceIn'}
		   animationOut={'bounceOut'}
		   animationInTiming={1000}
		   animationOutTiming={1000}
		 >
			<View style={[styles.container, {borderRadius: 6}]}>
			   <ScrollView>
				  <Text style={[styles.main_title, styles.modalWidth]}>
					 Lista de alumnos
				  </Text>
				  <View
					style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}
				  />
				  <View
					style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}
				  >
					 {alumnos}
				  </View>
			   </ScrollView>
			   <TouchableOpacity
				 style={[styles.bigButton, styles.modalWidth, {
					backgroundColor: this.state.mainColor, borderColor: this.state.mainColor
				 }]}
				 onPress={() => this.setState({isModalalumno: false})}
			   >
				  <Text style={styles.textButton}>Aceptar</Text>
			   </TouchableOpacity>
			</View>
		 </Modal>
		 <ScrollView showsVerticalScrollIndicator={false}>
			<Text
			  style={[styles.main_title, {
				 color: this.state.thirdColor, marginBottom: 1
			  }]}
			>
			   Elija el periodo a consultar
			</Text>
			<ModalSelector
			  data={data}
			  selectStyle={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 2}]}
			  cancelText='Cancelar'
			  optionTextStyle={{color: this.state.thirdColor}}
			  initValue={mess}
			  onChange={option => this.onChange(option)}
			/>
			<GradoyGrupo
			  onListItemPressedGrupos={(indexGrupo, grupo) => this.onListItemPressedGrupos(indexGrupo, grupo)}
			  onListItemPressedGrado={(indexGrado, grado) => this.onListItemPressedGrado(indexGrado, grado)}
			  onListItemPressedLista={(indexLista, taller) => this.onListItemPressedLista(indexLista, taller)}
			  todos={'0'}
			/>
			<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
			   Seleccione el alumno
			</Text>

			<View style={{alignItems: 'center'}}>
			   <TouchableOpacity
				 style={[styles.bigButton, {
					borderColor: this.state.secondColor,
					width: responsiveWidth(47),
					height: responsiveHeight(5),
					borderWidth: 1,
					borderRadius: 6,
					backgroundColor: 'white'
				 }]}
				 onPress={() => this.setState({isModalalumno: true})}
			   >
				  <Text style={[styles.textButton, {color: 'black'}]}>
					 Ver lista
				  </Text>
			   </TouchableOpacity>
			   <Text style={{marginTop: 10}}>Alumno seleccionado</Text>
			   <Text
				 style={[styles.textButton, {color: 'black', marginBottom: 10}]}
			   >
				  {this.state.elAlumno}
			   </Text>
			</View>

			<View style={{width: responsiveWidth(94)}}>
			   <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
				  Lista de pagos vencidos
			   </Text>
			</View>
			<View>
			   <View style={styles.cargos}>
				  <Text style={{marginLeft: 5, marginTop: 20, fontWeight: '700'}}>
					 Concepto
				  </Text>
				  <Text
					style={{marginLeft: 78, marginTop: 20, fontWeight: '700'}}
				  >
					 Importe
				  </Text>
				  <Text
					style={{
					   marginLeft: 65, marginTop: 20, fontWeight: '700', textAlign: 'center'
					}}
				  >
					 Recargo
				  </Text>
			   </View>
			   <View
				 style={{
					height: responsiveHeight(23), borderBottomWidth: 1, borderTopWidth: 1, marginBottom: 5
				 }}
			   >
				  <ScrollView showsVerticalScrollIndicator={false}>
					 {listaPagos}
				  </ScrollView>
			   </View>
			</View>
			<View
			  style={{
				 alignItems: 'flex-start', width: responsiveWidth(95), height: responsiveHeight(5)
			  }}
			>
			   <Text
				 style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}
			   >
				  Detalle del pago
			   </Text>
			   <Text
				 style={{
					fontSize: responsiveFontSize(2), fontWeight: '300', marginTop: 0, color: this.state.thirdColor
				 }}
			   >
				  Descripción:
			   </Text>
			</View>
			<View
			  style={{
				 backgroundColor: '#ddd',
				 width: responsiveWidth(95),
				 height: responsiveHeight(20),
				 marginTop: 25,
				 borderRadius: 6,
				 marginBottom: 30
			  }}
			>
			   <Text
				 style={{
					width: responsiveWidth(90), marginTop: 10, marginHorizontal: 10, textAlign: 'left'
				 }}
			   >
				  El pago fue realizado el dia{'    '}
				  <Text
					style={{
					   fontWeight: '600', marginLeft: 20
					}}
				  >
					 XX/XX/XXXX
				  </Text>
				  fue realizado por medio de: xxxxxxxxxxxxx
			   </Text>
			   <Text
				 style={{
					width: responsiveWidth(90),
					height: responsiveHeight(8),
					marginTop: 30,
					marginHorizontal: 10,
					textAlign: 'center',
					fontWeight: '600'
				 }}
			   >
				  Descripcion
			   </Text>
			</View>
			<View style={[styles.widthall, {marginTop: -15}]}>
			   <View
				 style={{borderRadius: 6, padding: 10, backgroundColor: 'red'}}
			   >
				  <Text
					style={{
					   color: 'white', textAlign: 'center'
					}}
				  >
					 <Text style={{fontWeight: '700'}}>Importante: </Text> todos los
																		  pagos vencidos generan un
					 <Text style={{fontWeight: '700'}}>
						recargo automático del 10%
					 </Text>por cada mes de retraso que se acumule. Le invitamos a
																		  ponerse al corriente lo antes posible
				  </Text>
			   </View>
			</View>
		 </ScrollView>
	  </View>);
   }
}
