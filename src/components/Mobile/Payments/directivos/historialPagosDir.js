import React from 'react';
import {
    Alert,
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import GradoyGrupo from '../../Globales/GradoyGrupo';
import Modal from 'react-native-modal';
import {Actions} from 'react-native-router-flux';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class historialPagosDir extends React.Component {
    _showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});
    _hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});
    _handleDatePicked = date => {
        let fecha = moment(date).format('LL');
        this.setState({fecha1: fecha});
        this._hideDateTimePicker();
    };

    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false,
            botonSelected: 'Consulta individual',
            textInputValue: '',
            cososPicker: [],
            grados: [],
            grupos: [],
            elGrado: '',
            mes: moment().month() + 1,
            elGrupo: '',
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexCargo: -1,
            selectedIndex: -1,
            elAlumno: '',
            elId: '',
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            alumnos: [],
            selectedTipo: '',
            selItem: '',
            padre: '',
            data: []
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getMeses()
    }

    async getURL() {
        await this.setState({
            uri: await AsyncStorage.getItem('uri'),
            token: await AsyncStorage.getItem('token'),
            mainColor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor')
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            elGrupo: grupo,
            selectedIndexAlumno: '',
            elAlumno: '',
            elId: '',
            selectedTipo: '',
            data: [],
            selItem: ''
        });
        if (this.state.elGrado !== '') {
            await this.getAlumnos();
        }
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            elGrado: grado,
            selectedIndexAlumno: '',
            elAlumno: '',
            elId: '',
            selectedTipo: '',
            data: [],
            selItem: ''
        });
        if (this.state.elGrupo !== '') {
            await this.getAlumnos();
        }
    }

    //+++++++++++++++++++++++++++++++++++++++ MESES ++++++++++++++++++++++++
    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            mes: option.mes,
            selectedIndexAlumno: '',
            elAlumno: '',
            elId: '',
            selectedTipo: '',
            data: [],
            selItem: ''
        });
    }

    async getMeses() {
        await fetch(this.state.uri + '/api/config/extra/meses', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error' + ' continua' + ' pónganse en contacto con soporte (Meses)', [{
                        text: 'Entendido'
                    }]);
                } else {
                    this.setState({cososPicker: responseJson});
                }
            });
    }

    // +++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++

    async getAlumnos() {
        await fetch(this.state.uri + '/api/user/alumnos/' + this.state.elGrado + '/' + this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error' + ' continua' + ' pónganse en contacto con soporte ((HP) Alumnos)', [{
                        text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'News'}})
                    }]);
                } else {
                    this.setState({alumnos: responseJson});
                }
            });
    }

    onListPressedAlumno(indexAlumno, alumno, id, padre) {
        this.setState({
            selectedIndexAlumno: indexAlumno,
            elAlumno: alumno,
            elId: id,
            padre: padre,
            selectedTipo: '',
            data: [],
            selItem: ''
        });
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno === indexAlumno) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<TouchableHighlight
            key={indexAlumno}
            underlayColor={this.state.secondColor}
            style={[tabRow, smallButtonStyles]}
            onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.id, itemAlumno.padre_id)}>
            <View>
                {this.state.isModalalumno === true ? this.isValor(itemAlumno, indexAlumno) : null}
            </View>
        </TouchableHighlight>);
    }

    isValor(itemAlumno, indexAlumno) {
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno === indexAlumno) {
            texto.push(styles.textoB);
        }
        return (<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
            <View>
                <Text style={[texto, {marginRight: 25}]}>{itemAlumno.name}</Text>
            </View>
        </View>);
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    // render cargos
    async getCargos(i) {
        let t = i === 1 ? 'progreso' :
            i === 2 ? 'vencidos' : i === 3 ? 'requeridos' : i === 4 ? 'realizados' : i === 5 ? 'extemporaneos' : null;
        let api = i === 1 ? '/api/payment/dos/get/cargos/' + t + '/directivos/' + this.state.elGrado + '/' + this.state.elGrupo + '/' + this.state.elId + '/' + this.state.padre + '/' + this.state.mes :
            '/api/payment/dos/get/cargos/' + t + '/' + this.state.elGrado + '/' + this.state.elGrupo + '/' + this.state.elId + '/' + this.state.mes;
        await fetch(this.state.uri + api, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error si el error continua pónganse en contacto con soporte', [{
                        text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'historialPagosDir'}})
                    }]);
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    renderCargo(it, i) {
        let bigButtonStyles = [styles.listButton, styles.listButtonBig, styles.row, {
            backgroundColor: '#f2f2f2', borderWidth: 0
        }];
        if (this.state.selectedIndexCargo === i) {
            bigButtonStyles.push(styles.listButton, styles.listButtonBig, styles.row, {backgroundColor: '#ddd'});
        }
        return (<TouchableOpacity
            key={i}
            style={bigButtonStyles}
            underlayColor={this.state.secondColor}
            onPress={() => this.onListItemPressedCargo(it, i)}>
            <View style={[styles.btn3, {marginLeft: 9}]}>
                <Text numberOfLines={1} style={{textAlign: 'left'}}>
                    {it.descripcion}
                </Text>
            </View>
            <View
                style={[styles.btn6, {height: responsiveHeight(3.75), justifyContent: 'center'}]}>
                <Text
                    style={{fontSize: responsiveFontSize(1.5), textAlign: 'right'}}>
                    ${it.importe_a_pagar}
                </Text>
            </View>
            <View
                style={[styles.btn3T, {height: responsiveHeight(3.75), justifyContent: 'center'}]}>
                <Text
                    style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>
                    {moment(it.fecha_limite_pago).format('DD/MM/YY')}
                </Text>
            </View>
        </TouchableOpacity>);
    }

    renderCargos() {
        let buttonsGrupos = [];
        if (this.state.data.length !== 0) {
            this.state.data.forEach((it, i) => {
                buttonsGrupos.push(this.renderCargo(it, i));
            });
        }
        return buttonsGrupos;
    }

    async onListItemPressedCargo(it, i) {
        await this.setState({selectedIndexCargo: i, selItem: it});
    }

    // seleccion tipo
    async selectTipo(i) {
        await this.setState({
            selectedTipo: i, selItem: '', selectedIndexCargo: -1
        });
        await this.getCargos(i);
        await this.getBeca();
    }

    async onChange1(option) {
        await this.setState({
            textInputValue: option.label, selected: option.id
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected, indexSelected: indexSelected
        });
    }

    async getBeca() {
        await fetch(this.state.uri + '/api/user/alumno/' + this.state.elId + '/beca', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error si el error continua pónganse en contacto con soporte (Cargos)', [{
                        text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'GestionarPagos'}})
                    }]);
                } else {
                    this.setState({
                        becaAsignada: Number(responseJson),
                    });
                }
            });
    }

    openModal() {
        if (this.state.elGrado === '' || this.state.elGrupo === '') {
            Alert.alert('Atención', 'Seleccione un grado y un grupo antes', [{
                text: 'Entendido'
            }]);
        } else {
            this.setState({isModalalumno: true})
        }
    }

    render() {
        let t = this.state.selectedTipo === 1 ? 'En progreso' : this.state.selectedTipo === 2 ? 'Requerido' :
            this.state.selectedTipo === 3 ? 'Requerido' :
                this.state.selectedTipo === 4 ? 'Completado' : this.state.selectedTipo === 5 ? 'Completado' : null;
        const listaPagos = this.renderCargos();
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        let desc = 0;
        let finalprice = 0;
        if (this.state.selectedTipo === 3) {
            if (this.state.becaAsignada !== 0) {
                if (this.state.becaAsignada !== 0) {
                    desc = this.state.selItem.importe_a_pagar - ((this.state.selItem.importe_a_pagar * this.state.becaAsignada) / 100);
                    finalprice = Number(desc) + ((desc * 3.1) / 100) + 2.5;
                }
            }
            else {
                desc = this.state.selItem.importe_a_pagar;
                finalprice = Number(desc) + ((desc * 3.1) / 100) + 2.5;
            }
        }
        else {
            desc = this.state.selItem.importe_a_pagar;
            finalprice = Number(desc) + ((desc * 3.1) / 100) + 2.5;
        }
        const alumnos = this.renderAlumnos();
        return (<View style={styles.container}>
            <StatusBar
                backgroundColor={this.state.mainColor}
                barStyle='light-content'
            />
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.isModalalumno}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text style={[styles.main_title, styles.modalWidth]}>
                            Lista de alumnos
                        </Text>
                        <ScrollView>
                            <View
                                style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}>
                                {alumnos}
                            </View>
                        </ScrollView>
                        <View style={[styles.modalWidth, styles.row]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({
                                    isModalalumno: false,
                                    selectedIndexAlumno: -1,
                                    elAlumno: '',
                                    elId: '',
                                    padre: ''
                                })}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({isModalalumno: false})}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 20}}>
                    <Text
                        style={[styles.main_title, {
                            color: this.state.thirdColor, marginBottom: 1
                        }]}>
                        Elija el periodo a consultar
                    </Text>
                    <ModalSelector
                        data={data}
                        cancelText='Cancelar'
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 1}]}
                        initValue={mess}
                        onChange={option => this.onChange(option)}/>
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos(indexBtn, itemBtn)}
                        onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado(indexBtn, itemBtn)}
                        todos={'1'}/>
                    <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 7}]}>
                        Seleccione el alumno
                    </Text>
                    <View style={[
                        styles.widthall, {
                            backgroundColor: this.state.fourthColor,
                            borderRadius: 6,
                            padding: 10,
                            alignItems: 'center'
                        }
                    ]}
                    >
                        <TouchableOpacity
                            style={[styles.bigButton, {
                                borderColor: this.state.secondColor,
                                width: responsiveWidth(47),
                                height: responsiveHeight(5),
                                borderWidth: 1,
                                borderRadius: 6,
                                ...ifIphoneX({marginBottom: 15}),
                                backgroundColor: 'white'
                            }]}
                            onPress={() => this.openModal()}>
                            <Text style={[styles.textButton, {color: 'black'}]}>
                                Ver lista
                            </Text>
                        </TouchableOpacity>
                        <Text style={{fontWeight: '600'}}>Alumno seleccionado</Text>
                        <Text style={[styles.textButton, {color: 'black', marginBottom: 2}]}>
                            {this.state.elAlumno}
                        </Text>
                    </View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el tipo de cargo
                    </Text>
                    <View style={styles.row}>
                        <TouchableOpacity style={{
                            width: responsiveWidth(46),
                            borderWidth: 1,
                            borderRadius: 6,
                            padding: 8,
                            alignItems: 'center',
                            borderColor: this.state.secondColor,
                            backgroundColor: this.state.selectedTipo === 3 ? this.state.secondColor : '#fff'
                        }}
                                          onPress={() => this.selectTipo(3)}>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                color: this.state.selectedTipo === 3 ? '#fff' : '#000'
                            }}> Por Pagar </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            width: responsiveWidth(46),
                            borderWidth: 1,
                            marginLeft: responsiveWidth(2),
                            borderRadius: 6,
                            padding: 8,
                            alignItems: 'center',
                            borderColor: this.state.secondColor,
                            backgroundColor: this.state.selectedTipo === 4 ? this.state.secondColor : '#fff'
                        }}
                                          onPress={() => this.selectTipo(4)}>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                color: this.state.selectedTipo === 4 ? '#fff' : '#000'
                            }}>Pagados</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                        <TouchableOpacity style={{
                            width: responsiveWidth(46),
                            borderWidth: 1,
                            borderRadius: 6,
                            padding: 8,
                            alignItems: 'center',
                            borderColor: this.state.secondColor,
                            backgroundColor: this.state.selectedTipo === 2 ? this.state.secondColor : '#fff'
                        }}
                                          onPress={() => this.selectTipo(2)}>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                color: this.state.selectedTipo === 2 ? '#fff' : '#000'
                            }}>Vencidos</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            width: responsiveWidth(46),
                            marginLeft: responsiveWidth(2),
                            borderWidth: 1,
                            borderRadius: 6,
                            padding: 8,
                            alignItems: 'center',
                            borderColor: this.state.secondColor,
                            backgroundColor: this.state.selectedTipo === 5 ? this.state.secondColor : '#fff'
                        }}
                                          onPress={() => this.selectTipo(5)}>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                color: this.state.selectedTipo === 5 ? '#fff' : '#000'
                            }}>Extemporáneos</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.row, {marginTop: 3}]}>
                    </View>
                    <View style={[styles.row, {width: responsiveWidth(62.5), marginTop: 5}]}>

                    </View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Lista de pagos realizados
                    </Text>
                    <View>
                        <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                            <View style={styles.btn3}>
                                <Text style={{fontWeight: '700', textAlign: 'center'}}>
                                    Concepto
                                </Text>
                            </View>
                            <View style={styles.btn3}>
                                <Text style={{fontWeight: '700', textAlign: 'right'}}>
                                    Pagado
                                </Text>
                            </View>
                            <View style={styles.btn3}>
                                <Text
                                    style={{
                                        fontWeight: '700', textAlign: 'center', marginLeft: 5
                                    }}>
                                    Fecha
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                height: responsiveHeight(20), borderBottomWidth: 1, borderTopWidth: 1, marginBottom: 5
                            }}>
                            <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 5, marginTop: 5}}>
                                {listaPagos}
                            </ScrollView>
                        </View>
                    </View>
                    <View
                        style={[styles.widthall, {
                            alignItems: 'flex-start',
                            height: responsiveHeight(5),
                        }]}>
                        <Text
                            style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>
                            Detalle del pago
                        </Text>
                    </View>
                    {this.state.selectedIndexCargo !== -1 ? (<View
                        style={{
                            width: responsiveWidth(95),
                            paddingVertical: 10,
                            borderRadius: 6,
                            overflow: 'hidden',
                            backgroundColor: this.state.fourthColor,
                        }}>

                        {/*<View style={[styles.triangleCorner,{borderTopColor: this.state.thirdColor}]}></View>*/}
                        {/*<View style={[styles.triangleCornerLayer, {borderTopColor: this.state.fourthColor}]}></View>*/}
                        {/*<View style={[styles.triangleCorner1, {borderTopColor: this.state.secondColor}]}></View>*/}

                        <View style={[styles.row, {alignItems: 'flex-start'}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                    Concepto</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>{this.state.selItem.descripcion}</Text>
                            </View>
                        </View>

                        {this.state.selectedTipo === 4 ? null : (this.state.selItem.importe_a_pagar - desc) > 0 ?
                            <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text
                                        style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                        Importe normal</Text>
                                </View>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text>${Number(this.state.selItem.importe_a_pagar).toLocaleString('en-US')}</Text>
                                </View>
                            </View> : null}

                        {this.state.selectedTipo === 4 ? null : (this.state.selItem.importe_a_pagar - desc) > 0 ?
                            <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text
                                        style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                        Descuento por beca</Text>
                                </View>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text>${Number(this.state.selItem.importe_a_pagar - desc).toLocaleString('en-US')}</Text>
                                </View>
                            </View> : null}
                        {this.state.selectedTipo === 4 ? <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text style={{color: this.state.secondColor, fontWeight: '800', textAlign: 'right'}}>
                                        Total pagado</Text>
                                </View>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text style={{fontWeight: '800'}}>${this.state.selectedTipo === 3
                                        ? Number(desc).toLocaleString('en-US')
                                        : this.state.selectedTipo === 2
                                            ? Number(this.state.selItem.importe_a_pagar).toLocaleString('en-US')
                                            : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                                </View>
                            </View> :
                            <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text
                                        style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                        Importe a pagar</Text>
                                </View>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text>${this.state.selectedTipo === 3
                                        ? Number(desc).toLocaleString('en-US')
                                        : this.state.selectedTipo === 2
                                            ? Number(this.state.selItem.importe_a_pagar).toLocaleString('en-US')
                                            : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                                </View>
                            </View>}

                        {this.state.selectedTipo === 4
                            ? null
                            : <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text
                                        style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                        Comisión pasarela</Text>
                                </View>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text>${this.state.selectedTipo === 3
                                        ? Number(finalprice - desc).toLocaleString('en-US')
                                        : this.state.selectedTipo === 2
                                            ? Number(finalprice - this.state.selItem.importe_a_pagar).toLocaleString('en-US')
                                            : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                                </View>
                            </View>}

                        {this.state.selectedTipo === 4
                            ? null
                            : <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text
                                        style={{color: this.state.secondColor, fontWeight: '800', textAlign: 'right'}}>
                                        Total a pagar</Text>
                                </View>
                                <View style={{width: responsiveWidth(45)}}>
                                    <Text style={{fontWeight: '800'}}>${this.state.selectedTipo === 3
                                        ? Number(finalprice).toLocaleString('en-US')
                                        : this.state.selectedTipo === 2
                                            ? Number(finalprice).toLocaleString('en-US')
                                            : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                                </View>
                            </View>}
                        <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>Fecha
                                    de
                                    creación</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>{moment(this.state.selItem.created_at).format('DD/MM/YY')}</Text>
                            </View>
                        </View>
                        <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>Fecha
                                    límite de pago</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>{moment(this.state.selItem.fecha_limite_pago).format('DD/MM/YY')}</Text>
                            </View>
                        </View>
                        <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                    Estatus</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>{t}</Text>
                            </View>
                        </View>
                    </View>) : null}
                </ScrollView>
            </View>
        </View>);
    }
}
