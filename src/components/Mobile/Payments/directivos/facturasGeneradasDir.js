import React from "react";
import {
    Alert,
    AsyncStorage,
    ScrollView,
    Text,
    TouchableHighlight, TouchableOpacity,
    View
} from 'react-native';
import ModalSelector from "react-native-modal-selector";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import GradoyGrupo from "../../Globales/GradoyGrupo";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Modal from "react-native-modal";
import {Actions} from 'react-native-router-flux';
import Feather from 'react-native-vector-icons/Feather';
import Spinner from 'react-native-loading-spinner-overlay';
import MultiBotonRow from "../../Globales/MultiBotonRow";
import Switch from "react-native-switch-pro";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class facturasGeneradasDir extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            textInputValue: "",
            textInputValue2: "",
            elMes: "",
            elMes2: "",
            cososPicker: [],
            grados: [],
            grupos: [],
            elGrado: "",
            mes: moment().month() + 1,
            mes2: moment().month() + 1,
            elGrupo: "",
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selectedIndex: -1,
            elAlumno: "",
            alumnos: [],
            lista: false,
            facturas: [],
            cancelar: false,
            botonSelected: 'Facturas generadas',
            selected1: 1,
            lacantidad: '',
            listaAlumnosFactura: false,
            losAlumnos:[]
        };
    }

    async componentWillMount(){
        await this.getURL();
        await this.getMeses();
    }
    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        await this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });

    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            elGrupo: grupo
        });
        await this.getAlumnos();
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            elGrado: grado
        });
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno === indexAlumno) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<TouchableHighlight
            key={indexAlumno + 'Al'}
            underlayColor={this.state.secondColor}
            style={[tabRow, smallButtonStyles]}
            onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.padre_id, itemAlumno.id)}>
            <View style={[styles.campoTablaG, {alignItems: 'center'}]}>
                <Text style={[texto, {marginRight: 25}]}>{itemAlumno.name}</Text>
            </View>
        </TouchableHighlight>);
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    // +++++++++++++++++++++++++++++++++++++ALUMNOS+++++++++++++++++++++++++++++


    async getAlumnos() {
        let alumnoList = await fetch(this.state.uri + '/api/user/alumnos/' + this.state.elGrado + '/' + this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        });
        let alumnosList = await alumnoList.json();
        await this.setState({alumnos: alumnosList});
        console.log(this.state.alumnos)
    }


    //+-+-+-+-+-+-+-+-+-+-+-+-ModalALumno
    async openListAlumn() {
        this.state.elAlumno = this.state.elAlumno2;
        this.state.alumnosId = this.state.alumnosId2;
        this.state.elReceptor = this.state.elReceptor2;
        this.state.selectedIndexAlumno = this.state.selectedIndexAlumno2;
        if (this.state.elGrado === '') {
            Alert.alert('Campo grado vacío', 'Seleccione un grado para poder continuar', [
                {text: 'Entendido'}
            ])
        } else if (this.state.elGrupo === '') {
            Alert.alert('Campo grupo vacío', 'Seleccione un grupo para poder continuar', [
                {text: 'Entendido'}
            ])
        } else {
            this.setState({isModalCateg: true, selectedIndexAlumno: this.state.selectedIndexAlumno2});
        }
    }

    async closeListAlumn() {
        if (this.state.selectedIndexAlumno === -1) {
            Alert.alert('Alumno no seleccionado', '', [{text: 'Entendido'}]);
        } else {
            this.state.elAlumno2 = this.state.elAlumno;
            this.state.alumnosId2 = this.state.alumnosId;
            this.state.elReceptor2 = this.state.elReceptor;
            this.state.selectedIndexAlumno2 = this.state.selectedIndexAlumno;
            this.setState({isModalCateg: false});
        }
    }


    //+++++++++++++++++++++++++++++ MESES ++++++++++++++++++++++++++++++++++++++
    async onChange(option) {
        await this.setState({textInputValue: option.label, mes: option.mes});
    }


    async onChange2(option) {
        await this.setState({textInputValue2: option.label, mes2: option.mes});
        console.log(this.state.mes2);
        await this.aFacturar();
    }
    async getMeses() {
        let dataPicker = await fetch(this.state.uri + "/api/config/extra/meses");
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }


    onListPressedAlumno(indexAlumno, alumno) {
        this.setState({selectedIndexAlumno: indexAlumno, elAlumno: alumno});
    }

    verDetalle(item){
        fetch(this.state.uri + '/api/get/detalle/factura/'+item.facturama_id,
            {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();

            }).then(responseJson => {
            if (responseJson.error === null) {
                Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Facturas)');
            } else {
                Actions.DetalleFacturaDirector({laFactura: responseJson});
            }
        });

    }

    cancelarFactura(item){
        console.log(item.facturama_id)
        this.setState({cancelar: true})
        fetch(this.state.uri + '/api/cancelarFactura/' + item.facturama_id,
            {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();
            }).then(responseJson => {
            console.log(responseJson)
            if (responseJson.error === null) {
                Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Facturación)', [{
                    text: 'Enterado', onPress: () => [this.setState({cancelar: false})]
                }])
            } else {
                Alert.alert('Felicidades', 'Se ha cancelado la factura correctamente', [{
                    text: 'Enterado', onPress: () => [this.setState({cancelar: false}), this.requestMultipart()]
                }]);

            }
        })
    }

    card() {
        let carta = [];
        this.state.facturas.forEach((item, i) => {
            carta.push(
                <View
                    key={i + 'carta'}
                    style={[
                        {
                            marginVertical: 5,
                            marginHorizontal: 5,
                            borderWidth: 0.5,
                            borderRadius: 6,
                            borderColor: 'lightgray',
                            backgroundColor: this.state.fourthColor,
                            paddingTop: 10,
                            width: responsiveWidth(80),
                        }
                    ]}>
                    <View style={[styles.row, styles.widthall, {
                        justifyContent: 'flex-start',
                        padding: 0,
                        paddingLeft: 5
                    }]}>
                        <View style={[styles.btn2, {alignItems: 'flex-start'}]}>
                            <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Descripción
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                                marginTop: 3
                            }}>{item.descripcion}</Text>
                        </View>
                        <View style={[styles.btn3_lll]}>
                            <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Folio
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                                marginTop: 3
                            }}>{item.folio}</Text>
                        </View>
                        <View style={[styles.btn3_lll]}>
                            <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Serie
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                                marginTop: 3,
                                marginLeft: 12
                            }}>{item.serie}</Text>
                        </View>
                    </View>
                    {(item.status === 'Cancelada') ?
                        (<View style={[styles.row, {
                            textAlign: 'center',
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingVertical: 10,
                        }]}>
                            <TouchableOpacity style={[styles.row, {
                                borderRadius: 6,
                                backgroundColor: this.state.secondColor,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                paddingHorizontal: 15,
                                width: responsiveWidth(30),
                            }]}
                                              onPress={() => [this.verDetalle(item), this.setState({lista: false})]}
                            >
                                <Text style={[styles.textW, {color: "white", fontSize: responsiveFontSize(1.3)}]}>
                                    Ver detalle
                                </Text>
                                <Feather name='eye' size={20} color='#fff'
                                         style={{marginLeft: 10, paddingVertical: 3}}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style={[{
                                marginLeft: 10,
                                borderRadius: 6,
                                paddingVertical: 3,
                                paddingHorizontal: 15,
                                backgroundColor: 'red',
                                width: responsiveWidth(30),
								height: responsiveHeight(5)
                            }]}
                                              onPress={() => [(
                                                  Alert.alert('Error', 'La factura ya fué cancelada', [{
                                                      text: 'Enterado'
                                                  }])
                                              )]}
                            >
                                <Text style={[styles.textW, {textAlign: 'center', color: "white", fontSize: responsiveFontSize(1.3)}]}>
                                    Factura cancelada
                                </Text>
                            </TouchableOpacity>
                        </View>):
                        (<View style={[styles.row, {
                            textAlign: 'center',
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingVertical: 10,
                        }]}>
                            <TouchableOpacity style={[styles.row, {
                                borderRadius: 6,
                                backgroundColor: this.state.secondColor,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                paddingHorizontal: 15,
                                width: responsiveWidth(30),
                            }]}
                                              onPress={() => [this.verDetalle(item), this.setState({lista: false})]}
                            >
                                <Text style={[styles.textW, {color: "white", fontSize: responsiveFontSize(1.3)}]}>
                                    Ver detalle
                                </Text>
                                <Feather name='eye' size={20} color='#fff'
                                         style={{marginLeft: 10, paddingVertical: 3}}
                                />
                            </TouchableOpacity>
                            {/*<TouchableOpacity style={[{*/}
                                {/*marginLeft: 10,*/}
                                {/*borderRadius: 6,*/}
                                {/*paddingVertical: 3,*/}
                                {/*paddingHorizontal: 15,*/}
                                {/*backgroundColor: this.state.mainColor,*/}
                                {/*width: responsiveWidth(30),*/}
                                {/*height: responsiveHeight(5)*/}
                            {/*}]}*/}
                                              {/*onPress={() => [(*/}
                                                  {/*Alert.alert('Cancelar Factura', '¿Está seguro que desea cancelar la factura?', [*/}
                                                      {/*{*/}
                                                          {/*text: 'Sí',*/}
                                                          {/*onPress: () => [this.cancelarFactura(item)]*/}
                                                      {/*}, {text: 'No'}*/}
                                                  {/*])*/}
                                              {/*)]}*/}
                            {/*>*/}
                                {/*<Text style={[styles.textW, {textAlign: 'center', color: "white", fontSize: responsiveFontSize(1.3)}]}>*/}
                                    {/*Cancelar Factura*/}
                                {/*</Text>*/}
                            {/*</TouchableOpacity>*/}
                        </View>)
                    }
                </View>


            )
        });
        return carta;
    }

    async requestMultipart() {
        await this.setState({lista: true});
        await fetch(this.state.uri + '/api/get/admin/facturas/' + this.state.mes+(this.state.elGrado === "" ? '' : '/' + this.state.elGrado) +(this.state.elGrupo === "" ? '' : '/' + this.state.elGrupo)  + (this.state.elAlumno === "" ? '' : '/' + this.state.elAlumno ) , {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                console.log(responseJson)
                if (responseJson.error !== undefined) {
                } else {
                	this.setState({facturas: responseJson})
                    if (this.state.facturas.length === 0){
                        Alert.alert('Atención', 'No existen facturas aún', [{
                            text: 'Enterado', onPress: () => [this.setState({lista: false})]
                        }]);
                    }
                }
            });

    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    lasFacturas(){
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (
            <View>
                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor, marginBottom: 1}
                    ]}
                >
                    Elija el mes a consultar
                </Text>
                <ModalSelector
                    data={data}
                    selectStyle={[
                        [styles.inputPicker, {borderColor: this.state.secondColor}],
                        {marginTop: 2}
                    ]}
                    cancelText="Cancelar"
                    optionTextStyle={{color: this.state.thirdColor}}
                    initValue={mess}
                    onChange={option => this.onChange(option)}
                />
                <GradoyGrupo
                    onListItemPressedGrupos={(indexBtn, itemBtn) =>
                        this.onListItemPressedGrupos(indexBtn, itemBtn)
                    }
                    onListItemPressedGrado={(indexBtn, itemBtn) =>
                        this.onListItemPressedGrado(indexBtn, itemBtn)
                    }
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione el alumno
                </Text>
                <View style={{
                    alignItems: 'center',
                    backgroundColor: this.state.fourthColor,
                    padding: 15,
                    borderRadius: 6
                }}
                >
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            styles.btnShowMod,
                            {
                                marginTop: 5,
                                marginBottom: 5,
                                borderColor: this.state.secondColor,
                                backgroundColor: '#fff'
                            }
                        ]}
                        onPress={() => this.openListAlumn()}>
                        <Text>Ver lista</Text>
                    </TouchableOpacity>
                    <Text style={{marginTop: 10}}>Alumno seleccionado:</Text>
                    <Text style={[styles.textButton, {color: 'black'}]}>
                        {this.state.elAlumno2}
                    </Text>
                </View>
                <TouchableOpacity
                    style={[styles.bigButton, {
                        backgroundColor: this.state.secondColor
                    }]}
                    onPress={() => this.requestMultipart()}>
                    <Text style={styles.textButton}>Consultar facturas</Text>
                </TouchableOpacity>
            </View>
        )
    }



    //+++++++ MultibotonRow +++++++
    filtroSelected(i) {
        this.setState({selected1: i});
    }

    generarFactura(){
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return(
            <View>
                <Text
                style={[
                    styles.main_title,
                    {color: this.state.thirdColor, marginBottom: 1}
                ]}
            >
                Elija el mes a facturar
            </Text>
                <ModalSelector
                    data={data}
                    selectStyle={[
                        [styles.inputPicker, {borderColor: this.state.secondColor}],
                        {marginTop: 2}
                    ]}
                    cancelText="Cancelar"
                    optionTextStyle={{color: this.state.thirdColor}}
                    initValue={mess}
                    onChange={option => this.onChange2(option)}
                />
                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor, marginBottom: 1}
                    ]}
                >
                    A facturar:
                </Text>
                <View style={{alignItems: 'center', marginTop: 25}}>
                    <Text style={[
                        {color: this.state.mainColor, margin: 10, fontSize: 50}
                    ]}>${this.state.lacantidad}</Text>
                <TouchableOpacity
                    style={[
                        {marginTop:15, borderRadius: 10,width: responsiveWidth(70), height: responsiveHeight(4),alignItems: 'center', justifyContent: 'center',backgroundColor: 'white', borderColor: this.state.secondColor, borderWidth: 2}
                    ]}
                    onPress={() => [this.setState({listaAlumnosFactura: true})]}
                >
                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Lista de alumnos incluidos en la factura</Text>
                </TouchableOpacity>
                </View>
                <View style={[styles.modalWidth, styles.row]}>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {marginTop: 200, backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                    ]}
                    onPress={() => [Alert.alert('Factura', 'Crear factura')]}
                >
                    <Text style={[styles.textButton, {color: '#fff'}]}>Generar factura global</Text>
                </TouchableOpacity>
                </View>
            </View>
        )
    }

    //-*-*-*-*-*-*-*-*-*-*--*-*-Obtener el total de cantidad a facturar-*--*-*-*-*-*-*-*-*-*-*--*
    aFacturar(){
        fetch('https://gestion-dev.controlescolar.pro/api/get/pagosFacturaGlobal/'+this.state.mes2 , {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                console.log(responseJson)
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({lacantidad: responseJson.total})
                    this.setState({losAlumnos: responseJson.alumnos})
                }
            });
    }


    historial(){
        return(
            <View><Text>HISTORIAL</Text></View>
        )
    }
    facturaGlobal(){
        let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];

        let texto1 = [styles.textoB, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnTem = [styles.exaCalif, {backgroundColor: this.state.thirdColor}];
        return(
            <View>
                <View style={[styles.widthall, {alignItems: 'center'}]}>
                    <View
                        style={[styles.rowsCalif, {
                            width: responsiveWidth(60), marginTop: 10, marginBottom: 15
                        }]}>
                        <TouchableHighlight
                            underlayColor={'transparent'}
                            style={this.state.selected1 === 1 ? btnTem : btnCal}
                            onPress={() => this.filtroSelected(1)}>
                            <Text style={this.state.selected1 === 1 ? texto1 : texto}>Generar factura</Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                            underlayColor={'transparent'}
                            style={this.state.selected1 === 2 ? btnTem : btnCal}
                            onPress={() => this.filtroSelected(2)}>
                            <Text style={this.state.selected1 === 2 ? texto1 : texto}>Historial</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                {this.state.selected1 === 1 ? this.generarFactura(): null}
                {this.state.selected1 === 2 ? this.historial(): null}
               </View>
        )

    }

    renderAlumnosFactura(){
        let cards = [];
        this.state.losAlumnos.forEach((itm, ix) => {
            let a = this.state.losAlumnos.length - 1;
            let tabRow = [styles.rowTabla];
            if (ix !== a) {
                tabRow.push({borderColor: this.state.secondColor});
            }
                cards.push(
                    <View
                        key={ix + 'alumnCal'}
                        underlayColor={'transparent'}
                        style={[tabRow, styles.campoTablaG, styles.row, styles.modalWidth]}
                    >
                        <Text style={{width: responsiveWidth(60)}}>{itm.name}</Text>
                        <Text style={{width: responsiveWidth(15)}}>{itm.grado}</Text>
                        <Text style={{width: responsiveWidth(20)}}>{itm.grupo}</Text>
                    </View>
                )
        });
        return cards;
    }

    render() {
        const alumnosList = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.cancelar} textContent='Cancelando factura...'/>
                <Modal
                    isVisible={this.state.isModalCateg}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text style={[styles.main_title, styles.modalWidth]}>
                            Seleccione el alumno:
                        </Text>
                        <View
                            style={[styles.tabla,{
                                borderColor: this.state.secondColor,
                                marginTop: 10,
                                height: responsiveHeight(60)
                            }]}>
                            <ScrollView>
                                {alumnosList}
                            </ScrollView>
                        </View>

                        <View style={[styles.modalWidth, styles.row]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({isModalCateg: false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.closeListAlumn()}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Modal 	isVisible={this.state.lista}
                          backdropOpacity={0.8}
                          animationIn={'bounceIn'}
                          animationOut={'bounceOut'}
                          animationInTiming={1000}
                          animationOutTiming={1000}>
                    <View style={[styles.container, {height: responsiveHeight(80), borderRadius: 6, flex: 0}]}>
                        <View style={{width: responsiveWidth(85)}}>
                            <Text
                                style={[
                                    styles.main_title,
                                    {color: this.state.thirdColor, marginTop: 10}
                                ]}
                            >
                                Lista de facturas generadas
                            </Text>
                        </View>
                        <View>
                            <View
                                style={{
                                    height: responsiveHeight(66),
                                    borderBottomWidth: 1,
                                    borderTopWidth: 1,
                                    marginBottom: 5
                                }}
                            >
                                <ScrollView showsVerticalScrollIndicator={false}>
                                    {this.card()}
                                </ScrollView>
                            </View>
                        </View>
                        <TouchableOpacity
                            style={[
                                styles.modalBigBtn,
                                {backgroundColor: '#fff', borderColor: this.state.secondColor, marginVertical: 10}
                            ]}
                            onPress={() => this.setState({lista: false})}
                        >
                            <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal 	isVisible={this.state.listaAlumnosFactura}
                          backdropOpacity={0.8}
                          animationIn={'bounceIn'}
                          animationOut={'bounceOut'}
                          animationInTiming={1000}
                          animationOutTiming={1000}>
                    <View style={[styles.container, {height: responsiveHeight(80), borderRadius: 6, flex: 0}]}>
                        <View style={{width: responsiveWidth(85)}}>
                            <Text
                                style={[
                                    styles.main_title,
                                    {color: this.state.thirdColor, marginTop: 10}
                                ]}
                            >
                                Alumnos incluidos en la factura global
                            </Text>
                        </View>
                        <View>
                            <View
                                style={{
                                    height: responsiveHeight(66),
                                    borderBottomWidth: 1,
                                    borderTopWidth: 1,
                                    marginBottom: 5
                                }}
                            >
                                <ScrollView showsVerticalScrollIndicator={false}>
                                    <View
                                        style={[
                                            styles.tabla,
                                            {
                                                borderColor: this.state.secondColor,
                                                marginTop: 5,
                                            }
                                        ]}
                                    >
                                        {this.renderAlumnosFactura()}
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                        <TouchableOpacity
                            style={[
                                {marginTop:15, borderRadius: 10,width: responsiveWidth(70), height: responsiveHeight(4),alignItems: 'center', justifyContent: 'center',backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                            ]}
                            onPress={() => [this.setState({listaAlumnosFactura: false})]}
                        >
                            <Text style={[styles.textW, {color: '#fff'}]}>Cerrar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <View>
                    <MultiBotonRow
                        itemBtns={["Facturas generadas", "Factura global"]}
                        onSelectedButton={(indexBtn, itemBtn) =>
                            this.botonSelected(indexBtn, itemBtn)
                        }
                        cantidad={2}
                    />
                    {this.state.botonSelected === 'Facturas generadas' ? this.lasFacturas(): null}
                    {this.state.botonSelected === 'Factura global' ? this.facturaGlobal(): null}
                </View>
            </View>
        );
    }
}

