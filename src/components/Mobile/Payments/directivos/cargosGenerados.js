import React from "react";
import {AsyncStorage, FlatList, Text, View} from "react-native";
import ModalSelector from "react-native-modal-selector";
import {responsiveFontSize, responsiveWidth} from "react-native-responsive-dimensions";
import styles from "../../../styles";

export default class cargosGenerados extends React.Component {
    _keyExtractor = (item, index) => item.id;

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            cososPicker: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getCargos();
        await this.getMeses();
    }

    async componentWillMount() {
        await this.getURL();
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, mes: option.mes});
        await this.getFeed();
    }

    async getCargos() {
        let request = await fetch(
            this.state.uri + "/api/payments/charge/return/all",
            {
                method: "GET",
                headers: {
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let data = await request.json();
        this.setState({data: data});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + "/api/config/extra/meses");
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    render() {
        const data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ModalSelector
                    data={data}
                    selectStyle={[
                        styles.inputPicker,
                        {borderColor: this.state.secondColor}
                    ]}
                    initValue={mess}
                    onChange={option => this.onChange(option)}
                />
                <View>
                    <View style={styles.cargos}>
                        <Text style={styles.subTitleMain}>Concepto</Text>
                        <Text style={{marginLeft: 80, marginTop: 20, fontWeight: "700"}}>
                            Importe
                        </Text>
                        <Text style={{marginLeft: 35, marginTop: 20, fontWeight: "700"}}>
                            Fecha de pago
                        </Text>
                    </View>
                    <FlatList
                        keyExtractor={this._keyExtractor}
                        data={this.state.data}
                        renderItem={({item}) => (
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignItems: "stretch",
                                    justifyContent: "space-between",
                                    width: responsiveWidth(95),
                                    marginVertical: 15
                                }}
                            >
                                <Text style={{width: responsiveWidth(28)}}>
                                    {item.descripcion}
                                </Text>
                                <Text style={{textAlign: "center"}}>
                                    {item.importe_a_pagar}
                                </Text>
                                <Text
                                    style={{
                                        textAlign: "center",
                                        fontSize: responsiveFontSize(1.5),
                                        paddingTop: 3
                                    }}
                                >
                                    {item.fecha_limite_pago}
                                </Text>
                            </View>
                        )}
                    />
                </View>
            </View>
        );
    }
}
