import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight, Alert
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class DetalleFacturaDirector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            laFactura: this.props.laFactura,
        };
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async componentWillMount() {
        await this.getURL();
    }


    render() {
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {this.state.laFactura.Status === 'canceled'?
                        (<View style={{
                            backgroundColor: 'red',
                            marginTop: 8
                        }}>
                            <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                                Factura cancelada
                            </Text>
                        </View>):(<View style={{
                            backgroundColor: this.state.secondColor,
                            marginTop: 8
                        }}>
                            <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                                Factura activa
                            </Text>
                        </View>)
                    }
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Factura
                        </Text>
                    </View>
                    <View style={[styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1,
                    }]}>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Tipo de CFDI
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Type}</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Serie
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Serie}</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Folio
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Folio}</Text>
                        </View>
                    </View>
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Emisor
                        </Text>
                    </View>
                    <View style={[styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1
                    }]}>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Régimen Fiscal
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Issuer.FiscalRegime}</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                RFC
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Issuer.Rfc}</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Nombre
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Issuer.TaxName}</Text>
                        </View>
                    </View>
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        borderRadius: 3,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Receptor
                        </Text>
                    </View>
                    <View style={[styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1
                    }]}>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                RFC
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Receiver.Rfc}</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Nombre del Receptor
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.state.laFactura.Receiver.Name}</Text>
                        </View>
                    </View>
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        borderRadius: 3,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Detalles
                        </Text>
                    </View>
                    <View style={[styles.row, styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1,
                        borderBottomWidth: 0
                    }]}>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Fecha
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{moment(this.state.laFactura.Date).format('L')}</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Número de certificado
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{this.state.laFactura.CertNumber}</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Método de pago
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{this.state.laFactura.PaymentTerms}</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Tipo de pago
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{this.state.laFactura.PaymentMethod}</Text>
                        </View>
                    </View>
                    <View style={[styles.row, styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1,
                        borderTopWidth: 0
                    }]}>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Moneda
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>'MXN'</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Subtotal
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>${this.state.laFactura.Subtotal}</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Total
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>${this.state.laFactura.Total}</Text>
                        </View>
                    </View>
                </ScrollView>
                <View style={[styles.row, styles.widthall, {justifyContent: "center", marginBottom: 20}]}>
                    <TouchableOpacity
                        style={[
                            styles.modalBigBtn,
                            {backgroundColor: '#fff', borderColor: this.state.secondColor, margin: 5}
                        ]}
                        onPress={() => [Actions.facturasGeneradasDir()]}
                    >
                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Regresar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

