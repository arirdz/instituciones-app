import React from 'react';
import {AsyncStorage, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import GradoyGrupo from '../../Globales/GradoyGrupo';
import styles from '../../../styles';
import Modal from 'react-native-modal';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class consultarSaldosDir extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            textInputValue: '',
            mes: moment().month() + 1,
            cososPicker: [],
            grados: [],
            grupos: [],
            elCargo: '',
            elGrado: '',
            elGrupo: '',
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexGrupo: -1,
            selectedIndex: -1,
            elAlumno: '',
            alumnos: []
        };
        // this.renderPagos = this.renderPagos.bind(this);
    }

    async getURL() {
        await this.setState({
            uri: await AsyncStorage.getItem("uri"),
            token: await AsyncStorage.getItem("token"),
            mainColor: await AsyncStorage.getItem("mainColor"),
            secondColor: await AsyncStorage.getItem("secondColor"),
            thirdColor: await AsyncStorage.getItem("thirdColor"),
            fourthColor: await AsyncStorage.getItem("fourthColor")
        });
        await this.getMeses();
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo, elGrupo: grupo
        });
        if (this.state.elGrado !== '') {
            await this.getCargos();
        }
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado, elGrado: grado
        });
        if (this.state.elGrupo !== '') {
            await this.getCargos();
        }
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ MESES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async onChange(option) {
        await this.setState({textInputValue: option.label, mes: option.mes});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    //++++++++++ Recordatorio

    async recordatorio() {
        let faltantes = [];
        this.state.alumnos.forEach((it) => {
            faltantes.push(it.id)
        });

        // let formData = new FormData();
        // formData.append('new', JSON.stringify({
        //  data: {
        // 	cargo: item.concepto,
        // 	fecha: moment(item.fecha).format('DD/MM/YY'),
        // 	importe: item.total,
        // 	id: faltantes
        //  }
        // }));
        //  await fetch(this.state.uri + '/api/recordatorio/pagos', {
        //  method: 'POST', headers: {
        // 	'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
        //  }, body: formData
        // });
    }

    //++++++++++ agradecimiento

    async agradecimiento() {
        let faltantes = [];
        this.state.alumnos.forEach((it) => {
            faltantes.push(it.id)
        });

        // let formData = new FormData();
        // formData.append('new', JSON.stringify({
        //  data: {
        // 	cargo: item.concepto,
        // 	fecha: moment(item.fecha).format('DD/MM/YY'),
        // 	importe: item.total,
        // 	id: faltantes
        //  }
        // }));
        // await fetch(this.state.uri + '/api/recordatorio/pagos', {
        //  method: 'POST', headers: {
        // 	'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
        //  }, body: formData
        // });
    }

    //+++++++++++++++++++++++++++++ALUMNOS
    async getAlumnos() {
        let alumnoList = await fetch(this.state.uri + '/api/user/alumnos/' + this.state.elGrado + '/' + this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        });
        let alumnosList = await alumnoList.json();
        this.setState({alumnos: alumnosList});
    }

    onListPressedAlumno(indexAlumno, alumno, id) {
        this.setState({
            selectedIndexAlumno: indexAlumno, elAlumno: alumno, elId: id
        });
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (<View key={indexAlumno} style={tabRow}>
            <View>
                {this.state.isModalalumno === true ? this.isValor(itemAlumno, indexAlumno) : null}
                {this.state.isModalalumno1 === true ? this.isValor(itemAlumno, indexAlumno) : null}
            </View>
        </View>);
    }

    isValor(itemAlumno, indexAlumno) {
        return (<View style={styles.campoTablaG}>
            <View style={[styles.rowsCalif, styles.modalWidth]}>
                <Text>{itemAlumno.name}</Text>
            </View>
        </View>);
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async getCargos() {
        await fetch(this.state.uri + '/api/payment/dos/get/saldos/' + this.state.mes + '/' + this.state.elGrado + '/' + this.state.elGrupo, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({cargos: responseJson});
                }
            });
    }

    renderPago(itemPago, indexPago) {
        const alumnos = this.renderAlumnos();
        return (<View
            key={indexPago}
            style={[styles.row, {width: responsiveWidth(94), marginTop: 4}]}>
            <Modal
                isVisible={this.state.isModalalumno}
                backdropOpacity={0.8}
                animationIn={'bounceIn'}
                animationOut={'bounceOut'}
                animationInTiming={1000}
                animationOutTiming={1000}>
                <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(1.5), textAlign: 'center', marginTop: 15
                        }}>
                        Concepto del pago:
                    </Text>
                    <Text
                        style={{
                            textAlign: 'center', marginTop: 2, fontWeight: '600', fontSize: responsiveFontSize(2.5)
                        }}>
                        {this.state.elCargo.concepto}
                    </Text>
                    <Text style={[styles.main_title, styles.modalWidth]}>
                        Alumnos con pago pendiente:
                    </Text>
                    <ScrollView>
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}>
                            {alumnos}
                        </View>
                    </ScrollView>
                    <View style={[styles.modalWidth, styles.row,{justifyContent:'center'}]}>
                        <TouchableOpacity
                            style={[
                                styles.modalBigBtn,
                                {backgroundColor: '#fff', borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.setState({isModalalumno: false})}
                        >
                            <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            <Modal
                isVisible={this.state.isModalalumno1}
                backdropOpacity={0.8}
                animationIn={'bounceIn'}
                animationOut={'bounceOut'}
                animationInTiming={1000}
                animationOutTiming={1000}>
                <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                    <Text style={[styles.main_title, styles.modalWidth]}>
                        Lista de alumnos
                    </Text>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(1.5), textAlign: 'center', marginTop: 15
                        }}>
                        Concepto del pago:
                    </Text>
                    <Text
                        style={{
                            textAlign: 'center', marginTop: 2, fontWeight: '600', fontSize: responsiveFontSize(2.5)
                        }}>
                        {this.state.elCargo.concepto}
                    </Text>
                    <Text style={[styles.main_title, styles.modalWidth]}>
                        Alumnos con pago acreditado:
                    </Text>
                    <ScrollView>
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}>
                            {alumnos}
                        </View>
                    </ScrollView>
                    <View style={[styles.modalWidth, styles.row,{justifyContent:'center'}]}>
                        <TouchableOpacity
                            style={[
                                styles.modalBigBtn,
                                {backgroundColor: '#fff', borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.setState({isModalalumno1: false})}
                        >
                            <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>
            <View style={styles.btn8}>
                <Text numberOfLines={1}>{itemPago.concepto}</Text>
            </View>
            <TouchableOpacity
                style={[ styles.row, {
                    height: responsiveHeight(3.75),
                    backgroundColor: 'green',
                    borderRadius: 6,
                    marginTop: 3,
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    width:responsiveWidth(27)
                }]}
                onPress={() => this.setState({
                    isModalalumno1: itemPago.pagado > 0, elCargo: itemPago, alumnos: itemPago.pagaron
                })}>
                <Text
                    style={{
                        fontSize: responsiveFontSize(1.4), marginRight: 3, color: 'white'
                    }}>
                    ${Number(itemPago.pagado).toLocaleString('en-US')}
                </Text>
                <View style={{marginRight: 5}}>
                    <MaterialCommunityIcons
                        name='alert-circle-outline'
                        size={14}
                        color='white'
                    />
                </View>
            </TouchableOpacity>
            <TouchableOpacity
                style={[ styles.row, {
                    height: responsiveHeight(3.75),
                    backgroundColor: 'red',
                    borderRadius: 6,
                    marginTop: 3,
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    width:responsiveWidth(27)
                }]}
                onPress={() => this.setState({
                    isModalalumno: true, elCargo: itemPago, alumnos: itemPago.noPagaron
                })}>
                <Text
                    style={{
                        fontSize: responsiveFontSize(1.4), marginRight: 3, color: 'white',fontWeight:'700'
                    }}
                >
                    ${Number(itemPago.pendiente).toLocaleString('en-US')}
                </Text>
                <View style={{marginRight: 2}}>
                    <MaterialCommunityIcons
                        name='alert-circle-outline'
                        size={14}
                        color='white'
                    />
                </View>
            </TouchableOpacity>
        </View>);
    }

    renderPagos() {
        let buttonsPagos = [];
        this.state.cargos.forEach((itemPago, indexPago) => {
            buttonsPagos.push(this.renderPago(itemPago, indexPago));
        });
        return buttonsPagos;
    }

    render() {
        const listaPagos = this.renderPagos();
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (<View style={styles.container}>
            <Text
                style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>
                Elija el periodo a consultar
            </Text>
            <ModalSelector
                data={data}
                selectStyle={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 2}]}
                cancelText='Cancelar'
                optionTextStyle={{color: this.state.thirdColor}}
                initValue={mess}
                onChange={option => this.onChange(option)}
            />
            <GradoyGrupo
                onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos(indexBtn, itemBtn)}
                onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado(indexBtn, itemBtn)}
                todos={'1'}
            />
            <View style={{width: responsiveWidth(94)}}>
                <Text
                    style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 5, marginTop: 6}]}>
                    Seleccione un cargo a consultar
                </Text>
            </View>
            <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                <View style={styles.btn8}>
                    <Text style={{fontWeight: '700'}}>Concepto</Text>
                </View>
                <View style={styles.btn3_5_l}>
                    <Text style={{fontWeight: '700', marginLeft: 24}}>Pagado</Text>
                </View>
                <View style={styles.btn3_5_l}>
                    <Text style={{fontWeight: '700', marginLeft: 15}}>Pendiente</Text>
                </View>
            </View>
            <View
                style={{
                    height: responsiveHeight(30),
                    borderBottomWidth: 1,
                    borderTopWidth: 1,
                    marginBottom: 5,
                    borderColor: this.state.fourthColor
                }}
            >
                <ScrollView showsVerticalScrollIndicator={false}>
                    {listaPagos}
                </ScrollView>
            </View>
        </View>);
    }
}
