import React from 'react';
import {Alert, AsyncStorage, Image, ScrollView, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import GradoyGrupo from '../../Globales/GradoyGrupo';
import styles from '../../../styles';
import Modal from 'react-native-modal';
import {ifIphoneX} from "react-native-iphone-x-helper";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class estadoCuenta extends React.Component {
    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            textInputValue: '',
            mes: moment().month() + 1,
            cososPicker: [],
            grados: [],
            grupos: [],
            elCargo: '',
            elGrado: '',
            elGrupo: '',
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexGrupo: -1,
            selectedIndex: -1,
            elAlumno: '',
            alumnos: [],
            totalPagado: 0,
            totalPorPagar: 0,
            visible: false
        };
        // this.renderPagos = this.renderPagos.bind(this);
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        await this.setState({
            uri: await AsyncStorage.getItem("uri"),
            token: await AsyncStorage.getItem("token"),
            mainColor: await AsyncStorage.getItem("mainColor"),
            secondColor: await AsyncStorage.getItem("secondColor"),
            thirdColor: await AsyncStorage.getItem("thirdColor"),
            fourthColor: await AsyncStorage.getItem("fourthColor")
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo, elGrupo: grupo, selectedIndexAlumno: -1, cargos: [], elAlumno: ''
        });
        if (this.state.elGrado !== '') {
            await this.getAlumnos();
        }
    }

    // +++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado, elGrado: grado, selectedIndexAlumno: -1, cargos: [], elAlumno: ''
        });
        if (this.state.elGrupo !== '') {
            await this.getAlumnos();
        }
    }

    async getAlumnos() {
        await fetch(this.state.uri + '/api/user/alumnos/' + this.state.elGrado + '/' + this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error' + ' continua' + ' pónganse en contacto con soporte ((HP) Alumnos)', [{
                        text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'News'}})
                    }]);
                } else {
                    this.setState({alumnos: responseJson});
                }
            });
    }

    async onListPressedAlumno(indexAlumno, alumno, id, padre) {
        await this.setState({
            selectedIndexAlumno: indexAlumno,
            elAlumno: alumno,
            elId: id,
            padre: padre,
            selectedTipo: '',
            data: [],
            selItem: ''
        });
        await this.getCargos();
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno === indexAlumno) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<TouchableHighlight
            key={indexAlumno}
            underlayColor={this.state.secondColor}
            style={[tabRow, smallButtonStyles]}
            onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.id, itemAlumno.padre_id)}>
            <View>
                {this.state.isModalalumno === true ? this.isValor(itemAlumno, indexAlumno) : null}
            </View>
        </TouchableHighlight>);
    }

    isValor(itemAlumno, indexAlumno) {
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno === indexAlumno) {
            texto.push(styles.textoB);
        }
        return (<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
            <View>
                <Text style={[texto, {marginRight: 25}]}>{itemAlumno.name}</Text>
            </View>
        </View>);
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async getCargos() {
        await this._changeWheelState();
        await fetch(this.state.uri + '/api/payment/dos/estado/cuenta/' + this.state.elId, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                this._changeWheelState()
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({cargos: responseJson});
                }
            });
    }

    renderPago(itemPago, indexPago) {
        return (
            <View style={[styles.row, {backgroundColor: this.state.fourthColor, marginVertical: 3, borderRadius: 6}]}
                  key={indexPago}>
                <View style={{width: responsiveWidth(44)}}>
                    <Text style={{padding: 10}}>{itemPago.descripcion}</Text>
                </View>
                <View style={{width: responsiveWidth(30)}}>
                    <Text>${itemPago.transaction === null
                        ? Number(itemPago.importe_a_pagar).toLocaleString('en-US')
                        : Number(itemPago.transaction.total).toLocaleString('en-US')}</Text>
                </View>
                <View style={{width: responsiveWidth(20), alignItems: 'center'}}>
                    <Image style={{width: 25, height: 25}}
                           source={itemPago.transaction === null
                               ? {uri: "https://png.icons8.com/color/80/000000/cancel.png"}
                               : itemPago.transaction.status === 'En progreso'
                                   ? {uri: "https://png.icons8.com/color/80/000000/hibernate.png"}
                                   : {uri: "https://png.icons8.com/color/80/000000/ok.png"}}/>
                </View>
            </View>);
    }

    renderPagos() {
        let buttonsPagos = [];
        this.state.totalPagado = 0;
        this.state.totalPorPagar = 0;
        this.state.cargos.forEach((itemPago, indexPago) => {
            buttonsPagos.push(this.renderPago(itemPago, indexPago));
            if (itemPago.transaction !== null) {
                if (itemPago.transaction.status !== 'En progreso') {
                    this.state.totalPagado = Number(this.state.totalPagado) + Number(itemPago.transaction.total);
                }
                else {
                    this.state.totalPorPagar = Number(this.state.totalPorPagar) + Number(itemPago.transaction.total);
                }
            }
            else {
                this.state.totalPorPagar = Number(this.state.totalPorPagar) + Number(itemPago.importe_a_pagar);
            }
        });
        return buttonsPagos;
    }

    openModal() {
        if (this.state.elGrado === '' || this.state.elGrupo === '') {
            Alert.alert('Atención', 'Seleccione un grado y un grupo antes', [{
                text: 'Entendido'
            }]);
        } else if (this.state.alumnos.length === 0) {
            Alert.alert('Atención', 'Ya se le ha asiganado un beca a todos los alumnos de ' + this.state.elGrado + this.state.elGrupo, [{
                text: 'Entendido'
            }]);
        } else {
            this.setState({isModalalumno: true})
        }
    }

    render() {
        const alumnos = this.renderAlumnos();
        const listaPagos = this.renderPagos();
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (<View style={styles.container}>
            <ScrollView style={{marginBottom: responsiveHeight(5)}} showsVerticalScrollIndicator={false}>
                <Modal
                    isVisible={this.state.isModalalumno}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text style={[styles.main_title, styles.modalWidth]}>
                            Lista de alumnos
                        </Text>
                        <ScrollView>
                            <View
                                style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}>
                                {alumnos}
                            </View>
                        </ScrollView>
                        <View style={[styles.modalWidth, styles.row]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({
                                    isModalalumno: false,
                                    selectedIndexAlumno: -1,
                                    elAlumno: '',
                                    elId: '',
                                    padre: ''
                                })}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({isModalalumno: false})}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <GradoyGrupo
                    onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos(indexBtn, itemBtn)}
                    onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado(indexBtn, itemBtn)}
                    todos={'1'}/>
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 7}]}>
                    Seleccione el alumno
                </Text>
                <View style={[
                    styles.widthall, {
                        backgroundColor: this.state.fourthColor,
                        borderRadius: 6,
                        padding: 10,
                        alignItems: 'center'
                    }
                ]}>
                    <TouchableOpacity
                        style={[styles.bigButton, {
                            borderColor: this.state.secondColor,
                            width: responsiveWidth(47),
                            height: responsiveHeight(5),
                            borderWidth: 1,
                            borderRadius: 6,
                            ...ifIphoneX({marginBottom: 15}),
                            backgroundColor: 'white'
                        }]}
                        onPress={() => this.openModal()}>
                        <Text style={[styles.textButton, {color: 'black'}]}>
                            Ver lista
                        </Text>
                    </TouchableOpacity>
                    <Text style={{fontWeight: '600'}}>Alumno seleccionado</Text>
                    <Text style={[styles.textButton, {color: 'black', marginBottom: 2}]}>
                        {this.state.elAlumno}
                    </Text>
                </View>
                <View
                    style={{
                        borderBottomWidth: 1,
                        borderTopWidth: 1,
                        marginBottom: 5,
                        borderColor: this.state.fourthColor
                    }}>
                    <Text
                        style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 5, marginTop: 6}]}>
                        Totales
                    </Text>
                    <View style={[styles.row, {margin: 0, marginBottom: 3, padding: 0}]} className="centerAll">
                        <View
                            style={{
                                width: responsiveWidth(46)
                            }}>
                            <View
                                style={{
                                    margin: 3,
                                    padding: 10,
                                    alignItems: 'center',
                                    backgroundColor: "#f1f1f1",
                                    borderRadius: 10,
                                }}>
                                <Text>Total pagado</Text>
                                <Text style={{
                                    paddingVertical: 10,
                                    fontSize: responsiveFontSize(2.5),
                                    fontWeight: 'bold'
                                }}>${Number(this.state.totalPagado).toLocaleString('en-US')}</Text>
                            </View>
                        </View>
                        <View
                            style={{
                                width: responsiveWidth(46)
                            }}
                            className="centerAll">
                            <View
                                style={{
                                    margin: 3,
                                    padding: 10,
                                    alignItems: 'center',
                                    backgroundColor: "#f1f1f1",
                                    borderRadius: 10,
                                }}>
                                <Text>Total por pagar</Text>
                                <Text style={{
                                    paddingVertical: 10,
                                    fontSize: responsiveFontSize(2.5),
                                    fontWeight: 'bold',
                                    color: 'red'
                                }}>${Number(this.state.totalPorPagar).toLocaleString('en-US')}</Text>
                            </View>
                        </View>
                    </View>
                    <Text
                        style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 5, marginTop: 6}]}>
                        Lista de pagos
                    </Text>
                    <View style={[styles.row, {paddingBottom: 10}]}>
                        <View style={{width: responsiveWidth(44)}}>
                            <Text style={{fontWeight: '700'}}>Concepto</Text>
                        </View>
                        <View style={{width: responsiveWidth(30)}}>
                            <Text style={{fontWeight: '700'}}>Importe</Text>
                        </View>
                        <View style={{width: responsiveWidth(20), alignItems: 'center'}}>
                            <Text style={{fontWeight: '700'}}>Estatus</Text>
                        </View>
                    </View>
                    {listaPagos}
                </View>
            </ScrollView>
        </View>);
    }
}
