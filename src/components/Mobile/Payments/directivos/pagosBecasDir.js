import React from "react";
import {
    Alert,
    AsyncStorage,
    ScrollView,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from "react-native";
import styles from '../../../styles';
import ModalSelector from "react-native-modal-selector";
import GradoyGrupo from "../../Globales/GradoyGrupo";
import {responsiveFontSize, responsiveHeight, responsiveWidth} from "react-native-responsive-dimensions";
import Modal from "react-native-modal";
import Feather from "react-native-vector-icons/Feather";
import MultiBotonRow from '../../Globales/MultiBotonRow';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Spinner from "react-native-loading-spinner-overlay";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

export default class pagosBecasDir extends React.Component {
    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            alumnos: [],
            selected: [],
            isModalalumno: false,
            selectedIndexAlumno: '',
            elAlumno: '',
            elId: '',
            estadisticas: [],
            estAlumnos: [],
            descuento: 0,
            descuentoE: 0,
            maxBeca: 0,
            option: 'Académica',
            selectedIndex: 0,
            selectedButton: 0,
            alumnosBecados: [],
            alumnosBecados1: [],
            cp: 0,
            tipo: 'Académica',
            tipo1: 'Académica',
            tipoE: 'Académica',
            elGrado: '',
            elGrupo: '',
            edit: false
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getConfig();
        await this.getEstadisticas();
        await this.getAlumnosBecados(this.state.estAlumnos[0].porcentaje, 0)
    }

    componentDidMount() {
        let desc = this.state.maxColegiatura * (this.state.descuento / 100);
        this.setState({desc: desc});
        let total = this.state.maxColegiatura - desc;
        this.setState({beca: total});
    }

    async getURL() {
        await this.setState({
            uri: await AsyncStorage.getItem("uri"),
            token: await AsyncStorage.getItem("token"),
            mainColor: await AsyncStorage.getItem("mainColor"),
            secondColor: await AsyncStorage.getItem("secondColor"),
            thirdColor: await AsyncStorage.getItem("thirdColor"),
            fourthColor: await AsyncStorage.getItem("fourthColor")
        });
    }

    async getConfig() {
        await fetch(this.state.uri + '/api/global/config/values/pagos', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if (recurso.error !== undefined) {
                    alert("Error");
                } else {
                    this.setState({
                        maxBeca: Number(recurso[0].option_key),
                        maxColegiatura: Number(recurso[4].option_key)
                    });
                }
            });
    }

    async getEstadisticas() {
        await fetch(this.state.uri + '/api/get/becas/estadistcias/' + this.state.tipo1, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if (recurso.error !== undefined) {
                    alert("Error");
                } else {
                    this.setState({estadisticas: recurso});
                    this.setState({estAlumnos: recurso.alumnos, cp: 0});
                }
            });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo, elGrupo: grupo
        });
        if (this.state.elGrado !== '') {
            await this.getAlumnos();
        }
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado, elGrado: grado
        });
        if (this.state.elGrupo !== '') {
            await this.getAlumnos();
        }
    }

    async onListItemPressedGrupos1(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos1: indexGrupo, elGrupo1: grupo
        });
        if (this.state.elGrado1 !== '') {
            await this.getAlumnosBecados1();
        }
    }

    async onListItemPressedGrado1(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado, elGrado1: grado
        });
        if (this.state.elGrupo1 !== '') {
            await this.getAlumnosBecados1();
        }
    }

    //+++++ ALUMNOS +++++

    async getAlumnos() {
        await fetch(this.state.uri + '/api/user/alumnos/no/becados/' + this.state.elGrado + '/' + this.state.elGrupo, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if (recurso.error !== undefined) {
                    alert("Error: código: 15-G-0111");
                } else {
                    this.setState({alumnos: recurso});
                }
            });
    }

    onListPressedAlumno(i, it) {
        this.setState({
            selectedIndexAlumno: i
        });

        function esAlumno(alumno) {
            return alumno.name === it.name;
        }

        if (this.state.selected.find(esAlumno)) {
            let ix = this.state.selected.indexOf(this.state.selected.filter(function (item) {
                return item.name === it.name;
            })[0]);
            this.state.selected.splice(ix, 1);
            this.setState({aux: 0});
        } else {
            this.state.selected.push(it)
        }

    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];

        function esAlumno(alumno) {
            return alumno.name === itemAlumno.name;
        }

        if (this.state.selected.find(esAlumno)) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
        }
        return (<TouchableHighlight
            key={indexAlumno}
            underlayColor={this.state.secondColor}
            style={[tabRow, smallButtonStyles]}
            onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno)}>
            <View>
                {this.state.isModalalumno === true ? this.celdaAlumno(itemAlumno, indexAlumno) : null}
            </View>
        </TouchableHighlight>);
    }

    celdaAlumno(itemAlumno, indexAlumno) {
        let texto = [styles.textoN];

        function esAlumno(alumno) {
            return alumno.name === itemAlumno.name;
        }

        if (this.state.selected.find(esAlumno)) {
            texto.push(styles.textoB);
        }
        return (<View key={indexAlumno + 'Al'} style={styles.campoTablaG}>
            <View style={[styles.rowsCalif, styles.modalWidth]}>
                <Text style={texto}>{itemAlumno.name}</Text>
            </View>
        </View>);
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    // ++++ alumnos seleccionados ++++

    alumnosSeleccionados() {
        let buttonsAlumnos = [];
        this.state.selected.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno1(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    renderAlumno1(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.row];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [styles.rowTabla, {borderColor: this.state.secondColor}];
        return (<TouchableHighlight
            key={indexAlumno}
            underlayColor={this.state.secondColor}
            style={[tabRow, smallButtonStyles]}>
            <View>
                {this.celdaAlumno1(itemAlumno, indexAlumno)}
            </View>
        </TouchableHighlight>);
    }

    celdaAlumno1(item, indexAlumno) {
        let texto = [styles.textoN];
        return (<View key={indexAlumno + 'Al'} style={styles.campoTablaG}>
            <View style={[styles.row]}>
                <View style={[{width: responsiveWidth(52), justifyContent: 'center'}]}>
                    <Text style={texto}>{item.name}</Text>
                    <Text style={texto}>{this.state.tipo}</Text>
                </View>
                <View style={[styles.row, {width: responsiveWidth(14), justifyContent: 'center'}]}>
                    <Text style={texto}>{item.grado}{item.grupo}</Text>
                </View>
                <View style={[styles.row, {width: responsiveWidth(14), justifyContent: 'center'}]}>
                    <Text style={texto}>{this.state.descuento}%</Text>
                </View>
                <View style={[styles.row, {width: responsiveWidth(14), justifyContent: 'center'}]}>
                    <Feather name='minus-circle' size={22} color='red' onPress={() => this.borrar(indexAlumno)}/>
                </View>
            </View>
        </View>);
    }

    async borrar(i) {
        Alert.alert('¡Alerta!', '¿Seguro quiere eliminar a este alumno?', [{
            text: 'Si', onPress: () => [this.state.selected.splice(i, 1), this.setState({aux: 0})]
        },
            {text: 'No'}]);
        await this.setState({aux: 0});
    }

    descuento(text) {
        if (Number(text) <= Number(this.state.maxBeca) && Number(text) > 0) {
            this.setState({descuento: Number(text)});
            let desc = this.state.maxColegiatura * (Number(text) / 100);
            this.setState({desc: desc});
            let total = this.state.maxColegiatura - desc;
            this.setState({beca: total});
        }
        this.setState({aux: 0})
    }

    descuentoE(e) {
        if (e <= this.state.maxBeca && e > 0) {
            this.setState({descuentoE: e});
        }
    }

    // alumnos y porcentajes
    async getAlumnosBecados(p, c) {
        await this.setState({xtaje: p});
        await fetch(this.state.uri + '/api/get/becas/alumnos/porcentajes/' + p + '/' + this.state.tipo1, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if (recurso.error !== undefined) {
                    alert("Error: código: 15-G-0111");
                } else {
                    this.setState({alumnosBecados: recurso, cp: c});
                }
            });
    }

    async getAlumnosBecados1() {
        await fetch(this.state.uri + '/api/get/becados/' + this.state.elGrado1 + '/' + this.state.elGrupo1, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if (recurso.error !== undefined) {
                    alert("Error: código: 15-G-0111");
                } else {
                    this.setState({alumnosBecados1: recurso});
                }
            });
    }

    renderPorcentajes() {
        let row = [];
        if (this.state.estadisticas.length !== 0) {
            for (let i = 0; i < this.state.estAlumnos.length; i = i + 3) {
                row.push(
                    <View style={[styles.row, {marginTop: 15}]} key={i + 'porcentaje'}>
                        <TouchableOpacity
                            onPress={() => this.getAlumnosBecados(this.state.estAlumnos[i].porcentaje, i)}>
                            <View style={[styles.row, {width: responsiveWidth(30)}]}>
                                <View style={{width: responsiveWidth(12)}}>
                                    <Text style={{textAlign: 'center', color: 'black', fontSize: 10}}>Alumnos</Text>
                                </View>
                                <View style={{width: responsiveWidth(18)}}>
                                    <Text style={{color: 'black', fontSize: 10, textAlign: 'center'}}>Porcentaje</Text>
                                </View>
                            </View>
                            <View
                                style={[styles.row, {
                                    textAlign: 'center',
                                    backgroundColor: this.state.cp === i ? '#d1d1d1' : '#f1f1f1',
                                    borderRadius: 7,
                                    width: responsiveWidth(30)
                                }]}>
                                <View style={{width: responsiveWidth(10)}}>
                                    <Text
                                        style={{textAlign: 'center'}}>{this.state.estAlumnos[i].alumnos.length}</Text></View>
                                <View style={{width: responsiveWidth(20), padding: responsiveHeight(.5)}}>
                                    <View style={{
                                        borderColor: this.state.secondColor, borderWidth: 1, backgroundColor: 'white',
                                        borderRadius: 7, paddingVertical: responsiveHeight(.5)
                                    }}>
                                        <Text style={{
                                            color: this.state.secondColor,
                                            textAlign: 'center'
                                        }}>{this.state.estAlumnos[i].porcentaje}%</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        {this.state.estAlumnos[i + 1] !== undefined
                            ? <TouchableOpacity
                                onPress={() => this.getAlumnosBecados(this.state.estAlumnos[i + 1].porcentaje, i + 1)}>
                                <View style={[styles.row, {width: responsiveWidth(30)}]}>
                                    <View style={{width: responsiveWidth(12)}}>
                                        <Text style={{textAlign: 'center', color: 'black', fontSize: 10}}>Alumnos</Text>
                                    </View>
                                    <View style={{width: responsiveWidth(18)}}>
                                        <Text style={{
                                            color: 'black',
                                            fontSize: 10,
                                            textAlign: 'center'
                                        }}>Porcentaje</Text>
                                    </View>
                                </View>
                                <View
                                    style={[styles.row, {
                                        textAlign: 'center',
                                        backgroundColor: this.state.cp === i + 1 ? '#d1d1d1' : '#f1f1f1',
                                        borderRadius: 7,
                                        width: responsiveWidth(30)
                                    }]}>
                                    <View style={{width: responsiveWidth(10)}}>
                                        <Text
                                            style={{textAlign: 'center'}}>{this.state.estAlumnos[i + 1].alumnos.length}</Text></View>
                                    <View style={{width: responsiveWidth(20), padding: responsiveHeight(.5)}}>
                                        <View style={{
                                            borderColor: this.state.secondColor,
                                            borderWidth: 1,
                                            backgroundColor: 'white',
                                            borderRadius: 7,
                                            paddingVertical: responsiveHeight(.5)
                                        }}>
                                            <Text style={{
                                                color: this.state.secondColor,
                                                textAlign: 'center'
                                            }}>{this.state.estAlumnos[i + 1].porcentaje}%</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            : <View style={{width: responsiveWidth(30)}}/>}
                        {this.state.estAlumnos[i + 2] !== undefined
                            ? <TouchableOpacity
                                onPress={() => this.getAlumnosBecados(this.state.estAlumnos[i + 2].porcentaje, i + 2)}>
                                <View style={[styles.row, {width: responsiveWidth(30)}]}>
                                    <View style={{width: responsiveWidth(12)}}>
                                        <Text style={{textAlign: 'center', color: 'black', fontSize: 10}}>Alumnos</Text>
                                    </View>
                                    <View style={{width: responsiveWidth(18)}}>
                                        <Text style={{
                                            color: 'black',
                                            fontSize: 10,
                                            textAlign: 'center'
                                        }}>Porcentaje</Text>
                                    </View>
                                </View>
                                <View
                                    style={[styles.row, {
                                        textAlign: 'center',
                                        backgroundColor: this.state.cp === i + 2 ? '#d1d1d1' : '#f1f1f1',
                                        borderRadius: 7,
                                        width: responsiveWidth(30)
                                    }]}>
                                    <View style={{width: responsiveWidth(10)}}>
                                        <Text
                                            style={{textAlign: 'center'}}>{this.state.estAlumnos[i + 2].alumnos.length}</Text></View>
                                    <View style={{width: responsiveWidth(20), padding: responsiveHeight(.5)}}>
                                        <View style={{
                                            borderColor: this.state.secondColor,
                                            borderWidth: 1,
                                            backgroundColor: 'white',
                                            borderRadius: 7,
                                            paddingVertical: responsiveHeight(.5)
                                        }}>
                                            <Text style={{
                                                color: this.state.secondColor,
                                                textAlign: 'center'
                                            }}>{this.state.estAlumnos[i + 2].porcentaje}%</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            : <View style={{width: responsiveWidth(30)}}/>}
                    </View>
                );
            }
        }
        return row;
    }

    renderAlumnosBecados() {
        let alumnos = [];
        this.state.alumnosBecados.forEach((it, i) => {
            alumnos.push(
                <View key={'becado' + i}
                      style={[styles.row, {
                          margin: 0,
                          padding: 5,
                          borderRadius: 7,
                          backgroundColor: "#f1f1f1",
                          marginTop: 3
                      }]}>
                    <View style={{width: responsiveWidth(45)}}>
                        <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                            <Text style={{textAlign: "left"}}>{it.nombre.name}</Text>
                        </View>
                        <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                            <Text className="work">{it.tipo}</Text>
                        </View>
                    </View>
                    <View style={{textAlign: "center"}}>
                        <View style={{margin: 0, padding: 0}}>
                            <Text>{it.nombre.grado + it.nombre.grupo}</Text>
                        </View>
                    </View>
                    <View style={{textAlign: "center"}}>
                        <View style={{margin: 0, padding: 0}}>
                            <Text>{it.porcentaje}%</Text>
                        </View>
                    </View>
                    <View style={{textAlign: "center"}}>
                        <Feather name='minus-circle' size={22} color='red'
                                 onPress={() => this.borrarBecado(it.id)}/>
                    </View>
                </View>);
        });
        return alumnos;
    }

    renderAlumnosBecados1() {
        let alumnos = [];
        this.state.alumnosBecados1.forEach((it, i) => {
            alumnos.push(
                <View key={'becadod' + i}
                      style={[styles.row, {
                          margin: 0,
                          padding: 5,
                          borderRadius: 7,
                          backgroundColor: "#f1f1f1",
                          marginTop: 3
                      }]}>
                    <View style={{width: responsiveWidth(45)}}>
                        <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                            <Text style={{textAlign: "left"}}>{it.nombre.name}</Text>
                        </View>
                        <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                            <Text className="work">{it.tipo}</Text>
                        </View>
                    </View>
                    <View style={{textAlign: "center"}}>
                        <View style={{margin: 0, padding: 0}}>
                            <Text>{it.porcentaje}%</Text>
                        </View>
                    </View>
                    <View style={{textAlign: "center"}}>
                        <MaterialIcons name='edit' size={22} color='gray'
                                       onPress={() => this.setState({edit: true, editId: it.id})}/>
                    </View>
                </View>);
        });
        return alumnos;
    }

    async borrarBecado(id) {
        await fetch(this.state.uri + '/api/delete/becado/' + id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if (recurso.error !== undefined) {
                    Alert.alert('Atención', 'No se ha podido eliminar la beca si el error persiste contactar con soporte', [{
                        text: 'Entendido'
                    }]);
                } else {
                    if (this.state.elGrupo !== '' && this.state.elGrado !== '') {
                        this.getAlumnos();
                    }
                    this.getEstadisticas();
                    this.getAlumnosBecados(this.state.xtaje);
                    Alert.alert('Felicidades', 'Se ha eliminado la beca correctamente', [{
                        text: 'Entendido'
                    }]);
                }
            });
    }

    async editar() {
        await this._changeWheelState();
        if (this.state.tipoE === '') {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta seleccionar el tipo de beca', [{
                text: 'Entendido'
            }]);
        } else if (this.state.descuentoE === 0) {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta el porcentaje de beca', [{
                text: 'Entendido'
            }]);
        } else if (Number(this.state.descuentoE) >= Number(this.state.maxBeca)) {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'El porcentaje de beca máximo es de ' + this.state.maxBeca, [{
                text: 'Entendido'
            }]);
        } else {
            await fetch(this.state.uri + '/api/edit/becado/' + this.state.editId + '/' + this.state.tipoE + '/' + this.state.descuentoE, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            })
                .then(response => {
                    return response.json();
                })
                .then(recurso => {
                    this._changeWheelState();
                    if (recurso.error !== undefined) {
                        Alert.alert('Atención', 'No se ha podido editar la beca si el error persiste contactar con soporte', [{
                            text: 'Entendido'
                        }]);
                    } else {
                        this.getEstadisticas();
                        this.getAlumnosBecados1();

                        Alert.alert('Felicidades', 'Se ha actualizado la beca correctamente', [{
                            text: 'Entendido', onPress: () => this.setState({edit: false})
                        }]);
                    }
                });
        }
    }

    onChange(option) {
        this.setState({tipo: option.label})
    }

    onChangeE(option) {
        this.setState({tipoE: option.label})
    }

    async onChange1(option) {
        await this.setState({tipo1: option.label});
        await this.getEstadisticas()
        await this.getAlumnosBecados(this.state.estAlumnos[0].porcentaje, 0)
    }

    openModal() {
        if (this.state.elGrado === '' || this.state.elGrupo === '') {
            Alert.alert('Atención', 'Seleccione un grado y un grupo antes', [{
                text: 'Entendido'
            }]);
        } else if (this.state.alumnos.length === 0) {
            Alert.alert('Atención', 'Ya se le ha asiganado un beca a todos los alumnos de ' + this.state.elGrado + this.state.elGrupo, [{
                text: 'Entendido'
            }]);
        } else {
            this.setState({isModalalumno: true})
        }
    }

    // ++++ render ++++

    render1() {
        let i = 0;
        const data = [
            {key: i++, label: "Académica"},
            {key: i++, label: "Deportiva"},
            {key: i++, label: "Institucional"},
            {key: i++, label: "Hermanos"}
        ];
        const alumnos = this.renderAlumnos();
        return (
            <ScrollView style={{...ifIphoneX({marginBottom: 30}, {marginBottom: 30})}}
                        showsVerticalScrollIndicator={false}>
                <Text
                    style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>
                    Indique el tipo de beca </Text>
                <ModalSelector
                    data={data}
                    selectStyle={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 2}]}
                    cancelText='Cancelar'
                    optionTextStyle={{color: this.state.thirdColor}}
                    initValue='Académica'
                    onChange={option => this.onChange(option)}
                />
                <Text
                    style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>
                    Porcentaje de beca
                </Text>
                <TextInput
                    style={{
                        height: responsiveHeight(5),
                        width: responsiveWidth(93.5),
                        borderColor: this.state.secondColor,
                        borderRadius: 6,
                        textAlign: 'center',
                        borderWidth: 1
                    }}
                    onChangeText={(text) => this.descuento(text)}
                    underlineColorAndroid={'transparent'}
                    placeholder="10%"
                    maxLength={3}
                    defaultValue={'' + this.state.descuento}
                    value={'' + this.state.descuento}
                    keyboardType={'numeric'}
                    keyboardAppearance={'dark'}
                />
                <GradoyGrupo
                    onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos(indexBtn, itemBtn)}
                    onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado(indexBtn, itemBtn)}
                    todos={'0'}
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione los alumnos
                </Text>
                <View style={{alignItems: 'center', backgroundColor: this.state.fourthColor}}>
                    <TouchableOpacity
                        style={[styles.bigButton, {
                            borderColor: this.state.secondColor,
                            width: responsiveWidth(47),
                            height: responsiveHeight(5),
                            borderWidth: 1,
                            borderRadius: 6,
                            backgroundColor: 'white'
                        }]}
                        onPress={() => this.openModal()}>
                        <Text style={[styles.textButton, {color: 'black'}]}>
                            Ver lista
                        </Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    isVisible={this.state.isModalalumno}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(1.5), textAlign: 'center', marginTop: 15
                            }}>
                            Seleccione los alumnos:
                        </Text>
                        <ScrollView>
                            <View
                                style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}>
                                {alumnos}
                            </View>
                        </ScrollView>
                        <View style={[styles.modalWidth, styles.row, {justifyContent: 'center'}]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({isModalalumno: false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </Modal>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Alumnos seleccionados
                </Text>
                <ScrollView style={{maxHeight: responsiveHeight(40)}}>
                    {this.alumnosSeleccionados()}
                </ScrollView>
                {this.state.selected.length !== 0 ?
                    <TouchableOpacity
                        style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
                        onPress={() => this.guardar()}>
                        <Text style={styles.textButton}>Guardar</Text>
                    </TouchableOpacity> : null}
            </ScrollView>
        )
    }

    async guardar() {
        await this._changeWheelState();
        if (this.state.tipo === '') {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta seleccionar el tipo de beca', [{
                text: 'Entendido'
            }]);
        } else if (this.state.descuento === 0) {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'Falta el porcentaje de beca', [{
                text: 'Entendido'
            }]);
        } else if (Number(this.state.descuento) >= Number(this.state.maxBeca)) {
            await this._changeWheelState();
            Alert.alert('¡Atención!', 'El porcentaje de beca máximo es de ' + this.state.maxBeca, [{
                text: 'Entendido'
            }]);
        } else {
            const body = new FormData();
            let sel = [];
            this.state.selected.forEach((it, i) => {
                sel.push({'user_id': it.id, 'porcentaje': this.state.descuento, 'tipo': this.state.tipo});
            });
            body.append('new', JSON.stringify(
                sel
            ));
            await fetch(this.state.uri + '/api/save/becas', {
                method: 'POST', headers: {
                    Accept: 'application/json', 'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }, body: body
            }).then(res => res.json())
                .then(responseJson => {
                    this._changeWheelState();
                    if (responseJson.error !== undefined) {
                    } else {
                        Alert.alert('¡Felicidades!', 'Se han asignado las becas correctamente', [{
                            text: 'Entendido'
                        }]);
                        this.setState({data: responseJson});
                        this.getEstadisticas();
                        this.getAlumnos();
                        this.setState({selected: []});
                        if (this.state.xtaje !== '') {
                            this.getAlumnosBecados(this.state.xtaje);
                        }
                    }
                });
        }
    }

    render2() {
        let i = 0;
        const data = [
            {key: i++, label: "Académica"},
            {key: i++, label: "Deportiva"},
            {key: i++, label: "Institucional"},
            {key: i++, label: "Hermanos"}
        ];
        let tBecas = this.state.maxColegiatura * (this.state.estadisticas.porcentajeTotal / 100);
        return (
            <ScrollView ref={ref => this.scrollView = ref}
                        onContentSizeChange={(contentWidth, contentHeight) => {
                            this.scrollView.scrollToEnd({animated: true});
                        }} style={{...ifIphoneX({marginBottom: 30}, {marginBottom: 30})}}>
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>Estadísticas de
                    becas</Text>
                <ModalSelector
                    data={data}
                    selectStyle={[[styles.inputPicker, {borderColor: this.state.secondColor}], {marginTop: 2}]}
                    cancelText='Cancelar'
                    optionTextStyle={{color: this.state.thirdColor}}
                    initValue='Académica'
                    onChange={option => this.onChange1(option)}
                />
                <View style={[styles.row, {margin: 0, marginBottom: 3, padding: 0, marginTop: 13}]}
                      className="centerAll">
                    <View
                        style={{
                            width: responsiveWidth(46)
                        }}>
                        <View
                            style={{
                                margin: 3,
                                padding: 10,
                                alignItems: 'center',
                                backgroundColor: "#f1f1f1",
                                borderRadius: 10,
                            }}>
                            <Text>Alumnos becados</Text>
                            <Text style={{
                                paddingVertical: 10,
                                fontSize: responsiveFontSize(2.5),
                                fontWeight: 'bold'
                            }}>{this.state.estadisticas.alumnosTotal}</Text>
                        </View>
                    </View>
                    <View
                        style={{
                            width: responsiveWidth(46)
                        }}
                        className="centerAll">
                        <View
                            style={{
                                margin: 3,
                                padding: 10,
                                alignItems: 'center',
                                backgroundColor: "#f1f1f1",
                                borderRadius: 10,
                            }}>
                            <Text>Porcentaje asigando</Text>
                            <Text style={{
                                paddingVertical: 10,
                                fontSize: responsiveFontSize(2.5),
                                fontWeight: 'bold'
                            }}>{this.state.estadisticas.porcentajeTotal}%</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.row}>
                    <View
                        style={{
                            width: responsiveWidth(46)
                        }}
                        className="centerAll">
                        <View
                            style={{
                                margin: 3,
                                padding: 10,
                                alignItems: 'center',
                                backgroundColor: "#f1f1f1",
                                borderRadius: 10,
                            }}>
                            <Text>Total en becas</Text>
                            <Text style={{
                                paddingVertical: 10,
                                fontSize: responsiveFontSize(2.5),
                                fontWeight: 'bold'
                            }}>{Number(tBecas).toLocaleString('en-US')}</Text>
                        </View>
                    </View>
                    <View
                        style={{
                            width: responsiveWidth(46),
                        }}
                        className="centerAll">
                        <View
                            style={{
                                margin: 3,
                                padding: 10,
                                alignItems: 'center',
                                backgroundColor: "#f1f1f1",
                                borderRadius: 10,
                            }}>
                            <Text>Beca promedio</Text>
                            <Text style={{
                                paddingVertical: 10,
                                fontSize: responsiveFontSize(2.5),
                                fontWeight: 'bold'
                            }}>{this.state.estadisticas.promedio}%</Text>
                        </View>
                    </View>
                </View>
                {this.state.estadisticas.alumnosTotal !== 0 ? <View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 1}]}>Consulta por
                        porcentaje</Text>
                    <Text style={[{marginBottom: 1}]}>Seleccione un porcentaje</Text>
                    {this.renderPorcentajes()}
                    <Text style={[styles.main_title, {
                        color: this.state.thirdColor,
                        marginBottom: 1
                    }]}>Alumnos
                        con {this.state.estAlumnos.length !== 0 ? this.state.estAlumnos[this.state.cp].porcentaje + '%' : null} de
                        beca</Text>
                    <View
                        style={[styles.row, {
                            margin: 0,
                            padding: 5,
                            borderRadius: 7,
                            backgroundColor: "#f1f1f1",
                            marginTop: 3
                        }]}>
                        <View style={{width: responsiveWidth(45)}}>
                            <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                                <Text style={{textAlign: "left"}}>Nombre del alumno</Text>
                            </View>
                            <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                                <Text className="work">Tipo de beca</Text>
                            </View>
                        </View>
                        <View style={{textAlign: "center"}}>
                            <View style={{margin: 0, padding: 0}}>
                                <Text>Porcentaje</Text>
                            </View>
                        </View>
                        <View style={{textAlign: "center"}}>
                            <Text>Eliminar</Text>
                        </View>
                    </View>
                    <View style={{marginTop: 15}}>{this.renderAlumnosBecados()}</View>
                </View> : null}
            </ScrollView>
        )
    }

    render3() {
        let i = 0;
        const data = [
            {key: i++, label: "Académica"},
            {key: i++, label: "Deportiva"},
            {key: i++, label: "Institucional"},
            {key: i++, label: "Hermanos"}
        ];
        return (
            <ScrollView style={{...ifIphoneX({marginBottom: 30}, {marginBottom: 30})}}>
                <GradoyGrupo
                    onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos1(indexBtn, itemBtn)}
                    onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado1(indexBtn, itemBtn)}
                    todos={'0'}/>
                <View
                    style={[styles.row, {
                        margin: 0,
                        padding: 5,
                        borderRadius: 7,
                        backgroundColor: "#f1f1f1",
                        marginTop: 3
                    }]}>
                    <View style={{width: responsiveWidth(45)}}>
                        <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                            <Text style={{textAlign: "left"}}>Nombre del alumno</Text>
                        </View>
                        <View style={{textAlign: "left", margin: 0, padding: 0, paddingLeft: 5}}>
                            <Text className="work">Tipo de beca</Text>
                        </View>
                    </View>
                    <View style={{textAlign: "center"}}>
                        <View style={{margin: 0, padding: 0}}>
                            <Text>Porcentaje</Text>
                        </View>
                    </View>
                    <View style={{textAlign: "center"}}>
                        <Text>Editar</Text>
                    </View>
                </View>
                {this.renderAlumnosBecados1()}
                <Modal
                    isVisible={this.state.edit}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text
                            style={[styles.main_title, styles.modalWidth, {
                                color: this.state.thirdColor,
                                marginBottom: 1
                            }]}>
                            Indique el tipo de beca </Text>
                        <ModalSelector
                            data={data}
                            selectStyle={[[styles.inputPicker, styles.modalWidth, {borderColor: this.state.secondColor}], {marginTop: 2}]}
                            cancelText='Cancelar'
                            optionTextStyle={{color: this.state.thirdColor}}
                            initValue='Académica'
                            onChange={option => this.onChangeE(option)}
                        />
                        <Text
                            style={[styles.main_title, styles.modalWidth, {
                                color: this.state.thirdColor,
                                marginBottom: 1
                            }]}>
                            Porcentaje de beca
                        </Text>
                        <TextInput
                            style={[styles.modalWidth, {
                                height: responsiveHeight(5),
                                borderColor: this.state.secondColor,
                                borderRadius: 6,
                                textAlign: 'center',
                                borderWidth: 1
                            }]}
                            onChangeText={(text) => this.descuentoE(text)}
                            underlineColorAndroid={'transparent'}
                            placeholder="10%"
                            maxLength={3}
                            defaultValue={'' + this.state.descuentoE}
                            value={'' + this.state.descuentoE}
                            keyboardType={'numeric'}
                            keyboardAppearance={'dark'}
                        />
                        <View style={[styles.modalWidth, styles.row]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({edit: false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.editar()}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        )
    }

    async onSelectedButton(indexBtn, itemBtn) {
        this.setState({
            selectedIndex: indexBtn,
            selectedButton: itemBtn,
            alumnosBecados1: [],
            elGrado: '',
            elGrado1: '',
            elGrupo: '',
            elGrupo1: ''
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} textContent='Procesando...'/>
                <MultiBotonRow
                    itemBtns={['Agregar becas', 'Editar becas', 'Estadísticos']}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.onSelectedButton(indexBtn, itemBtn)
                    }
                    cantidad={3}/>
                {this.state.selectedIndex === 0 ? this.render1()
                    : this.state.selectedIndex === 1
                        ? this.render3()
                        : this.render2()
                }
            </View>
        );
    }
}
