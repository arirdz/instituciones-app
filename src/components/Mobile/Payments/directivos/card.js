import React from 'react';
import {Alert, AsyncStorage, Image, ImageBackground, Text, View} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import Entypo from "react-native-vector-icons/Entypo";
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from "react-native-router-flux";
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class gestionarTarjetas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        };
    }

    _keyExtractor = item => item.id;

    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        await this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async borrarTarjeta() {
        await this._changeWheelState();
        let email_director = await AsyncStorage.getItem('email_director');
        console.log('http://127.0.0.1:8080' + '/api/customer/delete/card/' + this.props.id +'/'+ email_director )
        await fetch('http://127.0.0.1:8080' + '/api/customer/delete/card/' + this.props.id +'/'+ email_director , {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => {
            if (res.status === 200) {
                Alert.alert('¡Tarjeta eliminada!', 'Se ha borrado correctamente la tarjeta', [{
                    text: 'Entendido', onPress: () => [this._changeWheelState(), Actions.refresh({key: Math.random()})]
                }]);
            }
        });
    }

    render() {
        return (
            <View
                style={[styles.container, styles.widthall, {
                    ...ifIphoneX({
                        height: responsiveHeight(25),
                    }, {
                        height: responsiveHeight(30),
                    }),
                    marginVertical: 10,
                    borderRadius: 21,
                    borderWidth: .5,
                    padding: 1,
                    borderColor: '#a8a8a8'
                }]}>
                <Spinner visible={this.state.visible} textContent='Procesando...'/>
                <ImageBackground
                    style={[styles.container, {
                        resizeMode: 'contain',
                        paddingTop: 12
                    }]}
                    source={require('../../../../images/map-01-01.png')}>
                    <View
                        style={[styles.row, {
                            width: responsiveWidth(85),
                            ...ifIphoneX({
                                marginTop: responsiveHeight(.5)
                            }, {
                                marginTop: responsiveHeight(1.5)
                            }),

                        }]}>
                        {this.props.bank_name === 'AFIRME' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/AFIRME.png')}/>
                        ) : null}
                        {this.props.bank_name === 'AMERICAN EXPRESS' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(9), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/AMEX.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANCAMIFEL' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(9), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANCAMIFEL.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANCOAZTECA' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(9), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANCOAZTECA.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANCOMER' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(6), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANCOMER.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANCOPPEL' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(9), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANCOPPEL.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANORTE' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANORTE.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANREGIO' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANREGIO.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANSEFI' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANSEFI.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANSI' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BANSI.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BX+' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/BX+.png')}/>
                        ) : null}
                        {this.props.bank_name === 'Banamex' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(6), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/CITIBANAMEX.png')}/>
                        ) : null}
                        {this.props.bank_name === 'COMPARTAMOS BANCO' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(6), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/COMPARTAMOS.png')}/>
                        ) : null}
                        {this.props.bank_name === 'FAMSA' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/FAMSA.png')}/>
                        ) : null}
                        {this.props.bank_name === 'HSBC' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/HSBC.png')}/>
                        ) : null}
                        {this.props.bank_name === 'INBURSA' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/INBURSA.png')}/>
                        ) : null}
                        {this.props.bank_name === 'INTERACCIONES' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/INTERACCIONES.png')}/>
                        ) : null}
                        {this.props.bank_name === 'INVEX' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/INVEX.png')}/>
                        ) : null}
                        {this.props.bank_name === 'MULTIVA' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/MULTIVA.png')}/>
                        ) : null}
                        {this.props.bank_name === 'BANCO SANTANDER SERFIN S.A.' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/SANTANDER.png')}/>
                        ) : null}
                        {this.props.bank_name === 'SCOTIABANK' ? (
                            <Image
                                style={{width: responsiveWidth(34), height: responsiveHeight(8), resizeMode: 'contain'}}
                                source={require('../../../../images/bancos/SCOTIABANK.png')}/>
                        ) : null}
                        <Entypo name='circle-with-minus' size={22} color='red'
                                onPress={() => this.borrarTarjeta()}/>
                    </View>
                    <View style={{width: responsiveWidth(85),
                        ...ifIphoneX({
                            marginTop: responsiveHeight(2.5)
                        }, {
                            marginTop: responsiveHeight(4.5)
                        }),}}>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'flex-start'
                            }}>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontWeight: '800',
                                    fontSize: responsiveFontSize(2.5)
                                }}>
                                {' '}
                                {this.props.card_number === undefined
                                    ? 'XXXX'
                                    : this.props.card_number.substr(0, 4)}{' '}
                            </Text>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontWeight: '800',
                                    fontSize: responsiveFontSize(2.5)
                                }}>
                                {' '}
                                {this.props.card_number === undefined
                                    ? 'XXXX'
                                    : this.props.card_number.substr(4, 4)}{' '}
                            </Text>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontWeight: '800',
                                    fontSize: responsiveFontSize(2.5)
                                }}>
                                {' '}
                                {this.props.card_number === undefined
                                    ? 'XXXX'
                                    : this.props.card_number.substr(8, 4)}{' '}
                            </Text>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontWeight: '800',
                                    fontSize: responsiveFontSize(2.5)
                                }}>
                                {' '}
                                {this.props.card_number === undefined
                                    ? 'XXXX'
                                    : this.props.card_number.substr(12, 4)}{' '}
                            </Text>
                        </View>
                        <Text style={{color: 'grey', fontSize: responsiveFontSize(1), marginLeft: 5}}>
                            NÚMERO DE TARJETA
                        </Text>
                    </View>
                    <View
                        style={{
                            width: responsiveWidth(85),
                            flexDirection: 'row',
                            justifyContent: 'center',
                            ...ifIphoneX({
                                marginTop: responsiveHeight(1)
                            }, {
                                marginTop: responsiveHeight(2)
                            }),
                        }}>
                        <View
                            style={{
                                width: responsiveWidth(49),
                                marginHorizontal: 10,
                                justifyContent: 'center',
                                alignItems: 'flex-start'
                            }}>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontWeight: '800',
                                    width: responsiveWidth(49),
                                    fontSize: responsiveFontSize(1.7)
                                }}>
                                {this.props.holder_name === undefined
                                    ? 'XXXX XXXXXX XXX'
                                    : this.props.holder_name.toUpperCase()}{' '}
                            </Text>
                            <Text style={{color: 'grey', fontSize: responsiveFontSize(1)}}>
                                TARJETA HABIENTE
                            </Text>
                        </View>
                        <View
                            style={{
                                width: responsiveWidth(14.5),
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 6
                            }}>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontWeight: '800',
                                    fontSize: responsiveFontSize(1.5)
                                }}>
                                {this.props.expiration_month === undefined
                                    ? 'XX'
                                    : this.props.expiration_month}{' '}
                                /{' '}
                                {this.props.expiration_year === undefined
                                    ? 'XX'
                                    : this.props.expiration_year}
                            </Text>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontSize: responsiveFontSize(1)
                                }}>
                                VENCIMIENTO
                            </Text>
                        </View>
                        <View
                            style={{
                                width: responsiveWidth(20),
                                marginLeft: 7,
                                alignItems: 'flex-end'
                            }}>
                            {this.props.brand === 'visa' ? (
                                <Image
                                    style={[
                                        styles.imagen,
                                        {
                                            width: responsiveWidth(18),
                                            height: responsiveHeight(9)
                                        }
                                    ]}
                                    source={require('../../../../images/bancos/VISA.png')}/>
                            ) : null}
                            {this.props.brand === 'mastercard' ? (
                                <Image
                                    style={[
                                        styles.imagen,
                                        {
                                            width: responsiveWidth(18),
                                            height: responsiveHeight(9)
                                        }
                                    ]}
                                    source={require('../../../../images/bancos/MASTERCARD.png')}/>
                            ) : null}
                            {this.props.brand === 'american_express' ? (
                                <Image
                                    style={
                                        {
                                            width: responsiveWidth(18),
                                            resizeMode: 'contain',
                                            height: responsiveHeight(9)
                                        }
                                    }
                                    source={require('../../../../images/bancos/AMEX2.png')}/>
                            ) : null}
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
