import React from 'react';
import {Alert, AsyncStorage, Linking, Text, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import ModalSelector from 'react-native-modal-selector';
import openpay from 'react-native-openpay';

const moment = require('moment');

require('moment/locale/es');
moment.locale('es');

export default class pagoTarjetaServicio extends React.Component {
    getCardsByUser = () => {
        let formData = new FormData();
        formData.append('parameters', JSON.stringify({
            method: 'card',
            amount: this.props.FinalPrice,
            description: this.state.itemDescripcion,
            order_id: 'CE-0' + this.state.itempay + '-' + this.state.elHijo + '-' + this.props.intento,
            device_session_id: this.state.session,
            source_id: this.state.idTarjeta,
            currency: 'MXN',
            use_3d_secure: true,
            redirect_url: this.state.uri + '/Pago'
        }));
        fetch(this.state.uri + '/api/pasarelas/v1/openpay/customer/' + this.state.userID + '/bank/charge', {
            method: 'POST', headers: {
                'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
            }, body: formData
        })
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.response !== 'error') {
                    Linking.openURL(responseJson.result.payment_method.url);
                    Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
                        text: 'Gracias', onPress: () => Actions.popTo('GestionarPagos')
                    }]);
                    this.setState({recibo: responseJson});
                } else {
                    if (responseJson.error.code === 3001) {
                        Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3002) {
                        Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirada', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3003) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3004) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
                            text: 'Entendido'
                        }]);
                    } else if (responseJson.error.code === 3005) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
                            text: 'Entendido'
                        }]);

                    } else if (responseJson.error.code === 1006) {
                        Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
                            text: 'Entendido'
                        }]);
                    } else {
                        Linking.openURL(responseJson.result.payment_method.url);
                        Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
                            text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
                        }]);

                    }
                }
            })
            .catch(error => {
                Alert.alert('¡Ups!', 'No se pudo generar el cargo, intente más tarde', [{
                    text: 'Gracias', onPress: () => Actions.popTo('GestionarPagos')
                }]);
            });
    };

    constructor(props) {
        super(props);
        this.state = {
            cardData: [], datos: [], userRef: [], idTarjeta: '', textInputValue: '', session: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getCardByUserPay();
        console.log(this.props)
    }

    async getURL() {
        openpay.setup('m73t5j5ivwnszh4eytqc', 'sk_714b4062c189424196f18d0e115210eb');
        openpay
            .getDeviceSessionId()
            .then(sessionId => this.setState({session: sessionId}));
        this.setState({
            itempay: await AsyncStorage.getItem('itemid'),
            itemTotalToPay: await AsyncStorage.getItem('itemTotalToPay'),
            itemDescripcion: await AsyncStorage.getItem('itemDescripcion'),
            item_due_date: await AsyncStorage.getItem('item_due_date'),
            userID: await AsyncStorage.getItem('userID'),
            uri: await AsyncStorage.getItem('uri'),
            token: await AsyncStorage.getItem('token'),
            mainColor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor'),
            elHijo: await AsyncStorage.getItem('itemHijoId')
        });

    }

    async getCardByUserPay() {
        await fetch(this.state.uri + '/api/pasarelas/v1/openpay/customer/' + this.state.userID + '/card/get', {
            method: 'POST', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' esto puede ocurrir si no tiene ninguna tarjeta registrada', [{
                        text: 'Entendido', onPress: () => Actions.drawer()
                    }]);
                } else {
                    this.setState({cardData: responseJson.result});
                }

            });
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label, idTarjeta: option.idCard
        });
    }

    render() {
        let data = this.state.cardData.map((item, i) => {
            return {key: i, label: item.card_number, idCard: item.id};
        });


        return (<View style={styles.container}>
            <Text
                style={[styles.title, {color: this.state.thirdColor, paddingBottom: 1}]}>
                Información para el pago
            </Text>
            <View style={[styles.row_PT, {marginTop: 3}]}>
                <Text style={[styles.subTitleMain, {marginTop: 3}]}>Concepto:</Text>
                <View
                    style={{
                        alignItems: 'center',
                        backgroundColor: this.state.fourthColor,
                        borderColor: this.state.fourthColor,
                        borderRadius: 6,
                        borderWidth: 1,
                        height: responsiveHeight(5),
                        justifyContent: 'center',
                        width: responsiveWidth(94)
                    }}>
                    <Text
                        style={{
                            color: this.state.thirdColor,
                            fontSize: responsiveFontSize(2),
                            fontWeight: '400',
                            textAlign: 'center'
                        }}>
                        {this.state.itemDescripcion}
                    </Text>
                </View>
            </View>
            <View style={[styles.row_M, {height: responsiveHeight(9)}]}>
                <View>
                    <Text style={[styles.subTitleMain]}>Monto:</Text>
                    <View
                        style={{
                            alignItems: 'center',
                            backgroundColor: this.state.fourthColor,
                            borderColor: this.state.fourthColor,
                            borderRadius: 6,
                            borderWidth: 1,
                            height: responsiveHeight(5),
                            justifyContent: 'center',
                            width: responsiveWidth(27)
                        }}>
                        <Text
                            style={{
                                color: this.state.thirdColor,
                                fontSize: responsiveFontSize(2),
                                fontWeight: '400',
                                textAlign: 'left'
                            }}>
                            ${Number(this.props.FinalPrice).toLocaleString('en-US')}
                        </Text>
                    </View>
                </View>
                <View style={[styles.row_PT, {marginTop: 3}]}>
                    <Text style={[styles.subTitleMain, {marginTop: 1}]}>
                        Con cargo a:
                    </Text>
                    <ModalSelector
                        data={data}
                        cancelText='Cancelar'
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[styles.pickerPago, {
                            backgroundColor: this.state.fourthColor, borderColor: this.state.fourthColor
                        }]}
                        sectionTextStyle={[styles.subTitleMain, {color: this.state.thirdColor}]}
                        initValue='Seleccione una tarjeta'
                        onChange={option => this.onChange(option)}
                    />
                </View>
            </View>
            <View>
                <TouchableOpacity
                    style={[styles.btnPago, {backgroundColor: this.state.mainColor}]}
                    onPress={() => this.getCardsByUser()}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(2), color: 'white', fontWeight: '700'
                        }}>
                        Confirmar y Pagar
                    </Text>
                </TouchableOpacity>
            </View>
        </View>);
    }
}
