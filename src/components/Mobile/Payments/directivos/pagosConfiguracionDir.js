import React from "react";
import {AsyncStorage, ScrollView, Text, TextInput, TouchableHighlight, View} from "react-native";
import Switch from "react-native-switch-pro";
import styles from "../../../styles";
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';

export default class pagosConfiguracionDir extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            checked1: false,
            checked2: false,
            checked3: false,
            checked4: false,
            checked5: false,
            checked6: false,
            options: [],
            descuento: 10,
            recargo: 10,
            beca: 0,
            dias: 3,
            colegiatura: 2000,
            inscripcion: 2000,
            preinscripcion: 2000,
            loading: false
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        await this.setState({
            uri: await AsyncStorage.getItem("uri"),
            token: await AsyncStorage.getItem("token"),
            mainColor: await AsyncStorage.getItem("mainColor"),
            secondColor: await AsyncStorage.getItem("secondColor"),
            thirdColor: await AsyncStorage.getItem("thirdColor"),
            fourthColor: await AsyncStorage.getItem("fourthColor")
        });
        await this.getConfig();
    }

    async getConfig() {
        this.setState({loading: true});
        await fetch(this.state.uri + '/api/global/config/values/pagos', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if (recurso.error !== undefined) {
                    alert("Error");
                } else {
                    this.setState({
                        descuento: recurso[0].option_key,
                        recargo: recurso[1].option_key,
                        beca: recurso[2].option_key,
                        dias: recurso[3].option_key,
                        colegiatura: recurso[4].option_key,
                        inscripcion: recurso[5].option_key,
                        preinscripcion: recurso[6].option_key,
                        checked: recurso[0].option_key !== '0',
                        checked1: recurso[1].option_key !== '0',
                        checked2: recurso[2].option_key !== '0',
                        checked3: recurso[3].option_key !== '0',
                        checked4: recurso[4].option_key !== '0',
                        checked5: recurso[5].option_key !== '0',
                        checked6: recurso[6].option_key !== '0',
                    });
                    this.setState({loading: false});
                }
            });
    }

    async handleChange(checked) {
        await this.setState({checked});
    }

    async descuento(e) {
        if (Number(e.text) <= 100 && Number(e.text) > 0) {
            await this.setState({descuento: e.text})
        }
    }

    async handleChange1(checked1) {
        await this.setState({checked1});
    }

    async recargo(e) {
        if (Number(e.text) <= 100 && Number(e.text) > 0) {
            await this.setState({recargo: e.text})
        }
    }

    async handleChange2(checked2) {
        await this.setState({checked2});
    }

    async beca(e) {
        await this.setState({beca: e.text})
    }

    async handleChange3(checked3) {
        await this.setState({checked3});
    }

    async dias(e) {
        if (Number(e.text) <= 10 && Number(e.text) > 0) {
            await this.setState({dias: e.text})
        }
    }

    async handleChange4(checked4) {
        await this.setState({checked4});
    }

    async colegiatura(e) {
        if (Number(e.text) > 0) {
            this.state.colegiatura = await e.text;
        }
        await this.setState({aux: 0});
    }

    async handleChange5(checked5) {
        await this.setState({checked5});
    }

    async inscripcion(e) {
        if (Number(e.text) > 0) {
            await this.setState({inscripcion: e.text})
        }
    }

    async handleChange6(checked6) {
        await this.setState({checked6});
    }

    async preinscripcion(e) {
        if (Number(e.text) > 0) {
            await this.setState({preinscripcion: e.text})
        }
    }

    async save() {
        this.setState({loading: true});
        this.state.options[0] = {
            option_name: 'descuento',
            option_key: this.state.checked ? this.state.descuento : 0,
        };
        this.state.options[1] = {
            option_name: 'recargo',
            option_key: this.state.checked1 ? this.state.recargo : 0,
        };
        this.state.options[2] = {
            option_name: 'beca',
            option_key: this.state.checked2 ? this.state.checked2 : 0,
        };
        this.state.options[3] = {
            option_name: 'dias',
            option_key: this.state.checked3 ? this.state.dias : 0
        };
        this.state.options[4] = {
            option_name: 'colegiatura',
            option_key: this.state.checked4 ? this.state.colegiatura : 0
        };
        this.state.options[5] = {
            option_name: 'inscripcion',
            option_key: this.state.checked5 ? this.state.inscripcion : 0
        };
        this.state.options[6] = {
            option_name: 'preinscripcion',
            option_key: this.state.checked6 ? this.state.preinscripcion : 0
        };
        const body = new FormData();
        body.append('new', JSON.stringify(
            this.state.options
        ));
        await fetch(this.state.uri + '/api/save/config/payment', {
            method: 'POST', headers: {
                Accept: 'application/json', 'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }, body: body
        }).then(responseJson => {
            if (responseJson.error !== undefined) {
                // Functions.alertFail('Error al guardar');
                this.setState({loading: false});
            } else {
                this.setState({data: responseJson});
                // Functions.alertSucces('Su configuración fue guardada con éxito');
                this.setState({loading: false});
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{marginBottom: responsiveHeight(5)}} showsVerticalScrollIndicator={false}>
                    <View style={[styles.row, {
                        paddingVertical: responsiveHeight(2),
                        paddingHorizontal: responsiveWidth(3),
                        width: responsiveWidth(94),
                        borderRadius: 15,
                        backgroundColor: this.state.fourthColor,
                        marginVertical: 5
                    }]}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{
                                width: responsiveWidth(90),
                                paddingRight: responsiveWidth(2),
                                textAlign: 'justify'
                            }}>
                                Defina el importe a pagar de la colegiatura
                            </Text>
                            <View style={[styles.row, {
                                justifyContent: 'space-between',
                                width: responsiveWidth(47),
                                marginTop: responsiveHeight(2)
                            }]}>
                                <View style={{height: responsiveHeight(3.5), justifyContent: 'center'}}>
                                    <Switch
                                        value={this.state.checked4}
                                        onSyncPress={value => {
                                            this.handleChange4(value)
                                        }}
                                    />
                                </View>
                                <TextInput
                                    style={{
                                        width: responsiveWidth(30),
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        marginTop: responsiveHeight(1.5),
                                        borderRadius: 6,
                                        borderColor: this.state.secondColor,
                                        paddingVertical: responsiveHeight(.8),
                                        backgroundColor: '#fff'
                                    }}
                                    onChangeText={text => this.colegiatura({text})}
                                    underlineColorAndroid={'transparent'}
                                    maxLength={5}
                                    value={this.state.colegiatura}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={[styles.row, {
                        paddingVertical: responsiveHeight(2),
                        paddingHorizontal: responsiveWidth(3),
                        width: responsiveWidth(94),
                        borderRadius: 15,
                        backgroundColor: this.state.fourthColor,
                        marginVertical: 5
                    }]}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{
                                width: responsiveWidth(90),
                                paddingRight: responsiveWidth(2),
                                textAlign: 'justify'
                            }}>
                                Defina el importe a pagar de la inscripción
                            </Text>
                            <View style={[styles.row, {
                                justifyContent: 'space-between',
                                width: responsiveWidth(47),
                                marginTop: responsiveHeight(2)
                            }]}>
                                <View style={{height: responsiveHeight(3.5), justifyContent: 'center'}}>
                                    <Switch
                                        value={this.state.checked5}
                                        onSyncPress={value => {
                                            this.handleChange5(value)
                                        }}
                                    />
                                </View>
                                <TextInput
                                    style={{
                                        width: responsiveWidth(30),
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        marginTop: responsiveHeight(1.5),
                                        borderRadius: 6,
                                        borderColor: this.state.secondColor,
                                        paddingVertical: responsiveHeight(.8),
                                        backgroundColor: '#fff'
                                    }}
                                    onChangeText={text => this.inscripcion({text})}
                                    underlineColorAndroid={'transparent'}
                                    maxLength={3}
                                    value={this.state.inscripcion}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={[styles.row, {
                        paddingVertical: responsiveHeight(2),
                        paddingHorizontal: responsiveWidth(3),
                        width: responsiveWidth(94),
                        borderRadius: 15,
                        backgroundColor: this.state.fourthColor,
                        marginVertical: 5
                    }]}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{
                                width: responsiveWidth(90),
                                paddingRight: responsiveWidth(2),
                                textAlign: 'justify'
                            }}>
                                Defina el importe a pagar de la preinscripción
                            </Text>
                            <View style={[styles.row, {
                                justifyContent: 'space-between',
                                width: responsiveWidth(47),
                                marginTop: responsiveHeight(2)
                            }]}>
                                <View style={{height: responsiveHeight(3.5), justifyContent: 'center'}}>
                                    <Switch
                                        value={this.state.checked6}
                                        onSyncPress={value => {
                                            this.handleChange6(value)
                                        }}
                                    />
                                </View>
                                <TextInput
                                    style={{
                                        width: responsiveWidth(30),
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        marginTop: responsiveHeight(1.5),
                                        borderRadius: 6,
                                        borderColor: this.state.secondColor,
                                        paddingVertical: responsiveHeight(.8),
                                        backgroundColor: '#fff'
                                    }}
                                    onChangeText={text => this.preinscripcion({text})}
                                    underlineColorAndroid={'transparent'}
                                    maxLength={3}
                                    value={this.state.preinscripcion}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={[styles.row, {
                        paddingVertical: responsiveHeight(2),
                        paddingHorizontal: responsiveWidth(3),
                        width: responsiveWidth(94),
                        borderRadius: 15,
                        backgroundColor: this.state.fourthColor,
                        marginVertical: 5
                    }]}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{
                                width: responsiveWidth(90),
                                paddingRight: responsiveWidth(2),
                                textAlign: 'justify'
                            }}>
                                Defina el descuento máximo para el ototgamiento de becas en porcentaje
                            </Text>
                            <View style={[styles.row, {
                                justifyContent: 'space-between',
                                width: responsiveWidth(47),
                                marginTop: responsiveHeight(2)
                            }]}>
                                <View style={{height: responsiveHeight(3.5), justifyContent: 'center'}}>
                                    <Switch
                                        value={this.state.checked}
                                        onSyncPress={value => {
                                            this.handleChange(value)
                                        }}
                                    />
                                </View>
                                <TextInput
                                    style={{
                                        width: responsiveWidth(30),
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        marginTop: responsiveHeight(1.5),
                                        borderRadius: 6,
                                        borderColor: this.state.secondColor,
                                        paddingVertical: responsiveHeight(.8),
                                        backgroundColor: '#fff'
                                    }}
                                    onChangeText={text => this.descuento({text})}
                                    underlineColorAndroid={'transparent'}
                                    maxLength={3}
                                    value={this.state.descuento}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={[styles.row, {
                        paddingVertical: responsiveHeight(2),
                        paddingHorizontal: responsiveWidth(3),
                        width: responsiveWidth(94),
                        borderRadius: 15,
                        backgroundColor: this.state.fourthColor,
                        marginVertical: 5
                    }]}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{
                                width: responsiveWidth(90),
                                paddingRight: responsiveWidth(2),
                                textAlign: 'justify'
                            }}>
                                Para pagos morosos, realizar un cargo por pago tardío equivalente a un % del monto total
                                del pago
                            </Text>
                            <View style={[styles.row, {
                                justifyContent: 'space-between',
                                width: responsiveWidth(47),
                                marginTop: responsiveHeight(2)
                            }]}>
                                <View style={{height: responsiveHeight(3.5), justifyContent: 'center'}}>
                                    <Switch
                                        value={this.state.checked4}
                                        onSyncPress={value => {
                                            this.handleChange4(value)
                                        }}
                                    />
                                </View>
                                <TextInput
                                    style={{
                                        width: responsiveWidth(30),
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderRadius: 6,
                                        borderColor: this.state.secondColor,
                                        paddingVertical: responsiveHeight(.8),
                                        backgroundColor: '#fff'
                                    }}
                                    onChangeText={text => this.recargo({text})}
                                    underlineColorAndroid={'transparent'}
                                    maxLength={3}
                                    value={this.state.recargo}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={[styles.row, {
                        paddingVertical: responsiveHeight(2),
                        paddingHorizontal: responsiveWidth(3),
                        width: responsiveWidth(94),
                        borderRadius: 15,
                        backgroundColor: this.state.fourthColor,
                        marginVertical: 5
                    }]}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{
                                width: responsiveWidth(90),
                                paddingRight: responsiveWidth(2),
                                textAlign: 'justify'
                            }}>
                                El descuento de becas aplica unicamente a la colegiatura
                                mensual. El resto de pagos se requiere al 100%
                            </Text>
                            <View style={[styles.row, {
                                justifyContent: 'space-between',
                                width: responsiveWidth(47),
                                marginTop: responsiveHeight(2)
                            }]}>
                                <View style={{height: responsiveHeight(3.5), justifyContent: 'center'}}>
                                    <Switch
                                        value={this.state.checked2}
                                        onSyncPress={value => {
                                            this.handleChange2(value)
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.row, {
                        paddingVertical: responsiveHeight(2),
                        paddingHorizontal: responsiveWidth(3),
                        width: responsiveWidth(94),
                        borderRadius: 15,
                        backgroundColor: this.state.fourthColor,
                        marginVertical: 5
                    }]}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{
                                width: responsiveWidth(90),
                                paddingRight: responsiveWidth(2),
                                textAlign: 'justify'
                            }}>
                                Notificaciones diarias para recordatorios de pago desde cuantos días antes de la fecha
                                límite de
                                pago.
                            </Text>
                            <View style={[styles.row, {
                                justifyContent: 'space-between',
                                width: responsiveWidth(47),
                                marginTop: responsiveHeight(2)
                            }]}>
                                <View style={{height: responsiveHeight(3.5), justifyContent: 'center'}}>
                                    <Switch
                                        value={this.state.checked3}
                                        onSyncPress={value => {
                                            this.handleChange3(value)
                                        }}
                                    />
                                </View>
                                <TextInput
                                    style={{
                                        width: responsiveWidth(30),
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        marginTop: responsiveHeight(1.5),
                                        borderRadius: 6,
                                        borderColor: this.state.secondColor,
                                        paddingVertical: responsiveHeight(.8),
                                        backgroundColor: '#fff'
                                    }}
                                    onChangeText={text => this.dias({text})}
                                    underlineColorAndroid={'transparent'}
                                    maxLength={3}
                                    value={this.state.dias}
                                />
                            </View>
                        </View>
                    </View>
                    <TouchableHighlight
                        underlayColor={this.state.mainColor}
                        style={[styles.bigButton, {
                            backgroundColor: this.state.secondColor, borderColor: this.state.secondColor
                        }]}
                        onPress={() => this.save()}>
                        <Text style={styles.textButton}>Guardar</Text>
                    </TouchableHighlight>
                </ScrollView>
            </View>
        );
    }
}
