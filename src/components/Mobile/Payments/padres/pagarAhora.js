import React from "react";
import {
    View,
    Text,
    AsyncStorage,
    Image,
    ScrollView,
    TouchableOpacity
} from "react-native";
import {Actions} from "react-native-router-flux";
import styles from "../../../styles";

export default class FormasDePago extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: []
        };
    }

    async getURL() {
        let aydi = await AsyncStorage.getItem("itemid");
        this.setState({itempay: aydi});
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.menu} showsVerticalScrollIndicator={false}>
                    <View style={styles.row_PA}>
                        <TouchableOpacity
                            style={styles.btnContainer_PA}
                            onPress={() => Actions.proximosPagos()}
                        >
                            <Text style={styles.btnText_PA}>
                                <Text style={{fontWeight: "700"}}>Transferencia</Text>
                                ELECTRONICA
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.btnContainer_PA}
                            onPress={Actions.rugt()}
                        >
                            <Text style={styles.btnText_PA}>
                                Tarjeta de{" "}
                                <Text style={{fontWeight: "700"}}>CREDITO/DEBITO</Text>
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.row_PA}>
                        <TouchableOpacity style={styles.btnContainer_PA}>
                            <Image
                                style={{width: 120, height: 45}}
                                source={require("../../../../images/superama.png")}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.btnContainer_PA}
                            onPress={Actions.rugt()}
                        >
                            <Image
                                style={{width: 120, height: 45}}
                                source={require("../../../../images/walmart.png")}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.row_PA}>
                        <TouchableOpacity
                            style={styles.btnContainer_PA}
                            onPress={Actions.rugt()}
                        >
                            <Image
                                style={{width: 120, height: 45}}
                                source={require("../../../../images/fa.png")}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.btnContainer_PA}
                            onPress={Actions.rugt()}
                        >
                            <Image
                                style={{width: 120, height: 45}}
                                source={require("../../../../images/sams.png")}
                            />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
