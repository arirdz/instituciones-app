import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight, Alert
} from 'react-native';
import {
	responsiveFontSize,
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import Fumi from 'react-native-textinput-effects/lib/Fumi';
import Ionicons from 'react-native-vector-icons/Ionicons'
import MateriaCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Hijos from '../../Globales/hijos';
import MultiBotonRow from '../../Globales/MultiBotonRow'
import {Actions} from "react-native-router-flux";

export default class datosFiscales extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			items: {},
			laSolicitud: '',
			solicitudes: [],
			nombre: '',
			apellidos: '',
			email: '',
			rfc: '',
			telefono: '',
			calle: '',
			numero_exterior: '',
			numero_interior: '',
			codpos: '',
			colonia: '',
			ciudad: '',
			estado: '',
			accion: 'Guardar',
			accion2: '',
			selectedIndex: 0,
			curp: '',
			botonSelected: '',
			indexSelected: 0,
            mainColor: '#ffff',
            secondColor: '#ffff',
            thirdColor: '#ffff',
            fourthColor: '#ffff'
		};
	}

	async componentWillMount() {
		await this.getURL();
		this.getUserdata();
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected, indexSelected: indexSelected
		});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}


	async getUserdata() {
		await fetch(this.state.uri + '/api/get/datosFacturacion', {
			method: 'get',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}
		})
			.then(response => {
				return response.json();
			})
			.then(recurso => {
				this.setState({
					datos: recurso
				});
				this.setState({
					nombre: this.state.datos.nombre,
					email: this.state.datos.email,
					rfc: this.state.datos.rfc
				})
				if (this.state.nombre !== '') {
					this.setState({accion: 'Actualizar'})
				}
			});
	}

	crearCliente() {
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (reg.test(this.state.email) === false) {
			Alert.alert('Email inválido', 'ingresa un email válido', [{text: 'Enterado'}]);
		}
		if (this.state.accion === 'Guardar') {
			fetch(this.state.uri + '/api/web/fac/create/cli', {
				method: 'post', headers: {
					Accept: 'application/json, text/plain, */*',
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}, body: JSON.stringify({
					nombre: this.state.nombre,
					email: this.state.email,
					rfc: this.state.rfc
				})
			}).then(res => res)
				.then(responseJson => {
                    Alert.alert('Correcto', 'Se han guardado sus datos con éxito', [{
                        text: 'Enterado', onPress: () => [Actions.menuFacturas()]
                    }]);
				});
		} else if (this.state.accion === 'Actualizar') {
			fetch(this.state.uri + '/api/web/fac/editar/cli', {
				method: 'post', headers: {
					Accept: 'application/json, text/plain, */*',
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}, body: JSON.stringify({
					nombre: this.state.nombre,
					email: this.state.email,
					rfc: this.state.rfc
				})
			}).then(res => res.json())
            Alert.alert('Correcto', 'Se han actualizado sus datos con éxito', [{
                text: 'Enterado', onPress: () => [Actions.menuFacturas()]
            }]);
		}
	};

	async onListItemPressed(index, grado, grupo, id) {
		await this.setState({
			selectedIndex: index,
			selGrad: grado,
			selGrup: grupo,
			elHijo: id
		});
		this.getCurp()
	}

	getCurp() {
		console.log(this.state.elHijo)
		fetch(this.state.uri + '/api/get/curp/' + this.state.elHijo, {
			method: 'get', headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(response => {
			return response.json();
		})
			.then(recurso => {
				console.log(recurso)
				this.setState({curpHijo: recurso.curp});
				if (this.state.curpHijo === null || this.state.curpHijo === '') {
					console.log('guar')
					this.setState({accion2: 'Guardar'});
				} else {
					console.log('act')
					this.setState({accion2: 'Actualizar'});
				}
			});
	}

	guardarCurp() {
		console.log('status', this.state.curpHijo)
		console.log('status', this.state.elHijo)
		if (this.state.curpHijo === null) {
			Alert.alert('Error', 'No se ha ingresado el CURP', [{text: 'Enterado'}]);
		} else if (this.state.curpHijo.length !== 18) {
			Alert.alert('Error', 'Verifique el CURP. Debe contener 18 caracteres', [{text: 'Enterado'}]);
		} else {
			fetch(this.state.uri + '/api/save/curp', {
				method: 'post', headers: {
					Accept: 'application/json, text/plain, */*',
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}, body: JSON.stringify({
					elHijo: this.state.elHijo,
					curpHijo: this.state.curpHijo
				})
			}).then(response => {
				return response.json();
			}).then(recurso => {
				console.log(recurso);
				Alert.alert('Correcto', 'Se ha guardado el CURP correctamente', [{text: 'Enterado'}]);
			})
		}
	}

	async toMayus(text) {
		let texto = text;
		this.setState({curpHijo: texto.toUpperCase()});
		console.log(this.state.curpHijo)
		await this.setState({aux: 0});
	}

	async botonSelected(indexBtn, itemBtn) {
		await this.setState({
			botonSelected: itemBtn, indexSelected: indexBtn
		})
	}

	render() {
		let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		let texto1 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnTem = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyl='light-content'
				/>


				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Datos fiscales
				</Text>
				<Text style={[styles.opcionistas, styles.widthall, {color: 'red', marginBottom: 5}]}>
					Debe capturar tanto el CURP de cada hijo(a) como el RFC de la persona física para poder
					procesar la facturación de las colegiaturas.
				</Text>
				<MultiBotonRow
					itemBtns={[
						'RFC',
						'CURP'
					]}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelected(indexBtn, itemBtn)
					}
					cantidad={2}
				/>

				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={[styles.widthall, {}]}>
						{this.state.indexSelected==0?
							<View>
								<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
									Capture la información
								</Text>
								<Fumi
									label={'Nombre completo o Razón social'}
									iconClass={Ionicons}
									style={[styles.inputDato, {borderColor: this.state.secondColor}]}
									labelStyle={{color: this.state.mainColor}}
									iconName={'md-person'}
									inputStyle={{color: this.state.secondColor}}
									iconColor={this.state.mainColor}
									iconSize={20}
									onChangeText={text => (this.state.nombre = text)}
									keyboardType='default'
									value={this.state.nombre}
									returnKeyType='next'
									autoCapitalize='none'
									autoCorrect={false}
								/>
								<Fumi
									ref='domicilioFisc'
									label={'RFC'}
									iconClass={MateriaCommunityIcons}
									style={[styles.inputDato, {borderColor: this.state.secondColor}]}
									labelStyle={{color: this.state.mainColor}}
									iconName={'file-document'}
									inputStyle={{color: this.state.secondColor}}
									iconColor={this.state.mainColor}
									iconSize={20}
                                    maxLength={13}
									onChangeText={text => (this.state.rfc = text)}
									keyboardType='default'
									value={this.state.rfc}
									autoCapitalize='none'
									autoCorrect={false}
								/>
								<Fumi
									ref='correo'
									label={'Correo electrónico'}
									iconClass={MateriaCommunityIcons}
									style={[styles.inputDato, {borderColor: this.state.secondColor}]}
									labelStyle={{color: this.state.mainColor}}
									iconName={'email'}
									inputStyle={{color: this.state.secondColor}}
									iconColor={this.state.mainColor}
									iconSize={20}
									onChangeText={text => (this.state.email = text)}
									keyboardType='default'
									value={this.state.email}
									autoCapitalize='none'
									autoCorrect={false}
								/>
								<TouchableOpacity
									style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
									onPress={() => this.crearCliente()}
								>
									<Text
										style={styles.textButton}
									>
										{this.state.accion}
									</Text>
								</TouchableOpacity>
						</View>:
						<View>
						<Hijos
							onSelectedChamaco={(index, grado, grupo, id) =>
								this.onListItemPressed(index, grado, grupo, id)
							}
						/>
						<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
							CURP del hijo
						</Text>
						<Fumi
							ref='domicilioFisc'
							label={'CURP'}
							iconClass={MateriaCommunityIcons}
							style={[styles.inputDato, {borderColor: this.state.secondColor}]}
							labelStyle={{color: this.state.mainColor}}
							iconName={'file-document'}
							inputStyle={{color: this.state.secondColor}}
							iconColor={this.state.mainColor}
							iconSize={20}
							onChangeText={text => this.toMayus(text)}
							keyboardType='default'
							value={this.state.curpHijo}
							autoCapitalize='none'
							autoCorrect={false}
							maxLength={18}
						/>
						<TouchableOpacity
							style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
							onPress={() => this.guardarCurp()}
						>
							<Text
								style={styles.textButton}
							>
								{this.state.accion2}
							</Text>
						</TouchableOpacity>
						</View>}
					</View>
				</ScrollView>
			</View>
		);
	}
}
