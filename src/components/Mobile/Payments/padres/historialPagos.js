import React from 'react';
import {
    AsyncStorage,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../../styles';
import Hijos from '../../Globales/hijos';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class proximosPagos extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            selItem: [],
            uri: '',
            datosPicker: [],
            grupospicker: [],
            mes: moment().month() + 1,
            data: [],
            cososPicker: [],
            selectedIndexGrupo: '0', fecha: [], listaFacturas: ''
        };
        this.renderGrupos = this.renderGrupos.bind(this);
    }

    async onListItemPressedGrupo(index, item) {
        await this.setState({selectedIndexGrupo: index, selItem: item, idPago: item.id});
        await this.saveData(item);
        await this.getFecha();
    }

    async saveData(item) {
        try {
            await AsyncStorage.setItem('itemid', item.id.toString());
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            elHijo: id.toString(),
            selectedIndexGrupo: '0'
        });
        await this.getCargos();
    }

    async getUserdata() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(uri + '/api/user/data', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        let dato = await request.json();
        this.setState({datos: dato});
    }

    renderGrupo(itemGrupo, indexGrupo) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            styles.row,
            {backgroundColor: '#f2f2f2', borderWidth: 0}
        ];
        if (this.state.selectedIndexGrupo == indexGrupo) {
            bigButtonStyles.push(
                styles.listButton,
                styles.listButtonBig,
                styles.row,
                {backgroundColor: '#ddd', borderWidth: 0}
            );
        }

        return (
            <TouchableOpacity
                key={indexGrupo}
                style={bigButtonStyles}
                underlayColor={this.state.secondColor}
                onPress={() => this.onListItemPressedGrupo(indexGrupo, itemGrupo)}>
                <View style={[styles.btn3, {marginLeft: 9}]}>
                    <Text numberOfLines={1} style={{textAlign: 'left'}}>
                        {itemGrupo.descripcion}
                    </Text>
                </View>
                <View
                    style={[
                        styles.btn6,
                        {height: responsiveHeight(3.75), justifyContent: 'center'}
                    ]}>
                    <Text
                        style={{fontSize: responsiveFontSize(1.5), textAlign: 'right'}}>
                        ${itemGrupo.importe_a_pagar}
                    </Text>
                </View>
                <View
                    style={[
                        styles.btn3T,
                        {height: responsiveHeight(3.75), justifyContent: 'center'}
                    ]}>
                    <Text
                        style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>
                        {moment(itemGrupo.fecha_limite_pago).format('DD/MM/YY')}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.data.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    async getGrupos() {
        let grupicker = await fetch(this.state.uri + '/api/get/grupos');
        grupospicker = await grupicker.json();
        this.setState({
            grupos: grupospicker.map(item => item)
        });
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getMeses();
        this.getUserdata();
        this.getGrupos();
        this.getCargos();
        this.getFacturas();
    }

    async getCargos() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(
            uri +
            '/api/payments/paid/customer/' +
            this.state.datos.user_id +
            '/charges/' +
            this.state.elHijo +
            '/' +
            this.state.mes,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        let cargos = await request.json();
        await this.setState({data: cargos});
    }


    async getFacturas() {
        let request = await fetch(this.state.uri + '/api/lista/facturas', {
            method: 'GET'
        });
        let lista = await request.json();
        await this.setState({listaFacturas: lista.data});
    }

    async getFecha() {
        let request = await fetch(
            this.state.uri +
            '/api/payments/fecha/pago/customer/' +
            this.state.datos.user_id +
            '/' +
            this.state.elHijo +
            '/' +
            this.state.idPago,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let fecha = await request.json();
        await this.setState({fecha: fecha[0]});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, mes: option.mes});
        await this.getCargos();
    }

    render() {
        const listaPagos = this.renderGrupos();
        const data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (
            <ScrollView>
                <View style={styles.container}>
                    <View>
                        <Text
                            style={[
                                styles.main_title,
                                {
                                    color: this.state.thirdColor,
                                    marginBottom: 0
                                }
                            ]}>
                            Seleccione el mes
                        </Text>
                    </View>
                    <ModalSelector
                        data={data}
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[
                            [styles.inputPicker, {borderColor: this.state.secondColor}],
                            {
                                marginTop: 2,
                                marginBottom: 5
                            }
                        ]}
                        initValue={mess}
                        onChange={option => this.onChange(option)}
                    />
                    <Hijos
                        onSelectedChamaco={(index, grado, grupo, id) =>
                            this.onListItemPressed(index, grado, grupo, id)
                        }
                    />
                    <View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Lista de pagos
                        </Text>
                    </View>
                    <View>
                        <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                            <View style={styles.btn4}>
                                <Text style={{fontWeight: '700', textAlign: 'center'}}>
                                    Concepto
                                </Text>
                            </View>
                            <View style={styles.btn2_5}>
                                <Text style={{fontWeight: '700', textAlign: 'right'}}>
                                    importe
                                </Text>
                            </View>
                            <View style={styles.btn3}>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        textAlign: 'center',
                                        marginLeft: 5
                                    }}>
                                    Fecha límite
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                height: responsiveHeight(23),
                                borderBottomWidth: 1,
                                borderTopWidth: 1,
                                marginBottom: 5
                            }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {listaPagos}
                            </ScrollView>
                        </View>
                    </View>
                    <View
                        style={{
                            alignItems: 'flex-start',
                            width: responsiveWidth(94)
                        }}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Detalle del pago
                        </Text>
                        <Text
                            style={{
                                color: this.state.thirdColor,
                                fontSize: responsiveFontSize(2),
                                fontWeight: '300',
                                marginTop: 5
                            }}>
                            Descripción:
                        </Text>
                    </View>
                    {this.state.selectedIndexGrupo !== '0' ? (
                        <View
                            style={{
                                backgroundColor: '#ddd',
                                width: responsiveWidth(95),
                                height: responsiveHeight(20),
                                marginTop: 3,
                                paddingHorizontal: 10,
                                borderRadius: 6,
                                marginBottom: 10
                            }}>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    marginTop: 10,
                                    textAlign: 'left'
                                }}>
                                El pago fue realizado el dia{' '}
                                <Text
                                    style={{
                                        fontWeight: '600'
                                    }}>
                                    {moment(this.state.fecha.date).format(
                                        'DD/MM/YY'
                                    )}{' '}
                                </Text>
                                e incluye lo siguiente:{'\n'}
                                Importe: ${this.state.selItem.importe_a_pagar}{'\n'}
                                {this.state.selItem.importe_a_pagar !== this.state.fecha.total ?
                                    (<Text>Recargos:
                                        ${this.state.fecha.total - this.state.selItem.importe_a_pagar}</Text>)
                                    : null}

                            </Text>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    height: responsiveHeight(7),
                                    marginTop: 10,
                                    marginHorizontal: 10,
                                    textAlign: 'center',
                                    fontWeight: '600'
                                }}>
                                {this.state.selItem.descripcion}
                            </Text>
                        </View>
                    ) : null}

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 10,
                            marginBottom: 10,
                            borderRadius: 10,
                            backgroundColor: '#d3d3d3',
                            height: responsiveHeight(8),
                            width: responsiveWidth(95)
                        }}>
                        <View
                            style={{
                                width: responsiveWidth(90),
                                alignItems: 'stretch',
                                justifyContent: 'space-between',
                                flexDirection: 'row'
                            }}>
                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: responsiveFontSize(2),
                                    textAlign: 'center',
                                    textAlignVertical: 'center',
                                    paddingTop: 0,
                                    paddingRight: 5,
                                    fontWeight: '700'
                                }}>
                                Estatus Factura
                            </Text>
                            <TouchableOpacity
                                onPress={() => Actions.paymentMethod()}
                                style={{
                                    width: responsiveWidth(25),
                                    height: responsiveHeight(5),
                                    backgroundColor: '#263238',
                                    borderRadius: 6,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                <Text
                                    style={{
                                        color: 'white',
                                        fontSize: responsiveFontSize(2),
                                        fontWeight: '700'
                                    }}>
                                    Entregada
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => Actions.paymentMethod()}
                                style={{
                                    width: responsiveWidth(25),
                                    borderRadius: 6,
                                    backgroundColor: 'green',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                <Text
                                    style={{
                                        color: 'white',
                                        fontSize: responsiveFontSize(2),
                                        fontWeight: '700'
                                    }}>
                                    Descargar
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
