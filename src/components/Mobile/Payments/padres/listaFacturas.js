import React from 'react';
import {
    View,
    Text,
    AsyncStorage,
    ScrollView,
    TouchableOpacity, Alert
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../../styles';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';


const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class listarFacturas extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            facturas: [],
            cancelar: false,
            mail: false,
            solicitar: false,
            sfacturas: false
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.setState({sfacturas: true});
        await this.getFacturas();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getFacturas() {
        await fetch(this.state.uri + '/api/get/misFacturas',
            {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();
            }).then(responseJson => {
                if (responseJson.error === null) {
                    this.setState({sfacturas: false})
                    Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Facturación)');
                } else {
                    this.setState({sfacturas: false})
                    this.setState({facturas: []});
                    this.setState({facturas: responseJson});
                    if (this.state.facturas.length === 0) {
                        this.setState({sfacturas: false})
                    }
                    console.log(this.state.facturas)
                }
            });
    }


    cancelarFactura(item){
        Alert.alert('Atención',
            '¿Seguro que desea cancelar la factura?',
            [{
                text: 'Sí', onPress: () => {
                    this.setState({cancelar: true});
                    fetch(this.state.uri + '/api/cancelarFactura/'+item.facturama_id,
                        {
                            method: 'get',
                            headers: {
                                'Content-Type': 'application/json',
                                Authorization: 'Bearer ' + this.state.token
                            }
                        })
                        .then(response => {
                            return response.json();
                        }).then(responseJson => {
                        if (responseJson.error === null) {
                            Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Facturación)', [{
                                text: 'Enterado', onPress: () => [this.setState({cancelar: false})]
                            }])
                        } else {
                            Alert.alert('Felicidades', 'Se ha cancelado la factura correctamente', [{
                                text: 'Enterado', onPress: () => [this.setState({cancelar: false}), this.getFacturas()]
                            }]);

                        }
                    })}
            },
                {
                    text: 'No'
                }
            ]);
    }

    send(item) {
        console.log(item.facturama_id)
        this.setState({mail: true});
        fetch(this.state.uri + '/api/send/mail', {
            method: 'post',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }, body: JSON.stringify({
                facturaId: item.facturama_id
            })
        }).then(res => res.json())
            .then(responseJson => {
                console.log(responseJson)
                if(responseJson.success === false) {
                    Alert.alert('Error', 'Ha ocurrido un error al intentar enviar el correo', [{
                        text: 'Enterado', onPress: () => [this.setState({mail: false})]
                    }]);
                } else {
                    if(responseJson.success === true) {
                        Alert.alert('Felicidades', 'Se ha enviado la factura a su correo', [{
                            text: 'Enterado', onPress: () => [this.setState({mail: false})]
                        }]);
                    }
                }
            });
    }

    card() {
        let carta = [];
        this.state.facturas.forEach((item, i) => {
            carta.push(
                <View
                    key={i + 'carta'}
                    style={[
                        styles.widthall,
                        // styles.row,
                        {
                            marginVertical: 5,
                            borderWidth: 0.5,
                            borderRadius: 6,
                            borderColor: 'lightgray',
                            backgroundColor: this.state.fourthColor,
                            paddingTop: 10
                        }
                    ]}>
                    <View style={[styles.row, styles.widthall, {
                        justifyContent: 'flex-start',
                        padding: 0,
                        paddingLeft: 5
                    }]}>
                        <View style={[styles.btn7, {alignItems: 'flex-start'}]}>
                            <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Descripción
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{item.descripcion}</Text>
                        </View>
                        <View style={[styles.btn3_lll]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Folio
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{item.folio}</Text>
                        </View>
                        <View style={[styles.btn3_lll]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Serie
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{item.serie}</Text>
                        </View>
                    </View>
                    <View
                        style={{
                            borderBottomColor: this.state.thirdColor,
                            borderBottomWidth: .5,
                            paddingTop: 10
                        }}
                    />
                    {(item.status === 'Cancelada') ?
                        (<View style={[styles.row, styles.widthall, {
                            textAlign: 'center',
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingVertical: 10,
                        }]}>
                            <TouchableOpacity style={[styles.row, styles.btn8, {
                                borderRadius: 6,
                                backgroundColor: this.state.secondColor,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                paddingHorizontal: 15
                            }]}
                                              onPress={() => [(
                                                  Alert.alert('Error', 'No se puede enviar al correo, ya que la factura fue cancelada', [{
                                                      text: 'Enterado'
                                                  }])
                                              )]}
                            >
                                <Text style={[styles.textW, {color: "white", fontSize: responsiveFontSize(1.3)}]}>
                                    Enviar al correo
                                </Text>
                                <Feather name='mail' size={20} color='#fff'
                                         style={{marginLeft: 10, paddingVertical: 3}}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.row, styles.btn8, {
                                marginLeft: 10,
                                justifyContent: 'center',
                                borderRadius: 6,
                                paddingVertical: 5,
                                paddingHorizontal: 15,
                                backgroundColor: 'red',
                            }]}
                                              onPress={() => [(
                                                  Alert.alert('Atención', 'La factura ya fué cancelada', [{
                                                      text: 'Enterado'
                                                  }])
                                              )]}
                            >
                                <Text style={[styles.textW, {textAlign: 'center', color: "white", fontSize: responsiveFontSize(1.3)}]}>
                                    Factura cancelada
                                </Text>
                                <MaterialCommunityIcons name='close-circle' size={20} color='#fff'
                                                        style={{marginLeft: 10, paddingVertical: 3}}

                                />
                            </TouchableOpacity>
                        </View>)
                        :
                        (<View style={[styles.row, styles.widthall, {
                            textAlign: 'center',
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingVertical: 10,
                        }]}>
                            <TouchableOpacity style={[styles.row, styles.btn8, {
                                borderRadius: 6,
                                backgroundColor: this.state.secondColor,
                                justifyContent: 'center',
                                paddingVertical: 5,
                                paddingHorizontal: 15
                            }]}
                                              onPress={() => this.send(item)}
                            >
                                <Text style={[styles.textW, {color: "white", fontSize: responsiveFontSize(1.3)}]}>
                                    Enviar al correo
                                </Text>
                                <Feather name='mail' size={20} color='#fff'
                                         style={{marginLeft: 10, paddingVertical: 3}}
                                />
                            </TouchableOpacity>
                            {/*<TouchableOpacity style={[styles.row, styles.btn8, {*/}
                            {/*marginLeft: 10,*/}
                            {/*justifyContent: 'center',*/}
                            {/*borderRadius: 6,*/}
                            {/*paddingVertical: 5,*/}
                            {/*paddingHorizontal: 15,*/}
                            {/*backgroundColor: this.state.mainColor,*/}
                            {/*}]}*/}
                            {/*onPress={() => [(*/}
                            {/*Alert.alert('Cancelar Factura', '¿Está seguro que desea cancelar la factura?', [*/}
                            {/*{*/}
                            {/*text: 'Sí',*/}
                            {/*onPress: () => [this.cancelarFactura(item)]*/}
                            {/*}, {text: 'No'}*/}
                            {/*])*/}
                            {/*)]}*/}
                            {/*>*/}
                            {/*<Text style={[styles.textW, {textAlign: 'center', color: "white", fontSize: responsiveFontSize(1.3)}]}>*/}
                            {/*Cancelar Factura*/}
                            {/*</Text>*/}
                            {/*<MaterialCommunityIcons name='close-circle' size={20} color='#fff'*/}
                            {/*style={{marginLeft: 3, paddingVertical: 3}}*/}
                            {/*/>*/}
                            {/*</TouchableOpacity>*/}
                        </View>)
                    }
                </View>
            )
        });
        return carta;
    }


    render() {
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.cancelar} textContent='Cancelando factura...'/>
                <Spinner visible={this.state.solicitar} textContent='Solicitando cancelación...'/>
                <Spinner visible={this.state.mail} textContent='Enviando Factura...'/>
                <Spinner visible={this.state.sfacturas} textContent='Consultado Facturas...'/>
                <Text
                    style={[
                        styles.main_title,
                        {
                            color: this.state.thirdColor,
                            marginBottom: 1
                        }
                    ]}>
                    Elija la factura
                </Text>
                <ScrollView style={{marginBottom: 40}} showsVerticalScrollIndicator={false}>
                    {this.state.facturas.length === 0 ?
                        (<Text style={[styles.main_title, {
                            color: '#a8a8a8',
                            textAlign: 'center',
                            marginTop: 40,
                            marginBottom: 40,
                            fontSize: responsiveFontSize(3)
                        }]}>
                            No existen facturadas generadas en este momento
                        </Text>):
                        (this.card())
                    }
                </ScrollView>

            </View>
        );
    }
}

