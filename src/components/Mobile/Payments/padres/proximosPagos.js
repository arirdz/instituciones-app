import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    AsyncStorage,
    ScrollView
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../../styles';
import Hijos from '../../Globales/hijos';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class proximosPagos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cososPicker: [],
            data: [],
            datos: [],
            datosPicker: [],
            elGrado: '',
            elGrupo: '',
            elHijo: '',
            grupospicker: [],
            mes: moment().month() + 1,
            selectedIndex: -1,
            selectedIndexGrupo: -1,
            selItem: [],
            url: '',
            userRef: []
        };
        this.renderGrupos = this.renderGrupos.bind(this);
    }

    componentWillMount() {
        this.getURL();
        this.getMeses();
        this.userRegistrado();
        this.getUserdata();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            url: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    //+++++++++++++++++++++++++++ get hijos +++++++++++++++++++++++
    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            elHijo: id.toString(),
            selectedIndexGrupo: -1,
            selItem: ''
        });
        await this.getCargos();
    }

    //+++++++++++++++++++++++++++ get cargos +++++++++++++++++++++++
    async getCargos() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(
            uri +
            '/api/payments/next/customer/' +
            this.state.datos.user_id +
            '/charges/' +
            this.state.elHijo +
            '/' +
            this.state.mes,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        let cargos = await request.json();
        await this.setState({data: cargos});
    }

    renderGrupo(itemGrupo, indexGrupo) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            styles.row,
            {backgroundColor: '#f2f2f2', borderWidth: 0}
        ];
        if (this.state.selectedIndexGrupo == indexGrupo) {
            bigButtonStyles.push(
                styles.listButton,
                styles.listButtonBig,
                styles.row,
                {backgroundColor: '#ddd'}
            );
        }
        return (
            <TouchableOpacity
                key={indexGrupo}
                style={bigButtonStyles}
                underlayColor={this.state.secondColor}
                onPress={() => this.onListItemPressedGrupo(indexGrupo, itemGrupo)}>
                <View style={[styles.btn3, {marginLeft: 9}]}>
                    <Text numberOfLines={1} style={{textAlign: 'left'}}>
                        {itemGrupo.descripcion}
                    </Text>
                </View>
                <View
                    style={[
                        styles.btn6,
                        {height: responsiveHeight(3.75), justifyContent: 'center'}
                    ]}>
                    <Text
                        style={{fontSize: responsiveFontSize(1.5), textAlign: 'right'}}>
                        ${itemGrupo.importe_a_pagar}
                    </Text>
                </View>
                <View
                    style={[
                        styles.btn3T,
                        {height: responsiveHeight(3.75), justifyContent: 'center'}
                    ]}>
                    <Text
                        style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>
                        {moment(itemGrupo.fecha_limite_pago).format('DD/MM/YY')}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.data.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    async onListItemPressedGrupo(index, item) {
        await this.setState({selectedIndexGrupo: index, selItem: item});
        await this.saveData(item);
    }

    //+++++++++++++++++++++++++++ get meses +++++++++++++++++++++++
    async getMeses() {
        let uri = await AsyncStorage.getItem('uri');
        let dataPicker = await fetch(uri + '/api/config/extra/meses');
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            mes: option.mes,
            selItem: ''
        });
        this.getCargos();
    }

    //+++++++++++++++++++++++++++ get user data +++++++++++++++++++++++
    async userRegistrado() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let userRegistradoFerch = await fetch(
            uri + '/api/pagos/get/user/registered',
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        let regUser = await userRegistradoFerch.json();
        this.setState({userRef: regUser[0]});
    }

    async getUserdata() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(uri + '/api/user/data', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        let dato = await request.json();
        this.setState({datos: dato});
    }

    //+++++++++++++++++++++++++++ save Data +++++++++++++++++++++++
    async saveData(item) {
        try {
            await AsyncStorage.setItem('itemid', item.id.toString());
            await AsyncStorage.setItem('itemTotalToPay', item.importe_a_pagar);
            await AsyncStorage.setItem('itemHijoId', this.state.elHijo);
            await AsyncStorage.setItem('itemDescripcion', item.descripcion);
            await AsyncStorage.setItem('item_due_date', item.fecha_limite_pago);
            await AsyncStorage.setItem(
                'userID',
                this.state.userRef.user_id.toString()
            );
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    render() {
        const listaPagos = this.renderGrupos();
        const data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        let picker = [
            styles.inputPicker,
            {
                borderColor: this.state.secondColor,
                marginTop: 2,
                marginBottom: 4
            }
        ];
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text
                        style={[
                            styles.main_title,
                            {
                                color: this.state.thirdColor,
                                marginBottom: 0
                            }
                        ]}>
                        Seleccione el mes
                    </Text>
                    <ModalSelector
                        data={data}
                        cancelText="Cancelar"
                        optionTextStyle={{
                            color: this.state.thirdColor
                        }}
                        selectStyle={picker}
                        initValue={mes}
                        onChange={option => this.onChange(option)}
                    />
                    <View>
                        <Hijos
                            onSelectedChamaco={(index, grado, grupo, id) =>
                                this.onListItemPressed(index, grado, grupo, id)
                            }
                        />
                    </View>
                    <View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Lista de pagos
                        </Text>
                    </View>
                    <View>
                        <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                            <View style={styles.btn4}>
                                <Text style={{fontWeight: '700', textAlign: 'center'}}>
                                    Concepto
                                </Text>
                            </View>
                            <View style={styles.btn2_5}>
                                <Text style={{fontWeight: '700', textAlign: 'right'}}>
                                    importe
                                </Text>
                            </View>
                            <View style={styles.btn3}>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        textAlign: 'right',
                                        marginRight: 5
                                    }}>
                                    Fecha límite
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                height: responsiveHeight(23),
                                borderBottomWidth: 1,
                                borderTopWidth: 1,
                                marginBottom: 5
                            }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {listaPagos}
                            </ScrollView>
                        </View>
                    </View>
                    <View
                        style={{
                            alignItems: 'flex-start',
                            width: responsiveWidth(95),
                            height: responsiveHeight(5)
                        }}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Detalle del pago
                        </Text>
                    </View>
                    <View>
                        <Text
                            style={{
                                color: this.state.thirdColor,
                                fontSize: responsiveFontSize(2),
                                fontWeight: '300',
                                marginTop: 5
                            }}>
                            Descripción:
                        </Text>
                    </View>
                    {this.state.selectedIndexGrupo !== -1 ? (
                        <View
                            style={{
                                backgroundColor: '#ddd',
                                width: responsiveWidth(95),
                                height: responsiveHeight(20),
                                marginTop: 3,
                                paddingHorizontal: 10,
                                borderRadius: 6
                            }}>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    marginTop: 10,
                                    textAlign: 'left'
                                }}>
                                El pago fue requerido para el dia{' '}
                                <Text
                                    style={{
                                        fontWeight: '600'
                                    }}>
                                    {moment(this.state.selItem.fecha_limite_pago).format(
                                        'DD/MM/YY'
                                    )}{' '}
                                </Text>
                                e incluye lo siguiente:
                            </Text>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    height: responsiveHeight(8),
                                    marginTop: 30,
                                    marginHorizontal: 10,
                                    textAlign: 'center',
                                    fontWeight: '600'
                                }}>
                                {this.state.selItem.descripcion}
                            </Text>
                        </View>
                    ) : null}
                </ScrollView>
                {this.state.selectedIndexGrupo !== -1 ? (
                    <TouchableOpacity
                        onPress={() => Actions.formadepagoquenofunciona()}
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.mainColor,
                                borderColor: this.state.mainColor
                            }
                        ]}>
                        <Text style={styles.textButton}>Pagar ahora</Text>
                    </TouchableOpacity>
                ) : null}
            </View>
        );
    }
}
