import React from 'react';
import {View, Text, Alert} from 'react-native';
import Switch from 'react-native-switch-pro';
import styles from '../../../styles';

export default class pagosVencidos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row_PC}>
                    <Text style={styles.rowText}>Porcentaje de recargos</Text>
                    <Switch
                        onSyncPress={value => {
                            Alert.alert('hola', 'Callback Ejecutado');
                        }}
                    />
                </View>
                <View style={styles.row_PC}>
                    <Text style={styles.rowText}>
                        Bloquear la publicacion de cobros con menos de NaN dias previos a le
                        fecha
                    </Text>
                    <Switch
                        onSyncPress={value => {
                            Alert.alert('hola', 'Callback Ejecutado');
                        }}
                    />
                </View>
                <View style={styles.row_PC}>
                    <Text style={styles.rowText}>
                        El descuento de becas aplica unicamente a la colegiatura mensual. el
                        resto de pagos se requiere al 100%
                    </Text>
                    <Switch
                        onSyncPress={value => {
                            Alert.alert('hola', 'Callback Ejecutado');
                        }}
                    />
                </View>
                <View style={styles.row_PC}>
                    <Text style={styles.rowText}>Meses que se cobra colegiatura</Text>
                    <Switch
                        onSyncPress={value => {
                            Alert.alert('hola', 'Callback Ejecutado');
                        }}
                    />
                </View>
                <View style={styles.row_PC}>
                    <Text style={styles.rowText}>
                        El descuento de becas aplica unicamente a la colegiatura mensual. el
                        resto de pagos se requiere al 100%
                    </Text>
                    <Switch
                        onSyncPress={value => {
                            Alert.alert('hola', 'Callback Ejecutado');
                        }}
                    />
                </View>
            </View>
        );
    }
}
