import React from 'react';
import {Alert, AsyncStorage, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../../styles';
import Hijos from '../../Globales/hijos';
import Spinner from "react-native-loading-spinner-overlay";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class GestionarPagos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cososPicker: [],
            data: [],
            datos: [],
            datosPicker: [],
            elGrado: '',
            elGrupo: '',
            elHijo: '',
            grupospicker: [],
            mes: moment().month() + 1,
            selectedIndex: -1,
            selectedIndexCargo: -1,
            selItem: [],
            url: '',
            userRef: [],
            selectedTipo: 0,
            becaAsignada: 0,
            visible: false
        };
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    async componentWillMount() {
        await this.getURL();
        await this.getMeses();
        await this.userRegistrado();
        await this.getConfig();
    }

    async getConfig() {
        await fetch(this.state.uri + '/api/global/config/values/pagos', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error si el error continua pónganse en contacto con soporte (Cargos)', [{
                        text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'GestionarPagos'}})
                    }]);
                } else {
                    this.setState({
                        recargo: Number(responseJson[1].option_key),
                    });
                }
            });
    }

    async getBeca() {
        await fetch(this.state.uri + '/api/user/alumno/' + this.state.elHijo + '/beca', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error si el error continua pónganse en contacto con soporte (Cargos)', [{
                        text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'GestionarPagos'}})
                    }]);
                } else {
                    this.setState({
                        becaAsignada: Number(responseJson),
                    });
                }
            });
    }

    async getURL() {
        this.setState({
            uri: await AsyncStorage.getItem('uri'),
            token: await AsyncStorage.getItem('token'),
            mainColor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor')
        });
    }

    //+++++++++++++++++++++++++++ get hijos +++++++++++++++++++++++
    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({visible: true});
        await this.setState({
            selectedIndex: index,
            elGrado: grado,
            elGrupo: grupo,
            elHijo: id.toString(),
            selectedIndexCargo: -1,
            selItem: '',
            selectedTipo: 3
        });
        await this.getBeca();
        await this.getCargos(1);
        await this.getCargos(3);
        await this.setState({visible: false});
    }

    //+++++++++++++++++++++++++++ get hijos +++++++++++++++++++++++
    async selectTipo(i) {
        await this.setState({
            selectedTipo: i, selItem: '', selectedIndexCargo: -1
        });
        await this.getCargos(i);
    }

    async userRegistrado() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let userRegistradoFerch = await fetch(uri + '/api/pagos/get/user/registered', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + token
            }
        });
        let regUser = await userRegistradoFerch.json();
        this.setState({userRef: regUser[0]});
    }

    //+++++++++++++++++++++++++++ get cargos +++++++++++++++++++++++
    async getCargos(i) {
        let t = i === 1 ? 'progreso' :
            i === 2 ? 'vencidos' :
                i === 3 ? 'requeridos' :
                    i === 4 ? 'realizados' :
                        i === 5 ? 'extemporaneos' : null;
        await fetch(this.state.uri + '/api/payment/dos/get/cargos/' + t + '/' + this.state.elGrado + '/' + this.state.elGrupo + '/' + this.state.elHijo + '/' + this.state.mes, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error si el error continua pónganse en contacto con soporte (Cargos)', [{
                        text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'GestionarPagos'}})
                    }]);
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    renderCargo(it, i) {
        let bigButtonStyles = [styles.listButton, styles.listButtonBig, styles.row, {
            backgroundColor: '#f2f2f2', borderWidth: 0
        }];
        if (this.state.selectedIndexCargo === i) {
            bigButtonStyles.push(styles.listButton, styles.listButtonBig, styles.row, {backgroundColor: '#ddd'});
        }
        let desc = 0;
        let finalprice = 0;
        if (this.state.selectedTipo === 3) {
            if (this.state.becaAsignada !== 0) {
                if (this.state.becaAsignada !== 0) {
                    desc = it.importe_a_pagar - ((it.importe_a_pagar * this.state.becaAsignada) / 100);
                    finalprice = Number(desc);
                }
            }
            else {
                finalprice = it.importe_a_pagar;
            }
        }
        else {
            finalprice = it.importe_a_pagar;
        }
        return (<TouchableOpacity
            key={i}
            style={bigButtonStyles}
            underlayColor={this.state.secondColor}
            onPress={() => this.onListItemPressedCargo(it, i)}>
            <View style={[styles.btn3, {paddingHorizontal: 5}]}>
                <Text numberOfLines={1} style={{textAlign: 'left'}}>
                    {it.descripcion}
                </Text>
            </View>
            <View
                style={[styles.btn3, {height: responsiveHeight(3.75), justifyContent: 'center'}]}>
                <Text
                    style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>$
                    ${this.state.selectedTipo === 3
                        ? Number(finalprice).toLocaleString('en-US')
                        : this.state.selectedTipo === 2
                            ? Number(finalprice).toLocaleString('en-US')
                            : Number(it.pagado).toLocaleString('en-US')}
                </Text>
            </View>
            <View
                style={[styles.btn3, {height: responsiveHeight(3.75), justifyContent: 'center'}]}>
                <Text
                    style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>
                    {moment(it.fecha_limite_pago).format('DD/MM/YY')}
                </Text>
            </View>
        </TouchableOpacity>);
    }

    renderCargos() {
        let buttonsGrupos = [];
        this.state.data.forEach((it, i) => {
            buttonsGrupos.push(this.renderCargo(it, i));
        });
        return buttonsGrupos;
    }

    async onListItemPressedCargo(it, i) {
        await this.setState({selectedIndexCargo: i, selItem: it});
        await this.saveData(it);
    }

    //+++++++++++++++++++++++++++ get meses +++++++++++++++++++++++
    async getMeses() {
        let uri = await AsyncStorage.getItem('uri');
        let dataPicker = await fetch(uri + '/api/config/extra/meses');
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label, mes: option.mes, selItem: ''
        });
        await this.getCargos();
    }

    //+++++++++++++++++++++++++++ save Data +++++++++++++++++++++++
    async saveData(item) {
        try {
            await AsyncStorage.setItem('itemid', item.id.toString());
            await AsyncStorage.setItem('itemTotalToPay', item.importe_a_pagar);
            await AsyncStorage.setItem('itemHijoId', this.state.elHijo);
            await AsyncStorage.setItem('itemDescripcion', item.descripcion);
            if (this.state.selectedTipo !== 2) {
                await AsyncStorage.setItem('item_due_date', item.fecha_limite_pago);
            } else {
                let tomorrow = await moment().add(1, 'd').format();
                await AsyncStorage.setItem('item_due_date', tomorrow);
            }
            await AsyncStorage.setItem('userID', this.state.userRef.user_id.toString());
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    async sendToMail() {
        let formData = new FormData();
        formData.append('data', JSON.stringify({
            transaction_id: this.state.selItem.transaction_id,
            email: await AsyncStorage.getItem('email')
        }));
        fetch(this.state.uri + "/api/comprobante/pago", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            },
            body: formData
        })
            .then(response => response.json())
            .then(responseJson => {
            });
    }

    render() {
        const listaPagos = this.renderCargos();
        const data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let picker = [styles.inputPicker, {
            borderColor: this.state.secondColor, marginTop: 2, marginBottom: 4
        }];
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        let t = this.state.selectedTipo === 1 ? 'En progreso' : this.state.selectedTipo === 2 ? 'Requerido' :
            this.state.selectedTipo === 3 ? 'Requerido' :
                this.state.selectedTipo === 4 ? 'Completado' : this.state.selectedTipo === 5 ? 'Completado' : null;
        let desc = 0;
        let finalprice = 0;
        if (this.state.selectedTipo === 3) {
            if (this.state.becaAsignada !== 0) {
                if (this.state.becaAsignada !== 0) {
                    desc = this.state.selItem.importe_a_pagar - ((this.state.selItem.importe_a_pagar * this.state.becaAsignada) / 100);
                    finalprice = Number(desc) + ((desc * 3.1) / 100) + 2.5;
                }
            }
            else {
                desc = this.state.selItem.importe_a_pagar;
                finalprice = Number(desc) + ((desc * 3.1) / 100) + 2.5;
            }
        }
        else {
            desc = this.state.selItem.importe_a_pagar;
            finalprice = Number(desc) + ((desc * 3.1) / 100) + 2.5;
        }
        return (<View style={styles.container}>
            <Spinner visible={this.state.visible} textContent='Procesando...'/>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Text
                    style={[styles.main_title, {
                        color: this.state.thirdColor, marginBottom: 0
                    }]}>
                    Seleccione el mes
                </Text>
                <ModalSelector
                    data={data}
                    cancelText='Cancelar'
                    optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
                    selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
                    selectStyle={picker}
                    initValue={mess}
                    onChange={option => this.onChange(option)}
                />
                <View>
                    <Hijos
                        onSelectedChamaco={(index, grado, grupo, id) => this.onListItemPressed(index, grado, grupo, id)}
                    />
                </View>
                <Text
                    style={[styles.main_title, {
                        color: this.state.thirdColor, marginBottom: 0
                    }]}>
                    Seleccione el tipo de pago
                </Text>
                <View style={[styles.row, {marginTop: 10}]}>
                    {/*<TouchableOpacity style={{*/}
                    {/*width: responsiveWidth(30.5),*/}
                    {/*borderWidth: 1,*/}
                    {/*borderRadius: 6,*/}
                    {/*padding: 8,*/}
                    {/*alignItems: 'center',*/}
                    {/*borderColor: this.state.secondColor,*/}
                    {/*backgroundColor: this.state.selectedTipo === 1 ? this.state.secondColor : '#fff'*/}
                    {/*}}*/}
                    {/*onPress={() => this.selectTipo(1)}>*/}
                    {/*<Text style={{color: this.state.selectedTipo === 1 ? '#fff' : '#000'}}>En*/}
                    {/*proceso</Text>*/}
                    {/*</TouchableOpacity>*/}
                    <TouchableOpacity style={{
                        width: responsiveWidth(46.7),
                        borderWidth: 1,
                        borderRadius: 6,
                        padding: 8,
                        alignItems: 'center',
                        borderColor: this.state.secondColor,
                        backgroundColor: this.state.selectedTipo === 3 ? this.state.secondColor : '#fff'
                    }}
                                      onPress={() => this.selectTipo(3)}>
                        <Text style={{color: this.state.selectedTipo === 3 ? '#fff' : '#000'}}>Por pagar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        width: responsiveWidth(46.7),
                        borderWidth: 1,
                        borderRadius: 6,
                        padding: 8,
                        alignItems: 'center',
                        borderColor: this.state.secondColor,
                        backgroundColor: this.state.selectedTipo === 4 ? this.state.secondColor : '#fff'
                    }}
                                      onPress={() => this.selectTipo(4)}>
                        <Text style={{color: this.state.selectedTipo === 4 ? '#fff' : '#000'}}>Pagados</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.row, {marginTop: 6}]}>

                    <TouchableOpacity style={{
                        width: responsiveWidth(46.7),
                        borderWidth: 1,
                        borderRadius: 6,
                        padding: 8,
                        alignItems: 'center',
                        borderColor: this.state.secondColor,
                        backgroundColor: this.state.selectedTipo === 2 ? this.state.secondColor : '#fff'
                    }}
                                      onPress={() => this.selectTipo(2)}>
                        <Text style={{color: this.state.selectedTipo === 2 ? '#fff' : '#000'}}>Vencidos</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        width: responsiveWidth(46.7),
                        borderWidth: 1,
                        borderRadius: 6,
                        padding: 8,
                        alignItems: 'center',
                        borderColor: this.state.secondColor,
                        backgroundColor: this.state.selectedTipo === 5 ? this.state.secondColor : '#fff'
                    }}
                                      onPress={() => this.selectTipo(5)}>
                        <Text style={{color: this.state.selectedTipo === 5 ? '#fff' : '#000'}}>Extemporáneo</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Lista de pagos
                    </Text>
                </View>
                <View>
                    <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                        <View style={styles.btn3}>
                            <Text style={{fontWeight: '700', textAlign: 'left'}}>
                                Concepto
                            </Text>
                        </View>
                        <View style={styles.btn3}>
                            <Text style={{fontWeight: '700', textAlign: 'center'}}>
                                Importe
                            </Text>
                        </View>
                        <View style={styles.btn3}>
                            <Text
                                style={{
                                    fontWeight: '700', textAlign: 'center',
                                }}>
                                Fecha límite
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            borderBottomWidth: 1, borderTopWidth: 1, marginBottom: 5
                        }}>
                        {listaPagos}
                    </View>
                </View>
                <View
                    style={{
                        alignItems: 'flex-start', width: responsiveWidth(95)
                    }}>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Detalle del pago
                    </Text>
                </View>
                {this.state.selectedIndexCargo !== -1 ? (<View
                    style={{
                        width: responsiveWidth(95),
                        marginVertical: 5,
                        paddingVertical: 10,
                        borderRadius: 6,
                        overflow: 'hidden',
                        backgroundColor: this.state.fourthColor,
                    }}>

                    {/*<View style={[styles.triangleCorner,{borderTopColor: this.state.thirdColor}]}></View>*/}
                    {/*<View style={[styles.triangleCornerLayer, {borderTopColor: this.state.fourthColor}]}></View>*/}
                    {/*<View style={[styles.triangleCorner1, {borderTopColor: this.state.secondColor}]}></View>*/}

                    <View style={[styles.row, {alignItems: 'flex-start'}]}>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                Concepto</Text>
                        </View>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text>{this.state.selItem.descripcion}</Text>
                        </View>
                    </View>

                    {this.state.selectedTipo === 4 ? null : (this.state.selItem.importe_a_pagar - desc) > 0 ?
                        <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                    Importe normal</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>${Number(this.state.selItem.importe_a_pagar).toLocaleString('en-US')}</Text>
                            </View>
                        </View> : null}

                    {this.state.selectedTipo === 4 ? null : (this.state.selItem.importe_a_pagar - desc) > 0 ?
                        <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                    Descuento por beca</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>${Number(this.state.selItem.importe_a_pagar - desc).toLocaleString('en-US')}</Text>
                            </View>
                        </View> : null}
                    {this.state.selectedTipo === 4 ? <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '800', textAlign: 'right'}}>
                                    Total pagado</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{fontWeight: '800'}}>${this.state.selectedTipo === 3
                                    ? Number(desc).toLocaleString('en-US')
                                    : this.state.selectedTipo === 2
                                        ? Number(this.state.selItem.importe_a_pagar).toLocaleString('en-US')
                                        : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                            </View>
                        </View> :
                        <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                    Importe a pagar</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>${this.state.selectedTipo === 3
                                    ? Number(desc).toLocaleString('en-US')
                                    : this.state.selectedTipo === 2
                                        ? Number(this.state.selItem.importe_a_pagar).toLocaleString('en-US')
                                        : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                            </View>
                        </View>}

                    {this.state.selectedTipo === 4
                        ? null
                        : <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                    Comisión pasarela</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text>${this.state.selectedTipo === 3
                                    ? Number(finalprice - desc).toLocaleString('en-US')
                                    : this.state.selectedTipo === 2
                                        ? Number(finalprice - this.state.selItem.importe_a_pagar).toLocaleString('en-US')
                                        : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                            </View>
                        </View>}

                    {this.state.selectedTipo === 4
                        ? null
                        : <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{color: this.state.secondColor, fontWeight: '800', textAlign: 'right'}}>
                                    Total a pagar</Text>
                            </View>
                            <View style={{width: responsiveWidth(45)}}>
                                <Text style={{fontWeight: '800'}}>${this.state.selectedTipo === 3
                                    ? Number(finalprice).toLocaleString('en-US')
                                    : this.state.selectedTipo === 2
                                        ? Number(finalprice).toLocaleString('en-US')
                                        : Number(this.state.selItem.pagado).toLocaleString('en-US')}</Text>
                            </View>
                        </View>}
                    <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>Fecha
                                de
                                creación</Text>
                        </View>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text>{moment(this.state.selItem.created_at).format('DD/MM/YY')}</Text>
                        </View>
                    </View>
                    <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>Fecha
                                límite de pago</Text>
                        </View>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text>{moment(this.state.selItem.fecha_limite_pago).format('DD/MM/YY')}</Text>
                        </View>
                    </View>
                    <View style={[styles.row, {marginTop: responsiveHeight(1)}]}>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text style={{color: this.state.secondColor, fontWeight: '600', textAlign: 'right'}}>
                                Estatus</Text>
                        </View>
                        <View style={{width: responsiveWidth(45)}}>
                            <Text>{t}</Text>
                        </View>
                    </View>
                    {this.state.selectedIndexCargo !== -1 ? this.state.selectedTipo === 4 ? (
                        <View style={[{
                            width: responsiveWidth(94),
                            alignItems: 'center'
                        }]}>
                            <TouchableOpacity
                                onPress={() => this.sendToMail()}
                                style={[styles.modalBigBtn, {
                                    width: responsiveWidth(40),
                                    height: responsiveHeight(5),
                                    backgroundColor: '#fff',
                                    borderColor: this.state.secondColor
                                }]}>
                                <Text style={{color: this.state.secondColor, fontSize: responsiveFontSize(2)}}>
                                    Enviar recibo</Text>
                            </TouchableOpacity>
                        </View>) : null : null}
                </View>) : null}
                {this.state.selectedIndexCargo !== -1 ? this.state.selectedTipo === 2 || this.state.selectedTipo === 3 ? (
                    <TouchableOpacity
                        onPress={() => Actions.formadepagoquenofunciona({FinalPrice: finalprice})}
                        style={[styles.bigButton, {
                            backgroundColor: this.state.mainColor, borderColor: this.state.mainColor
                        }]}>
                        <Text style={styles.textButton}>Pagar ahora</Text>
                    </TouchableOpacity>) : null : null}
            </ScrollView>
        </View>);
    }
}