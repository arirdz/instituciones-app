import React from 'react';
import {Alert, AsyncStorage, FlatList, KeyboardAvoidingView, Text, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {CreditCardInput} from 'rn-credit-card-view';
import styles from '../../../styles';
import MultiBotonRow from '../../Globales/MultiBotonRow';
import Card from './card';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Spinner from 'react-native-loading-spinner-overlay';
import openpay from "react-native-openpay";

export default class gestionarTarjetas extends React.Component {
    _onChange = formData => {
        let card = formData.values.number;
        let cardJunto = card.split(' ');
        let expire = formData.values.expiry;
        let expiraSeparado = expire.split('/');
        this.setState({
            cardNumber: cardJunto[0] + cardJunto[1] + cardJunto[2] + cardJunto[3],
            holderName: formData.values.name,
            cvv2: formData.values.cvc,
            month: expiraSeparado[0],
            year: expiraSeparado[1]
        });
    };

    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    async tokenizar() {
        await this._changeWheelState();
        if (this.state.cardNumber.length === 16) {
            if (this.state.cvv2.length) {
                await openpay.setup('mpb6xcs65mhmahdjv935', 'pk_1c63493c1ce241319bfe713ece85fa26');
                await openpay.createCardToken({
                    holder_name: this.state.holderName,
                    card_number: this.state.cardNumber,
                    cvv2: this.state.cvv2,
                    expiration_month: this.state.month,
                    expiration_year: this.state.year,
                    address: {
                        line1: this.state.datos.calle,
                        line2: this.state.datos.colonia,
                        line3: '',
                        state: this.state.datos.estado,
                        city: this.state.datos.ciudad,
                        postal_code: this.state.datos.cp,
                        country_code: 'MX'
                    }
                }).then(token => {
                    this.setState({cardToken: token});
                    openpay.getDeviceSessionId().then(sessionId => {
                        this.setState({session_id: sessionId});
                        this.saveData();
                    });

                }).catch(e => {
                    Alert.alert('Tarjeta invalida', e.message, [{
                        text: 'Entendido', onPress: () => this.setState({visible: false})
                    }]);
                });
            } else {
                Alert.alert('Tarjeta invalida', 'El número del ccv debe ser de 3 a 4 dígitos', [{
                    text: 'Entendido'
                }]);
            }
        } else {
            Alert.alert('Tarjeta invalida', 'El número de la tarjeta debe ser de 16  dígitos', [{
                text: 'Entendido'
            }]);
        }

    }

    saveData = () => {
        let formData = new FormData();
        formData.append('parameters', JSON.stringify({
            token_id: this.state.cardToken,
            device_session_id: this.state.session_id
        }));
        fetch(this.state.uri + '/api/pasarelas/v1/openpay/customer/' + this.state.datos.user_id + '/card/create', {
            method: 'POST', headers: {
                'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(response => response.json())
            .then(responseJson => {
                if (responseJson.response === 'error') {
                    if (responseJson.error.code === 3001) {
                        Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                    } else if (responseJson.error.code === 3002) {
                        Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirada', [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                    } else if (responseJson.error.code === 3003) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                    } else if (responseJson.error.code === 3004) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                    } else if (responseJson.error.code === 3005) {
                        Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                    } else {
                        Alert.alert('¡Tarjeta guardadas!', 'Se ha guardado correctamente la tarjeta', [{
                            text: 'Entendido',
                            onPress: () => [this.setState({visible: false}), Actions.refresh({key: Math.random()})]
                        }]);
                    }
                } else {
                    Alert.alert('¡Tarjeta guardada!', 'Se ha guardado correctamente la tarjeta', [{
                        text: 'Entendido',
                        onPress: () => [this.setState({visible: false}), Actions.refresh({key: Math.random()})]
                    }]);
                }
            });
    };

    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            botonSelected: 'Ver tarjetas registradas',
            Numero: [],
            values: [],
            datos: [],
            cardData: [],
            source_id: '',
            singleCard: [],
            visible: false
        };
    }

    async onChange(option, data) {
        await this.setState({
            textInputValue: option.label, brand: option.brand, source_id: option.id, bank_name: option.bank_name
        });
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getUserdata();
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected, indexSelected: indexSelected
        });
    }

    async getUserdata() {
        let request = await fetch(this.state.uri + '/api/user/data', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let dato = await request.json();
        await this.setState({datos: dato});
        await this.getCardByUser();
    }

    async getCardByUser() {
        await  this.setState({visible: true});
        await fetch(this.state.uri + '/api/pasarelas/v1/openpay/customer/' + this.state.datos.user_id + '/card/get', {
            method: 'POST', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => response.json())
            .then(responseJson => {
                this.setState({cardData: responseJson.result});
            })
            .catch(error => {
                console.warn(error);
            });
        await setTimeout(() => {
            this.setState({visible: false});
        }, 2000)
    }

    registrarTarjetas() {
        return (<KeyboardAvoidingView style={styles.container}>
            <View
                style={{
                    width: responsiveWidth(95), height: responsiveHeight(40), paddingTop: 17
                }}>
                <CreditCardInput
                    autoFocus
                    requiresName
                    requiresCVC
                    allowScroll={true}
                    labels={{
                        number: 'Número de Tarjeta', expiry: 'Expira', cvc: 'CVC/CCV', name: 'Nombre'
                    }}
                    placeholders={{
                        number: '8899 6677 4433 1122', expiry: 'MM/YY', cvc: 'CVC', name: 'Nombre Completo'
                    }}
                    validColor={'green'}
                    invalidColor={'red'}
                    placeholderColor={'darkgray'}
                    onChange={this._onChange}
                />
            </View>
        </KeyboardAvoidingView>);
    }

    render() {
        return (<View style={styles.container}>
            <Spinner visible={this.state.visible} textContent='Procesando...'/>
            <MultiBotonRow
                itemBtns={['Ver tarjetas registradas', 'Registrar nueva tarjeta']}
                onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
                cantidad={2}
            />
            {this.state.botonSelected === 'Ver tarjetas registradas' ? (
                <View style={[styles.container, {...ifIphoneX({marginBottom: 40}, {marginBottom: 20}), marginTop: 10}]}>
                    <FlatList
                        keyExtractor={this._keyExtractor}
                        data={this.state.cardData}
                        renderItem={({item}) => (<Card {...item} />)}
                    />
                </View>
            ) : null}
            {this.state.botonSelected === 'Registrar nueva tarjeta' ? this.registrarTarjetas() : null}
            {this.state.botonSelected === 'Registrar nueva tarjeta' ? (
                <TouchableOpacity style={[styles.bigButton,{backgroundColor:this.state.secondColor}]} onPress={() => this.tokenizar()}>
                    <Text style={styles.textButton}>Guardar Tarjeta</Text>
                </TouchableOpacity>) : null}
        </View>);
    }
}
