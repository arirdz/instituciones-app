import React from 'react';
import {
    AsyncStorage,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import Hijos from '../../Globales/hijos';
import styles, {thirdColor} from '../../../styles';
import {Actions} from 'react-native-router-flux';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class pagosVencidos extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            textInputValue: '',
            cososPicker: [],
            mes: moment().month() + 1,
            cargos: [],
            selectedIndex: -1,
            selectedIndexGrupo: -1,
            selGrad: '',
            selGrup: '',
            elId: '',
            elAlumno: '',
            alumnos: [],
            datos: []
        };
        this.renderPagos = this.renderPagos.bind(this);
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getUserdata();
        this.getMeses();
    }

    //+++++++++++++++++++++++++++ get hijos +++++++++++++++++++++++
    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            elHijo: id.toString(),
            selectedIndexGrupo: -1,
            selItem: ''
        });
        await this.getCargos();
    }

    //+++++++++++++++++++++++++++ get cargos +++++++++++++++++++++++
    async getCargos() {
        let request = await fetch(
            this.state.uri +
            '/api/payments/unpaid/customer/' +
            this.state.datos.user_id +
            '/charges/' +
            this.state.elHijo +
            '/' +
            this.state.mes,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let data = await request.json();
        this.setState({cargos: data});
    }

    async onListItemPressedPago(index, item, total) {
        await this.setState({selectedIndexGrupo: index, selItem: item});
        await this.saveData(item, total);
    }

    renderPago(itemPago, indexPago) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            styles.row,
            {backgroundColor: '#f2f2f2', borderWidth: 0}
        ];
        if (this.state.selectedIndexGrupo == indexPago) {
            bigButtonStyles.push(
                styles.listButton,
                styles.listButtonBig,
                styles.row,
                {backgroundColor: '#ddd', borderWidth: 0}
            );
        }
        var dif = moment().diff(moment(itemPago.fecha_limite_pago), 'months') + 1;
        let recargo = itemPago.importe_a_pagar * (dif / 10);
        recargo = recargo.toFixed(2);
        let total = Number(itemPago.importe_a_pagar) + Number(recargo);
        return (
            <View>
                {this.state.cargos[0].response !== 'Empty' ? (
                    <TouchableOpacity
                        key={indexPago}
                        style={bigButtonStyles}
                        underlayColor={this.state.secondColor}
                        onPress={() =>
                            this.onListItemPressedPago(indexPago, itemPago, total.toString())
                        }>
                        <View style={[styles.btn3, {marginLeft: 9}]}>
                            <Text numberOfLines={1} style={{textAlign: 'left'}}>
                                {itemPago.descripcion}
                            </Text>
                        </View>
                        <View
                            style={[
                                styles.btn6,
                                {height: responsiveHeight(3.75), justifyContent: 'center'}
                            ]}>
                            <Text
                                style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center'
                                }}>
                                ${itemPago.importe_a_pagar}
                            </Text>
                        </View>
                        <View
                            style={[
                                styles.btn3T,
                                {height: responsiveHeight(3.75), justifyContent: 'center'}
                            ]}>
                            <Text
                                style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center'
                                }}>
                                ${recargo}
                            </Text>
                        </View>
                    </TouchableOpacity>
                ) : null}
            </View>
        );
    }

    renderPagos() {
        let buttonsPagos = [];
        this.state.cargos.forEach((itemPago, indexPago) => {
            buttonsPagos.push(this.renderPago(itemPago, indexPago));
        });
        return buttonsPagos;
    }

    // ++++++++++++++++ MESES +++++++++++++++++++++++++
    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            mes: option.mes,
            selItem: ''
        });
        this.getCargos();
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    //+++++++++++++++++++++++++++ get user data +++++++++++++++++++++++
    async getUserdata() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(uri + '/api/user/data', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        let dato = await request.json();
        this.setState({datos: dato});
    }

    //+++++++++++++++++++++++++++ save data +++++++++++++++++++++++
    async saveData(item, total) {
        try {
            await AsyncStorage.setItem('itemid', item.id.toString());
            await AsyncStorage.setItem('itemTotalToPay', total);
            await AsyncStorage.setItem('itemHijoId', this.state.elHijo);
            await AsyncStorage.setItem('itemDescripcion', item.descripcion);
            await AsyncStorage.setItem('item_due_date', item.fecha_limite_pago);
            await AsyncStorage.setItem('userID', this.state.datos.user_id.toString());
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    render() {
        const listaPagos = this.renderPagos();
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== ''){
            if (this.state.cososPicker.length !== 0){
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text
                        style={[
                            styles.main_title,
                            {
                                color: this.state.thirdColor,
                                marginBottom: 1
                            }
                        ]}>
                        Elija el periodo a consultar
                    </Text>
                    <ModalSelector
                        data={data}
                        cancelText="Cancelar"
                        optionTextStyle={{
                            color: thirdColor
                        }}
                        selectStyle={[
                            styles.inputPicker,
                            {
                                borderColor: this.state.secondColor,
                                marginTop: 2
                            }
                        ]}
                        initValue={mess}
                        onChange={option => this.onChange(option)}
                    />
                    <Hijos
                        onSelectedChamaco={(index, grado, grupo, id) =>
                            this.onListItemPressed(index, grado, grupo, id)
                        }
                    />
                    <View
                        style={{
                            width: responsiveWidth(95)
                        }}>
                        <Text
                            style={[
                                styles.title,
                                {
                                    color: this.state.thirdColor,
                                    paddingBottom: 0
                                }
                            ]}>
                            Lista de pagos vencidos
                        </Text>
                    </View>
                    <View>
                        <View style={[styles.rowsCalif, {marginBottom: 5}]}>
                            <View style={styles.btn4}>
                                <Text style={{fontWeight: '700', textAlign: 'center'}}>
                                    Concepto
                                </Text>
                            </View>
                            <View style={styles.btn2_5}>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        textAlign: 'right'
                                    }}>
                                    importe
                                </Text>
                            </View>
                            <View style={styles.btn2}>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        textAlign: 'center',
                                        marginLeft: 5
                                    }}>
                                    Recargo
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                height: responsiveHeight(23),
                                borderBottomWidth: 1,
                                borderTopWidth: 1,
                                marginBottom: 5
                            }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {listaPagos}
                            </ScrollView>
                        </View>
                    </View>
                    <View
                        style={{
                            alignItems: 'flex-start',
                            width: responsiveWidth(95),
                            height: responsiveHeight(5)
                        }}>
                        <Text
                            style={[
                                styles.main_title,
                                {
                                    color: this.state.thirdColor,
                                    marginBottom: 1
                                }
                            ]}>
                            Detalle del pago
                        </Text>
                        <Text
                            style={{
                                color: this.state.thirdColor,
                                fontSize: responsiveFontSize(2),
                                fontWeight: '300'
                            }}>
                            Descripción:
                        </Text>
                    </View>
                    {this.state.selectedIndexGrupo !== -1 ? (
                        <View
                            style={{
                                backgroundColor: '#ddd',
                                width: responsiveWidth(95),
                                height: responsiveHeight(20),
                                marginTop: 25,
                                borderRadius: 6,
                                marginBottom: 10
                            }}>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    marginTop: 10,
                                    marginHorizontal: 10,
                                    textAlign: 'left'
                                }}>
                                El pago fue solicitado el dia:{' '}
                                <Text
                                    style={{
                                        fontWeight: '600',
                                        marginLeft: 20
                                    }}>
                                    {moment(this.state.selItem.fecha_limite_pago).format(
                                        'DD/MM/YY'
                                    )}
                                </Text>
                                {' \n'}
                                Por el monto de: ${this.state.selItem.importe_a_pagar}
                            </Text>
                            <Text
                                style={{
                                    width: responsiveWidth(90),
                                    height: responsiveHeight(8),
                                    marginTop: 30,
                                    marginHorizontal: 10,
                                    textAlign: 'center',
                                    fontWeight: '600'
                                }}>
                                {this.state.selItem.descripcion}
                            </Text>
                        </View>
                    ) : null}
                    <View style={[styles.widthall, {paddingTop: 10}]}>
                        <View
                            style={{borderRadius: 6, padding: 10, backgroundColor: 'red'}}>
                            <Text
                                style={{
                                    color: 'white',
                                    textAlign: 'center'
                                }}>
                                <Text style={{fontWeight: '700'}}>Importante:</Text> todos los
                                pagos vencidos generan un
                                <Text style={{fontWeight: '700'}}>
                                    {' '}
                                    recargo automatico del 10%
                                </Text>por cada mes de retraso que se acumule. Le invamos a
                                ponerse al corriente lo antes posible
                            </Text>
                        </View>
                    </View>
                </ScrollView>
                {this.state.selectedIndexGrupo !== -1 ? (
                    <TouchableOpacity
                        onPress={() => Actions.formadepagoquenofunciona()}
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.mainColor,
                                borderColor: this.state.mainColor
                            }
                        ]}>
                        <Text style={styles.textButton}>Pagar ahora</Text>
                    </TouchableOpacity>
                ) : null}
            </View>
        );
    }
}
