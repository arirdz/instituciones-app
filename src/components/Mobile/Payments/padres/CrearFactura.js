import React from 'react';
import PropTypes from 'prop-types';
import {
	AsyncStorage,
	Text,
	View,
	TouchableHighlight,
	StatusBar,
	ScrollView,
	TextInput,
	TouchableOpacity, Alert
} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../../styles';

import {Actions} from 'react-native-router-flux';
import Feather from 'react-native-vector-icons/Feather';
import Spinner from 'react-native-loading-spinner-overlay';
import datosFiscales from "./datosFiscales";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class CrearFactura extends React.Component {
	static propTypes = {};

	static defaultProps = {};

	constructor(props) {
		super(props);
		this.state = {
			pagos: [],
			consultar: false,
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getPagos();
	}


	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	getPagos() {
		this.setState({consultar: true})
		fetch(this.state.uri + '/api/get/pagosCompletados', {
			method: 'get', headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		})
			.then(response => {
				return response.json();
			})
			.then(recurso => {

					this.setState({consultar: false,pagos: recurso})
					console.log(this.state.pagos)
			});
	}

	detalleFacturacion(item){
		console.log(item)
		if(item.datos_fac === null){
            Alert.alert('Atención', 'Para facturar, primero debe llenar los datos de facturación (RFC)', [{
                text: 'Enterado', onPress: () => [Actions.datosFiscales()]
            }]);
		}else if (item.hijo_data.curp === '1'){
            Alert.alert('Atención', 'Para facturar, primero debe llenar los datos de facturación (CURP)', [{
                text: 'Enterado', onPress: () => [Actions.datosFiscales()]
            }]);
		}else{
            Actions.DetalleFacturacion({factura: item})
		}
	}

	card() {
		let carta = [];
		this.state.pagos.forEach((item, index) => {
			carta.push(
				<View
					key={'i' + index}
					style={[
						styles.widthall,
						// styles.row,
						{
							marginVertical: 5,
							borderWidth: 0.5,
							borderRadius: 6,
							borderColor: 'lightgray',
							backgroundColor: this.state.fourthColor,
							paddingVertical: 6
						}
					]}>
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'flex-start',
						padding: 0,
						paddingHorizontal: 10
					}]}>
						<View style={[styles.btn1_2]}>
							<Text
								style={{
									fontSize: responsiveFontSize(1.5),
									textAlign: 'left'
								}}>{item.cargo.tipo_pago}</Text>
							<Text
								numberOfLines={2}
								ellipsizeMode={'tail'}
								style={[
									styles.textW,
									{fontSize: responsiveFontSize(1.2), textAlign: 'left', fontWeight: '900'}
								]}>
								{item.cargo.descripcion}
							</Text>
						</View>
						<View style={[styles.btn5, {alignItems: 'center'}]}>
							<Text style={{textAlign: 'center', fontSize: responsiveFontSize(1.3)}}>
								Fecha
							</Text>
							<Text style={[styles.textW, {fontSize: responsiveFontSize(1.2), fontWeight: '900'}]}>
								{moment(item.date).format('DD/MM/YY')}
							</Text>
						</View>
						<View style={[styles.btn5, {alignItems: 'center'}]}>
							<Text style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>Importe</Text>
							<Text
								numberOfLines={2}
								ellipsizeMode={'tail'}
								style={[
									styles.textW,
									{fontSize: responsiveFontSize(1.2), textAlign: 'center', fontWeight: '900'}
								]}>
								{'$' + item.total}
							</Text>
						</View>
					</View>
					<View
						style={{
							borderBottomColor: this.state.thirdColor,
							borderBottomWidth: .5,
							marginVertical: 10
						}}
					/>
					<View style={[styles.row, styles.widthall, {
						textAlign: 'center',
						justifyContent: 'center',
						alignItems: 'center',
						margin: 0
					}]}>
						<TouchableOpacity style={[styles.row, {
							backgroundColor: this.state.secondColor,
							paddingVertical: 5,
							paddingHorizontal: 10,
							borderRadius: 5,
							marginVertical: 5,
							alignItems: "center",
							justifyContent: "center",
						}]}
										  onPress={() => [ this.detalleFacturacion(item)]}>
							<Text style={[styles.textW, {color: 'white', marginRight: 10}]}>Facturar ahora</Text>
							<Feather name='chevrons-right' size={20} color='#fff'
									 style={{paddingVertical: 3}}
									 onPress={() => this.send(item)}
							/>
						</TouchableOpacity>
					</View>
				</View>
			)
		});
		return carta;
	}


	render() {
		const lacarta = this.card();
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.consultar} textContent='Consultando pagos facturables...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Lista de pagos facturables
				</Text>
				<ScrollView showsVerticalScrollIndicator={false}>
					{this.state.pagos.length === 0 ?
						(<Text style={[styles.main_title, {
							color: '#a8a8a8',
							textAlign: 'center',
							marginTop: 40,
							marginBottom: 40,
							fontSize: responsiveFontSize(3)
						}]}>
							No existen pagos facturables en este momento
						</Text>):
						(lacarta)
					}
				</ScrollView>
			</View>
		);
	}
}