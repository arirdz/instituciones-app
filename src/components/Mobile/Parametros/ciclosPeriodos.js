import React from 'react';
import {
	Alert,
	AsyncStorage,
	ScrollView,
	StatusBar,
	Text,
	TextInput,
	TouchableOpacity,
	KeyboardAvoidingView,
	View
} from 'react-native';
import {responsiveFontSize, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ModalSelector from 'react-native-modal-selector';
import {Actions} from 'react-native-router-flux';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class ciclosPeriodos extends React.Component {


	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			isDateTimePickerVisible: [],
			isDateTimePickerVisible2: [],
			data: [],
			elCiclo: [],
			selecNum: '',
			fechagg: [],
			selectedPD: -1,
			fechagg1: [],
			nombCiclo: '',
			nombPer: '',
			elNivel: [],
			periodoDur: '',
			tipoCicl: [],
			elAño: '1',
			aux: 0,
			elIndice: '',
			indexAño: 0,
			elIndice2: '',
			cantCiclo: [],
			cicloPicker: [],
			pickerCiclo: [],
			elPeriodo: [],
			nivelEsc: [],
			elValue: '0',
			escuelasDatos: [],
			nombrEsc: [],
			elRequest: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getSchoolData();
		await this.getNiveles();
		await this.getCiclosPeriodos();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});

	}


	async borrarCiclo() {
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				periodo: 1,
				periodoName: this.state.nombPer,
				ciclo: 1,
				cicloName: this.state.nombCiclo,
				activo: 0,
				inicio: this.state.fechagg[0 + '' + 0],
				fin: this.state.fechagg1[0 + '' + 0]
			})
		);
		await fetch(this.state.uri + '/api/delete/periodos', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Ciclos periodos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			await this.requestMultipart();
		}
	}

	arregloPeriodo() {
		let btnPeriodo = [];
		for (let i = 0; i < this.state.selecNum; i++) {
			for (let j = 0; j < this.state.periodoDur; j++) {
				btnPeriodo.push({
					periodo: j + 1,
					periodoName: this.state.nombPer,
					ciclo: i + 1,
					cicloName: this.state.nombCiclo,
					activo: 1,
					inicio: this.state.fechagg[j],
					fin: this.state.fechagg1[j]
				})
			}
		}
		return btnPeriodo
	}

	async requestMultipart() {
		const elPeriodoArr = this.arregloPeriodo();
		let formData = new FormData();
		formData.append('new', JSON.stringify(elPeriodoArr));
		await fetch(this.state.uri + '/api/post/periodos', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Ciclos periodos)',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		} else {
			await this.borrarGrado();

		}
	}

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	async borrarGrado() {
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			grado: 1
		}));
		await fetch(this.state.uri + '/api/delete/grados', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Ciclos grados)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			await this.requestMultipart3();
		}
	}

	async requestMultipart3() {
		let formData = new FormData();
		for (let i = 0; i < this.state.selecNum; i++) {
			formData.append('new', JSON.stringify({
				grado: i + 1
			}));
			await fetch(this.state.uri + '/api/post/grados', {
				method: 'POST', headers: {
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				}, body: formData
			}).then(res => res.json())
				.then(responseJson => {
					this.setState({elRequest: responseJson});
				})
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Ciclos grados)',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('¡Felicidades!',
				'Los ciclos han sido guardados',
				[{
					text: 'Entendido',
					onPress: () => Actions.menuParam()
				}]
			);
		}
	}

	async cicloGrpo() {
		Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [{
			text: 'Si',
			onPress: () => [
				this.borrarCiclo()
			]
		}, {text: 'No'}]);
	}

	_handleDatePicked(date, i) {
		let fecha = moment(date).format('YYYY-MM-DD');
		this.state.fechagg[i] = fecha;
		this.setState({aux: 0});
		this._hideDateTimePicker(i);
	};

	_handleDatePicked1(date, i) {
		let fecha1 = moment(date).format('YYYY-MM-DD');
		this.state.fechagg1[i] = fecha1;
		this.setState({aux: 0});
		this._hideDateTimePicker2(i);
	};

	//++++++++++timepicker
	async _hideDateTimePicker(i) {
		this.state.isDateTimePickerVisible[i] = false;
		await this.setState({aux: 0});
	}

	//+++++++++++timepicker 1
	async _hideDateTimePicker2(i) {
		this.state.isDateTimePickerVisible2[i] = false;
		await this.setState({aux: 0});
	}

	async onChangeText(text) {
		await this.setState({
			nombPer: text
		});
		await this.setState({aux: 0});
	}

	//++++++++++++++++++++++++++++ciclos cursables
	async onChange1(option1) {
		this.state.periodoDur = option1.label;
		for (let i = 0; i < this.state.periodoDur; i++) {
			this.state.isDateTimePickerVisible.push(false);
			this.state.isDateTimePickerVisible2.push(false);
		}
		await this.setState({aux: 0});
	}

	// async onChange2(option2) {
	// 	this.state.periodoDur[1] = option2.label;
	// 	for (let i = 0; i < this.state.periodoDur[1]; i++) {
	// 		this.state.isDateTimePickerVisible.push(false);
	// 		this.state.isDateTimePickerVisible2.push(false);
	// 	}
	// 	await this.setState({
	// 		aux: 0
	// 	});
	// }
	//
	// async onChange3(option3) {
	// 	this.state.periodoDur[2] = option3.label;
	// 	for (let i = 0; i < this.state.periodoDur[2]; i++) {
	// 		this.state.isDateTimePickerVisible.push(false);
	// 		this.state.isDateTimePickerVisible2.push(false);
	// 	}
	// 	await this.setState({
	// 		aux: 0
	// 	});
	// }
	//
	// async onChange4(option4) {
	// 	this.state.periodoDur[3] = option4.label;
	// 	for (let i = 0; i < this.state.periodoDur[3]; i++) {
	// 		this.state.isDateTimePickerVisible.push(false);
	// 		this.state.isDateTimePickerVisible2.push(false);
	// 	}
	// 	await this.setState({
	// 		aux: 0
	// 	});
	// }
	//
	// async onChange5(option5) {
	// 	this.state.periodoDur[4] = option5.label;
	// 	for (let i = 0; i < this.state.periodoDur[4]; i++) {
	// 		this.state.isDateTimePickerVisible.push(false);
	// 		this.state.isDateTimePickerVisible2.push(false);
	// 	}
	// 	await this.setState({
	// 		aux: 0
	// 	});
	// }
	//
	// async onChange6(option6) {
	// 	this.state.periodoDur[5] = option6.label;
	// 	for (let i = 0; i < this.state.periodoDur[5]; i++) {
	// 		this.state.isDateTimePickerVisible.push(false);
	// 		this.state.isDateTimePickerVisible2.push(false);
	// 	}
	// 	await this.setState({
	// 		aux: 0
	// 	});
	// }

	async getSchoolData() {
		let scid = await AsyncStorage.getItem('schoolId');
		let schoolData = await fetch('https://controlescolar.pro/api/get/escuelas/datos/' + scid);
		let scData = await schoolData.json();
		this.setState({escuelasDatos: scData});
		this.setState({nombrEsc: this.state.escuelasDatos[0].nombre_corto});
	}


	async getNiveles() {
		let lv = await fetch(this.state.uri + '/api/get/escuela/level/' + this.state.nombrEsc, {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let lvs = await lv.json();
		await this.setState({elNivel: lvs});
		if (this.state.elNivel.nivel === 'Secundaria') {
			this.state.selecNum = '3';
			this.state.nombCiclo = 'Año';
		} else if (this.state.elNivel.nivel === 'Preparatoria') {
			this.state.selecNum = '6';
			this.state.nombCiclo = 'Semestre';
		} else if (this.state.elNivel.nivel === 'Primaria') {
			this.state.selecNum = '6';
			this.state.nombCiclo = 'Año';
		} else if (this.state.elNivel.nivel === 'Preescolar') {
			this.state.selecNum = '3';
			this.state.nombCiclo = 'Año';
		}
	}

	////+++++++++++++++++++++++++++++++++++++++++++++++++++Ciclos nombre
	async getCiclosPeriodos() {
		let ciclo = await fetch(this.state.uri + '/api/get/periodos/parametro', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let ciclos = await ciclo.json();
		await this.setState({elCiclo: ciclos});
		await this.setState({nombPer: this.state.elCiclo[0].nombre_periodo.toString()});
		await this.setState({periodoDur: this.state.elCiclo[0].periodos.length.toString()});
		for (let i = 0; i < this.state.periodoDur; i++) {
			this.state.fechagg[i] = this.state.elCiclo[0].periodos[i].inicio;
			this.state.fechagg1[i] = this.state.elCiclo[0].periodos[i].fin;
		}
		await this.setState({aux: 0});
	}

	//-+-+-+-+-+-+-+-+-+-+-+-+-+-

	async onChange(option) {
		await this.setState({
			selected: option.label,
			nombCiclo: option.name
		});
	}

	async onChangeGdo(option) {
		await this.setState({
			indexAño: option.key,
			elAño: option.label
		});
	}

	//+++++++++++++++++++++++++++++++++++ duracion +++++++++++++++++++++++++++++++
	async prueba(i) {
		this.state.isDateTimePickerVisible[i] = true;
		await this.setState({aux: 0});
	}

	async prueba2(i) {
		this.state.isDateTimePickerVisible2[i] = true;
		await this.setState({
			elIndice2: i
		});
		await this.setState({aux: 0});
	}

	renderDuracion(i) {
		let picker = [styles.inputPicker, {borderColor: this.state.secondColor}];
		return (
			<View
				key={i + 'picker'}
				style={[styles.row, styles.widthall, {marginVertical: 3, justifyContent: 'space-between'}]}>
				<Text
					style={{width: responsiveWidth(20), fontSize: responsiveFontSize(1.6)}}
				>
					{this.state.nombPer} {i + 1}
				</Text>
				<TouchableOpacity
					style={[picker, styles.btn2_5]}
					onPress={() => this.prueba(i)}
				>
					{this.state.fechagg[i] === undefined ? (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							Seleccione fecha
						</Text>
					) : (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							{moment(this.state.fechagg[i],'YYYY-MM-DD').format('DD/MM/YYYY')}
						</Text>
					)}
				</TouchableOpacity>
				<DateTimePicker
					locale={'es'}
					isVisible={this.state.isDateTimePickerVisible[i]}
					onConfirm={(date) => this._handleDatePicked(date, i)}
					onCancel={() => this._hideDateTimePicker(i)}
					titleIOS={'Seleccione una fecha'}
					confirmTextIOS={'Seleccionar'}
					cancelTextIOS={'Cancelar'}
				/>
				<TouchableOpacity
					style={[picker, styles.btn2_5]}
					onPress={() => this.prueba2(i)}
				>
					{this.state.fechagg1[i] === undefined ? (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							Seleccione fecha
						</Text>
					) : (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							{moment(this.state.fechagg1[i],'YYYY-MM-DD').format('DD/MM/YYYY')}
						</Text>)}
				</TouchableOpacity>
				<DateTimePicker
					locale={'es'}
					isVisible={this.state.isDateTimePickerVisible2[i]}
					onConfirm={(date) => this._handleDatePicked1(date, i)}
					onCancel={() => this._hideDateTimePicker2(i)}
					titleIOS={'Seleccione una fecha'}
					confirmTextIOS={'Seleccionar'}
					cancelTextIOS={'Cancelar'}
				/>
			</View>
		);
	}

	renderDuraciones() {
		let duraPeriodo = [];
		for (let i = 0; i < this.state.periodoDur; i++) {
			duraPeriodo.push(this.renderDuracion(i));
		}
		return duraPeriodo;
	}

	//+++++++++++++++++++ ciclo
	// renderCiclo(index) {
	// 	if (this.state.indexAño === index) {
	// 		const duracion = this.renderDuraciones(index);
	// 		return (
	// 			<View key={index + 'rndCiclo'} style={styles.container}>
	// 				<View style={[styles.widthall, {marginTop: 20}]}>
	// 					{/*<Text style={styles.main_title}>*/}
	// 						{/*{this.state.nombCiclo} {index + 1}*/}
	// 					{/*</Text>*/}
	// 					<View style={styles.ciclosRen}>
	// 						<Text style={styles.textW}>Inicia</Text>
	// 						<Text style={styles.textW}>Finaliza</Text>
	// 					</View>
	// 					{duracion}
	// 				</View>
	// 			</View>
	// 		);
	// 	}
	// }
	//
	// renderCiclos() {
	// 	let btnCiclo = [];
	// 	for (let i = 0; i < this.state.selecNum; i++) {
	// 		btnCiclo.push(this.renderCiclo(i));
	// 		this.state.cicloPicker.push(i);
	// 	}
	// 	return btnCiclo;
	// }

	// duplicado() {
	// 	var el = this.state.cicloPicker.concat().sort();
	// 	for (var i = 1; i < el.length;) {
	// 		if (el[i - 1] === el[i]) el.splice(i, 1);
	// 		else i++;
	// 	}
	// 	this.state.pickerCiclo = el;
	// 	this.setState({aux: 0});
	// }

	// async duraciones(i) {
	// 	if (this.state.periodoDur[i] === []) {
	// 		Alert.alert(
	// 			'¡Ups!', 'te faltó poner cantidad de periodos por ciclo',
	// 			[
	// 				{text: 'Entendido'}
	// 			]);
	// 	} else if (this.state.nombPer === '') {
	// 		Alert.alert(
	// 			'¡Ups!', 'te faltó poner nombre de los periodos',
	// 			[
	// 				{text: 'Entendido'}
	// 			]);
	// 	} else {
	// 		await this.setState({
	// 			selectedPD: 0
	// 		});
	// 	}
	// 	await this.setState({
	// 		aux: 0
	// 	});
	// }

	rendCiclos() {
		let pickercito = [styles.pickerParam1, {borderColor: this.state.secondColor}];
		let picker = [styles.inputPicker, {borderColor: this.state.secondColor, marginTop: 3}];
		let textito = [{fontSize: responsiveFontSize(1.25)}];
		let index = 0;
		const data1 = [
			{key: index++, label: '1'},
			{key: index++, label: '2'},
			{key: index++, label: '3'},
			{key: index++, label: '4'},
			{key: index++, label: '5'}
		];
		// const data2 = [
		// 	{key: index++, label: '1'},
		// 	{key: index++, label: '2'},
		// 	{key: index++, label: '3'},
		// 	{key: index++, label: '4'},
		// 	{key: index++, label: '5'}
		// ];
		// const data3 = [
		// 	{key: index++, label: '1'},
		// 	{key: index++, label: '2'},
		// 	{key: index++, label: '3'},
		// 	{key: index++, label: '4'},
		// 	{key: index++, label: '5'}
		// ];
		// const data4 = [
		// 	{key: index++, label: '1'},
		// 	{key: index++, label: '2'},
		// 	{key: index++, label: '3'},
		// 	{key: index++, label: '4'},
		// 	{key: index++, label: '5'}
		// ];
		// const data5 = [
		// 	{key: index++, label: '1'},
		// 	{key: index++, label: '2'},
		// 	{key: index++, label: '3'},
		// 	{key: index++, label: '4'},
		// 	{key: index++, label: '5'}
		// ];
		// const data6 = [
		// 	{key: index++, label: '1'},
		// 	{key: index++, label: '2'},
		// 	{key: index++, label: '3'},
		// 	{key: index++, label: '4'},
		// 	{key: index++, label: '5'}
		// ];
		return (
			<View>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>Paso 1: Ciclos</Text>

				<View style={styles.widthParm}>
					<Text
						style={[styles.widthall, {fontWeight: '800', margin: 0}]}
					>
						Cantidad de periodos de evaluación
					</Text>
					{this.state.selecNum === '' ? (
						<Text
							style={[styles.modalWidth, {textAlign: 'center', fontStyle: 'italic'}]}
						>
							"Una vez seleccionado el número de ciclos cursables apareceran recuadros para
							definir, cuantos periodos habrá en cada ciclo"
						</Text>
					) : null}
					{/*{this.state.selecNum === '1' ? (*/}
					<ModalSelector
						data={data1}
						selectStyle={picker}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor}}
						initValue={this.state.periodoDur}
						onChange={option1 => this.onChange1(option1)}
					/>
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*) : null}*/}
					{/*{this.state.selecNum === '2' ? (*/}
					{/*<View>*/}
					{/*{this.state.ciclos === true ? (*/}
					{/*<View style={[styles.row, styles.btn7]}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data1}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{color: this.state.thirdColor}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[0]}*/}
					{/*onChange={option1 => this.onChange1(option1)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data2}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{color: this.state.thirdColor}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[1]}*/}
					{/*onChange={option2 => this.onChange2(option2)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*) : (*/}
					{/*<View style={[styles.row, styles.btn7]}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[0]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[1]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*)}*/}
					{/*</View>*/}
					{/*) : null}*/}
					{/*{this.state.selecNum === '3' ? (*/}
					{/*<View>*/}
					{/*{this.state.ciclos === true ? (*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data1}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.periodoDur[0]}*/}
					{/*onChange={option1 => this.onChange1(option1)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data2}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={*/}
					{/*this.state.elCiclo === [] ? '0' : this.state.periodoDur[1]*/}
					{/*}*/}
					{/*onChange={option2 => this.onChange2(option2)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data3}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[2]}*/}
					{/*onChange={option3 => this.onChange3(option3)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*3er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*) : (*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[0]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[1]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[2]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*3ro {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*)}*/}
					{/*</View>*/}
					{/*) : null}*/}
					{/*{this.state.selecNum === '4' ? (*/}
					{/*<View>*/}
					{/*{this.state.ciclos === true ? (*/}
					{/*<View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data1}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[0]}*/}
					{/*onChange={option => this.onChange1(option)}*/}
					{/*/>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.25)}}*/}
					{/*>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data2}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[1]}*/}
					{/*onChange={option2 => this.onChange2(option2)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data3}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[2]}*/}
					{/*onChange={option3 => this.onChange3(option3)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*3er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*<View style={styles.btn3}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data4}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[3]}*/}
					{/*onChange={option4 => this.onChange4(option4)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*4to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*) : (*/}
					{/*<View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[0]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[1]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[2]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*3ro {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*<View style={styles.btn3}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[3]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*4to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*)}*/}
					{/*</View>*/}
					{/*) : null}*/}
					{/*{this.state.selecNum === '5' ? (*/}
					{/*<View>*/}
					{/*{this.state.ciclos === true ? (*/}
					{/*<View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data1}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[0]}*/}
					{/*onChange={option1 => this.onChange1(option1)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data2}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[1]}*/}
					{/*onChange={option2 => this.onChange2(option2)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data3}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[2]}*/}
					{/*onChange={option3 => this.onChange3(option3)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*3er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*<View style={[styles.row, styles.btn7]}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data4}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[3]}*/}
					{/*onChange={option4 => this.onChange3(option4)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*4to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data5}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[4]}*/}
					{/*onChange={option5 => this.onChange5(option5)}*/}
					{/*/>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.25)}}*/}
					{/*>*/}
					{/*5to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*) : (*/}
					{/*<View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[0]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[1]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[2]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*3ro {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*<View style={[styles.row, styles.btn7]}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[3]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*4to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[4]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*5to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*)}*/}
					{/*</View>*/}
					{/*) : null}*/}
					{/*{this.state.selecNum === '6' ? (*/}
					{/*<View>*/}
					{/*{this.state.ciclos === true ? (*/}
					{/*<View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data1}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[0]}*/}
					{/*onChange={option1 => this.onChange1(option1)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data2}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[1]}*/}
					{/*onChange={option2 => this.onChange2(option2)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data3}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[2]}*/}
					{/*onChange={option3 => this.onChange3(option3)}*/}
					{/*/>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.25)}}*/}
					{/*>*/}
					{/*3er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data4}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[3]}*/}
					{/*onChange={option4 => this.onChange4(option4)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*4to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data5}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[4]}*/}
					{/*onChange={option5 => this.onChange5(option5)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*5to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<ModalSelector*/}
					{/*data={data6}*/}
					{/*selectStyle={pickercito}*/}
					{/*cancelText='Cancelar'*/}
					{/*optionTextStyle={{*/}
					{/*color: this.state.thirdColor*/}
					{/*}}*/}
					{/*initValue={this.state.elCiclo === [] ? '0' : this.state.periodoDur[5]}*/}
					{/*onChange={option6 => this.onChange6(option6)}*/}
					{/*/>*/}
					{/*<Text style={textito}>*/}
					{/*6to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*) : (*/}
					{/*<View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[0]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*1er {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[1]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*2do {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[2]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*3ro {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*<View style={styles.row}>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[3]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*4to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[4]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*5to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<View style={{alignItems: 'center'}}>*/}
					{/*<View style={pickercito}>*/}
					{/*<Text*/}
					{/*style={{fontSize: responsiveFontSize(1.9), color: 'grey'}}>*/}
					{/*{this.state.periodoDur[5]}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*<Text style={textito}>*/}
					{/*6to {this.state.nombCiclo}*/}
					{/*</Text>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*</View>*/}
					{/*)}*/}
					{/*</View>*/}
					{/*) : null}*/}
				</View>
				<Text
					style={[styles.widthall, {fontWeight: '800', marginTop: 10}]}
				>
					Nombre del los periodos de evaluación
				</Text>
				<View>
					<TextInput
						keyboardType='default'
						maxLength={254}
						onChangeText={text => this.onChangeText(text)}
						placeholder={'ej.: Bimestre, Módulo, Parcial, etc...'}
						defaultValue={this.state.nombPer}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						style={[picker, {textAlign: 'center'}]}
					/>
				</View>
				{/*<TouchableOpacity*/}
				{/*style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}*/}
				{/*onPress={(i) => [this.duplicado(), this.duraciones(i)]}*/}
				{/*>*/}
				{/*<Text style={styles.textButton}>Definir tiempos de duración</Text>*/}
				{/*</TouchableOpacity>*/}
			</View>
		);
	}

	rendDuracion() {

		// let data = this.state.pickerCiclo.map((item, i) => {
		// 	return {
		// 		key: i,
		// 		label: item + 1
		// 	}
		// });
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Paso 2: Duración de los periodos
				</Text>
				{/*<ModalSelector*/}
				{/*data={data}*/}
				{/*selectStyle={[*/}
				{/*styles.inputPicker,*/}
				{/*{borderColor: this.state.secondColor, marginBottom: 5}*/}
				{/*]}*/}
				{/*cancelText='Cancelar'*/}
				{/*optionTextStyle={{color: this.state.thirdColor}}*/}
				{/*initValue={this.state.elAño}*/}
				{/*onChange={option => this.onChangeGdo(option)}*/}
				{/*/>*/}
				{/*<ScrollView showsVerticalScrollIndicator={false}>*/}
				<View
					style={[
						styles.widthall,
						{
							...ifIphoneX({marginBottom: 30}, {marginBottom: 10}),
							marginTop: 15,
							alignItems: 'flex-end'
						}
					]}
				>
					<View style={[styles.row, styles.btn2, {marginRight: 50}]}>
						<Text style={styles.textW}>Inicio</Text>
						<Text style={[styles.textW, {marginRight: 10}]}>Fin</Text>
					</View>
					{this.renderDuraciones()}
				</View>
				{/*</ScrollView>*/}
				{/*<View style={[styles.row]}>*/}
				{/*<TouchableOpacity*/}
				{/*style={[styles.btnCalifs, {backgroundColor: this.state.secondColor}]}*/}
				{/*onPress={() => this.setState({selectedPD: -1})}*/}
				{/*>*/}
				{/*<Text style={styles.textButton}>Volver</Text>*/}
				{/*</TouchableOpacity>*/}
				<TouchableOpacity
					style={[styles.bigButton, {
						backgroundColor: this.state.secondColor, borderLeftWidth: 1, borderColor: 'white'
					}]}
					onPress={() => this.cicloGrpo()}
				>
					<Text style={styles.textButton}>Guardar</Text>
				</TouchableOpacity>
				{/*</View>*/}
			</View>
		);
	}

	render() {
		const parteUno = this.rendCiclos();
		const parteDos = this.rendDuracion();
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				{parteUno}
				{parteDos}
			</View>
		);
	}
}
