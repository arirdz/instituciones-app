import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	StatusBar,
	TextInput,
	KeyboardAvoidingView,
	TouchableOpacity,
	ScrollView,
	Alert, Platform
} from 'react-native';
import {
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import ModalSelector from 'react-native-modal-selector';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import {Fumi} from 'react-native-textinput-effects';
import {Actions} from 'react-native-router-flux';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class AltaMaestro extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			data: [],
			elActivo: '',
			elNombre: '',
			apellido_paterno: '',
			apellido_materno: '',
			email: '',
			elGenero: '',
			maincolor: '#fff',
			secondColor: '#fff',
			thirdColor: '#fff',
			fourthColor: '#fff',
			elRequest: []
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async requestMultipart() {
		await fetch(this.state.uri + '/api/auth/singup', {
			method: 'post',
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: this.state.elNombre + ' ' + this.state.apellido_paterno + ' ' + this.state.apellido_materno,
				email: this.state.email,
				password: 'asdasd',
				role: 'Maestro',
				activo: this.state.elActivo,
				estatus: '',
				puesto: 'Maestro',
				PushNotificationsID: ' ',
				escuela_id: '1',
				grado: '0',
				grupo: '0',
				padre_id: '0',
				genero: 'Masculino'
			})
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Alta maestros)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
            this.sendCorreo();
			Alert.alert('¡Felicidades!',
				'Se ha dado de alta al maestro',
				[{
					text: 'Entendido',
					onPress: () => Actions.refresh({key: Math.random()})
				}]);
		}
	};

    async sendCorreo(){
        await fetch(this.state.uri + '/api/get/enviarCorreo/nuevoMaestro/'+ this.state.email, {
            method: 'get', headers: {
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    let text = '¡Ups! Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Noticia)';
                    this.alert(text);
                } else {
                    let text = 'Maestro dado de alta en el sistema';
                    this.alert1(text);
                }
            });

    }

	async captMaestro() {
		if (this.state.elNombre === '') {
			Alert.alert(
				'¡Ups!', 'No ha escrito el nombre',
				[
					{text: 'Entendido'}
				]);
		} else if (this.state.apellido_paterno === '') {
			Alert.alert(
				'¡Ups!', 'No ha escrito el apellido paterno',
				[
					{text: 'Entendido'}
				]);
		} else if (this.state.apellido_materno === '') {
			Alert.alert(
				'¡Ups!', 'No ha escrito el apellido materno',
				[
					{text: 'Entendido'}
				]);
		} else if (this.state.email === '') {
			Alert.alert(
				'¡Ups!', 'No ha escrito el correo electronico',
				[
					{text: 'Entendido'}
				]);
		} else if (this.state.elActivo === '') {
			Alert.alert(
				'¡Ups!', 'No ha definido el estatus del maestro',
				[
					{text: 'Entendido'}
				]);
		}
		else {
			Alert.alert('¡Atencion!', '¿Seguro que desea continuar?', [
				{
					text: 'Si',
					onPress: () => this.requestMultipart()
				},
				{
					text: 'No'
				}
			]);
		}
		await this.setState({aux: 0});
	}

	async onChange(option) {
		await this.setState({
			elActivo: option.activo,
			elEstatus: option.label
		})
	}

	render() {
		let index = 0;
		const data = [
			{key: index++, label: 'Activo', activo: '1'},
			{key: index++, label: 'Inactivo', activo: '0'}
		];
		return (
			<KeyboardAvoidingView
				style={[styles.container, {backgroundColor: '#fff'}]}
				behavior={Platform.OS === 'ios' ? 'padding' : null}
				keyboardVerticalOffset={120}
				enabled
			>
				<ScrollView>
					<Text style={[styles.main_title, {color: this.state.thirdColor, paddingTop: 0}]}>
						Alta de maestros
					</Text>

					<Text style={[styles.widthall, {fontWeight: '800', marginTop: 5}]}>
						Ingrese el nombre(s)
					</Text>
					<Fumi
						label={'Nombre(s)'}
						iconClass={Ionicons}
						style={[
							styles.inputDato,
							{
								borderColor: this.state.secondColor,
								height: responsiveHeight(9.5)
							}
						]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.elNombre = text)}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.apellidoPat.focus()}
					/>
					<Text style={[styles.widthall, {fontWeight: '800', marginTop: 10}]}>
						Ingrese el primer apellido
					</Text>
					<Fumi
						ref='apellidoPat'
						label={'Apellido paterno'}
						iconClass={Ionicons}
						style={[styles.inputDato, {
							borderColor: this.state.secondColor,
							height: responsiveHeight(9.5)
						}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.apellido_paterno = text)}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.apellidoMat.focus()}
					/>
					<Text style={[styles.widthall, {fontWeight: '800', marginTop: 10}]}>
						Ingrese el segundo apellido
					</Text>
					<Fumi
						ref='apellidoMat'
						label={'Apellido materno'}
						iconClass={Ionicons}
						style={[styles.inputDato, {
							borderColor: this.state.secondColor,
							height: responsiveHeight(9.5)
						}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.apellido_materno = text)}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.email.focus()}
					/>
					<Text style={[styles.widthall, {fontWeight: '800', marginTop: 10}]}>
						Ingrese dirección de e-mail
					</Text>
					<Fumi
						ref='email'
						label={'Email'}
						iconClass={Entypo}
						style={[styles.inputDato, {
							borderColor: this.state.secondColor,
							height: responsiveHeight(9.5)
						}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'email'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.email = text)}
						keyboardType='email-address'
						returnKeyType='next'
						autoCapitalize='none'
						autoCorrect={false}
					/>
					<Text style={[styles.widthall, {fontWeight: '800', marginTop: 10}]}>
						Defina estatus del maestro
					</Text>
					<ModalSelector
						data={data}
						selectStyle={[
							styles.inputPicker,
							{borderColor: this.state.secondColor, marginTop: 3}
						]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor}}
						initValue='Seleccione un estatus'
						onChange={option => this.onChange(option)}
					/>
					<TouchableOpacity
						style={[
							styles.bigButton,
							{backgroundColor: this.state.secondColor}
						]}
						onPress={() => this.captMaestro()}
					>
						<Text style={styles.textButton}>Capturar maestro</Text>
					</TouchableOpacity>
				</ScrollView>

			</KeyboardAvoidingView>
		);
	}
}
