import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	KeyboardAvoidingView,
	TextInput,
	TouchableHighlight
} from 'react-native';
import {
	responsiveHeight,
	responsiveFontSize
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Entypo from 'react-native-vector-icons/Entypo';
import ModalSelector from 'react-native-modal-selector';
import MultiBotonRow from '../Globales/MultiBotonRow';
import MatCapt from './MatCapt';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class captMateria extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			nombMat: '',
			grados: [],
			friends: [],
			hrsClas: '',
			selectedIndexGrados: -1,
			grado: '',
			botonSelected: '',
			indexSelected: 0,
			aux: 0
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		this.getGrados();
	}

	async requestMultipart() {
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				nombre: this.state.nombMat,
				grado: this.state.grado,
				horas: this.state.hrsClas
			})
		);
		await fetch(this.state.uri + '/api/post/materias', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al guardar materia',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Materias)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('Materia guardada', 'Se ha guardado la materia', [
				{
					text: 'Entendido',
					onPress: () => [
						Actions.refresh({key: Math.random()})
					]
				}
			]);
		}
		this.setState({aux: 0});
	}


	async saveMateria() {
		if (this.state.nombMat === '') {
			Alert.alert('¡Ups!', 'No ha puesto el nombre a la materia', [
				{text: 'Entendido'}
			]);
		} else if (this.state.grado === '') {
			Alert.alert('¡Ups!', 'No ha seleccionado un grado', [
				{text: 'Entendido'}
			]);
		} else if (this.state.hrsClas === '') {
			Alert.alert('¡Ups!', 'No ha establecido las horas por semana', [
				{text: 'Entendido'}
			]);
		} else {
			Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
				{
					text: 'Si',
					onPress: () => [this.requestMultipart()]
				},
				{text: 'No'}
			]);
		}
	}

	async getGrados() {
		let gradpicker = await fetch(this.state.uri + '/api/get/grados');
		let gradospicker = await gradpicker.json();
		await this.setState({grados: gradospicker});
	}

	async onChangeText(text) {
		await this.setState({
			nombMat: text
		});
	}

	async onChange(option) {
		await this.setState({
			grado: option
		});
	}

	async onChange1(text) {
		await this.setState({
			hrsClas: text
		});
	}


	captMate() {
		let index = 0;
		const dataH = [
			{key: index++, label: '1 '},
			{key: index++, label: '2 '},
			{key: index++, label: '3 '},
			{key: index++, label: '4 '},
			{key: index++, label: '5 '},
			{key: index++, label: '6 '},
			{key: index++, label: '7 '},
			{key: index++, label: '8 '},
			{key: index++, label: '9'},
			{key: index++, label: '10 '}
		];
		let data = this.state.grados.map((item, i) => {
			return {
				key: i,
				label: item.grado
			};
		});
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Captura de materia y grado
				</Text>
				<Text
					style={[
						styles.widthall,
						styles.textInfo,
						styles.textW,
						{textAlign: 'left', marginTop: 8}
					]}>
					Ingrese el nombre de la materia:
				</Text>
				<View
					style={[
						styles.widthall,
						{borderBottomWidth: 1, borderColor: this.state.fourthColor}
					]}>
					<TextInput
						keyboardType='default'
						maxLength={254}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						onChangeText={text => this.onChangeText(text)}
						placeholder={'Nombre de la materia'}
						style={[
							styles.inputPicker,
							{
								borderColor: this.state.secondColor,
								paddingHorizontal: 10,
								marginTop: 3,
								marginBottom: 5
							}
						]}
					/>
					<Text
						style={[
							styles.widthall,
							styles.textInfo,
							styles.textW,
							{textAlign: 'left', marginTop: 10}
						]}>
						Asigne grado a la materia:
					</Text>
					<ModalSelector
						data={data}
						selectStyle={[
							styles.inputPicker,
							{
								borderColor: this.state.secondColor,
								marginTop: 3,
								marginBottom: 5
							}
						]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor}}
						initValue='Grados'
						onChange={option => this.onChange(option.label)}
					/>
					<Text
						style={[
							styles.widthall,
							styles.textInfo,
							styles.textW,
							{textAlign: 'left', marginTop: 10}
						]}>
						Asigne horas por semana a la materia:
					</Text>
					<TextInput
						keyboardType='numeric'
						maxLength={254}
						onChangeText={text => this.onChange1(text)}
						placeholder={'horas de la materia'}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						style={[
							styles.inputPicker,
							{
								borderColor: this.state.secondColor,
								paddingHorizontal: 10,
								marginTop: 3,
								textAlign: 'center'
							}
						]}
					/>
					<View
						style={[
							styles.widthall,
							{alignItems: 'center', marginVertical:10}
						]}>
						<Entypo
							name='circle-with-plus' size={40} color='#43d551' onPress={() => this.saveMateria()}
						/>
					</View>
				</View>
			</View>
		);
	}

	async botonSelected(indexSelected, itemSelected) {
		await
			this.setState({
				botonSelected: itemSelected,
				indexSelected: indexSelected
			});
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<MultiBotonRow
					itemBtns={['Capturar materia', 'Materias capturadas']}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelected(indexBtn, itemBtn)
					}
					cantidad={2}
				/>
				{this.state.indexSelected === 0 ? this.captMate() : (<MatCapt/>)}
			</View>
		);
	}
}
