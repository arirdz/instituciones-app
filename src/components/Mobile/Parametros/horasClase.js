import React from 'react';
import {Alert, AsyncStorage, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import ModalSelector from 'react-native-modal-selector';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Entypo from 'react-native-vector-icons/Entypo';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modal';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class horasClase extends React.Component {
    _handleDatePickedF1 = date => {
        let fechaF = moment(date, 'HH:mm:ss').format('HH:mm');
        let a = this.state.friends.length - 1;
        if (this.state.friends[a].fin <= fechaF) {
            this.state.fechaF1 = fechaF;
            this.setState({aux: 0});
            this._hideDateTimePickerF1();
        }
        else {
            Alert.alert('Error al seleccionar la hora',
                'No se puede seleccionar esta hora',
                [{
                    text: 'Entendido',
                    onPress: () => Actions.drawer()
                }]);
        }
    };
    _handleDatePickedF2 = date => {
        let fecha2F = moment(date, 'HH:mm:ss').format('HH:mm');
        this.setState({fechaF2: fecha2F});
        this._hideDateTimePickerF2();
    };

    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            isDateTimePickerVisibleF1: false,
            isDateTimePickerVisibleF2: false,
            fechaF1: '',
            selected: [],
            tipo: [],
            fechaF2: '',
            solicitudes: [],
            friends: [],
            isModalHra: [],
            elRequest: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getHoras();
    }

    async borrarHoras(i) {
        let formData = new FormData();
        formData.append(
            'update',
            JSON.stringify({
                id: this.state.friends[i].id,
                horas: this.state.friends[i].inicio +
                    ' - ' +
                    this.state.friends[i].fin,
                tipo_hora: this.state.friends[i].tipo_hora,
                inicio: this.state.friends[i].inicio,
                fin: this.state.friends[i].inicio
            })
        );
        await fetch(this.state.uri + '/api/borrar/horas', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            },
            body: formData
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({elRequest: responseJson});
            });
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('Error al tratar de borrar horas',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Materias)',
                [{
                    text: 'Entendido',
                    onPress: () => Actions.drawer()
                }]);
        }
    }

    async updateHorario(i) {
        this.state.friends[i].tipo_hora = this.state.tipo;
        this.state.friends[i].inicio = this.state.fechaF1;
        this.state.friends[i].fin = this.state.fechaF2;
        let formData = new FormData();
        formData.append(
            'update',
            JSON.stringify({
                id: this.state.friends[i].id,
                horas: this.state.friends[i].inicio +
                    ' - ' +
                    this.state.friends[i].fin,
                tipo_hora: this.state.friends[i].tipo_hora,
                inicio: this.state.friends[i].inicio,
                fin: this.state.friends[i].fin
            })
        );
        await fetch(this.state.uri + '/api/update/horas', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            },
            body: formData
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({elRequest: responseJson});
            });
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('Error al actualizar la hora',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' al tratar de actualizar las horas, si el error continua pónganse en contacto con soporte'
                +
                '(Materias)',
                [{
                    text: 'Entendido',
                    onPress: () => Actions.drawer()
                }]);
        } else {
            Actions.refresh({key: Math.random()})
        }
        await this.closeModal(i)
    }

    async requestMultipart() {
        let formData = new FormData();
        formData.append(
            'new',
            JSON.stringify({
                horas: this.state.fechaF1 +
                    ' - ' +
                    this.state.fechaF2,
                tipo_hora: this.state.tipo,
                inicio: this.state.fechaF1,
                fin: this.state.fechaF2
            })
        );
        await fetch(this.state.uri + '/api/post/horario', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            },
            body: formData
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({elRequest: responseJson});
            });
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('¡Ups!',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Materias)',
                [{
                    text: 'Entendido', onPress: () => Actions.drawer()
                }]);
        } else {
            Actions.refresh({key: Math.random()});
        }
    }

    async saveHorario(i) {
        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
            {
                text: 'Si',
                onPress: () => [this.updateHorario(i)]
            },
            {text: 'No'}


        ]);
    }

    async _hideDateTimePickerF1() {
        this.setState({isDateTimePickerVisibleF1: false});
    }

    async _hideDateTimePickerF2() {
        this.setState({isDateTimePickerVisibleF2: false});
    }

    async getHoras() {
        let switchHora = await fetch(this.state.uri + '/api/get/horarios',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            });
        let switchHoras = await switchHora.json();
        await this.setState({friends: switchHoras});
        this.state.friends.forEach(() => {
            this.state.isModalHra.push(false);
        });
        await this.setState({aux: 0});
    }

    async Agregar() {
        this.state.friends.push({
            tipo_hora: this.state.tipo,
            inicio: this.state.fechaF1,
            fin: this.state.fechaF2
        });
        this.setState({aux: 0});
    }

	async alertBorrar(index) {
		Alert.alert('¡Atención!',
			'Si borra esta hora afectará al funcionamiento de todo el sistema y tendrá que reconfigurar los horarios de clase y disponibilidad de citas para todos los usuarios.\n\n¿Está seguro que desea borrar esta hora?', [
				{
					text: 'Sí',
					onPress: () => this.borrar(index)
				},
				{text: 'No'}
			]);
	}

    async borrar(index) {
        await this.borrarHoras(index);
        this.state.friends.splice(index, 1);
        await this.setState({aux: 0});
    }

    async openModal(i) {
        this.state.isModalHra[i] = true;
        this.state.fechaF1 = this.state.friends[i].inicio;
        this.state.fechaF2 = this.state.friends[i].fin;
        this.state.tipo = this.state.friends[i].tipo_hora;
        await this.setState({aux: 0});
    }

    async closeModal(i) {
        this.state.isModalHra[i] = false;
        await this.setState({aux: 0});
    }

    rendHora(_, key) {
        let tipo = '';
        if (this.state.tipo === '1') {
            tipo = 'Clase';
        } else {
            tipo = 'Receso';
        }
        let index = 0;
        const dataF = [
            {key: index++, label: 'Clase', tipo_hora: '1'},
            {key: index++, label: 'Receso', tipo_hora: '2'}
        ];
        return (
            <View
                key={key + 'rndHora'}
                style={[
                    styles.rowsCalif,
                    styles.cardHoras,
                    {backgroundColor: this.state.fourthColor}
                ]}
            >
                <Modal
                    isVisible={this.state.isModalHra[key]}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text
                            style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                        >
                            Edita la hora {key + 1}
                        </Text>
                        <View style={[styles.row_v3, styles.modalWidth]}>
                            <View style={{alignItems: 'center', marginVertical: 8}}>
                                <Text>Inicia</Text>
                                <TouchableOpacity
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        {
                                            borderColor: this.state.secondColor,
                                            width: responsiveWidth(40),
                                            height: responsiveHeight(5)
                                        }
                                    ]}
                                    onPress={() => this.setState({isDateTimePickerVisibleF1: true})}
                                >
                                    {this.state.fechaF1 === undefined ? (
                                        <Text>Horas</Text>
                                    ) : (
                                        <Text
                                            style={styles.textW}>{moment(this.state.fechaF1, 'HH:mm:ss').format('HH:mm')} hrs.</Text>
                                    )}
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems: 'center', marginVertical: 8}}>
                                <Text>Finaliza</Text>
                                <TouchableOpacity
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        {
                                            borderColor: this.state.secondColor,
                                            width: responsiveWidth(40),
                                            height: responsiveHeight(5)
                                        }
                                    ]}
                                    onPress={() => this.setState({isDateTimePickerVisibleF2: true})}
                                >
                                    {this.state.fechaF2 === undefined ? (
                                        <Text>Horas</Text>
                                    ) : (
                                        <Text
                                            style={styles.textW}>{moment(this.state.fechaF2, 'HH:mm:ss').format('HH:mm')} hrs.</Text>
                                    )}
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{alignItems: 'center', marginVertical: 8}}>
                            <Text>Clase/Receso</Text>
                            <ModalSelector
                                data={dataF}
                                selectStyle={[
                                    styles.inputPicker,
                                    styles.modalWidth,
                                    {borderColor: this.state.secondColor}
                                ]}
                                selectTextStyle={[
                                    styles.textW,
                                    {fontSize: responsiveFontSize(1.9)}
                                ]}
                                cancelText='Cancelar'
                                optionTextStyle={{color: this.state.thirdColor}}
                                initValue={tipo}
                                onChange={option => this.onChangeF(option)}
                            />
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisibleF1}
                            onConfirm={this._handleDatePickedF1}
                            mode={'time'}
                            is24Hour={true}
                            onCancel={() => this.setState({isDateTimePickerVisibleF1: false})}
                            titleIOS={'Seleccione una hora'}
                            confirmTextIOS={'Seleccionar'}
                            cancelTextIOS={'Cancelar'}
                        />
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisibleF2}
                            onConfirm={this._handleDatePickedF2}
                            mode={'time'}
                            is24Hour={true}
                            onCancel={() => this.setState({isDateTimePickerVisibleF2: false})}
                            titleIOS={'Seleccione una hora'}
                            confirmTextIOS={'Seleccionar'}
                            cancelTextIOS={'Cancelar'}
                        />
                        <View style={[styles.modalWidth, styles.row]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => [(this.state.isModalHra[key] = false), this.setState({aux: 0})]}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.saveHorario(key)}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <View
                    style={[
                        styles.btn5,
                        {justifyContent: 'center', alignItems: 'center'}
                    ]}
                >
                    <Text
                        style={[
                            styles.main_title,
                            styles.btn5,
                            {
                                color: this.state.thirdColor,
                                paddingTop: 0,
                                marginBottom: 15
                            }
                        ]}
                    >
                        Hora {key + 1}
                    </Text>
                </View>
                <View style={{alignItems: 'center', marginVertical: 5}}>
                    <Text>Tiempo de duración:</Text>
                    <View style={styles.btn2}>
                        <Text style={{textAlign: 'center', fontWeight: '800'}}>
                            {moment(_.inicio, 'HH:mm:ss').format('HH:mm')} hrs.
                            - {moment(_.fin, 'HH:mm:ss').format('HH:mm')} hrs.
                        </Text>
                    </View>
                    <Text style={{marginTop: 3}}>Tipo de hora:</Text>
                    <View style={styles.btn4}>
                        <Text style={{textAlign: 'center', fontWeight: '800'}}>
                            {_.tipo_hora === '1' ? 'Clase' : 'Receso'}
                        </Text>
                    </View>
                </View>
                <View
                    style={[styles.btn6, styles.row]}
                >
                    <MaterialIcons name='edit' size={25} color='grey' onPress={() => this.openModal(key)}/>
                    <Entypo name='circle-with-minus' size={25} color='red' onPress={() => this.alertBorrar(key)}/>
                </View>

            </View>
        );
    }

    rendHoras() {
        let btnHoras = [];
        this.state.friends.forEach((_, key) => {
            btnHoras.push(this.rendHora(_, key));
        });
        return btnHoras;
    }

	async guardarHoras() {
		if (this.state.fechaF1 === '') {
			Alert.alert('¡Ups!', 'No ha seleccionado una hora de inicio', [
				{text: 'Entendido'}
			]);
		} else {
			if (this.state.fechaF2 === '') {
				Alert.alert('¡Ups!', 'No ha seleccionado una hora de finalizado', [
					{text: 'Entendido'}
				]);
			} else {
				if (this.state.tipo === '') {
					Alert.alert('¡Ups!', 'No ha establecido el tipo de clase', [
						{text: 'Entendido'}
					]);
				} else {
					Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
						{
							text: 'Sí',
							onPress: () => [this.Agregar(), this.requestMultipart()]
						},
						{text: 'No'}
					]);
				}
			}
		}
	}

    async onChangeF(option) {
        await this.setState({
            selected: option.label,
            tipo: option.tipo_hora
        });
    }

    render() {
        let index = 0;
        const dataF = [
            {key: index++, label: 'Clase', tipo_hora: '1'},
            {key: index++, label: 'Receso', tipo_hora: '2'}
        ];
        return (
            <View style={styles.container}>
                <Text style={[styles.main_titleM, {color: this.state.thirdColor}]}>
                    Defina el horario de cada clase o receso
                </Text>
                <View
                    style={[
                        styles.row,
                        styles.widthall,
                        {paddingTop: 3}
                    ]}
                >
                    <View style={{alignItems: 'center'}}>
                        <Text style={{fontWeight: '600'}}>Inicia</Text>
                        <TouchableOpacity
                            style={[
                                styles.pickerParam1,
                                styles.btn3,
                                {borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.setState({isDateTimePickerVisibleF1: true})}
                        >
                            {this.state.fechaF1 === '' ? (
                                <Text>Horas</Text>
                            ) : (
                                <Text
                                    style={styles.textW}>{moment(this.state.fechaF1, 'HH:mm:ss').format('HH:mm')} hrs.</Text>
                            )}
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <Text style={{fontWeight: '600'}}>Finaliza</Text>
                        <TouchableOpacity
                            style={[
                                styles.pickerParam1,
                                styles.btn3,
                                {borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.setState({isDateTimePickerVisibleF2: true})}
                        >
                            {this.state.fechaF2 === '' ? (
                                <Text>Horas</Text>
                            ) : (
                                <Text
                                    style={styles.textW}>{moment(this.state.fechaF2, 'HH:mm:ss').format('HH:mm')} hrs.</Text>
                            )}
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <Text style={{fontWeight: '600'}}>Clase/Receso</Text>
                        <ModalSelector
                            data={dataF}
                            selectStyle={[
                                styles.pickerParam1,
                                styles.btn3,
                                {borderColor: this.state.secondColor}
                            ]}
                            selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
                            cancelText='Cancelar'
                            optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
                            initValue='Tipo'
                            onChange={option => this.onChangeF(option)}
                        />
                    </View>
                </View>
                <View style={{marginVertical: 10}}>
                    <Entypo name='circle-with-plus' size={35} color='#43d551' onPress={() => this.guardarHoras()}/>
                </View>
                <View
                    style={[styles.widthall, {borderWidth: .5, borderColor: 'lightgrey'}]}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{...ifIphoneX({marginBottom: 35, marginTop: 5}, {marginBottom: 20, marginTop: 5})}}
                >
                    {this.rendHoras()}
                </ScrollView>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisibleF1}
                    onConfirm={this._handleDatePickedF1}
                    mode={'time'}
                    is24Hour={true}
                    onCancel={() => this.setState({isDateTimePickerVisibleF1: false})}
                    titleIOS={'Seleccione una hora'}
                    confirmTextIOS={'Seleccionar'}
                    cancelTextIOS={'Cancelar'}
                />
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisibleF2}
                    onConfirm={this._handleDatePickedF2}
                    mode={'time'}
                    is24Hour={true}
                    onCancel={() => this.setState({isDateTimePickerVisibleF2: false})}
                    titleIOS={'Seleccione una hora'}
                    confirmTextIOS={'Seleccionar'}
                    cancelTextIOS={'Cancelar'}
                />
            </View>
        );
    }
}
