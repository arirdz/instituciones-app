import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	TouchableOpacity,
	ScrollView
} from 'react-native';
import {
	responsiveHeight
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Modal from 'react-native-modal';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Spinner from 'react-native-loading-spinner-overlay';

export default class EditarList extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			data: [],
			lasListas: [],
			editar: '',
			isModalalumno: [],
			elIndice: 0,
			aux: 0,
			indice: [],
			alumnos: []
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			aux: 0
		});
		await this.getListasVar();
	}

	async borrarList(index) {
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				id_lista: this.state.lasListas[index].id,
				id: this.state.lasListas[index].id
			})
		);
		await fetch(this.state.uri + '/api/post/delete/lista', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al borrar lista',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' al tratar de borrar la lista si el error continua pónganse en contacto con soporte'
				+
				'(listas variables)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('Lista borrada', 'Usted a borrado una lista', [
				{
					text: 'Entendido',
					onPress: () => this.borrarLista(index)
				}
			]);
		}

	}

	async saveMateria(index) {
		Alert.alert('Borrar lista', 'Está a punto de eliminar una lista\n¿Seguro que desea continuar?', [
			{text: 'Sí',onPress: () => this.borrarList(index)},{text: 'No'}
		]);
		this.setState({aux: 0});
	}

	//++++++++++++++++++++++++++++++++++++++> Alumnos

	renderAlumno(i, j) {
		let a = this.state.lasListas[i].alumnos - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (j !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		return (
			<View key={j + 'alumnos'} style={tabRow}>
				{this.state.isModalalumno[i] === true ? (
					<View style={[styles.campoTablaG, styles.row, styles.modalWidth, {
						alignItems: 'center',
						paddingHorizontal: 3
					}]}>
						<Text style={[styles.btn1_2, {marginRight: 25}]}>
							{this.state.lasListas[i].alumnos[j].name}
						</Text>
						<Text style={{marginRight: 25}}>
							{this.state.lasListas[i].alumnos[j].grado}
						</Text>
						<Text style={{marginRight: 25}}>
							{this.state.lasListas[i].alumnos[j].grupo}
						</Text>
					</View>
				) : null}
			</View>
		);
	}


	renderAlumnos(i) {
		let btnAlumnos = [];
		let alumnosBtn = this.state.lasListas[i].alumnos;
		for (let j = 0; j < alumnosBtn.length; j++) {
			btnAlumnos.push(this.renderAlumno(i, j));
		}
		return btnAlumnos;
	}

	async openModal(i) {
		this.state.isModalalumno[i] = true;
		await this.setState({aux: 0});
	}

	async closeModal(i) {
		this.state.isModalalumno[i] = false;
		await this.setState({aux: 0});
	}

	//+++++++++++++++++++++++++++++++++++++> Listas cuadro
	async laEdicion(i) {
		this.state.editar = this.state.lasListas[i];
		Actions.editarListPt2({
			edicion: this.state.editar
		});
	}

	async getListasVar() {
		let lista = await fetch(this.state.uri + '/api/get/lista', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let listas = await lista.json();
		await this.setState({lasListas: listas});
		this.state.lasListas.forEach(() => {
			this.state.isModalalumno.push(false)
		});
	}

	async borrarLista(i) {
		this.state.lasListas.splice(i, 1);
		await this.setState({aux: 0});
	}

	rendListas() {
		let btnLista = [];
		for (let i = 0; i < this.state.lasListas.length; i++) {
			btnLista.push(
				<View
					key={i + 'rndLista'}
					style={[
						styles.widthall,
						styles.row,
						{
							marginVertical: 5,
							borderRadius: 6,
							padding: 7,
							backgroundColor: this.state.fourthColor
						}
					]}
				>
					<View>
						<View style={[styles.row, styles.btn1_2]}>
							<Text style={[styles.textW]}>Nombre del taller:</Text>
							<Text style={styles.btn3}>
								{'  '}
								{this.state.lasListas[i].taller}
							</Text>
						</View>
						<View style={[styles.row, styles.btn4]}>
							<Text style={[styles.textW, {marginVertical: 3}]}>Integrates:</Text>
							<Text>
								{'  '}
								{this.state.lasListas[i].alumnos.length.toString()}
							</Text>
						</View>

					</View>
					<View style={[styles.row, styles.btn_2]}>
						<MaterialIcons
							name='view-list' size={25} color='grey' onPress={() => this.openModal(i)}
						/>
						<MaterialIcons name='edit' size={22} color='grey' onPress={() => this.laEdicion(i)}/>
						<Entypo name='circle-with-minus' size={22} color='red' onPress={() => this.saveMateria(i)}/>
					</View>
					<Modal
						isVisible={this.state.isModalalumno[i]}
						backdropOpacity={0.5}
						animationIn={'bounceIn'}
						animationOut={'bounceOut'}
						animationInTiming={1000}
						animationOutTiming={1000}
					>
						<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>

							<Text style={[styles.main_title, styles.modalWidth]}>
								Lista de alumnos
							</Text>

							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10, height:responsiveHeight(60)}
								]}
							>
								<ScrollView showsVerticalScrollIndicator={false}>
									{this.renderAlumnos(i)}
								</ScrollView>
							</View>

							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.closeModal(i)}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
							</TouchableOpacity>
						</View>
					</Modal>
				</View>
			);
		}
		return btnLista;
	}

	render() {
		const varList = this.rendListas();
		return (
			<View style={styles.container}>

				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione la lista a editar.
				</Text>
				<ScrollView showsVerticalScrollIndicator={false} style={{marginTop: 5}}>
					{varList}
				</ScrollView>
			</View>
		);
	}
}
