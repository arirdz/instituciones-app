import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, StatusBar, Text, TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ModalSelector from 'react-native-modal-selector';
import MultiBotonRow from '../Globales/MultiBotonRow';
import AltaMaestro from './AltaMaestro';
import Modal from 'react-native-modal';

export default class captMaestro extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			isMateria: '',
			laRelacion: [],
			elTotal: [],
			isModalAddMat: [],
			isModalMat: [],
			isModalName: [],
			lasHoras: [],
			elActivo: '1',
			losGrupos: [],
			grupos: [],
			activoMaest: [],
			materias: [],
			laMateria: '',
			hrasMateria: [],
			elidMateria: '',
			elIdMaestro: [],
			elIdMaestro2: [],
			checkBoxBtns: [],
			checkBoxBtnsIndex: [],
			maestros: [],
			maestros2: [],
			grupillo: [],
			elMaestro: [],
			maestroMt: [],
			botonSelected: '',
			indexSelected: 0,
			elRequest: [],
			selectedIndexGrupos: -1,
			elGrupo: '',
			aux: 0
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getRelacionMaterias();
		await this.getMaterias();
		await this.getGrupos();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async updateProf(i) {
		Alert.alert('¡Alerta!', '¿Está seguro que desea editar al maestro: ' + this.state.laRelacion[i].maestro + '?', [{
			text: 'Sí', onPress: () => this.updateMaestro(i)
		}, {
			text: 'No'
		}]);
	}

	async updateMaestro(i) {
		this.state.laRelacion[i].maestro = this.state.elMaestro[i];
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: this.state.laRelacion[i].id_maestro,
			name: this.state.laRelacion[i].maestro,
			activo: this.state.laRelacion[i].activo
		}));
		await fetch(this.state.uri + '/api/maestro/update', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al editar maestro', 'Ha ocurrido un error ' + this.state.elRequest.error.status_code + ' si el error continua pónganse en contacto con soporte' + '(Editar maestros)', [{
				text: 'Entendido', onPress: () => Actions.drawer()
			}]);
		} else {
			if (this.state.laRelacion[i].activo === '0') {
				await this.borrarRelacion2(i);
			}
			Alert.alert('Datos actualizados', 'Se han actualizado los datos del maestro', [{
				text: 'Entendido', onPress: () => [Actions.refresh({key: Math.random()}), this.closeModalN(i)]
			}]);
		}
	}

	async deleteMaestro(i) {
		this.state.laRelacion[i].maestro = this.state.elMaestro[i];
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			id: this.state.laRelacion[i].id_maestro,
			user_id: this.state.laRelacion[i].id_maestro
		}));
		await fetch(this.state.uri + '/api/delete/maestro', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});

			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al borrar maestro', 'Ha ocurrido un error ' + this.state.elRequest.error.status_code +
				' si el error continua pónganse en contacto con soporte' + '(Borrar maestros)', [{
				text: 'Entendido', onPress: () => Actions.drawer()
			}]);
		} else {
			await this.borrarRelacion2(i);
			Alert.alert('Maestro borrado', 'Se ha borrado el maestro de la institución', [{
				text: 'Entendido', onPress: () => [Actions.refresh({key: Math.random()}), this.closeModalN(i)]
			}]);
		}
	}

	async borrarRelacion2(i) {
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			id_maestro: this.state.laRelacion[i].id_maestro
		}));
		await fetch(this.state.uri + '/api/borrar/relacion/materias/maestro', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error',
				'Ha ocurrido un error ' + this.state.elRequest.error.status_code
				+ '' +
				' si el error continua pónganse en contacto con soporte'
				+
				'(Editar maestros)',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		}
	}

	async arregloEquis() {
		for (let i = 0; i < this.state.laRelacion.length; i++) {
			await this.onChangeCriValue(i);
		}
		await this.setState({aux: 0});
	}

	async borrarRelacion() {
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			id_maestro: 0,
			id_materia: 0,
			grupo: 0
		}));
		await fetch(this.state.uri + '/api/borrar/relacion/materias', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error', 'Ha ocurrido un error ' + this.state.elRequest.error.status_code + ' si el error continua pónganse en contacto con soporte' + '(Relación materia maestro)', [{
				text: 'Entendido', onPress: () => Actions.drawer()
			}]);
		} else {
			await this.requestMultipart();
		}
	}

	unArreglo2() {
		let btnArreglo = [];
		for (let i = 0; i < this.state.laRelacion.length; i++) {
			for (let j = 0; j < this.state.laRelacion[i].gpoYmat.length; j++) {
				for (let h = 0; h < this.state.laRelacion[i].gpoYmat[j].grupos.length; h++) {
					btnArreglo.push({
						id_materia: this.state.laRelacion[i].gpoYmat[j].id_materia,
						id_maestro: this.state.laRelacion[i].id_maestro,
						grupo: this.state.laRelacion[i].gpoYmat[j].grupos[h]
					});
				}
			}
		}
		return btnArreglo;
	}

	async requestMultipart() {
		const arreglo = this.unArreglo2();
		let formData = new FormData();
		formData.append('new', JSON.stringify(arreglo));
		await fetch(this.state.uri + '/api/post/relacion/materias', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al guardar la relación de maestros', 'Ha ocurrido un error ' + this.state.elRequest.error.status_code + ' si el error continua pónganse en contacto con soporte' + '(relación materia maestro)', [{
				text: 'Entendido', onPress: () => Actions.drawer()
			}]);
		} else {
			Alert.alert('Relación de maestros guardada', 'Se ha capturado la relación con éxito', [{
				text: 'Entendido', onPress: () => Actions.menuParam()
			}]);
		}
	}

	async saveMateria() {
		Alert.alert('Guardar relación', '¿Seguro que desea continuar?', [{
			text: 'Sí', onPress: () => [this.borrarRelacion()]
		}, {text: 'No'}]);
	}

	async getRelacionMaterias() {
		let relacion = await fetch(this.state.uri + '/api/get/maestros/relacion/materia/' + this.state.elActivo, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let relaciones = await relacion.json();
		await this.setState({laRelacion: relaciones});
		await this.unArreglo();
	}


	async getMaterias() {
		let materia = await fetch(this.state.uri + '/api/all/materias', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let materias = await materia.json();
		await this.setState({materias: materias});
	}

	async unArreglo() {
		for (let i = 0; i < this.state.elMaestro.length; i++) {
			this.state.laRelacion.push({
				maestro: this.state.elMaestro[i],
				id_maestro: this.state.elIdMaestro[i],
				activo: this.state.laRelacion[i].activo,
				gpoYmat: []
			});
		}
		await this.setState({aux: 0});
		await this.arregloEquis();
	}

	async addMatYgpo(i) {
		let grupillo = this.state.checkBoxBtns.toString();
		let patron = /,/gi;
		let esta = grupillo.replace(patron, '');
		let array = Array.from(esta);
		this.state.grupillo = array;
		this.state.laRelacion[i].gpoYmat.push({
			horas: this.state.hrasMateria,
			materia: this.state.isMateria,
			id_materia: this.state.elidMateria,
			grupos: this.state.grupillo
		});
		await this.setState({aux: 0});
		await this.onChangeCriValue(i);
		await this.setState({
			checkBoxBtns: [], checkBoxBtnsIndex: []
		});
	}


	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-Materias
	async alertBorarMat(i, index) {
		Alert.alert('¡Alerta!', '¿Está seguro de que desea borrar esta materia?', [
			{
				text: 'Sí', onPress: () => this.borrarMat(i, index)
			},
			{text: 'No'}
		]);
	}

	async borrarMat(i, index) {
		await this.state.laRelacion[i].gpoYmat.splice(index, 1);
		await this.setState({aux: 0});
		await this.getMaterias();
		await this.onChangeCriValue(i);
	}


	async closeModalMat(i, j) {
		this.state.laRelacion[i].gpoYmat[j].grupos = this.state.checkBoxBtns;
		await this.setState({checkBoxBtnsIndex: [], checkBoxBtns: []});
		this.state.isModalMat[i + '' + j] = false;
		await this.onChangeCriValue(i);
	}

	async openModalMat(i, j) {
		await this.getGrupos();
		for (let h = 0; h < this.state.laRelacion[i].gpoYmat[j].grupos.length; h++) {
			for (let k = 0; k < this.state.grupos.length; k++) {
				if (this.state.laRelacion[i].gpoYmat[j].grupos[h] === this.state.grupos[k].grupo) {
					this.state.checkBoxBtns = this.state.laRelacion[i].gpoYmat[j].grupos;
					this.state.checkBoxBtnsIndex[k] = 1;
				}
			}
		}
		await this.setState({aux: 0});
		this.state.isModalMat[i + '' + j] = true;
		await this.setState({aux: 0});
	}

	rndMat(item, i, index) {
		return (<View
			key={index + 'rndMat'}
			style={[styles.row, {borderBottomWidth: .5, borderColor: 'lightgrey', marginBottom: 5}]}
		>
			<Modal
				isVisible={this.state.isModalMat[i + '' + index]}
				backdropOpacity={0.8}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}
			>
				<View
					style={[styles.container, {borderRadius: 6, flex: 0}]}
				>
					<Text
						style={[styles.main_title, styles.modalWidth, {
							marginTop: 15,
							textAlign: 'center',
							color: this.state.thirdColor,
							fontSize: responsiveFontSize(1.9)
						}]}
					>
						{this.state.laRelacion[i].gpoYmat[index].materia}
					</Text>
					<Text style={[styles.textW, styles.modalWidth, {marginTop: 10, textAlign: 'center'}]}>
						Grupos actualmente asignados
					</Text>
					<View style={[styles.modalWidth, {
						height: responsiveHeight(5), marginTop: 5, alignItems: 'center'
					}]}>
						<View style={styles.buttonsRow}>{this.renderGrupos(i)}</View>
					</View>
					<View style={[styles.modalWidth, styles.row]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => [(this.state.isModalMat[i + '' + index] = false), this.setState({aux: 0})]}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.closeModalMat(i, index)}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
						</TouchableOpacity>
					</View>

				</View>
			</Modal>
			<View>
				<TouchableOpacity
					style={[styles.inputPicker, styles.btn2, styles.row, {
						padding: 3, marginTop: 4
					}]}
					onPress={() => this.alertBorarMat(i, index)}
				>
					<Text numberOfLines={1} style={[styles.btn8, {fontSize: responsiveFontSize(1.4)}]}>
						{item.materia}
					</Text>

					<FontAwesome name='times-circle' size={14} color='black'/>
				</TouchableOpacity>
				<View
					style={[styles.row, styles.btn2, {
						height: responsiveHeight(4), padding: 1.5, marginTop: 0, justifyContent: 'flex-start'
					}]}
				>
					{this.rndGpos(i, index)}
				</View>
			</View>
			<MaterialIcons name='edit' size={22} color='grey' onPress={() => this.openModalMat(i, index)}/>
		</View>);
	}

	//-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*--* calculo de las horas
	async onChangeCriValue(index) {
		let a = 0;
		let d = 0;
		for (let i = 0; i < this.state.laRelacion[index].gpoYmat.length; i++) {
			let b = parseInt(this.state.laRelacion[index].gpoYmat[i].horas);
			let c = this.state.laRelacion[index].gpoYmat[i].grupos.length.toString();
			d = b * c;
			a = a + d;
		}
		this.state.elTotal[index] = a;
		await this.setState({aux: 0});
	}

	rndMats(i) {
		let btnMat = [];
		if (this.state.laRelacion[i].gpoYmat.length !== 0) {
			this.state.laRelacion[i].gpoYmat.forEach((item, index) => {
				btnMat.push(this.rndMat(item, i, index));
			});
		}
		return btnMat;
	}


	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- Grupos +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

	rndGpo(item, i, j, indx) {
		return (
			<View
				key={indx + 'rdnGpo'}
				style={[styles.inputPicker, styles.btn4_lT, {
					padding: 1.5,
					paddingHorizontal: 7,
					marginTop: 0,
					margin: 3
				}]}
			>
				<Text style={{fontSize: responsiveFontSize(1.4), fontWeight: '700'}}>
					{item}
				</Text>
			</View>
		);
	}

	rndGpos(i, j) {
		let btnGpo = [];
		if (this.state.laRelacion[i].gpoYmat[j].grupos.length !== 0) {
			this.state.laRelacion[i].gpoYmat[j].grupos.forEach((item, indx) => {
				btnGpo.push(this.rndGpo(item, i, j, indx));
			});
		}
		return btnGpo;
	}

	renderBtnGrupos(i, j, index) {
		let btnGpillo = [];
		if (this.state.laRelacion[i].gpoYmat.length !== index) {
			btnGpillo.push(this.rndGpos(i, j, index));
		}
		return btnGpillo;
	}

	///*-*-*-*-*-*-*-*-*-*-+-+-+-+-+-+-+-+-+-+-+- Maestros +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
	async borrar(i) {
		Alert.alert('¡Alerta!', '¿Está seguro de que desea eliminar a este maestro?', [
			{
				text: 'Sí', onPress: () => this.deleteMaestro(i)
			},
			{text: 'No'}
		]);
		await this.setState({aux: 0});
	}

	async openModalN(i) {
		this.state.isModalName[i] = true;
		this.state.elMaestro[i] = this.state.laRelacion[i].maestro;
		await this.setState({aux: 0});
	}

	async closeModalN(i) {
		this.state.isModalName[i] = false;
		await this.setState({aux: 0});
		await this.onChangeCriValue(i);
	}

	async openModal(i) {
		await this.funcionMat(i);
		await this.setState({
			checkBoxBtns: [],
			checkBoxBtnsIndex: []
		});
		this.state.isModalAddMat[i] = true;
		await this.setState({aux: 0});
	}

	async funcionMat(i) {
		for (let j = 0; j < this.state.laRelacion[i].gpoYmat.length; j++) {
			for (let h = 0; h < this.state.materias.length; h++) {
				if (this.state.laRelacion[i].gpoYmat[j].materia === this.state.materias[h].nombre) {
					this.state.materias.splice(h, 1);
				}
			}
		}
	}

	async closeModal(i) {
		this.state.isModalAddMat[i] = false;
		await this.setState({aux: 0});
		await this.getMaterias();
		await this.onChangeCriValue(i);
	}

	async onChange(option) {
		await this.getGrupos();
		await this.setState({isMateria: option.label, hrasMateria: option.hrs, elidMateria: option.id});
		await this.funcionGpos();
	}

	async funcionGpos() {
		let gruposA = this.state.losGrupos;
		for (let i = 0; i < this.state.laRelacion.length; i++) {
			for (let j = 0; j < this.state.laRelacion[i].gpoYmat.length; j++) {
				if (this.state.laRelacion[i].gpoYmat[j].id_materia.toString().indexOf(this.state.elidMateria.toString()) === 0) {
					let asd = this.state.laRelacion[i].gpoYmat[j].grupos.length;
					for (let s = 0; s < gruposA.length; s++) {
						for (let h = 0; h < asd; h++) {
							let gpo = this.state.laRelacion[i].gpoYmat[j].grupos[h];
							if (this.state.losGrupos[s] === gpo) {
								gruposA.splice(gruposA.indexOf(gpo), 1);
							}
						}
					}
				}
			}
		}
		this.state.losGrupos = gruposA;
		if (this.state.losGrupos.length === 0) {
			Alert.alert('¡Alerta!', 'Ya no quedan grupos disponibles para esta materia, selccione otra por favor', [
				{text: 'Entendido'}
			]);
		}
		await this.setState({aux: 0});
	}

	renderMat(i) {
		let index = 0;
		let activo = '';
		if (this.state.laRelacion[i].activo === '1') {
			activo = 'Activo';
		}
		if (this.state.laRelacion[i].activo === '0') {
			activo = 'Inactivo';
		}
		const data = [{key: index++, label: 'Activo', activo: '1'}, {key: index++, label: 'Inactivo', activo: '0'}];
		let dataMat = this.state.materias.map((item, i) => {
			return {
				key: i, id: item.id, label: item.nombre, hrs: item.horas
			};
		});
		return (<View
			key={i + 'renderMat'}
			style={styles.widthall}>
			<Modal
				isVisible={this.state.isModalName[i]}
				backdropOpacity={0.8}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}
			>
				<View
					style={[styles.container, {borderRadius: 6, flex: 0}]}
				>
					<Text
						style={[styles.main_title, styles.modalWidth, {
							marginTop: 15,
							color: this.state.thirdColor
						}]}
					>
						Edite el nombre del maestro
					</Text>
					<Text style={[styles.textW, styles.modalWidth, {marginTop: 10}]}>
						Nombre del maestro
					</Text>
					<TextInput
						keyboardType='default'
						maxLength={254}
						onChangeText={text => (this.state.elMaestro[i] = text)}
						placeholder={'ej.: Taller, eq. de football, Danza, etc...'}
						returnKeyType='next'
						defaultValue={this.state.elMaestro[i]}
						underlineColorAndroid='transparent'
						style={[
							styles.inputPicker, styles.modalWidth, {
								borderColor: this.state.secondColor, marginTop: 3, textAlign: 'center'
							}]}
					/>
					<Text style={[styles.textW, styles.modalWidth, {marginTop: 15}]}>
						Estatus del maestro
					</Text>
					<ModalSelector
						data={data}
						selectStyle={[styles.inputPicker, styles.modalWidth, {
							borderColor: this.state.secondColor,
							marginTop: 3
						}]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor}}
						sectionTextStyle={{fontSize: responsiveFontSize(1.5)}}
						initValue={activo}
						onChange={option => (this.state.laRelacion[i].activo = option.activo)}
					/>
					<View style={[styles.modalWidth, styles.row]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => [(this.state.isModalName[i] = false), this.setState({aux: 0})]}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.updateProf(i)}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
			<Modal
				isVisible={this.state.isModalAddMat[i]}
				backdropOpacity={0.8}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}
			>
				<View
					style={[styles.container, {borderRadius: 6, flex: 0}]}
				>
					<Text
						style={[styles.main_title, styles.modalWidth, {
							marginTop: 10,
							color: this.state.thirdColor
						}]}
					>
						Seleccione la materia y grupos
					</Text>
					<ModalSelector
						data={dataMat}
						selectStyle={[styles.inputPicker, styles.modalWidth, {
							borderColor: this.state.secondColor, marginTop: 3
						}]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
						selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
						initValue='Seleccione una materia '
						onChange={option => this.onChange(option, i)}
					/>
					<View style={{height: responsiveHeight(5), marginTop: 10}}>
						<View style={[styles.buttonsRow, {marginBottom: 0}]}>{this.renderGrupos(i)}</View>
					</View>
					<View style={[styles.modalWidth, styles.row, {marginTop: -10}]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.closeModal(i)}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => [this.addMatYgpo(i), this.closeModal(i)]}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Agregar</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
			<View
				style={[styles.widthall, styles.cardMat, {backgroundColor: this.state.fourthColor}]}
			>
				<View style={styles.row}>
					<View style={[styles.inputPicker, styles.btn5, {
						borderColor: this.state.secondColor, marginTop: 3, padding: 0, height: responsiveHeight(4)
					}]}>
						<Text>{activo}</Text>
					</View>

					<View style={[styles.btn2]}>
						<Text style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>
							Nombre completo
						</Text>
						<Text
							style={[styles.textW, {fontSize: responsiveFontSize(1.6), textAlign: 'center'}]}
						>
							{this.state.laRelacion[i].maestro}
						</Text>
					</View>
					<View style={[styles.btn6, styles.row]}>
						<MaterialCommunityIcons
							name='account-edit' size={25} color='grey' onPress={() => this.openModalN(i)}
						/>
						<Entypo name='circle-with-minus' size={22} color='red' onPress={() => this.borrar(i)}/>
					</View>
				</View>
				{this.state.laRelacion[i].activo === '1' ? (
					<View>
						<View style={[{borderBottomWidth: .5, borderColor: 'grey', marginTop: 5}]}>
							<Text>Materia(s) y grupo(s) agregado(s):</Text>
						</View>
						<View style={{margin: 5}}>{this.rndMats(i)}</View>
						<View style={{borderWidth: .5, borderColor: 'grey'}}/>
						<View style={[styles.row, {marginTop: 3}]}>
							<TouchableOpacity
								style={[styles.inputPicker, styles.btn3, {
									backgroundColor: this.state.thirdColor, padding: 4, marginTop: 3
								}]}
								onPress={() => this.openModal(i)}
							>
								<Text
									style={[styles.textButton, {fontSize: responsiveFontSize(1.2)}]}
								>
									Agregar materia(s){' '}
									<Text style={{fontWeight: '900', fontSize: responsiveFontSize(1.3)}}>
										+
									</Text>
								</Text>
							</TouchableOpacity>
							<Text
								style={{
									textAlign: 'center',
									paddingRight: 10,
									fontSize: responsiveFontSize(1.6),
									marginTop: 3
								}}
							>
								Total de horas asignadas: {' '}{this.state.elTotal[i]}
							</Text>
						</View>
					</View>
				) : null}
			</View>
		</View>);
	}

	renderMats() {
		let btnMat = [];
		for (let i = 0; i < this.state.laRelacion.length; i++) {
			btnMat.push(this.renderMat(i));
		}
		return btnMat;
	}

	// ++++++++++++++++++++++++++++++++ GRUPOS ++++++++++++++++++++++++++++++++++

	async getGrupos() {
		let grupopicker = await fetch(this.state.uri + '/api/get/grupos', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let grupospicker = await grupopicker.json();
		await this.setState({grupos: grupospicker});
		await this.setState({losGrupos: []});
		this.state.grupos.forEach((item, i) => {
			this.state.losGrupos.push(item.grupo);
		});
	}

	async onListItemPressedGrupo(index, grupo) {
		this.setState({selectedIndexGrupos: index, elGrupo: grupo});
		let i = this.state.checkBoxBtns.indexOf(grupo);
		if (i > -1) {
			this.state.checkBoxBtns.splice(i, 1);
			this.state.checkBoxBtnsIndex[index] = 0;
		} else {
			this.state.checkBoxBtns.push(grupo);
			this.state.checkBoxBtnsIndex[index] = 1;
		}
	}

	renderGrupo1(itmGpo, i, indxGpo) {
		let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn4_lT, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.checkBoxBtnsIndex[indxGpo] === 1) {
			smallButtonStyles.push(styles.listButtonSelected, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		if (itmGpo !== 'Todos') {
			return (
				<TouchableHighlight
					key={indxGpo + 'gpoMat'}
					style={smallButtonStyles}
					underlayColor={this.state.secondColor}
					onPress={() => this.onListItemPressedGrupo(indxGpo, itmGpo)}
				>
					<View style={styles.listItem}>
						<Text style={texto}>{itmGpo}</Text>
					</View>
				</TouchableHighlight>
			);
		}
	}

	renderGrupos(i) {
		let buttonsGrupos = [];
		this.state.losGrupos.forEach((itmGpo, indxGpo) => {
			buttonsGrupos.push(this.renderGrupo1(itmGpo, i, indxGpo));
		});
		return buttonsGrupos;
	}


	async activoMaestro1() {
		if (this.state.elActivo === '0') {
			await this.setState({
				elActivo: '1', laRelacion: []
			});
			await this.getRelacionMaterias();
		}
		await this.setState({aux: 0});
	}

	async activoMaestro2() {
		if (this.state.elActivo === '1') {
			await this.setState({
				elActivo: '0'
			});
			await this.setState({aux: 0});
			await this.getRelacionMaterias();
		}
		await this.setState({aux: 0});
	}

	captMaest() {
		const materiasss = this.renderMats();
		let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		if (this.state.elActivo === '0') {
			btnCal.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
			texto.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		let texto1 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnTem = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		if (this.state.elActivo === '1') {
			btnTem.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
			texto1.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		return (<View style={styles.container}>
			<Text style={[styles.main_title, {color: this.state.thirdColor, paddingTop: 0, marginTop: 5}]}>
				Tipo de maestros a consultar
			</Text>
			<View
				style={[styles.rowsCalif, {
					width: responsiveWidth(62), marginTop: 10, marginBottom: 15
				}]}>
				<TouchableHighlight
					underlayColor={this.state.thirdColor}
					style={btnTem}
					onPress={() => this.activoMaestro1()}>
					<Text style={texto1}>Activos</Text>
				</TouchableHighlight>
				<TouchableHighlight
					underlayColor={this.state.thirdColor}
					style={btnCal}
					onPress={() => this.activoMaestro2()}>
					<Text style={texto}>Inactivos</Text>
				</TouchableHighlight>
			</View>
			<ScrollView showsVerticalScrollIndicator={false}>{materiasss}</ScrollView>
			<TouchableOpacity
				style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
				onPress={() => this.saveMateria()}
			>
				<Text style={styles.textButton}>Guardar</Text>
			</TouchableOpacity>
		</View>);
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected, indexSelected: indexSelected
		});
	}

	render() {
		return (<View style={styles.container}>
			<StatusBar
				backgroundColor={this.state.mainColor}
				barStyle='light-content'
			/>
			<MultiBotonRow
				itemBtns={['Alta de maestros', 'Plantilla docente']}
				onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
				cantidad={2}
			/>
			{this.state.indexSelected === 0 ? (<AltaMaestro/>) : this.captMaest()}
		</View>);
	}
}
