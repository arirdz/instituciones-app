import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	TouchableOpacity,
	TextInput,
	ScrollView,
	Alert
} from 'react-native';
import {
	responsiveFontSize,
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import ModalSelector from 'react-native-modal-selector';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modal';
import {Actions} from 'react-native-router-flux';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class MatCapt extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			lasMaterias: [],
			elGrado: '1',
			grado: '',
			materias: [],
			gdoSelect: [],
			gradoPick: [],
			isModalMat: [],
			nombMat: '',
			hrsClas: '',
			indexGdo: 0,
			aux: 0

		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.getGrados();
		await this.getMateriaPorGrado();
	}

	async borrarMateria(i) {
		let formData = new FormData();
		formData.append(
			'update',
			JSON.stringify({
				id: this.state.lasMaterias[i].id,
				nombre: this.state.lasMaterias[i].nombre,
				grado: this.state.lasMaterias[i].grado,
				horas: this.state.lasMaterias[i].horas
			})
		);
		await fetch(this.state.uri + '/api/borrar/materias', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Materias)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		}
	}

	async updateMateria(i) {
		this.state.lasMaterias[i].nombre = this.state.nombMat;
		this.state.lasMaterias[i].horas = this.state.hrsClas;
		this.state.lasMaterias[i].grado = this.state.grado;
		let formData = new FormData();
		formData.append(
			'update',
			JSON.stringify({
				id: this.state.lasMaterias[i].id,
				nombre: this.state.lasMaterias[i].nombre,
				grado: this.state.lasMaterias[i].grado,
				horas: this.state.lasMaterias[i].horas
			})
		);
		await fetch(this.state.uri + '/api/update/materias', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Materias)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		}
		await this.closeModal(i);
	}

	async getGrados() {
		let gradpicker = await fetch(this.state.uri + '/api/get/grados',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			});
		let gradospicker = await gradpicker.json();
		this.setState({gradoPick: gradospicker});
	}

	async getMateriaPorGrado() {
		let materia = await fetch(this.state.uri + '/api/feed/view/materias/' + this.state.elGrado,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			});
		let materias = await materia.json();
		await this.setState({lasMaterias: materias});
		this.state.lasMaterias.forEach(() => {
			this.state.isModalMat.push(false);
		});
		await this.setState({aux: 0})
	}

	async onChangeGdo(option) {
		await this.setState({
			indexGdo: option.key,
			elGrado: option.label
		});
		await this.getMateriaPorGrado();
	}

//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+Materia
	async alertBorrar(i) {
		Alert.alert('¡Atencion!',
			'¿Esta seguro de borrar esta materia?' + this.state.lasMaterias[i].nombre,
			[

				{
					text: 'Si',
					onPress: () => this.borrar(i)
				},
				{text: 'No'}
			])
	}

	async borrar(i) {
		await this.borrarMateria(i);
		this.state.lasMaterias.splice(i, 1);
		await this.setState({aux: 0});
	}

	async openModal(i) {
		this.state.isModalMat[i] = true;
		this.state.nombMat = this.state.lasMaterias[i].nombre;
		this.state.hrsClas = this.state.lasMaterias[i].horas;
		this.state.grado = this.state.lasMaterias[i].grado;
		await this.setState({aux: 0});
	}

	async closeModal(i) {
		this.state.isModalMat[i] = false;
		await this.setState({aux: 0});
	}

	async onChange1(option) {
		await this.setState({
			hrsClas: option.label
		});
	}

	async onChange(option) {
		await this.setState({
			grado: option.label
		});
	}

	materia(item, i) {
		let index = 0;
		const dataH = [
			{key: index++, label: '1 '},
			{key: index++, label: '2 '},
			{key: index++, label: '3 '},
			{key: index++, label: '4 '},
			{key: index++, label: '5 '},
			{key: index++, label: '6 '},
			{key: index++, label: '7 '},
			{key: index++, label: '8 '},
			{key: index++, label: '9'},
			{key: index++, label: '10 '}
		];
		let data = this.state.gradoPick.map((item, i) => {
			return {
				key: i,
				label: item.grado
			}
		});
		if (this.state.lasMaterias[i].grado === item.grado) {
			return (
				<View
					key={i + 'rndMats'}
					style={[
						styles.widthall,
						styles.row,
						{
							height: responsiveHeight(8),
							marginVertical: 5,
							borderWidth: 0.5,
							borderRadius: 6,
							borderColor: 'lightgray',
							backgroundColor: this.state.fourthColor,
							paddingHorizontal: 6
						}
					]}>
					<Modal
						isVisible={this.state.isModalMat[i]}
						backdropOpacity={0.8}
						animationIn={'bounceIn'}
						animationOut={'bounceOut'}
						animationInTiming={1000}
						animationOutTiming={1000}
					>
						<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
							<Text
								style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
							>
								Editar materia
							</Text>
							<Text
								style={[
									styles.modalWidth,
									styles.textInfo,
									styles.textW,
									{textAlign: 'left'}
								]}>
								Ingrese el nombre de la materia:
							</Text>
							<TextInput
								keyboardType='default'
								maxLength={254}
								onChangeText={text => (this.state.nombMat = text)}
								placeholder={'Nombre de la materia'}
								defaultValue={this.state.nombMat}
								returnKeyType='next'
								underlineColorAndroid='transparent'
								style={[
									styles.inputPicker,
									styles.modalWidth,
									{
										borderColor: this.state.secondColor,
										textAlign: 'center',
										marginTop: 3
									}
								]}
							/>
							<Text
								style={[
									styles.modalWidth,
									styles.textInfo,
									styles.textW,
									{marginTop: 10}
								]}>
								Asigne grado y horas por semana a la materia:
							</Text>
							<View style={[styles.modalWidth, styles.row]}>
								<View style={{alignItems: 'center'}}>
									<ModalSelector
										data={data}
										selectStyle={[
											styles.modalBigBtn,
											{
												borderColor: this.state.secondColor,
												marginTop: 3,
												marginBottom: 3,
												borderWidth: .5
											}
										]}
										cancelText='Cancelar'
										optionTextStyle={{color: this.state.thirdColor}}
										initValue={this.state.grado}
										onChange={option => (this.state.grado = option.label)}
									/>
									<Text style={styles.textInfo}>Grado</Text>
								</View>
								<View style={{alignItems: 'center'}}>
									<ModalSelector
										data={dataH}
										selectStyle={[
											styles.modalBigBtn,
											{
												borderColor: this.state.secondColor,
												marginTop: 3,
												marginBottom: 3,
												borderWidth: .5
											}
										]}
										cancelText='Cancelar'
										optionTextStyle={{color: this.state.thirdColor}}
										initValue={this.state.hrsClas}
										onChange={option => (this.state.hrsClas = option.label)}
									/>
									<Text style={styles.textInfo}>Hrs/Semana</Text>
								</View>
							</View>
							<View style={[styles.modalWidth, styles.row]}>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: '#fff', borderColor: this.state.secondColor}
									]}
									onPress={() => [(this.state.isModalMat[i] = false), this.setState({aux: 0})]}
								>
									<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
									]}
									onPress={() => this.updateMateria(i)}
								>
									<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
								</TouchableOpacity>
							</View>
						</View>
					</Modal>
					<View style={[styles.btn3]}>
						<Text style={{fontSize: responsiveFontSize(1.5), textAlign: 'center'}}>Nombre:</Text>
						<Text
							numberOfLines={2}
							ellipsizeMode={'tail'}
							style={[
								styles.textW,
								{fontSize: responsiveFontSize(1.2), textAlign: 'center', fontWeight: '900'}
							]}>
							{item.nombre}
						</Text>
					</View>
					<View style={[styles.btn6, {alignItems: 'center', marginTop: 2}]}>
						<Text style={{textAlign: 'center', fontSize: responsiveFontSize(1.3)}}>Grado:</Text>
						<Text
							style={[
								styles.textW,
								{fontSize: responsiveFontSize(1.5), textAlign: 'center', fontWeight: '900'}
							]}>
							{item.grado}
						</Text>
					</View>
					<View style={[styles.btn6, {alignItems: 'center'}]}>
						<Text style={{textAlign: 'center', fontSize: responsiveFontSize(1.3)}}>
							Hrs/sem:
						</Text>
						<Text style={[styles.textW, {fontSize: responsiveFontSize(1.2), fontWeight: '900'}]}>
							{item.horas}Hrs
						</Text>
					</View>
					<View style={[styles.btn5, styles.row]}>
						<MaterialIcons name='edit' size={25} color='grey' onPress={() => this.openModal(i)}/>
						<Entypo name='circle-with-minus' size={25} color='red' onPress={() => this.alertBorrar(i)}/>
					</View>
				</View>
			);
		}
	}

	materias() {
		let btnMateria = [];
		this.state.lasMaterias.forEach((item, i) => {
			btnMateria.push(this.materia(item, i));
		});
		return btnMateria;
	}


	render() {
		let data = this.state.gradoPick.map((item, i) => {
			return {
				key: i,

				label: item.grado
			}
		});
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione un grado a consultar
				</Text>
				<ModalSelector
					data={data}
					selectStyle={[
						styles.inputPicker,
						{borderColor: this.state.secondColor, marginBottom: 5}
					]}
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
					selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
					initValue={this.state.elGrado}
					onChange={option => this.onChangeGdo(option)}
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Materias del grado {this.state.elGrado}
				</Text>
				<ScrollView style={{marginTop: 8, ...ifIphoneX({marginBottom: 40}, {marginBottom: 20})}}>
					{this.materias()}
				</ScrollView>
			</View>
		);
	}
}
