import React from "react";
import {
    Text,
    View,
    TextInput,
    AsyncStorage,
    StatusBar,
    KeyboardAvoidingView,
    TouchableOpacity,
    Keyboard,
    ScrollView,
} from "react-native";
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import Grado from "../Globales/Grado";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Switch from "react-native-switch-pro";

export default class limiteSalon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            gruposGG: [],
            grupos: [],
            aux:0,
            selectedIndexGrado: 0,
            elLimite: [],
            elGrado: "1"
        };
    }

    async componentWillMount(){
        await this.getURL();
        await this.getGrupos();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getGrupos() {
        let grupopicker = await fetch(this.state.uri + "/api/get/grupos", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let grupospicker = await grupopicker.json();
        await this.setState({grupos: grupospicker});
        await this.state.grupos.forEach(() => {
            this.state.gruposGG.push(true);
            this.state.elLimite.push("1");
        })
    }

    async onListItemPressedGrado(index, grado) {
        await this.setState({
            selectedIndexGrado: index,
            elGrado: grado
        });
    }

    async elLimite(text, i){
        this.state.elLimite[i] = text;
        await this.setState({aux:0});
    }

    async switchPro(i) {
        if (this.state.gruposGG[i] === true) {
            this.state.gruposGG[i] = false
        } else {
            this.state.gruposGG[i] = true
        }
        await this.setState({aux: 0});
    }

    renderGrpo(itmGrpo, indGrpo) {
        return (
            <View
                key={indGrpo + "a"}
                style={[
                    styles.widthall,
                    styles.row,
                    {
                        borderTopWidth: 1,
                        alignItems: "center",
                        paddingVertical: 10,
                        borderColor: this.state.fourthColor
                    }
                ]}
            >
                <Text
                    style={[
                        styles.textW,
                        {fontSize: responsiveFontSize(2), color: this.state.secondColor}
                    ]}>
                    Grupo: {itmGrpo.grupo}
                </Text>
                <View style={styles.switch_P}>
                    <FontAwesome name="lock" size={18} color="gray"/>
                    <Switch
                        value={true}
                        onSyncPress={() =>[this.setState({aux:0}), this.switchPro(indGrpo)]}
                    />
                    <FontAwesome name="unlock-alt" size={18} color="gray"/>
                </View>
                {this.state.gruposGG[indGrpo] === true ? (
                    <View>
                        <TextInput
                            keyboardType={"numeric"}
                            maxLength={3}
                            onChangeText={ text => this.elLimite(text, indGrpo)}
                            placeholder={"Número"}
                            returnKeyType={"next"}
                            underlineColorAndroid="transparent"
                            style={[
                                styles.inputPicker,
                                styles.btn5,
                                {textAlign: "center", height: responsiveHeight(5), marginTop: 0}
                            ]}
                        />
                    </View>
                ):(
                    <View style={[
                        styles.inputPicker,
                        styles.btn5,
                        {alignItems: "center", height: responsiveHeight(5), marginTop: 0}
                    ]}>
                        <Text>{this.state.elLimite[indGrpo]}</Text>
                    </View>
                )}
            </View>
        );
    }

    renderGrpos() {
        let btnGrupo = [];
        this.state.grupos.forEach((itmGrpo, indGrpo) => {
            btnGrupo.push(this.renderGrpo(itmGrpo, indGrpo))
        });
        return btnGrupo;
    }

    render() {
        const grupos = this.renderGrpos();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <KeyboardAvoidingView behavior={'position'} keyboardVerticalOffset={100}>
                    <Grado
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Defina la cantidad de alumnos que tendrá el salon de cada grupo
                        </Text>
                        <View style={{marginTop: 10}}>{grupos}</View>
                        <View style={[styles.widthall, {borderBottomWidth: 1, borderColor: this.state.fourthColor}]}/>
                    </View>
                </KeyboardAvoidingView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor, marginTop: 132}
                    ]}
                    // onPress={}
                >
                    <Text style={styles.textButton}>Guardar</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
