import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	StatusBar,
	TextInput,
	TouchableOpacity,
	ScrollView,
	Alert,
	Keyboard
} from 'react-native';
import {
	responsiveFontSize,
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Modal from 'react-native-modal';

export default class cantidadGrupos extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			selecNum: '',
			nombCiclo: '',
			grupitos: [],
			gruposGG: true,
			isModalGpo: [],
			nombPer: [],
			grupoS: '',
			indicee: '',
			indexPer: '',
			periodoDur: [],
			losGrupos: [],
			elGrupo: [],
			gdoPick: [],
			grupoCant: '',
			indexGrado: 0,
			elGrado: '1',
			elGrado1: '',
			grupoN: [],
			aux: 0,
			elRequest: []
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.getGrados();
		await this.getGruposParametro();
	}

	async updateGrupo(i) {
		this.state.losGrupos[i].gpo = this.state.grupoS;
		this.state.losGrupos[i].limiteGpo = this.state.grupoCant;
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				grado: this.state.elGrado,
				gdo: this.state.nombCiclo,
				grupo: i + 1,
				gpo: this.state.grupoS,
				limiteGpo: this.state.grupoCant
			})
		);
		await fetch(this.state.uri + '/api/post/gg', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});

			});

		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Grupos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			await this.closeModal(i);
		}
	}

	async requestMultipart2() {
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				grado: this.state.elGrado1,
				gdo: this.state.nombCiclo,
				grupo: '',
				gpo: this.state.grupoS,
				limiteGpo: this.state.grupoCant
			})
		);
		await fetch(this.state.uri + '/api/post/gg', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});

			});

		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Grupos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('¡Felicidades!',
				'Se han guardado los grupos', [
					{
						text: 'Entendido',
						onPress: () => [this.requestMultipart4()]
					}
				])
		}
	}


	async borrarGrupos(i) {
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				id: this.state.losGrupos[i].id
			})
		);
		await fetch(this.state.uri + '/api/borrar/gg', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Grupos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		}
	}


	async requestMultipart4() {
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				grupo: this.state.grupoS
			})
		);
		await fetch(this.state.uri + '/api/post/grupos', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Grupos)',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		} else {
			Actions.refresh({key: Math.random()});
		}
	}

	async cicloGrpo(i) {
		Alert.alert('¡Atención!', '¿Está seguro que desea borrar este grupo?', [
			{
				text: 'Sí',
				onPress: () => [
					this.borrar(i)
				]
			},
			{text: 'No'}
		]);
	}

//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-grados
	async getGrados() {
		let gradpicker = await fetch(this.state.uri + '/api/get/grados',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			});
		let gradospicker = await gradpicker.json();
		this.setState({gdoPick: gradospicker});
	}

//-+-+-+-+-+--+-+-+--+---+-+--+-++-+-+-+-+--+-+Get grupos
	async getGruposParametro() {
		let grupo = await fetch(this.state.uri + '/api/get/gg/parametro/' + this.state.elGrado,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			});
		let grupos = await grupo.json();
		await this.setState({losGrupos: grupos});
		await this.setState({nombCiclo: this.state.losGrupos[0].gdo});
		this.state.losGrupos.forEach(() => {
			this.state.isModalGpo.push(false);
		});
	}

	//-+-+-+-+-+---+-+--+-+-+-+-+-+-+--+-+-+-+-+-+-+ Grupos
	async borrar(i) {
		this.borrarGrupos(i);
		this.state.losGrupos.splice(i, 1);
		await this.setState({aux: 0});
	}

	async agregar() {
		await this.requestMultipart2();
		if (this.state.elGrado === this.state.elGrado1) {
			this.state.losGrupos.push({
				grado_gpo: this.state.elGrado1,
				gpo: this.state.grupoS,
				limiteGpo: this.state.grupoCant
			});
		}
		await this.setState({aux: 0});
	}

	async agregarGrupo() {
		if (this.state.grupoS === '') {
			Alert.alert(
				'Ups!',
				'No ha puesto nombre al grupo',
				[
					{
						text: 'Entendido'
					}
				]
			);
		} else if (this.state.elGrado1 === '') {
			Alert.alert(
				'Ups!',
				'No ha definido el grado del grupo',
				[
					{
						text: 'Entendido'
					}
				]
			);
		} else if (this.state.grupoCant === '') {
			Alert.alert(
				'Ups!',
				'No ha definido la cantidad maxima del grupo',
				[
					{
						text: 'Entendido'
					}
				]
			);
		} else {
			await this.agregar();
		}
	}

	async openModal(i) {
		this.state.isModalGpo[i] = true;
		this.state.grupoS = this.state.losGrupos[i].gpo;
		this.state.grupoCant = this.state.losGrupos[i].limiteGpo;
		await this.setState({aux: 0});
	}

	async closeModal(i) {
		this.state.isModalGpo[i] = false;
		await this.setState({aux: 0});
	}

	cantGrupos(item, i) {
		let btnCant = [];
		this.state.losGrupos.forEach((item, i) => {
			btnCant.push(
				<View
					key={i + 'cantGrupo'}
					style={[
						styles.widthall,
						styles.row,
						styles.cardEnc,
						{backgroundColor: this.state.fourthColor, height: responsiveHeight(5)}
					]}
				>
					<Modal
						isVisible={this.state.isModalGpo[i]}
						backdropOpacity={0.8}
						animationIn={'bounceIn'}
						animationOut={'bounceOut'}
						animationInTiming={1000}
						animationOutTiming={1000}
					>
						<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
							<Text
								style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
							>
								Edita el grupo o cantidad de alumnos
							</Text>
							<View style={[styles.row, styles.modalWidth, {marginTop: 8}]}>
								<Text style={{fontWeight: '700'}}>Nombre del grupo</Text>
								<Text style={{width: responsiveWidth(42), fontWeight: '700'}}>Grado del grupo</Text>
							</View>
							<View style={[styles.modalWidth, styles.row]}>
								<TextInput
									keyboardType='default'
									maxLength={20}
									multiline={false}
									onChangeText={text => (this.state.grupoS = text)}
									onSubmitEditing={Keyboard.dismiss}
									placeholder='Nombra al grupo'
									returnKeyType='done'
									underlineColorAndroid='transparent'
									value={this.state.grupoS}
									style={[
										styles.modalBigBtn,
										{
											textAlign: 'center',
											backgroundColor: '#fff',
											borderColor: this.state.secondColor,
											borderWidth: .5,
											marginTop: 3,
											marginBottom: 0
										}
									]}
								/>
								<TextInput
									keyboardType='default'
									maxLength={20}
									multiline={false}
									onChangeText={text => (this.state.grupoCant = text)}
									onSubmitEditing={Keyboard.dismiss}
									placeholder='Cantidad del grupo'
									returnKeyType='done'
									underlineColorAndroid='transparent'
									value={this.state.grupoCant}
									style={[
										styles.modalBigBtn,
										{
											backgroundColor: '#fff',
											borderColor: this.state.secondColor,
											borderWidth: .5,
											marginTop: 3,
											textAlign: 'center',
											marginBottom: 0
										}
									]}
								/>
							</View>
							<View style={[styles.modalWidth, styles.row]}>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: '#fff', borderColor: this.state.secondColor}
									]}
									onPress={() => [(this.state.isModalGpo[i] = false), this.setState({aux: 0})]}
								>
									<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
									]}
									onPress={() => this.updateGrupo(i)}
								>
									<Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
								</TouchableOpacity>
							</View>
						</View>
					</Modal>
					<View style={[styles.btn6_6]}>
						<Entypo name='circle-with-minus' size={22} color='red' onPress={() => this.cicloGrpo(i)}/>
					</View>
					<Text style={[styles.textW, styles.btn_2]}>Grupo {i + 1}</Text>
					<Text style={[styles.textW, styles.btn6, {marginRight: 10}]}>{item.gpo}</Text>
					<Text style={[styles.textW, styles.btn5]}>{item.limiteGpo}</Text>
					<View style={[styles.btn6_6]}>
						<MaterialIcons name='edit' size={21} color='grey' onPress={() => this.openModal(i)}/>
					</View>
				</View>
			);
		});
		return btnCant;
	}


	async onChangeGpo(option) {
		await this.setState({
			indexGrado: option.key,
			elGrado: option.label
		});
		await this.getGruposParametro();
	}

	async onChangeGdo(option) {
		await this.setState({
			indexGrado1: option.key,
			elGrado1: option.label
			// elGrado: option.label
		})
	}

	render() {
		let data = this.state.gdoPick.map((item, i) => {
			return {
				key: i,
				label: item.grado
			}
		});
		let data1 = this.state.gdoPick.map((item, i) => {
			return {
				key: i,
				label: item.grado
			}
		});
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Crear nuevo grupo
				</Text>
				<View style={[styles.row, styles.widthall, {marginTop: 10}]}>
					<Text style={{fontWeight: '800'}}>Nombre del grupo</Text>
					<Text style={{width: responsiveWidth(46), fontWeight: '800'}}>Grado del grupo</Text>
				</View>
				<View style={[styles.row, styles.widthall, {marginTop: 2}]}>
					<TextInput
						keyboardType='default'
						maxLength={254}
						onSubmitEditing={Keyboard.dismiss}
						onChangeText={text => (this.state.grupoS = text)}
						placeholder='Nombra al grupo'
						returnKeyType='next'
						underlineColorAndroid='transparent'
						style={[
							styles.inputPicker,
							{
								borderColor: this.state.secondColor,
								textAlign: 'center',
								marginTop: 0,
								width: responsiveWidth(46)
							}
						]}
					/>
					<ModalSelector
						data={data1}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor}}
						initValue='Seleccione un grado'
						onChange={option => this.onChangeGdo(option)}
						selectStyle={[
							styles.inputPicker,
							{
								borderColor: this.state.secondColor,
								marginTop: 0,
								width: responsiveWidth(46)
							}
						]}
					/>
				</View>
				<Text style={[styles.widthall, {marginTop: 10, fontWeight: '800'}]}>
					Defina el limite máximo de alumnos
				</Text>
				<View style={[styles.row, styles.widthall, {paddingHorizontal: 0}]}>
					<TextInput
						keyboardType='numeric'
						maxLength={254}
						onSubmitEditing={Keyboard.dismiss}
						onChangeText={text => (this.state.grupoCant = text)}
						placeholder='Cantidad del grupo'
						returnKeyType='next'
						underlineColorAndroid='transparent'
						style={[
							styles.inputPicker,
							styles.btn11,
							{borderColor: this.state.secondColor, textAlign: 'center', marginTop: 0}
						]}
					/>
					<View style={[styles.btn2_2T, {alignItems: 'center'}]}>
						<Entypo name='circle-with-plus' size={40} color='#43d551' onPress={() => this.agregarGrupo()}/>
					</View>
				</View>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione el grado a consultar
				</Text>
				<ModalSelector
					data={data}
					selectStyle={[
						styles.inputPicker,
						{borderColor: this.state.secondColor, marginTop: 3}
					]}
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor}}
					initValue={this.state.elGrado}
					onChange={option => this.onChangeGpo(option)}
				/>
				<Text
					style={[styles.main_title, {
						color: this.state.thirdColor,
						fontSize: responsiveFontSize(2),
						marginBottom: 10
					}]}
				>
					Año {this.state.elGrado}
				</Text>
				<View style={[styles.widthall, styles.row, {marginBottom: 10}]}>
					<View style={[styles.btn4]}>
						<Text
							style={[{fontSize: responsiveFontSize(1.4), fontWeight: '600', marginLeft: 10}]}
						>
							Borrar
						</Text>
					</View>
					<View style={[styles.btn4]}>
						<Text
							style={{
								fontSize: responsiveFontSize(1.4),
								fontWeight: '600',
								textAlign: 'right',
								marginLeft: 10
							}}
						>
							Nombre
						</Text>
					</View>
					<View style={[styles.btn4]}>
						<Text
							style={{
								fontSize: responsiveFontSize(1.4),
								fontWeight: '600',
								textAlign: 'center',
								marginLeft: 25
							}}
						>
							Límite
						</Text>
					</View>
					<View style={[styles.btn4]}>
						<Text
							style={{
								fontSize: responsiveFontSize(1.4),
								fontWeight: '600',
								textAlign: 'center',
								marginLeft: 10
							}}
						>
							Editar
						</Text>
					</View>
				</View>
				<ScrollView>
					{this.cantGrupos()}
				</ScrollView>
				{/*<TouchableOpacity*/}
				{/*style={[*/}
				{/*styles.bigButton,*/}
				{/*{backgroundColor: this.state.secondColor}*/}
				{/*]}*/}
				{/*onPress={() => this.cicloGrpo()}*/}
				{/*>*/}
				{/*<Text style={styles.textButton}>Guardar</Text>*/}
				{/*</TouchableOpacity>*/}
			</View>
		);
	}
}
