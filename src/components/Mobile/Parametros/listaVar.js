import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	StatusBar,
	Keyboard,
	TextInput,
	KeyboardAvoidingView,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight
} from 'react-native';
import {
	responsiveHeight,
	responsiveFontSize
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Entypo from 'react-native-vector-icons/Entypo';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Modal from 'react-native-modal';
import MultiBotonRow from '../Globales/MultiBotonRow';
import EditarList from './EditarList';
import Spinner from 'react-native-loading-spinner-overlay';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class listaVar extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			elTitulo: '',
			checkBoxBtns: [],
			visible: false,
			checkBoxBtnsIndex: [],
			selectedIndexGrados: -1,
			elGrado: '',
			isModalalumno: false,
			isModalLista: false,
			isModalT: false,
			isVisible: false,
			estaList: [],
			aux: 0,
			elRequest: [],
			botonSelected: 'Nueva lista',
			indexSelected: 0,
			varList: [],
			tituloEditado: '',
			laLista: [],
			alumnos: [],
			listaV2: [],
			selectedIndexAlumno: -1,
			elAlumno: '',
			elIdAlumno: [],
			gradoAlumn: [],
			grupoAlumn: [],
			selectedIndexGrupos: -1,
			elGrupo: ''
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async requestMultipart() {
		this._changeWheelState();
		let formData = new FormData();
		for (let i = 0; i < this.state.laLista.length; i++) {
			formData.append(
				'new',
				JSON.stringify({
					nombre_lista: this.state.elTitulo,
					id_alumno: this.state.laLista[i].id
				})
			);
			await fetch(this.state.uri + '/api/post/lista', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				},
				body: formData
			}).then(res => res.json())
				.then(responseJson => {
					this.setState({elRequest: responseJson});
				});
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al guardar lista',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Listas variables)',
				[{
					text: 'Entendido', onPress: () => this.setState({visible: false})
				}]);
		} else {
			Alert.alert('Lista guardada',
				'Se ha creado una nueva lista con éxito. Para consultarla vaya a la opción listas guardadas',
				[{
					text: 'Entendido',
					onPress: () => this.setState({visible: false, laLista: [], elTitulo: '', isVisible: false})
				}
				]);
		}
	}

	modalList() {
		this.setState({isModalLista: true});
	}

	lista() {
		if (this.state.isVisible === false) {
			this.setState({isVisible: true});
		}
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			indexSelected: indexSelected,
			botonSelected: itemSelected

		});
	}

	async openModal() {
		if (this.state.selectedIndexGrados === -1) {
			Alert.alert('Campo grado vacío', 'No ha seleccionado un grado', [
				{text: 'Entendido'}
			]);
		} else if (this.state.selectedIndexGrupos === -1) {
			Alert.alert('Campo grupo vacío', 'No ha seleccionado un grupo', [
				{text: 'Entendido'}
			]);
		} else if (this.state.alumnos.length === 0) {
			Alert.alert('Sin alumnos', 'No hay alumnos en este grupo', [
				{text: 'Entendido'}
			]);
		} else {
			for (let i = 0; i < this.state.laLista.length; i++) {
				for (let j = 0; j < this.state.alumnos.length; j++) {
					if (this.state.laLista[i].id === this.state.alumnos[j].id) {
						this.state.alumnos.splice(j, 1);
						this.state.checkBoxBtnsIndex[j] = 0;
					}
				}
			}
			if (this.state.alumnos.length === 0) {
				Alert.alert('Sin alumnos por seleccionar', 'Ya ha seleccionado a todos los alumnos de este grupo', [
					{text: 'Entendido'}
				]);
			} else {
				this.setState({isModalalumno: true, checkBoxBtnsIndex: [], checkBoxBtns: []});
			}
		}
		await this.setState({aux: 0});
	}

	async saveMateria() {
		Alert.alert('Guardar lista', 'Está a punto de crear una lista\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.requestMultipart()}, {text: 'No'}
		]);
	}

	async nombre() {
		if (this.state.elTitulo === '') {
			Alert.alert('Nombre de lista indefinido', 'No ha definido el título a la lista', [
				{text: 'Entendido'}
			]);
		} else {
			this.setState({isModalLista: false});
			this.lista();
		}
	}

	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado,
			checkBoxBtnsIndex: []
		});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo,
			checkBoxBtnsIndex: []
		});
		await this.getAlumnos();
	}

	async addAlumnos() {
		this.state.checkBoxBtns.forEach((it, i) => {
			this.state.laLista.push(it);
		});
		await this.setState({aux: 0});
	}

	async borrar(index) {
		this.state.laLista.splice(index, 1);
		await this.setState({aux: 0});
		await this.getAlumnos();
	}


	alumnosVar() {
		let btnVarlist = [];
		let listOrden = this.state.laLista;
		let a = listOrden.sort(function (a, b) {
			if (a.grado + a.grupo + a.name < b.grado + b.grupo + b.name)
				return -1;
			if (a.grado + a.grupo + a.name > b.grado + b.grupo + b.name)
				return 1;
			return 0;
		});

		var index;
		for (index in a) {
			btnVarlist.push(
				<View
				key={index}
				style={[
					styles.row,
					styles.widthall,
					{marginVertical: 5, paddingHorizontal: 3}
				]}
			>
				<View style={[styles.btn8]}>
					<Text>{this.state.laLista[index].name}</Text>
				</View>
				<View style={[styles.row, {alignItems: 'flex-start'}]}>
					<Text>{this.state.laLista[index].grado}</Text>
					<Text>{this.state.laLista[index].grupo}</Text>
				</View>
				<TouchableOpacity
					style={{alignItems: 'center'}}
					onPress={() => this.borrar(index)}
				>
					<Entypo name='circle-with-minus' size={25} color='red'/>
				</TouchableOpacity>
			</View>);
		}
		return btnVarlist;
	}

	async closeModal() {
		await this.setState({isModalalumno: false});
	}

	// +++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++

	async getAlumnos() {
		let alumnoList = await fetch(
			this.state.uri +
			'/api/user/alumnos/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let alumnosList = await alumnoList.json();
		await this.setState({alumnos: alumnosList});
		this.state.alumnos.forEach(() => {
			this.state.checkBoxBtnsIndex.push(0);
		});
	}

	async onListPressedAlumno(itemAlumno, indexAlumno) {
		await this.setState({
			selectedIndexAlumno: indexAlumno,
			elAlumno: itemAlumno
		});
		let i = this.state.checkBoxBtns.indexOf(itemAlumno);
		if (i > -1) {
			this.state.checkBoxBtns.splice(i, 1);
			this.state.checkBoxBtnsIndex[indexAlumno] = 0;
		} else {
			this.state.checkBoxBtns.push(itemAlumno);
			this.state.checkBoxBtnsIndex[indexAlumno] = 1;
		}
		await this.setState({aux: 0});
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [
			styles.rowTabla,
			styles.modalWidth,
			{borderColor: this.state.secondColor}
		];
		let texto = [styles.textoN];
		if (this.state.checkBoxBtnsIndex[indexAlumno] === 1) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<TouchableHighlight
				key={indexAlumno}
				underlayColor={'transparent'}
				style={[tabRow, smallButtonStyles]}
				onPress={() => this.onListPressedAlumno(itemAlumno, indexAlumno)}
			>
				<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
					{this.state.isModalalumno === true ? (
						<View>
							<Text style={[texto, {marginRight: 25}]}>
								{itemAlumno.name}
							</Text>
						</View>
					) : null}
				</View>
			</TouchableHighlight>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	async agregarAlumns() {
		if (this.state.checkBoxBtnsIndex.length === 0) {
			Alert.alert('Sin alumnos seleccionados', 'Seleccione uno o varios alumnos antes de continuar', [{text: 'Entendido'}]);
		} else {
			this.addAlumnos();
			this.closeModal();
		}
	}


	async openModalT() {
		this.state.tituloEditado = this.state.elTitulo;
		await this.setState({isModalT: true});
	}

	async closeModalT() {
		this.state.elTitulo = this.state.tituloEditado;
		await this.setState({isModalT: false});
	}

	listNew() {
		const alumnos = this.renderAlumnos();
		const listaVar = this.alumnosVar();
		return (
			<View style={styles.container}>
				<Modal
					isVisible={this.state.isModalalumno}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>

						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}
							>
								{alumnos}
							</View>
						</ScrollView>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({
									isModalalumno: false,
									checkBoxBtns: [],
									checkBoxBtnsIndex: []
								})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => [this.agregarAlumns()]}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Agregar alumno(s)</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModalLista}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<KeyboardAvoidingView
						keyboardVerticalOffset={20}
						behavior={'padding'}
						style={[styles.container, {borderRadius: 6, flex: 0}]}
					>
						<Text
							style={[
								styles.main_title,
								styles.modalWidth,
								{backgroundColor: '#fff'}
							]}
						>
							Defina el nombre de la lista a crear
						</Text>
						<Text style={[styles.modalWidth, styles.textW, {marginTop: 10}]}>
							Nombre:
						</Text>
						<TextInput
							keyboardType='default'
							maxLength={254}
							onChangeText={text => (this.state.elTitulo = text)}
							placeholder={'ej.: Taller, eq. de football, Danza, etc...'}
							returnKeyType='next'
							underlineColorAndroid='transparent'
							style={[
								styles.inputPicker,
								styles.modalWidth,
								{borderColor: this.state.secondColor, marginTop: 3}
							]}
						/>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalLista: false, elTitulo: ''})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.nombre()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
							</TouchableOpacity>
						</View>
					</KeyboardAvoidingView>
				</Modal>
				<Modal
					isVisible={this.state.isModalT}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<KeyboardAvoidingView
						keyboardVerticalOffset={20}
						behavior={'padding'}
						style={[styles.container, {borderRadius: 6, flex: 0}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Edite el nombre de la lista
						</Text>
						<Text style={[styles.modalWidth, styles.textW, {marginTop: 10}]}>
							Nombre:
						</Text>
						<TextInput
							keyboardType='default'
							maxLength={254}
							onEndEditing={() => Keyboard.dismiss()}
							onChangeText={text => (this.state.tituloEditado = text)}
							defaultValue={this.state.tituloEditado}
							placeholder={'ej.: Taller, eq. de football, Danza, etc...'}
							returnKeyType='next'
							underlineColorAndroid='transparent'
							style={[
								styles.inputPicker,
								styles.modalWidth,
								{
									borderColor: this.state.secondColor,
									marginTop: 3,
									textAlign: 'center'
								}
							]}
						/>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalT: false})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => [this.closeModalT()]}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
							</TouchableOpacity>
						</View>
					</KeyboardAvoidingView>
				</Modal>
				{this.state.isVisible === false ? (
					<View style={styles.container}>
						<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
							Seleccione para crear una nueva lista.
						</Text>
						<TouchableOpacity
							style={[
								styles.pickerParam1,
								styles.btn2,
								{
									backgroundColor: '#fff',
									marginVertical: 9,
									borderColor: this.state.secondColor
								}
							]}
							onPress={() => this.modalList()}
						>
							<Text style={[styles.textButton, {color: '#000'}]}>Crear nueva lista</Text>
						</TouchableOpacity>

						<Text
							style={[
								styles.widthall,
								{
									fontSize: responsiveFontSize(1.5),
									fontStyle: 'italic',
									textAlign: 'center',
									paddingHorizontal: 20,
									color: 'black'
								}
							]}
						>
							Despues de crear una lista personalizada, podra editarla en "Listas
							guardadas"
						</Text>
					</View>
				) : null}
				{this.state.isVisible === true ? (
					<View style={styles.container}>
						<Text
							style={[
								styles.main_title,
								{color: this.state.thirdColor, paddingTop: 0}
							]}
						>
							Nombre de la lista.
						</Text>
						<View style={styles.row}>
							<Text
								style={[
									styles.textButton,
									{
										color: 'black',
										fontSize: responsiveFontSize(2.5)
									}
								]}
							>
								{this.state.elTitulo}{'  '}
							</Text>
							<MaterialIcons name='edit' size={20} color='grey' onPress={() => this.openModalT()}/>
						</View>
						<GradoyGrupo
							onListItemPressedGrado={(grado, indexGrado) => this.onListItemPressedGrado(grado, indexGrado)}
							onListItemPressedGrupos={(grupo, indexGrupo) => this.onListItemPressedGrupos(grupo, indexGrupo)}
						/>
						<Text style={[styles.main_title, {color: this.state.thirdColor, paddingTop: 0, marginTop: 0}]}>
							Seleccione el alumno
						</Text>
						<View style={{alignItems: 'center'}}>
							<TouchableOpacity
								style={[
									styles.bigButton,
									styles.btnLista,
									{borderColor: this.state.secondColor, ...ifIphoneX({marginBottom: 15}, {marginBottom: 10})}
								]}
								onPress={() => this.openModal()}
							>
								<Text style={[styles.textButton, {color: 'black'}]}>
									Ver lista
								</Text>
							</TouchableOpacity>

						</View>
						<Text style={[styles.widthall, {fontWeight: '800', textAlign: 'left'}]}>Alumnos
																								seleccionados:</Text>
						<ScrollView
							style={[styles.widthall, {
								borderTopWidth: 1,
								borderBottomWidth: 1,
								padding: 4,
								marginVertical: 5
							}]}
						>
							{listaVar}
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.bigButton,
								{backgroundColor: this.state.secondColor}
							]}
							onPress={() => this.saveMateria()}
						>
							<Text style={[styles.textButton]}>Guardar lista</Text>
						</TouchableOpacity>
					</View>
				) : null}
			</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Guardando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<MultiBotonRow
					itemBtns={['Nueva lista', 'Listas guardadas']}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelected(indexBtn, itemBtn)
					}
					cantidad={2}
				/>
				{this.state.botonSelected === 'Nueva lista' ? (
					this.listNew()
				) : (
					<EditarList/>
				)}
			</View>
		);
	}
}
