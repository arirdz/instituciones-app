import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import ModalSelector from 'react-native-modal-selector';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class horarios extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			elHorario: [],
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexGrupos: -1,
			elGrupo: '',
			horarioV2: [],
			horarioV3: [],
			horasPar: [],
			aux: 0,
			indice: 0,
			materias: [],
			materias2: [],
			dia: '',
			dia2: '',
			indexDia: -1,
			laMateria: [],
			estatDia: [],
			laHora: [],
			elRequest: [],
			elDia: [
				{id: '1', dia: 'Lunes'},
				{id: '2', dia: 'Martes'},
				{id: '3', dia: 'Miércoles'},
				{id: '4', dia: 'Jueves'},
				{id: '5', dia: 'Viernes'}
			],
			indice1: 0,
			elidMateria: [],
			elIdProf: [],
			laMateria2: [],
			horarioMat: [],
			horasPar2: [],
			faltaMat2: [],
			faltaMat: []
			// hasError:false
		};
	}

	//  componentDidCatch(error, info) {
	// 	// Display fallback UI
	// 	 this.setState({ hasError: true });
	// 	// You can also log the error to an error reporting service
	// 	console.log('We did catch', error, info);
	// }

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.getHorasAt();
	}

	//++++++++++++++++++++++++++++++++++++++ Request

	async guardarHorario() {
		Alert.alert('¡Atención!',
			'¿Está seguro que desea guardar?',
			[
				{text: 'Sí', onPress: () => this.borrarHorario()},
				{text: 'No'}
			]);
	}

	async borrarHorario() {
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			grado: this.state.elGrado,
			grupo: this.state.elGrupo,
			dia: this.state.dia
		}));
		await fetch(this.state.uri + '/api/borrar/horario/clases', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Encuadres)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			await this.requestMultipart();
		}
	}

	async requestMultipart() {
		let formData = new FormData();
		for (let i = 0; i < this.state.horarioV2.length; i++) {
			if (this.state.horarioV2[i] !== undefined) {
				formData.append('new', JSON.stringify({
					id_maestro: this.state.horarioV2[i].id_maestro,
					id_materia: this.state.horarioV2[i].id_materia,
					grado: this.state.horarioV2[i].grado,
					grupo: this.state.horarioV2[i].grupo,
					dia: this.state.horarioV2[i].Dia,
					id_horario: i + 1,
					ciclo: this.state.elGrado,
					periodo: 1
				}));
				await fetch(this.state.uri + '/api/post/horario/clases', {
					method: 'POST', headers: {
						'Content-Type': 'multipart/form-data', Authorization: 'Bearer ' + this.state.token
					}, body: formData
				}).then(res => res.json())
					.then(responseJson => {
						this.setState({elRequest: responseJson});
					});
			}
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al crear horarios',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Encuadres)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('Horario de clases creado',
				'Se ha guardado el horario con éxito',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		}
	}

	//+++++++++++tools++++++++++++++++++++++++++++++++++++++++++
	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado,
			laMateria: [],
			laMateria2: []
		});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo,
			laMateria: [],
			laMateria2: []
		});
		await this.getMaterias();
	}

	//+++++++++++++++++++++++++++++Materias++++++++++++++++++++++++++++++++
	async getMaterias() {
		let materiapicker = await fetch(
			this.state.uri
			+ '/api/get/materias/horario/' +
			this.state.elGrado
			+ '/' +
			this.state.elGrupo, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			});
		let materiaspicker = await materiapicker.json();
		await this.setState({materias: materiaspicker});
		if (this.state.materias.incompleto) {
			this.state.materias.incompleto.forEach((item, i) => {
				this.state.faltaMat.push(item.nombre);
			});
			let patron = /,/gi;
			let nuevoPatron = ',\n';
			this.state.faltaMat2 = this.state.faltaMat.toString().replace(patron, nuevoPatron);

			Alert.alert('¡Alerta!',
				'En este grupo, la(s) siguiente(s) materia(s) aun no tienen maestro asignado:\n' + this.state.faltaMat2,
				[
					{
						text: 'Asignar maestro',
						onPress: () => Actions.captMaestro()
					}
				])
		} else {
			this.state.materias2 = this.state.materias;
		}
		await this.setState({aux: 0});
	}

	//+++++++++++++++++++++++++++++horas
	async getHorasAt() {
		let switchHora = await fetch(this.state.uri + '/api/get/horarios', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer' + this.state.token
			}
		});
		let switchHoras = await switchHora.json();
		await this.setState({laHora: switchHoras});
	}

	async getHorasParametro() {
		await fetch(
			this.state.uri
			+ '/api/get/horarios/parametro/' +
			this.state.elGrado
			+ '/' +
			this.state.elGrupo
			+ '/' +
			this.state.dia,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar el horario', 'Ha ocurrido un error ' +
						responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Horario escolar)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({horarioMat: responseJson});
					this.setState({horasPar: responseJson});
				}
			});
	}

	//++++++++++++++++++++++++++++++ horassadassdcswecwecwecwecwcew
	async onChange(j, option) {
		await this.setState({
			elGrado: this.state.elGrado,
			elGrupo: this.state.elGrupo,
			dia: this.state.dia,
			laMateria: option.label,
			elidMateria: option.materia,
			elIdProf: option.maestro,
			indice1: j
		});
		await this.getHorarios();
		await this.saveHorario(j);
	}

	async saveHorario(i) {
		this.state.horarioV2[i] = this.state.elHorario;
		await this.setState({aux: 0});
	}

	async getHorarios() {
		let btnHorario = await Object({
			id_hora: this.state.indice1 + 1,
			Dia: this.state.dia,
			id_materia: this.state.elidMateria,
			id_maestro: this.state.elIdProf,
			grado: this.state.elGrado,
			grupo: this.state.elGrupo
		});
		await this.setState({elHorario: btnHorario});
	}

	async abrirModal(i) {
		if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacío', 'Seleccione un grado para poder continuar', [{text: 'Enterado'}])
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacío', 'Seleccione un grupo para poder continuar', [{text: 'Enterado'}])
		} else if (this.state.dia === '') {
			Alert.alert('Campo día vacío', 'Seleccione un día para poder continuar', [{text: 'Enterado'}])
		}
		await this.setState({aux: 0});
	}

	renderHora(itemHora, j) {
		let data = this.state.materias2.map((item, i) => {
			return {key: i, label: item.nombre.nombre, materia: item.nombre.id, maestro: item.maestro.id};
		});
		if (itemHora.tipo_hora === '2') {
			return (
				<View key={j + 'rndHora'}
					  style={[styles.row, styles.widthall, {paddingHorizontal: 6, marginVertical: 5}]}>
					<Text style={[{marginTop: 7, fontSize: responsiveFontSize(1.8)}]}>
						{moment(this.state.laHora[j].inicio, 'HH:mm').format('HH:mm')} -
						{moment(this.state.laHora[j].fin, 'HH:mm').format('HH:mm')}
					</Text>
					<View
						style={[styles.inputPicker, styles.btn2_3, {
							borderColor: 'lightgrey',
							backgroundColor: 'lightgrey'
						}]}>
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							Receso
						</Text>
					</View>
				</View>
			);
		} else {
			if (this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.dia !== '') {
				return (
					<View key={j + 'rndHor'}
						  style={[styles.row, styles.widthall, {paddingHorizontal: 6, marginVertical: 5}]}>
						<Text style={[{marginTop: 7, fontSize: responsiveFontSize(1.8)}]}>
							{moment(this.state.laHora[j].inicio, 'HH:mm').format('HH:mm')} -
							{moment(this.state.laHora[j].fin, 'HH:mm').format('HH:mm')}
						</Text>
						<ModalSelector
							cancelText='Cancelar'
							optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
							selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
							data={data}
							onChange={option => this.onChange(j, option)}
							selectStyle={[styles.inputPicker, styles.btn2_3, {borderColor: this.state.secondColor}]}
							initValue={this.state.laMateria2[j] === undefined ? 'Seleccione una opción' : this.state.laMateria2[j]}
						/>
					</View>
				);
			} else {
				return (
					<View key={j + 'rndHor'}
						  style={[styles.row, styles.widthall, {paddingHorizontal: 6, marginVertical: 5}]}>
						<Text style={[{marginTop: 7, fontSize: responsiveFontSize(1.8)}]}>
							{moment(this.state.laHora[j].inicio, 'HH:mm').format('HH:mm')} -
							{moment(this.state.laHora[j].fin, 'HH:mm').format('HH:mm')}
						</Text>
						<TouchableOpacity
							style={[styles.inputPicker, styles.btn2_3, {borderColor: this.state.secondColor}]}
							onPress={() => this.abrirModal(j)}
						>
							<Text style={{fontSize: responsiveFontSize(1.5)}}>Seleccione una opción</Text>
						</TouchableOpacity>

					</View>
				);
			}
		}

	}

	renderHoras() {
		let btnHora = [];
		this.state.laHora.forEach((itemHora, j) => {
			btnHora.push(this.renderHora(itemHora, j));
		});
		return btnHora;
	}

	async onChangeDia(dia) {
		await this.setState({dia: dia.id, dia2: dia.label, laMateria: []});
		await this.getMaterias();
		await this.getHorasParametro();
		this.state.horasPar.forEach((item, j) => {
			this.state.laMateria[j] = item.materias.nombre;
		});

		this.state.laHora.forEach((itemHora, j) => {
			if (itemHora.tipo_hora === '2') {
				this.state.horasPar.splice(j, 0, 'Receso');
				this.state.laMateria.splice(j, 0, 'Seleccione una opción');
			}
		});

		await this.setState({horasPar2: this.state.horasPar, laMateria2: this.state.laMateria});
	}

	render() {
		const estaHora = this.renderHoras();
		let dataD = this.state.elDia.map((it, i) => {
			return {key: i, id: it.id, label: it.dia}
		});
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<GradoyGrupo
					onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos(indexBtn, itemBtn)}
					onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado(indexBtn, itemBtn)}
					onListItemPressedLista={(indexBtn, itemBtn) => this.onListItemPressedLista(indexBtn, itemBtn)}
				/>
				<Text
					style={[styles.main_title, {color: this.state.thirdColor, paddingTop: 2, marginTop: 0}]}
				>
					Seleccione un día
				</Text>
				<ModalSelector
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
					selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
					data={dataD}
					onChange={option => this.onChangeDia(option)}
					selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
					initValue={'Seleccione el día'}
				/>
				<ScrollView style={{marginTop: 15}}>
					{estaHora}
				</ScrollView>

				{this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.dia !== '' ? (
					<TouchableOpacity
						style={[
							styles.bigButton,
							{backgroundColor: this.state.secondColor}
						]}
						onPress={() => this.guardarHorario()}
					>
						{this.state.horarioMat.length === 2 ?
							(<Text style={styles.textButton}>Guardar horario del {this.state.dia2}</Text>) : (
								<Text style={styles.textButton}>Actualizar horario del {this.state.dia2}</Text>
							)}
					</TouchableOpacity>
				) : (
					<View style={[
						styles.bigButton,
						{backgroundColor: this.state.secondColor}
					]}>
						<Text style={styles.textButton}>Haga una seleccion</Text>
					</View>
				)}
			</View>
		);
	}
}
