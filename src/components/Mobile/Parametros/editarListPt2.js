import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	StatusBar,
	KeyboardAvoidingView,
	TouchableOpacity,
	Keyboard,
	ScrollView,
	TextInput,
	TouchableHighlight
} from 'react-native';
import {
	responsiveHeight,
	responsiveWidth,
	responsiveFontSize
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Entypo from 'react-native-vector-icons/Entypo';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Spinner from 'react-native-loading-spinner-overlay';
import Modal from 'react-native-modal';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class listaVar extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			elTitulo: '',
			elTituloNew: '',
			elIdLista: '',
			checkBoxBtns: [],
			checkBoxBtnsIndex: [],
			elRequest: [],
			selectedIndexGrados: -1,
			elGrado: '',
			isModalalumno: false,
			isModalTitulo: false,
			estaList: [],
			aux: 0,
			visible: false,
			laLista: [],
			listProp: [],
			alumnos: [],
			listaV2: [],
			selectedIndexAlumno: -1,
			elAlumno: '',
			selectedIndexGrupos: -1,
			elGrupo: '',
			selectedIndexLista: -1
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.conversion();
	}

	async conversion() {
		this.state.laLista = this.props.edicion.alumnos;
		this.state.elIdLista = this.props.edicion.id;
		this.state.elTitulo = this.props.edicion.taller;
		await this.setState({aux: 0});
	}

	async borrarList() {
		this._changeWheelState();
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				id_lista: this.state.elIdLista,
				id: this.state.elIdLista
			})
		);
		await fetch(this.state.uri + '/api/post/delete/lista', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Editar lista)',
				[{
					text: 'Entendido', onPress: () => this.setState({visible: false})
				}]);
		} else {
			await this.requestMultipart();
		}
	}

	async requestMultipart() {
		let formData = new FormData();
		for (let i = 0; i < this.state.laLista.length; i++) {
			formData.append(
				'new',
				JSON.stringify({
					nombre_lista: this.state.elTitulo,
					id_alumno: this.state.laLista[i].id
				})
			);
			await fetch(this.state.uri + '/api/post/lista', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				},
				body: formData
			}).then(res => res.json())
				.then(responseJson => {
					this.setState({elRequest: responseJson});
				});
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al editar lista',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' al tratar de editar la lista, si el error continua pónganse en contacto con soporte'
				+
				'(Editar lista)',
				[{
					text: 'Entendido', onPress: () => this.setState({visible: false})
				}]);
		} else {
			Alert.alert('Lista editada', 'Su lista ha sido editada correctamente', [
				{
					text: 'Entendido',
					onPress: () => [this.setState({visible: false}), Actions.listaVar()]
				}
			]);
		}

	}

	async openModal() {
		if (this.state.selectedIndexGrados === -1) {
			Alert.alert('Campo grado vacío', 'No ha seleccionado un grado', [
				{text: 'Entendido'}
			]);
		} else if (this.state.selectedIndexGrupos === -1) {
			Alert.alert('Campo grupo vacío', 'No ha seleccionado un grupo', [
				{text: 'Entendido'}
			]);
		} else if (this.state.alumnos.length === 0) {
			Alert.alert('Sin alumnos', 'No hay alumnos en este grupo', [
				{text: 'Entendido'}
			]);
		} else {
			for (let i = 0; i < this.props.edicion.alumnos.length; i++) {
				for (let j = 0; j < this.state.alumnos.length; j++) {
					if (this.props.edicion.alumnos[i].id === this.state.alumnos[j].id) {
						this.state.alumnos.splice(j, 1);
						this.state.checkBoxBtnsIndex[j] = 0;
					}
				}
			}
			if (this.state.alumnos.length === 0) {
				Alert.alert('Sin alumnos por seleccionar', 'Ya ha seleccionado a todos los alumnos de este grupo', [
					{text: 'Entendido'}
				]);
			} else {
				this.setState({isModalalumno: true, checkBoxBtnsIndex: [], checkBoxBtns: []});
			}
		}
		await this.setState({aux: 0});
	}

	async saveMateria() {
		Alert.alert('Editar lista', 'Está a punto de editar la lista\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.borrarList()},
			{text: 'No'}
		]);
	}

	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado,
			checkBoxBtnsIndex: [],
			aux: 0
		});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo,
			checkBoxBtnsIndex: [],
			aux: 0
		});
		await this.getAlumnos();
	}

	//--------------------------------> nuevos alumnos
	async addAlumnos() {
		this.state.checkBoxBtns.forEach((it, i) => {
			this.state.laLista.push(it);
		});
		await this.setState({aux: 0});
	}

	//-------------------------------> lista actual
	async borrar(index) {
		this.state.laLista.splice(index, 1);
		await this.setState({aux: 0});
		await this.getAlumnos()
	}

	alumnoVar(index) {
		return (
			<View
				key={index + 'alumnoVar'}
				style={[
					styles.row,
					styles.widthall,
					{marginVertical: 5, paddingHorizontal: 10}
				]}
			>
				<View style={[styles.btn2]}>
					<Text style={{fontSize: responsiveFontSize(1.5)}}>{this.state.laLista[index].name}</Text>
				</View>
				<View style={[styles.row, {alignItems: 'flex-start'}]}>
					<Text style={{fontSize: responsiveFontSize(1.5)}}>{this.state.laLista[index].grado}</Text>
					<Text style={{fontSize: responsiveFontSize(1.5)}}>{this.state.laLista[index].grupo}</Text>
				</View>
				<Entypo name='circle-with-minus' size={25} color='red' onPress={() => this.borrar(index)}/>
			</View>
		);
	}

	alumnosVar() {
		let btnVarlist = [];
		let listOrden = this.state.laLista;
		let a = listOrden.sort(function (a, b) {
			if (a.grado + a.grupo + a.name < b.grado + b.grupo + b.name)
				return -1;
			if (a.grado + a.grupo + a.name > b.grado + b.grupo + b.name)
				return 1;
			return 0;
		});

		var index;
		for (index in a) {
			btnVarlist.push(this.alumnoVar(index));
		}
		return btnVarlist;
	}

	// +++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++

	async getAlumnos() {
		let alumnoList = await fetch(
			this.state.uri +
			'/api/user/alumnos/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let alumnosList = await alumnoList.json();
		await this.setState({alumnos: alumnosList});
		this.state.alumnos.forEach(() => {
			this.state.checkBoxBtnsIndex.push(0);
		});
	}

	comprobar() {
		for (let i = 0; i < this.props.edicion.alumnos.length; i++) {
			for (let j = 0; j < this.state.alumnos.length; j++) {
				if (this.props.edicion.alumnos[i].id === this.state.alumnos[j].id) {
					this.state.alumnos.splice(j, 1);
					this.state.checkBoxBtnsIndex[j] = 0;
				}
			}
		}
		this.setState({aux: 0});
	}

	async onListPressedAlumno(itemAlumno, indexAlumno) {
		await this.setState({
			selectedIndexAlumno: indexAlumno,
			elAlumno: itemAlumno
		});
		let i = this.state.checkBoxBtns.indexOf(itemAlumno);
		if (i > -1) {
			this.state.checkBoxBtns.splice(i, 1);
			this.state.checkBoxBtnsIndex[indexAlumno] = 0;
		} else {
			this.state.checkBoxBtns.push(itemAlumno);
			this.state.checkBoxBtnsIndex[indexAlumno] = 1;
		}
		await this.setState({aux: 0});
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [
			styles.rowTabla,
			styles.modalWidth,
			{borderColor: this.state.secondColor}
		];
		let texto = [styles.textoN];
		if (this.state.checkBoxBtnsIndex[indexAlumno] === 1) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<TouchableHighlight
				key={indexAlumno + 'rndAlumno'}
				underlayColor={this.state.secondColor}
				style={[tabRow, smallButtonStyles]}
				onPress={() => this.onListPressedAlumno(itemAlumno, indexAlumno)}
			>
				<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
					{this.state.isModalalumno === true ? (
						<View>
							<Text style={[texto, {marginRight: 25}]}>
								{itemAlumno.name}
							</Text>
						</View>
					) : null}
				</View>
			</TouchableHighlight>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	async closeModal() {
		await this.setState({
			isModalalumno: false,
			aux: 0
		});
	}

	async openModalT() {
		this.state.elTituloNew = this.props.edicion.taller;
		this.state.elTituloNew = this.state.elTitulo;
		this.state.isModalTitulo = true;
		await this.setState({aux: 0});
	}

	async closeModalT() {
		this.props.edicion.taller = this.state.elTituloNew;
		this.state.elTitulo = this.state.elTituloNew;
		this.state.isModalTitulo = false;
		await this.setState({aux: 0});
	}

	render() {
		const alumnos = this.renderAlumnos();
		const listaVar = this.alumnosVar();
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Guardando...'/>
				<Modal
					isVisible={this.state.isModalalumno}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>

						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<Text
							style={[
								styles.modalWidth,
								{fontSize: responsiveFontSize(1.5)}
							]}
						>
							*NOTA: Solo apareceran los alumnos que aun no pertenecen a este
							taller
						</Text>
						<ScrollView style={{marginTop: 10}}>

							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}
							>
								{alumnos}
							</View>
						</ScrollView>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({
									isModalalumno: false, checkBoxBtns: [],
									checkBoxBtnsIndex: []
								})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => [this.addAlumnos(), this.closeModal()]}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Agregar alumno(s)</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModalTitulo}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<KeyboardAvoidingView
						keyboardVerticalOffset={20}
						behavior={'padding'}
						style={[styles.container, {borderRadius: 6, flex: 0}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Edite el nombre de la lista
						</Text>
						<Text style={[styles.modalWidth, styles.textW, {marginTop: 10}]}>
							Nombre:
						</Text>
						<TextInput
							keyboardType='default'
							maxLength={254}
							onEndEditing={() => Keyboard.dismiss()}
							onChangeText={text => (this.state.elTituloNew = text)}
							defaultValue={this.state.elTituloNew}
							placeholder={'ej.: Taller, eq. de football, Danza, etc...'}
							returnKeyType='next'
							underlineColorAndroid='transparent'
							style={[
								styles.inputPicker,
								styles.modalWidth,
								{
									borderColor: this.state.secondColor,
									marginTop: 3,
									textAlign: 'center'
								}
							]}
						/>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalTitulo: false})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => [this.closeModalT()]}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
							</TouchableOpacity>
						</View>
					</KeyboardAvoidingView>
				</Modal>
				<View style={styles.container}>
					<Text
						style={[
							styles.main_title,
							{color: this.state.thirdColor, paddingTop: 0}
						]}
					>
						Nombre de la lista.
					</Text>
					<View style={styles.row}>
						<Text
							style={[
								styles.textButton,
								{
									color: 'black',
									fontSize: responsiveFontSize(2.5)
								}
							]}
						>
							{this.props.edicion.taller}{'  '}
						</Text>
						<MaterialIcons name='edit' size={20} color='grey' onPress={() => this.openModalT()}/>
					</View>
					<View style={{paddingTop: 0}}>
						<GradoyGrupo
							onListItemPressedGrado={(grado, indexGrado) => this.onListItemPressedGrado(grado, indexGrado)}
							onListItemPressedGrupos={(grupo, indexGrupo) => this.onListItemPressedGrupos(grupo, indexGrupo)}
						/>
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor, paddingTop: 0, marginTop: 0}]}>
						Seleccione el alumno
					</Text>
					<View style={{alignItems: 'center'}}>
						<TouchableOpacity
							style={[
								styles.bigButton,
								styles.btnLista,
								{borderColor: this.state.secondColor, ...ifIphoneX({marginBottom: 20}, {marginBottom: 15})}
							]}
							onPress={() => this.openModal()}
						>
							<Text style={[styles.textButton, {color: 'black'}]}>
								Ver lista
							</Text>
						</TouchableOpacity>
						<Text style={[styles.widthall, {fontWeight: '600'}]}>Alumnos seleccionados:</Text>
					</View>
					<View style={[styles.widthall, {borderWidth: .5, marginBottom: 8}]}/>
					<ScrollView>
						{listaVar}
					</ScrollView>
					<View style={[styles.widthall, {borderWidth: .5, marginTop: 8}]}/>
					<TouchableOpacity
						style={[
							styles.bigButton,
							{backgroundColor: this.state.secondColor}
						]}
						onPress={() => this.saveMateria()}
					>
						<Text style={[styles.textButton]}>Guardar cambios</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
