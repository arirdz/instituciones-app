import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modal';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class asuntosParam extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			elAsunto: [],
			elRequest: [],
			data: [],
			elTipoAsunto: '',
			elTipo: '',
			onlyPers: false,
			asuntoCord: '',
			isModalAsunt: [],
			elIdAsunto: [],
			aux: 0
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.getUserdata();
		await this.getAsuntos();
	}

	async requestMultipart() {
		await this.addAsunto();
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				asunto: this.state.asuntoCord,
				tipo: this.state.elTipoAsunto,
				role: this.state.data.role,
				puesto: this.state.data.puesto
			})
		);
		await fetch(this.state.uri + '/api/post/asuntos', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al guardar los asuntos',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Asuntos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Actions.refresh({key: Math.random()})
		}
	}

	async updateAsunto(i) {
		this.state.elAsunto[i].asunto = this.state.asuntoCord;
		let formData = new FormData();
		formData.append(
			'update',
			JSON.stringify({
				id: this.state.elAsunto[i].id,
				asunto: this.state.elAsunto[i].asunto,
				tipo: this.state.elAsunto[i].tipo,
				role: this.state.data.role,
				puesto: this.state.data.puesto
			})
		);
		await fetch(this.state.uri + '/api/update/asuntos', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al editar asunto',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' al tratar de editar el asunto si el error continua pónganse en contacto con soporte'
				+
				'(Asuntos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		}
		await this.closeModal(i);
	}

	async borrarAsuntos(i) {
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({id: this.state.elIdAsunto[i]})
		);
		await fetch(this.state.uri + '/api/borrar/asuntos', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al borrar asunto',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Asuntos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			await this.borrarElAsunto(i);
		}
	}


	async alertBorrar(i) {
		Alert.alert('Borrar asunto', 'Está a punto de borrar un asunto\n¿Seguro que desea continuar?', [
			{
				text: 'Sí',
				onPress: () => this.borrarAsuntos(i)
			},
			{text: 'No'}

		]);
	}

	async getUserdata() {
		let admin = await fetch(this.state.uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let adminId = await admin.json();
		await this.setState({data: adminId});
		if (this.state.data.puesto === 'Coordinador Academico') {
			this.state.elTipoAsunto = 'Académicas';
		}
		if (this.state.data.puesto === 'Secretaria') {
			this.state.elTipoAsunto = 'Administrativas';
		}
		if (this.state.data.puesto === 'SubDirector') {
			this.state.elTipoAsunto = 'Directivo';
		}
		if (this.state.data.puesto === 'Director') {
			this.state.elTipoAsunto = 'Institucional';
		}
		if (this.state.data.puesto === 'Cordisi') {
			this.state.elTipoAsunto = 'Conductual';
		}
	}

	async borrarElAsunto(indexAsunto) {
		this.state.elAsunto.splice(indexAsunto, 1);
		this.setState({aux: 0});
	}

	async addAsunto() {
		this.state.elAsunto.push({
			asunto: this.state.asuntoCord,
			tipo:
				this.state.onlyPers === true
					? 'Admin'
					: 'Padre'
		});
		this.setState({aux: 0});
	}

	async getAsuntos() {
		let asnto = await fetch(
			this.state.uri + '/api/get/view/asuntos/'
			+ this.state.data.puesto,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			});
		let asuntos = await asnto.json();
		await this.setState({elAsunto: asuntos});
		this.state.elAsunto.forEach((item, i) => {
			this.state.elIdAsunto.push(item.id);
			this.state.isModalAsunt.push(false);
		});
	}

	async openModal(i) {
		this.state.isModalAsunt[i] = true;
		this.state.asuntoCord = this.state.elAsunto[i].asunto;
		await this.setState({aux: 0});
	}

	async saveAsunto() {
		if (this.state.asuntoCord === '') {
			Alert.alert('Sin asunto definido', 'No ha escrito algún asunto', [
				{text: 'Entendido'}
			]);
		} else {
			Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
				{
					text: 'Sí',
					onPress: () => [this.requestMultipart()]
				},
				{text: 'No'}

			]);
		}

	}

	async closeModal(i) {
		this.state.isModalAsunt[i] = false;
		await this.setState({aux: 0});
	}

	renderAsunto(i) {
		return (
			<View
				key={i + 'rndAsunto'}
				style={[
					styles.diasLab,
					styles.btn1,
					styles.row,
					{borderColor: this.state.fourthColor}
				]}
			>
				<Modal
					isVisible={this.state.isModalAsunt[i]}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
						<Text
							style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
						>
							Edita el asunto
						</Text>
						<TextInput
							keyboardType='default'
							maxLength={100}
							multiline={false}
							onChangeText={text => (this.state.asuntoCord = text)}
							placeholder='Nombra al grupo'
							returnKeyType='done'
							underlineColorAndroid='transparent'
							defaultValue={this.state.asuntoCord}
							style={[
								styles.inputPicker,
								styles.modalWidth,
								{borderColor: this.state.secondColor, textAlign: 'center'}
							]}
						/>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => [(this.state.isModalAsunt[i] = false), this.setState({aux: 0})]}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.updateAsunto(i)}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<Text style={{fontSize: responsiveFontSize(2)}}>
					<Text style={[styles.textW, {fontSize: responsiveFontSize(2)}]}>
						Asunto:
					</Text>
					{'\n'}
					{this.state.elAsunto[i].asunto}
				</Text>
				<View
					style={[styles.btn6, styles.row]}
				>
					<MaterialIcons name='edit' size={25} color='grey' onPress={() => this.openModal(i)}/>
					<Entypo name='circle-with-minus' size={25} color='red' onPress={() => this.alertBorrar(i)}/>
				</View>
			</View>
		);
	}

	renderAsuntos() {
		let btnAsunto = [];
		for (let i = 0; i < this.state.elAsunto.length; i++) {
			btnAsunto.push(this.renderAsunto(i));
		}
		return btnAsunto;
	}

	render() {
		const asuntosss = this.renderAsuntos();
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Escriba los asuntos a tratar en citas
				</Text>
				<TextInput
					keyboardType='default'
					maxLength={254}
					onChangeText={text => (this.state.asuntoCord = text)}
					placeholder={'Título del asunto'}
					returnKeyType='next'
					underlineColorAndroid='transparent'
					style={[
						styles.inputPicker,
						{
							paddingHorizontal: 15,
							borderColor: this.state.secondColor
						}
					]}
				/>
				<TouchableOpacity
					style={[
						styles.widthall,
						{alignItems: 'center', justifyContent: 'center', marginVertical: 10}
					]}
					onPress={() => this.saveAsunto()}
				>
					<Entypo name='circle-with-plus' size={40} color='#43d551'/>
				</TouchableOpacity>
				<Text
					style={[
						styles.main_title,
						{color: this.state.thirdColor, marginBottom: 10, marginTop: 0}
					]}
				>
					Lista de asuntos
				</Text>
				<ScrollView>{asuntosss}</ScrollView>
			</View>
		);
	}
}
