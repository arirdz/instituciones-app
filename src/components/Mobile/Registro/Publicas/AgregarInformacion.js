import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight, Alert
} from 'react-native';
import {
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import Swipeable from 'react-native-swipeable';
import Ionicons from "react-native-vector-icons/Ionicons";
import Fumi from "react-native-textinput-effects/lib/Fumi";
import {Actions} from "react-native-router-flux";
import ModalSelector from "react-native-modal-selector";

export default class AgregarInformacion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            sitio_web: '',
            turnos: []
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getTurnos();
        this.setState({tel_escuela: this.props.laEscuela.telefono})
        this.setState({no_alumnos: this.props.laEscuela.alumnos_total})
        console.log(this.props.laEscuela.id);
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    getTurnos(){
        fetch('https://gestion-dev.controlescolar.pro/api/get/turnos/' + this.props.laEscuela.clave_centro_trabajo, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    console.log(responseJson)
                    this.setState({turnos: responseJson})
                }
            });
    }

    async requestMultipart(){
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(this.state.correo) === false) {
            Alert.alert('Email inválido', 'ingresa un email válido para la escuela');
            return false;
        }
        if (reg.test(this.state.contacto_email) === false) {
            Alert.alert('Email inválido', 'ingresa un email válido para la persona de contacto principal');
            return false;
        }
        if (this.state.correo === undefined || this.state.contacto_nombre === undefined ||
            this.state.contacto_cargo === undefined || this.state.contacto_email === undefined ||
            this.state.contacto_tel === undefined || this.state.zona === undefined
            || this.state.sector === undefined || this.state.modalidad === undefined
            || this.state.turno === undefined
        ) {
            Alert.alert('Error', 'Verifique que se hayan llenado todos los campos', [{text: 'Enterado'}]);
        } else {
            fetch('https://gestion-dev.controlescolar.pro/api/realizar/solicitud', {
                method: 'post',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }, body: JSON.stringify({
                    id_escuela_mexico: this.props.laEscuela.id,
                    // nombre_escuela: this.state.nombre_escuela,
                    // calle: this.state.calle,
                    // entre_calle: this.state.entre_calle,
                    // colonia: this.state.colonia,
                    // cp: this.state.cp,
                    // ciudad: this.state.ciudad,
                    // estado: this.state.estado,
                    // correo: this.state.correo,
                    // tel_escuela: this.state.tel_escuela,
                    // clave_escuela: this.state.clave_escuela,
                    // nivel: this.state.nivel,
                    zona: this.state.zona,
                    sector: this.state.sector,
                    modalidad: this.state.modalidad,
                    turno: this.state.turno,
                    contactar: this.state.contactar,
                    correo: this.state.correo,
                    tel_escuela: this.state.tel_escuela,
                    no_campus: this.state.no_campus,
                    // nombre_campus_uno: this.state.nombre_campus,
                    no_alumnos: this.state.no_alumnos,
                    contacto_nombre: this.state.contacto_nombre,
                    contacto_cargo: this.state.contacto_cargo,
                    contacto_email: this.state.contacto_email,
                    contacto_tel: this.state.contacto_tel,
                    // primario: this.state.primario,
                    // secundario: this.state.secundario,
                    // terciario: this.state.terciario,
                    // img: this.state.img,
                    // logo: this.state.logo,
                    //comentarios: this.state.comentarios,
                    sitio_web: this.state.sitio_web,
                })
            }).then(res => res.json())
                .then(responseJson => {
                    console.log(responseJson)
                    if (responseJson.error !== undefined) {
                        Alert.alert('Error', 'Ha ocurrido un error, intente más tarde', [{text: 'Enterado'}]);
                    } else {
                        Alert.alert('Felicidades', 'Sus datos se guardaron con éxito', [{
                            text: 'OK',
                            onPress: () => [this.irComentarios()]
                        }]);
                    }
                });
        }
    }

    onchangeModalidad(option){
        this.setState({modalidad: option.label})

    }
    onchangeTurno(option){
        this.setState({turno: option.label})

    }
    onchangePreferencia(option){
        this.setState({contactar: option.label})
    }

    irComentarios(){
        console.log('comen'+this.props.laEscuela.id)
        Alert.alert('Alerta', '¿Desea realizar algún comentario adicional?', [{
            text: 'Sí',
            onPress: () => [Actions.Comentario({id_escuela_mexico: this.props.laEscuela.id})]
        }, {text: 'No', onPress: () => [this.finalizado()]}]);
}

    async finalizado() {
        await this.setState({ran: (Math.random().toString(36).substr(2, 5))})
        await console.log(this.state.ran)
        await AsyncStorage.setItem('ya_registro', this.state.ran);
        let yaRegistro = await AsyncStorage.getItem('ya_registro');
        await Actions.Bienvenida()

    }

    render() {
        const data  = [
            {key: 1, label: "Escolarizado"},
            {key: 2, label: "Semiescolarizado"},
            {key: 3, label: "Abierto"},
            {key: 4, label: "A distancia u online"},
        ];
        const data3  = [
            {key: 1, label: "E-mail"},
            {key: 2, label: "Llamada telefónica"},
            {key: 3, label: "Whatsapp"},
        ];
        const datosTurno = this.state.turnos.map((item,i) => {
            return {key: i, label: item.nombre_turno}
        })
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <Text style={[styles.title1, {color: this.state.thirdColor}]}>
                            {this.props.laEscuela.nombre_centro_trabajo}
                        </Text>
                        <Text style={{marginTop: 0, textAlign: 'center'}}>{this.props.laEscuela.nivel_educativo}</Text>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Datos generales de la institución
                        </Text>
                        <Fumi
                            label={'E-mail de la escuela'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-mail'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.correo = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <Fumi
                            label={'Número telefónico de la escuela'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.tel_escuela = text)}
                            keyboardType='numeric'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                            maxLength={10}
                        />
                        <Fumi
                            label={'Dirección'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            // onChangeText={text => (this.state.no_campus = text)}
                            keyboardType='numeric'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                            defaultValue={this.props.laEscuela.domicilio}
                        />
                        <Fumi
                            label={'Entre la calle'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            // onChangeText={text => (this.state.nombre_campus_uno = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                            defaultValue={this.props.laEscuela.entre_la_calle}
                        />
                        <Fumi
                            label={'Colonia'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            // onChangeText={text => (this.state.nombre_campus_uno = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                            defaultValue={this.props.laEscuela.nombre_colonia}
                        />
                        <Fumi
                            label={'Zona escolar'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.zona = text)}
                            keyboardType='numeric'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <Fumi
                            label={'Sector escolar'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.sector = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <ModalSelector
                            data={data}
                            selectStyle={[
                                [styles.inputPicker, {borderColor: '#126775', backgroundColor: 'transparent'}],
                                {marginTop: 10}
                            ]}
                            cancelText='Cancelar'
                            optionTextStyle={{color: '#75745a'}}
                            initValue='Seleccione la modalidad de su escuela'
                             onChange={option => this.onchangeModalidad(option)}
                        />
                        <ModalSelector
                            data={datosTurno}
                            selectStyle={[
                                [styles.inputPicker, {borderColor: '#126775', backgroundColor: 'transparent'}],
                                {marginTop: 10}
                            ]}
                            cancelText='Cancelar'
                            optionTextStyle={{color: '#75745a'}}
                            initValue='¿Cuál es el turno de su escuela?'
                            onChange={option => this.onchangeTurno(option)}
                        />
                        <ModalSelector
                            data={data3}
                            selectStyle={[
                                [styles.inputPicker, {borderColor: '#126775', backgroundColor: 'transparent'}],
                                {marginTop: 10}
                            ]}
                            cancelText='Cancelar'
                            optionTextStyle={{color: '#75745a'}}
                            initValue='¿Como prefiere que lo contactemos?'
                            onChange={option => this.onchangePreferencia(option)}
                        />
                        <Fumi
                            label={'Persona de contacto principal'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor, marginTop: 15}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.contacto_nombre = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <Fumi
                            label={'Cargo de la persona de contacto'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.contacto_cargo = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <Fumi
                            label={'E-mail de la persona de contacto'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.contacto_email = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <Fumi
                            label={'Teléfono de la persona de contacto'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.contacto_tel = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                            maxLength={10}
                        />
                        <Fumi
                            label={'Número de alumnos de la escuela'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.no_alumnos = text)}
                            keyboardType='default'
                            // value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                            defaultValue={this.props.laEscuela.alumnos_total}
                        />
                        <TouchableOpacity
                            onPress={() => this.requestMultipart()}
                            style={[
                                styles.bigButton,
                                {
                                    backgroundColor: this.state.secondColor,
                                    borderRadius: 8,
                                    width: responsiveWidth(80),
                                    margin: 20,
                                    shadowColor: 'lightgrey',
                                    shadowOpacity: 1,
                                    shadowOffset: {width: 5, height: 5}
                                }
                            ]}
                        >
                            <Text style={styles.textButton}>Enviar</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
