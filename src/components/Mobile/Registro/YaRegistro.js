import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight, Image
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';
import {Actions} from "react-native-router-flux";

export default class YaRegistro extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <View style={styles.container}>
                    <Image
                        style={{width: 200, height: 200, marginTop: 35}}
                        source={{
                            uri: 'https://img.icons8.com/color/384/000000/verified-account.png'
                        }}
                    />
                <View
                    style={{
                        width: responsiveWidth(94),
                        alignItems: 'center',
                        justifyContent: 'center',
                        margin: 10,
                        marginTop: 0
                    }}>

                    <Text
                        style={{
                            textAlign: 'center',
                            color: this.state.secondColor,
                            backgroundColor: 'transparent',
                            fontWeight: '700',
                            fontSize: responsiveFontSize(4)
                        }}>
                        ¡Felicidades! {"\n"} Hemos recibido su solicitud de registro
                    </Text>
                    <Text style={{margin:25,
                        textAlign: 'center',
                        color: this.state.mainColor,
                        backgroundColor: 'transparent',
                        fontWeight: '500',
                        fontSize: responsiveFontSize(2)}}>Pronto uno de nuestros asesores se pondrá en contacto con usted para terminar el proceso de personalización</Text>
                    <TouchableOpacity
                        onPress={() => [Actions.infoCE()]}
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.secondColor,
                                borderRadius: 8,
                                width: responsiveWidth(90),
                                marginTop: 140,
                                shadowColor: 'lightgrey',
                                shadowOpacity: 1,
                                shadowOffset: {width: 5, height: 5}
                            }
                        ]}
                    >
                        <Text style={styles.textButton}>Registrar otra escuela</Text>
                    </TouchableOpacity>
                </View>
                </View>
            </View>
        );
    }
}
