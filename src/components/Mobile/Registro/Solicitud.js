import React from 'react';
import {Alert, AsyncStorage, Image, Linking, Platform, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from '../../styles';
import {responsiveHeight, responsiveWidth, responsiveFontSize} from 'react-native-responsive-dimensions';
import Communications from 'react-native-communications';
import {MediaQuery} from 'react-native-responsive';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {Actions} from "react-native-router-flux";
import Ionicons from "react-native-vector-icons/Ionicons";
import StepIndicator from 'rn-step-indicator';
import Fumi from "react-native-textinput-effects/lib/Fumi";
import MultiBotonRow from "./../Globales/MultiBotonRow";
import Spinner from "react-native-loading-spinner-overlay";

const labels_estForm = [
    'Agregar\ninformación',
    'Seleccionar\nplan',
    'Agregar\ncomentarios',
];

const customStyles = {
    stepIndicatorSize: 40,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#777777',
    separatorFinishedColor: '#16a900',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#16a900',
    stepIndicatorUnFinishedColor: '#aaaaaa',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: responsiveFontSize(1.3),
    currentStepIndicatorLabelFontSize: responsiveFontSize(1.3),
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
    labelColor: '#666666',
    labelSize: responsiveFontSize(1.5),
    currentStepLabelColor: '#777777'
};

export default class Solicitud extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            escuelasDatos: [],
            uri: 'https://controlescolar.pro',
            mainColor: '#263238',
            secondColor: '#116876',
            thirdColor: '#75745A',
            fourthColor: '#E6E6E6',
            botonSelected: 'Datos',
            indexSelected: 0,
            escuela: [],
            cct: '',
            procesoPriv: 0
        }
    }

    async componentWillMount() {

    }

    async requestMultipart(){
        if (this.state.cct === '') {
            Alert.alert('CCT/RVOE vacío', 'Por favor ingrese el CCT para poder solicitar el registro de su escuela', [{text: 'Enterado'}]);
        }else if (this.state.cct.length !== 10){
            Alert.alert('CCT/RVOE inválido', 'Debe contener 10 caracteres', [{text: 'Enterado'}]);
        }else{
        await this.setState({visible: true})
        await fetch('https://gestion-dev.controlescolar.pro/api/get/escuela/' + this.state.cct, {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    this.setState({visible: false})
                    console.log(responseJson)
                } else {
                    console.log(responseJson)
                    if(responseJson.length === 0){
                        Alert.alert('Error', 'El CCT/RVOE es incorrecto, por favor verifíquelo ', [{text: 'Enterado', onPress:() => this.setState({visible: false})}]);
                    }else{
                        this.setState({visible: false})
                    this.setState({escuela: responseJson})
                    if(this.state.escuela[0].nombre_control === 'PÚBLICO'){
                        this.setState({ menu: 1})
                        if(this.state.escuela[0].proceso_solicitud === null) {
                            this.setState({procesoPub: 0})
                        }else if(this.state.escuela[0].proceso_solicitud === 1){
                            this.setState({procesoPub: 1})
                        }else if(this.state.escuela[0].proceso_solicitud === 2) {
                            this.setState({procesoPub: 2})
                        }
                    }else if(this.state.escuela[0].nombre_control === 'PRIVADO'){
                        this.setState({ menu: 2})
                        if(this.state.escuela[0].proceso_solicitud === null) {
                            this.setState({procesoPriv: 1})
                        }else if(this.state.escuela[0].proceso_solicitud === 1){
                            this.setState({procesoPriv: 2})
                        }else if(this.state.escuela[0].proceso_solicitud === 2) {
                            this.setState({procesoPriv: 3})
                        }
                    }
                }
                }
            });
        }
    }

    render() {
        return (
            <View
                style={styles.container}>
                <Spinner visible={this.state.visible} textContent='Buscando el Centro de Trabajo...'/>
                <StatusBar
                    backgroundColor={this.state.secondColor}
                    barStyle={'light-content'}
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    ¿Cuál es el CCT/RVOE de la escuela?
                </Text>
                <Fumi
                    label={'CCT/RVOE'}
                    iconClass={Ionicons}
                    style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                    labelStyle={{color: this.state.mainColor}}
                    iconName={'md-person'}
                    inputStyle={{color: this.state.secondColor}}
                    iconColor={this.state.mainColor}
                    iconSize={20}
                    onChangeText={text => (this.state.cct = text)}
                    keyboardType='default'
                    value={this.state.cct}
                    returnKeyType='next'
                    autoCapitalize='none'
                    autoCorrect={false}
                />
                <TouchableOpacity
                    onPress={() => this.requestMultipart()}
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: this.state.secondColor,
                            borderRadius: 8,
                            width: responsiveWidth(94),
                            margin: 20,
                            shadowColor: 'lightgrey',
                            shadowOpacity: 1,
                            shadowOffset: {width: 5, height: 5}
                        }
                    ]}
                >
                    <Text style={styles.textButton}>Enviar</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
