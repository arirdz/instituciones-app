import React from 'react';
import {Alert, AsyncStorage, Image, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../../styles';
import {Actions} from "react-native-router-flux";
import Modal from "react-native-modal";
import ModalSelector from "react-native-modal-selector";

export default class Planes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            planes: [],
            esencial: false,
            profesional: false,
            avanzado: false,
            ran: '',
            elplan: 'Avanzado',
            mesesBTN: [{"meses": "12"}, {"meses": "18"}, {"meses": "24"}, {"meses": "36"}],
            funcionalidades: [],
            meses: false,
            elmes: "",
            indexMes: 3

        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    seleccionarPlan(plan) {
        this.setState({elplan: plan})
    }


    vistaComentarios() {
        this.setState({esencial: false})
        this.setState({profesional: false})
        this.setState({avanzado: false})
        Actions.Comentario({id_escuela_mexico: this.props.id_escuela_mexico});
    }

    async finalizado() {
        await this.setState({ran: (Math.random().toString(36).substr(2, 5))})
        await console.log(this.state.ran)
        await AsyncStorage.setItem('ya_registro', this.state.ran);
        let yaRegistro = await AsyncStorage.getItem('ya_registro');
        await Actions.replace('YaRegistro', {key: Math.random()})

    }


    requestMultipart() {
        if (this.state.elplan === '') {
            Alert.alert('Error', 'Seleccione un plan para continuar la solicitud', [{
                text: 'OK',
            }]);
        } else {
            Alert.alert('Atención', '¿Está seguro que desea seleccionar el plan ' + this.state.elplan + '?', [{
                text: 'Sí',
                onPress: () => [
                    this.setState({meses: true}),
                    fetch('https://gestion-dev.controlescolar.pro/api/update/solicitud/' + this.props.id_escuela_mexico + '/plan/' + this.state.elplan, {
                        method: 'GET', headers: {
                            Accept: 'application/json',
                            'Content-Type': 'multipart/form-data',
                        }
                    }).then(res => res.json())
                ]
                // this.irComentarios()]
            }, {text: 'No'}]);
        }
    }

    guardarMeses(mes) {
        fetch('https://gestion-dev.controlescolar.pro/api/update/solicitud/' + this.props.id_escuela_mexico + '/meses_solicitados/' + mes, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    console.log(responseJson)
                    Alert.alert('Alerta', '¿Desea realizar algún comentario adicional?', [{
                        text: 'Sí',
                        onPress: () => [this.vistaComentarios(), this.setState({meses: false})]
                    }, {text: 'No', onPress: () => [this.finalizado(), this.setState({meses: false})]}]);
                }
            })
        }

    getFuncionalidades(item) {
        fetch('https://gestion-dev.controlescolar.pro/api/get/funcionalidades/' + item, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    this.setState({funcionalidades: responseJson})
                    if (item === 1) {
                        this.setState({esencial: true})
                    } else if (item === 2) {
                        this.setState({profesional: true})
                    } else {
                        this.setState({avanzado: true})
                    }
                }
            });
    }

    renderCards() {
        let cards = [];
        this.state.funcionalidades.forEach((item, i) => {
            cards.push(
                <View style={[styles.row, {
                    // height: responsiveHeight(4, 5),
                    width: responsiveWidth(65),
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    borderColor: 'gray',
                    backgroundColor: "#f1f1f1",
                    borderWidth: 1,
                    borderRadius: 5,
                    marginTop: 5,
                    padding: 5,
                }]}>
                    <Image
                        style={{marginLeft: 15, height: 30, width: 30}}
                        source={{uri: item.url}}
                    />
                    <Text style={{textAlign: 'center', marginLeft: 10}}>{item.funcionalidad}</Text>
                </View>
            )
        });
        return cards;
    }

    selectedMes(item, i) {
        this.setState({elmes: item.mes, indexMes: i})
        // this.guardarMeses(item.mes)
    }

    renderMesesBTN() {
        let cards = [];
        this.state.mesesBTN.forEach((item, i) => {
            let indexBTN = this.state.indexMes;
            cards.push(
                <TouchableOpacity style={[styles.btn4, {
                    // height: responsiveHeight(4, 5),
                    // width: responsiveWidth(22),
                    height: responsiveHeight(5),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor: 'gray',
                    backgroundColor: indexBTN === i ? this.state.mainColor : "#f1f1f1",
                    borderWidth: .3,
                    borderRadius: 5,
                    marginTop: 5,
                    padding: 5,
                }]}

                                  onPress={() => this.selectedMes(item, i)}

                >
                    <Text style={{
                        color: indexBTN === i ? this.state.fourthColor : this.state.mainColor,
                        textAlign: 'center',
                        marginLeft: 0,
                        fontSize: responsiveFontSize(3),
                        fontWeight: "700"
                    }}>{item.meses}</Text>
                </TouchableOpacity>
            )
        });
        return cards;
    }


    render() {
        // const data = [
        //     {key: 2, label: "12 Meses", value: "12"},
        //     {key: 3, label: "18 Meses", value: "18"},
        //     {key: 4, label: "24 Meses", value: "24"},
        //     {key: 5, label: "36 Meses", value: "36"},
        // ];
        let estiloEsencial = {
            alignItems: 'center',
            marginTop: 10,
            width: responsiveWidth(75),
            // borderColor: 'lightgrey',
            // borderWidth: 2,
            borderRadius: 12,
            backgroundColor: "#BABABA",

        }
        if (this.state.elplan === 'Esencial') {
            estiloEsencial = {
                alignItems: 'center',
                marginTop: 10,
                width: responsiveWidth(75),
                // borderColor: this.state.mainColor,
                // borderWidth: 4,
                borderRadius: 12,
                backgroundColor: this.state.mainColor,
            }
        }
        let estiloProfesional = {
            alignItems: 'center',
            marginTop: 10,
            width: responsiveWidth(75),
            // borderColor: 'lightgrey',
            // borderWidth: 2,
            borderRadius: 12,
            backgroundColor: "#BABABA",

        }
        if (this.state.elplan === 'Profesional') {
            estiloProfesional = {
                alignItems: 'center',
                marginTop: 10,
                width: responsiveWidth(75),
                // borderColor: this.state.mainColor,
                // borderWidth: 4,
                borderRadius: 12,
                backgroundColor: this.state.mainColor,
            }
        }
        let estiloAvanzado = {
            alignItems: 'center',
            marginTop: 10,
            width: responsiveWidth(90),
            // borderColor: this.state.secondColor,
            // borderWidth: .5,
            borderRadius: 12,
            backgroundColor: "#BABABA",
        }
        if (this.state.elplan === 'Avanzado') {
            estiloAvanzado = {
                alignItems: 'center',
                marginTop: 10,
                width: responsiveWidth(90),
                // borderColor: this.state.mainColor,
                // borderWidth: 4,
                borderRadius: 12,
                backgroundColor: this.state.mainColor,
            }
        }

        return (
            <View style={[styles.container, {alignItems: "center"}]}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 10}]}>Seleccione un plazo de
                    contratación:</Text>
                <View style={[styles.widthall, styles.row, {alignItems: 'center'}]}>
                    {this.renderMesesBTN()}
                </View>
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 10}]}>Seleccione un plan de
                    servicio:</Text>
                <View style={[styles.widthall, {alignItems: 'center'}]}>
                    <TouchableOpacity onPress={() => this.seleccionarPlan('Esencial')}
                                      style={estiloEsencial}
                    >
                        {/*<Modal*/}
                            {/*isVisible={this.state.meses}*/}
                            {/*backdropOpacity={0.8}*/}
                            {/*animationIn={'bounceIn'}*/}
                            {/*animationOut={'bounceOut'}*/}
                            {/*animationInTiming={1000}*/}
                            {/*animationOutTiming={1000}*/}
                        {/*>*/}
                            {/*<View*/}
                                {/*style={[styles.container, {margin: 0, borderRadius: 6, flex: 0, paddingVertical: 20}]}>*/}
                                {/*<Text style={[{*/}
                                    {/*marginVertical: 5,*/}
                                    {/*color: 'black',*/}
                                    {/*fontSize: responsiveFontSize(2.7),*/}
                                    {/*fontWeight: 'bold'*/}
                                {/*}]}>*/}
                                    {/*Seleccionar duración del plan*/}
                                {/*</Text>*/}
                                {/*<ModalSelector*/}
                                    {/*data={data}*/}
                                    {/*selectStyle={[*/}
                                        {/*[{*/}
                                            {/*borderColor: '#126775',*/}
                                            {/*backgroundColor: 'transparent',*/}
                                            {/*width: responsiveWidth(70)*/}
                                        {/*}],*/}
                                        {/*{marginTop: 10}*/}
                                    {/*]}*/}
                                    {/*cancelText='Cancelar'*/}
                                    {/*optionTextStyle={{color: '#75745a'}}*/}
                                    {/*initValue='Seleccione los meses del plan'*/}
                                    {/*onChange={option => this.onchangeMeses(option)}*/}
                                {/*/>*/}
                                {/*<TouchableOpacity*/}
                                    {/*onPress={() => this.guardarMeses()}*/}
                                    {/*style={[*/}
                                        {/*styles.bigButton,*/}
                                        {/*{*/}
                                            {/*backgroundColor: this.state.secondColor,*/}
                                            {/*borderRadius: 8,*/}
                                            {/*width: responsiveWidth(70),*/}
                                            {/*marginTop: 50,*/}
                                            {/*shadowColor: 'lightgrey',*/}
                                            {/*shadowOpacity: 1,*/}
                                            {/*shadowOffset: {width: 5, height: 5}*/}
                                        {/*}*/}
                                    {/*]}*/}
                                {/*>*/}
                                    {/*<Text style={styles.textButton}>Enviar</Text>*/}
                                {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                        {/*</Modal>*/}

                        <Modal
                            isVisible={this.state.esencial}
                            backdropOpacity={0.8}
                            animationIn={'bounceIn'}
                            animationOut={'bounceOut'}
                            animationInTiming={1000}
                            animationOutTiming={1000}
                        >
                            <View
                                style={[styles.container, {margin: 0, borderRadius: 6, flex: 0, paddingVertical: 20}]}>
                                <Text style={[{
                                    marginVertical: 5,
                                    color: 'black',
                                    fontSize: responsiveFontSize(2.7),
                                    fontWeight: 'bold'
                                }]}>
                                    Plan Esencial
                                </Text>
                                <View style={{marginBottom: 15, paddingHorizontal: 0, alignItems: 'center'}}>
                                    <Text style={[styles.main_title, {
                                        textAlign: 'center',
                                        width: responsiveWidth(85),
                                        margin: 0,
                                        padding: 0
                                    }]}>Funcionalidades
                                        incluídas:</Text>
                                    <View style={{marginTop: 10, paddingHorizontal: 0, alignItems: 'center'}}>
                                        {this.renderCards()}
                                    </View>
                                </View>
                                <View style={[styles.row, {alignItems: 'center'}]}>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                        ]}
                                        onPress={() => this.setState({esencial: false})}
                                    >
                                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                        <Modal
                            isVisible={this.state.avanzado}
                            backdropOpacity={0.8}
                            animationIn={'bounceIn'}
                            animationOut={'bounceOut'}
                            animationInTiming={1000}
                            animationOutTiming={1000}
                        >
                            <View style={[styles.container, {borderRadius: 6, flex: 0, paddingVertical: 20}]}>
                                <Text style={[{
                                    color: 'black',
                                    fontSize: responsiveFontSize(2.7),
                                    fontWeight: 'bold'
                                }]}>
                                    Avanzado
                                </Text>
                                <View style={{alignItems: 'center', marginVertical: 0}}>
                                    <Text style={[styles.main_title, {
                                        textAlign: 'center',
                                        width: responsiveWidth(85),
                                    }]}>Incluye todo lo del
                                        plan Profesional, más:</Text>
                                    <View style={{marginVertical: 10, paddingHorizontal: 0, alignItems: 'center'}}>
                                        {this.renderCards()}
                                    </View>
                                </View>
                                <View style={[styles.row, {alignItems: 'center'}]}>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                        ]}
                                        onPress={() => this.setState({avanzado: false})}
                                    >
                                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                        <Modal
                            isVisible={this.state.profesional}
                            backdropOpacity={0.8}
                            animationIn={'bounceIn'}
                            animationOut={'bounceOut'}
                            animationInTiming={1000}
                            animationOutTiming={1000}
                        >
                            <View style={[styles.container, {borderRadius: 6, flex: 0, paddingVertical: 20}]}>
                                <Text style={[{
                                    color: 'black', fontSize: responsiveFontSize(2.7), fontWeight: 'bold'
                                }]}>
                                    Profesional
                                </Text>
                                <View style={{marginVertical: 10, alignItems: 'center'}}>
                                    <Text
                                        style={[styles.main_title, {textAlign: 'center', width: responsiveWidth(85)}]}>Incluye
                                        todo lo del
                                        plan Esencial, más:</Text>
                                    <View style={{marginVertical: 10, paddingHorizontal: 0, alignItems: 'center'}}>
                                        {this.renderCards()}
                                    </View>
                                </View>
                                <View style={[styles.row, {alignItems: 'center'}]}>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                        ]}
                                        onPress={() => this.setState({profesional: false})}
                                    >
                                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                        <View style={{
                            width: responsiveWidth(75),
                            marginTop: 10,
                            alignItems: 'center',
                        }}>
                            <Text style={[{
                                color: this.state.fourthColor, fontSize: responsiveFontSize(2.7), fontWeight: 'bold'
                            }]}>
                                Esencial
                            </Text>
                        </View>
                        <Text style={{fontSize: responsiveFontSize(4), color: this.state.secondColor}}>
                            ${this.props.planes[0].precio}.00
                        </Text>
                        <Text style={{color: this.state.fourthColor, fontSize: responsiveFontSize(1.5)}}>
                            por alumno/mes
                        </Text>
                        <TouchableOpacity
                            onPress={() => this.getFuncionalidades(1)}
                            style={[
                                {
                                    backgroundColor: '#fff',
                                    borderColor: this.state.secondColor,
                                    marginVertical: 10,
                                    borderRadius: 8,
                                    borderWidth: 1,
                                    padding: 5,
                                    width: responsiveWidth(50),
                                    alignItems: 'center'
                                }
                            ]}
                        >
                            <Text style={[styles.textW, {color: this.state.secondColor}]}>Ver detalles</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.seleccionarPlan('Avanzado')}
                                      style={estiloAvanzado}>
                        <View style={{
                            width: responsiveWidth(90),
                            alignItems: 'center',
                            marginTop: 10
                        }}>
                            <Text style={[{
                                color: this.state.fourthColor, fontSize: responsiveFontSize(3), fontWeight: 'bold'
                            }]}>
                                Avanzado
                            </Text>
                        </View>
                        <Text
                            style={{fontSize: responsiveFontSize(4.5), fontWeight: "900", color: this.state.secondColor}}>
                            ${this.props.planes[2].precio}.00
                        </Text>
                        <Text style={{fontSize: responsiveFontSize(1.5), color: this.state.fourthColor}}>
                            por alumno/mes
                        </Text>
                        <TouchableOpacity
                            onPress={() => this.getFuncionalidades(3)}
                            style={[
                                {
                                    backgroundColor: '#fff',
                                    borderColor: this.state.secondColor,
                                    marginTop: 5,
                                    marginBottom: 20,
                                    borderRadius: 8,
                                    borderWidth: 1,
                                    padding: 5,
                                    width: responsiveWidth(50),
                                    alignItems: 'center'
                                }
                            ]}
                        >
                            <Text style={[styles.textW, {
                                fontSize: responsiveFontSize(2),
                                color: this.state.secondColor
                            }]}>Ver detalles</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.seleccionarPlan('Profesional')}
                                      style={estiloProfesional}
                    >
                        <View style={{
                            width: responsiveWidth(75),
                            marginTop: 10,
                            alignItems: 'center',
                        }}>
                            <Text style={[{
                                color: this.state.fourthColor, fontSize: responsiveFontSize(2.7), fontWeight: 'bold'
                            }]}>
                                Profesional
                            </Text>
                        </View>
                        <Text style={{fontSize: responsiveFontSize(4), color: this.state.secondColor}}>
                            ${this.props.planes[1].precio}.00
                        </Text>
                        <Text style={{color: this.state.fourthColor, fontSize: responsiveFontSize(1.5)}}>
                            por alumno/mes
                        </Text>
                        <TouchableOpacity
                            onPress={() => this.getFuncionalidades(2)}
                            style={[
                                {
                                    backgroundColor: '#fff',
                                    borderColor: this.state.secondColor,
                                    marginVertical: 10,
                                    borderRadius: 8,
                                    borderWidth: 1,
                                    padding: 5,
                                    width: responsiveWidth(50),
                                    alignItems: 'center'
                                }
                            ]}
                        >
                            <Text style={[styles.textW, {color: this.state.secondColor}]}>Ver detalles</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={() => this.requestMultipart()}
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: this.state.secondColor,
                            borderRadius: 8,
                            width: responsiveWidth(90),
                            marginTop: 15,
                            shadowColor: 'lightgrey',
                            shadowOpacity: 1,
                            shadowOffset: {width: 5, height: 5}
                        }
                    ]}
                >
                    <Text style={styles.textButton}>Guardar y enviar</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
