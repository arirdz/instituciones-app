import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight
} from 'react-native';
import {
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';

export default class consultaNotif extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                  <Text>HOLA WE</Text>
                </ScrollView>
            </View>
        );
    }
}
