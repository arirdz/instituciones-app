import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight,
    TextInput, Alert
} from 'react-native';
import {
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {Actions} from "react-native-router-flux";

export default class Comentario extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    async componentWillMount() {
        await this.getURL();
        await console.log('props'+this.props.id_escuela_mexico)
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    requestMultipart(){
        console.log(this.state.comentario)
        Actions.HomeCE();
    }

    guardarComentario(plan) {
        console.log('http://127.0.0.1:8000/api/update/solicitud/' + this.props.id_escuela_mexico + '/comentario/' + this.state.comentario)
        fetch('http://127.0.0.1:8000/api/update/solicitud/' + this.props.id_escuela_mexico + '/comentario/' + this.state.comentario, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    Alert.alert('Felicidades',
                        'Se ha guardado tu comentario con éxito', [
                            {text: 'Enterado', onPress: ()=> this.finalizado()}
                        ]);
                }
            });
    }

    async finalizado() {
        await this.setState({ran: (Math.random().toString(36).substr(2, 5))})
        await console.log(this.state.ran)
        await AsyncStorage.setItem('ya_registro', this.state.ran);
        let yaRegistro = await AsyncStorage.getItem('ya_registro');
        await Actions.replace('YaRegistro',{key: Math.random()})

    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <View style={styles.widthall}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            ¿Tiene algún comentario adicional?
                        </Text>
                        <View >
                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholder="Cuéntanos detalladamente"
                                placeholderTextColor="grey"
                                numberOfLines={10}
                                multiline={true}
                                style={[styles.inputContenido, {
                                    borderColor: this.state.secondColor,
                                    backgroundColor: 'transparent',
                                    borderWidth: 2,
                                    heigth: responsiveHeight(50)
                                }]}
                                onChangeText={text => (this.state.comentario = text)}
                            />
                            <Text style={{textAlign: "justify", padding: 10}}>
                                Una vez enviada su solicitud, uno de nuestros compañeros de registro se comunicará con usted para afinar los últimos detalles. Por favor, manténgase al tanto. Si tuviese cualquier otra duda o pregunta sobre su registro, siéntase libre de escribirnos un e-mail a registro@controlescolar.pro
                            </Text>
                            <TouchableOpacity
                                onPress={() => this.guardarComentario()}
                                style={[
                                    styles.bigButton,
                                    {
                                        backgroundColor: this.state.secondColor,
                                        borderRadius: 8,
                                        width: responsiveWidth(80),
                                        margin: 20,
                                        shadowColor: 'lightgrey',
                                        shadowOpacity: 1,
                                        shadowOffset: {width: 5, height: 5}
                                    }
                                ]}
                            >
                                <Text style={styles.textButton}>Enviar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
            </View>
        );
    }
}
