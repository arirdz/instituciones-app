import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    TouchableOpacity,
    StatusBar,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Hijos from '../Globales/hijos';
import styles from '../../styles';
import MultiBotonRow from '../Globales/MultiBotonRow';
import {
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class uniformes extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            textInputValue: '',
            uri: '',
            grado: 'Todos',
            grupo: 'Todos',
            cososPicker: [],
            selectedIndex: null,
            ninosPicker: [],
            mes: moment().month() + 1,
            botonSelected: 'Pedido nuevo'
        };
        this.getFeed = this.getFeed.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getRole();
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    _keyExtractor = item => item.id;

    async getFeed() {
        let mes = this.state.mes;
        let grado = this.state.selGrad;
        let role = await AsyncStorage.getItem('role');
        let grupo = this.state.selGrup;
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(
            this.state.uri +
            '/api/feed/show/' +
            mes +
            '/' +
            role +
            '/' +
            grado +
            '/' +
            grupo,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        let data = await request.json();
        this.setState({data: data});
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            id: id
        });
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, selected: option.id});
    }

    isNotPapa() {
        return (
            <View>
                <Hijos
                    onSelectedChamaco={(index, grado, grupo, id) =>
                        this.onListItemPressed(index, grado, grupo, id)
                    }
                />
            </View>
        );
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    pedidoNuevo() {
        return (
            <View style={styles.container}>
                <View
                    style={{
                        marginTop: 20,
                        borderBottomWidth: 0.5,
                        width: responsiveWidth(94),
                        borderTopWidth: 0.5
                    }}>
                    <Text style={{color: 'red'}}>IMPORTANTE</Text>
                    <Text style={{width: responsiveWidth(94), textAlign: 'left'}}>
                        Una vez realizado el pago y el pedido, se hará entrega del uniforme
                        en las instalaciones de nuestra escuela en un periodo de 2 a 3
                        semanas. Una vez que esté listo para recogerse, le notificaremos
                        oficialmente para que pase a recogerlo. Gracias
                    </Text>
                </View>
            </View>
        );
    }

    pedidoAnterior() {
        return (
            <View style={styles.container}>
                <View
                    style={{
                        marginTop: 20,
                        borderBottomWidth: 0.5,
                        width: responsiveWidth(94),
                        borderTopWidth: 0.5,
                        height: responsiveHeight(20)
                    }}>
                    <Text style={{color: 'red', marginTop: 8}}>IMPORTANTE</Text>
                    <Text style={{width: responsiveWidth(94), textAlign: 'left'}}>
                        Estos son los pedidos anteriores
                    </Text>
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {this.state.admin === 'Padre' ? this.isNotPapa() : null}
                <View style={{marginTop: 5, width: responsiveWidth(94)}}>
                    <Text
                        style={[
                            styles.main_title,
                            {marginRight: 70, color: this.state.thirdColor, marginTop: 1}
                        ]}>
                        Seleccione la acción
                    </Text>
                    <MultiBotonRow
                        itemBtns={['Pedido nuevo', 'Pedido anterior']}
                        onSelectedButton={(indexBtn, itemBtn) =>
                            this.botonSelected(indexBtn, itemBtn)
                        }
                        cantidad={2}
                    />
                </View>
                {this.state.botonSelected === 'Pedido nuevo'
                    ? this.pedidoNuevo()
                    : null}
                {this.state.botonSelected === 'Pedido anterior'
                    ? this.pedidoAnterior()
                    : null}
                {this.state.botonSelected === 'Pedido nuevo' ? (
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.secondColor,
                                borderColor: this.state.secondColor
                            }
                        ]}
                        onPress={() => Actions.pedidoNuevo({id: this.state.id})}>
                        <Text style={styles.textButton}>Hacer pedido</Text>
                    </TouchableOpacity>
                ) : null}
            </View>
        );
    }
}
