import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import {
    responsiveWidth,
    responsiveFontSize
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import ModalDropdown from 'react-native-modal-dropdown';
import SimpleStepper from 'react-native-simple-stepper';
import ModalSelector from 'react-native-modal-selector';

export default class pedidoNuevo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            displayValue: [],
            lasCantidades: [],
            lasTallas: [],
            piloto: '',
            precioUnitario: [],
            subTotales: [0, 0, 0, 0, 0, 0],
            uniformes: [],
            pedidos: []
        };
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
        await this.userData();
        await this.getUnifTallas();
    }

    async componentWillMount() {
        await this.getURL();
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Hijo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async userData() {
        let dataa = await fetch(
            this.state.uri + '/api/data/user/' + this.props.id,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let data = await dataa.json();
        await this.setState({data: data});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Tallas +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getUnifTallas() {
        let talla = await fetch(this.state.uri + '/api/uniformes/tallas', {
            //let talla = await fetch(this.state.uri + '/api/get/uniforme/info/' + this.state.uniforme + '/' + this.state.talla, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let tallass = await talla.json();
        this.setState({tallas: tallass});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Nombres +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async getUnifNombres(tipo) {
        let nombre = await fetch(
            this.state.uri +
            '/api/uniformes/nombres/' +
            tipo +
            '/' +
            this.state.data[0].genero,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let nombres = await nombre.json();
        await this.setState({uniformes: nombres});

        await this.state.uniformes.forEach(() => {
            this.state.precioUnitario.push(0);
            this.state.lasTallas.push('0');
            this.state.displayValue.push(0);
            this.state.subTotales.push(0);
        });

        let a = this.state.uniformes.length;
        this.state.displayValue.length = a;

        this.state.subTotales.length = a;
    }

    renderCards() {
        let cards = [];
        this.state.uniformes.forEach((item, index) => {
            cards.push(this.renderUnifNombres(item, index));
        });
        return cards;
    }

    gala() {
        const uniformes = this.renderCards();
        return <View>{uniformes}</View>;
    }

    dep() {
        const uniformes = this.renderCards();
        return <View>{uniformes}</View>;
    }

    otrPr() {
        const uniformes = this.renderCards();
        return <View>{uniformes}</View>;
    }

    renderUnifNombres(item, index) {
        return (
            <View>
                <View
                    style={[
                        styles.cartPed,
                        {borderColor: 'white', backgroundColor: this.state.fourthColor}
                    ]}>
                    <View style={{flexDirection: 'row', width: responsiveWidth(93)}}>
                        <View
                            style={[
                                styles.nombPrend,
                                {backgroundColor: this.state.mainColor, borderColor: 'white'}
                            ]}>
                            <Text
                                style={[
                                    styles.textButton,
                                    {fontWeight: '700', color: this.state.fourthColor}
                                ]}>
                                {item}
                            </Text>
                        </View>
                        <View
                            style={[
                                styles.dispon,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    borderColor: 'white'
                                }
                            ]}>
                            <Text style={{fontWeight: '700'}}> Disponible: </Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', width: responsiveWidth(93)}}>
                        <View style={[styles.tallaPrend, {borderColor: 'white'}]}>
                            <Text style={{fontWeight: '700', marginTop: 3}}>Talla:</Text>
                            <ModalDropdown
                                onSelect={option => this.dropChange(option, index)}
                                options={this.state.tallas}
                                style={styles.dropPed}
                                textStyle={{
                                    fontSize: responsiveFontSize(1.8),
                                    fontWeight: '400'
                                }}
                                defaultValue="Seleccione talla"
                                dropdownStyle={{width: responsiveWidth(30)}}
                                optionTextStyle={{fontSize: responsiveFontSize(1.8)}}
                            />
                        </View>
                        <View
                            style={[
                                styles.cantprend,
                                {
                                    borderColor: 'white',
                                    backgroundColor: this.state.fourthColor
                                }
                            ]}>
                            <Text style={{fontWeight: '700', marginTop: 3}}>Cantidad:</Text>
                            <View style={styles.viewStepper}>
                                <SimpleStepper
                                    valueChanged={value => this.valueChanged(value, index)}
                                    tintColor="#000"
                                    minimumValue={0}
                                    stepValue={1}
                                    maximumValue={10}
                                />
                                <Text
                                    style={{
                                        marginLeft: 10,
                                        fontSize: responsiveFontSize(3.5),
                                        fontWeight: '400'
                                    }}>
                                    {this.state.displayValue[index]}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', width: responsiveWidth(94)}}>
                        <View style={styles.precioPrend}>
                            <Text>Precio unitario: ${this.state.precioUnitario[index]}</Text>
                        </View>
                        <View
                            style={[
                                styles.precioPrend,
                                {borderLeftWidth: 1, borderColor: 'white'}
                            ]}>
                            <Text>Subtotal: ${this.state.subTotales[index]}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Info +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async getUnifInfo(prenda, index) {
        let info = await fetch(
            this.state.uri +
            '/api/get/uniforme/info/' +
            prenda +
            '/' +
            this.state.lasTallas[index],
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let infos = await info.json();
        this.setState({informacion: infos[0]});
        this.state.precioUnitario[index] = this.state.informacion.precio;
        await this.setState({piloto: ' '});
    }

    valueChanged(value, index) {
        const displayValue = value.toFixed(0);
        this.state.subTotales[index] =
            displayValue * parseInt(this.state.precioUnitario[index]);
        this.state.displayValue[index] = displayValue;
        this.setState({piloto: ' '});
    }

    async dropChange(option, index) {
        let prenda = this.state.uniformes[index];
        this.state.lasTallas[index] = this.state.tallas[option];
        await this.getUnifInfo(prenda, index);
        await this.setState({piloto: ' '});
    }

    async onChange(option) {
        this.setState({
            displayValue: [],
            lasTallas: [],
            precioUnitario: [],
            subTotales: []
        });
        await this.setState({textInputValue: option.label, selected: option.id});
        await this.getUnifNombres(option.id);
    }

    pedido() {
        this.state.pedidos.length = this.state.uniformes.length;
        for (let i = 0; i < this.state.uniformes.length; i++) {
            if (isNaN(this.state.subTotales[i])) {
                this.state.pedidos[i] = {
                    articulo: this.state.uniformes[i],
                    cantidad: this.state.displayValue[i],
                    precio: this.state.precioUnitario[i],
                    subTotales: '0',
                    talla: this.state.lasTallas[i]
                };
            } else {
                this.state.pedidos[i] = {
                    articulo: this.state.uniformes[i],
                    cantidad: this.state.displayValue[i],
                    precio: this.state.precioUnitario[i],
                    subTotales: this.state.subTotales[i],
                    talla: this.state.lasTallas[i]
                };
            }
        }
        Actions.confirmarUniformes({
            pedido: this.state.pedidos
        });
    }

    render() {
        let index = 0;
        const data = [
            {key: index++, label: 'GALA/DIARIO', id: 'gala'},
            {key: index++, label: 'DEPORTIVO', id: 'deportivo'},
            {key: index++, label: 'OTRAS PRENDAS', id: 'otro'}
        ];
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />

                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione el tipo de uniforme:
                </Text>
                <ModalSelector
                    data={data}
                    style={{
                        width: responsiveWidth(94),
                        borderColor: this.state.fourthColor,
                        marginBottom: 5
                    }}
                    cancelText="Cancelar"
                    optionTextStyle={{color: this.state.thirdColor}}
                    initValue="Seleccione tipo de uniforme"
                    onChange={option => this.onChange(option)}
                />
                <ScrollView showsVerticalScrollIndicator={true}>
                    {this.state.selected === 'deportivo' ? this.dep() : null}
                    {this.state.selected === 'gala' ? this.gala() : null}
                    {this.state.selected === 'otro' ? this.otrPr() : null}
                </ScrollView>
                <TouchableOpacity
                    onPress={() => this.pedido()}
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: this.state.secondColor,
                            borderColor: this.state.secondColor
                        }
                    ]}>
                    <Text style={styles.textButton}>Realizar Pedido</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
