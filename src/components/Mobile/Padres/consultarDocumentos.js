import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    ScrollView,
    TouchableOpacity,
} from "react-native";
import {
    deviceHeight
} from "react-native-responsive-dimensions";
import styles from "../../styles";

export default class consultarDocumentos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        this.getURL();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            url: uri,
            token: token
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.main_title}>
                    Seleccione el documento a consultar
                </Text>
                <View style={[styles.cargos, {marginRight: 15}]}>
                    <Text style={[styles.subTitleMain_PP, {marginRight: 185}]}>
                        Documentos
                    </Text>
                    <Text style={[styles.subTitleMain_PP]}>Estatus</Text>
                </View>
                <ScrollView>
                    <View style={styles.ButtonsRow}>
                        <View style={{marginRight: 5}}>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}
                            >
                                <View>
                                    <Text numberOfLines={1}>Manual de Convivencia Escolar</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}
                            >
                                <View>
                                    <Text numberOfLines={1}>Directivas de Responsabilidad</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}
                            >
                                <View>
                                    <Text numberOfLines={1}>Manual de Seguridad Escolar</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}
                            >
                                <View>
                                    <Text numberOfLines={1}>Ley General de Educación</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}
                            >
                                <View>
                                    <Text numberOfLines={1}>
                                        Lineamiento Para Asignación de Becas
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}
                            >
                                <View>
                                    <Text numberOfLines={1}>Política de Privacidad</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.mainColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.secondColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View>
                                    <Text style={styles.textoB}>Pendiente</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.mainColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.secondColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View>
                                    <Text style={styles.textoB}>Pendiente</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        borderColor: this.state.fourthColor,
                                        backgroundColor: this.state.mainColor
                                    }
                                ]}
                            >
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        borderColor: this.state.fourthColor,
                                        backgroundColor: this.state.mainColor
                                    }
                                ]}
                            >
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
