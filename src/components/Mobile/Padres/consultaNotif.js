import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, View} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';
import {Actions} from 'react-native-router-flux';
import MultiBotonRow from '../Globales/MultiBotonRow';
import ModalSelector from 'react-native-modal-selector';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


export default class consultaNotif extends React.Component {
   constructor(props) {
	  super(props);
	  this.state = {
		 data: [],
		 items: {},
		 laSolicitud: '',
		 solicitudes: [],
		 types: [],
		 type: '',
		 elRequest: [],
		 indexSelected: 0,
		 data: []
	  };
   }

   async componentWillMount() {
	  await this.getURL();
   }

   async getURL() {
	  this.setState({
		 uri: await AsyncStorage.getItem('uri'),
		 token: await AsyncStorage.getItem('token'),
		 mainColor: await AsyncStorage.getItem('maincolor'),
		 secondColor: await AsyncStorage.getItem('secondColor'),
		 thirdColor: await AsyncStorage.getItem('thirdColor'),
		 fourthColor: await AsyncStorage.getItem('fourthColor')
	  });
   }

   //++++++ Modal +++++++++
   async getFilterMias() {
	  await fetch(this.state.uri + '/api/filter1/notificaciones/' + this.state.type, {
		 method: 'GET', headers: {
			Accept: 'application/json',
			'Content-Type': 'multipart/form-data',
			Authorization: 'Bearer ' + this.state.token
		 }
	  }).then(res => res.json())
		.then(responseJson => {
		   if (responseJson.error !== undefined) {
			  Alert.alert('Error',
				  'Ha ocurrido un error ' + responseJson.error.status_code + ' al cargar notificaciones si el error continua pónganse en contacto con soporte (Notificaciones)', [{
				 text: 'Entendido', onPress: () => Actions.drawer()
			  }]);
		   } else {
			  this.setState({elRequest: responseJson});
		   }
		});
   }

   async getFilterPorMi() {
	  await fetch(this.state.uri + '/api/filter2/notificaciones/' + this.state.type, {
		 method: 'GET', headers: {
			Accept: 'application/json',
			'Content-Type': 'multipart/form-data',
			Authorization: 'Bearer ' + this.state.token
		 }
	  }).then(res => res.json())
		.then(responseJson => {
		   if (responseJson.error !== undefined) {
			  Alert.alert('Error en notificaciones',
				  'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de cargar las notificaciones, si el error continua pónganse en contacto con soporte (Notificaciones)', [{
				 text: 'Entendido', onPress: () => Actions.drawer()
			  }]);
		   } else {
			  this.setState({elRequest: responseJson});
		   }
		});
   }

   async onChange(option) {
	  await this.setState({type: option.id});
	  if (this.state.indexSelected === 0) {
		 await this.getFilterMias();
	  } else {
		 await this.getFilterPorMi();
	  }
   }

   renderNotificaciones() {
	  let buttonsRespuestas = [];
	  this.state.elRequest.forEach((it, i) => {
		 buttonsRespuestas.push(this.notificaciones(it, i));
	  });
	  return buttonsRespuestas;
   }

   async ir(it) {
	  if (this.state.type === 'noticias') {
		 let request = await fetch(this.state.uri + '/api/get/post/by/notification/' + it.url, {
			method: 'GET', headers: {
			   Authorization: 'Bearer ' + this.state.token
			}
		 });
		 let data = await request.json();
		 await this.setState({data: []});
		 await this.setState({data: data});
		 await Actions.newsSingle({data: this.state.data});
	  } else if (this.state.type === 'cita') {
		 await Actions.agendarCitas();
	  }
   }

   notificaciones(it, i) {
	  const rightButtons = [<TouchableHighlight
		underlayColor={'transparent'}
		style={{backgroundColor: '#FC451E',width: responsiveWidth(20),height: responsiveHeight(10),alignItems: 'center',
		   justifyContent: 'center'}}>
		 <MaterialIcons onPress={() => this.ir(it)} style={{width: 30, height: 30}} color={'#fff'}
						  name='delete-forever' size={30}/>
	  </TouchableHighlight>];
	  return (<Swipeable key={i + 'not'} rightButtons={rightButtons}>
		 <View
		   style={[styles.notificaciones, {borderColor: this.state.fourthColor}]}>
			<View style={{
			   width: responsiveWidth(18),
			   height: responsiveHeight(10),
			   alignItems: 'center',
			   justifyContent: 'center'
			}}>
			   <Ionicons style={{width: 35, height: 35}} color={this.state.fourthColor} name='ios-notifications-outline' size={35}/>
			</View>
			<View style={{width: responsiveWidth(65)}}>
			   {this.state.type === 'cita' ? (<Text style={styles.notifTitle}>
				  Cita agendada
			   </Text>) : null}
			   {this.state.type === 'noticias' ? (<Text style={styles.notifTitle}>
				  Nueva noticia
			   </Text>) : null}
			   {this.state.type === 'pagos' ? (<Text style={styles.notifTitle}>
				  Nuevo pago
			   </Text>) : null}
			   {this.state.type === 'encuadres' ? (<Text style={styles.notifTitle}>
				  Encuadre
			   </Text>) : null}
			   <Text style={styles.textInfo}>
				  {it.data.substr(0, 70)+'...'}
			   </Text>
			</View>
			<View style={{
			   width: responsiveWidth(10),
			   alignItems: 'center',
			   justifyContent: 'center'
			}}>
			   <SimpleLineIcons onPress={() => this.ir(it)} style={{width: 30, height: 30}} color={this.state.fourthColor}
								name='arrow-right' size={30}/>
			</View>
		 </View>
	  </Swipeable>);
   }

   //+++++++ MultibotonRow +++++++
   async botonSelected(indexSelected, itemSelected) {
	  await this.setState({
		 botonSelected: itemSelected, indexSelected: indexSelected
	  });
   }

   render() {
	  let index = 0;
	  const notificaciones = this.renderNotificaciones();
	  const data = [{key: index++, label: 'Pagos', id: 'pagos'}, {
		 key: index++, label: 'Noticias', id: 'noticias'
	  }, {key: index++, label: 'Citas', id: 'cita'}, {key: index++, label: 'Encuadres', id: 'encuadre'}];


	  return (<View style={styles.container}>
		 <StatusBar
		   backgroundColor={this.state.mainColor}
		   barStyle='light-content'
		 />
		 <MultiBotonRow
		   itemBtns={['Para mí', 'Hechas por mí']}
		   onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
		   cantidad={2}
		 />
		 <ModalSelector
		   data={data}
		   cancelText='Cancelar'
		   optionTextStyle={{
			  color: this.state.thirdColor
		   }}
		   selectStyle={[styles.inputPicker, {
			  borderColor: this.state.secondColor, marginTop: 2
		   }]}
		   initValue='Seleccione un tipo de notificacion'
		   onChange={option => this.onChange(option)}
		 />
		 <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
			Seleccione la notificación
		 </Text>
		 <ScrollView style={styles.widthall} showsVerticalScrollIndicator={false}>
			{notificaciones}
		 </ScrollView>
	  </View>);
   }
}
