import React from 'react';
import {
	Alert, AsyncStorage, KeyBoard, KeyboardAvoidingView, RefreshControl, ScrollView, StatusBar, Text, TextInput,
	TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {Actions} from 'react-native-router-flux';
import Modal from 'react-native-modal';
import MultiBotonRow from './../Globales/MultiBotonRow';
import StepIndicator from 'rn-step-indicator';
import Spinner from 'react-native-loading-spinner-overlay';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Entypo from 'react-native-vector-icons/Entypo';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

const labels_estForm = [
	'Solicitar\ninfo',
	'Agendar\ncita',
	'Subir\ndocumentos',
	'Acreditar\npreinscripción',
	'Día de\ninmersión',
	'Acreditar\ninscripción'
];

const customStyles = {
	stepIndicatorSize: 20,
	currentStepIndicatorSize: 25,
	separatorStrokeWidth: 1,
	currentStepStrokeWidth: 2,
	stepStrokeCurrentColor: '#777777',
	separatorFinishedColor: '#16a900',
	separatorUnFinishedColor: '#aaaaaa',
	stepIndicatorFinishedColor: '#16a900',
	stepIndicatorUnFinishedColor: '#aaaaaa',
	stepIndicatorCurrentColor: '#ffffff',
	stepIndicatorLabelFontSize: responsiveFontSize(1.3),
	currentStepIndicatorLabelFontSize: responsiveFontSize(1.3),
	stepIndicatorLabelCurrentColor: '#000000',
	stepIndicatorLabelFinishedColor: '#ffffff',
	stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
	labelColor: '#666666',
	labelSize: responsiveFontSize(1),
	currentStepLabelColor: '#777777'
};

export default class aspirantePendiente extends React.Component {
	_onRefresh = () => {
		let alumnAlta = [];
		let alumnPend = [];
		this.setState({refreshing: true});
		fetch(this.state.uri + '/api/get/aspirante/' + this.state.data.id_registro).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Inscripciones)',
						[{text: 'Entendido', onPress: () => this.spinner(false)}]);
				} else {
					this.setState({aspirantes: responseJson});
					if (this.state.aspirantes.length !== 0) {
						this.state.aspirantes.forEach((it, i) => {
							this.state.currentPosition.push(0);
							this.state.modalAspirante.push(false);
							if (it.estatus_alta === 'alta') {
								alumnAlta.push(it);
							} else {
								alumnPend.push(it);
							}
						});
					}
					this.state.alumnAlta = alumnAlta;
					this.state.alumnPend = alumnPend;
					this.setState({refreshing: false});
				}
			});
	};

	spinner = (state) => {
		this.setState({visible: state});
	};

	constructor(props) {
		super(props);
		this.state = {
			aspirantes: [],
			refreshing: false,
			stepsLabel: [],
			data: [],
			ninosPicker: [],
			visible: false,
			modalVisible1: false,
			botonSelected: 'Registro',
			indexSelected: 0,
			modalAspirante: [],
			currentPosition: [],
			elComentario: '',
			idAlumno: '',
			alumnAlta: [],
			alumnPend: [],
			autorizados: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getUserdata();
		await this.getHijos();
		await this.setState({aux: 0});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}


//+-+-+-+-+--+-+
	async getUserdata() {
		let admin = await fetch(this.state.uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let adminId = await admin.json();
		await this.setState({data: adminId});
		await this.getAspirantes(this.state.data.id_registro);
	}

//-+-+-+-+-++---+-+-+-+-+get aspirantes
	async getAspirantes(id) {
		let alumnAlta = [];
		let alumnPend = [];
		await this.spinner(true);
		await fetch(this.state.uri + '/api/get/aspirante/' + id).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Inscripciones)',
						[{text: 'Entendido', onPress: () => this.spinner(false)}]);
				} else {
					this.setState({aspirantes: responseJson});
					if (this.state.aspirantes.length !== 0) {
						this.state.aspirantes.forEach((it, i) => {
							this.state.currentPosition.push(0);
							this.state.modalAspirante.push(false);
							if (it.estatus_alta === 'alta') {
								alumnAlta.push(it);
							} else {
								alumnPend.push(it);
							}
						});
					}
					this.state.alumnAlta = alumnAlta;
					this.state.alumnPend = alumnPend;
					this.setState({refreshing: false});
					this.spinner(false);
				}
			});
	}

	//-+-+-+-+-+-+-+-+-+-+-+-+-+enviar indicacion
	async openModal(aspirante, i) {
		this.state.modalAspirante[i] = true;
		this.state.elComentario = aspirante.indicacion_especial;
		this.state.idAlumno = aspirante.id;
		await this.setState({aux: 0});
	}

	async closeModal(indicacion, i) {
		this.state.aspirantes[i].indicacion_especial = indicacion;
		this.state.modalAspirante[i] = false;
		await this.setState({aux: 0});
	}

	async enviarIndicacion(id, indicacion, i) {
		if (this.state.elComentario === '') {
			Alert.alert(
				'Campo indicaciones vacío', 'Escriba las indicaciones para poder continuar de lo contrario presione cancelar',
				[{text: 'Entendido'}]
			);
		} else {
			await fetch(this.state.uri + '/api/indicacion/especial/' + id + '/' + indicacion).then(res => res)
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al enviar la solicitud', 'Ha ocurrido un error ' + responseJson.error.status_code +
							' al tratar de enviar la solicitud de informes, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
							[{
								text: 'Entendido',
								onPress: () => [this.closeModal(i)]
							}]);
					} else {
						Alert.alert(
							'¡Gracias!',
							'Hemos recibido sus indicaciones',
							[{text: 'Entendido', onPress: () => [this.closeModal(indicacion, i)]}]
						)
					}
				});
		}

	}

	//+-+-+-+-+-+-+-+ revisar pago
	async pagoAgain(aspirante) {
		if (aspirante.paso_registro === '3.1') {
			Actions.pagoPreinscrip({elAspirante: aspirante})
		} else if (aspirante.paso_registro === '5.1') {
			Actions.pagoInscrip({elAspirante: aspirante})
		}
		await this.setState({aux: 0});
	}

	async revisarPago(aspirante, tipo_pago, paso) {
		await this.spinner(true);
		await fetch(this.state.uri + '/api/get/estatus/pago/' + aspirante.id + '/' + tipo_pago, {
			method: 'GET', headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{text: 'Entendido'}]);
				} else {
					if (responseJson.result.status === 'in_progress') {
						Alert.alert('Su pago está en proceso',
							'Su pago está siendo procesado,\neste será reflejado en un lapso de 24 a 48 horas despues pagado\n' +
							'¿Desea crear el cargo de nuevo?', [
								{
									text: 'Sí',
									onPress: () => [this.spinner(false), this.pagoAgain(aspirante)]
								}, {text: 'No', onPress: () => this.spinner(false)}
							]);
					} else if (responseJson.result.status === 'completed') {
						if (paso === '6') {
							Alert.alert('Su pago ha sido acreditado',
								'Su pago ha sido acreditado. Ahora podrá continuar con el proceso de inscripción', [
									{
										text: 'Entendido',
										onPress: () => [this.spinner(false), this.updateAspirante(aspirante, paso, 'alta')]
									}
								]);
						} else {
							Alert.alert('Su pago ha sido acreditado',
								'Su pago ha sido acreditado. Ahora podrá continuar con el proceso de inscripción', [
									{
										text: 'Entendido',
										onPress: () => [this.spinner(false), this.updateAspirante(aspirante, paso, 'en proceso')]
									}
								]);
						}
					}
				}
			});
	}

	//+-+-++-+-+- updates aspiante
	async updateAspirante(aspirante, paso, estatus) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: aspirante.id,
			paso_registro: paso,
			estatus_alta: estatus
		}));
		await fetch(this.state.uri + '/api/cita/aspirantes', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Pago en efectivo)', [
							{text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})}
						]);
				} else {
					if (paso === '6') {
						Alert.alert('¡FELICIDADES!',
							'Ahora el aspirante ' + aspirante.nombre_aspirante + ' ya es parte de la comunidad escolar', [
								{
									text: 'Enterado ¡Gracias!',
									onPress: () => this.createAlumno(aspirante, this.state.data.id)
								}
							])
					} else {
						this.getAspirantes(aspirante.id_tutor)
					}
				}
			});
	}

	async pasoSiguiente(aspirante) {
		if (aspirante.paso_registro === '1') {
			Actions.citaPreinscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '2') {
			Actions.subirDoctos({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '2.1') {
			Alert.alert('Documentos en revisión',
				'Los documentos están siendo revisados, se le notificará cuándo pueda continuar con el proceso', [
					{text: 'Enterado ¡Gracias!'}
				]);
		} else if (aspirante.paso_registro === '3') {
			Actions.pagoPreinscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '3.1') {
			this.revisarPago(aspirante, 'preinscripcion', '4');
		} else if (aspirante.paso_registro === '4') {
			Actions.diaInmersion({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '5') {
			Actions.pagoInscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '5.1') {
			this.revisarPago(aspirante, 'inscripcion', '6');
		} else if (aspirante.paso_registro === '6') {
			this.updateAspirante(aspirante, '6', 'alta');
		} else if (aspirante.paso_registro === '7') {
			Actions.citaPreinscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '7') {
			Actions.citaPreinscrip({elAspirante: aspirante});
		} else if (aspirante.paso_registro === '7.1') {
			Alert.alert('Cita pendiente',
				'La cita para el aspirante ' + aspirante.nombre_aspirante + ' fue agendada para el dia ' + moment(aspirante.fecha_cita).format('ll') + ' a las ' + moment(aspirante.hora_cita, 'hh:mm').format('hh:mm a'))
		}
	}

	rndAspirantes() {
		let aspirante = [];
		this.state.aspirantes.forEach((it, ix) => {
			let txtBtn = 'Ir a siguiente paso';
			if (it.paso_registro === '1') {
				txtBtn = 'Solicitar cita';
			} else if (it.paso_registro === '2') {
				txtBtn = 'Subir documentos';
			} else if (it.paso_registro === '2.1') {
				txtBtn = 'Documentos en revisión';
			} else if (it.paso_registro === '3') {
				txtBtn = 'Pago de preinscripción';
			} else if (it.paso_registro === '3.1') {
				txtBtn = 'Estatus de pago';
			} else if (it.paso_registro === '4') {
				txtBtn = 'Ir a día de inmersión'
			} else if (it.paso_registro === '5') {
				txtBtn = 'Pago de inscripción';
			} else if (it.paso_registro === '5.1') {
				txtBtn = 'Estatus de pago';
			}
			if (it.paso_registro === '6') {
				txtBtn = 'Dar de alta';
			} else if (it.paso_registro === '7') {
				txtBtn = 'Solicitar cita';
			} else if (it.paso_registro === '7.1') {
				txtBtn = 'Cita pendiente';
			}
			if (it.estatus_alta !== 'alta')
				aspirante.push(
					<View
						key={ix + 'aspCard'}
						style={[
							styles.cardHoras,
							styles.widthall,
							{
								backgroundColor: this.state.fourthColor,
								marginVertical: 5,
								padding: 10,
								alignItems: 'center'
							}
						]}
					>
						<Modal
							isVisible={this.state.modalAspirante[ix]}
							backdropOpacity={0.8}
							animationIn={'bounceIn'}
							animationOut={'bounceOut'}
							animationInTiming={1000}
							animationOutTiming={1000}
						>
							<KeyboardAvoidingView
								style={[styles.container, {borderRadius: 6, flex: 0}]}
								keyboardVerticalOffset={150} behavior='padding'
							>
								<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
									Escriba la información:
								</Text>
								<TextInput
									keyboardType='default'
									returnKeyType='next'
									multiline={true}
									placeholder='Si existe cualquier información relevante sobre el estado de salud de su hijo o cualquier diagnóstico médico o psicológico relevante, favor de escribirlo aquí.'
									underlineColorAndroid='transparent'
									onChangeText={text => (this.state.elComentario = text)}
									defaultValue={this.state.elComentario}
									style={[
										styles.inputContenido,
										styles.modalWidth,
										{borderColor: this.state.secondColor, padding: 5, height: responsiveHeight(20)}
									]}
								/>
								<View style={[styles.modalWidth, styles.row]}>
									<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{backgroundColor: '#fff', borderColor: this.state.secondColor}
										]}
										onPress={() => [(this.state.modalAspirante[ix] = false), this.setState({aux: 0})]}
									>
										<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
									</TouchableOpacity>
									<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{
												backgroundColor: this.state.secondColor,
												borderColor: this.state.secondColor
											}
										]}
										onPress={() => this.enviarIndicacion(this.state.idAlumno, this.state.elComentario, ix)}
									>
										<Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
									</TouchableOpacity>
								</View>
							</KeyboardAvoidingView>
						</Modal>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Nombre del aspirante: </Text>
							<Text style={[styles.textW]}> {it.nombre_aspirante}</Text>
						</View>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Escuela procedente: </Text>
							<Text style={[styles.textW]}> {it.escuela_procedente}</Text>
						</View>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Ultimo promedio gral: </Text>
							<Text style={[styles.textW]}> {it.promedio_gral}</Text>
						</View>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Grado al que aplica: </Text>
							<Text style={[styles.textW]}> {it.grado_interes}</Text>
						</View>
						<View style={[styles.btn1, {marginTop: 20, marginBottom: 10}]}>
							<StepIndicator
								customStyles={customStyles}
								stepCount={6}
								currentPosition={Number(this.state.aspirantes[ix].paso_registro)}
								labels={labels_estForm}
							/>
						</View>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.openModal(this.state.aspirantes[ix], ix)}
							>
								<Text style={[styles.textW, {
									color: this.state.secondColor,
									fontSize: responsiveFontSize(1.5)
								}]}>
									Indicaciones especiales
								</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{
										backgroundColor: this.state.secondColor,
										borderColor: this.state.secondColor
									}
								]}
								onPress={() => this.pasoSiguiente(this.state.aspirantes[ix], ix)}
							>
								<Text style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
									{txtBtn}
								</Text>
							</TouchableOpacity>
						</View>
					</View>
				)
		});
		return aspirante;
	}

	//+-+-+-+-+-+-+-+- altas aspiante
	async createAlumno(aspirante, id) {
		await fetch(this.state.uri + '/api/alta/tutor/aspirante', {
			method: 'POST',
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: aspirante.nombre_aspirante,
				email: 'alumno' + id + Math.floor((Math.random() * 100) + 1) + '@ce.pro',
				password: 'asdasd',
				role: 'Alumno',
				activo: '0',
				estatus: aspirante.tipo_alta,
				puesto: 'Alumno',
				PushNotificationsID: '',
				escuela_id: '0',
				grado: aspirante.grado_interes,
				grupo: '0',
				padre_id: id,
				id_registro: aspirante.id,
				genero: aspirante.genero
			})
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'Ha ocurrido un error '
						+
						responseJson.error.status_code
						+
						' si el error continua pónganse en contacto con soporte'
						+
						'(Alta de aspirante)',
						[{
							text: 'Entendido',
							onPress: () => [this.spinner(false), Actions.pop({refresh: {key: 'Drawer'}})]
						}]);
				} else {
					this.spinner(false);
					Actions.refresh({key: Math.random()});
				}
			});
	}

//+-+-+-+-+-++-+-+-+-+--+- Aspirantes dados de alta
	rndAlta() {
		let aspirante = [];
		this.state.aspirantes.forEach((it, ix) => {
			if (it.estatus_alta === 'alta') {
				aspirante.push(
					<View
						key={ix + 'aspCard'}
						style={[
							styles.cardHoras,
							styles.widthall,
							{
								backgroundColor: this.state.fourthColor,
								marginVertical: 5,
								padding: 10,
								alignItems: 'center'
							}
						]}
					>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Aspirante: </Text>
							<Text style={[styles.textW]}> {it.nombre_aspirante}</Text>
						</View>
					</View>
				)
			}
		});
		return aspirante;
	}

	async botonSelected(indexBtn, itemBtn) {
		await this.setState({botonSelected: itemBtn, indexSelected: indexBtn});
	}

//+-+-+-+-+-+-+-+-+-+-+-+ reinscripción
	async getHijos() {
		let autorizados = [];
		let token = await AsyncStorage.getItem('token');
		let hijoPicker = await fetch(this.state.uri + '/api/user/hijos', {
			method: 'GET', headers: {
				Authorization: 'Bearer ' + token
			}
		});
		let hijosPicker = await hijoPicker.json();
		this.setState({ninosPicker: hijosPicker});
		if (this.state.ninosPicker.length !== 0) {
			this.state.ninosPicker.forEach((it, i) => {
				if (it.reinscripcion === 'Autorizado') {
					autorizados.push(it);
				}
			});
			this.state.autorizados = autorizados;
		}
	}

	alumnReinscrip() {
		let aspirante = [];
		this.state.ninosPicker.forEach((it, ix) => {
			if (it.reinscripcion === 'Autorizado') {
				aspirante.push(
					<View
						key={ix + 'aspCard'}
						style={[
							styles.cardHoras,
							styles.widthall,
							{
								backgroundColor: this.state.fourthColor,
								marginVertical: 5
							}
						]}
					>
						<View style={[styles.row, {
							justifyContent: 'flex-start',
							padding: 10
						}]}>
							<Text>Alumno: </Text>
							<Text style={[styles.textW]}> {it.name} </Text>

						</View>
						<View style={[styles.row, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Grado:</Text>
							<Text style={[styles.textW]}> {it.grado + ' ' + it.grupo}</Text>
						</View>
						<View style={[styles.widthall, {alignItems: 'center', marginVertical: 10}]}>
							<TouchableOpacity
								style={[styles.btn3, {
									borderRadius: 6,
									backgroundColor: this.state.secondColor,
									padding: 6
								}]}
								onPress={() => Actions.GestionarPagos()}
							>
								<Text style={[styles.textW, {
									color: '#fff',
									textAlign: 'center',
									lineHeight: responsiveHeight(1.75)
								}]}>
									Pagar ahora
								</Text>
							</TouchableOpacity>
						</View>
					</View>
				)
			}
		});
		return aspirante;
	}

	rndReinscripcion() {
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Alumnos autorizados para la reinscripción
				</Text>
				<View style={{marginTop: 10}}>
					{this.state.autorizados.length !== 0 ? this.alumnReinscrip() :
						<Text style={[styles.main_title, {
							color: '#a8a8a8',
							textAlign: 'center',
							marginVertical: 10,
							fontSize: responsiveFontSize(2)
						}]}>
							No hay alumnos por autorizar reinscripción
						</Text>}
				</View>
			</View>
		)
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<MultiBotonRow
					itemBtns={[
						'Inscripciones',
						'Reinscripciones'
					]}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelected(indexBtn, itemBtn)
					}
					cantidad={2}
				/>
				{this.state.indexSelected === 0 ? (
					<View>
						<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
							Estudiantes dados de alta
						</Text>
						{this.state.alumnAlta.length !== 0 ? this.rndAlta() :
							<Text style={[styles.main_title, {
								color: '#a8a8a8',
								textAlign: 'center',
								marginVertical: 10,
								fontSize: responsiveFontSize(2)
							}]}>
								No hay alumnos dados de alta
							</Text>}
						<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
							Estudiantes con registro pendiente
						</Text>
						<ScrollView
							refreshControl={
								<RefreshControl
									refreshing={this.state.refreshing}
									onRefresh={this._onRefresh}
								/>
							}
							overScrollMode='always'
							showsVerticalScrollIndicator={false}
							style={{...ifIphoneX({marginBottom: 100}, {marginBottom: 20})}}
						>
							{this.state.alumnPend.length !== 0 ? this.rndAspirantes() :
								<Text style={[styles.main_title, {
									color: '#a8a8a8',
									textAlign: 'center',
									marginTop: 5,
									fontSize: responsiveFontSize(2)
								}]}>
									No hay alumnos con registro pendiente{'\n'}
									¿Desea agregar un nuevo alumno?
								</Text>}
							<View style={[styles.widthall, {alignItems: 'center', marginVertical: 10}]}>
								<Entypo
									name='circle-with-plus' size={40} color='#43d551'
									onPress={() => Actions.newAsp({
										id_registro: this.state.data.id_registro,
										id_user: this.state.data.id
									})}
								/>
							</View>
						</ScrollView>
					</View>
				) : this.rndReinscripcion()}
			</View>
		);
	}
}
