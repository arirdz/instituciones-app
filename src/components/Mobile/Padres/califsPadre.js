import React from "react";
import {Alert, AsyncStorage, Platform, ScrollView, StatusBar, Text, TouchableHighlight, View} from "react-native";
import Hijos from "../Globales/hijos";
import Periodos from "../Globales/Periodos";
import Spinner from 'react-native-loading-spinner-overlay';
import styles from "../../styles";
import {responsiveFontSize, responsiveWidth} from "react-native-responsive-dimensions";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class califsPadre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            aux: 0,
            lasCalifs: [],
            allCalifs: [],
            visible: false,
            selectedIndex: 0,
            selGrad: '',
            elHijo: '',
            selGrup: '',
            selectedIndexPeriodo: -1,
            elPeriodo: '',
            indexSelected: 0,
            elTipo: 'calif',
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.califsByIdAlumno(this.state.elHijo, this.state.selGrad);
    }

    _spinner = (state) => {
        this.setState({visible: state});
    };

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onListItemPressedPeriodo(indexPeriodo, ciclo) {
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: ciclo,
            lasCalifs: [],
        });
        await this.getCalifs(this.state.elHijo, this.state.selGrad);
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            elHijo: id,
            lasCalifs: [],
        });
        if (this.state.indexSelected === 0) {
            await this.califsByIdAlumno(id, grado)
        }
        if (this.state.selectedIndexPeriodo !== -1) {
            await this.getCalifs(this.state.elHijo, this.state.selGrad);
        }
    }

    async califsByIdAlumno(id_alumno, grado) {
        await this._spinner(true);
        await fetch('http://127.0.0.1:8000/api/get/califs/' + id_alumno + '/' + grado, {
            method: "GET", headers: {
                "Content-Type": "application/json"
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al guardar las calificaciones',
                        'Hubo un error al tratar de guardar las calificaciones', [
                            {text: 'Etendido', onPress: () => this._spinner(false)}
                        ]);
                } else {
                    this.setState({allCalifs: responseJson});
                    this._spinner(false)
                }
            });
    }

    rndAcademicas() {
        let mates = [];
        this.state.allCalifs.forEach((itm, ix) => {
            let a = this.state.allCalifs.length - 1;
            let tabRow = [styles.rowTabla];
            if (ix !== a) {
                tabRow.push({borderColor: this.state.secondColor});
            }
            if (itm.tipo_materia === 'formacion academica') {
                mates.push(
                    <View
                        key={ix + 'alumnCal'}
                        underlayColor={'transparent'}
                        style={[tabRow, styles.campoTablaG, styles.row]}
                    >
                        <Text numberOfLines={1} style={{width: responsiveWidth(50)}}>{itm.materia}</Text>
                        <View style={[styles.row, {width: responsiveWidth(30)}]}>
                            {this.rndCalifsAcad(itm, ix)}
                        </View>
                    </View>
                )
            }
        });
        return mates;
    }

    rndCalifsAcad(mate, i) {
        let califs = [];
        this.state.allCalifs[i].calificaciones.forEach((itm, ix) => {
            let califValue = Number(itm.calificacion);
            let value = '';
            if (califValue === 5) {
                value = 'I';
            } else if (califValue >= 6 && califValue <= 7.49) {
                value = 'II';
            } else if (califValue >= 7.5 && califValue <= 9.49) {
                value = 'III';
            } else if (califValue >= 9.5) {
                value = 'IV';
            } else if (isNaN(califValue) || califValue >= 0 || califValue < 11) {
                value = 'N/D';
            }
            if (mate.tipo_materia === 'formacion academica') {
                califs.push(
                    <Text
                        key={ix + 'asd'}
                        style={{
                            fontWeight: '500',
                            fontSize: responsiveFontSize(1.5),
                            width: responsiveWidth(10),
                            textAlign: 'center'
                        }}
                    >
                        {this.state.elTipo === 'calif' ? itm.calificacion : value}
                    </Text>
                )
            }
        });
        return califs;
    }

    rndNiveles() {
        let mates = [];
        this.state.allCalifs.forEach((itm, ix) => {
            let a = this.state.allCalifs.length - 1;
            let tabRow = [styles.rowTabla];
            if (ix !== a) {
                tabRow.push({borderColor: this.state.secondColor});
            }
            if (itm.tipo_materia !== 'formacion academica') {
                mates.push(
                    <View
                        key={ix + 'alumnCal'}
                        underlayColor={'transparent'}
                        style={[tabRow, styles.campoTablaG, styles.row]}
                    >
                        <Text numberOfLines={1} style={{width: responsiveWidth(50)}}>{itm.materia}</Text>
                        <View style={[styles.row, {width: responsiveWidth(30)}]}>
                            {this.rndCalifNivel(itm, ix)}
                        </View>
                    </View>
                )
            }
        });
        return mates;
    }

    rndCalifNivel(mate, ix) {
        let califs = [];
        this.state.allCalifs[ix].calificaciones.forEach((itm, i) => {
            let califValue = Number(itm.calificacion);
            let value = '';
            if (califValue === 0) {
                value = 'I';
            } else if (califValue === 1) {
                value = 'II';
            } else if (califValue === 2) {
                value = 'III';
            } else if (califValue === 3) {
                value = 'IV';
            } else if (isNaN(califValue)) {
                value = 'N/D';
            }
            if (mate.tipo_materia !== 'formacion academica') {
                califs.push(
                    <Text
                        key={i + 'asd'}
                        style={{
                            fontWeight: '500',
                            fontSize: responsiveFontSize(1.5),
                            width: responsiveWidth(10),
                            textAlign: 'center'
                        }}
                    >
                        {value}
                    </Text>
                )
            }
        });
        return califs;
    }

    //+++++++++++++++++++++++++++++calificaciones++++++++++++++++++++++++++++++++
    async getCalifs(id_alumno, grado) {
        await this._spinner(true);
        await fetch('http://127.0.0.1:8000/api/get/califs/padre/' + this.state.elPeriodo + '/' + id_alumno + '/' + grado, {
            method: "GET", headers: {
                "Content-Type": "application/json"
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al guardar las calificaciones',
                        'Hubo un error al tratar de guardar las calificaciones', [
                            {text: 'Etendido', onPress: () => this._spinner(false)}
                        ]);
                } else {
                    this.setState({lasCalifs: responseJson});
                    this._spinner(false);
                }
            });
    }


    rndMatAca() {
        let mates = [];
        this.state.lasCalifs.forEach((itm, ix) => {
            let a = this.state.lasCalifs.length - 1;
            let tabRow = [styles.rowTabla];
            if (ix !== a) {
                tabRow.push({borderColor: this.state.secondColor});
            }
            let califValue = Number(itm.calificacion);
            let value = '';
            if (califValue === 5) {
                value = 'I';
            } else if (califValue >= 6 && califValue <= 7.49) {
                value = 'II';
            } else if (califValue >= 7.5 && califValue <= 9.49) {
                value = 'III';
            } else if (califValue >= 9.5) {
                value = 'IV';
            } else if (isNaN(califValue) || califValue >= 0 || califValue < 11) {
                value = 'N/D';
            }
            if (itm.tipo_materia === 'formacion academica') {
                mates.push(
                    <View
                        key={ix + 'alumnCal'}
                        underlayColor={'transparent'}
                        style={[tabRow, styles.campoTablaG, styles.row]}
                    >
                        <Text numberOfLines={1} style={{width: responsiveWidth(50)}}>{itm.materia}</Text>
                        <Text style={{width: responsiveWidth(20), textAlign: 'center'}}>{itm.calificacion}</Text>
                        <Text style={{width: responsiveWidth(15), textAlign: 'center'}}>{value}</Text>
                    </View>
                )
            }
        });
        return mates;
    }

    async changeTipo(value) {
        await this.setState({elTipo: value});
    }

    rndMatOtro() {
        let mates = [];
        this.state.lasCalifs.forEach((itm, ix) => {
            let a = this.state.lasCalifs.length - 1;
            let tabRow = [styles.rowTabla];
            if (ix !== a) {
                tabRow.push({borderColor: this.state.secondColor});
            }
            let califValue = Number(itm.calificacion);
            let value = '';
            if (califValue === 0) {
                value = 'I';
            } else if (califValue === 1) {
                value = 'II';
            } else if (califValue === 2) {
                value = 'III';
            } else if (califValue === 3) {
                value = 'IV';
            } else if (isNaN(califValue)) {
                value = 'N/D';
            }
            if (itm.tipo_materia !== 'formacion academica') {
                mates.push(
                    <View
                        key={ix + 'alumnCal'}
                        underlayColor={'transparent'}
                        style={[tabRow, styles.campoTablaG, styles.row]}
                    >
                        <Text numberOfLines={1} style={{width: responsiveWidth(70)}}>{itm.materia}</Text>
                        <Text style={{width: responsiveWidth(12.5)}}>{value}</Text>
                    </View>
                )
            }
        });
        return mates;
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async botonSelected(numero) {
        await this.setState({indexSelected: numero, elPeriodo: '', selectedIndexPeriodo: -1, lasCalifs: []});
        if (numero === 0 && this.state.allCalifs.length === 0) {
            await this.califsByIdAlumno(this.state.elHijo, this.state.selGrad)
        }
    }

    render() {
        let index = this.state.indexSelected;
        let tipo = this.state.elTipo;
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} textContent='Cargando, por favor espere...'/>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <Hijos
                    onSelectedChamaco={(index, grado, grupo, id) =>
                        this.onListItemPressed(index, grado, grupo, id)
                    }
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione un tipo
                </Text>
                <View style={[styles.widthall, {alignItems: 'center'}]}>
                    <View
                        style={[styles.rowsCalif, {
                            width: responsiveWidth(62), marginTop: 10, marginBottom: 0
                        }]}>
                        <TouchableHighlight
                            underlayColor={'transparent'}
                            style={{
                                borderRadius: 6,
                                alignItems: 'center',
                                width: responsiveWidth(30),
                                padding: 6,
                                backgroundColor: index === 0 ? this.state.thirdColor : this.state.fourthColor
                            }}
                            onPress={() => this.botonSelected(0)}>
                            <Text
                                style={[styles.textW,
                                    {
                                        fontSize: responsiveFontSize(1.7),
                                        fontWeight: '700',
                                        color: index === 0 ? '#fff' : '#000'
                                    }]}
                            >
                                Todos
                            </Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                            underlayColor={'transparent'}
                            style={{
                                borderRadius: 6,
                                width: responsiveWidth(30),
                                alignItems: 'center',
                                padding: 6,
                                backgroundColor: index === 1 ? this.state.thirdColor : this.state.fourthColor
                            }}
                            onPress={() => this.botonSelected(1)}>
                            <Text
                                style={[styles.textW,
                                    {
                                        fontSize: responsiveFontSize(1.7),
                                        fontWeight: '700',
                                        color: index === 1 ? '#fff' : '#000'
                                    }]}
                            >
                                Por periodo
                            </Text>
                        </TouchableHighlight>
                    </View>
                </View>
                {index === 1 ? <Periodos
                    onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                        this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                    }
                /> : null}
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 10}]}>
                    Calificaciones disponibles
                </Text>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    overScrollMode={'always'}
                    style={{marginBottom: 20}}
                >
                    <Text
                        style={[styles.main_title, {
                            color: this.state.thirdColor,
                            fontSize: responsiveFontSize(1.7),
                            marginTop: 0
                        }]}>
                        Formación academica
                    </Text>
                    {index === 1 ?
                        <View>
                            <View
                                style={[styles.row, styles.widthall, {paddingHorizontal: 4}]}
                            >
                                <Text
                                    style={{
                                        fontWeight: '500',
                                        width: responsiveWidth(53),
                                        fontSize: responsiveFontSize(1.5)
                                    }}
                                >
                                    Nombre de la materia
                                </Text>
                                <Text
                                    style={{fontWeight: '500', fontSize: responsiveFontSize(1.5)}}
                                >
                                    Califcación
                                </Text>
                                <Text
                                    style={{fontWeight: '500', fontSize: responsiveFontSize(1.5), textAlign: 'center'}}
                                >
                                    Nivel de{'\n'}desempeño
                                </Text>
                            </View>
                            {this.state.lasCalifs.length === 0 ?
                                <Text style={[styles.main_title, {
                                    color: '#a8a8a8',
                                    textAlign: 'center',
                                    ...Platform.select({
                                        ios: {marginTop: 40, marginBottom: 40,},
                                        android: {marginTop: 35, marginBottom: 35}
                                    }),

                                    fontSize: responsiveFontSize(3)
                                }]}
                                >
                                    Seleccione un periodo
                                </Text> :
                                <View
                                    style={[
                                        styles.tabla,
                                        {
                                            borderColor: this.state.secondColor,
                                            marginTop: 5,
                                        }
                                    ]}
                                >
                                    {this.rndMatAca()}
                                </View>}
                        </View> :
                        <View>
                            <View style={[styles.widthall, {alignItems: 'flex-end'}]}>
                                <View
                                    style={[styles.row, {width: responsiveWidth(31.5), marginVertical: 5}]}
                                >
                                    <TouchableHighlight
                                        underlayColor={'transparent'}
                                        style={{
                                            borderRadius: 10,
                                            width: responsiveWidth(15),
                                            alignItems: 'center',
                                            padding: 3,
                                            backgroundColor: tipo === 'calif' ? this.state.thirdColor : this.state.fourthColor
                                        }}
                                        onPress={() => this.changeTipo('calif')}>
                                        <Text
                                            style={[styles.textW,
                                                {
                                                    fontSize: responsiveFontSize(1),
                                                    fontWeight: '700',
                                                    color: tipo === 'calif' ? '#fff' : '#000'
                                                }]}
                                        >
                                            Calificación
                                        </Text>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        underlayColor={'transparent'}
                                        style={{
                                            borderRadius: 10,
                                            alignItems: 'center',
                                            width: responsiveWidth(15),
                                            padding: 3,
                                            backgroundColor: tipo === 'nivel' ? this.state.thirdColor : this.state.fourthColor
                                        }}
                                        onPress={() => this.changeTipo('nivel')}>
                                        <Text
                                            style={[styles.textW,
                                                {
                                                    fontSize: responsiveFontSize(1),
                                                    fontWeight: '700',
                                                    color: tipo === 'nivel' ? '#fff' : '#000'
                                                }]}
                                        >
                                            Nivel
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                                <View style={{
                                    width: responsiveWidth(31.5),
                                    borderBottomWidth: .5,
                                    borderColor: this.state.secondColor,
                                    marginBottom: 2
                                }}/>
                            </View>
                            <View
                                style={[styles.row, styles.widthall, {paddingHorizontal: 4}]}
                            >
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        width: responsiveWidth(60),
                                        fontSize: responsiveFontSize(1.5)
                                    }}
                                >
                                    Nombre de la materia
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P1
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P2
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P3
                                </Text>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    {
                                        borderColor: this.state.secondColor,
                                        marginTop: 5,
                                    }
                                ]}
                            >
                                {this.rndAcademicas()}
                            </View>
                        </View>
                    }
                    {index === 0 ?
                        <View>
                            <Text style={[styles.main_title, {
                                color: this.state.thirdColor,
                                marginTop: 20,
                                fontSize: responsiveFontSize(1.7)
                            }]}>
                                Desarrollo personal y autonomia curricular
                            </Text>
                            <View
                                style={[styles.row, styles.widthall, {paddingHorizontal: 4}]}
                            >
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        width: responsiveWidth(60),
                                        fontSize: responsiveFontSize(1.5)
                                    }}
                                >
                                    Nombre de la materia
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P1
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P2
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P3
                                </Text>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    {
                                        borderColor: this.state.secondColor,
                                        marginTop: 5,
                                    }
                                ]}
                            >
                                {this.rndNiveles()}
                            </View>
                        </View>
                        : <View>
                            <Text style={[styles.main_title, {
                                color: this.state.thirdColor,
                                marginTop: 20,
                                fontSize: responsiveFontSize(1.7)
                            }]}>
                                Desarrollo personal y autonomia curricular
                            </Text>
                            <View
                                style={[styles.row, styles.widthall, {paddingHorizontal: 4}]}
                            >
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        width: responsiveWidth(45),
                                        fontSize: responsiveFontSize(1.5)
                                    }}
                                >
                                    Nombre de la materia
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        textAlign: 'center',
                                        width: responsiveWidth(20)
                                    }}
                                >
                                    Nivel de{'\n'}desempeño
                                </Text>
                            </View>
                            {this.state.lasCalifs.length === 0 ?
                                <Text style={[styles.main_title, {
                                    color: '#a8a8a8',
                                    textAlign: 'center',
                                    ...Platform.select({
                                        ios: {marginTop: 40, marginBottom: 40,},
                                        android: {marginTop: 35, marginBottom: 35}
                                    }),

                                    fontSize: responsiveFontSize(3)
                                }]}
                                >
                                    Seleccione un periodo
                                </Text> :
                                <View
                                    style={[
                                        styles.tabla,
                                        {
                                            borderColor: this.state.secondColor,
                                            marginTop: 5,
                                        }
                                    ]}
                                >
                                    {this.rndMatOtro()}
                                </View>}
                        </View>
                    }
                </ScrollView>
            </View>
        );
    }
}
