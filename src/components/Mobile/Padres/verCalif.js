import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    ScrollView,
    StatusBar
} from "react-native";
import Hijos from "../Globales/hijos";
import Periodos from "../Globales/Periodos";
import ModalSelector from "react-native-modal-selector";
import styles from "../../styles";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class verCalif extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            materias: [],
            evidencias: [],
            laMateria: [],
            selectedIndex: 0,
            selGrad: "",
            elHijo: [],
            elEncuadre: [],
            selGrup: "",
            selectedIndexPeriodo: -1,
            elPeriodo: ""
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onListItemPressedPeriodo(indexPeriodo, ciclo) {
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: ciclo
        });
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            elHijo: id
        });
        await this.getMaterias();
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia
        });
        await this.getVerEvidencias();
        await this.getVerEncuadres();
    }

    //++++++++++++++++++++++++++++++++++Encuadre++++++++++++++++++++++++++++++++++
    async getVerEncuadres() {
        let encuadrepicker = await fetch(
            this.state.uri +
            "/api/list/encuadre/" +
            this.state.elHijo +
            "/" +
            this.state.elPeriodo +
            "/" +
            this.state.laMateria,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let encuadrespicker = await encuadrepicker.json();
        await this.setState({elEncuadre: encuadrespicker});
    }

    renderEncuadre(itemEnc, indexEnc) {
        let b = this.state.elEncuadre.length - 1;
        let tabRowE = [styles.rowTabla];
        if (indexEnc !== b) {
            tabRowE.push(styles.btn2_3, {borderColor: this.state.secondColor});
        }
        const listae = this.isEncuadre(itemEnc, indexEnc);
        return (
            <View key={indexEnc} style={tabRowE}>
                {listae}
            </View>
        );
    }

    isEncuadre(itemEnc, indexEnc) {
        return (
            <View style={[styles.campoTablaG, styles.btn2]}>
                <Text>{itemEnc}</Text>
            </View>
        );
    }

    renderEncuadres() {
        let btnEnc = [];
        this.state.elEncuadre.forEach((itemEnc, indexEnc) => {
            btnEnc.push(this.renderEncuadre(itemEnc, indexEnc));
        });
        return btnEnc;
    }

    // ++++++++++++++++++++++++++++++ Evidencias +++++++++++++++++++++++++++++++++
    async getVerEvidencias() {
        let evidenciapicker = await fetch(
            this.state.uri +
            "/api/calificaciones/alumno/" +
            this.state.elHijo +
            "/" +
            this.state.elPeriodo +
            "/" +
            this.state.laMateria,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let evidenciaspicker = await evidenciapicker.json();
        this.setState({evidencias: evidenciaspicker});
    }

    renderEvidencia(itemEv, indexEv) {
        let a = this.state.evidencias.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexEv !== a) {
            tabRow.push(styles.btn3, {borderColor: this.state.secondColor});
        }
        const lista = this.isEvidencia(itemEv, indexEv);
        return (
            <View key={indexEv} style={tabRow}>
                {lista}
            </View>
        );
    }

    isEvidencia(itemEv, indexEv) {
        return (
            <View style={[styles.campoTablaG, styles.btn3]}>
                <View style={[styles.row, styles.btn4]}>
                    <Text>{itemEv.puntos}</Text>
                    <Text>{itemEv.obtenido}%</Text>
                </View>
            </View>
        );
    }

    renderEvidencias() {
        let btnEviden = [];
        this.state.evidencias.forEach((itemEv, indexEv) => {
            btnEviden.push(this.renderEvidencia(itemEv, indexEv));
        });
        return btnEviden;
    }

    //+++++++++++++++++++++++++++++Materias++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(
            this.state.uri +
            "/api/cordacad/get/materias/" +
            this.state.selGrad +
            "/" +
            this.state.selGrup,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    render() {
        const listEnc = this.renderEncuadres();
        const evidenciaList = this.renderEvidencias();
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <Hijos
                        onSelectedChamaco={(index, grado, grupo, id) =>
                            this.onListItemPressed(index, grado, grupo, id)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione la materia
                    </Text>
                    <ModalSelector
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        data={data}
                        onChange={option => this.onChange(option)}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        initValue="Seleccione la materia"
                    />
                    <View style={styles.califList}>
                        <View style={styles.califList}>
                            <View style={[styles.rowsCalif, {marginTop: 5}]}>
                                <Text style={{marginLeft: 239}}>Cant.</Text>
                                <Text style={{marginRight: 20}}>Valor</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View
                                style={[
                                    styles.btn2_3,
                                    styles.tablaEnc,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                {listEnc}
                                <View
                                    style={[
                                        styles.rowTablaAj,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                >
                                    <View style={[styles.campoTablaG, styles.btn2]}>
                                        <Text>-------</Text>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={[
                                    styles.tablaEnv,
                                    styles.btn3,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                {evidenciaList}
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
