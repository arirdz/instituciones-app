import React from 'react';
import {
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';

export default class pedidoNuevo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            total: '',
            iva: '',
            subTotal: ''
        };
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async componentWillMount() {
        await this.getURL();
        this.total();
    }

    number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }

    total() {
        let t = 0;
        for (let i = 0; i < this.props.pedido.length; i++) {
            if (this.props.pedido[i].subTotales !== 0)
                t = t + parseFloat(this.props.pedido[i].subTotales);
            this.props.pedido[i].subTotales = this.number_format(
                this.props.pedido[i].subTotales,
                2
            );
        }
        let i = parseFloat(t * 0.16);
        let s = t;
        t = t + i;

        this.setState({
            total: this.number_format(t, 2),
            iva: this.number_format(i, 2),
            subTotal: this.number_format(s, 2)
        });
    }

    renderLoQueSeaALVCompaFierroParienteCeroMiedo(item, index) {
        return (
            <View
                style={[
                    styles.campoTablaG,
                    {
                        borderColor: this.state.secondColor,
                        borderBottomWidth: 1
                    }
                ]}>
                <View style={[styles.row, {paddingTop: 0}]}>
                    <View style={[styles.btn5, styles.centered_RT]}>
                        <Text>{this.props.pedido[index].articulo}</Text>
                    </View>
                    <View style={[styles.btn5, styles.centered_RT]}>
                        <Text>{this.props.pedido[index].cantidad}</Text>
                    </View>
                    <View style={[styles.btn5, styles.centered_RT]}>
                        <Text>{this.props.pedido[index].talla}</Text>
                    </View>
                    <View style={[styles.btn5, styles.centered_RT]}>
                        <Text>{this.props.pedido[index].precio}</Text>
                    </View>
                    <View style={[styles.btn5, styles.centered_RT]}>
                        <Text>{this.props.pedido[index].subTotales}</Text>
                    </View>
                </View>
            </View>
        );
    }

    renderFac() {
        let facButtons = [];
        this.props.pedido.forEach((item, index) => {
            facButtons.push(
                this.renderLoQueSeaALVCompaFierroParienteCeroMiedo(item, index)
            );
        });
        return facButtons;
    }

    render() {
        const facturas = this.renderFac();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Confirma los detalles de pedido:
                    </Text>
                    <View underlayColor={this.state.secondColor}>
                        <View
                            style={[
                                styles.campoTablaG,
                                {borderColor: this.state.secondColor, borderBottomWidth: 1}
                            ]}>
                            <View style={[styles.row, {paddingTop: 0}]}>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Articulo</Text>
                                </View>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Cantidad</Text>
                                </View>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Talla</Text>
                                </View>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Precio</Text>
                                </View>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Subtotal</Text>
                                </View>
                            </View>
                        </View>
                        {facturas}
                        <View style={styles.row_factura}>
                            <View style={[styles.campoTablaG, styles.btn2_5]}>
                                <View style={[styles.row_factura]}>
                                    <View style={[styles.btn5]}>
                                        <Text style={{textAlign: 'right'}}>Sub total:</Text>
                                    </View>
                                    <View style={[styles.btn5]}>
                                        <Text style={{fontWeight: '800', textAlign: 'right'}}>
                                            {this.state.subTotal}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.row_factura}>
                            <View style={[styles.campoTablaG, styles.btn2_5]}>
                                <View style={[styles.row_factura]}>
                                    <View style={[styles.btn5]}>
                                        <Text style={{textAlign: 'right'}}>IVA:</Text>
                                    </View>
                                    <View style={[styles.btn5]}>
                                        <Text style={{fontWeight: '800', textAlign: 'right'}}>
                                            {this.state.iva}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.row_factura}>
                            <View style={[styles.campoTablaG, styles.btn2_5]}>
                                <View style={[styles.row_factura]}>
                                    <View style={[styles.btn5]}>
                                        <Text style={{textAlign: 'right'}}>Total:</Text>
                                    </View>
                                    <View style={[styles.btn5]}>
                                        <Text style={{fontWeight: '800', textAlign: 'right'}}>
                                            {this.state.total}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    onPress={() => Actions.confirmarUniformes()}
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: this.state.secondColor,
                            borderColor: this.state.secondColor
                        }
                    ]}>
                    <Text style={styles.textButton}>Realizar Pedido</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
