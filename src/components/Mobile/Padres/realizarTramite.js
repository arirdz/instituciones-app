import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native';
import {
    responsiveWidth,
    responsiveFontSize,
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import Hijos from '../Globales/hijos';
import styles from '../../styles';
import StepIndicator from 'rn-step-indicator';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');
const labels_sReinscrip = [
    'Iniciar el proceso',
    'Verificar disponibilidad',
    'Actualizar información',
    'Realizar el pago',
    'Finalizar'
];
const labels_estSimp = [
    'Solicitar constancia',
    'Realizar el pago',
    'Esperar liberación',
    'Se entrega por e-mail',
    'Confirmar y finalizar'
];
const labels_estForm = [
    'Solicitar',
    'Entregar foto',
    'Realizar el pago',
    'Esperar liberación',
    'Recoger en oficina',
    'Finalizar'
];
const lables_crBeca = [
    'Iniciar tramite',
    'Enviar carta',
    'Esperar resolución',
    'Finalizar'
];
const labels_sBeca = [
    'Iniciar la solicitud',
    'Definir el tipo de beca',
    'Entregar Documentos',
    'Esperar la resolución',
    'Confirmar y finalizar'
];

export default class realizarTramite extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            textInputValue: '',
            uri: '',
            grado: 'Todos',
            grupo: 'Todos',
            cososPicker: [],
            selectedIndex: null,
            ninosPicker: [],
            mes: moment().month() + 1,
            currentPosition: 0
        };
        this.getFeed = this.getFeed.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    onPageChange(position) {
        this.setState({currentPosition: position});
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getRole();
    }

    async getFeed() {
        let mes = this.state.mes;
        let grado = this.state.selGrad;
        let role = await AsyncStorage.getItem('role');
        let grupo = this.state.selGrup;
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(
            this.state.uri +
            '/api/feed/show/' +
            mes +
            '/' +
            role +
            '/' +
            grado +
            '/' +
            grupo,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        let data = await request.json();
        this.setState({data: data});
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    _keyExtractor = item => item.id;

    async onListItemPressed(index, grado, grupo) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo
        });
    }

    isNotPapa() {
        return (
            <View style={{marginBottom: 0, marginTop: 0}}>
                <Hijos
                    onSelectedChamaco={(index, grado, grupo) =>
                        this.onListItemPressed(index, grado, grupo)
                    }
                />
            </View>
        );
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, selected: option.id});
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    iniciar() {
        if (this.state.selected === 'sReinscrip') {
            Actions.solicitudReinscrip({data: this.state.selected});
        } else if (this.state.selected === 'estSimp') {
            Actions.constEstSimp({data: this.state.selected});
        } else if (this.state.selected === 'estForm') {
            Actions.constEstForm({data: this.state.selected});
        } else if (this.state.selected === 'crBeca') {
            Actions.cartRenvBeca({data: this.state.selected});
        } else if (this.state.selected === 'sBeca') {
            Actions.solBeca({data: this.state.selected});
        }
    }

    render() {
        let customStyles = {
            stepIndicatorSize: 25,
            currentStepIndicatorSize: 30,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: '#777777',
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: '#777777',
            stepStrokeUnFinishedColor: '#aaaaaa',
            separatorFinishedColor: '#777777',
            separatorUnFinishedColor: '#aaaaaa',
            stepIndicatorFinishedColor: '#777777',
            stepIndicatorUnFinishedColor: '#ffffff',
            stepIndicatorCurrentColor: '#ffffff',
            stepIndicatorLabelFontSize: 13,
            currentStepIndicatorLabelFontSize: 13,
            stepIndicatorLabelCurrentColor: '#777777',
            stepIndicatorLabelFinishedColor: '#ffffff',
            stepIndicatorLabelUnFinishedColor: '#aaaaaa',
            labelColor: '#aaaaaa',
            labelSize: responsiveFontSize(1.3),
            currentStepLabelColor: '#777777'
        };
        let index = 0;
        const data = [
            {key: index++, label: 'Solicitud de reinscripción', id: 'sReinscrip'},
            {key: index++, label: 'Constancia de estudios simple', id: 'estSimp'},
            {key: index++, label: 'Constancia de estudios formal', id: 'estForm'},
            {key: index++, label: 'Carta de renovación de beca', id: 'crBeca'},
            {key: index++, label: 'Solicitud de beca', id: 'sBeca'}
        ];
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView>
                    <View style={{marginTop: 0}}>
                        {this.state.admin == 'Padre' ? this.isNotPapa() : null}
                    </View>
                    <View style={[styles.titulo_N, {marginBottom: 25, marginTop: 0}]}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Indique el trámite que desea realizar
                        </Text>
                    </View>
                    <ModalSelector
                        data={data}
                        style={{
                            width: responsiveWidth(94),
                            borderColor: this.state.fourthColor
                        }}
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        initValue="Seleccione el documento"
                        onChange={option => this.onChange(option)}
                    />
                    <Text style={styles.main_title}>
                        Este es el procedimiento a seguir:
                    </Text>
                    <View
                        style={{
                            width: responsiveWidth(94),
                            marginTop: 10,
                            marginBottom: 8
                        }}>
                        {this.state.selected === 'sReinscrip' ? (
                            <StepIndicator
                                customStyles={customStyles}
                                stepCount={5}
                                currentPosition={this.state.currentPosition}
                                labels={labels_sReinscrip}
                            />
                        ) : this.state.selected === 'estSimp' ? (
                            <StepIndicator
                                customStyles={customStyles}
                                stepCount={5}
                                currentPosition={this.state.currentPosition}
                                labels={labels_estSimp}
                            />
                        ) : this.state.selected === 'estForm' ? (
                            <StepIndicator
                                customStyles={customStyles}
                                stepCount={6}
                                currentPosition={this.state.currentPosition}
                                labels={labels_estForm}
                            />
                        ) : this.state.selected === 'crBeca' ? (
                            <StepIndicator
                                customStyles={customStyles}
                                stepCount={4}
                                currentPosition={this.state.currentPosition}
                                labels={lables_crBeca}
                            />
                        ) : this.state.selected === 'sBeca' ? (
                            <StepIndicator
                                customStyles={customStyles}
                                stepCount={5}
                                currentPosition={this.state.currentPosition}
                                labels={labels_sBeca}
                            />
                        ) : null}
                    </View>

                    {this.state.selected === 'sReinscrip' ? (
                        <View
                            style={[
                                styles.infoRt,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    borderColor: this.state.fourthColor
                                }
                            ]}>
                            <Text>
                                Este trámite está disponible en días y horas hábiles{'\n'}
                                del 1 de Febrero al 31 de Marzo del presente ciclo escolar
                            </Text>
                        </View>
                    ) : this.state.selected === 'estSimp' ? (
                        <View
                            style={[
                                styles.infoRt,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    borderColor: this.state.fourthColor
                                }
                            ]}>
                            <Text>
                                Este trámite está disponible esta disponible{'\n'}
                                durante todo el ciclo escolar las 24hrs los 7 días
                            </Text>
                        </View>
                    ) : this.state.selected === 'estForm' ? (
                        <View
                            style={[
                                styles.infoRt,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    borderColor: this.state.fourthColor
                                }
                            ]}>
                            <Text>
                                Este trámite está disponible en días y horas hábiles{'\n'}
                                durante todo el ciclo escolar
                            </Text>
                        </View>
                    ) : this.state.selected === 'crBeca' ? (
                        <View
                            style={[
                                styles.infoRt,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    borderColor: this.state.fourthColor
                                }
                            ]}>
                            <Text>
                                Este trámite está disponible en días y horas hábiles{'\n'}
                                del 1 al 31 de Julio del presente ciclo escolar
                            </Text>
                        </View>
                    ) : this.state.selected === 'sBeca' ? (
                        <View
                            style={[
                                styles.infoRt,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    borderColor: this.state.fourthColor
                                }
                            ]}>
                            <Text>
                                Este trámite está disponible en días y horas hábiles{'\n'}
                                del 1 al 31 de Julio del presente ciclo escolar
                            </Text>
                        </View>
                    ) : null}
                    <Text style={{marginLeft: 20, marginTop: 10, marginBottom: 1}}>
                        Puede realizar el trámite desde aquí:
                    </Text>
                </ScrollView>
                <View>
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            {
                                marginTop: 1,
                                backgroundColor: this.state.secondColor,
                                borderColor: this.state.secondColor
                            }
                        ]}
                        onPress={() => this.iniciar()}>
                        <Text style={styles.textButton}>Iniciar trámite</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
