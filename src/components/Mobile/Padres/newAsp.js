import React from 'react';
import {
	Alert, AsyncStorage, Keyboard, KeyboardAvoidingView, Platform, RefreshControl, ScrollView, StatusBar, Text,
	TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import ModalSelector from 'react-native-modal-selector';
import {Fumi} from 'react-native-textinput-effects';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import {TextInputMask} from 'react-native-masked-text';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class newAsp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			datos: [],
			elGrado: '1',
			maincolor: '#fff',
			secondColor: '#fff',
			thirdColor: '#fff',
			fourthColor: '#fff',
			selectedIndexGrados: -1,
			grados: '6',
			tipoAlta: 'ingreso',
			elGenero: '',
			elPromedio: '',
			escuela: '',
			curpAlumn: '',
			nombreAlumn: '',
			apellidPatAlumn: '',
			apellidMatAlumn: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getUserdata();
		await this.getUserdataV2();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getUserdataV2() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				Authorization: 'Bearer ' + token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 4)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({data: responseJson});
				}
			});
	}

	async getUserdata() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data', {
			method: 'GET', headers: {
				Authorization: 'Bearer ' + token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos',
						'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 7)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({datos: responseJson});
				}
			});
	}

	//-+-+-+-+-+-+-+-grados
	async onListItemPressedGrado(i) {
		await this.setState({elGrado: i + 1, selectedIndexGrados: i});
	}

	rndGrados() {
		let grados = [];
		for (let i = 0; i < this.state.grados; i++) {
			let smallButtonStyles = [
				styles.listButton,
				{
					borderColor: this.state.secondColor,
					height: responsiveHeight(4),
					width: responsiveWidth(11)
				}
			];
			let texto = [styles.textoN, {fontSize: responsiveFontSize(1.5)}];
			if (this.state.selectedIndexGrados === i) {
				smallButtonStyles.push(styles.listButtonSelected, {
					backgroundColor: this.state.secondColor
				});
				texto.push(styles.textoB, {fontSize: responsiveFontSize(1.5)});
			}
			grados.push(
				<TouchableHighlight
					key={i + 'gdo'}
					underlayColor={this.state.secondColor}
					style={smallButtonStyles}
					onPress={() => this.onListItemPressedGrado(i)}
				>
					<View style={styles.listItem}>
						<Text style={texto}>{i + 1}</Text>
					</View>
				</TouchableHighlight>
			);
		}
		return grados;
	}

//-+-+-+-+-+-+-+-+-+-+-+-+-+--++-
	async alertNewAps() {
		if (this.state.nombreAlumn === '') {
			Alert.alert('Campo nombre vacío', 'Escriba el nombre del aspirante para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.apellidPatAlumn === '') {
			Alert.alert('Campo apellido paterno vacío', 'Escriba el apellido paterno para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.apellidMatAlumn === '') {
			Alert.alert('Campo apellido materno vacío', 'Escriba el apellido materno para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.curpAlumn === '') {
			Alert.alert('Campo C.U.R.P vacío', 'Escriba la curp del aspirante para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.elGenero === '') {
			Alert.alert('Campo genero vacío', 'Defina el genero del aspirante para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacío', 'Defina el grado de interes del aspirante', [
				{text: 'Enterado'}
			])
		} else if (this.state.escuela === '') {
			Alert.alert('Escuela indefinida', 'CCT de escuela indefinida', [
				{text: 'Enterado'}
			])
		} else if (this.state.elPromedio === '') {
			Alert.alert('Campo ultimo promedio general vacío',
				'Asegurese de llenar todos correctamente este campo que no sea mayor a 10 o menor a 0 para agregar otro aspirante', [
					{text: 'Enterado'}
				])
		} else {
			Alert.alert('Añadir aspirante',
				'Está a punto de añadir a un nuevo aspirante ¿Seguro que desea continuar?', [
					{text: 'Sí', onPress: () => this.requestMultipart()}, {text: 'No'}
				])
		}
	}

	async requestMultipart() {
		if (this.props.id_registro === null || this.props.id_registro === '' || this.props.id_registro === ' ') {
			await fetch(this.state.uri + '/api/post/nuevo/tutor', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json'
				}, body: JSON.stringify({
					nombre: this.state.datos.nombre,
					apellidos: this.state.datos.apellido_paterno + ' ' + this.state.datos.apellido_materno,
					genero: this.state.data.genero,
					telefono_contacto: this.state.datos.telefono_celular_tutor,
					email: this.state.data.email,
					password: '',
					PushNotificationsID: this.state.data.PushNotificationsID
				})
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al enviar la solicitud', 'Ha ocurrido un error ' + responseJson.error.status_code +
							' al tratar de enviar la solicitud de infores, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
							[{
								text: 'Entendido',
								onPress: () => [Actions.pop({refresh: {key: 'Drawer'}})]
							}]);
					} else {
						this.requestMultipart2(responseJson.id);
						this.updateRegistro(responseJson.id);
					}
				});
		} else {
			this.requestMultipart2(this.props.id_registro);
		}
	}

	async updateRegistro(id_registro) {
		await fetch(this.state.uri + '/api/update/registro/' + this.state.data.id + '/' + id_registro, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}
		}).then(res => res.json())
			.then(responseJson => console.log('id_registro', responseJson));
	}

	async requestMultipart2(id_tutor) {
		await fetch(this.state.uri + '/api/new/aspirante/user', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}, body: JSON.stringify({
				id_tutor: id_tutor,
				nombre_aspirante: this.state.nombreAlumn + ' ' + this.state.apellidPatAlumn + ' ' + this.state.apellidMatAlumn,
				curp_aspirante: this.state.curpAlumn,
				genero: this.state.elGenero,
				grado_interes: this.state.elGrado,
				estado_escuela: '0',
				escuela_procedente: this.state.escuela,
				promedio_gral: this.state.elPromedio,
				paso_registro: '1',
				fecha_cita: '',
				hora_cita: '',
				tipo_alta: this.state.tipoAlta,
				estatus_alta: 'en proceso',
				id_user: this.props.id_user
			})
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar la solicitud', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de enviar la solicitud de infores, si el error continua pónganse en contacto con soporte (Aspirante nuevo)',
						[{
							text: 'Entendido',
							onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					Alert.alert('Nuevo aspirante añadido', 'Se ha añadido un nuevo aspirante', [
						{text: 'Enterado', onPress: () => Actions.pop({refresh: {key: Math.random()}})}
					]);
				}
			});

	}

	//-+-+-+-+-+-+-+-+++-
	async onChangeTipo(option) {
		this.state.tipoAlta = option;
		if (this.state.tipoAlta === 'ingreso') {
			this.state.selectedIndexGrados = 0;
			this.state.elGrado = '1';
		} else {
			this.state.selectedIndexGrados = -1;
			this.state.elGrado = '';
		}
		await this.setState({aux: 0});
	}

	async onChangeGenero(option) {
		this.state.elGenero = option.label;
		await this.setState({aux: 0});
	}

	async cct(text) {
		let texto = text;
		this.state.escuela = texto.toUpperCase();
		await this.setState({aux: 0});
	}

	async curp(text) {
		let texto = text;
		this.state.curpAlumn = texto.toUpperCase();
		await this.setState({aux: 0});
	}

	render() {
		let tipo_alta = this.state.tipoAlta;
		let index = 0;
		const data = [
			{key: index++, label: 'Masculino', activo: '1'},
			{key: index++, label: 'Femenino', activo: '0'}
		];
		return (
			<KeyboardAvoidingView
				style={[styles.container]}
				behavior={Platform.OS === 'ios' ? 'padding' : null}
				keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : 0}
				enabled
			>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<ScrollView
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{...ifIphoneX({marginBottom: 38}, {marginBottom: 20})}}
				>
					<Text style={[styles.main_title, {
						color: this.state.thirdColor
					}]}>
						Capture los datos del nuevo aspirante
					</Text>
					<Text style={[styles.main_title, {color: '#000', fontSize: responsiveFontSize(1.6)}]}>
						1) Seleccione una opción
					</Text>
					<View style={[styles.row, {marginTop: 5}]}>
						<TouchableOpacity
							style={[styles.btn2, {
								borderRadius: 6,
								padding: 3,
								backgroundColor: tipo_alta === 'ingreso' ? this.state.mainColor : this.state.fourthColor
							}]}
							onPress={() => this.onChangeTipo('ingreso')}
						>
							<Text style={[styles.textW, {
								textAlign: 'center',
								color: tipo_alta === 'ingreso' ? '#fff' : '#000',
								fontSize: responsiveFontSize(2),
								marginVertical: 3
							}]}>
								Nuevo ingreso
							</Text>
							<Text
								style={{
									textAlign: 'center',
									color: tipo_alta === 'ingreso' ? '#fff' : '#000',
									fontSize: responsiveFontSize(1.3),
									lineHeight: responsiveHeight(1.5),
									marginBottom: 4
								}}
							>
								Primera vez{'\n'}a primer grado
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[styles.btn2, {
								borderRadius: 6,
								padding: 3,
								backgroundColor: tipo_alta === 'traslado' ? this.state.mainColor : this.state.fourthColor
							}]}
							onPress={() => this.onChangeTipo('traslado')}
						>
							<Text style={[styles.textW, {
								textAlign: 'center',
								color: tipo_alta === 'traslado' ? '#fff' : '#000',
								fontSize: responsiveFontSize(2),
								marginVertical: 3
							}]}>
								Traslado
							</Text>
							<Text
								style={{
									textAlign: 'center',
									color: tipo_alta === 'traslado' ? '#fff' : '#000',
									fontSize: responsiveFontSize(1.3),
									lineHeight: responsiveHeight(1.5),
									marginBottom: 4
								}}
							>
								Mediados del{'\n'}ciclo escolar
							</Text>
						</TouchableOpacity>
					</View>
					<Text style={[styles.main_title, {color: '#000', fontSize: responsiveFontSize(1.6)}]}>
						2) Complete la información
					</Text>
					<Fumi
						label={'Nombre(s)'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.nombreAlumn = text)}
						value={this.state.nombreAlumn}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.apellidoPAsp.focus()}
					/>
					<Fumi
						ref='apellidoPAsp'
						label={'Apellido paterno'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.apellidPatAlumn = text)}
						value={this.state.apellidPatAlumn}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.apellidoMAsp.focus()}
					/>
					<Fumi
						ref='apellidoMAsp'
						label={'Apellido materno'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-person'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						onChangeText={text => (this.state.apellidMatAlumn = text)}
						value={this.state.apellidMatAlumn}
						keyboardType='default'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.curpAsp.focus()}
					/>
					<Fumi
						ref='curpAsp'
						label={'C.U.R.P del aspirante'}
						iconClass={Ionicons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'md-paper'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						maxLength={18}
						onChangeText={text => this.curp(text)}
						value={this.state.curpAlumn}
						keyboardType='default'
						returnKeyType='next'
						autoCapitalize='none'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.cctAsp.focus()}
					/>
					<ModalSelector
						data={data}
						selectStyle={[
							styles.inputPicker,
							{borderColor: this.state.secondColor, marginTop: 5}
						]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor}}
						initValue='Género del aspirante'
						onChange={option => this.onChangeGenero(option)}
					/>
					{this.state.tipoAlta === 'traslado' ?
						<View style={[styles.widthall, {alignItems: 'center', marginVertical: 8}]}>
							<Text
								style={[styles.subTitulo, styles.widthall, {
									textAlign: 'center',
									color: this.state.mainColor
								}]}>
								Seleccione un grado de interés
							</Text>
							<View style={[styles.buttonsRow, {marginVertical: 5}]}>{this.rndGrados()}</View>
						</View> : null}
					<Fumi
						ref='cctAsp'
						label={'CCT de la escuela de procedencia (Clave)'}
						iconClass={FontAwesome}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'building-o'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						maxLength={10}
						onChangeText={text => this.cct(text)}
						value={this.state.escuela}
						keyboardType='default'
						returnKeyType='next'
						autoCapitalize='none'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.promedioAsp.focus()}
					/>
					<Fumi
						ref='promedioAsp'
						label={'Ultimo promedio general'}
						iconClass={MaterialCommunityIcons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'school'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={22}
						maxLength={4}
						onChangeText={text => (this.state.elPromedio = text)}
						value={this.state.elPromedio}
						keyboardType='numeric'
						returnKeyType='next'
						autoCorrect={false}
						onSubmitEditing={() => Keyboard.dismiss()}
					/>
					<TouchableOpacity
						style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
						onPress={() => this.alertNewAps()}
					>
						<Text style={styles.textButton}>
							Guardar aspirante
						</Text>
					</TouchableOpacity>
				</ScrollView>
			</KeyboardAvoidingView>
		);
	}
}
