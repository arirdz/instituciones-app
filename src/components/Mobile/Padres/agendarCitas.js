import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Hijos from '../Globales/hijos';
import styles from '../../styles';
import MultiBotonRow from '../Globales/MultiBotonRow';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';
import CitasPadre from './citasPadre';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Feather from 'react-native-vector-icons/Feather';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class agendarCitas extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedIndexAsunto: -1,
			selectedIndexAsunto2: -1,
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			selectedIndexDia: -1,
			selectedIndexDia2: -1,
			laHora: '',
			laHora2: '',
			dia: '',
			dia2: '',
			mes: '',
			mes2: '',
			elAsunto: '',
			elAsunto2: '',
			elDia: '',
			elDia2: '',
			asuntos: [],
			data: [],
			dias: [],
			botonSelected: 'Solicitar una cita',
			elAdmin: [],
			elAdminId: '',
			elAdminId2: '',
			elHijo: '',
			elIdMaestro: '',
			elMaestro: [],
			elReceptor: '',
			elTipo: '',
			selectedIndex: -1,
			selGrad: '',
			selGrup: '',
			horas: [],
			items: {},
			laMateria: '',
			laMateria2: '',
			nombreMat: '',
			nombreMat2: '',
			materias: [],
			tipoAsuntos: [],
			textInputValue: '',
			tipos: [],
			laHora1: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getAdmin();
		await this.getTipoAsuntos();
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
	}

	async onListItemPressed(index, grado, grupo, id) {
		await this.setState({
			selectedIndex: index,
			selGrad: grado,
			selGrup: grupo,
			elHijo: id,
			selectedIndexAsunto: -1,
			selectedIndexDia: -1,
			selectedIndexHora: -1,
			selectedIndexMateria: -1
		});
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected, indexSelected: indexSelected
		});
	}

	async getAdmin() {
		let admin = await fetch(this.state.uri + '/api/datos/adminId', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let adminId = await admin.json();
		await this.setState({elAdmin: adminId});
	}

	// +++++++++++++++++++++++++++ ASUNTOS ++++++++++++++++++++++++++++
	async getAsuntos() {
		let tipo = this.state.elTipo;
		let liAsunt = await fetch(this.state.uri + '/api/feed/view/asuntos/' + tipo, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let listAsunt = await liAsunt.json();
		this.setState({asuntos: listAsunt});
	}

	async onListPressedAsunto(indexAsunto, asunto) {
		await this.setState({selectedIndexAsunto: indexAsunto, elAsunto: asunto});
	}

	renderAsunto(itemAsunto, indexAsunto) {
		let a = this.state.asuntos.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexAsunto !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexAsunto === indexAsunto) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (<TouchableHighlight
			key={indexAsunto}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.onListPressedAsunto(indexAsunto, itemAsunto.asunto)}
		>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>{itemAsunto.asunto}</Text>
			</View>
		</TouchableHighlight>);
	}

	renderAsuntos() {
		let buttonsAsuntos = [];
		this.state.asuntos.forEach((itemAsunto, indexAsunto) => {
			buttonsAsuntos.push(this.renderAsunto(itemAsunto, indexAsunto));
		});
		return buttonsAsuntos;
	}

	// +++++++++++++++++++++++++++++++++++++DIAS++++++++++++++++++++++++++++++++++

	async getDias() {
		this.setState({dias: []});
		if (this.state.elTipo !== 'Académicas') {
			await fetch(this.state.uri + '/api/feed/view/dias_dis/' + this.state.elAdminId, {
				method: 'GET', headers: {
					'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert(
							'Error al cargar los dias', 'Ha ocurrido un error ' +
							responseJson.error.status_code +
							' al tratar de cargar los dias de disponibilidad si el error continua pónganse en contacto con soporte (Citas)',
							[{text: 'Entendido'}]
						);
					} else {

						if (responseJson.length === 0) {
							this.state.dias.push(
								{dia_atencion: '1'}, {dia_atencion: '2'}, {dia_atencion: '3'}, {dia_atencion: '4'}, {dia_atencion: '5'}
							);
						} else {
							this.setState({dias: responseJson});
						}
					}
				});
		} else {
			await fetch(this.state.uri + '/api/feed/view/dias_dis/' + this.state.elIdMaestro, {
				method: 'GET', headers: {
					'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert(
							'Error al cargar los dias', 'Ha ocurrido un error ' +
							responseJson.error.status_code +
							' al tratar de cargar los dias de disponibilidad si el error continua pónganse en contacto con soporte (Citas)',
							[{text: 'Entendido'}]
						);
					} else {
						if (responseJson.length === 0) {
							this.state.dias.push(
								{dia_atencion: '1'}, {dia_atencion: '2'}, {dia_atencion: '3'}, {dia_atencion: '4'}, {dia_atencion: '5'}
							);
						} else {
							this.setState({dias: responseJson});
						}
					}
				});
		}
		await this.setState({aux: 0});
	}

	async onListItemPressedDia(index, dia, mes, sdia) {
		await this.setState({
			selectedIndexDia: index, elDia: dia, mes: mes, dia: sdia, selectedIndexHora: -1, laHora: ''
		});
		await this.getHoras();
	}

	renderDia(itemDia, indexDia) {
		let a = this.state.dias.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexDia !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexDia === indexDia) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		let nombreDia = '';
		let num = parseInt(itemDia.dia_atencion) - 1;
		let sdia = 1;
		let mes = 0;
		if (num < moment().weekday()) {
			sdia = moment().weekday(num + 7);
			mes = sdia.format('MMMM');
			sdia = sdia.format('D');
		} else {
			sdia = moment().weekday(num);
			mes = sdia.format('MMMM');
			sdia = sdia.format('D');
		}
		if (itemDia.dia_atencion === '1') {
			nombreDia = 'Lunes';
		}
		if (itemDia.dia_atencion === '2') {
			nombreDia = 'Martes';
		}
		if (itemDia.dia_atencion === '3') {
			nombreDia = 'Miércoles';
		}
		if (itemDia.dia_atencion === '4') {
			nombreDia = 'Jueves';
		}
		if (itemDia.dia_atencion === '5') {
			nombreDia = 'Viernes';
		}
		return (<TouchableHighlight
			key={indexDia}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.onListItemPressedDia(indexDia, itemDia.dia_atencion, mes, sdia)}
		>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>
					{nombreDia} {sdia} {mes}
				</Text>
			</View>
		</TouchableHighlight>);
	}


	renderDias() {
		let buttonsDias = [];
		this.state.dias.forEach((itemDia, indexDia) => {
			buttonsDias.push(this.renderDia(itemDia, indexDia));
		});
		return buttonsDias;
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++HORAS+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	async getDataUser(id) {
		await fetch(this.state.uri + '/api/datos/user/' + id, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en horas de atencion',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					this.setState({
						info: responseJson[0].nombre + ' ' + responseJson[0].apellido_paterno + ' ' + responseJson[0].apellido_materno
					});
				}
			});
	}

	showInfo() {
		return (<View style={{justifyContent: 'center'}}>
			<Text style={{fontWeight: '900', textAlign: 'center'}}>
				{this.state.info}
			</Text>
			{this.state.elTipo === 'Directivo' ? (
				<Text style={{textAlign: 'center', marginTop: 5, fontSize: responsiveFontSize(1.9)}}>
					Subdirector
				</Text>
			) : this.state.elTipo === 'Institucional' ? (
				<Text style={{textAlign: 'center', marginTop: 5, fontSize: responsiveFontSize(1.9)}}>
					Director
				</Text>
			) : this.state.elTipo === 'Administrativas' ? (
				<Text style={{textAlign: 'center', marginTop: 5, fontSize: responsiveFontSize(1.9)}}>
					Administrador
				</Text>) : (<Text numberOfLines={1} style={[styles.btn3_5, {
				textAlign: 'center',
				marginTop: 5,
				fontSize: responsiveFontSize(1.9)
			}]}>
				Maestro de {this.state.nombreMat}
			</Text>)}
		</View>);
	}


	async getHoras() {
		await this.setState({horas: []});
		if (this.state.elTipo !== 'Académicas') {
			await fetch(this.state.uri + '/api/feed/view/horarios_at/' + this.state.elAdminId + '/' + this.state.elDia, {
				method: 'GET', headers: {
					'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error en horas de atencion',
							'Ha ocurrido un error ' + responseJson.error.status_code +
							' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
					} else {
						if (responseJson.length === 0) {
							this.getHorasTodas();
						} else {
							this.setState({horas: responseJson});
						}
					}
				});
		} else {
			await fetch(this.state.uri + '/api/feed/view/horarios_at/' + this.state.elIdMaestro + '/' + this.state.elDia, {
				method: 'GET', headers: {
					'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error en horas de atencion',
							'Ha ocurrido un error ' + responseJson.error.status_code +
							' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
					} else {
						if (responseJson.length === 0) {
							this.getHorasTodas();
						} else {
							this.setState({horas: responseJson});
						}
					}
				});
		}
	}

	async getHorasTodas() {
		await fetch(this.state.uri + '/api/feed/view/horas/disponibles', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en horas de atencion',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					this.setState({horas: responseJson});
				}
			});
	}

	async onListItemPressedHora(index, hora1) {
		await this.setState({
			selectedIndexHora: index,
			laHora: moment(hora1, 'HH:mm:ss').format('HH:mm')
		});

	}

	renderHora(itemHora, indexHora) {
		let a = this.state.horas.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexHora !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexHora === indexHora) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (<TouchableHighlight
			key={indexHora}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.onListItemPressedHora(indexHora, itemHora.horas2.inicio)}
		>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>{itemHora.horas2.horas}</Text>
			</View>
		</TouchableHighlight>);
	}

	renderHoras() {
		let buttonsHoras = [];
		this.state.horas.forEach((itemHora, indexHora) => {
			buttonsHoras.push(this.renderHora(itemHora, indexHora));
		});
		return buttonsHoras;
	}

	// ++++++++++++++++++++++++++++++++++++++GUARDAR++++++++++++++++++++++++++++++

	requestMultipart() {
		if (this.state.elReceptor2 === '' || this.state.elDia2 === '' || this.state.elAsunto2 === '' || this.state.laHora2 === '') {
			Alert.alert('Datos incompletos', 'Falta algún dato, asegurese de llenar todos los campos correspondientes por favor');
		} else {

			let formData = new FormData();
			formData.append('new', JSON.stringify({
				id_receptor: this.state.elReceptor2,
				padre_id: 0,
				hijo_id: this.state.elHijo,
				fecha_cita: this.state.dia2 + ' de ' + this.state.mes2,
				hora: this.state.laHora2,
				asunto: this.state.elAsunto2,
				aprobada: 0
			}));

			fetch(this.state.uri + '/api/feed/citas', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				}, body: formData
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al enviar la cita', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Citas)', [{
							text: 'Entendido', onPress: () => Actions.drawer()
						}]);
					} else {
						this.setState({idNoticia: responseJson.id});
						Alert.alert('Cita enviada', 'Se ha solicitado la cita con éxito', [{
							text: 'Entendido', onPress: () => Actions.HomePage()
						}]);
					}
				});
		}
	}

	// +++++++++++++++++++++++++++++++++++++MATERIAS+++++++++++++++++++++++++++++

	async onChange(option) {
		await this.setState({
			laMateria: option.materia,
			selectedIndexMateria: option.key,
			nombreMat: option.label,
			info: '',
			selectedIndexDia: -1,
			selectedIndexHora: -1,
			laMateria2: '',
			nombreMat2: '',
			elIdMaestro: '',
			elIdMaestro2: '',
			selectedIndexHora2: -1,
			selectedIndexDia2: -1,
			laHora: '',
			laHora2: '',
			dia: '',
			dia2: '',
			mes: '',
			mes2: '',
			elDia: '',
			elDia2: ''
		});
		await this.getMaestro();
	}

	async getMaestro() {
		let maestroPicker = await fetch(this.state.uri + '/api/feed/view/maestros/' + this.state.laMateria + '/' + this.state.selGrup, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let maestrosPicker = await maestroPicker.json();
		await this.setState({elMaestro: maestrosPicker.map(item => item)});
		await this.setState({elIdMaestro: this.state.elMaestro[0].id});
		await this.setState({elIdMaestro: parseInt(this.state.elIdMaestro)});
		await this.getDias();
	}

	async getMaterias() {
		let materiaPicker = await fetch(this.state.uri + '/api/feed/view/materias/' + this.state.selGrad, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let materiasPicker = await materiaPicker.json();
		this.setState({materias: materiasPicker});
	}

	// +++++++++++++++++++++++++++++++++++++EXTRA++++++++++++++++++++++++++++++++++
	async getTipoAsuntos() {
		await fetch(this.state.uri + '/api/feed/view/tiposasuntos', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({tipoAsuntos: responseJson});
			});
	}

	async onChange1(option1) {
		await this.setState({
			elTipo: option1.label,
			info: '',
			laMateria: '',
			laMateria2: '',
			nombreMat: '',
			nombreMat2: '',
			elIdMaestro: '',
			elIdMaestro2: '',
			elAdminId2: '',
			selectedIndexAsunto: -1,
			selectedIndexAsunto2: -1,
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			selectedIndexDia: -1,
			selectedIndexDia2: -1,
			laHora: '',
			laHora2: '',
			dia: '',
			dia2: '',
			mes: '',
			mes2: '',
			elDia: '',
			elDia2: '',
			elAsunto: '',
			elAsunto2: '',
			horas: []
		});
		if (option1.label === 'Académicas') {
			this.state.elAdmin.forEach((item) => {
				if (item.puesto === 'Coordinador Academico') {
					this.setState({elAdminId: item.id});
				}
			});
		} else if (option1.label === 'Administrativas') {
			this.state.elAdmin.forEach((item) => {
				if (item.puesto === 'Secretaria') {
					this.setState({elAdminId: item.id});
				}
			});
		} else if (option1.label === 'Conductuales') {
			this.state.elAdmin.forEach((item) => {
				if (item.puesto === 'Cordisi') {
					this.setState({elAdminId: item.id});
				}
			});
		} else if (option1.label === 'Institucional') {
			this.state.elAdmin.forEach((item) => {
				if (item.puesto === 'Director') {
					this.setState({elAdminId: item.id});
				}
			});
		} else if (option1.label === 'Directivo') {
			this.state.elAdmin.forEach((item) => {
				if (item.puesto === 'SubDirector') {
					this.setState({elAdminId: item.id});
				}
			});
		}
		await this.getAsuntos();
		if (this.state.elTipo !== 'Académicas') {
			await this.getDias();
		} else if (this.state.elTipo === 'Académicas') {
			await this.getMaterias();
		}
	}

	//*********************************** Asuntos +++++++++++++++++++++++++++++++++
	async openModalAsunto() {
		if (this.state.elTipo === '') {
			Alert.alert('Campo categoria vacío', 'Seleccione una categoria para continuar', [
				{text: 'Enterado'}
			])
		} else {
			this.state.elAsunto = this.state.elAsunto2;
			this.state.selectedIndexAsunto = this.state.selectedIndexAsunto2;
			await this.setState({isModalCateg: true})
		}
	}

	async closeModalAsunto() {
		if (this.state.selectedIndexAsunto === -1) {
			Alert.alert('Asunto sin seleccionar', 'Seleccione un asunto para continuar', [
				{text: 'Enterado'}
			])
		} else {
			this.state.elAsunto2 = this.state.elAsunto;
			this.state.selectedIndexAsunto2 = this.state.selectedIndexAsunto;
			await this.setState({isModalCateg: false})
		}
	}

	//*********************************** Dias +++++++++++++++++++++++++++++++++
	async openModalDia() {
		if (this.state.selectedIndexAsunto2 === -1) {
			Alert.alert('Asunto sin seleccionar', 'Seleccione un asunto para continuar', [
				{text: 'Enterado'}
			]);
		} else {
			if (this.state.elTipo === 'Académicas') {
				this.state.laMateria = this.state.laMateria2;
				this.state.nombreMat = this.state.nombreMat2;
				this.state.elIdMaestro = this.state.elIdMaestro2;
			}
			this.state.elDia = this.state.elDia2;
			this.state.laHora = this.state.laHora2;
			this.state.selectedIndexDia = this.state.selectedIndexDia2;
			this.state.selectedIndexHora = this.state.selectedIndexHora2;
			this.state.dia = this.state.dia2;
			this.state.mes = this.state.mes2;
			await this.setState({isModalDia: true});
		}
	}

	async closeModalDia() {
		if (this.state.elTipo === 'Académicas' && this.state.laMateria === '') {
			Alert.alert('Campo materia vacío', 'Seleccione una materia para continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.elDia === '') {
			Alert.alert('Campo dia vacío', 'Seleccione un dia para continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.laHora === '') {
			Alert.alert('Campo hora vacío', 'Seleccione una hora para continuar', [
				{text: 'Enterado'}
			]);
		} else {
			if (this.state.elTipo === 'Académicas') {
				this.state.laMateria2 = this.state.laMateria;
				this.state.nombreMat2 = this.state.nombreMat;
				this.state.elIdMaestro2 = this.state.elIdMaestro;
			}
			this.state.elDia2 = this.state.elDia;
			this.state.laHora2 = this.state.laHora;
			this.state.selectedIndexDia2 = this.state.selectedIndexDia;
			this.state.selectedIndexHora2 = this.state.selectedIndexHora;
			this.state.dia2 = this.state.dia;
			this.state.mes2 = this.state.mes;
			await this.setState({isModalDia: false});
			if (this.state.elTipo !== 'Académicas') {
				await this.setState({elReceptor2: this.state.elAdminId});
				await this.getDataUser(this.state.elAdminId);
			} else {
				await this.setState({elReceptor2: this.state.elIdMaestro2});
				await this.getDataUser(this.state.elIdMaestro2);
			}
		}
	}

	//*********************************** Render +++++++++++++++++++++++++++++++++
	solCita() {
		let nombreDia = '';
		if (this.state.elDia2 === '1') {
			nombreDia = 'Lunes';
		}
		if (this.state.elDia2 === '2') {
			nombreDia = 'Martes';
		}
		if (this.state.elDia2 === '3') {
			nombreDia = 'Miércoles';
		}
		if (this.state.elDia2 === '4') {
			nombreDia = 'Jueves';
		}
		if (this.state.elDia2 === '5') {
			nombreDia = 'Viernes';
		}
		let data1 = this.state.tipoAsuntos.map((it, i) => {
			return {label: it.tipo, elPuesto: it.puesto, key: i};
		});
		let data = this.state.materias.map((item, i) => {
			return {
				grado: item.grado, key: i, label: item.nombre, materia: item.id
			};
		});
		return (<View style={styles.container}>
			<Modal
				isVisible={this.state.isModalCateg}
				backdropOpacity={0.8}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}>
				<View style={{height: 500}}>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione el asunto:
						</Text>
						<ScrollView>
							<View
								style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}>
								{this.renderAsuntos()}
							</View>
						</ScrollView>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalCateg: false})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.closeModalAsunto()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</Modal>
			<Modal
				isVisible={this.state.isModalDia}
				backdropOpacity={0.8}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}>
				<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
					{this.state.elTipo === 'Académicas' ? (<View>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione la materia
						</Text>
						<ModalSelector
							cancelText='Cancelar'
							optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
							selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
							data={data}
							onChange={option => this.onChange(option)}
							selectStyle={[styles.inputPicker, styles.modalWidth, {borderColor: this.state.secondColor}]}
							initValue={this.state.nombreMat === '' ? 'Seleccione la materia' : this.state.nombreMat}/>
					</View>) : null}
					<Text style={[styles.main_title, styles.modalWidth]}>
						Seleccione el día:
					</Text>
					<ScrollView>
						<View
							style={[styles.tabla, {borderColor: this.state.secondColor}]}>
							{this.renderDias()}
						</View>
						<Text
							style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
							Seleccione una hora
						</Text>
						<View
							style={[styles.tabla, {borderColor: this.state.secondColor}]}>
							{this.renderHoras()}
						</View>
					</ScrollView>
					<View style={[styles.modalWidth, styles.row]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModalDia: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.closeModalDia()}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
			<ScrollView showsVerticalScrollIndicator={false}>
				<View>
					<Hijos
						onSelectedChamaco={(index, grado, grupo, id) => this.onListItemPressed(index, grado, grupo, id)}
					/>
				</View>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione la categoría
				</Text>
				<ModalSelector
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor}}
					data={data1}
					onChange={option1 => this.onChange1(option1)}
					selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
					initValue='Seleccione la categoría'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione el asunto
				</Text>
				<View style={{
					alignItems: 'center',
					backgroundColor: this.state.fourthColor,
					padding: 15,
					borderRadius: 6
				}}>
					<TouchableOpacity
						style={[
							styles.bigButton,
							styles.btnShowMod,
							{
								marginTop: 10,
								marginBottom: 0,
								borderColor: this.state.secondColor,
								backgroundColor: '#fff'
							}
						]}
						onPress={() => this.openModalAsunto()}>
						<Text style={[styles.textButton, {color: 'black'}]}>
							Ver lista
						</Text>
					</TouchableOpacity>
					<Text style={{marginTop: 10}}>Asunto seleccionado</Text>
					<Text style={[styles.textButton, {color: 'black'}]}>
						{this.state.elAsunto2}
					</Text>
				</View>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Fechas disponibles
				</Text>
				<View style={{
					alignItems: 'center',
					backgroundColor: this.state.fourthColor,
					padding: 15,
					borderRadius: 6
				}}>
					<TouchableOpacity
						style={[
							styles.bigButton,
							styles.btnShowMod,
							{
								marginTop: 10,
								marginBottom: 10,
								borderColor: this.state.secondColor,
								backgroundColor: '#fff'
							}
						]}
						onPress={() => this.openModalDia()}
					>
						<Text style={[styles.textButton, {color: 'black'}]}>
							Ver disponibilidad
						</Text>
					</TouchableOpacity>
					<View style={{flexDirection: 'row', marginTop: 5}}>
						<Text style={{fontSize: responsiveFontSize(1.8)}}>Para el dia </Text>
						<Text style={[styles.textW, {color: 'black', fontSize: responsiveFontSize(1.8)}]}>
							{nombreDia} {this.state.dia2} {this.state.mes2}
						</Text>
						<Text style={{fontSize: responsiveFontSize(1.8)}}> a las </Text>
						<Text style={[styles.textW, {color: 'black', fontSize: responsiveFontSize(1.8)}]}>
							{this.state.laHora2}<Text
							style={{fontWeight: '500', fontSize: responsiveFontSize(1.8)}}> hrs</Text>
						</Text>
					</View>
				</View>
				<View style={{alignItems: 'center'}}>
					<Text
						style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 10}]}>
						La persona que le atenderá:
					</Text>
					{this.showInfo()}
				</View>
				<TouchableOpacity
					style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
					onPress={() => this.alertCita()}>
					<Text style={styles.textButton}>Enviar la solicitud</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>);
	}

	async alertCita() {
		Alert.alert('Agendar cita', 'Está a punto de agendar una cita,\n ¿Seguro que desea Continuar?', [
			{text: 'Sí', onPress: () => this.requestMultipart()},
			{text: 'No'}
		])
	}

	render() {
		return (<View style={styles.container}>
			<StatusBar
				backgroundColor={this.state.mainColor}
				barStyle='light-content'
			/>
			<MultiBotonRow
				itemBtns={['Solicitar una cita', 'Atender una cita']}
				onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
				cantidad={2}
			/>
			{this.state.botonSelected === 'Solicitar una cita' ? this.solCita() : null}
			{this.state.botonSelected === 'Atender una cita' ? (<CitasPadre/>) : null}
		</View>);
	}
}
