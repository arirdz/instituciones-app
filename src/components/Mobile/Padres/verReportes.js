import React from 'react';
import {AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import styles from '../../styles';
import Modal from 'react-native-modal';

export default class verReportes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ciclos: [],
            detalles: [],
            elCiclo: '',
            elDetalle: '',
            elHijo: '',
            elPeriodo: '',
            grado: '',
            grupo: '',
            incidenciasCount: [],
            itemBtns: ['FALTAS', 'INCIDENCIAS', 'FECHA'],
            ninosPicker: [],
            opcion: 0,
            periodos: [],
            selectedIndex: -1,
            selectedIndexBtn: 0,
            selectedIndexCiclo: 0,
            selectedIndexPeriodo: 0
        };
        this.renderList = this.renderList.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');

        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });

        await this.getCiclos();
    }

    async onListItemPressedCiclo(index, ciclo) {
        await this.setState({
            selectedIndexCiclo: index,
            elCiclo: ciclo,
            selectedIndexPeriodo: -1,
            selectedIndex: -1
        });
        await this.getPeriodos();
    }

    async getCiclos() {
        let ciclopicker = await fetch(this.state.uri + '/api/ciclos', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let ciclospicker = await ciclopicker.json();
        this.setState({ciclos: ciclospicker});
        this.setState({elCiclo: this.state.ciclos[0]});
        await this.getPeriodos();
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++PERIODO++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    renderCiclo(itemCiclo, indexCiclo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexCiclo === indexCiclo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexCiclo}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedCiclo(indexCiclo, itemCiclo)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemCiclo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderCiclos() {
        let buttonsCiclos = [];
        this.state.ciclos.forEach((itemCiclo, indexCiclo) => {
            buttonsCiclos.push(this.renderCiclo(itemCiclo, indexCiclo));
        });
        return buttonsCiclos;
    }

    onListItemPressedPeriodo(index, ciclo) {
        this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo,
            selectedIndex: -1
        });
        this.getHijos();
    }

    async getPeriodos() {
        let periodopicker = await fetch(
            this.state.uri + '/api/periodos/' + this.state.elCiclo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let periodospicker = await periodopicker.json();
        this.setState({
            periodos: periodospicker,
            elPeriodo: this.state.periodos[0]
        });
        this.getHijos();
    }

    renderPeriodo(itemPeriodo, indexPeriodo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexPeriodo === indexPeriodo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexPeriodo}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() =>
                    this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                }>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemPeriodo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderPeriodos() {
        let buttonsPeriodos = [];
        this.state.periodos.forEach((itemPeriodo, indexPeriodo) => {
            buttonsPeriodos.push(this.renderPeriodo(itemPeriodo, indexPeriodo));
        });
        return buttonsPeriodos;
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ALUMNOS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getHijos() {
        let hijoPicker = await fetch(this.state.uri + '/api/user/hijos', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let hijosPicker = await hijoPicker.json();
        this.setState({ninosPicker: hijosPicker.map(item => item)});
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            grado: grado,
            grupo: grupo,
            elHijo: id
        });
        await this.getIncidencias();
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++INCIDENCIAS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    renderItem(listItem, index) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            {borderColor: this.state.secondColor}
        ];
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndex === index) {
            bigButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={index}
                underlayColor={'transparent'}
                onPress={() =>
                    this.onListItemPressed(
                        index,
                        listItem.grado,
                        listItem.grupo,
                        listItem.id
                    )
                }>
                <View style={styles.listItem}>
                    <View style={bigButtonStyles}>
                        <Text style={texto}>{listItem.name}</Text>
                    </View>
                    <View style={smallButtonStyles}>
                        <Text style={texto}>{listItem.grado}</Text>
                    </View>
                    <View style={smallButtonStyles}>
                        <Text style={texto}>{listItem.grupo}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }

    renderList() {
        let buttons = [];
        this.state.ninosPicker.forEach((item, index) => {
            buttons.push(this.renderItem(item, index));
        });
        return buttons;
    }

    async getIncidencias() {
        let incidenciaList = await fetch(
            this.state.uri +
            '/api/incidencias/count/' +
            this.state.elHijo +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let incidenciasList = await incidenciaList.json();
        this.setState({incidenciasCount: incidenciasList});
    }

    renderIncidencias() {
        let academicas = this.state.incidenciasCount.IncidenciasAC;
        let conductuales = this.state.incidenciasCount.IncidenciasCC;
        return (
            <View style={styles.row}>
                <View style={styles.cuadro}>
                    <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                        INCIDENCIAS{'\n'}ACADÉMICAS
                    </Text>
                    <View
                        style={[
                            styles.buttonEnterados,
                            {backgroundColor: this.state.fourthColor},
                            styles.cuadro1
                        ]}>
                        <Text style={styles.text_VE}>{academicas}</Text>
                    </View>
                    {this.state.incidenciasCount.IncidenciasAC === 0 ? (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles1()}>
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles('Académicas')}>
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
                <View style={styles.cuadro}>
                    <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                        INCIDENCIAS{'\n'}CONDUCTUALES
                    </Text>
                    <View
                        style={[
                            styles.buttonEnterados,
                            {backgroundColor: this.state.mainColor},
                            styles.cuadro1
                        ]}>
                        <Text style={styles.text_VE}>{conductuales}</Text>
                    </View>
                    {this.state.incidenciasCount.IncidenciasCC === 0 ? (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles1()}>
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles('Conductual')}>
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++MOSTRAR DETALLES++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getDetalles(tipo) {
        let detalleList = await fetch(
            this.state.uri +
            '/api/detalles/incidencias/' +
            this.state.elHijo +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.elPeriodo +
            '/' +
            tipo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let detallesList = await detalleList.json();
        await this.setState({detalles: detallesList, isModalVisible: true});
    }

    async getDetalles1() {
        let detalleList = await fetch(this.state.uri + '/api/detalles/vacio', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let detallesList = await detalleList.json();
        await this.setState({detalles: detallesList, isModalVisible: true});
    }

    renderDetalle(itemDetalle) {
        return (
            <View
                style={[
                    styles.noticia,
                    styles.modalWidth,
                    {borderColor: '#bdbdbd', borderWidth: 1}
                ]}>
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Materia:{' '}
                        <Text style={{color: '#000'}}>{itemDetalle.materia.nombre}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Faltas:{' '}
                        <Text style={{color: '#000'}}>{itemDetalle.tipos_faltas}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Incidencias:{' '}
                        <Text style={{color: '#000'}}>{itemDetalle.extras}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Fecha:{' '}
                        <Text style={{color: '#000'}}>{itemDetalle.created_at}</Text>
                    </Text>
                </View>
            </View>
        );
    }

    renderDetalles() {
        let buttonsDetalles = [];
        this.state.detalles.forEach((itemDetalle, indexDetalle) => {
            buttonsDetalles.push(this.renderDetalle(itemDetalle, indexDetalle));
        });
        return buttonsDetalles;
    }

    render() {
        const detalles = this.renderDetalles();
        const list = this.renderList();
        const ciclos = this.renderCiclos();
        const periodos = this.renderPeriodos();
        const incidencias = this.renderIncidencias();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el ciclo escolar
                    </Text>
                    <View style={styles.listItem}>{ciclos}</View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el periodo
                    </Text>
                    <View style={styles.listItem}>{periodos}</View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Selccione el alumno
                    </Text>
                    {list}
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Informe de incidencias
                    </Text>
                    {incidencias}
                    <Modal
                        isVisible={this.state.isModalVisible}
                        backdropOpacity={0.5}
                        animationIn={'bounceIn'}
                        animationOut={'bounceOut'}
                        animationInTiming={1000}
                        animationOutTiming={1000}>
                        <View style={[styles.container, {borderRadius: 6}]}>
                            <ScrollView>
                                <Text
                                    style={[
                                        styles.main_title,
                                        styles.modalWidth,
                                        {color: this.state.thirdColor}
                                    ]}>
                                    Incidencias
                                </Text>
                                {detalles}
                            </ScrollView>
                            <TouchableOpacity
                                style={[
                                    styles.bigButton,
                                    styles.modalWidth,
                                    {
                                        backgroundColor: this.state.mainColor,
                                        borderColor: this.state.mainColor
                                    }
                                ]}
                                onPress={() => this.setState({isModalVisible: false})}>
                                <Text style={styles.textButton}>Aceptar</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                </ScrollView>
            </View>
        );
    }
}
