import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../styles';
import Hijos from '../Globales/hijos';
import Periodos from '../Globales/Periodos';
import Modal from 'react-native-modal';
import Entypo from "react-native-vector-icons/Entypo";

export default class encuadrePadre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            elEncuadre: [],
            modalPonder: [],
            grupos: [],
            elEstatus: [],
            selectedIndex: '',
            materiasArray: [],
            encuadresArray: [],
            laMateria: '',
            nombreMat: '',
            elId: '',
            selGrad: '',
            selGrup: '',
            incompletoMT: false,
            elCiclo: '1',
            selectedIndexPeriodo: -1,
            elPeriodo: '',
            elTeacher: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
        if (this.state.selGrad !== '' && this.state.encuadresArray.length !== 0) {
            await this.getEncuadresArray();
            await this.getEncuadres();
        }
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async onListItemPressedPeriodo(indexPeriodo, itemPeriodo) {
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: itemPeriodo,
            elEncuadre: [],
            incompletoMT: true,
        });
        if (this.state.selGrad !== '' && this.state.laMateria !== '') {
            await this.getEncuadres();
        }
        await this.getEncuadresArray();
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            incompletoMT: true,
            elId: id,
            laMateria: '',
            elEncuadre: [],
            elTeacher: ''
        });
        await this.getEncuadresArray();
    }

    //+-+-+-+-+-+-+-+-+-+-+-EncuadreArray
    async getEncuadresArray() {
        await this.setState({incompletoMT: false});
        await fetch('http://127.0.0.1:8000/api/get/encuadre/array/padre/' + this.state.selGrad + '/' + this.state.selGrup + '/' + this.state.elPeriodo + '/' + this.state.elCiclo,
            {
                method: 'GET', headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar el encuadres', 'Ha ocurrido un error al tratar de cargar el encuadre. Si el error continua pónganse en contacto con soporte (cod. 2)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({materiasArray: []});
                    this.setState({encuadresArray: responseJson});
                    if (this.state.encuadresArray.length === 0) {
                        this.setState({incompletoMT: true})
                    }
                    this.state.encuadresArray.forEach((it, i) => {
                        this.state.materiasArray.push(it.mate);
                    });
                }
            });
        await this.setState({aux: 0});
    }

    async onChange(option) {
        await this.setState({laMateria: option.materia, nombreMat: option.label});
        await this.setState({elTeacher: this.state.encuadresArray[option.key].maestro.name});
        await this.getEncuadres();
    }

    //+++++++++++++++++++++++++++++++Encuadres++++++++++++++++++++++++++++++++++++
    async getEncuadres() {
        await fetch('http://127.0.0.1:8000/api/get/encuadre/' +
            this.state.laMateria +
            '/' +
            this.state.elPeriodo +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.selGrup, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Encuadre en revisión', 'Este encuadre esta siendo revisado por el coordinador, se le notificara cuando el encuadre sea publicado, Gracias', [
                        {text: 'Entendido'}
                    ]);
                } else {
                    this.setState({elEncuadre: responseJson});
                    if (this.state.elEncuadre.length !== 0) {
                        this.state.elEncuadre.forEach(() => {
                            this.state.modalPonder.push(false);
                        });
                        this.setState({elEstatus: this.state.elEncuadre[0].estatus});
                    }
                }
            });
        if (this.state.elEstatus === '0' || this.state.elEstatus === '1') {
            Alert.alert(
                'Encuadre en revisión', 'Este encuadre esta siendo revisado por el coordinador, ' +
                'se le notificara cuando el encuadre sea publicado, Gracias', [
                    {text: 'Entendido'}
                ])
        }
    }

    async openModalP(i, state) {
        this.state.modalPonder[i] = state;
        await this.setState({aux: 0});
    }

    renderEncuadres() {
        let btnEncuadre = [];
        this.state.elEncuadre.forEach((item, index) => {
            btnEncuadre.push(
                <View
                    key={index + 'encuadreDir'}
                    style={[
                        styles.widthall,
                        styles.row,
                        styles.cardEnc,
                        {backgroundColor: this.state.fourthColor, height: responsiveHeight(4)}
                    ]}
                >
                    <View style={styles.btn2}>
                        <Text style={styles.textW}>{item.descripcion}</Text>
                    </View>
                    <View style={styles.btn6}>
                        <Text style={[styles.textW, {textAlign: 'center'}]}>{item.valor}%</Text>
                    </View>
                    {Number(item.tipo) === 0 ? <View style={styles.btn6_ls}>
                        <Entypo name='magnifying-glass' size={25} color='#000'
                                style={{marginTop: 1}}
                                onPress={() => this.openModalP(index, true)}/>
                    </View> : <View style={styles.btn6_ls}/>}
                    {/*+-+-+-+-+-+-+Pondereaciones +-+-+-+-+-*/}
                    <Modal
                        isVisible={this.state.modalPonder[index]}
                        backdropOpacity={0.5}
                        animationIn={'bounceIn'}
                        animationOut={'bounceOut'}
                        animationInTiming={1000}
                        animationOutTiming={1000}
                    >
                        <View
                            style={[styles.container, {borderRadius: 6, flex: 0, maxHeight: responsiveHeight(80)}]}>
                            <Text
                                style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                            >
                                Ponderaciones de {item.descripcion}
                            </Text>
                            <View style={[styles.modalWidth, {marginVertical: 10, alignItems: 'center'}]}>
                                {this.rndPonderaciones(index)}
                            </View>
                            <View style={[styles.modalWidth, {marginVertical: 5, alignItems: 'center'}]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.openModalP(index, false)}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>
            )
        });
        return btnEncuadre;
    }

    rndPonderaciones(i) {
        let ponderaciones = [];
        if (this.state.elEncuadre[i].ponderaciones.length !== 0) {
            this.state.elEncuadre[i].ponderaciones.forEach((itm, i) => {
                ponderaciones.push(
                    <View
                        key={i + 'pondDir'}
                        style={[
                            styles.modalWidth,
                            styles.row,
                            styles.cardEnc,
                            {backgroundColor: this.state.fourthColor, height: responsiveHeight(3.4)}
                        ]}
                    >
                        <View style={styles.btn2}>
                            <Text style={styles.textW}>{itm.descripcion}</Text>
                        </View>
                        <View style={styles.btn6}>
                            <Text style={[styles.textW, {textAlign: 'center'}]}>{itm.valor}</Text>
                        </View>
                    </View>
                )
            });
        }
        return ponderaciones;
    }

    // ++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++
    async alertModalMt() {
        Alert.alert('Atención',
            'Los encuadres de este grado y grupo estan en revisión', [
                {text: 'Entendido'}
            ])
    }

    async alertModal() {
        if (this.state.selGrad === '') {
            Alert.alert('Campo alumno', 'Seleccione a un alumno para continuar', [
                {text: 'Entendido'}
            ])
        } else if (this.state.elPeriodo === '') {
            Alert.alert('Campo periodo vacío', 'Seleccione un periodo para continuar', [
                {text: 'Entendido'}
            ])
        }
    }

    render() {
        const verEn = this.renderEncuadres();
        let idMate = this.state.laMateria;
        let nombMat = this.state.nombreMat;
        let data = this.state.materiasArray.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre,
                materia: item.id
            };
        });
        return (
            <View style={styles.container}>

                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <Periodos
                    onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                        this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                    }
                />
                <Hijos
                    onSelectedChamaco={(index, grado, grupo, id) =>
                        this.onListItemPressed(index, grado, grupo, id)
                    }
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione la materia
                </Text>
                {this.state.elPeriodo !== '' && this.state.selGrad !== '' && this.state.incompletoMT === false ? (
                    <ModalSelector
                        cancelText='Cancelar'
                        data={data}
                        initValue={idMate === '' ? 'Seleccione la materia' : nombMat}
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
                        selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                    />
                ) : this.state.incompletoMT === true ? (
                    <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        onPress={() => this.alertModalMt()}
                    >
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>Seleccione la materia</Text>
                    </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        onPress={() => this.alertModal()}
                    >
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>Seleccione la materia</Text>
                    </TouchableOpacity>
                )}
                <Text style={{marginTop: 10}}>Docente a cargo: <Text
                    style={{fontWeight: '600'}}>{this.state.elTeacher}</Text></Text>
                <Text
                    style={[styles.main_title, {color: this.state.thirdColor}]}
                >
                    Encuadre
                </Text>
                {this.state.elEncuadre.length === 0
                || this.state.elEstatus === '0'
                || this.state.elEstatus === '1'
                || this.state.elEstatus === 0
                || this.state.elEstatus === 1 ? (
                    <Text style={[styles.main_title, {
                        color: '#a8a8a8',
                        textAlign: 'center',
                        marginTop: 40,
                        fontSize: responsiveFontSize(3)
                    }]}>
                        Encuadre en revisión
                    </Text>
                ) : this.state.elEncuadre.length !== 0 || this.state.elEstatus === '2' || this.state.elEstatus === 2 ? (
                    <View style={{
                        borderTopWidth: .5,
                        borderBottomWidth: .5,
                        borderColor: 'grey',
                        height: responsiveHeight(30),
                        paddingVertical: 5
                    }}>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            overScrollMode={'always'}
                        >
                            {verEn}
                        </ScrollView>
                    </View>
                ) : null}
            </View>
        );
    }
}
