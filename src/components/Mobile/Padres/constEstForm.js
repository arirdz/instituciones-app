import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import {
    responsiveWidth,
    responsiveFontSize,
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import StepIndicator from "rn-step-indicator";

const labels_estForm = [
    "Solicitar",
    "Entregar foto",
    "Realizar el pago",
    "Esperar liberación",
    "Recoger en oficina",
    "Finalizar"
];

export default class constEstForm extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            currentPosition: 0
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    constEstForm() {
        return (
            <View>
                {this.state.currentPosition === 0 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>paso 1</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 1 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>paso 2</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 2 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>paso 3</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 3 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>paso 4</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 4 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>paso 5</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 5 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}>
                        <Text>paso 6</Text>
                    </View>
                ) : null}
            </View>
        );
    }

    onPageChange(position) {
        this.setState({currentPosition: position});
    }

    pasoSig() {
        if (this.state.currentPosition <= 4) {
            this.setState({
                currentPosition: this.state.currentPosition + 1,
                temporal: false,
                calidad: false
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition + 0,
                temporal: false,
                calidad: false
            });
        }
    }

    pasoAnt() {
        if (this.state.currentPosition <= 0) {
            this.setState({
                currentPosition: 0,
                temporal: false,
                calidad: false
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition - 1,
                temporal: false,
                calidad: false
            });
        }
    }

    render() {
        const estForm = this.constEstForm();
        let customStyles = {
            stepIndicatorSize: 25,
            currentStepIndicatorSize: 30,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: "#777777",
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: "#777777",
            stepStrokeUnFinishedColor: "#aaaaaa",
            separatorFinishedColor: "#777777",
            separatorUnFinishedColor: "#aaaaaa",
            stepIndicatorFinishedColor: "#777777",
            stepIndicatorUnFinishedColor: "#ffffff",
            stepIndicatorCurrentColor: "#ffffff",
            stepIndicatorLabelFontSize: 13,
            currentStepIndicatorLabelFontSize: 13,
            stepIndicatorLabelCurrentColor: "#777777",
            stepIndicatorLabelFinishedColor: "#ffffff",
            stepIndicatorLabelUnFinishedColor: "#aaaaaa",
            labelColor: "#aaaaaa",
            labelSize: responsiveFontSize(1.3),
            currentStepLabelColor: "#777777"
        };
        let index = 0;
        const data = [
            {id: "sReinscrip"},
            {id: "estSimp"},
            {id: "estForm"},
            {id: "crBeca"},
            {id: "sBeca"}
        ];
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView>
                    <Text style={styles.main_title}>
                        Este es el procedimiento a seguir:
                    </Text>
                    <View style={{width: responsiveWidth(94), marginTop: 10}}>
                        {this.props.data === "estForm" ? (
                            <StepIndicator
                                customStyles={customStyles}
                                stepCount={6}
                                currentPosition={this.state.currentPosition}
                                labels={labels_estForm}
                            />
                        ) : null}
                        {this.props.data === "estForm" ? this.constEstForm() : null}
                    </View>
                </ScrollView>
                <View style={[styles.buttonsRow2]}>
                    <TouchableOpacity
                        style={[
                            styles.middleButton,
                            {
                                backgroundColor: this.state.secondColor,
                                borderColor: this.state.secondColor
                            }
                        ]}
                        onPress={() => this.pasoAnt()}>
                        <Text style={styles.textButton}>Paso anterior</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.middleButton,
                            {
                                backgroundColor: this.state.secondColor,
                                borderColor: this.state.secondColor
                            }
                        ]}
                        onPress={() => this.pasoSig()}>
                        <Text style={styles.textButton}>Siguiente paso</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
