import React from 'react';
import {Alert, AsyncStorage, RefreshControl, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import styles from '../../styles';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions'
import {Actions} from 'react-native-router-flux';
import {Agenda, LocaleConfig} from 'react-native-calendars';
import Entypo from 'react-native-vector-icons/Entypo';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

LocaleConfig.locales['es_MX'] = {
	monthNames: [
		'Enero',
		'Febrero',
		'Marzo',
		'Abril',
		'Mayo',
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre'
	],
	monthNamesShort: [
		'Ene.',
		'Feb.',
		'Mar',
		'Abr',
		'May',
		'Jun',
		'Jul.',
		'Ago',
		'Sept.',
		'Oct.',
		'Nov.',
		'Dec.'
	],
	dayNames: [
		'Domingo',
		'Lunes',
		'Martes',
		'Miércoles',
		'Jueves',
		'Viernes',
		'Sábado'
	],
	dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vier.', 'Sab.']
};

LocaleConfig.defaultLocale = 'es_MX';


export default class calendarEvent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			evento: [],
			visible: false,
			refreshing: false,
			itemsI: {
				'1995-11-27': [{
					name: 'Nacimiento',
					description: 'Naci este día'
				}]
			},
			fechas: [],
			moreInfo: [],
			btnSelected: 'Calendario',
			indxSelected: 0,
			dots: {},
			fechaCard: -1,
			isModalEvento: false
		};
	}

	_onRefresh = () => {
		this.setState({refreshing: true});
		this.setState({refreshing: false});
	};

	spinner = (state) => {
		this.setState({
			visible: state
		});
	};

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	getDateArray(startDate, endDate, item) {
		const conviviencia = {key: 'conviviencia', color: 'red'};
		const evaluacion = {key: 'evaluacion', color: 'blue'};
		const vacaciones = {key: 'vacaciones', color: 'green'};
		const suspension = {key: 'suspension', color: 'yellow'};
		const preinscripcion = {key: 'preinscripcion', color: 'grey'};
		const curso = {key: 'curso', color: 'pink'};
		const examen = {key: 'examen', color: 'brown'};
		const academicas = {key: 'academicas', color: 'black'};
		const reporte = {key: 'reporte', color: 'purple'};
		const cte = {key: 'cte', color: 'violet'};

		function esAlumno(alumno) {
			return alumno.key === item.tipo_evento;
		}

		let dates = [];
		let dots = this.state.dots;
		while (startDate <= endDate) {
			dates.push({date: startDate, item: item});
			if (dots[startDate]) {
				if (item.tipo_evento === 'conviviencia') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(conviviencia);
					}
				}
				if (item.tipo_evento === 'evaluacion') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(evaluacion);
					}
				}
				if (item.tipo_evento === 'vacaciones') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(vacaciones);
					}
				}
				if (item.tipo_evento === 'suspension') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(suspension);
					}
				}
				if (item.tipo_evento === 'preinscripcion') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(preinscripcion);
					}
				}
				if (item.tipo_evento === 'curso') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(curso);
					}
				}
				if (item.tipo_evento === 'examen') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(examen);
					}
				}
				if (item.tipo_evento === 'academicas') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(academicas);
					}
				}
				if (item.tipo_evento === 'reporte') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(reporte);
					}
				}
				if (item.tipo_evento === 'cte') {
					if (!dots[startDate].dots.find(esAlumno)) {
						dots[startDate].dots.push(cte);
					}
				}
			} else {
				if (item.tipo_evento === 'conviviencia') {
					dots[startDate] = {dots: [conviviencia]}
				}
				if (item.tipo_evento === 'evaluacion') {
					dots[startDate] = {dots: [evaluacion]}
				}
				if (item.tipo_evento === 'vacaciones') {
					dots[startDate] = {dots: [vacaciones]}
				}
				if (item.tipo_evento === 'suspension') {
					dots[startDate] = {dots: [suspension]}
				}
				if (item.tipo_evento === 'preinscripcion') {
					dots[startDate] = {dots: [preinscripcion]}
				}
				if (item.tipo_evento === 'curso') {
					dots[startDate] = {dots: [curso]}
				}
				if (item.tipo_evento === 'examen') {
					dots[startDate] = {dots: [examen]}
				}
				if (item.tipo_evento === 'academicas') {
					dots[startDate] = {dots: [academicas]}
				}
				if (item.tipo_evento === 'reporte') {
					dots[startDate] = {dots: [reporte]}
				}
				if (item.tipo_evento === 'cte') {
					dots[startDate] = {dots: [cte]}
				}
			}
			startDate = moment(startDate).add(1, 'days').format('YYYY-MM-DD');
		}
		this.setState({dots: dots});

		return dates;
	};

	loadItems() {
		return fetch(this.state.uri + '/api/get/activdad/calendario/padre',{
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then((response) => {
				return response.json();
			}).then((responseData) => {
				return responseData;
			}).then((data) => {
				data.map((item) => {
					let startDate = item.fecha_inicio;
					let endDate = item.fecha_fin;
					let datesPush = this.getDateArray(startDate, endDate, item);
					datesPush.forEach((it, i) => {
						const strTime = this.timeToString(it.date);
						if (!this.state.itemsI[strTime]) {
							this.state.itemsI[strTime] = [];
						}
						if (this.state.itemsI[strTime].filter(event => (event.id === item.id)).length <= 0) {
							this.state.itemsI[strTime].push({
								id: item.id,
								start: item.fecha_inicio,
								end: item.fecha_fin,
								name: item.titulo,
								place:item.lugar,
								hora_inicio: item.hora_inicio,
								hora_fin: item.hora_fin,
								description: item.descripcion,
								destinatarios: item.destinatarios
							});
						}
					})
				});
				const newItems = {};
				Object.keys(this.state.itemsI).forEach(key => {
					newItems[key] = this.state.itemsI[key];
				});
				this.setState({itemsI: newItems});
			}).catch((error) => {
				Alert.alert('Error al cargar las fechas', '' + error + '')
			});
	}


	renderItem(item) {
		return (
			<View
				style={[
					styles.item,
					{backgroundColor: this.state.fourthColor}
				]}
			>
					<Text style={{fontSize: responsiveFontSize(1.9), fontWeight:'900'}}>{item.name}</Text>
					<Text style={[styles.textW,{marginTop: 5, fontSize: responsiveFontSize(1.5)}]}>
						Descripción del evento:
					</Text>
					<Text>{item.description}</Text>
					<Text style={[styles.textW,{marginTop: 5, fontSize: responsiveFontSize(1.5)}]}>
						Duración del evento:
					</Text>
					<Text>
						de {item.start} a {item.end}
					</Text>
					<Text style={[styles.textW,{marginTop: 5, fontSize: responsiveFontSize(1.5)}]}>
						Tiempo de duración:
					</Text>
					<Text>De {moment(item.hora_inicio, 'hh:mm').format('hh:mm a')} a {moment(item.hora_fin, 'hh:mm').format('hh:mm a')}</Text>
					<Text style={[styles.textW,{marginTop: 5, fontSize: responsiveFontSize(1.5)}]}>
						Lugar del evento:
					</Text>
					<Text>{item.place}</Text>
			</View>
		);
	}

	renderEmptyDate() {
		return (
			<View
				style={{
					height: responsiveHeight(10),
					flex: 1,
					paddingTop: 30
				}}>
				<Text>No hay eventos</Text>
			</View>
		);
	}

	rowHasChanged(r1, r2) {
		return r1.name !== r2.name;
	}

	timeToString(time) {
		const date = new Date(time);
		return date.toISOString().split('T')[0];
	}

	calendario() {
		return (
			<View>
				<Agenda
					items={this.state.itemsI}
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					renderItem={this.renderItem.bind(this)}
					loadItemsForMonth={this.loadItems.bind(this)}
					renderEmptyDate={this.renderEmptyDate.bind(this)}
					rowHasChanged={this.rowHasChanged.bind(this)}
					markedDates={this.state.dots}
					renderKnob={() => {
						return (<Entypo name='chevron-thin-down' size={25} color='lightgrey'/>);
					}}
					markingType={'multi-dot'}
					style={{width: responsiveWidth(94), height: responsiveHeight(10)}}
				/>
			</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando, Por favor espere...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				{this.calendario()}
			</View>
		);
	}
}
