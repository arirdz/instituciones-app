import React from 'react';
import {Alert, AsyncStorage, ScrollView, Text, TouchableOpacity, View,RefreshControl} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {responsiveWidth, responsiveHeight} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class CitasPadre extends React.Component {
	_onRefresh = () => {
		this.setState({refreshing: true});
		this.getSolicitudes();
		this.getHistorial();
		this.getCitasHechas();
	};
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			refreshing: true,
			laSolicitud: '',
			solicitudes: [],
			historial: [],
			citasHechas: [],
			selected: 'Citas finalizadas'
		};
	}

	async getURL() {
		this.setState({
			uri: await AsyncStorage.getItem('uri'),
			token: await AsyncStorage.getItem('token'),
			maincolor: await AsyncStorage.getItem('mainColor'),
			secondColor: await AsyncStorage.getItem('secondColor'),
			thirdColor: await AsyncStorage.getItem('thirdColor'),
			fourthColor: await AsyncStorage.getItem('fourthColor')
		});

	}

	async componentWillMount() {
		await this.getURL();
		await this.getSolicitudes();
		await this.getHistorial();
		await this.getCitasHechas();
	}

	async getSolicitudes() {
		let solicitudesList = await fetch(this.state.uri + '/api/feed/citas/getCitas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let listSolicitudes = await solicitudesList.json();
		this.setState({solicitudes: listSolicitudes});
		this.setState({refreshing: false});
	}

	renderSolicitud(itemSolicitud, i) {
		return (<View
			key={i + 'cita'}
			style={[styles.cardCita, {
				borderColor: this.state.fourthColor,
				shadowColor: this.state.fourthColor
			}]}>
			<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 5, alignItems: 'center'}}>
				<Text style={styles.notifTitle}>Solicitud de Cita</Text>
			</View>
			<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
				<Text style={styles.textInfo}>
					Se ha solicitado una cita por: <Text
					style={{fontWeight: '700', textAlign: 'right'}}>{'"' + itemSolicitud.user.name + '"'}</Text> para
					tratar el asunto: <Text
					style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.asunto}</Text> con respecto a su
					hijo:
					<Text style={{fontWeight: '700'}}>
						{itemSolicitud.hijos.name} del
												   Grado: {itemSolicitud.hijos.grado} Grupo: {itemSolicitud.hijos.grupo}{' '}
					</Text>
				</Text>
				<Text style={{fontWeight: '700'}}>{'\n'}
					Agendada para el dia {itemSolicitud.fecha_cita} a
					las {moment(itemSolicitud.hora, 'HH:mm:ss').format('HH:mm')}
				</Text>
			</View>
			<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
				<TouchableOpacity
					onPress={() => this.requestMultipart(itemSolicitud.id)}
					style={[styles.btnCita, {backgroundColor: '#3fa345'}]}
				>
					<Text style={{color: '#fff'}}>Aceptar</Text>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={() => this.cancelarCitaAgend(itemSolicitud.id)}
					style={[styles.btnCita, {backgroundColor: '#fc001e'}]}
				>
					<Text style={{color: '#fff'}}>Declinar</Text>
				</TouchableOpacity>
				<TouchableOpacity
					// onPress={() => this.cancelarCitaAgend(itemSolicitud.id)}
					style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor, paddingHorizontal: 10}]}
				>
					<Text>Mensaje</Text>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={() => this.llamar(itemSolicitud.user.id)}
					style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor, paddingHorizontal: 10}]}
				>
					<Text>Llamar</Text>
				</TouchableOpacity>
			</View>
		</View>);
	}

	async aceptarCita(id) {
		await fetch(this.state.uri + '/api/feed/citas/update/' + id, {
			method: 'POST', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Aceptar Citas)', [{
						text: 'Entendido'
					}]);
				} else {
					Alert.alert('¡Excelente!', 'Has Aceptado la cita', [{
						text: 'Entendido', onPress: () => Actions.refresh({key: 'gestCitasMtro'})
					}]);
				}
			});
	}

	async requestMultipart(id) {
		Alert.alert('', '¿Está seguro de aceptar la cita?', [{
			text: 'Si', onPress: () => this.aceptarCita(id)
		}, {text: 'No'}
		]);
	}

	renderSolicitudes() {
		let buttonsSolicitudes = [];
		this.state.solicitudes.forEach((itemSolicitud, indexSolicitud) => {
			buttonsSolicitudes.push(this.renderSolicitud(itemSolicitud, indexSolicitud));
		});
		return buttonsSolicitudes;
	}


	//+-+-+--+-+-+-+-+-+-+Historial de citas
	async getHistorial() {
		let solicitudesList = await fetch(this.state.uri + '/api/feed/citas/getCitas/aceptadas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let listSolicitudes = await solicitudesList.json();
		this.setState({historial: listSolicitudes});
		this.setState({refreshing: false});
	}


	historial(itemSolicitud, i) {
		if (itemSolicitud.user.role === 'Maestro' || itemSolicitud.user.role === 'Admin') {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View style={{
						borderBottomWidth: 1,
						borderColor: this.state.fourthColor,
						padding: 5,
						alignItems: 'center'
					}}>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							Se solicito una cita por: <Text style={{
							fontWeight: '700',
							textAlign: 'right'
						}}>{'"' + itemSolicitud.user.name + '"'}</Text> para tratar el asunto: <Text
							style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.asunto}</Text> con respecto
							a su
							hijo:
							<Text style={{fontWeight: '700'}}>
								{itemSolicitud.hijos.name} del
														   Grado: {itemSolicitud.hijos.grado} Grupo: {itemSolicitud.hijos.grupo}{' '}
							</Text>
						</Text>
						{itemSolicitud.aprobada === '2' ? (
							<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>
								*Esta cita fue cancelada
							</Text>) : null}
						<Text style={{fontWeight: '700'}}>
							Agendada para el {itemSolicitud.fecha_cita} a
							las {moment(itemSolicitud.hora, 'HH:mm:ss').format('HH:mm')}
						</Text>
					</View>
				</View>
			);
		} else if (itemSolicitud.user.role === 'Padre') {
			if (itemSolicitud.aprobada === '2') {
				return (
					<View
						key={i + 'cita'}
						style={[styles.cardCita, {
							borderColor: this.state.fourthColor,
							shadowColor: this.state.fourthColor
						}]}
					>
						<View style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}>
							<Text style={styles.notifTitle}>Solicitud de Cita</Text>
						</View>
						<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
							<Text style={styles.textInfo}>
								Cancelaste una cita con: <Text style={{fontWeight: '700', textAlign: 'right'}}>
								{'"' + itemSolicitud.receptor.name + '"'}{'\n'}</Text>para tratar el asunto: <Text
								style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.asunto}{'\n'}</Text>
								con respecto a su hijo: <Text style={{fontWeight: '700'}}>
								{itemSolicitud.hijos.name}{'\n'}del
																Grado: {itemSolicitud.hijos.grado} Grupo: {itemSolicitud.hijos.grupo}{' '}
							</Text>
							</Text>
							{itemSolicitud.aprobada === '2' ? (
								<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>
									*Cancelaste esta cita
								</Text>
							) : null}
							<Text style={{fontWeight: '700', fontStyle: 'italic'}}>
								Agendada el {itemSolicitud.fecha_cita} a
								las {moment(itemSolicitud.hora, 'HH:mm:ss').format('HH:mm')}
							</Text>
						</View>
					</View>
				);
			}
		}
	}

	historiales() {
		let btnSolicitudes = [];
		this.state.historial.forEach((itemSolicitud, indexSolicitud) => {
			btnSolicitudes.push(this.historial(itemSolicitud, indexSolicitud));
		});
		return btnSolicitudes;
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+Get citas realizadas
	async getCitasHechas() {
		let btnHecho = await fetch(this.state.uri + '/api/feed/citas/hechas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let btnHechos = await btnHecho.json();
		this.setState({citasHechas: btnHechos});
		this.setState({refreshing: false});
	}

	async cancelarLaCita(id) {
		fetch(this.state.uri + '/api/feed/citas/cancelar/' + id, {
			method: 'POST', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Aceptar Citas)', [{text: 'Entendido', onPress: () => Actions.pop()}]);
				} else {
					Alert.alert('Cita cancelada', 'Has cancelado la cita', [{
						text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
					}]);
				}
			});
	}

	async cancelarCitaAgend(id) {
		Alert.alert('Cancelar cita', '¿Está seguro que desea cancelar la cita?',
			[{text: 'Si', onPress: () => this.cancelarLaCita(id)}, {text: 'No'}]);
	}

	rndHecho(itemSolicitud, i) {
		return (
			<View
				key={i + 'cita'}
				style={[styles.cardCita, {
					borderColor: this.state.fourthColor,
					shadowColor: this.state.fourthColor
				}]}>
				<View style={{
					borderBottomWidth: 1,
					borderColor: this.state.fourthColor,
					padding: 5,
					alignItems: 'center'
				}}>
					<Text style={styles.notifTitle}>Solicitud de Cita</Text>
				</View>
				<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
					<Text style={styles.textInfo}>
						Has solicitado una cita para tratar el asunto de: <Text
						style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.asunto}</Text>{'\n'}
						con el maestro(a):{' '}
						<Text style={{fontWeight: '700'}}>
							{itemSolicitud.receptor.name}{'\n'}
						</Text>
					</Text>
					<Text style={{fontWeight: '700'}}>{'\n'}
						Agendada para el dia {itemSolicitud.fecha_cita} a
						las {moment(itemSolicitud.hora, 'HH:mm:ss').format('HH:mm')}
					</Text>
				</View>
				<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
					<TouchableOpacity
						onPress={() => this.cancelarCitaAgend(itemSolicitud.id)}
						style={[styles.btnCita, {backgroundColor: '#fc001e', paddingVertical: 4}]}
					>
						<Text style={{color: '#fff'}}>Cancelar Cita</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}

	rndHechos() {
		let btnCitaH = [];
		this.state.citasHechas.forEach((it, i) => {
			btnCitaH.push(this.rndHecho(it, i))
		});
		return btnCitaH;
	}

	async onChange(option) {
		this.setState({selected: option.label});
	}

	render() {
		let index = 0;
		const data = [
			{key: index++, label: 'Citas finalizadas'},
			{key: index++, label: 'Citas recibidas'},
			{key: index++, label: 'Citas realizadas'}
		];
		return (<View style={styles.container}>
			<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
				Revisar solicitudes:
			</Text>
			<ModalSelector
				data={data}
				selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
				cancelText='Cancelar'
				optionTextStyle={{color: this.state.thirdColor}}
				initValue={this.state.selected}
				onChange={option => this.onChange(option)}/>

			{this.state.selected === 'Citas recibidas' ? (<View>
				<Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 10}]}>
					Solicitudes recibidas
				</Text>
				<View style={{height: responsiveHeight(60)}}>
					<ScrollView
						refreshControl={
							<RefreshControl
								refreshing={this.state.refreshing}
								onRefresh={this._onRefresh}
							/>
						}
						overScrollMode='always'
						showsVerticalScrollIndicator={false}
					>
						{this.renderSolicitudes()}
					</ScrollView>
				</View>
			</View>) : this.state.selected === 'Citas finalizadas' ? (<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 10}]}>
					Solicitudes finalizadas
				</Text>
				<View style={{height: responsiveHeight(60)}}>
					<ScrollView
						refreshControl={
							<RefreshControl
								refreshing={this.state.refreshing}
								onRefresh={this._onRefresh}
							/>
						}
						overScrollMode='always'
						showsVerticalScrollIndicator={false}
					>
						{this.historiales()}
					</ScrollView>
				</View>
			</View>) : (<View>
				<Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 10}]}>
					Solicitudes realizadas
				</Text>
				<View style={{height: responsiveHeight(60)}}>
					<ScrollView
						refreshControl={
							<RefreshControl
								refreshing={this.state.refreshing}
								onRefresh={this._onRefresh}
							/>
						}
						overScrollMode='always'
						showsVerticalScrollIndicator={false}
					>
						{this.rndHechos()}
					</ScrollView>
				</View>
			</View>)}
		</View>);
	}
}
