import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, View} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class menuEscPadre extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			elMenu: [],
			visible: false
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getMenuEscolarPadre();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}


	async getMenuEscolarPadre() {
		await this._changeWheelState();
		await fetch(this.state.uri + '/api/get/menu/escolar/padres', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'No hay menú', 'La escuela aún no publica el menú de esta semana',
						[{text: 'Entendido', onPress: () => this.setState({visible: false})}]
					);
				} else {
					this.setState({elMenu: responseJson, visible: false});
				}
			});
	}


	rndMenuDias() {
		let btnDias = [];
		if (this.state.elMenu.length !== 0) {
			this.state.elMenu.producto.forEach((itm, i) => {
				btnDias.push(<View
					key={i + 'dia'}
					style={[styles.widthall, styles.cardMat, {
						backgroundColor: '#f1f1f1',
						alignItems: 'center'
					}]}
				>
					<View style={[styles.widthall, styles.row, {
						alignItems: 'flex-start',
						paddingHorizontal: 8,
						marginBottom: 8
					}]}>
						<View
							style={[
								styles.inputPicker,
								styles.btn2,
								{
									borderRadius: 7,
									borderWidth: .5,
									paddingVertical: 4,
									alignItems: 'flex-start',
									backgroundColor: '#f7f7f7',
									marginTop: 0
								}
							]}
						>
							<Text style={[styles.textW, {textAlign: 'left'}]}>{itm.dia}</Text>
						</View>
					</View>
					{this.rndProductos(i)}
				</View>)
			});
		}
		return btnDias;
	}


	rndProductos(i) {
		let btnProduct = [];
		if (this.state.elMenu.length !== 0) {
			this.state.elMenu.producto[i].producto.forEach((itm, j) => {
				btnProduct.push(
					<View
						key={j + 'producto'}
						style={[styles.row, styles.btn1, {
							alignItems: 'center',
							marginVertical: 3,
							backgroundColor: 'transparent'
						}]}
					>
						<View
							style={[styles.inputPicker, styles.btn7, styles.row, {
								padding: 4, marginTop: 0, backgroundColor: '#f7f7f7'
							}]}
						>
							<Text style={[{fontSize: responsiveFontSize(1.5)}]}>
								{itm.nombre_producto}
							</Text>
						</View>
						<View
							style={[
								styles.inputPicker,
								{padding: 4, marginTop: 0, backgroundColor: '#f7f7f7', width: responsiveWidth(26)}
							]}
						>
							<Text style={[{fontSize: responsiveFontSize(1.5)}]}>
								${itm.precio}
							</Text>
						</View>
					</View>
				);
			});
		}
		return btnProduct;
	}


	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text
					style={[styles.main_title, {
						fontSize: responsiveFontSize(3),
						color: this.state.thirdColor,
						textAlign: 'center'
					}]}
				>
					{this.state.elMenu.length === 0 ? 'No hay menu' : this.state.elMenu.titulo}
				</Text>
				<Text style={[{
					marginTop: 3,
					marginBottom: 15,
					fontSize: responsiveFontSize(1.7)
				}]}>{this.state.elMenu.length === 0 ? 'No hay menú' : this.state.elMenu.duracion}</Text>
				<ScrollView>
					{this.rndMenuDias()}
					<Text style={[styles.textW, {marginTop: 15}]}>Comentarios adicionales:</Text>
					<Text style={[
						styles.inputPicker,
						{
							marginTop: 3,
							padding: 8,
							height: responsiveHeight(15),
							borderColor: this.state.secondColor,
							backgroundColor: '#f1f1f1',
							borderBottomWidth: .5,
							fontStyle: 'italic',
							marginBottom: responsiveHeight(3),
							fontSize: responsiveFontSize(1.3)
						}
					]}>
						{this.state.elMenu.length === 0 ? 'No hay menú' : this.state.elMenu.comentario}
					</Text>
				</ScrollView>
			</View>
		);
	}
}
