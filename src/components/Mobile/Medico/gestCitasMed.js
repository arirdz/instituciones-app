import React from 'react';
import {
	Alert,
	AsyncStorage,
	ScrollView,
	StatusBar,
	Text,
	TouchableOpacity,
	View,
	RefreshControl
} from 'react-native';
import {
	responsiveFontSize, responsiveWidth
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import MultiBotonRow from '../Globales/MultiBotonRow';
import AgendarCitaMed from './AgendarCitaMed';
import ModalSelector from 'react-native-modal-selector';
import EditarDisp from '../Globales/EditarDisp';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Communications from 'react-native-communications';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class gestCitasMed extends React.Component {
	_onRefresh = () => {
		this.setState({refreshing: true});
		if (this.state.selected === 'Citas finalizadas') {
			this.getHistorial();
		}else if (this.state.selected ==='Citas recibidas'){
			this.getSolicitudes();
		} else if (this.state.selected ==='Citas realizadas') {
			this.getCitasHechas();
		}
	};
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			refreshing: false,
			items: {},
			botonSelected: 'Atender citas',
			laSolicitud: '',
			selected: 'Citas recibidas',
			solicitudes: [],
			historial: [],
			citasHechas: []
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		await this.getSolicitudes();
		await this.getHistorial();
		await this.getCitasHechas();
	}

	async componentWillMount() {
		await this.getURL();
	}

	async onChange(option) {
		this.setState({selected: option.label});
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected,
			indexSelected: indexSelected
		});
	}

	async getSolicitudes() {
		let solicitudesList = await fetch(
			this.state.uri + '/api/feed/citas/getCitas',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let listSolicitudes = await solicitudesList.json();
		await this.setState({solicitudes: listSolicitudes});
		await this.setState({refreshing: false});
	}

	async onListPressedSolicitud(indexSolicitud, solicitud) {
		this.setState({
			selectedIndexSolicitud: indexSolicitud,
			laSolicitud: solicitud
		});
	}

	borrar(indexSolicitud) {
		this.state.solicitudes.splice(indexSolicitud, 1);
		this.setState({aux: 0});
	}

	async getHistorial() {
		let solicitudesList = await fetch(this.state.uri + '/api/feed/citas/getCitas/aceptadas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let listSolicitudes = await solicitudesList.json();
		this.setState({historial: listSolicitudes});
		this.setState({refreshing: false});
	}


	historial(itm, i) {
		if (itm.user.role === 'Padre') {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View
						style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}
					>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							Se ha solicitado una cita del Sr(a).{' '}
							<Text style={styles.textW}>{itm.user.name}.</Text> tutor
							del alumno(a):{' '}
							<Text style={styles.textW}>{itm.hijos.name}.</Text>{' '}
							{'\n'}del Grado y grupo:{' '}
							<Text style={styles.textW}>
								{itm.hijos.grado} {itm.hijos.grupo}.
							</Text>
						</Text>

						<Text style={styles.textInfo}>
							Agendada para la fecha de:{' '}
							<Text style={styles.textW}>{itm.fecha_cita}</Text> a las
							{moment(itm.hora, 'HH:mm:ss').format('HH:mm')}. {'\n'}para tratar sobre:{' '}
							<Text style={styles.textW}>{itm.asunto}.</Text>
						</Text>
						{itm.aprobada === '2' ? (
							<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>*Esta cita fue
																								 cancelada</Text>) : null}
					</View>
				</View>
			);
		} else if (itm.user.role === 'Admin') {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View
						style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}
					>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							El maestro(a).{' '}
							<Text style={styles.textW}>{itm.user.name}.</Text>
							ha solicitado su asistencias en:{' '}
							<Text style={styles.textW}>{itm.hijo_id}.</Text>{' '}
						</Text>
						<Text style={styles.textInfo}>
							Agendada para la fecha de:{' '}
							<Text style={styles.textW}>{itm.fecha_cita}</Text> a las
							{moment(itm.hora, 'HH:mm:ss').format('HH:mm')} hrs. {'\n'}para tratar sobre:{' '}
							<Text style={styles.textW}>{itm.asunto}.</Text>
						</Text>
						{itm.aprobada === '2' ? (
							<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>
								*Esta cita fue cancelada
							</Text>) : null}
					</View>
				</View>
			);
		} else {
			if (itm.aprobada === '2') {
				return (
					<View
						key={i + 'cita'}
						style={[styles.cardCita, {
							borderColor: this.state.fourthColor,
							shadowColor: this.state.fourthColor
						}]}
					>
						<View
							style={{
								borderBottomWidth: 1,
								borderColor: this.state.fourthColor,
								padding: 5,
								alignItems: 'center'
							}}
						>
							<Text style={styles.notifTitle}>Solicitud de Cita</Text>
						</View>
						<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
							<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
								<Text style={styles.textInfo}>
									Has solicitado una cita para tratar el asunto de: <Text
									style={{fontWeight: '700', textAlign: 'center'}}>{itm.asunto}</Text>{'\n'}
									con el tutor del alumno:{' '}
									<Text style={{fontWeight: '700'}}>
										{itm.hijos.name}{'\n'} del
															   Grado: {itm.hijos.grado} Grupo: {itm.hijos.grupo}{' '}
									</Text>
								</Text>
								<Text style={{fontWeight: '700', fontStyle: 'italic'}}>{'\n'}
									Agendada el {itm.fecha_cita} a
									las {moment(itm.hora, 'HH:mm:ss').format('HH:mm')}
								</Text>
							</View>
							{itm.aprobada === '2' ? (
								<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>
									*Cancelaste esta cita
								</Text>) : null}
						</View>
					</View>
				);
			}
		}
	}

	historiales() {
		let btnSolicitudes = [];
		this.state.historial.forEach((itemSolicitud, indexSolicitud) => {
			btnSolicitudes.push(this.historial(itemSolicitud, indexSolicitud));
		});
		return btnSolicitudes;
	}

	//+-+-+-+--+-+--+-+-+Citas hechas
	async getCitasHechas() {
		let btnHecho = await fetch(this.state.uri + '/api/feed/citas/hechas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let btnHechos = await btnHecho.json();
		await this.setState({citasHechas: btnHechos});
		await this.setState({refreshing: false});
	}

	async cancelarLaCita(id) {
		fetch(this.state.uri + '/api/feed/citas/cancelar/' + id, {
			method: 'POST', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Aceptar Citas)', [{text: 'Entendido' /*onPress: () => Actions.drawer()*/}]);
				} else {
					Alert.alert('Cita cancelada', 'Has cancelado la cita', [{
						text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
					}]);
				}
			});
	}

	async cancelarCitaAgend(id) {

		Alert.alert('Cancelar cita', '¿Está seguro que desea cancelar la cita?', [{
			text: 'Si', onPress: () => this.cancelarLaCita(id)
		}, {text: 'No'}
		]);
	}

	rndHecho(itemSolicitud, i) {
		return (
			<View
				key={i + 'cita'}
				style={[styles.cardCita, {
					borderColor: this.state.fourthColor,
					shadowColor: this.state.fourthColor
				}]}>
				<View style={{
					borderBottomWidth: 1,
					borderColor: this.state.fourthColor,
					padding: 5,
					alignItems: 'center'
				}}>
					<Text style={styles.notifTitle}>Solicitud de Cita</Text>
				</View>
				<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
					<Text style={styles.textInfo}>
						Has solicitado una cita para tratar el asunto de: <Text
						style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.asunto}</Text>{'\n'}
						con el tutor del alumno:{' '}
						<Text style={{fontWeight: '700'}}>
							{itemSolicitud.hijos.name}{'\n'} del
															 Grado: {itemSolicitud.hijos.grado} Grupo: {itemSolicitud.hijos.grupo}{' '}
						</Text>
					</Text>
					<Text style={{fontWeight: '700', fontStyle: 'italic'}}>{'\n'}
						Agendada el {itemSolicitud.fecha_cita} a
						las {moment(itemSolicitud.hora, 'HH:mm:ss').format('HH:mm')}
					</Text>
				</View>
				<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
					<TouchableOpacity
						onPress={() => this.cancelarCitaAgend(itemSolicitud.id)}
						style={[styles.btnCita, {backgroundColor: '#fc001e', paddingVertical: 4}]}
					>
						<Text style={{color: '#fff'}}>Cancelar Cita</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}

	rndHechos() {
		let btnCitaH = [];
		this.state.citasHechas.forEach((it, i) => {
			btnCitaH.push(this.rndHecho(it, i))
		});
		return btnCitaH;
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-+-+
	async llamar(id) {
		await fetch(this.state.uri + '/api/datos/user/' + id, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (LLamada en citas)', [{
						text: 'Entendido', onPress: () => Actions.drawer()
					}]);
				} else {
					this.setState({numTel: responseJson[0].telefono_casa_tutor});
					Communications.phonecall(this.state.numTel, true);
				}
			});
	}

	async aceptarCita(id) {
		await fetch(this.state.uri + '/api/feed/citas/update/' + id, {
			method: 'POST', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Aceptar Citas)', [{
						text: 'Entendido'
					}]);
				} else {
					Alert.alert('¡Excelente!', 'Has Aceptado la cita', [{
						text: 'Entendido', onPress: () => Actions.refresh({key: 'gestCitasMed'})
					}]);
				}
			});
	}

	async requestMultipart(id) {

		Alert.alert('', '¿Está seguro de aceptar la cita?', [{
			text: 'Si', onPress: () => this.aceptarCita(id)
		}, {text: 'No'}
		]);
	}

	async requestMultipart1() {
		Alert.alert('¡Ups!', 'Por ahora no disponibles', [{
			text: 'Entendido'
		}]);
	}

	renderSolicitud(itm, i) {
		if (itm.user.role === 'Padre') {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View
						style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}
					>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							Se ha solicitado una cita del Sr(a).{' '}
							<Text style={styles.textW}>{itm.user.name}.</Text> tutor
							del alumno(a):{' '}
							<Text style={styles.textW}>{itm.hijos.name}.</Text>{' '}
							{'\n'}del Grado y grupo:{' '}
							<Text style={styles.textW}>
								{itm.hijos.grado} {itm.hijos.grupo}.
							</Text>
						</Text>
						<Text style={styles.textInfo}>
							Agendada para la fecha de:{' '}
							<Text style={styles.textW}>{itm.fecha_cita}</Text>
							a las {moment(itm.hora, 'HH:mm:ss').format('HH:mm')}. {'\n'}para tratar sobre:{' '}
							<Text style={styles.textW}>{itm.asunto}.</Text>
						</Text>
					</View>

					<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
						<TouchableOpacity
							onPress={() => this.requestMultipart(itm.id)}
							style={[styles.btnCita, {backgroundColor: '#3fa345'}]}
						>
							<Text style={{color: '#fff'}}>Aceptar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnCita, {backgroundColor: '#fc001e'}]}
						>
							<Text style={{color: '#fff'}}>Declinar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnCita, {backgroundColor: this.state.fourthColor}]}
						>
							<Text>Mensaje</Text>
							{/*<Ionicons name='ios-text' size={24}/>*/}
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.llamar(itm.user.id)}
							style={[styles.btnCita, {backgroundColor: this.state.fourthColor}]}
						>
							<Text>Llamar</Text>
							{/*<Ionicons name='ios-call' size={24}/>*/}
						</TouchableOpacity>
					</View>
				</View>
			);
		} else if (itm.user.role === 'Admin') {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View
						style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}
					>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							El maestro(a).{' '}
							<Text style={styles.textW}>{itm.user.name}.</Text>
							ha solicitado su asistencias en:{' '}
							<Text style={styles.textW}>{itm.hijo_id}.</Text>{' '}
						</Text>
						<Text style={styles.textInfo}>
							Agendada para la fecha de:{' '}
							<Text style={styles.textW}>{itm.fecha_cita}{' '}</Text>
							{' '}a las {moment(itm.hora, 'HH:mm:ss').format('HH:mm')} hrs. {'\n'}para tratar sobre:{' '}
							<Text style={styles.textW}>{itm.asunto}.</Text>
						</Text>
					</View>

					<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
						<TouchableOpacity
							onPress={() => this.requestMultipart(itm.id)}
							style={[styles.btnCita, {backgroundColor: '#3fa345'}]}
						>
							<Text style={{color: '#fff'}}>Aceptar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnCita, {backgroundColor: '#fc001e'}]}
						>
							<Text style={{color: '#fff'}}>Declinar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor}]}
						>
							<Ionicons name='ios-text' size={24}/>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.llamar(itm.user.id)}
							style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor}]}
						>
							<Ionicons name='ios-call' size={24}/>
						</TouchableOpacity>
					</View>
				</View>
			);
		}
	}

	async requestMultipart(id) {
		await fetch(
			this.state.uri + '/api/feed/citas/update/' + id,
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);

		Alert.alert('Cita aceptada', 'Has Aceptado la cita', [
			{
				text: 'Entendido',
				onPress: () => Actions.refresh({key: 'gestCitasMed'})
			}
		]);
	}

	async requestMultipart1() {
		Alert.alert('¡Ups!', 'Por ahora no disponibles', [
			{
				text: 'Entendido'
			}
		]);
	}

	renderSolicitudes() {
		let buttonsSolicitudes = [];
		this.state.solicitudes.forEach((itemSolicitud, indexSolicitud) => {
			buttonsSolicitudes.push(
				this.renderSolicitud(itemSolicitud, indexSolicitud)
			);
		});
		return buttonsSolicitudes;
	}

	gestCita() {
		let index = 0;
		const data = [
			{key: index++, label: 'Citas recibidas'},
			{key: index++, label: 'Historial'},
			{key: index++, label: 'Citas realizadas'}
		];
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Revisar solicitudes:
				</Text>
				<ModalSelector
					data={data}
					selectStyle={[
						styles.inputPicker,
						{borderColor: this.state.secondColor}
					]}
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
					selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
					initValue={this.state.selected}
					onChange={option => this.onChange(option)}
				/>
				{this.state.selected === 'Citas recibidas' ? (
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Solicitudes recientes
					</Text>
				) : this.state.selected === 'Historial' ? (
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Historial de solicitudes
					</Text>
				) : (
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Citas creadas
					</Text>
				)}
				<ScrollView
					style={[styles.widthall, {marginTop: 10, ...ifIphoneX({marginBottom: 40}, {marginBottom: 20})}]}
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
				>
					{this.state.selected === 'Citas recibidas' ? this.renderSolicitudes()
						: this.state.selected === 'Historial' ? this.historiales() : this.rndHechos()}
				</ScrollView>
			</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<MultiBotonRow
					itemBtns={[
						'Atender citas',
						'Solicitar cita',
						'Editar horario'
					]}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelected(indexBtn, itemBtn)
					}
					cantidad={3}
				/>
				{this.state.botonSelected === 'Atender citas' ? this.gestCita() : null}
				{this.state.botonSelected === 'Solicitar cita' ? (
					<AgendarCitaMed/>
				) : null}
				{this.state.botonSelected === 'Editar horario' ? (
					<EditarDisp/>
				) : null}
			</View>
		);
	}
}
