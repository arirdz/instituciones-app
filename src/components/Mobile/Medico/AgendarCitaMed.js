import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, Text, TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';
import GradoyGrupo from '../Globales/GradoyGrupo';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class AgendarCitaMed extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			laHora1: '',
			laHora3: '',
			selectedIndexDia2: -1,
			elDia2: '',
			mes: '',
			mes2: '',
			dia: '',
			dia2: '',
			dias: [],
			elAsunto: '',
			elAsunto2: '',
			elDia: '',
			botonSelected: 'Padres',
			elReceptor: '',
			elReceptor2: '',
			horas: [],
			grupos: [],
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexGrupos: -1,
			elGrupo: '',
			laHora: '',
			selectedIndexLista: -1,
			elTaller: '',
			selectedIndexAsunto: -1,
			selectedIndexDia: -1,
			textInputValue: '',
			alumnos: [],
			elAlumno: '',
			alumnosId: '',
			alumnosId2: '',
			elAlumno2: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getUserData();
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let role = await AsyncStorage.getItem('role');
		let puesto = await AsyncStorage.getItem('puesto');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			role: role,
			puesto: puesto,
			uri: uri
		});
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected, indexSelected: indexSelected
		});
	}

	async onListItemPressedLista(indexLista, taller) {
		await this.setState({
			selectedIndexLista: indexLista,
			elTaller: taller
		});
	}

	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado,
			selectedIndexGrupos: -1,
			elGrupo: '',
			selectedIndexHora: -1,
			laHora1: '',
			selectedIndexDia: -1,
			elDia: '',
			mes: '',
			dia: '',
			elAsunto: '',
			elAsunto2: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAlumno: '',
			elAlumno2: '',
			alumnosId: '',
			alumnosId2: '',
			elReceptor: '',
			elReceptor2: ''
		});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo,
			selectedIndexHora: -1,
			laHora1: '',
			selectedIndexDia: -1,
			elDia: '',
			elAsunto: '',
			elAsunto2: '',
			mes: '',
			dia: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAlumno: '',
			elAlumno2: '',
			alumnosId: '',
			alumnosId2: '',
			elReceptor: '',
			elReceptor2: ''
		});
		await this.getAlumnos();
	}

	async getUserData() {
		fetch(this.state.uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de cargar los datos de usuario si el error continua pónganse en contacto con soporte (Citas)', [{
							text: 'Entendido', onPress: () => Actions.drawer()
						}]);
				} else {
					this.setState({userData: responseJson});
				}
			});
	}

	// +++++++++++++++++++++++++++++++++++++DIAS++++++++++++++++++++++++++++++++++

	async getDias() {
		await this.setState({dias: []});
		await fetch(
			this.state.uri + '/api/feed/view/dias_dis/' + this.state.elReceptor,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar los dias', 'Ha ocurrido un error ' +
						responseJson.error.status_code +
						' al tratar de cargar los dias de disponibilidad si el error continua pónganse en contacto con soporte (Citas)',
						[{text: 'Entendido'}]
					);
				} else {
					if (responseJson.length === 0) {
						this.state.dias.push(
							{dia_atencion: '1'}, {dia_atencion: '2'}, {dia_atencion: '3'}, {dia_atencion: '4'}, {dia_atencion: '5'}
						);
					} else {
						this.setState({dias: responseJson});
					}
				}
			});
	}

	async onListItemPressedDia(index, dia, mes, sdia) {
		await this.setState({
			selectedIndexDia: index, elDia: dia, mes: mes, dia: sdia, selectedIndexHora: -1, laHora1: ''
		});
		await this.getHoras(this.state.elDia);
	}

	renderDia(itemDia, indexDia) {
		let a = this.state.dias.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexDia !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexDia === indexDia) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		let nombreDia = '';
		let num = parseInt(itemDia.dia_atencion) - 1;
		let sdia = 1;
		let mes = 0;
		if (num < moment().weekday()) {
			sdia = moment().weekday(num + 7);
			mes = sdia.format('MMMM');
			sdia = sdia.format('D');
		} else {
			sdia = moment().weekday(num);
			mes = sdia.format('MMMM');
			sdia = sdia.format('D');
		}
		if (itemDia.dia_atencion === '1') {
			nombreDia = 'Lunes';
		}
		if (itemDia.dia_atencion === '2') {
			nombreDia = 'Martes';
		}
		if (itemDia.dia_atencion === '3') {
			nombreDia = 'Miércoles';
		}
		if (itemDia.dia_atencion === '4') {
			nombreDia = 'Jueves';
		}
		if (itemDia.dia_atencion === '5') {
			nombreDia = 'Viernes';
		}
		return (<TouchableHighlight
			key={indexDia + 'Dia'}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.onListItemPressedDia(indexDia, itemDia.dia_atencion, mes, sdia)}>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>
					{nombreDia} {sdia} {mes}
				</Text>
			</View>
		</TouchableHighlight>);
	}

	renderDias() {
		let buttonsDias = [];
		this.state.dias.forEach((itemDia, indexDia) => {
			buttonsDias.push(this.renderDia(itemDia, indexDia));
		});
		return buttonsDias;
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++HORAS+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	async getDataUser() {
		let nreceptor = await fetch(this.state.uri + '/api/datos/user/' + this.state.elReceptor2, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let receptor = await nreceptor.json();
		await this.setState({
			info: receptor[0].nombre + ' ' + receptor[0].apellido_paterno + ' ' + receptor[0].apellido_materno
		});
	}

	showInfo() {
		return (<View style={{justifyContent: 'center'}}>
			<Text style={{fontWeight: '900', textAlign: 'center'}}>
				{this.state.info}
			</Text>
		</View>);
	}


	async getHoras(dia) {
		await fetch(this.state.uri + '/api/feed/view/horarios_at/' + this.state.elReceptor + '/' + dia,
			{
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en horas de atencion',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					if (responseJson.length === 0) {
						this.getHorasTodas();
					} else {
						this.setState({horas: responseJson});
					}
				}
			});
	}

	async getHorasTodas() {
		await fetch(this.state.uri + '/api/feed/view/horas/disponibles', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en horas de atencion',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					this.setState({horas: responseJson});
				}
			});
	}

	async onListItemPressedHora(index, hora, hora1) {
		await this.setState({
			selectedIndexHora: index,
			laHora: hora,
			laHora1: moment(hora1, 'HH:mm:ss').format('HH:mm')
		});
	}

	renderHora(itemHora, indexHora) {
		let a = this.state.horas.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexHora !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexHora === indexHora) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (<TouchableHighlight
			key={indexHora + 'hrs'}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.onListItemPressedHora(indexHora, itemHora.horas2.horas, itemHora.horas2.inicio)}
		>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>{itemHora.horas2.horas}</Text>
			</View>
		</TouchableHighlight>);
	}

	renderHoras() {
		let buttonsHoras = [];
		this.state.horas.forEach((itemHora, indexHora) => {
			buttonsHoras.push(this.renderHora(itemHora, indexHora));
		});
		return buttonsHoras;
	}

	// ++++++++++++++++++++++++++++++++++++++GUARDAR++++++++++++++++++++++++++++++

	async alertCita() {
		if (this.state.elReceptor2 === '' || this.state.elDia2 === '' || this.state.elAsunto2 === '' || this.state.laHora3 === '') {
			Alert.alert('Datos incompletos', 'Asegurece de llenar todos los campos para la cita');
		} else {
			Alert.alert('Solicitar cita', 'Está a punto de solicitar una cita\n¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.requestMultipart()}, {text: 'No'}
			])
		}
	}

	async requestMultipart() {
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			id_receptor: this.state.elReceptor2,
			padre_id: this.state.elReceptor2,
			hijo_id: this.state.alumnosId2,
			fecha_cita: this.state.dia2 + ' de ' + this.state.mes2,
			hora: this.state.laHora3,
			asunto: this.state.elAsunto2,
			aprobada: 0
		}));
		await fetch(this.state.uri + '/api/feed/citas', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'News'}})
						}]);
				} else {
					Alert.alert('Cita enviada', 'Su cita ha sido agendada correctamente.', [{
						text: 'Aceptar', onPress: () => Actions.HomePage()
					}]);
				}
			});
	}

	// +++++++++++++++++++++++++++++++++++++ALUMNOS+++++++++++++++++++++++++++++


	async getAlumnos() {
		let alumnoList = await fetch(this.state.uri + '/api/user/alumnos/' + this.state.elGrado + '/' + this.state.elGrupo, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let alumnosList = await alumnoList.json();
		await this.setState({alumnos: alumnosList});
	}

	async onListPressedAlumno(indexAlumno, alumno, padreId, id) {
		await this.setState({
			selectedIndexAlumno: indexAlumno,
			elAlumno: alumno,
			alumnosId: id,
			elReceptor: padreId,
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			laHora1: '',
			laHora3: '',
			elAsunto: '',
			elAsunto2: '',
			selectedIndexDia: -1,
			selectedIndexDia2: -1,
			elDia: '',
			elDia2: '',
			mes: '',
			mes2: '',
			dia: '',
			dia2: ''
		});
		await this.getDias();
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexAlumno === indexAlumno) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (<TouchableHighlight
			key={indexAlumno + 'Al'}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.padre_id, itemAlumno.id)}>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>{itemAlumno.name}</Text>
			</View>
		</TouchableHighlight>);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-ModalALumno
	async openListAlumn() {
		this.state.elAlumno = this.state.elAlumno2;
		this.state.alumnosId = this.state.alumnosId2;
		this.state.elReceptor = this.state.elReceptor2;
		this.state.selectedIndexAlumno = this.state.selectedIndexAlumno2;
		if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacío', 'Seleccione un grado para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacío', 'Seleccione un grupo para poder continuar', [
				{text: 'Entendido'}
			])
		} else {
			this.setState({isModalCateg: true, selectedIndexAlumno: this.state.selectedIndexAlumno2});
		}
	}

	async closeListAlumn() {
		if (this.state.selectedIndexAlumno === -1) {
			Alert.alert('Alumno no seleccionado', '', [{text: 'Entendido'}]);
		} else {
			this.state.elAlumno2 = this.state.elAlumno;
			this.state.alumnosId2 = this.state.alumnosId;
			this.state.elReceptor2 = this.state.elReceptor;
			this.state.selectedIndexAlumno2 = this.state.selectedIndexAlumno;
			this.setState({isModalCateg: false});
			await this.getDataUser();
		}
	}

//+-+-+-+--+-+-+-+-+-+--++MosdalAsuntos
	async openModalAsunto() {
		if (this.state.elAlumno2 === '') {
			Alert.alert('Sin alumno seleccionado', 'Seleccione antes a un alumno para poder continuar', [
				{text: 'Entendido'}
			])
		} else {
			this.state.selectedIndexHora = this.state.selectedIndexHora2;
			this.state.laHora1 = this.state.laHora3;
			this.state.elAsunto = this.state.elAsunto2;
			this.state.selectedIndexDia = this.state.selectedIndexDia2;
			this.state.elDia = this.state.elDia2;
			this.state.mes = this.state.mes2;
			this.state.dia = this.state.dia2;
			this.setState({
				isModalDia: true,
				selectedIndexHora: this.state.selectedIndexHora2,
				selectedIndexDia: this.state.selectedIndexDia2,
				elAsunto: this.state.elAsunto2
			});
		}
	}

	async closeModalAsunto() {
		if (this.state.elAsunto === '') {
			Alert.alert('Campo asunto vacío', 'Defina un asunto para continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.selectedIndexDia === -1) {
			Alert.alert('Campo día vacío', 'Seleccione un día para continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.selectedIndexHora === -1) {
			Alert.alert('Campo hora vacío', 'Seleccione una hora para continuar', [
				{text: 'Enterado'}
			])
		} else {
			this.state.selectedIndexHora2 = this.state.selectedIndexHora;
			this.state.laHora3 = this.state.laHora1;
			this.state.elAsunto2 = this.state.elAsunto;
			this.state.selectedIndexDia2 = this.state.selectedIndexDia;
			this.state.elDia2 = this.state.elDia;
			this.state.mes2 = this.state.mes;
			this.state.dia2 = this.state.dia;
			await this.setState({isModalDia: false});
		}
		await this.setState({aux: 0});
		await this.setState({aux: 0});
		await this.setState({aux: 0});
		await this.setState({aux: 0});
	}

	render() {
		let nombreDia = '';
		if (this.state.elDia2 === '1') {
			nombreDia = 'Lunes';
		}
		if (this.state.elDia2 === '2') {
			nombreDia = 'Martes';
		}
		if (this.state.elDia2 === '3') {
			nombreDia = 'Miércoles';
		}
		if (this.state.elDia2 === '4') {
			nombreDia = 'Jueves';
		}
		if (this.state.elDia2 === '5') {
			nombreDia = 'Viernes';
		}
		const alumnosList = this.renderAlumnos();
		const dias = this.renderDias();
		const horarios = this.renderHoras();
		const info = this.showInfo();
		return (<View style={styles.container}>
			<Modal
				isVisible={this.state.isModalCateg}
				backdropOpacity={0.8}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}>
				<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
					<Text style={[styles.main_title, styles.modalWidth]}>
						Seleccione el alumno:
					</Text>
					<View
						style={[styles.tabla, {
							borderColor: this.state.secondColor,
							marginTop: 10,
							height: responsiveHeight(60)
						}]}>
						<ScrollView>
							{alumnosList}
						</ScrollView>
					</View>

					<View style={[styles.modalWidth, styles.row]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModalCateg: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.closeListAlumn()}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
			<Modal
				isVisible={this.state.isModalDia}
				backdropOpacity={0.8}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}>
				<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
					<Text style={[styles.main_title, styles.modalWidth]}>
						Escriba el asunto:
					</Text>
					<TextInput
						onChangeText={text => (this.state.elAsunto = text)}
						placeholder={'Asunto'}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						defaultValue={this.state.elAsunto}
						style={[styles.inputPicker, styles.modalWidth, {
							borderColor: this.state.secondColor, height: responsiveHeight(5), padding: 5
						}]}/>
					<Text
						style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
						Seleccione el dia:
					</Text>
					<View style={[styles.tabla, {
						borderColor: this.state.secondColor,
						marginTop: 10,
						height: responsiveHeight(23)
					}]}>
						<ScrollView>
							{dias}
						</ScrollView>
					</View>
					<Text
						style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
						Seleccione una hora
					</Text>
					{this.state.selectedIndexDia !== -1 ? (
						<View
							style={[styles.tabla, {
								borderColor: this.state.secondColor,
								marginTop: 10,
								height: responsiveHeight(23)
							}]}
						>
							<ScrollView>
								{horarios}
							</ScrollView>
						</View>
					) : null}
					<View style={[styles.modalWidth, styles.row]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModalDia: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.closeModalAsunto()}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
			<ScrollView showsVerticalScrollIndicator={false}
						style={{...ifIphoneX({marginBottom: 40}, {marginBottom: 20})}}>
				<GradoyGrupo
					onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos(indexBtn, itemBtn)}
					onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado(indexBtn, itemBtn)}
					onListItemPressedLista={(indexBtn, itemBtn) => this.onListItemPressedLista(indexBtn, itemBtn)}
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione al alumno
				</Text>
				<View style={{
					alignItems: 'center',
					backgroundColor: this.state.fourthColor,
					padding: 15,
					borderRadius: 6
				}}
				>
					<TouchableOpacity
						style={[
							styles.bigButton,
							styles.btnShowMod,
							{
								marginTop: 5,
								marginBottom: 5,
								borderColor: this.state.secondColor,
								backgroundColor: '#fff'
							}
						]}
						onPress={() => this.openListAlumn()}>
						<Text>Ver lista</Text>
					</TouchableOpacity>
					<Text style={{marginTop: 10}}>Alumno seleccionado:</Text>
					<Text style={[styles.textButton, {color: 'black'}]}>
						{this.state.elAlumno2}
					</Text>
				</View>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Asunto y fecha
				</Text>
				<View style={{
					alignItems: 'center',
					backgroundColor: this.state.fourthColor,
					padding: 15,
					borderRadius: 6
				}}
				>
					<TouchableOpacity
						style={[
							styles.bigButton,
							styles.btnShowMod,
							{
								marginTop: 0,
								marginBottom: 5,
								borderColor: this.state.secondColor,
								backgroundColor: '#fff'
							}
						]}
						onPress={() => this.openModalAsunto()}
					>
						<Text>Ver Asunto y horarios</Text>
					</TouchableOpacity>
					<Text style={{marginTop: 10}}>Asunto seleccionado:</Text>
					<Text style={[styles.textButton, {color: 'black', fontSize: responsiveFontSize(2)}]}>
						{this.state.elAsunto2}
					</Text>
					<View style={{flexDirection: 'row', marginTop: 5}}>
						<Text style={{fontSize: responsiveFontSize(1.7)}}>Para el dia </Text>
						<Text style={[styles.textW, {color: 'black', fontSize: responsiveFontSize(1.7)}]}>
							{nombreDia} {this.state.dia2} {this.state.mes2}
						</Text>
						<Text style={{fontSize: responsiveFontSize(1.7)}}> a las </Text>
						<Text style={[styles.textW, {color: 'black', fontSize: responsiveFontSize(1.7)}]}>
							{this.state.laHora3}<Text
							style={{fontWeight: '500', fontSize: responsiveFontSize(1.7)}}> hrs</Text>
						</Text>
					</View>
				</View>
				<View style={{alignItems: 'center'}}>
					<Text
						style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 10}]}>
						Padre o tutor al que se solicita:
					</Text>
					{info}
				</View>
				<TouchableOpacity
					style={[styles.bigButton, {
						backgroundColor: this.state.secondColor
					}]}
					onPress={() => this.alertCita()}>
					<Text style={styles.textButton}>Enviar la solicitud</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>);
	}
}
