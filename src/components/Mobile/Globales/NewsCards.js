import React from 'react';
import {Alert, AsyncStorage, Text, TouchableOpacity, View} from 'react-native'
import {Actions} from 'react-native-router-flux';
import TimeAgo from 'react-native-timeago';
import styles from '../../styles';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Feather from "react-native-vector-icons/Feather";
import {responsiveWidth} from "react-native-responsive-dimensions";
import News from '../Globales/news';

export default class Noticias extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            enterados: ''
        };
    }

    async getURL() {
        let auth = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let role = await AsyncStorage.getItem('role');
        let hijo = await AsyncStorage.getItem('hijo');
        await this.setState({
            uri: uri,
            token: auth,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            role: role,
            hijo: hijo
        });
    }

    async componentWillMount() {
        await this.getURL();
        await this.getEnterado();
    }

    async getEnterado() {
        let id = this.props.id;
        let url = this.state.uri + '/api/feed/view/enterados/' + id;
        if (this.state.role === 'Padre') {
            url = this.state.uri + '/api/feed/view/enterados/' + id + '/' + this.state.hijo;
        }
        await fetch(url, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Card Noticia)', [{
                        text: 'Entendido', onPress: () => Actions.drawer()
                    }]);
                } else {
                    this.setState({enterados: responseJson});
                }
            });
    }

    isEnterado() {
        return (<View
            style={[styles.enteradoBtnOutside, {backgroundColor: this.state.mainColor, alignItems: 'center'}]}>
            <Text style={styles.textEnterado}>Leído</Text>
        </View>);
    }

    noEnterado() {
        return (<View
            style={[styles.enteradoBtnOutside, {backgroundColor: this.state.secondColor}]}>
            <Text style={styles.textEnterado}>Por leer</Text>
        </View>);
    }

    delete() {
        Alert.alert('Atención', 'Al borrar la noticia ya no se podrá recuperar, ¿está seguro de continuar?', [{
            text: 'Entendido', onPress: () => this.deleteConfirm()
        }, {
            text: 'Cancelar'
        }]);
    }

    async deleteConfirm() {
        await fetch(this.state.uri + '/api/eliminar/noticia/' + this.props.id, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error contacte a soporte', [{
                        text: 'Entendido'
                    }]);
                } else {
                    Alert.alert('Enhorabuena', 'Noticia Borrada', [{
                        text: 'Entendido', onPress: () => this.props.refresh
                    }]);
                }
            });
    }

    edit(it) {
        Alert.alert('Atención', 'Al editar la noticia ya no se podrá recuperar lo anterior, ¿está seguro de continuar?', [{
            text: 'Entendido', onPress: () => this.editConfirm()
        }, {
            text: 'Cancelar'
        }]);
        this.setState({deleteId: this.props.id})
    }

    editConfirm(it) {
        console.log('Edited')
    }

    render() {
        const onPressButton = () => Actions.newsSingle({data: this.props});
        return (<TouchableOpacity onPress={onPressButton} style={styles.card}>
            <View
                style={styles.noticia}>
                <View style={[styles.head, {padding: 5}]}>
                    <Text numberOfLines={1} style={styles.titulo_noticia}>
                        {this.props.titulo}
                    </Text>
                    <TimeAgo
                        style={styles.fecha}
                        interval={20000}
                        time={this.props.created_at}
                    />
                </View>
                <View
                    style={[styles.descripcion, {padding: 5, paddingBottom: 0}]}>
                    <Text>{this.props.resumen}</Text>
                </View>
                <View style={[styles.row, {padding: 5, paddingTop: 0}]}>
                    <View style={{
                        textAlign: "right",
                        width: responsiveWidth(74)
                    }}>{this.state.enterados !== 0 ? this.isEnterado() : this.noEnterado()}</View>

                    <View style={{textAlign: "right", width: responsiveWidth(10)}}>
                        <MaterialIcons name='edit' size={22} color='gray'
                                       onPress={() => this.edit()}/>
                    </View>
                    <View style={{textAlign: "right", width: responsiveWidth(10)}}>
                        <Feather name='minus-circle' size={22} color='red'
                                 onPress={() => this.delete()}/>
                    </View>
                </View>

            </View>
        </TouchableOpacity>);
    }
}
