import React from 'react';
import PropTypes from 'prop-types';
import {AsyncStorage, Platform, ScrollView, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {MediaQuery} from 'react-native-responsive';
import Modal from 'react-native-modal';
import Feather from 'react-native-vector-icons/Feather';
import {ifIphoneX} from "react-native-iphone-x-helper";

export default class GradoyGrupo extends React.Component {

    static propTypes = {
        urlGrado: PropTypes.string,
        urlGrupo: PropTypes.string,
        objetoGrupo: PropTypes.string,
        objetoGrado: PropTypes.string,
        gradoSelected: PropTypes.string,
        grupoSelected: PropTypes.string,
        onListItemPressedGrado: PropTypes.func.isRequired,
        onListItemPressedGrupos: PropTypes.func.isRequired,
        onListItemPressedLista: PropTypes.func,
        indxValGrad: PropTypes.number,
        indxValGrup: PropTypes.number,
        itmValGrad: PropTypes.string,
        itmValGrup: PropTypes.string,
        todos: PropTypes.string,
        listaVar: PropTypes.bool
    };

    static defaultProps = {
        urlGrado: '/api/get/grados',
        urlGrupo: '/api/get/grupos',
        urlLista: '/api/get/lista',
        objetoGrupo: 'grupos',
        objetoGrado: 'grados',
        gradoSelected: '',
        grupoSelected: '',
        indxValGrad: -1,
        indxValGrup: -1,
        itmValItmGrad: '',
        itmValItmGrup: '',
        todos: '0',
        listaVar: false
    };

    constructor(props) {
        super(props);
        this.state = {
            grado: this.props.itmValGrad,
            selectedIndexGrados: this.props.indxValGrad,
            isModalLista: false,
            grados: [],
            talleres: '',
            grupo: this.props.itmValGrup,
            selectedIndexGrupos: this.props.indxValGrup,
            lasListas: [],
            uri: '',
            token: '',
            grupos: [],
            todos: '0',
            listaVar: false,
            listaId: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getGrados();
        await this.getGrupos();
        await this.getListas();
        await this.setSelected();
    }

    setSelected() {
        let it = [];
        it['grupo'] = this.props.grupoSelected;
        it['grado'] = this.props.gradoSelected;

        function esGrupo(grupo) {
            return grupo.grupo === it.grupo;
        }

        function esGrado(grado) {
            return grado.grado === it.grado;
        }

        function esLista(lista) {
            return lista.taller === it.grupo;
        }

        if (this.state.grupos.find(esGrupo)) {
            let ix = this.state.grupos.indexOf(this.state.grupos.filter(function (item) {
                return item.grupo === it.grupo;
            })[0]);
            if (this.state.grados.find(esGrado)) {
                let ixG = this.state.grados.indexOf(this.state.grados.filter(function (item) {
                    return item.grado === it.grado;
                })[0]);
                this.setState({selectedIndexGrupos: ix, grupo: this.props.grupoSelected});
                this.setState({selectedIndexGrados: ixG, grado: this.props.gradoSelected})
            }
        } else {
            if (this.state.lasListas.find(esLista)) {
                let li = this.state.lasListas.indexOf(this.state.lasListas.filter(function (item) {
                    return item.taller === it.grupo;
                })[0]);
                this.setState({
                    selectedIndexLista: li,
                    talleres: this.state.lasListas[li].taller,
                    litsaId: this.state.lasListas[li].id
                });
            }
        }
    }

    async getURL() {
        this.setState({
            uri: await AsyncStorage.getItem('uri'),
            token: await AsyncStorage.getItem('token'),
            mainColor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor')
        });
    }

    async getGrados() {
        let gradpicker = await fetch(this.state.uri + this.props.urlGrado);
        let gradospicker = await gradpicker.json();
        this.setState({
            grados: gradospicker
        });
    }

    renderGrados() {
        let buttons = [];
        this.state.grados.forEach((item, index) => {
            buttons.push(this.renderGrado(item, index));
        });
        return buttons;
    }

    renderGrados1() {
        let buttons = [];
        this.state.grados.forEach((item, index) => {
            buttons.push(this.renderGrado1(item, index));
        });
        return buttons;
    }

    renderGrado(listItem, index) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn6, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.grado === listItem.grado) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<View key={index + 'rndGdo'}>
            {this.props.todos === '1' && index < 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrado(index, listItem.grado)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{listItem.grado}</Text>
                </View>
            </TouchableHighlight>) : listItem.grado !== 'Todos' && index < 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrado(index, listItem.grado)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{listItem.grado}</Text>
                </View>
            </TouchableHighlight>) : null}
        </View>);
    }

    renderGrado1(listItem, index) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn6, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.grado === listItem.grado) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<View key={index + 'rndGdo'}>
            {this.props.todos === '1' && index >= 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrado(index, listItem.grado)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{listItem.grado}</Text>
                </View>
            </TouchableHighlight>) : listItem.grado !== 'Todos' && index >= 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrado(index, listItem.grado)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{listItem.grado}</Text>
                </View>
            </TouchableHighlight>) : null}
        </View>);
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++GRUPOS+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async getGrupos() {
        let grupopicker = await fetch(this.state.uri + this.props.urlGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        });
        let grupospicker = await grupopicker.json();
        this.setState({grupos: grupospicker});
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.props.onListItemPressedGrupos(indexGrupo, grupo);
        await this.setState({
            selectedIndexGrupos: indexGrupo, grupo: grupo, selectedIndexLista: -1, talleres: ''
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.props.onListItemPressedGrado(indexGrado, grado);
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado,
            selectedIndexLista: -1,
            talleres: ''
        });
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    renderGrupos1() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo1(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    renderGrupo1(itemGrupo, indexGrupo) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn6, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.grupo === itemGrupo.grupo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (<View key={indexGrupo + 'rndGpo'}>
            {this.props.todos === '1' && indexGrupo >= 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrupos(indexGrupo, itemGrupo.grupo)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>) : itemGrupo.grupo !== 'Todos' && indexGrupo >= 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrupos(indexGrupo, itemGrupo.grupo)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>) : null}
        </View>);
    }

    renderGrupo(itemGrupo, indexGrupo) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn6, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.grupo === itemGrupo.grupo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (<View key={indexGrupo + 'rndGpo1'}>
            {this.props.todos === '1' && indexGrupo < 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrupos(indexGrupo + 10, itemGrupo.grupo)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>) : itemGrupo.grupo !== 'Todos' && indexGrupo < 6 ? (<TouchableHighlight
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedGrupos(indexGrupo + 10, itemGrupo.grupo)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>) : null}
        </View>);
    }

    //+++++++++++++++++++++++++++++++++++++listas variables
    async getListas() {
        let lista = await fetch(this.state.uri + this.props.urlLista, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        });
        let listas = await lista.json();
        await this.setState({lasListas: listas});
    }

    async onListItemPressedLista(indexLista, taller, id) {
        await this.props.onListItemPressedLista(indexLista, taller, id);
        await this.setState({
            selectedIndexLista: indexLista,
            talleres: taller,
            litsaId: id,
            selectedIndexGrupos: -1,
            grupo: '',
            selectedIndexGrados: -1,
            grado: ''
        });
    }

    rendLista(itemList, indexList) {
        let a = this.state.lasListas.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexList !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];
        if (this.state.selectedIndexLista === indexList) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<TouchableHighlight
            key={indexList + 'rndList'}
            underlayColor={'transparent'}
            style={[tabRow, smallButtonStyles]}
            onPress={() => this.onListItemPressedLista(indexList, itemList.taller, itemList.id)}
        >
            <View style={[styles.campoTablaG, {alignItems: 'center'}]}>
                <Text style={[texto, {marginRight: 25}]}>{itemList.taller}</Text>
            </View>
        </TouchableHighlight>);
    }

    rendListas() {
        let btnLista = [];
        this.state.lasListas.forEach((itemList, indexList) => {
            btnLista.push(this.rendLista(itemList, indexList));
        });
        return btnLista;
    }

    render() {
        const talleres = this.rendListas();
        const grupos = this.renderGrupos();
        const list = this.renderGrados();
        const list1 = this.renderGrados1();
        const grupos1 = this.renderGrupos1();
        let altura = [];
        if (this.props.todos === '1') {
            altura = [{...ifIphoneX({height: responsiveHeight(21.6)}, {height: responsiveHeight(23.5)})}];
            if (this.state.grupos.length > 6) {
                altura = [{height: responsiveHeight(32)}];
            } else if (this.state.grupos.length - 1 > 6) {
                altura = [{height: responsiveHeight(32)}];
            }
        } else {
            altura = [{...ifIphoneX({height: responsiveHeight(18)}, {height: responsiveHeight(23.5)})}];
            if (this.state.grupos.length > 6) {
                altura = [{height: responsiveHeight(32)}];
            } else if (this.state.grupos.length - 1 > 6) {
                altura = [{height: responsiveHeight(32)}];
            }
        }
        let listaSel = this.state.talleres;
        return (<View>
            {/* para iphone */}
            <MediaQuery maxDeviceHeight={896}>
                <View>
                    <View style={altura}>
                        <View>
                            <Text
                                style={[styles.main_title, {color: this.state.thirdColor}]}>
                                Seleccione grado y grupo
                            </Text>
                        </View>
                        <Text style={styles.subTitulo}>Grado</Text>
                        <View style={styles.buttonsRow}>{list}</View>
                        {this.props.todos === '1' ? (
                                this.state.grados.length > 6 ? (
                                    <View style={[styles.buttonsRow, {marginTop: Platform.OS === 'ios' ? null : 15}]}>
                                        {list1}
                                    </View>
                                ) : null) :
                            this.state.grados.length - 1 > 6 ? (
                                <View
                                    style={[styles.buttonsRow, {marginTop: Platform.OS === 'ios' ? null : 15}]}>{list1}</View>
                            ) : null}
                        <Text style={[styles.subTitulo, {marginTop: Platform.OS === 'ios' ? 7 : 15}]}>Grupo</Text>
                        <View style={styles.buttonsRow}>{grupos}</View>
                        {this.props.todos === '1' ? (
                                this.state.grupos.length > 6 ? (
                                    <View
                                        style={[styles.buttonsRow, {marginTop: Platform.OS === 'ios' ? null : 15}]}>{grupos1}</View>
                                ) : null)
                            : this.state.grupos.length - 1 > 6 ? (
                                <View
                                    style={[styles.buttonsRow, {marginTop: Platform.OS === 'ios' ? null : 15}]}>{grupos1}</View>) : null}
                    </View>
                    {this.props.listaVar === true ? (<View style={[styles.row, styles.btn1, {
                        borderBottomWidth: 1, borderTopWidth: 1, marginTop: Platform.OS === 'ios' ? 5 : 15
                    }]}>
                        <View style={{paddingVertical: 5}}>
                            <Text style={{fontWeight: '600'}}>O elija una lista personalizada</Text>
                            <Text>Actual: <Text
                                style={styles.textW}>{''}{this.state.talleres === '' ? ('Ninguno') : listaSel}</Text></Text>
                        </View>
                        <TouchableOpacity
                            style={[styles.inputPicker, styles.btn3, {
                                backgroundColor: this.state.secondColor,
                                height: responsiveHeight(4.7),
                                marginBottom: 0,
                                padding: 0
                            }]}
                            onPress={() => this.setState({isModalLista: true})}>
                            <Text style={[styles.textButton, {fontSize: responsiveFontSize(1.7)}]}>
                                Ver listas
                            </Text>
                        </TouchableOpacity>
                    </View>) : null}
                    <Modal
                        isVisible={this.state.isModalLista}
                        backdropOpacity={0.8}
                        animationIn={'bounceIn'}
                        animationOut={'bounceOut'}
                        animationInTiming={1000}
                        animationOutTiming={1000}
                    >
                        <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight: responsiveHeight(80)}]}>
                            <View style={[styles.modalWidth, {marginTop: 5, alignItems: 'flex-end'}]}>
                                <Feather name='x' size={20} color={'black'}
                                         onPress={() => this.setState({isModalLista: false})}/>
                            </View>
                            <Text style={[styles.main_title, styles.modalWidth, {marginTop: 0, paddingTop: 0}]}>
                                Lista de talleres
                            </Text>
                            <ScrollView>
                                <View
                                    style={[styles.tabla, {borderColor: this.state.secondColor, marginTop: 10}]}>
                                    {talleres}
                                </View>
                            </ScrollView>
                            <View style={[styles.modalWidth, styles.row, {marginBottom: 5}]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.setState({
                                        isModalLista: false,
                                        selectedIndexLista: -1,
                                        talleres: '',
                                        litsaId: ''
                                    })}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Limpiar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.setState({isModalLista: false})}
                                >
                                    <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>
            </MediaQuery>
            {/* para ipad */}
            <MediaQuery maxDeviceWidth={1024} minDeviceHeight={897}>
                <View>
                    <View style={altura}>
                        <View>
                            <Text
                                style={[styles.main_title, {color: this.state.thirdColor}]}>
                                Seleccione grado y grupos
                            </Text>
                        </View>
                        <Text style={styles.subTitulo}>Grado</Text>
                        <View style={styles.buttonsRow}>{list}</View>
                        <Text style={styles.subTitulo}>Grupo</Text>
                        <View style={styles.buttonsRow}>{grupos}</View>
                        {this.props.todos === '1' ? (this.state.grupos.length > 6 ? (<View
                            style={styles.buttonsRow}>{grupos1}</View>) : null) : this.state.grupos.length - 1 > 6 ? (
                            <View style={styles.buttonsRow}>{grupos1}</View>) : null}
                    </View>
                </View>
            </MediaQuery>
        </View>);
    }
}
