import React from 'react';
import {AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../styles';
import GradoyGrupo from './GradoyGrupo';
import Feather from "react-native-vector-icons/Feather";

export default class newMessage extends React.Component {
    _keyExtractor = (item, index) => item.id;

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            userData: '',
            nombres: [],
            userbyrole: [],
            selectedIndex: 1,
            textInputValue: 'Administrador',
            id: 'Admin',
            materias: [],
            laMateria: [],
            grado: '',
            grupo: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getUserdata();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let mainColor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: mainColor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getRole();
        await this.getMaterias();
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    //+++++++++++++++++++++++++++++++usuarios
    async getUserdata() {
        let url;
        if (this.state.id === 'Admin') {
            url = this.state.uri +
                '/api/users/mensajes/' +
                this.state.id
        }
        else if (this.state.id === 'Maestro' || this.state.id === 'Padre') {
            url = this.state.uri +
                '/api/users/mensajes/' +
                this.state.id +
                '/' +
                this.state.grado +
                '/' +
                this.state.grupo;
        }
        let request = await fetch(
            url,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let dato = await request.json();
        await this.setState({nombres: dato});
    }

    async onListPressedName(index, user) {
        await this.setState({
            selectedIndexName: index,
            userData: user
        });
        Actions.Messages({
            title: this.state.userData.name,
            data: this.state.userData
        })
    }

    renderName(it, i) {
        let smallButtonStyles = [
            {borderColor: this.state.secondColor, borderTopWidth: 0}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexName === i) {
            smallButtonStyles.push({
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableOpacity
                style={[styles.mList, {borderColor: this.state.fourthColor, alignItems: 'center'}]}
                onPress={() => this.onListPressedName(i, it.hijos !== undefined
                    ? it.hijos
                    : it.maestro !== undefined
                        ? it.maestro
                        : it !== undefined
                            ? it
                            : null)}>
                <View>
                    <Text style={styles.nombre}>
                        {it.hijos !== undefined
                            ? it.hijos.name
                            : it.maestro !== undefined
                                ? it.maestro.name
                                : it.name !== undefined
                                    ? it.name
                                    : null}
                    </Text>
                    <Text style={styles.textInfo}>
                        {it.hijos !== undefined
                            ? it.name
                            : it.maestro !== undefined
                                ? it.nombre.nombre
                                : it.name !== undefined
                                    ? it.puesto
                                    : null}
                    </Text>
                </View>
                <Feather
                    name="chevron-right"
                    size={20}
                    color="black"
                />
            </TouchableOpacity>
        );
    }

    renderNames() {
        let buttonsNames = [];
        this.state.nombres.forEach((itemName, indexName) => {
            buttonsNames.push(this.renderName(itemName, indexName));
        });
        return buttonsNames;
    }

    async onListItemPressed(index, grado, grupo) {
        await this.setState({
            selectedIndex: index,
            grado: grado,
            grupo: grupo
        });
    }

    async onListItemPressedName(index) {
        await this.setState({
            selectedIndexName: index
        });
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, id: option.id});
        await this.getUserdata();
    }

    async onListItemPressedGrado(index, grado) {
        await this.setState({
            selectedIndex: index,
            grado: grado
        });
    }

    async onListItemPressedGrupo(index, grupo) {
        await this.setState({
            selectedIndex: index,
            grupo: grupo
        });
        await this.getUserdata();
    }

    //++++++++++++++++++++materia alumno
    async getMaterias() {
        let materiapicker = await fetch(
            this.state.uri + '/api/get/materias/alumno',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    isAlumno() {
        const nombres = this.renderNames();
        let data1 = this.state.materias.map((item, i) => {
            return {
                grado: item.materia.materia[0].grado,
                key: i,
                label:
                    item.materia.materia[0].nombre + ' ' + item.materia.materia[0].grado,
                materia: item.materia.id_materia
            };
        });
        return (
            <View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione la materia
                </Text>
                <View style={[styles.asuntos]}>
                    <ModalSelector
                        data={data1}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        initValue="Seleccione la materia"
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        onChange={option => this.onChange1(option)}
                    />
                </View>
                <View
                    style={{
                        width: responsiveWidth(94),
                        height: responsiveHeight(30),
                        marginTop: 35,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <View style={[styles.contAlumnos2, {borderWidth: 0}]}>
                        <ScrollView
                            style={[
                                styles.titulosContainer,
                                {borderColor: this.state.secondColor}
                            ]}
                            horizontal={false}
                            showsVerticalScrollIndicator={false}>
                            {nombres}
                        </ScrollView>
                    </View>
                    <TouchableHighlight
                        onPress={() =>
                            Actions.Messages({
                                title: 'alo2',
                                data: this.state.userData
                            })
                        }
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.mainColor,
                                marginTop: responsiveHeight(10)
                            }
                        ]}>
                        <Text style={styles.textButton}>Enviar Mensaje</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    isPapa2() {
        return (
            <View>
                <Text style={styles.main_title}>Seleccione un usuario</Text>
                {this.renderNames()}
            </View>
        )
    }

    isAdmin() {
        const nombres = this.renderNames();
        let dex = 0;
        const data = [
            {key: dex++, label: 'Maestro', id: 'Maestro'},
            {key: dex++, label: 'Administrativo', id: 'Admin'},
            {key: dex++, label: 'Padre', id: 'Padre'}
        ];
        return (
            <View style={styles.container}>
                <ScrollView
                    horizontal={false}
                    showsVerticalScrollIndicator={false}
                    overScrollMode={'always'}>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el destinatario
                    </Text>
                    <View style={[styles.asuntos]}>
                        <ModalSelector
                            data={data}
                            selectStyle={[
                                [styles.inputPicker, {borderColor: this.state.secondColor}],
                                {marginTop: 1}
                            ]}
                            initValue="Administrativo"
                            cancelText="Cancelar"
                            optionTextStyle={{color: this.state.thirdColor}}
                            onChange={option => this.onChange(option)}
                        />
                    </View>
                    {this.state.id !== 'Admin' ? (
                        <GradoyGrupo
                            onListItemPressedGrado={(index, grado) =>
                                this.onListItemPressedGrado(index, grado)
                            }
                            onListItemPressedGrupos={(index, grupo) =>
                                this.onListItemPressedGrupo(index, grupo)
                            }
                            todos={'0'}
                        />
                    ) : null}
                    <Text style={styles.main_title}>Seleccione un usuario</Text>
                    {nombres}
                </ScrollView>
            </View>
        );
    }

    render() {
        return (
            <View style={[styles.container, {height: responsiveHeight(100)}]}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {this.state.admin === 'Padre' ? this.isPapa2() : null}
                {this.state.admin === 'Admin' ? this.isAdmin() : null}
                {this.state.admin === 'Alumno' ? this.isPapa2() : null}
                {this.state.admin === 'Maestro' ? this.isPapa2() : null}
            </View>
        );
    }
}
