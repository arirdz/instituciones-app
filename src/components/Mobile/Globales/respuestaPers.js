import React from 'react';
import {
	AsyncStorage, ScrollView, Text, TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Entypo from 'react-native-vector-icons/Entypo';
import Modal from 'react-native-modal';

export default class respuestaPers extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isModalSub: [],
			laSubOpcion: [],
			laOpcion: [],
			selectedIndexSub: [],
			elSub: [],
			elIndice: '',
			indice: '',
			aux: 0,
			btnPrueba: [
				{tipo: 'Múltiple'},
				{tipo: 'Única'},
				{tipo: 'Numérica'},
				{tipo: 'Texto'}
			],
			btnSub: [
				{tipo: 'Múltiple'},
				{tipo: 'Única'},
				{tipo: 'Numérica'},
				{tipo: 'Texto'}
			],
			elSubText: [],
			selectedIndexPrueba: 0,
			laPrueba: 'Múltiple',
			laSubResp: [],
			laResp: [],
			elText: [],
			losTexts: [0],
			losSubTexts: [0],
			subLel: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.pushes();
	}

	async getURL() {
		let auth = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		await this.setState({
			uri: uri,
			token: auth,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async saveOpcion() {
		let elBuenObjeto = [];
		this.state.laResp.forEach((it, ix) => {
			if (((this.state.laPrueba === 'Múltiple') || (this.state.laPrueba === 'Única')) &&
				((this.state.elSub[ix] === 'Múltiple') || (this.state.elSub[ix] === 'Única'))) {
				let suvRespuestas = [];
				for (let i = 1; i <= this.state.losTexts.length; i++) {
					let extra = [];
					for (let j = 0; j < this.state.losSubTexts.length; j++) {
						if (this.state.laSubResp[i + '' + j] !== '' && this.state.laSubResp[i + '' + j] !== undefined) {
							extra.push(this.state.laSubResp[i + '' + j]);
						}
					}
					suvRespuestas.push(extra);
				}
				elBuenObjeto.push({
					'Tipo': this.state.laPrueba,
					'Respuesta': this.state.laResp[ix],
					'SubTipo': this.state.elSub[ix],
					'SubRespuestas': suvRespuestas[ix]
				});
			} else if (((this.state.laPrueba === 'Múltiple') || (this.state.laPrueba === 'Única')) && (this.state.elSub[ix] === 'Numérica')) {
                elBuenObjeto.push({
					'Tipo': this.state.laPrueba,
					'Respuesta': this.state.laResp[ix],
					'SubTipo': this.state.elSub[ix]
				});
			} else if (((this.state.laPrueba === 'Múltiple') || (this.state.laPrueba === 'Única')) && (this.state.elSub[ix] === 'Texto')) {
                elBuenObjeto.push({
					'Tipo': this.state.laPrueba,
					'Respuesta': this.state.laResp[ix],
					'SubTipo': this.state.elSub[ix]
				});
			}
		});
		if (this.state.laPrueba === 'Numérica') {
            elBuenObjeto.push({
				'Tipo': this.state.laPrueba
			});
		} else if (this.state.laPrueba === 'Texto') {
            elBuenObjeto.push({
				'Tipo': this.state.laPrueba
			});
		}
	}

	async pushes() {
		this.state.losTexts.forEach(() => {
			this.state.isModalSub.push(false)
		})
	}

	async isSubCarac(text, i) {
		this.state.elSubText[i] = text;
		this.setState({aux: 0})
	}

	async isCaracteres(text) {
		this.setState({elText: text})
	}

	//++++++++++++++++++++++++++++++++++++++++++++Respuesta
	async agregarOtro() {
		this.state.losTexts.length !== 4 ?
			this.state.losTexts.push(0)
			: null;
		await this.setState({aux: 0})
	}

	borrar(index) {
		this.state.losTexts.splice(index, 1);
		this.setState({aux: 0});
	}

	async openModal(i) {
		this.state.isModalSub[i] = true;
		await this.setState({aux: 0});
	}

	async closeModal(i) {
		this.state.isModalSub[i] = false;
		await this.setState({aux: 0});
	}

	isModal(i) {
		const subElec = this.renderSubs(i);
		const subTextInpt = this.subRespuestas(i);
		return (
			<View>
				<Modal
					isVisible={this.state.isModalSub[i]}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}>
					<View style={[styles.container, {borderRadius: 6}]}>
						<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
							Seleccione el tipo de sub respuesta
						</Text>
						<View
							style={[
								styles.row,
								{height: responsiveHeight(7)}
							]}>
							{subElec}
						</View>
						{this.state.selectedIndexSub[i] === 0 ? (
								<View
									style={[styles.modalWidth, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
									<Text
										style={{
											fontSize: responsiveFontSize(1.5),
											fontStyle: 'italic',
											textAlign: 'center',
											marginBottom: 5
										}}>
										Con esta selección el destinatario podrá elegir más de una de las opciones de
										respuesta
									</Text>
								</View>
							) :
							this.state.selectedIndexSub[i] === 1 ? (
									<View style={[styles.modalWidth, {
										marginTop: 5,
										alignItems: 'center',
										borderBottomWidth: 1
									}]}>
										<Text
											style={{
												fontSize: responsiveFontSize(1.5),
												fontStyle: 'italic',
												textAlign: 'center',
												marginBottom: 5
											}}>
											Con esta selección el destinatario podrá elegir solo una de las opciones de
											respuesta
										</Text>
									</View>
								) :
								this.state.selectedIndexSub === 2 ? (
										<View style={[styles.modalWidth, {
											marginTop: 5,
											alignItems: 'center',
											borderBottomWidth: 1
										}]}>
											<Text
												style={{
													fontSize: responsiveFontSize(1.5),
													fontStyle: 'italic',
													textAlign: 'center',
													marginBottom: 5
												}}>
												Con esta selección el destinatario podrá capturar un número como respuesta
											</Text>
										</View>
									) :
									this.state.selectedIndexSub[i] === 3 ? (
										<View
											style={[
												styles.modalWidth,
												{
													marginTop: 5,
													alignItems: 'center',
													borderBottomWidth: 1
												}
											]}>
											<Text
												style={{
													fontSize: responsiveFontSize(1.5),
													fontStyle: 'italic',
													textAlign: 'center',
													marginBottom: 5
												}}>
												Con esta selección el destinatario podrá capturar un texto como
												respuesta
											</Text>
										</View>
									) : null}
						{this.state.selectedIndexSub[i] === 0 || this.state.selectedIndexSub[i] === 1 ? (
							<ScrollView>
								<View style={[styles.modalWidth, {alignItems: 'center'}]}>
									<Text style={{marginTop: 10}}>
										Defina la sub-respuesta que habrá en la noticia
									</Text>
									{subTextInpt}
									{this.state.losSubTexts.length !== 2 ? (<View><TouchableOpacity
										onPress={() => this.agregarOtroSub()}
									>
										<Entypo name='plus' size={35} color='#2b2b2b'/>
									</TouchableOpacity>
										<Text>Agregue otra sub-respuesta</Text></View>) : null}
								</View>
							</ScrollView>
						) : this.state.selectedIndexSub[i] === 2 ? (
							<ScrollView>
								<View style={{alignItems: 'center'}}>
									<Text style={{marginTop: 10}}>
										Defina que parámetro numérico tendrá esta respuesta
									</Text>
									<TextInput
										keyboardType='numeric'
										maxLength={2}
										onChangeText={text => this.onChangeNum(text, i)}
										placeholder={'ej.: del 1 al 99'}
										returnKeyType='next'
										underlineColorAndroid='transparent'
										value={this.state.subLel[i]}
										style={[
											styles.inputPicker,
											styles.btn11,
											{
												borderColor: this.state.secondColor,
												height: responsiveHeight(5),
												padding: 5
											}
										]}
									/>
								</View>
							</ScrollView>
						) : this.state.selectedIndexSub[i] === 3 ? (
							<ScrollView>
								<View style={{alignItems: 'center'}}>
									<Text style={{marginTop: 10}}>
										Esta estilo de respuesta les llegará a los destinatarios y será respondido con
										un máximo
										de 120 caracteres
									</Text>
									<Text
										style={{
											fontStyle: 'italic',
											textAlign: 'center',
											marginTop: 10
										}}>
										ejemplo:
									</Text>
									<TextInput
										keyboardType='default'
										maxLength={120}
										multiline={true}
										onChangeText={(text) => this.isSubCarac(text, i)}
										placeholder={'Responda de manera breve'}
										returnKeyType='next'
										value={this.state.elSubText[i]}
										underlineColorAndroid='transparent'
										style={[
											styles.inputPicker,
											styles.btn11,
											{
												borderColor: this.state.secondColor,
												height: responsiveHeight(9),
												padding: 5
											}
										]}
									/>
									<Text>{this.state.elSubText[i] === undefined ? '0' : this.state.elSubText[i].length}/120</Text>
								</View>
							</ScrollView>
						) : (
							<ScrollView style={{marginTop: 50}}>
								<Text> "No hay ningún tipo de sub opción seleccionado"</Text>
							</ScrollView>
						)}
						<TouchableOpacity
							style={[
								styles.bigButton,
								styles.btn9,
								styles.btnModal,
								{
									backgroundColor: this.state.mainColor,
									borderColor: this.state.mainColor
								}
							]}
							onPress={() => this.closeModal(i)}
						>
							<Text style={styles.textButton}>Continuar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
			</View>
		);
	}

	respuesta(i) {
		const modal = this.isModal(i);
		return (
			<View style={styles.row}>
				{modal}
				<Text style={[
					styles.btn5_l,
					{fontSize: responsiveFontSize(3), paddingBottom: 20}
				]}
				>
					{i + 1}
				</Text>
				<View>
					<TextInput
						keyboardType='default'
						maxLength={254}
						multiline={true}
						onChangeText={text => this.onChangeRes(text, i)}
						placeholder={'ej.: , etc...'}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						value={this.state.laResp[i]}
						style={[
							styles.inputPicker,
							styles.btn12,
							{
								borderColor: this.state.secondColor,
								height: responsiveHeight(10),
								padding: 5
							}
						]}
					/>
					<View style={[styles.btn12, styles.row, {alignItems: 'flex-end'}]}>
						<TouchableOpacity onPress={() => this.borrar(i)} style={{marginBottom: 7}}>
							<Text style={[styles.textW, {color: 'red'}]}>Borrar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.inputPicker,
								styles.btn3,
								{
									height: responsiveHeight(3.5),
									backgroundColor: this.state.secondColor
								}
							]}
							onPress={() => this.openModal(i)}
						>
							<Text style={styles.textoB}>Sub opción</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}

	respuestas() {
		let btnResp = [];
		for (let i = 0; i < this.state.losTexts.length; i++) {
			btnResp.push(this.respuesta(i))
		}
		return btnResp;
	}

	async onChangeRes(text, i) {
		this.state.laResp[i] = await  text;
		await this.setState({aux: 0});
	}

	async onChangeNum(text, i) {
		this.state.subLel[i] = await  text;
		await this.setState({aux: 0});
	}

	//++++++++++++++++++++++++++++++++++++++++++++SUB-RESPUESTA
	async agregarOtroSub() {
		this.state.losSubTexts.length !== 2 ?
			this.state.losSubTexts.push(0) : null;
		await this.setState({aux: 0})
	}

	async onChangeRes1(text, i, j) {
		this.state.laSubResp[i + '' + j] = await  text;
		await this.setState({aux: 0});
	}

	borrarSub(index) {
		this.state.losSubTexts.splice(index, 1);
		this.setState({aux: 0});
	}


	subResp(i, j) {
		return (
			<View style={[styles.row, styles.modalWidth]}>
				<Text style={[
					styles.btn5_l,
					{fontSize: responsiveFontSize(2.5)}
				]}
				>
					{j + 1}
				</Text>
				<View style={{alignItems: 'center'}}>
					<TextInput
						keyboardType='default'
						maxLength={254}
						multiline={true}
						onChangeText={text => this.onChangeRes1(text, i, j)}
						placeholder={'ej.:sub , etc...'}
						underlineColorAndroid='transparent'
						value={this.state.laSubResp[i + '' + j]}
						style={[
							styles.inputPicker,
							styles.btn11,
							{
								borderColor: this.state.secondColor,
								height: responsiveHeight(10),
								padding: 5
							}
						]}
					/>
					<View style={[styles.btn11, styles.row, {alignItems: 'flex-end'}]}>
						<TouchableOpacity onPress={() => this.borrarSub(j)} style={{marginBottom: 7}}>
							<Text style={[styles.textW, {color: 'red'}]}>Borrar</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}

	subRespuestas(i) {
		let iv = i + 1;
		let btnResp = [];
		for (let j = 0; j < this.state.losSubTexts.length; j++) {
			btnResp.push(this.subResp(iv, j))
		}
		return btnResp;
	}

//++++++++++++++++++++++++++++++++++++++++++++checksBtns
	async onListPressedPrueba(itemPrueba, indexPrueba) {
		await this.setState({
			selectedIndexPrueba: indexPrueba,
			laPrueba: itemPrueba.tipo
		});
	}

	renderBtn(itemPrueba, indexPrueba) {
		let smallButtonStyles = [
			styles.listButton,
			styles.listButtonSmall,
			styles.btn5,
			{borderColor: this.state.secondColor}
		];
		let texto = [styles.textoN];
		if (this.state.selectedIndexPrueba === indexPrueba) {
			smallButtonStyles.push(styles.listButtonSelected, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<View style={{alignItems: 'center'}}>
				<TouchableHighlight
					style={smallButtonStyles}
					underlayColor={'transparent'}
					onPress={() => this.onListPressedPrueba(itemPrueba, indexPrueba)}
				>
					<Text style={texto}>{itemPrueba.tipo}</Text>
				</TouchableHighlight>
			</View>
		);
	}

	renderBtns() {
		let elBtn = [];
		this.state.btnPrueba.forEach((itemPrueba, indexPrueba) => {
			elBtn.push(this.renderBtn(itemPrueba, indexPrueba))
		});
		return elBtn;
	}

//+++++++++++++++++++++++++++++subSeleccion
	async onListPressedSub(itemSub, indexSub, i) {
		this.state.elSub[i] = itemSub.tipo;
		this.state.selectedIndexSub[i] = indexSub;
		await this.setState({indice: i});
	}

	rendersub(itemSub, indexSub, i) {
		let smallButtonStyles = [
			styles.listButton,
			styles.listButtonSmall,
			styles.btn5,
			{borderColor: this.state.secondColor}
		];
		let texto = [styles.textoN];

		if (this.state.selectedIndexSub[i] === indexSub) {
			smallButtonStyles.push(styles.listButtonSelected, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<View style={{alignItems: 'center'}}>
				<TouchableHighlight
					style={smallButtonStyles}
					underlayColor={'transparent'}
					onPress={() => this.onListPressedSub(itemSub, indexSub, i)}
				>
					<Text style={texto}>{itemSub.tipo}</Text>
				</TouchableHighlight>
			</View>
		);
	}

	renderSubs(i) {
		let elBtnSub = [];
		this.state.btnSub.forEach((itemSub, indexSub) => {
			elBtnSub.push(this.rendersub(itemSub, indexSub, i))
		});
		return elBtnSub;
	}

	renderPersss() {
		const pruebaCheck = this.renderBtns();
		const textIput = this.respuestas();
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione el tipo de respuesta
				</Text>
				<View style={[styles.row, {height: responsiveHeight(7)}]}>{pruebaCheck}</View>
				{this.state.selectedIndexPrueba === 0 ? (
						<View style={[styles.widthall, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
							<Text
								style={{
									fontSize: responsiveFontSize(1.5),
									fontStyle: 'italic',
									textAlign: 'center',
									marginBottom: 5
								}}>
								Con esta selección el destinatario podrá elegir más de una de las opciones de
								respuesta
							</Text>
						</View>
					) :
					this.state.selectedIndexPrueba === 1 ? (
							<View style={[styles.widthall, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
								<Text
									style={{
										fontSize: responsiveFontSize(1.5),
										fontStyle: 'italic',
										textAlign: 'center',
										marginBottom: 5
									}}>
									Con esta selección el destinatario podrá elegir solo una de las opciones de
									respuesta
								</Text>
							</View>
						) :
						this.state.selectedIndexPrueba === 2 ? (
								<View style={[styles.widthall, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
									<Text
										style={{
											fontSize: responsiveFontSize(1.5),
											fontStyle: 'italic',
											textAlign: 'center',
											marginBottom: 5
										}}>
										Con esta selección el destinatario podrá capturar un número como respuesta
									</Text>
								</View>
							) :
							this.state.selectedIndexPrueba === 3 ? (
								<View
									style={[
										styles.widthall,
										{
											marginTop: 5,
											alignItems: 'center',
											borderBottomWidth: 1
										}
									]}>
									<Text
										style={{
											fontSize: responsiveFontSize(1.5),
											fontStyle: 'italic',
											textAlign: 'center',
											marginBottom: 5
										}}>
										Con esta selección el destinatario podrá capturar un texto como respuesta
									</Text>
								</View>
							) : null}
				<ScrollView>
					{this.state.selectedIndexPrueba === 0 ||
					this.state.selectedIndexPrueba === 1 ? (
						<View>
							<Text style={{marginTop: 10}}>
								Defina las respuestas que habrá en la noticia
							</Text>
							{textIput}
							{this.state.losTexts.length !== 4 ? (
								<View style={[styles.widthall, {alignItems: 'center', marginTop: 5}]}>
									<TouchableOpacity
										onPress={() => this.agregarOtro()}
									>
										<Entypo name='plus' size={40} color='#2b2b2b'/>
									</TouchableOpacity>
									<Text>Añadir otra opción de respuesta</Text>
								</View>) : null}

						</View>
					) : this.state.selectedIndexPrueba === 2 ? (
						<View style={{alignItems: 'center'}}>
							<Text style={{marginTop: 10}}>
								Defina que parámetro numérico tendrá esta respuesta
							</Text>
							<TextInput
								keyboardType='numeric'
								maxLength={2}
								onChangeText={text => (this.state.lel = text)}
								placeholder={'ej.: del 1 al 99'}
								returnKeyType='next'
								underlineColorAndroid='transparent'
								style={[
									styles.inputPicker,
									styles.btn11,
									{
										borderColor: this.state.secondColor,
										height: responsiveHeight(5),
										padding: 5
									}
								]}
							/>
						</View>
					) : this.state.selectedIndexPrueba === 3 ? (
						<View style={{alignItems: 'center'}}>
							<Text style={{marginTop: 10}}>
								Este estilo de respuesta les llegará a los destinatarios y será respondido con un máximo
								de 120 caracteres
							</Text>
							<Text
								style={{
									fontStyle: 'italic',
									textAlign: 'center',
									marginTop: 10
								}}>
								ejemplo:
							</Text>
							<TextInput
								keyboardType='default'
								maxLength={120}
								multiline={true}
								onChangeText={(text) => this.isCaracteres(text)}
								placeholder={'Responda de manera breve'}
								returnKeyType='next'
								underlineColorAndroid='transparent'
								style={[
									styles.inputPicker,
									styles.btn11,
									{
										borderColor: this.state.secondColor,
										height: responsiveHeight(9.5),
										padding: 5
									}
								]}
							/>
							<Text>{this.state.elText.length}/120</Text>
						</View>
					) : null}

				</ScrollView>
				<TouchableOpacity
					style={[
						styles.bigButton,
						{backgroundColor: this.state.secondColor}
					]}
					onPress={() => this.saveOpcion()}
				>
					<Text style={styles.textButton}>Guardar</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
