import React from 'react';
import {
    AsyncStorage,
    Image,
    ImageBackground,
    RefreshControl,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import styles from '../../../styles';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';

export default class Padre extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            menu: [],
            refreshing: false,
        };
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        this.getMenus();
    };

    async componentWillMount() {
        await this.getURL();
        await this.getMenus();
    }

    async getURL() {
        this.setState({
            auth: await AsyncStorage.getItem('token'),
            uri: await AsyncStorage.getItem('uri'),
            maincolor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor'),
            fase: await AsyncStorage.getItem('fase'),
            puesto: await AsyncStorage.getItem('puesto'),
            role: await AsyncStorage.getItem('role')
        });

    }

    getMenus() {
        let api = this.state.uri + '/api/listas/menus/' + this.state.role + '/' + this.state.puesto + '/' + this.state.fase;
        if (this.state.puesto === 'Coordinador Academico') {
            api = this.state.uri + '/api/listas/menus/' + this.state.role + '/Academico/' + this.state.fase;
        }
        if (this.state.uri !== undefined) {
            fetch(api, {
                method: 'GET', headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.auth
                }
            })
                .then(response => {
                    return response.json();
                })
                .then(recurso => {
                    this.setState({refreshing: false});
                    this.setState({menu: recurso});
                });
        }
    }

    async setVisto(tipo, i) {
        await fetch(this.state.uri + '/api/update/notif/visto/' + tipo, {
            method: 'POST', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.auth
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                if(recurso.error === undefined){
                    this.getMenus();
                    Actions.push(this.state.menu[i].mobile);
                }
                else{
                    console.log(this.state.menu[i].mobile);
                    Actions.push(this.state.menu[i].mobile);
                }
            });
    }

    renderMenus() {
        let btn = [];
        for (let i = 0; i < this.state.menu.length; i = i + 2) {
            btn.push(this.renderMenu(i));
        }
        return btn;
    }

    renderMenu(i) {
        return (<View style={styles.row_M} key={i + 'menu'}>
            <TouchableOpacity
                style={[styles.btnContainer_M, {borderRadius: 15}]}
                onPress={() => this.setVisto(this.state.menu[i].nombre, i)}>
                <ImageBackground
                    source={require('../../../../images/FONDO7.png')}
                    style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor, borderRadius: 15}]}
                    imageStyle={{borderRadius: 15}}
                >
                    <Image
                        style={styles.imagen}
                        source={{uri: this.state.menu[i].url}}/>
                    <Text style={styles.btnText}>{this.state.menu[i].nombre}</Text>
                </ImageBackground>
                {this.state.menu[i].badge > 0 ? <View style={{
                    position: 'absolute',
                    top: 15,
                    right: this.state.menu[i].badge < 10 ? 20 : 15,
                }}>
                    <View style={{
                        width: this.state.menu[i].badge < 10 ? 25 : 41,
                        height: 25,
                        backgroundColor: 'red',
                        borderRadius: 15
                    }}/>
                    <Text style={{
                        position: 'absolute',
                        left: this.state.menu[i].badge < 10 ? 6.9 : this.state.menu[i].badge === 11 ? 10 : 9,
                        top: this.state.menu[i].badge < 10 ? '5%' : '5%',
                        color: '#fff',
                        fontSize: 18,
                        fontWeight: '700'
                    }}>{this.state.menu[i].badge}</Text>
                </View> : null}
            </TouchableOpacity>

            {this.state.menu.length !== i + 1 ?
                <TouchableOpacity
                    style={[styles.btnContainer_M, {borderRadius: 15}]}
                    onPress={() => this.setVisto(this.state.menu[i + 1].nombre, i + 1)}>
                    <ImageBackground
                        source={require('../../../../images/FONDO7.png')}
                        style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor, borderRadius: 15}]}
                        imageStyle={{borderRadius: 15}}
                    >
                        <Image
                            style={styles.imagen}
                            source={{uri: this.state.menu[i + 1].url}}/>
                        <Text style={styles.btnText}>{this.state.menu[i + 1].nombre}</Text>
                    </ImageBackground>
                    {this.state.menu[i + 1].badge > 0 ? <View style={{
                        position: 'absolute',
                        top: 15,
                        right: this.state.menu[i + 1].badge < 10 ? 20 : 15,
                    }}>
                        <View style={{
                            width: this.state.menu[i + 1].badge < 10 ? 25 : 41,
                            height: 25,
                            backgroundColor: 'red',
                            borderRadius: 15
                        }}/>
                        <Text style={{
                            position: 'absolute',
                            left: this.state.menu[i + 1].badge < 10 ? 6.9 : this.state.menu[i + 1].badge === 11 ? 10 : 9,
                            top: this.state.menu[i + 1].badge < 10 ? '5%' : '5%',
                            color: '#fff',
                            fontSize: 18,
                            fontWeight: '700'
                        }}>{this.state.menu[i + 1].badge}</Text>
                    </View> : null}
                </TouchableOpacity> : null}
        </View>);
    }

    render() {
        const menu = this.renderMenus();
        return (<View>
            <MediaQuery maxDeviceHeight={896}>
                <View>
                    <ScrollView
                        style={styles.menu}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                        overScrollMode='always'
                        showsVerticalScrollIndicator={false}>
                        {menu}
                    </ScrollView>
                </View>
            </MediaQuery>
        </View>);
    }
}
