import React from 'react';
import {
    Alert,
    AsyncStorage,
    Image,
    ImageBackground,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';
import styles from '../../../styles';

export default class menuDePagosDir extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [], dato: [], notification: {}
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getRole();
    }

    async getURL() {
        let auth = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            url: uri,
            Auth: auth,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    itsAdmin() {
        const comingSoon = () => {
            Alert.alert('¡Bloqueado!', 'Esta funcionalidad aun no esta disponible para su escuela', [{text: 'Entendido'}]);
        };
        return (<View>
            {/*para iphone  */}
            <MediaQuery maxDeviceHeight={896}>
                <View>
                    <ScrollView
                        style={styles.menu}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.row_M}>
                            <TouchableOpacity
                                style={[styles.btnContainer_M, {borderRadius: 15}]}
                                onPress={() => Actions.crearCargo()}>
                                <ImageBackground
                                    source={require('../../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {
                                        backgroundColor: this.state.fourthColor,
                                        borderRadius: 15
                                    }]}
                                    imageStyle={{borderRadius: 15}}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://png.icons8.com/cotton/40/000000/mobile-payment.png'}}/>
                                    <Text style={styles.btnText}>Crear un nuevo cargo</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.btnContainer_M, {borderRadius: 15}]}
                                onPress={() => Actions.historialPagosDir()}>
                                <ImageBackground
                                    source={require('../../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {
                                        backgroundColor: this.state.fourthColor,
                                        borderRadius: 15
                                    }]}
                                    imageStyle={{borderRadius: 15}}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://png.icons8.com/dusk/75/000000/payment-history.png'}}/>
                                    <Text style={styles.btnText}>Historial de pagos</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.row_M}>
                            <TouchableOpacity
                                style={[styles.btnContainer_M, {borderRadius: 15}]}
                                onPress={() => Actions.pagosBecasDir()}>
                                <ImageBackground
                                    source={require('../../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {
                                        backgroundColor: this.state.fourthColor,
                                        borderRadius: 15
                                    }]}
                                    imageStyle={{borderRadius: 15}}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://png.icons8.com/cotton/75/000000/receive-cash.png'}}/>
                                    <Text style={styles.btnText}>Asignación de becas</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.btnContainer_M, {borderRadius: 15}]}
                                onPress={() => Actions.consultarSaldosDir()}>
                                <ImageBackground
                                    source={require('../../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {
                                        backgroundColor: this.state.fourthColor,
                                        borderRadius: 15
                                    }]}
                                    imageStyle={{borderRadius: 15}}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://png.icons8.com/dusk/75/000000/accounting.png'}}/>
                                    <Text style={styles.btnText}>Consultar saldos</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.row_M}>
                            <TouchableOpacity
                                style={[styles.btnContainer_M, {borderRadius: 15}]}
                                onPress={() => Actions.pagosConfiguracionDir()}>
                                <ImageBackground
                                    source={require('../../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {
                                        backgroundColor: this.state.fourthColor,
                                        borderRadius: 15
                                    }]}
                                    imageStyle={{borderRadius: 15}}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://png.icons8.com/dusk/75/000000/services.png'}}/>
                                    <Text style={styles.btnText}>Configuración de cobros</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.btnContainer_M, {borderRadius: 15}]}
                                onPress={() => Actions.estadoCuenta()}>
                                <ImageBackground
                                    source={require('../../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {
                                        backgroundColor: this.state.fourthColor,
                                        borderRadius: 15
                                    }]}
                                    imageStyle={{borderRadius: 15}}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://png.icons8.com/dusk/75/000000/purchase-order.png'}}/>
                                    <Text style={styles.btnText}>Estado de cuenta</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </MediaQuery>
        </View>);
    }

   render() {
	  return (<ImageBackground
          style={styles.container}
          imageStyle={{ resizeMode: 'stretch' }}
          source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
      >
		   <StatusBar
			 backgroundColor={this.state.mainColor}
			 barStyle='light-content'
		   />
		   {this.state.admin === 'Admin' ? this.itsAdmin() : null}
		</ImageBackground>);
   }
}
