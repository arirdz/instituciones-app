import React from 'react';
import {AsyncStorage, BackHandler, ImageBackground, Linking, StatusBar} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../../../styles';
import Padre from './padre';
import OneSignal from 'react-native-onesignal';
import crossroads from 'crossroads';

export default class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            dato: [],
            puesto: '',
            Auth: '',
            notification: {},
            id: {}
        };
    }

    static async userLogout() {
        try {
            await AsyncStorage.clear();
            Actions.Bienvenida({type: 'reset'});

        } catch (error) {
            console.warn('Ocurrió un error ' + error);
        }
    }

    _handleNotification = notification => {
        this.setState({notification: notification});
    };

    componentDidMount() {
        Linking.getInitialURL()
            .then(url => this.handleOpenURL({url}))
            .catch(console.error);
        Linking.addEventListener('url', this.handleOpenURL);
        BackHandler.addEventListener('hardwareBackPress', () => {
            try {
                Actions.pop();
                return true;
            } catch (err) {
                console.debug('Can\'t pop. Exiting the app...');
                return false;
            }
        });
    }

    handleOpenURL(event) {
        if (event.url && event.url.indexOf('ce://') === 0) {
        }
    }

    async getUserData() {
        let id = await AsyncStorage.getItem('id');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        await fetch(uri + '/api/user/data', {
            method: 'GET', headers: {Authorization: 'Bearer ' + token}
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    fetch(this.state.uri + '/api/login/id/' + id, {
                        method: 'POST'
                    }).then(res => res.json())
                        .then(responseJson => {
                            AsyncStorage.setItem('token', responseJson.token);
                            this.getUserData();
                        });
                } else {
                    this.setState({datos: responseJson});
                    AsyncStorage.setItem('email', responseJson.email_tutor)
                }
            });
        let role = await AsyncStorage.getItem('role');
        let puesto = await AsyncStorage.getItem('puesto');
        if (this.state.datos !== undefined) {
            await fetch(await AsyncStorage.getItem('uri') + '/api/payment/dos/comprobar/cliente', {
                method: 'GET', headers: {Authorization: 'Bearer ' + await AsyncStorage.getItem('token')}
            }).then(res => res.json())
                .then(responseJson => {
                    this.setState({cliente: responseJson});
                    if (this.state.datos.passwordUpdated === '0') {
                        Actions.replace('PwdRest')
                    } else if (this.state.cliente === 0) {
                        if (role === 'Padre') {
                            Actions.replace('rugt')
                        }
                        if (puesto === 'Director') {
                            Actions.replace('rugt2')
                        }
                    }
                });
        }
    }

    async componentWillMount() {
        await this.getURL();
        await this.getUserData();
        OneSignal.init('0361dfc7-e393-468e-879d-7dfde0531c2a');
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
        Linking.addEventListener('url', this.handleOpenURL);
        OneSignal.inFocusDisplaying(2);
        OneSignal.configure();

    }

    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleOpenURL);
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('ids', this.onIds);
        BackHandler.removeEventListener('hardwareBackPress');
    }

    onReceived(notification) {
        console.log('Notification received: ', notification);
    }

    async onIds(device) {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        try {
            let formData = new FormData();
            formData.append(
                'PNToken',
                JSON.stringify({
                    PushNotificationsID: device.userId
                })
            );
            let request = await fetch(uri + '/api/user/register/device', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: 'Bearer ' + token
                },
                body: formData
            });
        } catch (e) {
            Alert.alert('Error', 'Intenta más tarde por favor', [
                {
                    text: 'Entendido'
                }
            ]);
        }
    }

    async onOpened(openResult) {
        let senderId = openResult.notification.payload.additionalData.id;
        if (openResult.notification.payload.launchURL === 'ce://Messages') {
            let uri = await AsyncStorage.getItem('uri');
            let token = await AsyncStorage.getItem('token');
            fetch(uri + '/api/data/user/' + senderId, {
                method: 'GET', headers: {
                    Authorization: 'Bearer ' + token
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                    } else {
                        Actions.push('Messages', {title: responseJson[0].name, data: responseJson[0]})
                    }
                });
        } else if (openResult.notification.payload.launchURL === 'ce://News') {
            let uri = await AsyncStorage.getItem('uri');
            let token = await AsyncStorage.getItem('token');
            fetch(uri + '/api/get/post/by/notification/' + senderId, {
                method: 'GET', headers: {
                    Authorization: 'Bearer ' +
                        token
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                    } else {
                        Actions.newsSingle({data: responseJson});
                    }
                });
        }
        if (senderId === 'notifAspirante') {
            Actions.newAspirantes();
        } else if (senderId === 'reinscripcionAlumn') {
            let r = openResult.notification.payload.additionalData.reinscrip;
            Actions.aspirantePendiente({reinscrp: r});
        } else if (openResult.notification.payload.launchURL === 'ce://listaFactraPaps') {
            Actions.listaFactraPaps();
        } else if (openResult.notification.payload.launchURL === 'ce://facturasGeneradasDir') {
            Actions.facturasGeneradasDir();
        } else if (openResult.notification.payload.launchURL === '://calendario_activ') {
            let role = await AsyncStorage.getItem('role');
            if (role === 'Admin') {
                Actions.planAnualTrab();
            } else {
                Actions.calendarEvent();
            }
        } else if (openResult.notification.payload.launchURL === '://Encuadres/Admin') {
            Actions.Encuadres();
        } else if (openResult.notification.payload.launchURL === '://Encuadres/Maestro') {
            Actions.encuadre();
        }else if (openResult.notification.payload.launchURL === '://Encuadres/Padre') {
            Actions.encuadrePadre();
        }
    }

    /* End push notifications */

    async getURL() {
        let auth = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        let role = await AsyncStorage.getItem('role');
        let puesto = await AsyncStorage.getItem('puesto');
        this.setState({
            uri: uri,
            Auth: auth,
            role: role,
            puesto: puesto
        });
    }

    mensajes(id) {
        Actions.Messages({user_id: id});
    }

    render() {
        // URL schemes
        crossroads.addRoute('mensajes/{id}', id => this.mensajes(id));
        return (
            <ImageBackground
                style={styles.container}
                imageStyle={{resizeMode: 'stretch'}}
                source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
            >
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                {this.state.data.passwordUpdated === 0 ? Actions.PwdRest() : null}
                <Padre/>
            </ImageBackground>
        );
    }
}
