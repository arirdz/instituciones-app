import React from 'react';
import {AsyncStorage, Image, ImageBackground, ScrollView, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';
import styles from '../../../styles';

export default class MenuPagos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            dato: [],
            notification: {}
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getRole();
    }

    async getURL() {
        let auth = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            url: uri,
            Auth: auth,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    isPapa() {
        return (
            <View>
                {/* para iphone */}
                <MediaQuery maxDeviceHeight={896}>
                    <View>
                        <ScrollView
                            style={styles.menu}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.row_M}>
                                <TouchableOpacity
                                    style={[styles.btnContainer_M, {borderRadius: 15}]}
                                    onPress={() => Actions.GestionarPagos()}>
                                    <ImageBackground
                                        source={require('../../../../images/FONDO7.png')}
                                        style={[styles.btnContainer_M, {
                                            backgroundColor: this.state.fourthColor,
                                            borderRadius: 15
                                        }]}
                                        imageStyle={{borderRadius: 15}}
                                    >
                                        <Image
                                            style={styles.imagen}
                                            source={{uri: 'https://img.icons8.com/dusk/75/000000/payment-history.png'}}/>
                                        <Text style={styles.btnText}>Gestionar pagos</Text>
                                    </ImageBackground>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.btnContainer_M, {borderRadius: 15}]}
                                    onPress={() => Actions.gestionarTarjetas()}>
                                    <ImageBackground
                                        source={require('../../../../images/FONDO7.png')}
                                        style={[styles.btnContainer_M, {
                                            backgroundColor: this.state.fourthColor,
                                            borderRadius: 15
                                        }]}
                                        imageStyle={{borderRadius: 15}}
                                    >
                                        <Image
                                            style={styles.imagen}
                                            source={{uri: 'https://img.icons8.com/dusk/75/000000/bank-cards.png'}}/>
                                        <Text style={styles.btnText}>Gestionar tarjetas</Text>
                                    </ImageBackground>
                                </TouchableOpacity>
                            </View>

                            {/*<View style={styles.row_M}>*/}
                            {/*<TouchableOpacity*/}
                            {/*style={[*/}
                            {/*styles.btnContainer_M,*/}
                            {/*{*/}
                            {/*backgroundColor: this.state.fourthColor*/}
                            {/*}*/}
                            {/*]}*/}
                            {/*onPress={() => Actions.pagosConfiguracionPaps()}>*/}
                            {/*<Image*/}
                            {/*style={styles.imagen}*/}
                            {/*source={require('../../../images/icons8/icons8-pos_terminal.png')}*/}
                            {/*/>*/}
                            {/*<Text style={styles.btnText}>Configurar pagos</Text>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                            {/*<View style={styles.row_M}>*/}
                            {/*<TouchableOpacity*/}
                            {/*style={[*/}
                            {/*styles.btnContainer_M,*/}
                            {/*{*/}
                            {/*backgroundColor: this.state.fourthColor,*/}
                            {/*width:responsiveWidth(94),*/}
                            {/*height:responsiveHeight(20)*/}
                            {/*}*/}
                            {/*]}*/}
                            {/*onPress={() => Actions.GestionarPagos()}>*/}
                            {/*<Image*/}
                            {/*style={{*/}
                            {/*height: 80, resizeMode: 'contain', width: 80*/}
                            {/*}}*/}
                            {/*source={require('../images/icons8/icons8-payment_history.png')}*/}
                            {/*/>*/}
                            {/*<Text style={styles.btnText}>Gestionar pagos</Text>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                            {/*<View style={styles.row_M}>*/}
                            {/*<TouchableOpacity*/}
                            {/*style={[*/}
                            {/*styles.btnContainer_M,*/}
                            {/*{*/}
                            {/*backgroundColor: this.state.fourthColor,*/}
                            {/*width:responsiveWidth(94),*/}
                            {/*height:responsiveHeight(20)*/}
                            {/*}*/}
                            {/*]}*/}
                            {/*onPress={() => Actions.gestionarTarjetas()}>*/}
                            {/*<Image*/}
                            {/*style={{*/}
                            {/*height: 80, resizeMode: 'contain', width: 80*/}
                            {/*}}*/}
                            {/*source={require('../images/icons8/icons8-bank_cards.png')}*/}
                            {/*/>*/}
                            {/*<Text style={styles.btnText}>Gestionar tarjetas</Text>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                            {/*<View style={styles.row_M}>*/}
                            {/*<TouchableOpacity*/}
                            {/*style={[*/}
                            {/*styles.btnContainer_M,*/}
                            {/*{*/}
                            {/*backgroundColor: this.state.fourthColor,*/}
                            {/*width:responsiveWidth(94),*/}
                            {/*height:responsiveHeight(20)*/}
                            {/*}*/}
                            {/*]}*/}
                            {/*onPress={() => Actions.pagosConfiguracionPaps()}>*/}
                            {/*<Image*/}
                            {/*style={{*/}
                            {/*height: 80, resizeMode: 'contain', width: 80*/}
                            {/*}}*/}
                            {/*source={require('../images/icons8/icons8-pos_terminal.png')}*/}
                            {/*/>*/}
                            {/*<Text style={styles.btnText}>Configuración de pagos</Text>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                            {/*<View style={styles.row_M}>*/}
                            {/*<TouchableOpacity*/}
                            {/*style={[*/}
                            {/*styles.btnContainer_M,*/}
                            {/*{*/}
                            {/*backgroundColor: this.state.fourthColor,*/}
                            {/*width:responsiveWidth(94),*/}
                            {/*height:responsiveHeight(20)*/}
                            {/*}*/}
                            {/*]}*/}
                            {/*onPress={() => Actions.listaFactraPaps()}>*/}
                            {/*<Image*/}
                            {/*style={{*/}
                            {/*height: 80, resizeMode: 'contain', width: 80*/}
                            {/*}}*/}
                            {/*source={require('../images/icons8/icons8-accounting.png')}*/}
                            {/*/>*/}
                            {/*<Text style={styles.btnText}>Ver todas las facturas </Text>*/}
                            {/*</TouchableOpacity>*/}
                            {/*</View>*/}

                        </ScrollView>
                    </View>
                </MediaQuery>

            </View>
        );
    }

    // itsAdmin() {
    //     const comingSoon = () => {
    //         Alert.alert(
    //             '¡Bloqueado!',
    //             'Esta funcionalidad aun no esta disponible para su escuela',
    //             [
    //                 {
    //                     text: 'Entendido'
    //                 }
    //             ]
    //         );
    //     };
    //     return (
    //         <View>
    //             {/*para iphone  */}
    //             <MediaQuery maxDeviceHeight={896}>
    //                 <View>
    //                     <ScrollView
    //                         style={styles.menu}
    //                         showsVerticalScrollIndicator={false}>
    //                         <View style={styles.row_M}>
    //
    //                             <TouchableOpacity
    //                                 style={[
    //                                     styles.btnContainer_M,
    //                                     {
    //                                         backgroundColor: this.state.fourthColor
    //                                     }
    //                                 ]}
    //                                 onPress={() => Actions.historialPagos()}>
    //                                 <Image
    //                                     style={styles.imagen}
    //                                     source={require('../../../images/icons8/icons8-payment_history.png')}
    //                                 />
    //                                 <Text style={styles.btnText}>Historial de pagos</Text>
    //                             </TouchableOpacity>
    // 						   <TouchableOpacity
    // 							 style={[
    // 								styles.btnContainer_M,
    // 								{
    // 								   backgroundColor: this.state.fourthColor
    // 								}
    // 							 ]}
    // 							 onPress={() => Actions.crearCargo()}>
    // 							  <Image
    // 								style={styles.imagen}
    // 								source={require('../../../images/icons8/icons8-bank_cards.png')}
    // 							  />
    // 							  <Text style={styles.btnText}>Crear un nuevo cargo</Text>
    // 						   </TouchableOpacity>
    //                         </View>
    //                         <View style={styles.row_M}>
    //                             <TouchableOpacity
    //                                 style={[
    //                                     styles.btnContainer_M,
    //                                     {
    //                                         backgroundColor: this.state.fourthColor
    //                                     }
    //                                 ]}
    //                                 onPress={() => Actions.consultaSaldos()}>
    //                                 <Image
    //                                     style={styles.imagen}
    //                                     source={require('../../../images/icons8/icons8-accounting.png')}
    //                                 />
    //                                 <Text style={styles.btnText}>Consultar saldos</Text>
    //                             </TouchableOpacity>
    //
    //                             <TouchableOpacity
    //                                 style={[
    //                                     styles.btnContainer_M,
    //                                     {
    //                                         backgroundColor: this.state.fourthColor
    //                                     }
    //                                 ]}
    //                                 onPress={() => Actions.facturasGeneradas()}>
    //                                 <Image
    //                                     style={styles.imagen}
    //                                     source={require('../../../images/icons8/icons8-check.png')}
    //                                 />
    //                                 <Text style={styles.btnText}>Ver facturas generadas </Text>
    //                             </TouchableOpacity>
    //                         </View>
    //
    //                         <View style={styles.row_M}>
    //                             <TouchableOpacity
    //                                 style={[
    //                                     styles.btnContainer_M,
    //                                     {
    //                                         backgroundColor: this.state.fourthColor
    //                                     }
    //                                 ]}
    //                                 onPress={() => Actions.pagosConfiguracion()}>
    //                                 <Image
    //                                     style={styles.imagen}
    //                                     source={require('../../../images/icons8/icons8-pos_terminal.png')}
    //                                 />
    //                                 <Text style={styles.btnText}>Configuración de cobros</Text>
    //                             </TouchableOpacity>
    //                             <TouchableOpacity
    //                                 style={[
    //                                     styles.btnContainer_M,
    //                                     {
    //                                         backgroundColor: this.state.fourthColor
    //                                     }
    //                                 ]}
    //                                 onPress={() => Actions.pagosVencidos()}>
    //                                 <Image
    //                                     style={styles.imagen}
    //                                     source={require('../../../images/icons8/icons8-clear_shopping_cart.png')}
    //                                 />
    //                                 <Text style={styles.btnText}>Pagos vencidos </Text>
    //                             </TouchableOpacity>
    //                         </View>
    //                         <View style={styles.row_M}>
    //                             <TouchableOpacity
    //                                 style={styles.btnContainerDisabled}
    //                                 onPress={comingSoon}>
    //                                 <Image
    //                                     style={styles.imagen}
    //                                     source={require('../../../images/icons8/icons8-wallet.png')}
    //                                 />
    //                                 <Text style={styles.btnText}>Asignación de becas</Text>
    //                             </TouchableOpacity>
    //                         </View>
    //                     </ScrollView>
    //                 </View>
    //             </MediaQuery>
    //         </View>
    //     );
    // }

    render() {
        return (
            <ImageBackground
                style={styles.container}
                imageStyle={{ resizeMode: 'stretch' }}
                source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
            >
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {this.state.admin === 'Padre' ? this.isPapa() : null}
                {this.state.admin === 'Admin' ? this.itsAdmin() : null}
                {/* {this.state.admin === "Maestro" ? this.itsMaestro() : null} */}
            </ImageBackground>
        );
    }
}
