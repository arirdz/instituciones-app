import React from 'react';
import {
	Alert, AsyncStorage, CameraRoll, Image, Modal, PermissionsAndroid, Platform, RefreshControl, ScrollView, Text,
	TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import moment from 'moment';
import styles from '../../styles'
import Spinner from 'react-native-loading-spinner-overlay';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Feather from 'react-native-vector-icons/Feather';

export default class CGaleria extends React.Component {
	_handleButtonPress = () => {
		CameraRoll.getPhotos({
			first: 1,
			assetType: 'All'
		})
			.then(r => {
				this.setState({photos: r.edges});
			})
			.catch((err) => {
				//Error Loading Images
			});
	};
	_onRefresh = () => {
		this.getAlbumes();
		this.setState({refreshing: true});
	};

	constructor(props) {
		super(props);
		this.state = {
			image: null,
			images: null,
			refreshing: false,
			modalVisible: false,
			modalVisible1: false,
			albumes: [],
			row: false,
			agregar: true,
			imagenes: [],
			imageURLs: [],
			elalbum: '',
			visible: false,
			pictures: [],
			open: false,
			indx: null,
			photos: [],
			indexSeleccionados: [],
			seleccionados: [],
			spinner: false
		}
	}

	async componentWillMount() {
		await this.getURL();
		await this.getAlbumes();
	}

	async componentDidMount() {
		await this.requestPhotosPermission()
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			text: ''
		});
	}

	async getAlbumes() {
		await fetch(this.state.uri + '/api/get/albumes',
			{
				method: 'get',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			})
			.then(response => {
				return response.json();
			}).then(responseJson => {
				if (responseJson.error === null) {
					Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Noticias)',[
						{text:'Enterado',onPress:()=>this.setState({refreshing: false})}
					]);
				} else {
					this.setState({albumes: []});
					this.setState({refreshing: false});
					this.setState({albumes: responseJson});
					if (this.state.albumes.length === 0) {
						Alert.alert('Aún no existen álbumes en la galería');
					}
				}
			});
	};

	setModalVisible(visible) {
		this.setState({modalVisible: visible});
		this._handleButtonPress();
		this.setState({seleccionados: []})
	}

	setModalVisible1(visible) {
		this.setState({modalVisible1: visible});
		this._handleButtonPress();
	}

	card() {
		let carta = [];
		this.state.albumes.forEach((item, i) => {
			carta.push(
				<View
					key={i + 'carta'}
					style={[
						styles.widthall,
						// styles.row,
						{
							marginVertical: 5,
							borderWidth: 0.5,
							borderRadius: 6,
							borderColor: 'lightgray',
							backgroundColor: this.state.fourthColor,
							paddingHorizontal: 6,
							paddingTop: 10
						}
					]}>
					<View style={[styles.row, styles.widthall, {justifyContent: 'flex-start', padding: 2}]}>
						<View style={[styles.btn3_5]}>
							<Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Nombre del álbum
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
								marginTop: 3
							}}>{item.nombre}</Text>
						</View>
						<View style={[styles.btn5]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Fecha
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>{moment(item.created_at).format('L')}</Text>
						</View>
						<View style={[styles.btn5, {alignItems: 'center'}]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Imágenes
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>{item.imagen_count}</Text>
						</View>
					</View>
					<View
						style={{
							borderBottomColor: this.state.thirdColor,
							borderBottomWidth: .5,
							paddingTop: 10
						}}
					/>
					<View style={[styles.row, styles.widthall, {
						textAlign: 'center',
						justifyContent: 'center',
						alignItems: 'center',
						margin: 5
					}]}>
						<View style={[{alignItems: 'center', margin: 5}]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Ver álbum
							</Text>
							<Feather name='eye' size={20} color='#000'
									 style={{paddingTop: 3}}
									 onPress={() => this.setState([{
										 row: false,
										 agregar: true
									 }, this.getPictures(item.id)])}/>
						</View>
					</View>
				</View>
			)
		});
		return carta;
	}

	getPictures(id) {
		this.setState({elalbum: id});
		fetch(this.state.uri + '/api/get/pictures/' + id,
			{
				method: 'get',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			})
			.then(response => {
				return response.json();

			}).then(responseJson => {
			if (responseJson.error === null) {
				Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Galeria)');
			} else {
				this.setState({pictures: []});
				this.setState({pictures: responseJson});
				if (this.state.pictures.length > 0) {
					this.state.imageURLs = this.state.pictures.map(
						(img: Object, index: number) => ({
							URI: img.url,
							thumbnail: img.url,
							id: String(index)

						})
					);
					this.setModalVisible(true);

				} else {
					null
				}
			}
		});
	};

	async open(i) {
		await this.setState({open: true, indx: i});

	}

	async selectImageOpCrack(it, ix) {
		if (this.state.seleccionados.length <= 25) {
			let i = this.state.seleccionados.indexOf(it);
			if (i > -1) {
				this.state.seleccionados.splice(i, 1);
				this.state.indexSeleccionados[ix] = 0;
			} else {
				this.state.seleccionados.push(it);
				this.state.indexSeleccionados[ix] = 1;
			}
		}
		await this.setState({aux: 0});
	}

	anterior() {
		if (this.state.indx > 0) {
			this.setState({indx: this.state.indx - 1})
		}
	}

	siguiente() {
		if (this.state.indx < this.state.pictures.length - 1) {
			this.setState({indx: this.state.indx + 1})
		}
	}

	async requestPhotosPermission() {

		try {
			const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				this._handleButtonPress();
			} else {
				console.log('Photos permission denied')
			}
		} catch (err) {
			console.warn(err)
		}
		try {
			const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				this._handleButtonPress();
			} else {
				console.log('Photos permission denied')
			}
		} catch (err) {
			console.warn(err)
		}
	}

	async descargar() {
		await this._handleButtonPress();
		await CameraRoll.saveToCameraRoll(this.state.pictures[this.state.indx].url);
		await Alert.alert('Imagen', 'Guardada', [{
			text: 'Entendido'
		}]);
	}

	rndGaleria() {
		let imagen = [];
		for (let i = 0; i < this.state.pictures.length; i = i + 2) {

			imagen.push(
				<View style={[styles.row, {backgroundColor: 'black'}]} key={i + 'foto'}>
					<TouchableOpacity onPress={() =>
						this.open(i)
					}>
						<Image
							style={{width: responsiveWidth(50), height: responsiveHeight(30), resizeMode: 'cover'}}
							defaultSource={require('../../../images/giphy.gif')}
							source={{
								uri: this.state.pictures[i].url
							}}
						/></TouchableOpacity>
					{this.state.pictures.length !== i + 1 ?
						<TouchableOpacity onPress={() =>
							this.open(i + 1)
						}><Image
							style={{width: responsiveWidth(50), height: responsiveHeight(30), resizeMode: 'cover'}}
							defaultSource={require('../../../images/giphy.gif')}
							source={{
								uri: this.state.pictures[i + 1].url
							}}
						/></TouchableOpacity>
						: null}
					<Modal
						style={{backgroundColor: 'lightgrey'}}
						animationType='slide'
						transparent={false}
						visible={this.state.open}>
						{Platform.OS === 'ios' ? (
							<View
								style={{backgroundColor: this.state.secondColor, ...ifIphoneX({height: responsiveHeight(5)}, {height: responsiveHeight(3)})}}/>
						) : null}
						{this.state.indx !== null ?
							<View style={[styles.container, {backgroundColor: '#fff'}]}>
								<View style={[styles.widthall, {
									alignItems: 'flex-end',
									marginTop: responsiveHeight(1.5)
								}]}>
									{/*<TouchableOpacity*/}
									{/*style={[*/}
									{/*styles.modalBigBtn,*/}
									{/*{backgroundColor: '#fff', borderColor: this.state.secondColor}*/}
									{/*]}*/}
									{/*onPress={() => this.setState({open: false})}*/}
									{/*>*/}
									{/*<Text style={[styles.textW, {*/}
									{/*color: this.state.secondColor,*/}
									{/*fontSize: responsiveFontSize(1.5)*/}
									{/*}]}>*/}
									{/*Cerrar*/}
									{/*</Text>*/}
									{/*</TouchableOpacity>*/}
									<Feather
										name='x-circle' size={40}
										onPress={() => this.setState({open: false})}
										color={this.state.secondColor}/>
								</View>

								<Image
									style={{
										width: responsiveWidth(100),
										height: responsiveHeight(75),
										resizeMode: 'contain',
										backgroundColor: '#fff'
									}}
									defaultSource={require('../../../images/giphy.gif')}
									source={{
										uri: this.state.pictures[this.state.indx].url
									}}
								/>
								<View style={[styles.row, styles.modalWidth, {
									width: responsiveWidth(80),
									backgroundColor: '#fff',
									marginVertical: 10,
									alignItems: 'center',
									justifyContent: 'space-between'
								}]}>
									<Feather
										name='arrow-left-circle' size={40}
										onPress={() => this.anterior()}
										color={this.state.secondColor}
									/>
									{Platform.OS === 'ios' ?
										<Feather
											name='download' size={40}
											onPress={() => this.descargar()}
											color={this.state.secondColor}
										/> : null}
									<Feather
										name='arrow-right-circle' size={40}
										onPress={() => this.siguiente()}
										color={this.state.secondColor}
									/>
								</View>
							</View> : null}
					</Modal>
				</View>
			);
		}
		return imagen;
	}

	galeriaPicker() {
		let img = [];
		for (let i = 0; i < this.state.photos.length; i = i + 2) {
			let select = null;
			let selected = null;
			let select2 = null;
			let selected2 = null;
			if (this.state.indexSeleccionados[i] === 1) {
				select = [{backgroundColor: '#113fff'}];
				selected = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'stretch',
					opacity: .6
				}];
			} else {
				select = [{padding: 0, backgroundColor: '#fff'}];
				selected = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'stretch'
				}];
			}
			if (this.state.indexSeleccionados[i + 1] === 1) {
				select2 = [{backgroundColor: '#113fff'}];
				selected2 = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'stretch',
					opacity: .6
				}];
			} else {
				select2 = [{padding: 0, backgroundColor: '#fff'}];
				selected2 = [{
					width: responsiveWidth(47),
					height: responsiveHeight(28),
					resizeMode: 'stretch'
				}];
			}
			img.push(
				<View
					key={i + 'photopicker'} style={styles.row}>
					<TouchableHighlight
						style={select}
						onPress={() => this.selectImageOpCrack(this.state.photos[i].node.image.uri, i)}>
						<Image
							style={selected}
							source={{uri: this.state.photos[i].node.image.uri}}
						/>
					</TouchableHighlight>
					{this.state.photos.length !== i + 1 ?
						<TouchableHighlight
							style={select2}
							onPress={() => this.selectImageOpCrack(this.state.photos[i + 1].node.image.uri, i + 1)}>
							<Image
								style={selected2}
								source={{uri: this.state.photos[i + 1].node.image.uri}}
							/>
						</TouchableHighlight> : null}
				</View>
			);
		}
		return img;
	}

	render() {
		return (
			<View style={styles.container}>
				<ScrollView style={{marginVertical: 10}}
							refreshControl={
								<RefreshControl
									refreshing={this.state.refreshing}
									onRefresh={this._onRefresh}
								/>
							}
							overScrollMode='always'
							showsVerticalScrollIndicator={false}>
					{this.card()}
					<Modal
						animationType='slide'
						transparent={false}
						visible={this.state.modalVisible1}
					>
						<View style={{backgroundColor: 'black'}}>
							<TouchableHighlight style={{marginVertical: 20}}>
								<Text onPress={() => this.setModalVisible1(false)}
									  style={{
										  color: 'white',
										  fontSize: 25,
										  textAlign: 'right',
										  margin: 20,
										  marginBottom: 0
									  }}
								>X</Text>
							</TouchableHighlight>
						</View>
						<View style={styles.container}>
							<Text style={styles.main_title}>Agregar fotos al album</Text>
							<TextInput
								editable={true}
								placeholder={'Escriba el título del album'}
								value={this.state.text}
								maxLength={40}
								onChangeText={(text) => this.setState({text})}
								style={[
									styles.main_title, styles.btn1,
									{
										marginTop: 10,
										paddingTop: 0,
										textAlign: 'center',
										color: this.state.thirdColor,
										fontSize: responsiveFontSize(3)
									}
								]}
							/>
							<View style={{marginTop: responsiveHeight(3)}}>
							</View>
							<ScrollView>
								{this.galeriaPicker()}
							</ScrollView>
							{this.state.seleccionados.length === 0 ?
								(<Text style={{marginTop: 10}}>{'No han seleccionado imágenes'}</Text>) :
								(<Text
									style={{marginTop: 10}}>{'Se han seleccionado ' + this.state.seleccionados.length + ' imágenes'}</Text>)}
							<TouchableOpacity onPress={() => this.saveImages()} style={[styles.bigBtn, {
								backgroundColor: this.state.secondColor,
								borderColor: this.state.secondColor, marginVertical: 10
							}]}>
								<Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
							</TouchableOpacity>
						</View>
					</Modal>
					<Modal
						animationType='slide'
						transparent={false}
						visible={this.state.modalVisible}>
						{Platform.OS === 'ios' ? (
							<View
								style={{backgroundColor: this.state.secondColor, ...ifIphoneX({height: responsiveHeight(5)}, {height: responsiveHeight(3)})}}/>
						) : null}
						<View style={[styles.widthall, {
							backgroundColor: '#fff',
							alignItems: 'flex-end',
							marginVertical: responsiveHeight(1.5)
						}]}>
							<Feather
								name='x-circle' size={40}
								onPress={() => this.setModalVisible(false)}
								color={this.state.secondColor}/>
						</View>
						<View style={[styles.container, {backgroundColor: '#fff', alignItems: 'flex-start'}]}>
							<Spinner visible={this.state.spinner} textContent='Cargando imágenes...'/>
							<ScrollView>
								{this.rndGaleria()}
							</ScrollView>
						</View>
					</Modal>
				</ScrollView>
			</View>
		);
	}

}
