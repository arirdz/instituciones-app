import React from 'react';
import {
   Alert, AsyncStorage, Image, ScrollView, Text, TextInput, TouchableHighlight, TouchableOpacity, View, Linking
} from 'react-native';
import HTMLView from 'react-native-htmlview';
import {Actions} from 'react-native-router-flux';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Switch from 'react-native-switch-pro';
import styles from '../../styles';

export default class newsSingle extends React.Component {
   constructor(props) {
	  super(props);
	  this.state = {
		 data: [],
		 selectedIndexRespuestas: [],
		 selectedIndexSubRespuestas: [],
		 respuesta: [],
		 subRespuesta: [],
		 checkBoxBtns: [],
		 checkBoxBtnsIndex: [0, 0],
		 checkBoxBtns1: [],
		 checkBoxBtnsIndex1: [0, 0],
		 checkBoxBtns2: [],
		 checkBoxBtnsIndex2: [0, 0],
		 checkBoxBtns3: [],
		 checkBoxBtnsIndex3: [0, 0],
		 diaAten: [false, false, false, false],
		 hideHora: [false, false, false, false],
		 tipo: ['texto', 'texto', 'texto', 'texto'],
		 text: [],
		 resp: [],
		 role: '',
		 contestador: ''
	  };
   }

   async componentWillMount() {
	  await this.getURL();
	  await this.getUserData();
	  await this.getEnterado();
   }

   async getURL() {
	  await this.setState({
		 uri: await AsyncStorage.getItem('uri'),
		 token: await AsyncStorage.getItem('token'),
		 mainColor: await AsyncStorage.getItem('mainColor'),
		 secondColor: await AsyncStorage.getItem('secondColor'),
		 thirdColor: await AsyncStorage.getItem('thirdColor'),
		 fourthColor: await AsyncStorage.getItem('fourthColor'),
		 hijo: await AsyncStorage.getItem('hijo')
	  });
   }

   async checkExist() {
	  let array = this.props.data.contestaran.split(',');
	  let i = array.indexOf(this.state.role);
	  if (i > -1) {
		 await this.setState({contestador: this.state.role});
		 await this.getRespuestas();
	  }
   }

   async getUserData() {
	  await fetch(this.state.uri + '/api/user/data/v2', {
		 method: 'GET', headers: {
			Accept: 'application/json',
			'Content-Type': 'multipart/form-data',
			Authorization: 'Bearer ' + this.state.token
		 }
	  }).then(res => res.json())
		.then(responseJson => {
		   this.setState({role: responseJson.role});
		});
	  await this.checkExist();
   }

   async getRespuestas() {
	  if (this.state.enterados !== 0) {
		 if (this.state.role === this.state.contestador) {
			await fetch(this.state.uri + '/api/get/respuesta/enterado/' + this.props.data.id + '/' + this.state.hijo, {
			   method: 'GET', headers: {
				  Accept: 'application/json',
				  'Content-Type': 'multipart/form-data',
				  Authorization: 'Bearer ' + this.state.token
			   }
			}).then(res => res.json())
			  .then(responseJson => {
				 if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Noticia)', [{
					   text: 'Entendido', onPress: () => Actions.drawer()
					}]);
				 } else {
					this.setState({resp: responseJson});
				 }
			  });
		 }
	  }
   }

   async sendEnterado() {
	  let id = this.props.data.id;
	  let url = this.state.uri + '/api/feed/set/' + id;
	  if (this.state.role === 'Padre') {
		 url = this.state.uri + '/api/feed/set/' + id + '/' + this.state.hijo;
	  }
	  await fetch(url, {
		 method: 'POST', headers: {
			'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
		 }
	  }).then(res => res.json())
		.then(responseJson => {
		   if (responseJson.error !== undefined) {
			  Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Noticia)', [{
				 text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'News'}})
			  }]);
		   } else {
			  Alert.alert('Enterado', 'Has marcado como enterado esta noticia.', [{
				 text: 'Aceptar', onPress: () => Actions.pop({refresh: {key: 'News'}})
			  }]);
		   }
		});
   }

   async getEnterado() {
	  let id = this.props.data.id;
	  let url = this.state.uri + '/api/feed/view/enterados/' + id;
	  if (this.state.role === 'Padre') {
		 url = this.state.uri + '/api/feed/view/enterados/' + id + '/' + this.state.hijo;
	  }
	  fetch(url, {
		 method: 'GET', headers: {
			Accept: 'application/json', 'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
		 }
	  }).then(res => res.json())
		.then(responseJson => {
		   if (responseJson.error !== undefined) {
			  Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Noticia)', [{
				 text: 'Entendido', onPress: () => Actions.drawer()
			  }]);
		   } else {
			  this.setState({enterados: responseJson});
		   }
		});
   }

   prueba() {
	  let elObjeto = [];
	  if (this.props.data.respuestas[0].tipo === 'Múltiple' || this.props.data.respuestas[0].tipo === 'Única') {
		 this.state.hideHora.forEach((item, index) => {
			if (item === true) {
			   if (this.props.data.respuestas[index].sub_respuestas[0].tipo === 'Múltiple') {
				  if (index === 0) {
					 elObjeto.push({
						'id_respuesta': this.props.data.respuestas[index].id,
						'tipo': this.props.data.respuestas[0].tipo,
						'sub_respuesta': this.state.checkBoxBtns.toString(),
						'subTipo': this.props.data.respuestas[index].sub_respuestas[0].tipo
					 });
				  } else if (index === 1) {
					 elObjeto.push({
						'id_respuesta': this.props.data.respuestas[index].id,
						'tipo': this.props.data.respuestas[0].tipo,
						'sub_respuesta': this.state.checkBoxBtns1.toString(),
						'subTipo': this.props.data.respuestas[index].sub_respuestas[0].tipo
					 });
				  } else if (index === 2) {
					 elObjeto.push({
						'id_respuesta': this.props.data.respuestas[index].id,
						'tipo': this.props.data.respuestas[0].tipo,
						'sub_respuesta': this.state.checkBoxBtns2.toString(),
						'subTipo': this.props.data.respuestas[index].sub_respuestas[0].tipo
					 });
				  } else if (index === 3) {
					 elObjeto.push({
						'id_respuesta': this.props.data.respuestas[index].id,
						'tipo': this.props.data.respuestas[0].tipo,
						'sub_respuesta': this.state.checkBoxBtns3.toString(),
						'subTipo': this.props.data.respuestas[index].sub_respuestas[0].tipo
					 });
				  }
			   } else if (this.props.data.respuestas[index].sub_respuestas[0].tipo === 'Única') {
				  elObjeto.push({
					 'id_respuesta': this.props.data.respuestas[index].id,
					 'tipo': this.props.data.respuestas[0].tipo,
					 'sub_respuesta': this.state.subRespuesta[index],
					 'subTipo': this.props.data.respuestas[index].sub_respuestas[0].tipo
				  });
			   } else if (this.props.data.respuestas[index].sub_respuestas[0].tipo === 'Texto') {
				  elObjeto.push({
					 'id_respuesta': this.props.data.respuestas[index].id,
					 'tipo': this.props.data.respuestas[0].tipo,
					 'sub_respuesta': this.state.text[index],
					 'subTipo': this.props.data.respuestas[index].sub_respuestas[0].tipo
				  });
			   } else if (this.props.data.respuestas[index].sub_respuestas[0].tipo === 'Numérica') {
				  elObjeto.push({
					 'id_respuesta': this.props.data.respuestas[index].id,
					 'tipo': this.props.data.respuestas[0].tipo,
					 'sub_respuesta': this.state.text[index],
					 'subTipo': this.props.data.respuestas[index].sub_respuestas[0].tipo
				  });
			   } else {
				  elObjeto.push({
					 'id_respuesta': this.props.data.respuestas[index].id,
					 'tipo': this.props.data.respuestas[0].tipo,
					 'sub_respuesta': 'ninguno',
					 'subTipo': 'ninguno'
				  });
			   }

			}
		 });
	  } else {
		 elObjeto.push({
			'id_respuesta': this.props.data.respuestas[0].id,
			'tipo': this.props.data.respuestas[0].tipo,
			'sub_respuesta': this.state.text[0],
			'subTipo': 'ninguno'
		 });
	  }

	  elObjeto.forEach((item) => {
		 let formData = new FormData();
		 formData.append('new', JSON.stringify(item));
		 fetch(this.state.uri + '/api/set/respuesta/enterado/' + this.props.data.id + '/' + this.state.hijo, {
			method: 'POST', headers: {
			   Accept: 'application/json',
			   'Content-Type': 'multipart/form-data',
			   Authorization: 'Bearer ' + this.state.token
			}, body: formData
		 }).then(res => res.json())
		   .then(responseJson => {
			  if (responseJson.error !== undefined) {
				 Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Noticia)', [{
					text: 'Entendido', onPress: () => Actions.drawer()
				 }]);
			  }
		   });

	  });
   }

   isEnterado() {
	  return (<View style={styles.btnContainer}>
		 <TouchableOpacity
		   style={[styles.bigButton, {backgroundColor: this.state.mainColor}]}
		   onPress={() => Actions.push('HomePage')}>
			<Text style={styles.textButton}>Salir</Text>
		 </TouchableOpacity>
	  </View>);
   }

   noEnterado() {
	  if (this.state.role === this.state.contestador) {
		 if (this.props.data.respuestas.length === 0) {
			return (<View style={styles.btnContainer}>
			   <TouchableOpacity
				 style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
				 onPress={() => this.sendEnterado()}>
				  <Text style={styles.textButton}>De acuerdo, ¡Gracias!</Text>
			   </TouchableOpacity>
			</View>);
		 } else {
			return (<View style={styles.btnContainer}>
			   <TouchableOpacity
				 style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
				 onPress={() => [this.sendEnterado(), this.prueba()]}>
				  <Text style={styles.textButton}>Responder</Text>
			   </TouchableOpacity>
			</View>);
		 }
	  } else {
		 return (<View style={styles.btnContainer}>
			<TouchableOpacity
			  style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
			  onPress={() => this.sendEnterado()}>
			   <Text style={styles.textButton}>De acuerdo, ¡Gracias!</Text>
			</TouchableOpacity>
		 </View>);
	  }
   }


   //******************* Respuestas

   async hideHrs(index) {
	  if (this.props.data.respuestas[0].tipo === 'Única') {
		 this.state.hideHora.forEach((it, i) => {
			this.state.hideHora[i] = false;
		 });
		 this.state.hideHora[index] = true;
	  } else {
		 if (this.state.hideHora[index]) {
			this.state.hideHora[index] = false;
		 } else {
			this.state.hideHora[index] = true;
		 }
	  }
	  this.setState({axu: 0});
   }

   renderRespuestas() {
	  let buttonsRespuestas = [];
	  this.props.data.respuestas.forEach((itemRespuestas, indexRespuestas) => {
		 buttonsRespuestas.push(this.renderRespuesta(itemRespuestas, indexRespuestas));
	  });
	  return buttonsRespuestas;
   }

   renderRespuesta(itemRespuestas, indexRespuestas) {
	  return (<View key={indexRespuestas + 'Resp'}>
		 <View
		   style={[styles.row_PC, {
			  borderColor: this.state.fourthColor, paddingLeft: 8, paddingRight: 8
		   }]}>
			<View style={[styles.btn2]}>
			   <Text style={{fontSize: responsiveFontSize(2)}}>
				  {itemRespuestas.respuesta}
			   </Text>
			</View>
			<View style={[styles.btn_2, {alignItems: 'flex-end'}]}>
			   <Switch value={this.state.hideHora[indexRespuestas]}
					   onSyncPress={() => [this.onListItemPressedRespuestas(indexRespuestas, itemRespuestas), this.hideHrs(indexRespuestas)]}/>
			</View>
		 </View>
		 {this.state.hideHora[indexRespuestas] === true ? (<View style={[styles.row, {alignItems: 'center'}]}>
			{(this.state.tipo[indexRespuestas] === 'Múltiple') || this.state.tipo[indexRespuestas] === 'Única' ?
			  <View style={styles.row}>{this.renderSubRespuestas(indexRespuestas)}</View> : null}
			{this.state.tipo[indexRespuestas] === 'Texto' ? <TextInput
			  onChangeText={text => this.onChangeNum(text, indexRespuestas)}
			  placeholder={'Respuesta'}
			  returnKeyType='next'
			  underlineColorAndroid='transparent'
			  value={this.state.text[indexRespuestas]}
			  style={[styles.inputPicker, styles.btn11, {
				 borderColor: this.state.secondColor, height: responsiveHeight(5), padding: 5
			  }]}
			/> : null}
			{this.state.tipo[indexRespuestas] === 'Numérica' ? <TextInput
			  keyboardType='numeric'
			  maxLength={2}
			  onChangeText={text => this.onChangeNum(text, indexRespuestas)}
			  placeholder={'ej.: del 1 al 99'}
			  returnKeyType='next'
			  underlineColorAndroid='transparent'
			  value={this.state.text[indexRespuestas]}
			  style={[styles.inputPicker, styles.btn11, {
				 borderColor: this.state.secondColor, height: responsiveHeight(5), padding: 5
			  }]}
			/> : null}
		 </View>) : null}
	  </View>);
   }


   async onListItemPressedRespuestas(indexRespuestas, itemRespuestas) {
	  this.state.tipo[indexRespuestas] = await itemRespuestas.sub_respuestas[0].tipo;
	  await this.setState({
		 selectedIndexRespuestas: indexRespuestas, respuesta: itemRespuestas.respuesta
	  });

   }

   //******************* Respuestas
   renderSubRespuestas(i) {
	  let buttonsRespuestas = [];
	  this.props.data.respuestas[i].sub_respuestas.forEach((itemRespuestas, indexRespuestas) => {
		 buttonsRespuestas.push(this.renderSubRespuesta(itemRespuestas, indexRespuestas, i));
	  });
	  return buttonsRespuestas;
   }

   renderSubRespuesta(itemRespuestas, indexRespuestas, i) {
	  let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn5, {borderColor: this.state.secondColor}];
	  let texto = [styles.textoN];

	  if (itemRespuestas.tipo === 'Única') {
		 if (this.state.selectedIndexSubRespuestas[i] === indexRespuestas) {
			smallButtonStyles.push(styles.listButtonSelected, {
			   backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		 }
		 return (<TouchableHighlight
		   key={indexRespuestas + i + 'Sub'}
		   underlayColor={this.state.secondColor}
		   style={smallButtonStyles}
		   onPress={() => this.onListItemPressedSubRespuestas(indexRespuestas, itemRespuestas, i)}>
			<View style={styles.listItem}>
			   <Text style={texto}>{itemRespuestas.respuesta}</Text>
			</View>
		 </TouchableHighlight>);
	  } else {
		 if (i === 0) {
			if (this.state.checkBoxBtnsIndex[indexRespuestas] === 1) {
			   smallButtonStyles.push(styles.listButtonSelected, {
				  backgroundColor: this.state.secondColor
			   });
			   texto.push(styles.textoB);
			}
		 } else if (i === 1) {
			if (this.state.checkBoxBtnsIndex1[indexRespuestas] === 1) {
			   smallButtonStyles.push(styles.listButtonSelected, {
				  backgroundColor: this.state.secondColor
			   });
			   texto.push(styles.textoB);
			}
		 } else if (i === 2) {
			if (this.state.checkBoxBtnsIndex2[indexRespuestas] === 1) {
			   smallButtonStyles.push(styles.listButtonSelected, {
				  backgroundColor: this.state.secondColor
			   });
			   texto.push(styles.textoB);
			}
		 } else if (i === 3) {
			if (this.state.checkBoxBtnsIndex3[indexRespuestas] === 1) {
			   smallButtonStyles.push(styles.listButtonSelected, {
				  backgroundColor: this.state.secondColor
			   });
			   texto.push(styles.textoB);
			}
		 }
		 return (<TouchableHighlight
		   key={indexRespuestas + i + 'Sub'}
		   underlayColor={this.state.secondColor}
		   style={smallButtonStyles}
		   onPress={() => this.onListItemPressedSubRespuestas1(indexRespuestas, itemRespuestas, i)}>
			<View style={styles.listItem}>
			   <Text style={texto}>{itemRespuestas.respuesta}</Text>
			</View>
		 </TouchableHighlight>);
	  }

   }

   async onListItemPressedSubRespuestas(indexRespuestas, itemRespuestas, i) {
	  this.state.selectedIndexSubRespuestas[i] = await indexRespuestas;
	  this.state.subRespuesta[i] = await itemRespuestas.id;
	  await this.setState({aux: 0});

   }

   async onListItemPressedSubRespuestas1(indexRespuestas, itemRespuestas, i) {
	  if (i === 0) {
		 let j = this.state.checkBoxBtns.indexOf(itemRespuestas.respuesta);
		 if (j > -1) {
			this.state.checkBoxBtns.splice(j, 1);
			this.state.checkBoxBtnsIndex[indexRespuestas] = 0;
		 } else {
			this.state.checkBoxBtns.push(itemRespuestas.respuesta);
			this.state.checkBoxBtnsIndex[indexRespuestas] = 1;
		 }
	  } else if (i === 1) {
		 let k = this.state.checkBoxBtns1.indexOf(itemRespuestas.respuesta);
		 if (k > -1) {
			this.state.checkBoxBtns1.splice(k, 1);
			this.state.checkBoxBtnsIndex1[indexRespuestas] = 0;
		 } else {
			this.state.checkBoxBtns1.push(itemRespuestas.respuesta);
			this.state.checkBoxBtnsIndex1[indexRespuestas] = 1;
		 }
	  } else if (i === 2) {
		 let c = this.state.checkBoxBtns2.indexOf(itemRespuestas.respuesta);
		 if (c > -1) {
			this.state.checkBoxBtns2.splice(c, 1);
			this.state.checkBoxBtnsIndex2[indexRespuestas] = 0;
		 } else {
			this.state.checkBoxBtns2.push(itemRespuestas.respuesta);
			this.state.checkBoxBtnsIndex2[indexRespuestas] = 1;
		 }
	  } else if (i === 3) {
		 let h = this.state.checkBoxBtns3.indexOf(itemRespuestas.respuesta);
		 if (h > -1) {
			this.state.checkBoxBtns3.splice(h, 1);
			this.state.checkBoxBtnsIndex3[indexRespuestas] = 0;
		 } else {
			this.state.checkBoxBtns3.push(itemRespuestas.respuesta);
			this.state.checkBoxBtnsIndex3[indexRespuestas] = 1;
		 }
	  }
	  await this.setState({aux: 0});
   }

   async onChangeNum(text, i) {
	  this.state.text[i] = await  text;
	  await this.setState({aux: 0});
   }

   // ************************* Render

   noEnteradoRespuesta() {
	  return (<View key={'RespuestaT'}>{this.props.data.respuestas[0].tipo === 'Texto' ? <TextInput
		onChangeText={text => this.onChangeNum(text, 0)}
		placeholder={'Respuesta'}
		returnKeyType='next'
		underlineColorAndroid='transparent'
		value={this.state.text[0]}
		style={[styles.inputPicker, styles.btn11, {
		   borderColor: this.state.secondColor, height: responsiveHeight(5), padding: 5
		}]}
	  /> : null}
		 {this.props.data.respuestas[0].tipo === 'Numérica' ? <TextInput
		   keyboardType='numeric'
		   maxLength={2}
		   onChangeText={text => this.onChangeNum(text, 0)}
		   placeholder={'Ej.: del 1 al 99'}
		   returnKeyType='next'
		   underlineColorAndroid='transparent'
		   value={this.state.text[0]}
		   style={[styles.inputPicker, styles.btn11, {
			  borderColor: this.state.secondColor, height: responsiveHeight(5), padding: 5
		   }]}
		 /> : null}
		 {this.props.data.respuestas[0].tipo === 'Múltiple' ? this.renderRespuestas() : null}
		 {this.props.data.respuestas[0].tipo === 'Única' ? this.renderRespuestas() : null}</View>);
   }

   isEnteradoRespuesta() {
	  return (<View style={{
		 borderWidth: .3, borderColor: this.state.secondColor, borderRadius: 6, padding: 10, marginVertical: 10
	  }}>
		 <Text>Usted a contestado:</Text>
		 {this.res()}
	  </View>);
   }

   res() {
	  let a = [];
	  this.state.resp.forEach((item, index) => {
		 if (item.respuestas[0].tipo === 'Múltiple' && item.respuestas[0].subTipo === 'Única') {
			a.push(<View style={{paddingLeft: 10, paddingVertical: 7}}>
			   <Text>{item.respuestas[0].respuesta}</Text>
			   <View style={{paddingLeft: 10}}>
				  <Text>{item.sub_respuestas[0].respuesta}</Text>
			   </View>
			</View>);
		 } else if (item.respuestas[0].tipo === 'Múltiple' && item.respuestas[0].subTipo !== 'ninguno') {
			a.push(<View style={{paddingLeft: 10, paddingVertical: 7}}>
			   <Text>{item.respuestas[0].respuesta}</Text>
			   <View style={{paddingLeft: 10}}>
				  <Text>{item.sub_respuesta}</Text>
			   </View>
			</View>);
		 } else if (item.respuestas[0].tipo === 'Múltiple' && item.respuestas[0].subTipo === 'ninguno') {
			a.push(<View style={{paddingLeft: 10, paddingVertical: 7}}>
			   <View style={{paddingLeft: 10}}>
				  <Text>{item.respuestas[0].respuesta}</Text>
			   </View>
			</View>);
		 } else if (item.respuestas[0].tipo === 'Única' && item.respuestas[0].subTipo === 'Única') {
			a.push(<View style={{paddingLeft: 10, paddingVertical: 7}}>
			   <Text>{item.respuestas[0].respuesta}</Text>
			   <View style={{paddingLeft: 10}}>
				  <Text>{item.sub_respuestas[0].respuesta}</Text>
			   </View>
			</View>);
		 } else if (item.respuestas[0].tipo === 'Única' && item.respuestas[0].subTipo !== 'ninguno') {
			a.push(<View style={{paddingLeft: 10, paddingVertical: 7}}>
			   <Text>{item.respuestas[0].respuesta}</Text>
			   <View style={{paddingLeft: 10}}>
				  <Text>{item.sub_respuesta}</Text>
			   </View>
			</View>);
		 } else if (item.respuestas[0].tipo === 'Única' && item.respuestas[0].subTipo === 'ninguno') {
			a.push(<View style={{paddingLeft: 10, paddingVertical: 7}}>
			   <View style={{paddingLeft: 10}}>
				  <Text>{item.respuestas[0].respuesta}</Text>
			   </View>
			</View>);
		 } else if (item.respuestas[0].sub_respuesta !== 'ninguno') {
			a.push(<View style={{paddingLeft: 10, paddingVertical: 7}}>
			   <View style={{paddingLeft: 10}}>
				  <Text>{item.sub_respuesta}</Text>
			   </View>
			</View>);
		 }
	  });
	  return a;
   }

   async link(url){
   	await Linking.openURL(url);
   }

   render() {
	  return (<View style={styles.container}>
		 <ScrollView style={[styles.noticia_NC, {backgroundColor: 'white'}]}>
			<Text style={styles.titulo}>{this.props.data.titulo}</Text>
			<HTMLView
			  style={styles.descripcion}
			  value={this.props.data.descripcion}
              onLinkPress={(url) => this.link(url)}
			/>
			{this.props.data.archivo !== null ? <Image
			  style={{
				 width: responsiveWidth(94), height: responsiveHeight(94), marginTop: 20
			  }}
			  source={{
				 uri: this.state.uri + '/' + this.props.data.archivo
			  }}
			/> : null}
			{this.state.role === this.state.contestador ? this.props.data.respuestas !== undefined ?
			  this.props.data.respuestas.length !== 0 ?
				this.state.enterados !== 0 ? this.isEnteradoRespuesta() : this.noEnteradoRespuesta() : null : null :
			  null}

		 </ScrollView>
		 {this.state.enterados !== 0 ? this.isEnterado() : this.noEnterado()}
	  </View>);
   }
}
