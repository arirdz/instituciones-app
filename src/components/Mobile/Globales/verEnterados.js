import React from 'react';
import {Alert, AsyncStorage, ScrollView, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {responsiveHeight} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import GradoyGrupo from './GradoyGrupo';
import Modal from "react-native-modal";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class VerEnterados extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            porcentajes: '',
            porcentajesN: '',
            nombres: [],
            cososPicker: [],
            grados: [],
            grupos: [],
            selectedIndexLista: -1,
            elTaller: '',
            role: [{label: 'Padres', role: 'Padre'}, {label: 'Alumnos', role: 'Alumno'}, {
                label: 'Admón.', role: 'Admin'
            }, {label: 'Maestros', role: 'Maestro'}],
            selectedIndexRole: -1,
            elRole: '',
            selGrad: 1,
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selGrup: 'A',
            mes: moment().month() + 1,
            aux: 0,
            losNombres: [],
            estadisticas: [],
            idNoticia: '',
            isModalCateg: false
        };

    }

    async componentWillMount() {
        await this.getURL();
        await this.getMeses();
    }

    async getURL() {
        this.setState({
            uri: await AsyncStorage.getItem('uri'),
            token: await AsyncStorage.getItem('token'),
            mainColor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor')
        });
    }

    //++++++++ Meses +++++++++
    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses');
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, mes: option.mes, selectedIndexRole: -1});
    }

    //+++++++++++ Roles +++++++++++
    async onListPressedRole(itemRole, indexRole) {
        await this.setState({
            selectedIndexRole: indexRole,
            elRole: itemRole.role,
            data: [],
            selGrad: '',
            selGrup: '',
            selectedIndexGrados: -1
        });
        if (this.state.elRole === 'Admin' || this.state.elRole === 'Maestro') {
            await this.getFeed();
        }
    }

    renderRole(itemRole, indexRole) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn_2, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.selectedIndexRole === indexRole) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<View key={indexRole + 'roles'}>
            <TouchableHighlight
                style={[smallButtonStyles]}
                underlayColor={'transparent'}
                onPress={() => this.onListPressedRole(itemRole, indexRole)}>
                <Text style={texto}>{itemRole.label}</Text>
            </TouchableHighlight>
        </View>);
    }

    renderRoles() {
        let btnRole = [];
        this.state.role.forEach((itemRole, indexRole) => {
            btnRole.push(this.renderRole(itemRole, indexRole));
        });
        return btnRole;
    }

    //++++++ Get Feed Admin ++++++
    getFeed() {
        fetch(this.state.uri + '/api/noticias/by/role/' + this.state.mes + '/' + this.state.elRole, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Enterados)', [{
                        text: 'Entendido', onPress: () => Actions.drawer()
                    }]);
                } else {
                    this.setState({data: responseJson});
                }
            });
    };

    //++++++ Grado y Grupo +++++
    async onListItemPressedGrado(index, grado) {
        await this.setState({
            selectedIndexGrados: index, selGrad: grado, porcentajes: '', porcentajesN: '', selectedIndexName: -1, data: []
        });
    }

    async onListItemPressedGrupos(index, grupo) {
        await this.setState({
            selectedIndexGrupo: index, selGrup: grupo, porcentajes: '', porcentajesN: '', selectedIndexName: -1
        });
        await this.getFeedPadre();
    }

    //++++++ Get Feed Padre Alumno ++++++
    getFeedPadre() {
        fetch(this.state.uri + '/api/noticias/by/role/' + this.state.mes + '/' + this.state.elRole + '/' + this.state.selGrad + '/' + this.state.selGrup, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Enterados)', [{
                        text: 'Entendido', onPress: () => Actions.drawer()
                    }]);
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    // **********************   lista de noticias

    async onListPressedName(index, nomvres, aaydi) {
        await this.setState({
            selectedIndexName: index, selName: nomvres, idNoticia: aaydi
        });
        await this.fetchPorcentajeses();
    }

    renderName(itemName, indexName) {
        let a = this.state.data.length - 1;
        let tabRow = [styles.rowTabla, styles.modalWidth];
        if (indexName !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {
            borderColor: this.state.secondColor,
            alignItems: 'center', justifyContent: 'center'
        }];
        let texto = [styles.textoN];
        if (this.state.selectedIndexName === indexName) {
            smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexName}
                underlayColor={this.state.secondColor}
                style={[
                    smallButtonStyles,
                    {
                        borderColor: this.state.secondColor,
                        height: responsiveHeight(4)
                    }
                ]}
                onPress={() => this.onListPressedName(indexName, itemName.titulo, itemName.id)}
            >
                <View style={styles.listItem}>
                    <Text numberOfLines={1} style={[texto, styles.btn2]}>
                        {itemName.titulo}
                    </Text>
                    <Text style={texto}>{moment(itemName.created_at).format('MMM D YYYY').toUpperCase()}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderNames() {
        let buttonsNames = [];
        this.state.data.forEach((itemName, indexName) => {
            buttonsNames.push(this.renderName(itemName, indexName));
        });
        return buttonsNames;
    }

    //++++++ Fetch Porcentajes ++++++
    async fetchPorcentajeses() {
        let url = this.state.uri + '/api/estadisticas/get/' + this.state.idNoticia + '/' + this.state.elRole + '/' + this.state.selGrad + '/' + this.state.selGrup;
        if (this.state.elRole === 'Admin' || this.state.elRole === 'Maestro') {
            url = this.state.uri + '/api/estadisticas/get/' + this.state.idNoticia + '/' + this.state.elRole;
        }
        await fetch(url, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => response.json())
            .then(responseJson => {
                console.log(responseJson);
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Por el momento no hay datos a consultar', [{
                        text: 'Gracias', onPress: () => Actions.News()
                    }]);
                } else {
                    this.setState({estadisticas: responseJson});
                }
            });
        let px = await 100 / (this.state.estadisticas.NoEnteradosCount + this.state.estadisticas.EnteradosCount);
        let noE = await px * this.state.estadisticas.NoEnteradosCount;
        let en = await px * this.state.estadisticas.EnteradosCount;
        await this.setState({porcentajes: en.toFixed(1), porcentajesN: noE.toFixed(1)});
        console.log(en.toFixed(1),noE.toFixed(1))
    };

    //+++++ Enterados ++++++
    fetchEnterados() {
        if (this.state.porcentajes === '0.0') {
            Alert.alert('¡Ups!', 'Nadie ha contestado esta noticia, ¿desea enviar una notificacion de recordatorio?', [{
                text: 'No gracias '
            }, {text: 'Sí por favor', onPress: () => this.sendRecordatorio()}]);
            this.setState({losNombres: []});
        } else if (this.state.porcentajes === '100.0') {
            Alert.alert('¡En hora buena!', 'Todos los usuarios seleccionados han contestado o marcado esta noticia como vista', [{
                text: 'Gracias'
            }]);
            this.setState({losNombres: this.state.estadisticas.nomEn, enteradosSelected: 0});
        } else {
            Alert.alert('¡Mmmmm!', 'Parece que no todos han contestado, ¿desea enviar una notificacion de recordatorio?', [{
                text: 'No gracias '
            }, {text: 'Si por favor', onPress: () => this.sendRecordatorio()}]);
            this.setState({losNombres: this.state.estadisticas.nomEn, enteradosSelected: 0});
        }
    }

    //++++++ No Enterados ++++++
    fetchNoEnterados() {
        if (this.state.porcentajesN === '100.0') {
            Alert.alert('¡Ups!', 'Nadie ha contestado esta noticia, ¿desea enviar una notificacion de recordatorio?', [{
                text: 'No gracias '
            }, {text: 'Si por favor', onPress: () => this.sendRecordatorio()}]);
            this.setState({losNombres: this.state.estadisticas.nomNoEn, enteradosSelected: 1});
        } else if (this.state.porcentajesN === '0.0') {
            Alert.alert('¡En hora buena!', 'Todos los usuarios seleccionados han contestado o marcado esta noticia como vista', [{
                text: 'Gracias'
            }]);
            this.setState({losNombres: []});
        } else {
            Alert.alert('¡Mmmmm!', 'Parece que no todos han contestado, ¿desea enviar una notificacion de recordatorio?', [{
                text: 'No gracias '
            }, {text: 'Si por favor', onPress: () => this.sendRecordatorio()}]);
            this.setState({losNombres: this.state.estadisticas.nomNoEn, enteradosSelected: 1});
        }
    }

    async onListItemPressedLista(indexLista, taller) {
        await this.setState({
            selectedIndexLista: indexLista, elTaller: taller, porcentajes: 0.0, porcentajesN: 0.0, data: []
        });
    }

    // ****** Lista de Enterados / No Enterados ++++++
    renderRespuesta(it, i) {
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {
            borderColor: this.state.secondColor,
            alignItems: 'center', justifyContent: 'center'
        }];
        let texto = [styles.textoN];
        return (<View style={[smallButtonStyles,{paddingVertical: responsiveHeight(1.5)}]} key={i + 'respuesta'}>
            {this.state.elRole === 'Padre'?
                <View>
                    <Text style={texto}>{it.user.name}</Text>
                    {it.respuestas_padre !== undefined ? it.respuestas_padre[i] !== undefined ? it.respuestas_padre[i].tipo === 'Texto' || it.respuestas_padre[i].tipo === 'Numérica' ? (
                        <Text style={texto}>Respuesta:{this.respuestasTexto(it)}</Text>) : null : null : null}
                </View>:
                <View>
                    <Text style={texto}>{it.ellos[0].name}</Text>
                </View>}
        </View>);
    }

    respuestasTexto(item) {
        let buttonsNames = [];
        item.respuestas_padre.forEach((it, i) => {
            buttonsNames.push(this.respuestaTexto(it, i));
        });
        return buttonsNames;
    }

    respuestaTexto(it, i) {
        return (<Text key={i + 'subResp'}>
            {it.sub_respuesta}
        </Text>);
    }

    renderRespuestas() {
        let buttonsNames = [];
        if (this.state.estadisticas.length !== 0) {
            this.state.estadisticas.LosEnterados.forEach((it, i) => {
                buttonsNames.push(this.renderRespuesta(it, i));
            });
        }
        return buttonsNames;
    }

    //+++++ nombres de los no enterados +++++
    renderNoEnterados() {
        let btn = [];
        if (this.state.losNombres.length !== 0) {
            this.state.losNombres.forEach((it, i) => {
                btn.push(this.renderNoEnterado(it, i));
            });
        }
        return btn;
    }

    renderNoEnterado(it, i) {
        let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {
            borderColor: this.state.secondColor,
            alignItems: 'center', justifyContent: 'center'
        }];
        let texto = [styles.textoN];
        return (
            <View
                key={i + 'noe'}
                underlayColor={this.state.secondColor}
                style={[
                    smallButtonStyles,
                    {
                        borderColor: this.state.secondColor,
                        height: responsiveHeight(4)
                    }
                ]}
            >
                <View style={styles.listItem}>
                    <Text numberOfLines={1} style={[texto, styles.btn2]}>
                        {it.name}
                    </Text>
                </View>
            </View>);
    }

    //+++++ Recordatorio +++++
    sendRecordatorio() {
        let formData = new FormData();
        formData.append('new', JSON.stringify(this.state.estadisticas.nomNoEn));
        fetch(this.state.uri + '/api/recordatorio/noticia/' + this.state.idNoticia + '/' + this.state.elRole, {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(response => response.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Recordatorio Noticia)', [{
                        text: 'Entendido'
                    }]);
                } else {
                    Alert.alert('¡Excelente!', 'Se han mandado los recordatorios con éxito', [{
                        text: 'Entendido'
                    }]);
                }
            });
    }

    async openModal() {
        await this.setState({isModalCateg: true});
    }

    async closeModal() {
        await this.setState({isModalCateg: false});
    }

    async openModal1() {
        await this.setState({isModalCateg1: true,});
    }

    async closeModal1() {

        await this.setState({isModalCateg1: false});
    }

    //++++++ Render +++++++
    render() {
        const losRoles = this.renderRoles();
        const noticias = this.renderNames();
        let listas = '';
        if (this.state.enteradosSelected === 0) {
            listas = this.renderRespuestas();
        } else {
            listas = this.renderNoEnterados();
        }
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (<View style={[styles.container, {...ifIphoneX({marginBottom: 80}, {marginBottom: 45})}]}>
            <ScrollView
                contentContainerStyle={styles.contentContainer}
                showsVerticalScrollIndicator={false}>
                <Text
                    style={[styles.main_title, {color: this.state.thirdColor, marginTop: 10}]}>
                    Seleccione el mes
                </Text>
                <ModalSelector
                    data={data}
                    selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
                    initValue={mess}
                    cancelText='Cancelar'
                    optionTextStyle={{color: this.state.thirdColor}}
                    onChange={option => this.onChange(option)}
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione destinatarios a consultar
                </Text>
                <View style={[styles.buttonsRow, {marginTop: 5}]}>{losRoles}</View>
                {this.state.selectedIndexRole === 1 || this.state.selectedIndexRole === 0 ? (<View>
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexGrupo, grupo) => this.onListItemPressedGrupos(indexGrupo, grupo)}
                        onListItemPressedGrado={(indexGrado, grado) => this.onListItemPressedGrado(indexGrado, grado)}
                        onListItemPressedLista={(indexLista, taller) => this.onListItemPressedLista(indexLista, taller)}
                        listaVar={true}
                        todos={'1'}/>
                </View>) : null}
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione la noticia a consultar
                </Text>
                <View style={{
                    alignItems: 'center',
                    backgroundColor: this.state.fourthColor,
                    padding: 15,
                    borderRadius: 6
                }}>
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            styles.btnShowMod,
                            {
                                marginTop: 5,
                                marginBottom: 5,
                                borderColor: this.state.secondColor,
                                backgroundColor: '#fff',
                                height: responsiveHeight(5)
                            }
                        ]}
                        onPress={() => this.openModal()}>
                        <Text>Ver noticias</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    isVisible={this.state.isModalCateg}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text style={[styles.main_title, styles.modalWidth]}>
                            Seleccione una opción
                        </Text>
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor, height: responsiveHeight(60)}]}
                            showsHorizontalScrollIndicator={false}
                        >
                            <ScrollView>
                                {noticias}
                            </ScrollView>
                        </View>

                        <View style={[styles.modalWidth, styles.row]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({isModalCateg: false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.closeModal()}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Modal
                    isVisible={this.state.isModalCateg1}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
                        <Text style={[styles.main_title, styles.modalWidth]}>
                            Seleccione una opción
                        </Text>
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor, height: responsiveHeight(60)}]}
                            showsHorizontalScrollIndicator={false}
                        >
                            <ScrollView>
                                {listas}
                            </ScrollView>
                        </View>

                        <View style={[styles.modalWidth, styles.row, {justifyContent: 'center'}]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.closeModal1()}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <View style={styles.adminContainer}>
                    <View style={styles.enteradosTitulo}>
                        <Text style={styles.textEnterados}>Enterados</Text>
                    </View>
                    <View style={styles.sinEnterarTitulo}>
                        <Text style={styles.textNoEnterados}>No Enterados</Text>
                    </View>
                </View>
                <View style={styles.adminContainer}>
                    <TouchableOpacity
                        style={[styles.buttonEnterados, {backgroundColor: this.state.fourthColor}]}
                        onPress={() => this.fetchEnterados()}>
                        <Text style={[styles.text_VE, {color: this.state.thirdColor}]}>
                            {this.state.porcentajes}%
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.buttonNoEnterados, {backgroundColor: this.state.mainColor}]}
                        onPress={() => this.fetchNoEnterados()}>
                        <Text style={styles.text_VE}>
                            {this.state.porcentajesN}%
                        </Text>
                    </TouchableOpacity>
                </View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Listado de usuarios
                </Text>
                <View style={{
                    alignItems: 'center',
                    backgroundColor: this.state.fourthColor,
                    padding: 15,
                    borderRadius: 6,
                    marginBottom: 15
                }}>
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            styles.btnShowMod,
                            {
                                marginTop: 5,
                                marginBottom: 5,
                                borderColor: this.state.secondColor,
                                backgroundColor: '#fff',
                                height: responsiveHeight(5)
                            }
                        ]}
                        onPress={() => this.openModal1()}>
                        <Text>Ver usuarios</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>);
    }
}
