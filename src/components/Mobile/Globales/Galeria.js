import React from 'react';
import {
    Alert,
    AsyncStorage,
    CameraRoll,
    Image,
    Modal,
    PermissionsAndroid,
    Platform,
    ScrollView,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
	RefreshControl,
    View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import styles from '../../styles'
import Feather from 'react-native-vector-icons/Feather';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class galeria extends React.Component {
    _handleButtonPress = () => {
        CameraRoll.getPhotos({
            first: 1000,
            assetType: 'All'
        })
            .then(r => {
                this.setState({photos: r.edges});
            })
            .catch((err) => {
                //Error Loading Images
            });
        this.setState({indexSeleccionados: [], seleccionados: []});
        this.state.photos.forEach((it, i) => {
            this.state.indexSeleccionados.push(i);
            this.state.seleccionados.push(it);
        })
    };

	_onRefresh = () => {
		this.setState({refreshing: true});
		this.getAlbumes();
	};

    constructor(props) {
        super(props);
        this.state = {
            image: null,
			refreshing:false,
            images: null,
            modalVisible: false,
            modalVisible1: false,
            albumes: [],
            row: false,
            agregar: true,
            imagenes: [],
            imageURLs: [],
            elalbum: '',
            visible: false,
            pictures: [],
            open: false,
            indx: null,
            photos: [],
            indexSeleccionados: [],
            seleccionados: [],
            spinner: false,
            spin: false
        }
    }

    async componentWillMount() {
        await this.getURL();
        await this.getAlbumes();
    }

    async componentDidMount() {
        await this.requestPhotosPermission()
    }

    async requestPhotosPermission() {
        try {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this._handleButtonPress();
            } else {
                console.log('Photos permission denied')
            }
        } catch (err) {
            console.warn(err)
        }
        try {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this._handleButtonPress();
            } else {
                console.log('Photos permission denied')
            }
        } catch (err) {
            console.warn(err)
        }
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let mainColor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: mainColor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            text: ''
        });
    }

    async getAlbumes() {
        await fetch(this.state.uri + '/api/get/albumes',
            {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();
            }).then(responseJson => {
				this.setState({refreshing: false});
                if (responseJson.error === null) {
                    Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Galeria)',[
                        {text:'Enterado'}
                    ]);
                } else {
                    this.setState({albumes: []});
                    this.setState({albumes: responseJson});
                }
            });
    };


    setModalVisible(visible) {
        this.setState({modalVisible: visible});
        this.getAlbumes();
        this.setState({seleccionados: []})
    }

    setModalVisible1(visible) {
        this.setState({modalVisible1: visible});
        this._handleButtonPress();
        this.getAlbumes();
    }

    saveImages() {
        if (this.state.seleccionados.length === 0) {
            Alert.alert('Atención', 'No se han seleccionado las imágenes');
        } else if (this.state.text === '') {
            Alert.alert('Atención', 'No se han estableecido un titulo para el album');
        } else {
            for (let i = 0; i < this.state.seleccionados.length; i++) {
                const fd = new FormData();
                let imagen = this.state.seleccionados[i];
                fd.append('image',
                    {
                        uri: this.state.seleccionados[i],
                        type: 'image/jpeg',
                        name: 'photo'

                    });
                fd.append('album', this.state.text);
                fetch(this.state.uri + '/api/post/imagenes', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        Authorization: 'Bearer ' + this.state.token
                    },
                    body: fd
                }).then(res => {
                    console.log(res)
                });
            }
            this.rndr();
        }

    };

    async rndr() {
        await Alert.alert('Subida de imágenes', 'Se ha iniciado el proceso de carga de imágenes, verifique el estatus en un momento', [{
            text: 'Enterado',
            onPress: () => [this.setModalVisible1(false), this.getAlbumes()]
        }]);
    }

    async borrar(img) {
        await Alert.alert('Atención', '¿Está seguro que desea borrar la imágen?', [{
            text: 'Si',
            onPress: () => this.deleteImg(img)
        }, {text: 'No'}]);
    }

    async deleteImg(item) {
        await fetch(this.state.uri + '/api/delete/imagenes', {
            method: 'post',
            headers: {
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            },
            body: JSON.stringify({
                url: item
            })
        }).then(res => res.json())
            .then(responseJson => {
                Alert.alert('', 'Imágen eliminada con éxito', [{
                    text: 'Enterado',
                    onPress: () => [this.setState({
                        open: false,
                        indx: 0
                    }), this.setModalVisible(false), this.getAlbumes()]
                }]);
            });
    }

    card() {
        let carta = [];
        this.state.albumes.forEach((item, i) => {
            carta.push(
                <View
                    key={i + 'carta'}
                    style={[
                        styles.widthall,
                        // styles.row,
                        {
                            // height: responsiveHeight(18),
                            marginVertical: 5,
                            borderWidth: 0.5,
                            borderRadius: 6,
                            borderColor: 'lightgray',
                            backgroundColor: this.state.fourthColor,
                            paddingHorizontal: 6,
                            paddingVertical: 10,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }
                    ]}>
                    <View style={[styles.row, styles.widthall, {padding: 10, justifyContent: 'flex-start'}]}>
                        <View style={[styles.btn3_5]}>
                            <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Nombre del álbum
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                                marginTop: 3
                            }}>{item.nombre}</Text>
                        </View>
                        <View style={[styles.btn5]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Fecha
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{moment(item.created_at).format('L')}</Text>
                        </View>
                        <View style={[styles.btn5, {alignItems: 'center'}]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Imágenes
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>{item.imagen_count}</Text>
                        </View>
                    </View>
                    <View
                        style={[styles.widthall, {
                            borderBottomColor: '#a9a9a9',
                            borderBottomWidth: .5,
                            paddingVertical: 5
                        }]}
                    />
                    <View style={[styles.row, styles.widthall, {paddingTop: 6}]}>
                        <View style={[styles.btn3]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Borrar álbum
                            </Text>
                            <Feather name='trash-2' size={20} color='#000' style={{paddingLeft: 50, paddingTop: 3}}
                                     onPress={() => this.setState(this.borrarAlbum(item))}/>
                        </View>
                        <View style={[styles.btn3]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Agregar imágenes
                            </Text>
                            <MaterialIcons name='add-a-photo' size={20} color='#000'
                                           style={{paddingLeft: 50, paddingTop: 3}}
                                           onPress={() => [this.setState({
                                               text: item.nombre,
                                               modalVisible1: true
                                           }), this._handleButtonPress()]}/>
                        </View>
                        <View style={[styles.btn3, {alignItems: 'center'}]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Ver álbum
                            </Text>
                            <Feather name='eye' size={20} color='#000'
                                     style={{paddingTop: 3}}
                                     onPress={() => this.setState([{
                                         row: false,
                                         agregar: true
                                     }, this.getPictures(item.id)])}/>
                        </View>
                    </View>
                </View>
            )
        });
        return carta;
    }

    borrarAlbum(item) {
        Alert.alert('Alerta', '¿Está seguro que desea borrar el álbum?', [{
            text: 'Sí',
            onPress: () => this.deleteAlbum(item)
        }, {text: 'No'}]);
    }

    deleteAlbum(item) {
        fetch(this.state.uri + '/api/delete/albumes/' + item.id,
            {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();
            }).then(responseJson => {
            if (responseJson.error === null) {
                Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Noticias)');
            } else {
                Alert.alert('', 'Álbum eliminado con éxito', [{text: 'Enterado', onPress: () => this.getAlbumes()}]);
            }
        });
    }

    getPictures(id) {
        this.setState({elalbum: id});
        fetch(this.state.uri + '/api/get/pictures/' + id,
            {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();

            }).then(responseJson => {
            if (responseJson.error === null) {
                Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Galeria)');
            } else {
                this.setState({pictures: []});
                this.setState({pictures: responseJson});
                if (this.state.pictures.length > 0) {
                    this.state.imageURLs = this.state.pictures.map(
                        (img: Object, index: number) => ({
                            URI: img.url,
                            thumbnail: img.url,
                            id: String(index)

                        })
                    );
                    this.setModalVisible(true);
                } else {
                    null
                }
            }
        });
    };

    async open(i) {
        await this.setState({open: true, indx: i});

    }

    async onListSelectedImage(it, ix) {
        if (this.state.seleccionados.length <= 25) {
            let i = this.state.seleccionados.indexOf(it);
            if (i > -1) {
                this.state.seleccionados.splice(i, 1);
                this.state.indexSeleccionados[ix] = 0;
            } else {
                this.state.seleccionados.push(it);
                this.state.indexSeleccionados[ix] = 1;
            }
        }
        await this.setState({aux: 0});
    }

    anterior() {
        if (this.state.indx > 0) {
            this.setState({indx: this.state.indx - 1})
        }
    }

    siguiente() {
        if (this.state.indx < this.state.pictures.length - 1) {
            this.setState({indx: this.state.indx + 1})
        }
    }

    async descargar() {
        await this._handleButtonPress();
        await CameraRoll.saveToCameraRoll(this.state.pictures[this.state.indx].url);
        await Alert.alert('Imagen', 'Guardada', [{
            text: 'Entendido'
        }]);
    }

    galeriaRender() {
        let imagen = [];
        for (let i = 0; i < this.state.pictures.length; i = i + 2) {
            imagen.push(
                <View style={[styles.row, {backgroundColor: 'lightgrey'}]} key={i + 'foto'}>
                    <TouchableOpacity onPress={() => this.open(i)}>
                        <Image
                            style={{width: responsiveWidth(50), height: responsiveHeight(30), resizeMode: 'cover'}}
                            defaultSource={require('../../../images/giphy.gif')}
                            source={{
                                uri: this.state.pictures[i].url
                            }}
                        /></TouchableOpacity>
                    {this.state.pictures.length !== i + 1 ?
                        <TouchableOpacity onPress={() =>
                            this.open(i + 1)
                        }><Image
                            style={{width: responsiveWidth(50), height: responsiveHeight(30), resizeMode: 'cover'}}
                            defaultSource={require('../../../images/giphy.gif')}
                            source={{
                                uri: this.state.pictures[i + 1].url
                            }}
                        /></TouchableOpacity>
                        : null}
                    <Modal
                        style={{backgroundColor: 'lightgrey'}}
                        animationType='slide'
                        transparent={false}
                        visible={this.state.open}>
                        {Platform.OS === 'ios' ? (
                            <View
                                style={{backgroundColor: this.state.secondColor, ...ifIphoneX({height: responsiveHeight(5)}, {height: responsiveHeight(3)})}}/>
                        ) : null}
                        {this.state.indx !== null ?
                            <View style={[styles.container, {backgroundColor: '#fff'}]}>
                                <View style={[styles.widthall, {
                                    alignItems: 'flex-end',
                                    marginTop: responsiveHeight(1.5)
                                }]}>
                                    {/*<TouchableOpacity*/}
                                    {/*style={[*/}
                                    {/*styles.modalBigBtn,*/}
                                    {/*{backgroundColor: '#fff', borderColor: this.state.secondColor}*/}
                                    {/*]}*/}
                                    {/*onPress={() => this.setState({open: false})}*/}
                                    {/*>*/}
                                    {/*<Text style={[styles.textW, {*/}
                                    {/*color: this.state.secondColor,*/}
                                    {/*fontSize: responsiveFontSize(1.5)*/}
                                    {/*}]}>*/}
                                    {/*Cerrar*/}
                                    {/*</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                    <Feather
                                        name='x-circle' size={40}
                                        onPress={() => this.setState({open: false})}
                                        color={this.state.secondColor}/>
                                </View>

                                <Image
                                    style={{
                                        width: responsiveWidth(100),
                                        height: responsiveHeight(75),
                                        resizeMode: 'contain',
                                        backgroundColor: '#fff'
                                    }}
                                    defaultSource={require('../../../images/giphy.gif')}
                                    source={{
                                        uri: this.state.pictures[this.state.indx].url
                                    }}
                                />
                                <View style={[styles.row, styles.widthall, {
                                    backgroundColor: '#fff',
                                    marginVertical: 10,
                                    width: responsiveWidth(80),
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }]}>
                                    <Feather
                                        name='arrow-left-circle' size={40}
                                        onPress={() => this.anterior()}
                                        color={this.state.secondColor}
                                    />
                                    <Feather
                                        name='trash-2' size={40}
                                        onPress={() => this.borrar(this.state.pictures[this.state.indx].url)}
                                        color={this.state.secondColor}
                                    />
                                    {Platform.OS === 'ios' ?
                                    <Feather
                                        name='download' size={40}
                                        onPress={() => this.descargar()}
                                        color={this.state.secondColor}
                                    />: null}
                                    <Feather
                                        name='arrow-right-circle' size={40}
                                        onPress={() => this.siguiente()}
                                        color={this.state.secondColor}
                                    />
                                </View>
                            </View> : null}
                    </Modal>
                </View>
            );
        }

        return imagen;

    }

    galeriaPicker() {
        let img = [];
        for (let i = 0; i < this.state.photos.length; i = i + 2) {
            let select = null;
            let selected = null;
            let select2 = null;
            let selected2 = null;
            if (this.state.indexSeleccionados[i] === 1) {
                select = [{backgroundColor: '#113fff'}];
                selected = [{
                    width: responsiveWidth(47),
                    height: responsiveHeight(28),
                    resizeMode: 'cover',
                    opacity: .6
                }];
            } else {
                select = [{padding: 0, backgroundColor: '#fff'}];
                selected = [{
                    width: responsiveWidth(47),
                    height: responsiveHeight(28),
                    resizeMode: 'cover'
                }];
            }
            if (this.state.indexSeleccionados[i + 1] === 1) {
                select2 = [{backgroundColor: '#113fff'}];
                selected2 = [{
                    width: responsiveWidth(47),
                    height: responsiveHeight(28),
                    resizeMode: 'cover',
                    opacity: .6
                }];
            } else {
                select2 = [{padding: 0, backgroundColor: '#fff'}];
                selected2 = [{
                    width: responsiveWidth(47),
                    height: responsiveHeight(28),
                    resizeMode: 'cover'
                }];
            }
            img.push(
                <View
                    key={i + 'photopicker'} style={styles.row}>
                    <TouchableHighlight
                        style={select}
                        onPress={() => this.onListSelectedImage(this.state.photos[i].node.image.uri, i)}>
                        <Image
                            style={selected}
                            defaultSource={require('../../../images/giphy.gif')}
                            source={{uri: this.state.photos[i].node.image.uri}}
                        />
                    </TouchableHighlight>
                    {this.state.photos.length !== i + 1 ?
                        <TouchableHighlight
                            style={select2}
                            onPress={() => this.onListSelectedImage(this.state.photos[i + 1].node.image.uri, i + 1)}>
                            <Image
                                style={selected2}
                                defaultSource={require('../../../images/giphy.gif')}
                                source={{uri: this.state.photos[i + 1].node.image.uri}}
                            />
                        </TouchableHighlight> : null}
                </View>
            );

        }
        return img;
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
                    style={{marginVertical: 10}}>
                    {this.card()}
                    <Modal
                        animationType='slide'
                        transparent={false}
                        visible={this.state.modalVisible1}>
                        {Platform.OS === 'ios' ? (
                            <View
                                style={{backgroundColor: this.state.secondColor, ...ifIphoneX({height: responsiveHeight(5)}, {height: responsiveHeight(3)})}}/>
                        ) : null}
                        <View style={styles.container}>

                            <Text style={styles.main_title}>Agregar fotos al album</Text>
                            <TextInput
                                editable={true}
                                placeholder={'Escriba el título del álbum'}
                                value={this.state.text}
                                maxLength={40}
                                onChangeText={(text) => this.setState({text})}
                                style={[
                                    styles.main_title, styles.btn1,
                                    {
                                        marginTop: 10,
                                        paddingTop: 0,
                                        textAlign: 'center',
                                        color: this.state.thirdColor,
                                        fontSize: responsiveFontSize(3)
                                    }
                                ]}
                            />
                            <View style={{marginTop: responsiveHeight(3)}}>
                            </View>
                            <ScrollView>
                                {this.galeriaPicker()}
                            </ScrollView>
                            {this.state.seleccionados.length === 0 ?
                                (<Text style={{marginTop: 10}}>{'No se han seleccionado imágenes'}</Text>) :
                                (<Text
                                    style={{marginTop: 10}}>{'Se han seleccionado ' + this.state.seleccionados.length + ' imágenes'}</Text>)}

                            <View style={[styles.modalWidth, styles.row]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.setModalVisible1(false)}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.saveImages()}
                                >
                                    <Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    {this.state.agregar === true ?
                        (<View style={[styles.widthall, {alignItems: 'center'}]}>
                                <MaterialIcons name='add-circle' size={35} color={this.state.secondColor}
                                               style={{marginVertical: 10}}
                                               onPress={() => [this.setState({
                                                   modalVisible1: true,
                                                   text: ''
                                               }), this._handleButtonPress()]}/>
                            </View>
                        ) : null
                    }
                    <Modal
                        animationType='slide'
                        transparent={false}
                        visible={this.state.modalVisible}>
                        {Platform.OS === 'ios' ? (
                            <View
                                style={{backgroundColor: this.state.secondColor, ...ifIphoneX({height: responsiveHeight(5)}, {height: responsiveHeight(3)})}}/>
                        ) : null}
                        <View style={[styles.widthall, {
                            backgroundColor: '#fff',
                            alignItems: 'flex-end',
                            marginVertical: responsiveHeight(1.5)
                        }]}>
                            <Feather
                                name='x-circle' size={40}
                                onPress={() => this.setModalVisible(false)}
                                color={this.state.secondColor}/>
                        </View>
                        <View style={[styles.container, {
                            alignItems: 'flex-start',
                            backgroundColor: '#fff'
                        }]}>
                            <ScrollView>
                                {this.galeriaRender()}
                            </ScrollView>
                        </View>
                    </Modal>
                </ScrollView>
            </View>
        );
    }
}
