import React from 'react';
import PropTypes from 'prop-types';
import {AsyncStorage, StatusBar, Text, TouchableHighlight, View} from 'react-native';
import {responsiveFontSize, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';

export default class Table extends React.Component {
    static propTypes = {
        arreglo: PropTypes.array,
        filas: PropTypes.array,
        columnas: PropTypes.array,
    };

    static defaultProps = {
        arreglo: ['100', '99', '98', '97', '96', '95', '94', '93', '92', '91', '90', '89', '88', '87', '86', '85',
            '84', '83', '82', '81', '80', '79', '78', '77', '76', '75', '74', '73', '72', '71', '70', '69', '68', '67',
            '66', '65'],
        filas: ['1','2','3'],
        columnas: ['A', 'B', 'C'],
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedIndexPorcent: -1,
            arreglo: [],
            filas: [],
            columnas: [],
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    //+++++++++++++++++++++++++++++++++++++Grados++++++++++++++++++++++++++++++

    renderColumna(itemColumna, indexGColumna) {
        if (itemColumna !== 'Todos') {
            return (
                <View style={styles.btncitoPors}>
                    <Text style={styles.textGrupo}>{itemColumna}</Text>
                </View>
            );
        }
    }

    renderColumnas() {
        let listCol = [];
        this.props.columnas.forEach((itemColumna, indexColumna) => {
            listCol.push(this.renderColumna(itemColumna, indexColumna));
        });
        return listCol;
    }

    //++++++++++++++++++++++++++Porcentaje+++++++++++++++++++++++++++++++++++++++
    buttonSelected(porcent, indexPorcent) {
        this.setState({selectedIndexPorcent: indexPorcent});
    }

    renderpocent(item, indexPorcent) {
        let btnCito = [
            styles.btncitoPors,
            {
                borderColor: this.state.fourthColor,
                backgroundColor: this.state.fourthColor
            }
        ];
        let texto = [styles.textoN, {fontSize: responsiveFontSize(1.77)}];
        if (this.state.selectedIndexPorcent === indexPorcent) {
            btnCito.push(styles.btncitoPors, {
                borderColor: this.state.secondColor,
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB, {fontSize: responsiveFontSize(1.77)});
        }
        return (
            <View>
                <TouchableHighlight
                    key={indexPorcent}
                    style={btnCito}
                    underlayColor={this.state.secondColor}
                    onPress={() =>this.buttonSelected(item, indexPorcent)}
                >
                    <Text style={texto}>{item}</Text>
                </TouchableHighlight>
            </View>
        );
    }

    renderPorcents(item, filas) {
        let buttons = [];
        let aux = 0;
        let aux1 = 0;
        let aux2 = this.props.columnas.length;
        let aux3 = this.props.arreglo.length;
        this.props.filas.forEach((value, index) => {
            if (index !== 0) {
                aux1 = aux1 + this.props.columnas.length;
                aux2 = aux2 + this.props.columnas.length;
            }
            let buttonsPorcent = [];
            this.props.arreglo.forEach((item, indexPorcent) => {
                buttonsPorcent.push(this.renderpocent(item, indexPorcent));
            });
            let buttons2Porcent = buttonsPorcent;
            buttons2Porcent.splice(aux2, aux3);
            buttons2Porcent.splice(aux, aux1);
            buttons.push(buttons2Porcent);
        });

        if (this.props.filas.length !== filas) {
            return (
                <View style={[styles.row]}>
                    <View style={styles.btncitoPors}>
                        <Text>{this.props.filas[filas]}</Text>
                    </View>
                    {buttons[filas]}
                </View>
            );
        }

    }

    renderPorcentajes() {
        let filasA = [];
        this.props.filas.forEach((item, filas) => {
            filasA.push(this.renderPorcents(item, filas));
        });
        return filasA;
    }

    //acaba++++++++++++++++++++++++++++++++++++++++++
    render() {
        const columna = this.renderColumnas();
        const porcent = this.renderPorcentajes();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor, marginTop: 5}
                    ]}>
                    Porcentaje de entrega de calificaciones
                </Text>
                <View
                    style={{
                        width: responsiveWidth(94),
                        alignItems: 'center',
                        marginTop: 10
                    }}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(2),
                            fontWeight: '700',
                            marginBottom: 5
                        }}>
                        Cuadro informativo
                    </Text>
                </View>
                <View style={{width: responsiveWidth(94), alignItems: 'center'}}>
                    <View>
                        <View style={[styles.row, {alignItems: 'center'}]}><View style={styles.btncitoPors}><Text
                            style={styles.textGrupo}>T</Text></View>{columna}
                        </View>
                        {porcent}
                    </View>
                </View>
            </View>
        );
    }
}
