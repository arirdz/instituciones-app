import React from "react";
import {
    AsyncStorage,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import StepIndicator from "rn-step-indicator";

const labels_estForm = [
    "Agendar \ncita",
    "Iniciar \nregistro",
    "Registrar \nhijos",
    "Elegir \nEscuela",
    "Solicitar \npreinscripción",
    "Subir \ndocumentos",
    "Hacer \npago",
    "Examen \ndiagnóstico",
    "Entrega \nde Resultado",
    "Resolución \nde ingreso",
    "Finalizar"
];

export default class preinscrip extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPosition: 0
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    preinscrip() {
        return (
            <View>
                {this.state.currentPosition === 0 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 1</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 1 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 2</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 2 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 3</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 3 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,

                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 4</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 4 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 5</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 5 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 6</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 6 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 7</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 7 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 8</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 8 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 9</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 9 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 10</Text>
                    </View>
                ) : null}
                {this.state.currentPosition === 10 ? (
                    <View
                        style={[
                            styles.infoReinsc,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor,
                                width: responsiveWidth(65),
                                height: responsiveHeight(58),
                                alignItems: "center",
                                marginLeft: 10
                            }
                        ]}>
                        <Text>paso 11</Text>
                    </View>
                ) : null}
            </View>
        );
    }

    pasoSig() {
        if (this.state.currentPosition <= 9) {
            this.setState({
                currentPosition: this.state.currentPosition + 1,
                temporal: false,
                calidad: false
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition + 0,
                temporal: false,
                calidad: false
            });
        }
    }

    pasoAnt() {
        if (this.state.currentPosition <= 0) {
            this.setState({
                currentPosition: 0,
                temporal: false,
                calidad: false
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition - 1,
                temporal: false,
                calidad: false
            });
        }
    }

    render() {
        const preIns = this.preinscrip();
        let customStyles = {
            stepIndicatorSize: 20,
            currentStepIndicatorSize: 25,
            separatorStrokeWidth: 1,
            currentStepStrokeWidth: 2,
            stepStrokeCurrentColor: "#777777",
            separatorFinishedColor: "#777777",
            separatorUnFinishedColor: "#aaaaaa",
            stepIndicatorFinishedColor: "#777777",
            stepIndicatorUnFinishedColor: "#aaaaaa",
            stepIndicatorCurrentColor: "#ffffff",
            stepIndicatorLabelFontSize: responsiveFontSize(1.3),
            currentStepIndicatorLabelFontSize: responsiveFontSize(1.3),
            stepIndicatorLabelCurrentColor: "#000000",
            stepIndicatorLabelFinishedColor: "#ffffff",
            stepIndicatorLabelUnFinishedColor: "rgba(255,255,255,0.5)",
            labelColor: "#666666",
            labelSize: responsiveFontSize(1.3),
            currentStepLabelColor: "#777777"
        };
        let index = 0;
        const data = [
            {id: "sReinscrip"},
            {id: "estSimp"},
            {id: "estForm"},
            {id: "crBeca"},
            {id: "sBeca"}
        ];
        return (
            <View style={[styles.container]}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <Text style={[styles.main_title, {marginTop: 5}]}>
                    Este es el procedimiento a seguir:
                </Text>
                <View
                    style={{
                        flexDirection: "row",
                        width: responsiveWidth(94),
                        marginTop: 5
                    }}>
                    <View>
                        <StepIndicator
                            customStyles={customStyles}
                            stepCount={11}
                            direction="vertical"
                            currentPosition={this.state.currentPosition}
                            labels={labels_estForm}
                        />
                    </View>
                    <View>{preIns}</View>
                </View>
                <View style={[styles.ButtonsRow]}>
                    <TouchableOpacity
                        style={[
                            styles.middleButton,
                            {
                                backgroundColor: this.state.secondColor,
                                borderColor: this.state.secondColor
                            }
                        ]}
                        onPress={() => this.pasoAnt()}>
                        <Text style={styles.textButton}>Paso anterior</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.middleButton,
                            {
                                backgroundColor: this.state.secondColor,
                                borderColor: this.state.secondColor
                            }
                        ]}
                        onPress={() => this.pasoSig()}>
                        <Text style={styles.textButton}>Siguiente paso</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
