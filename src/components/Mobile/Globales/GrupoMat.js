import React from "react";
import PropTypes from "prop-types";
import {AsyncStorage, Text, TouchableHighlight, View} from "react-native";
import styles from "../../styles";

export default class GrupoMat extends React.Component {
    static propTypes = {
        urlGrupo: PropTypes.string,
        onListItemPressedGrupos: PropTypes.func.isRequired,
        todos: PropTypes.string
    };

    static defaultProps = {
        urlGrupo: "/api/get/grupos",
        todos: "0"
    };

    constructor(props) {
        super(props);
        this.state = {
            checkBoxBtns: [],
            checkBoxBtnsIndex: [],
            grupo: "A",
            uri: "",
            token: "",
            grupos: [],
            todos: "0"
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getGrupos();
    }

    async componentWillReceiveProps(nextProps) {
        await this.setState({
            urlGrupo: nextProps.urlGrupo
        });
        await this.getGrupos();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    // ++++++++++++++++++++++++++++++++++ GRUPOS ++++++++++++++++++++++++++++++++++++++++++++

    async getGrupos() {
        let grupopicker = await fetch(this.state.uri + this.props.urlGrupo, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let grupospicker = await grupopicker.json();
        await this.setState({grupos: grupospicker});
        this.state.grupos.forEach(() => {
            this.state.checkBoxBtnsIndex.push(0);
        });
    }

    async onListItemPressedGrupo(itemGrupo, indexGrupo) {
        await this.props.onListItemPressedGrupos(indexGrupo, itemGrupo);
        await this.setState({selectedIndexGrupo: indexGrupo, grupo: itemGrupo});
        let i = this.state.checkBoxBtns.indexOf(itemGrupo);
        if (i > -1) {
            this.state.checkBoxBtns.splice(i, 1);
            this.state.checkBoxBtnsIndex[indexGrupo] = 0;
        } else {
            this.state.checkBoxBtns.push(itemGrupo);
            this.state.checkBoxBtnsIndex[indexGrupo] = 1;
        }
    }

    renderGrupo(itemGrupo, indexGrupo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn4_lT,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.checkBoxBtnsIndex[indexGrupo] === 1) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexGrupo}
                style={smallButtonStyles}
                underlayColor={this.state.secondColor}
                onPress={() => this.onListItemPressedGrupo(indexGrupo, itemGrupo.grupo)}
            >
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderGrupo1(itemGrupo, indexGrupo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn4_lT,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];
        if (this.state.selectedIndexGrupo === indexGrupo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexGrupo}
                style={smallButtonStyles}
                underlayColor={this.state.secondColor}
                onPress={() => this.onListItemPressedGrupo(indexGrupo, itemGrupo)}
            >
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            if (this.state.indexSelected === 0) {
                buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
            } else {
                buttonsGrupos.push(this.renderGrupo1(itemGrupo, indexGrupo));
            }
        });
        return buttonsGrupos;
    }

    render() {
        const grupos = this.renderGrupos();
        return (
            <View>
                <View style={styles.buttonsRow}>{grupos}</View>
                {/* {this.props.todos === "1" ? (
          this.state.grupos.length > 5 ? (
            <View style={styles.buttonsRow}>{grupos1}</View>
          ) : null
        ) : this.state.grupos.length - 1 > 5 ? (
          <View style={styles.buttonsRow}>{grupos}</View>
        ) : null} */}
            </View>
        );
    }
}
