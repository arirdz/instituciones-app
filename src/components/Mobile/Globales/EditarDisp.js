import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	TouchableOpacity,
	ScrollView, TouchableHighlight
} from 'react-native';
import {
	responsiveFontSize, responsiveWidth, responsiveHeight
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Switch from 'react-native-switch-pro';
import Spinner from 'react-native-loading-spinner-overlay';
import MultiBotonRow from './MultiBotonRow';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class EditarDisp extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			visible: false,
			botonSelected: '',
			indexSelected: 0,
			id: [],
			hideHora: [true, true, true, true, true],
			elDia: [
				{nombre: 'Lunes'},
				{nombre: 'Martes'},
				{nombre: 'Miércoles'},
				{nombre: 'Jueves'},
				{nombre: 'Viernes'}
			],
			laHora: [],
			diaAten: [false, false, false, false, false],
			horario: [],
			elHorario: [],
			lasHoras: [],
			lasHoras2: [],
			lasHoras3: [],
			aux: 0
		};
	}


	async componentWillMount() {
		await this.getURL();
		await this.getUserdata();
		await this.getHorasAtencion();
		await this.getHoras();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async saveHorario() {
		Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
			{text: 'No'},
			{
				text: 'Sí',
				onPress: () => [this.borrarDisponibilidad()]
			}
		]);
	}

	async botonSelected(indexSelected, itemSelected) {
		await
			this.setState({
				botonSelected: itemSelected,
				indexSelected: indexSelected
			});
	}


	async borrarDisponibilidad() {
		await this._changeWheelState();
		let formData = new FormData();
		formData.append(
			'new',
			JSON.stringify({
				id_usuario: this.state.id
			})
		);
		await fetch(this.state.uri + '/api/borrar/horario/atencion', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Disponibilidad)',
				[{
					text: 'Entendido', onPress: () => [Actions.drawer(), this.setState({visible: false})]
				}]);
		} else {
			await this.requestMultipart();
		}
		await this.setState({aux: 0});
	}

	async requestMultipart() {
		let formData = new FormData();
		for (let i = 0; i < this.state.diaAten.length; i++) {
			if (this.state.diaAten[i] === true) {
				for (let j = 0; j < this.state.laHora.length; j++) {
					if (this.state.horario[i + '' + j] === true) {
						formData.append(
							'new',
							JSON.stringify({
								id_usuario: this.state.id,
								id_horario_atencion: j + 1,
								dia_atencion: i + 1
							})
						);
						await fetch(this.state.uri + '/api/post/horario/atencion', {
							method: 'POST',
							headers: {
								'Content-Type': 'multipart/form-data',
								Authorization: 'Bearer ' + this.state.token
							},
							body: formData
						}).then(res => res.json())
							.then(responseJson => {
								this.setState({elRequest: responseJson});
							});
					}
				}
			}
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al  guardar el horario',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' al tratar de guardar horaio de disponibilidad si el error continua pónganse en contacto con soporte'
				+
				'(Disponibilidad)',
				[{
					text: 'Entendido', onPress: () => [Actions.drawer(), this.setState({visible: false})]
				}]);
		} else {
			Alert.alert('Horarios disponibles guardados',
				'Se ha guardado el horario de disponibilidad con éxito',
				[{
					text: 'Entendido',
					onPress: () => [this.getHorasAtencion(), this.setState({visible: false})]
				}]);
		}
		await this.setState({aux: 0});
	}


	async getUserdata() {
		await fetch(this.state.uri + '/api/user/data', {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({data: responseJson});
					this.setState({id: this.state.data.user_id})
				}
			});
	}

	async getHoras() {
		let switchHora = await fetch(this.state.uri + '/api/get/horarios',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			});
		let switchHoras = await switchHora.json();
		await this.setState({laHora: switchHoras});
	}

	async getHorasAtencion() {
		await fetch(
			this.state.uri + '/api/get/horarios/atencion', {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar diponibilidad', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar la disponibilidad si el error continua pónganse en contacto con soporte (cod. 1)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({elHorario: responseJson});
					this.state.elHorario.forEach((item, i) => {
						this.state.lasHoras.push(item.horas);
					});

					for (let i = 0; i < this.state.lasHoras.length; i++) {
						this.state.lasHoras2.push(this.state.lasHoras[i].length.toString());
					}

					for (let j = 0; j < this.state.lasHoras2.length; j++) {
						for (let h = 0; h < this.state.lasHoras2[j]; h++) {
							this.state.lasHoras3[j + '' + h] = this.state.elHorario[j].horas[h].horas2;
						}
					}

					for (let i = 0; i < this.state.elHorario.length; i++) {
						let dia = this.state.elHorario[i].dia - 1;
						this.state.diaAten[dia] = true;
						this.state.hideHora[dia] = false;
						for (let j = 0; j < this.state.elHorario[i].horas.length; j++) {
							let hora = this.state.elHorario[i].horas[j].id_horario_atencion - 1;
							this.state.horario[dia + '' + hora] = true;
						}
					}
				}
			});

		await this.setState({aux: 0});
	}


	async siDia(i) {
		if (this.state.diaAten[i] === false) {
			this.state.diaAten[i] = true;
		} else {
			this.state.diaAten[i] = false;
			this.setState({horario: []})
		}
		this.setState({aux: 0});
	}

	async siHora(i, j) {
		if (this.state.horario[i + '' + j] === true) {
			this.state.horario[i + '' + j] = false;
		} else {
			this.state.horario[i + '' + j] = true;
		}
		this.setState({aux: 0});
	}

	async hideHrs(index) {
		if (this.state.hideHora[index] === true) {
			this.state.hideHora[index] = false;
		} else {
			this.state.hideHora[index] = true;
		}
		await this.setState({aux: 0});
	}


	renderDia(index) {
		const horasss = this.renderHoras(index);
		return (
			<View>
				<View
					style={[
						styles.row_PC,
						{
							borderColor: this.state.fourthColor,
							paddingLeft: 8,
							paddingRight: 8
						}
					]}
				>
					<View style={[styles.btn2]}>
						<Text style={{fontSize: responsiveFontSize(2)}}>
							{this.state.elDia[index].nombre}
						</Text>
					</View>
					<View style={[styles.btn_2, {alignItems: 'flex-end'}]}>
						<Switch
							value={this.state.diaAten[index]}
							onSyncPress={() => this.siDia(index)}
						/>
					</View>
					{this.state.diaAten[index] === true ? (
						<Ionicons
							name='ios-arrow-down'
							size={19}
							color='black'
							onPress={() => this.hideHrs(index)}
						/>
					) : null}
				</View>
				{this.state.diaAten[index] === true ? horasss : null}
			</View>
		);
	}

	renderDias() {
		let btnDia = [];
		for (let i = 0; i < this.state.elDia.length; i++) {
			btnDia.push(this.renderDia(i));
		}
		return btnDia;
	}

	renderHora(index, j) {
		return (
			<View>
				{this.state.hideHora[index] === true ? (
					<View sytle={styles.container}>
						<View
							style={[
								styles.widthall,
								{alignItems: 'center'}
							]}
						>
							<View
								style={[styles.rowsCalif, styles.modalWidthEsp, styles.btn10, {paddingVertical: 10}]}
							>
								<Text>
									{this.state.laHora[j].tipo_hora === '1' ? 'Clase' : 'Receso'}
								</Text>
								<Text>{this.state.laHora[j].horas}</Text>
								<Switch
									value={this.state.horario[index + '' + j]}
									onSyncPress={() => this.siHora(index, j)}
								/>
							</View>
						</View>
					</View>
				) : null}
			</View>
		);
	}

	renderHoras(index) {
		let btnHora = [];
		for (let j = 0; j < this.state.laHora.length; ++j) {
			btnHora.push(this.renderHora(index, j));
		}
		return btnHora;
	}

	laHora(i, index) {
		return (
			<Text
				style={[{textAlign: 'center', fontSize: responsiveFontSize(1.5), marginHorizontal: 1}]}
			>
				{moment(this.state.lasHoras3[index + '' + i].inicio, 'HH:mm:ss').format('HH:mm')},{' '}
			</Text>
		);
	}

	lasHoras(index) {
		let btnLaHora = [];
		for (let i = 0; i < this.state.lasHoras2[index]; ++i) {
			btnLaHora.push(this.laHora(i, index));
		}
		return btnLaHora;
	}

	disponible(itemDisp, indexDisp) {
		let nombreDia = '';
		if (itemDisp.dia === '1') {
			nombreDia = 'Lunes';
		}
		if (itemDisp.dia === '2') {
			nombreDia = 'Martes';
		}
		if (itemDisp.dia === '3') {
			nombreDia = 'Miércoles';
		}
		if (itemDisp.dia === '4') {
			nombreDia = 'Jueves';
		}
		if (itemDisp.dia === '5') {
			nombreDia = 'Viernes';
		}
		return (
			<View
				style={[
					styles.widthall,
					styles.row,
					{
						paddingVertical: 8,
						paddingHorizontal: 15,
						borderTopWidth: 1
					}
				]}
			>
				<Text
					style={[
						styles.textButton, styles.btn_2,
						{color: 'black', textAlign: 'left'}
					]}
				>
					{nombreDia}
				</Text>
				<View style={[styles.btn3_5, {flexDirection: 'row', justifyContent: 'flex-end'}]}>
					{this.lasHoras(indexDisp)}
				</View>
			</View>
		);
	}

	disponibles() {
		let btnDispon = [];
		this.state.elHorario.forEach((itemDisp, indexDisp) => {
			btnDispon.push(this.disponible(itemDisp, indexDisp))
		});
		return btnDispon;
	}

	render() {
		let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnCal = [styles.btn8, {
			backgroundColor: this.state.fourthColor,
			justifyContent: 'center',
			alignItems: 'center',
			borderRadius: 6,
			height: responsiveHeight(4)
		}];
		if (this.state.indexSelected === 1) {
			btnCal.push(styles.btn8, {backgroundColor: this.state.thirdColor, borderRadius: 6});
			texto.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		let texto1 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnTem = [styles.btn8, {
			backgroundColor: this.state.fourthColor,
			justifyContent: 'center',
			alignItems: 'center',
			borderRadius: 6,
			height: responsiveHeight(4)
		}];
		if (this.state.indexSelected === 0) {
			btnTem.push(styles.btn8, {backgroundColor: this.state.thirdColor, borderRadius: 6});
			texto1.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Guardando...'/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione una opción a realizar
				</Text>
				<View style={[styles.widthall, {alignItems: 'center'}]}>
					<View
						style={[styles.row, {
							width: responsiveWidth(82), marginTop: 10
						}]}>
						<TouchableHighlight
							underlayColor={'transparent'}
							style={btnTem}
							onPress={() => this.botonSelected(0)}>
							<Text style={texto1}>Disponibilidad actual</Text>
						</TouchableHighlight>
						<TouchableHighlight
							underlayColor={'transparent'}
							style={btnCal}
							onPress={() => this.botonSelected(1)}>
							<Text style={texto}>Editar Disponibilidad</Text>
						</TouchableHighlight>
					</View>
				</View>
				{this.state.indexSelected === 1 ? (
					<View style={styles.container}>
						<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
							Seleccione día y hora
						</Text>
						<ScrollView style={{marginTop: 5}}>
							{this.renderDias()}
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.bigButton,
								{backgroundColor: this.state.secondColor}
							]}
							onPress={() => this.saveHorario()}
						>
							{this.state.elHorario.length === 0 ? (
								<Text style={styles.textButton}>Guardar</Text>
							) : (<Text style={styles.textButton}>Guardar edición</Text>)}
						</TouchableOpacity>
					</View>
				) : (
					<View>
						{this.state.elHorario.length === 0 ? (
							<View style={[styles.widthall, {fontStyle: 'italic', alignItems: 'center', marginTop:10}]}>
								<Text style={[styles.modalWidth, {fontStyle: 'italic', textAlign: 'center'}]}>
									En este momento usted está disponible todos los días y en todas las horas
									escolares. Para
									cambiar esta configuración vaya a 'Editar diponibilidad' y defina su horario
									personalizado de
									disponibilidad.
								</Text>
							</View>
						) : null}
						<Text style={[styles.main_title, {color: this.state.thirdColor}]}>Horarios establecidos</Text>

						<View style={{marginTop: 3, borderBottomWidth: 1}}>
							{this.disponibles()}
						</View>
					</View>
				)}
			</View>
		);
	}
}
