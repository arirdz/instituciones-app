import React from "react";
import PropTypes from "prop-types";
import {AsyncStorage, Text, TouchableHighlight, View} from "react-native";
import {responsiveHeight} from "react-native-responsive-dimensions";
import styles from "../../styles";

export default class Grupo extends React.Component {
    static propTypes = {
        urlGrupo: PropTypes.string,
        onListItemPressedGrupos: PropTypes.func.isRequired,
        todos: PropTypes.string
    };

    static defaultProps = {
        urlGrupo: "/api/get/grupos",
        todos: "0"
    };

    constructor(props) {
        super(props);
        this.state = {
            grupo: "A",
            uri: "",
            token: "",
            grupos: [],
            todos: "0"
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getGrupos();
    }

    async componentWillReceiveProps(nextProps) {
        await this.setState({
            urlGrupo: nextProps.urlGrupo
        });
        await this.getGrupos();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    // ++++++++++++++++++++++++++++++++++ GRUPOS ++++++++++++++++++++++++++++++++++++++++++++

    async getGrupos() {
        let grupopicker = await fetch(this.state.uri + this.props.urlGrupo, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let grupospicker = await grupopicker.json();
        await this.setState({grupos: grupospicker});
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.props.onListItemPressedGrupos(indexGrupo, grupo);
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    renderGrupos1() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo1(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    renderGrupo1(itemGrupo, indexGrupo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexGrupos === indexGrupo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <View>
                {this.props.todos === "1" && indexGrupo >= 5 ? (
                    <TouchableHighlight
                        key={indexGrupo}
                        underlayColor={this.state.secondColor}
                        style={smallButtonStyles}
                        onPress={() =>
                            this.onListItemPressedGrupos(indexGrupo, itemGrupo.grupo)
                        }
                    >
                        <View style={styles.listItem}>
                            <Text style={texto}>{itemGrupo.grupo}</Text>
                        </View>
                    </TouchableHighlight>
                ) : itemGrupo.grupo !== "Todos" && indexGrupo >= 5 ? (
                    <TouchableHighlight
                        key={indexGrupo}
                        underlayColor={this.state.secondColor}
                        style={smallButtonStyles}
                        onPress={() =>
                            this.onListItemPressedGrupos(indexGrupo, itemGrupo.grupo)
                        }
                    >
                        <View style={styles.listItem}>
                            <Text style={texto}>{itemGrupo.grupo}</Text>
                        </View>
                    </TouchableHighlight>
                ) : null}
            </View>
        );
    }

    renderGrupo(itemGrupo, indexGrupo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexGrupos === indexGrupo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <View>
                {this.props.todos === "1" && indexGrupo < 5 ? (
                    <TouchableHighlight
                        key={indexGrupo}
                        underlayColor={this.state.secondColor}
                        style={smallButtonStyles}
                        onPress={() =>
                            this.onListItemPressedGrupos(indexGrupo, itemGrupo.grupo)
                        }
                    >
                        <View style={styles.listItem}>
                            <Text style={texto}>{itemGrupo.grupo}</Text>
                        </View>
                    </TouchableHighlight>
                ) : itemGrupo.grupo !== "Todos" && indexGrupo < 5 ? (
                    <TouchableHighlight
                        key={indexGrupo}
                        underlayColor={this.state.secondColor}
                        style={smallButtonStyles}
                        onPress={() =>
                            this.onListItemPressedGrupos(indexGrupo, itemGrupo.grupo)
                        }
                    >
                        <View style={styles.listItem}>
                            <Text style={texto}>{itemGrupo.grupo}</Text>
                        </View>
                    </TouchableHighlight>
                ) : null}
            </View>
        );
    }

    render() {
        const grupos = this.renderGrupos();
        const grupos1 = this.renderGrupos1();
        let altura = [{height: responsiveHeight(15)}];
        if (this.props.todos === "1") {
            if (this.state.grupos.length > 5) {
                altura = [{height: responsiveHeight(21)}];
            }
        } else {
            if (this.state.grupos.length - 1 > 5) {
                altura = [{height: responsiveHeight(21)}];
            }
        }
        return (
            <View style={altura}>
                <View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el grupo
                    </Text>
                </View>
                <Text style={styles.subTitulo}>Grupo</Text>
                <View style={styles.buttonsRow}>{grupos}</View>
                {this.props.todos === "1" ? (
                    this.state.grupos.length > 5 ? (
                        <View style={styles.buttonsRow}>{grupos1}</View>
                    ) : null
                ) : this.state.grupos.length - 1 > 5 ? (
                    <View style={styles.buttonsRow}>{grupos1}</View>
                ) : null}
            </View>
        );
    }
}
