import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
} from 'react-native';
import {
    responsiveFontSize,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';

export default class EstadConduct extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: '',
            solicitudes: [],
            botonSelected: 'Aprobados',
            botonSelected1: 'Academicos',
            selectedIndexGrados: -1,
            grado: '',
            selectedIndexGrupos: -1,
            grupo: '',
            selectedIndexPeriodo: -1,
            elPeriodo: ''
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text
                    style={{
                        fontWeight: '700',
                        fontSize: responsiveFontSize(2),
                        marginTop: 10,
                        marginBottom: 8
                    }}>
                    Reportes
                </Text>
                <View
                    style={[
                        styles.row_v3,
                        styles.materiaCord,
                        {borderColor: this.state.secondColor}
                    ]}>
                    <View style={styles.porcentNum}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(5)
                            }}>
                            240
                        </Text>
                    </View>

                    <View
                        style={[
                            styles.porcentNum,
                            {borderLeftWidth: 1, borderColor: this.state.secondColor}
                        ]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(5)
                            }}>
                            80%
                        </Text>
                    </View>
                </View>

                <View style={{marginTop: 15}}>
                    <View
                        style={[
                            styles.row,
                            {width: responsiveWidth(85), marginLeft: 29}
                        ]}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text> </Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={[styles.centeredTxt, {left: 15}]}>Alumnos</Text>
                        </View>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Faltas</Text>
                        </View>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Promedio</Text>
                        </View>
                    </View>

                    <View style={[styles.row, {width: responsiveWidth(90)}]}>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>0{'\n'}Reportes</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.row, {width: responsiveWidth(90)}]}>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>1-2{'\n'}Reportes</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={[styles.row, {width: responsiveWidth(90)}]}>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>3-4 {'\n'}Reportes</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={[styles.row, {width: responsiveWidth(90)}]}>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>4-5{'\n'}Reportes</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={[styles.row, {width: responsiveWidth(90)}]}>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>6-7{'\n'}Reportes</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.row, {width: responsiveWidth(90)}]}>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>8-9{'\n'}Reportes</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={[styles.row, {width: responsiveWidth(90)}]}>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Totales</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text style={{color: 'white'}}>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {
                                            borderColor: '#fff'
                                        }
                                    ]}>
                                    <Text style={{color: 'white'}}>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <Text style={{color: 'white'}}>666</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <Text style={{color: 'white'}}>666</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <Text style={[styles.main_title, {marginTop: 10}]}>
                    Alumnos con mayor cantidad de reportes en la seleccion actual
                </Text>
                <View style={styles.campoTablaG}>
                    <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                        <View>
                            <Text style={{fontWeight: '700'}}>Alumno 1</Text>
                        </View>
                        <View>
                            <Text style={{fontWeight: '700'}}>Reportes</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
