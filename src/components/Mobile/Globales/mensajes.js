import React from 'react';
import {Alert, AsyncStorage, View} from 'react-native';

import {GiftedChat, Bubble, MessageText, Time, Avatar} from 'react-native-gifted-chat';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class mensajes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            data: [],
            message: [],
            idUser: []
        };
        this.onSend = this.onSend.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
        this.renderMessageText = this.renderMessageText.bind(this);
        this.renderTime = this.renderTime.bind(this);
        this.renderAvatar = this.renderAvatar.bind(this);
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let mainColor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        await this.setState({
            uri: uri,
            token: token,
            mainColor: mainColor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getUserdata();
        await this.getFeed();
    }

    async componentWillMount() {
        await this.getURL();
    }

    componentDidMount() {
        this._interval = setInterval(() => {
            this.getFeed();
        }, 1500);

    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }


    async getFeed() {
        let l = this.state.messages.length;

        let request = await fetch(
            this.state.uri +
            '/api/mensajes/gcbuid/' +
            (this.props.user_id === undefined
                ? this.props.data.id
                : this.props.data.role === 'Alumno'
                    ? this.props.data.padre_id
                    : this.props.data.id),
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let data = await request.json();
        if (data.length !== 0 && data.length !== l) {
            let dataMapeada = data.map(mensaje => {
                return {
                    _id: mensaje.id,
                    text: mensaje.message,
                    createdAt: mensaje.created_at,
                    user: {
                        _id: mensaje.sender.id,
                        name: mensaje.sender.name,
                        // avatar: mensaje.sender.id === 56 ? 'https://controlescolar.pro/images/logotipo.png' : 'https://www.shareicon.net/data/512x512/2016/07/08/117367_logo_512x512.png',
                    }
                };
            });
            this.setState({messages: dataMapeada.reverse()});

        }
    }

    async getUserdata() {
        let request = await fetch(this.state.uri + '/api/user/data', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error ' +
                        // responseJson.error.status_code +
                        ' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({idUser: responseJson});
                }
            });
    }

    async onSend(messages = []) {
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, messages),
            };
        });
        await fetch(this.state.uri + '/api/mensajes/send/' +
            this.props.data.id,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                },
                body: JSON.stringify({mensaje: messages[0].text})
            })
            .then(res => res.json())
            .then(responseJson => {
            });
    }

    renderBubble(props) {
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    left: {
                        backgroundColor: this.state.fourthColor
                    },
                    right: {
                        backgroundColor: this.state.secondColor,
                    }
                }}
                renderMessageText={this.renderMessageText}
                renderTime={this.renderTime}

            />
        );
    }

    renderMessageText(props) {
        return (
            <MessageText
                {...props}
                textStyle={{
                    left: {
                        color: '#000'
                    },
                    right: {
                        color: '#fff'
                    }
                }}
            />
        );
    }

    renderTime(props) {
        return (
            <Time
                {...props}
                textStyle={{
                    left: {
                        color: '#fff'
                    },
                    right: {
                        color: '#fff'
                    }
                }}
            />
        );
    }

    renderAvatar(props) {
        return (
            <Avatar
                {...props}
                containerStyle={{
                    right: {
                        marginLeft: -1
                    },
                    left: {
                        marginRight: -1
                    }
                }}
                imageStyle={{
                    right: {
                        resizeMode: 'contain',
                        backgroundColor: this.state.mainColor
                    },
                    left: {
                        resizeMode: 'contain',
                        backgroundColor: this.state.thirdColor
                    }
                }}
            />
        );
    }


    render() {
        return (
            <View style={[{backgroundColor: '#fff', flex: 1, ...ifIphoneX({marginBottom: 40})}]}>
                <GiftedChat
                    messages={this.state.messages}
                    placeholder={'Escriba su mensaje'}
                    onSend={this.onSend}
                    isAnimated={true}
                    locale={'es'}
                    timeFormat={'LTS'}
                    renderAvatar={this.renderAvatar}
                    user={{
                        _id: this.state.idUser.user_id,
                    }}
                    renderBubble={this.renderBubble}
                    messageText={this.renderMessageText}
                />
            </View>
        );
    }
}
