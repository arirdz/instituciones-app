import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import Noticias from './NewsCards';
import ModalSelector from 'react-native-modal-selector';
import Hijos from './hijos';
import styles from '../../styles';
import MultiBotonRow from './MultiBotonRow';
import VerEnterados from './verEnterados';
import NuevaPublicacion from './NuevaPublicacion';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions'
import {Actions} from "react-native-router-flux";
import TimeAgo from "react-native-timeago";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            textInputValue: '',
            puesto: '',
            grado: 'Todos',
            grupo: 'Todos',
            cososPicker: [],
            selectedIndex: null,
            ninosPicker: [],
            mes: moment().month() + 1,
            botonSelected: 'Noticias',
            elHijo: ''
        };
        // this.getFeed = this.getFeed.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    _keyExtractor = (item) => item.id.toString();

    async componentWillMount() {
        await this.getURL();
        await this.getMeses();
        if (this.state.admin === 'Padre' || this.state.admin === 'Alumno') {
            await this.showPadreNoticias();
        } else if ((this.state.admin === 'Admin') || (this.state.admin === 'Maestro')) {
            await this.getFeed();
        }
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let token = await AsyncStorage.getItem('token');
        let role = await AsyncStorage.getItem('role');
        let puesto = await AsyncStorage.getItem('puesto');
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            token: token,
            admin: role,
            puesto: puesto
        });
    }

    async onChange(option) {
        this.setState({data: []});
        await this.setState({mes: option.mes});
        if ((this.state.admin === 'Admin') || (this.state.admin === 'Maestro')) {
            await this.getFeed();
        } else {
            await this.showPadreNoticias();
        }
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async getFeed() {
        this.setState({data: []});
        let request = await fetch(this.state.uri + '/api/feed/show/' + this.state.mes, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let data = await request.json();
        this.setState({data: data});
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected, indexSelected: indexSelected
        });
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index, selGrad: grado, selGrup: grupo, elHijo: id
        });
        await AsyncStorage.setItem('hijo', id.toString());
        await this.showPadreNoticias();
    }

    renderView() {
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }
        return (<View>
            {this.state.botonSelected === 'Noticias' ? (
                <View style={{...ifIphoneX({height: responsiveHeight(75)}, {height: responsiveHeight(77)})}}>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Elija el período a consultar
                    </Text>
                    <ModalSelector
                        data={data}
                        selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
                        cancelText='Cancelar'
                        optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
                        selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
                        initValue={mess}
                        onChange={option => this.onChange(option)}
                    />
                    <Text
                        style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Estas son las últimas noticias:
                    </Text>
                    {this.state.data !== null ? (
                        <ScrollView>
                            {this.renderCards()}
                        </ScrollView>
                    ) : null}
                </View>) : null}
            {this.state.botonSelected === 'Publicar\nalgo nuevo' ? (<NuevaPublicacion/>) : null}
            {this.state.botonSelected === 'Revisar\nenterados' ? (<VerEnterados/>) : null}
        </View>);
    }

    isAdmin() {
        const laview = this.renderView();
        return (<View style={styles.container}>
            {this.state.puesto !== 'Prefecto' && this.state.puesto !== 'Cordisi' ? (<MultiBotonRow
                itemBtns={['Noticias', 'Publicar\nalgo nuevo', 'Revisar\nenterados']}
                onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
                cantidad={3}
            />) : null}
            {laview}
        </View>);
    }

    async showPadreNoticias() {
        this.setState({data: []});
        await fetch(this.state.uri + '/api/feed/show/' + this.state.mes + '/' + this.state.selGrad + '/' + this.state.selGrup + '/' + this.state.elHijo, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    async showAlumnoNoticias() {
        let request = await fetch(this.state.uri + '/api/feed/show/' + this.state.mes + '/' + this.state.selGrad + '/' + this.state.selGrup + '/' + this.state.elHijo, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let data = await request.json();
        this.setState({data: data});
    }

    isNotPapa() {
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }

        return (<View style={styles.container}>
            {this.state.admin === 'Padre' ? (<View>
                <Hijos
                    onSelectedChamaco={(index, grado, grupo, id) => this.onListItemPressed(index, grado, grupo, id)}
                />
            </View>) : null}
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                Elija el periodo a consultar
            </Text>
            <ModalSelector
                data={data}
                selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
                cancelText='Cancelar'
                optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
                selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
                initValue={mess}
                onChange={option => this.onChange(option)}/>
            <Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 5}]}>
                Seleccione una noticia
            </Text>
            {this.state.data !== null
                ? this.state.data.length !== 0
                    ? (
                        <ScrollView>
                            {this.renderCards()}
                        </ScrollView>
                    )
                    : null
                : null}
        </View>);
    }

    //+++++++++++++CARDS+++++++++++++
    async getEnterado() {
        let id = this.props.id;
        let url = this.state.uri + '/api/feed/view/enterados/' + id;
        if (this.state.role === 'Padre') {
            url = this.state.uri + '/api/feed/view/enterados/' + id + '/' + this.state.hijo;
        }
        await fetch(url, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Card Noticia)', [{
                        text: 'Entendido', onPress: () => Actions.drawer()
                    }]);
                } else {
                    this.setState({enterados: responseJson});
                }
            });
    }

    isEnterado() {
        return (<View
            style={[styles.enteradoBtnOutside, {backgroundColor: this.state.mainColor, alignItems: 'center'}]}>
            <Text style={styles.textEnterado}>Leído</Text>
        </View>);
    }

    noEnterado() {
        return (<View
            style={[styles.enteradoBtnOutside, {backgroundColor: this.state.secondColor}]}>
            <Text style={styles.textEnterado}>Por leer</Text>
        </View>);
    }

    delete(id) {
        Alert.alert('Atención', 'Al borrar la noticia ya no se podrá recuperar, ¿está seguro de continuar?', [{
            text: 'Entendido', onPress: () => this.deleteConfirm(id)
        }, {
            text: 'Cancelar'
        }]);
    }

    async deleteConfirm(id) {
        await fetch(this.state.uri + '/api/eliminar/noticia/' + id, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error contacte a soporte', [{
                        text: 'Entendido'
                    }]);
                } else {
                    Alert.alert('Enhorabuena', 'Noticia Borrada', [{
                        text: 'Entendido', onPress: () => this.getFeed()
                    }]);
                }
            });
    }

    edit(it) {
        Alert.alert('Atención', 'Al editar la noticia ya no se podrá recuperar lo anterior, ¿está seguro de continuar?', [{
            text: 'Sí, continuar', onPress: () => this.editConfirm(it)
        }, {
            text: 'Cancelar'
        }]);
    }

    editConfirm(it) {
        // console.log(it)
        Actions.editNew({data: it})
    }

    renderCards() {
        let cards = [];
        this.state.data.forEach((it, i) => {
            cards.push(this.renderCard(it, i))
        });
        return cards;
    }

    renderCard(it, i) {
        const onPressButton = () => Actions.newsSingle({data: it});
        return (<TouchableOpacity key={'cardNoticia' + i} onPress={onPressButton}>
            <View
                style={[styles.noticia, {borderColor: '#999999', borderWidth: 1}]}>
                <View style={[styles.head, {padding: 5}]}>
                    <Text numberOfLines={1} style={styles.titulo_noticia}>
                        {it.titulo}
                    </Text>
                    <TimeAgo
                        style={styles.fecha}
                        interval={20000}
                        time={it.created_at}
                    />
                </View>
                <View
                    style={[styles.descripcion, {padding: 5, paddingBottom: 0, height: 40}]}>
                    <Text>{it.resumen}</Text>
                </View>
                <View style={[styles.row, {padding: 5, paddingTop: 0}]}>
                    <View style={{
                        textAlign: "right",
                        width: responsiveWidth(74)
                    }}>{it.enterado !== 0 ? this.isEnterado() : this.noEnterado()}</View>

                    {this.state.admin === 'Admin' ?
                        <View style={{textAlign: "right", width: responsiveWidth(10)}}>
                            <MaterialIcons name='edit' size={22} color='gray'
                                           onPress={() => this.edit(it)}/>
                        </View> : null
                    }
                    {this.state.admin === 'Admin' ?
                        <View style={{textAlign: "right", width: responsiveWidth(10)}}>
                            <Entypo name='circle-with-minus' size={22} color='red'
                                    onPress={() => this.delete(it.id)}/>
                        </View> : null
                    }
                </View>
            </View>
        </TouchableOpacity>);
    }

    render() {
        return (<View style={[styles.container]}>
            <StatusBar
                backgroundColor={this.state.mainColor}
                barStyle='light-content'
            />
            {this.state.admin === 'Admin' ? this.isAdmin() : null}
            {this.state.admin === 'Padre' || this.state.admin === 'Maestro' || this.state.admin === 'Alumno' ? this.isNotPapa() : null}
        </View>);
    }
}
