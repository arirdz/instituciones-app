import React from 'react';
import {
    Alert,
    AsyncStorage,
    Image,
    RefreshControl,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TimeAgo from "react-native-timeago";


export default class Notificaciones extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: {},
            refreshing: false,
            laSolicitud: '',
            solicitudes: [],
            types: [],
            type: 'noticias',
            elRequest: [],
            indexSelected: 0,
            data: [],
            visible: false,
            swipeable: null,
            selected: 1
        };
    }

    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    _onRefresh = () => {
        if (this.state.indexSelected === 0 && this.state.type !== '') {
            this.setState({refreshing: true});
            this.getFilterMias();
        } else if (this.state.indexSelected === 1 && this.state.type !== '') {
            this.setState({refreshing: true});
            this.getFilterPorMi();
        }
    };

    async componentWillMount() {
        await this.getURL();
        await this.getFilterMias()
    }

    async getURL() {
        this.setState({
            uri: await AsyncStorage.getItem('uri'),
            token: await AsyncStorage.getItem('token'),
            mainColor: await AsyncStorage.getItem('maincolor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor'),
            role: await AsyncStorage.getItem('role'),
            puesto: await AsyncStorage.getItem('puesto')
        });
    }

    //++++++ Modal +++++++++
    async getFilterMias() {
        await fetch(this.state.uri + '/api/filter1/notificaciones/' + this.state.selected, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({refreshing: false});
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Notificaciones)', [{
                        text: 'Entendido', onPress: () => [Actions.drawer(), this.setState({visible: false})]
                    }]);
                } else {
                    this.setState({elRequest: responseJson, visible: false});
                }
            });

    }

    async getFilterPorMi() {
        await fetch(this.state.uri + '/api/filter2/notificaciones/' + this.state.type, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({refreshing: false});
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Notificaciones)', [{
                        text: 'Entendido', onPress: () => [Actions.drawer(), this.setState({visible: false})]
                    }]);
                } else {
                    this.setState({elRequest: responseJson, visible: false});
                }
            });

    }

    async onChange(option) {
        await this.setState({type: option.id});
        await this._changeWheelState();
        if (this.state.indexSelected === 0) {
            await this.getFilterMias();
        } else {
            await this.getFilterPorMi();
        }
    }

    renderNotificaciones() {
        let buttonsRespuestas = [];
        this.state.elRequest.forEach((it, i) => {
            buttonsRespuestas.push(this.notificaciones(it, i));
        });
        return buttonsRespuestas;
    }

    async ir(it) {
        if (it.tipo === 'noticias') {
            let request = await fetch(this.state.uri + '/api/get/post/by/notification/' + it.url, {
                method: 'GET', headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            });
            let data = await request.json();
            await this.setState({data: []});
            await this.setState({data: data});
            await Actions.newsSingle({data: this.state.data});
        } else if (it.tipo === 'cita') {
            if (this.state.role === 'Padre') {
                await Actions.agendarCitas();
            } else if (this.state.role === 'Maestro') {
                await Actions.gestCitasMtro();
            } else if (this.state.role === 'Admin') {
                if (this.state.puesto === 'Director') {
                    await Actions.gestCitasAdmin();
                } else if (this.state.puesto === 'SubDirector') {
                    await Actions.gestCitasAdmin();
                } else if (this.state.puesto === 'Coordinador Academico') {
                    await Actions.gestCitasAdmin();
                } else if (this.state.puesto === 'Secretaria') {
                    await Actions.gestCitasAdmin();
                }
            }
        } else if (it.tipo === 'encuadre') {
            if (this.state.role === 'Padre') {
                await Actions.encuadrePadre();
            } else if (this.state.role === 'Maestro') {
                await Actions.encuadre();
            } else if (this.state.role === 'Admin') {
                if (this.state.puesto === 'Director') {
                    await Actions.Encuadres();
                } else if (this.state.puesto === 'SubDirector') {
                    await Actions.Encuadres();
                } else if (this.state.puesto === 'Coordinador Academico') {
                    await Actions.Encuadres();
                } else if (this.state.puesto === 'Secretaria') {
                    await Actions.Encuadres();
                }
            }
        } else if (it.tipo === 'Pago') {
            if (this.state.role === 'Padre') {
                await Actions.menuDePagos();
            } else if (this.state.role === 'Admin') {
                if (this.state.puesto === 'Director') {
                    await Actions.menuDePagosDir();
                } else if (this.state.puesto === 'SubDirector') {
                    await Actions.menuDePagosDir();
                } else if (this.state.puesto === 'Coordinador Academico') {
                    await Actions.menuDePagosDir();
                } else if (this.state.puesto === 'Secretaria') {
                    await Actions.menuDePagosDir();
                }
            }
        }
    }


    async delete(id, i) {
        await fetch(this.state.uri + '/api/delete/notificacion/' + id, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Borrar notificaciones)', [{
                        text: 'Entendido'
                    }]);
                } else {
                    this.state.elRequest.splice(i, 1);
                    this.state.swipeable.recenter();
                    this.setState({aux: 0});
                }
            });
    }

    notificaciones(it, i) {
        let rightButtons = [];
        if (this.state.indexSelected === 0) {
            rightButtons = [<TouchableHighlight
                onPress={() => this.delete(it.id, i)}
                underlayColor={'transparent'}
                style={{
                    backgroundColor: '#fc0200',
                    borderTopWidth: 1,
                    borderColor: this.state.secondColor,
                    width: responsiveWidth(19),
                    height: responsiveHeight(8),
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <MaterialIcons style={{width: 30, height: 30}} color={'#fff'}
                               name='delete-forever' size={30}/>
            </TouchableHighlight>];
        } else {
            rightButtons = [<TouchableHighlight
                onPress={() => this.delete(it.id, i)}
                underlayColor={'transparent'}
                style={{
                    backgroundColor: '#fc0200',
                    borderTopWidth: 1,
                    borderColor: this.state.secondColor,
                    width: responsiveWidth(19),
                    height: 70,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <MaterialIcons style={{width: 30, height: 30}} color={'#fff'}
                               name='delete-forever' size={30}/>
            </TouchableHighlight>]
        }
        return (<Swipeable key={i + 'not'}
                           onRef={ref => this.swipe = ref}
                           onRightButtonsOpenRelease={(event, gestureState, swipe) => {
                               if (this.state.swipeable && this.state.swipeable !== swipe) {
                                   this.state.swipeable.recenter();
                               }
                               this.setState({swipeable: swipe});
                           }}
                           onRightButtonsCloseRelease={() => this.setState({swipeable: null})}
                           rightButtons={rightButtons}
        >
            <View
                style={[styles.notificaciones, {borderColor: this.state.secondColor}]}>
                <View style={{
                    width: responsiveWidth(10),
                    height: 70,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Image
                        style={{width: 40, height: 40}}
                        source={{uri: 'https://png.icons8.com/' + it.type + '/75/000000/' + it.img + '.png'}}/>
                </View>
                <View style={{width: responsiveWidth(70), paddingLeft: 12}}>
                    {it.tipo === 'cita' ? (<Text style={styles.textInfo}>
                        {it.user.genero === 'Femenino' ? 'La ' : 'El '}
                        {it.user.genero === 'Femenino'
                            ? it.user.puesto === 'Director'
                                ? 'Directora'
                                : it.user.puesto === 'SubDirector'
                                    ? 'Subdirectora'
                                    : null
                            : it.user.puesto} le ha solicitado una cita
                    </Text>) : null}
                    {it.tipo === 'noticias' ? (<Text style={styles.textInfo}>
                        {it.user.genero === 'Femenino' ? 'La ' : 'El '}
                        {it.user.genero === 'Femenino'
                            ? it.user.puesto === 'Director'
                                ? 'Directora'
                                : it.user.puesto === 'SubDirector'
                                    ? 'Subdirectora'
                                    : null
                            : it.user.puesto} ha publicado una nueva noticia
                    </Text>) : null}
                    {it.tipo === 'noticias' ? (<Text numberOfLines={1} style={[styles.textInfo]}>
                        {it.titulo}
                    </Text>) : null}
                    {it.tipo === 'Pago' ? (<Text style={styles.textInfo}>
                        Se ha generado un nuevo cargo para su hijo:
                    </Text>) : null}
                    {it.tipo === 'Pago' ? (<Text numberOfLines={1} style={[styles.textInfo,{marginTop:5}]}>
                        {it.name}
                    </Text>) : null}
                    {it.tipo !== 'Pago' && it.tipo !== 'noticias' && it.tipo !== 'cita' ? (
                        <Text numberOfLines={2} style={styles.textInfo}>
                            {it.data}
                        </Text>) : null}
                    <TimeAgo
                        style={styles.fecha}
                        interval={20000}
                        time={it.created_at}
                    />
                </View>
                <View style={{
                    width: responsiveWidth(8), alignItems: 'center', justifyContent: 'center'
                }}>
                    <Ionicons onPress={() => this.ir(it)} style={{width: 35, height: 35}}
                              color={this.state.secondColor}
                              name='ios-arrow-dropright-circle' size={35}/>
                </View>
            </View>
        </Swipeable>);
    }

    //+++++++ MultibotonRow +++++++
    async filtroSelected(i) {
        await this.setState({selected: i});
        await this.getFilterMias();
    }


    render() {
        const notificaciones = this.renderNotificaciones();
        let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];

        let texto1 = [styles.textoB, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnTem = [styles.exaCalif, {backgroundColor: this.state.thirdColor}];


        return (<View style={styles.container}>
            <Spinner visible={this.state.visible} textContent='Cargando...'/>
            <StatusBar
                backgroundColor={this.state.mainColor}
                barStyle='light-content'
            />
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                Elija un filtro
            </Text>
            <View style={[styles.widthall, {alignItems: 'center'}]}>
                <View
                    style={[styles.rowsCalif, {
                        width: responsiveWidth(94), marginTop: 10, marginBottom: 15
                    }]}>
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={this.state.selected === 1 ? btnTem : btnCal}
                        onPress={() => this.filtroSelected(1)}>
                        <Text style={this.state.selected === 1 ? texto1 : texto}>Semanal</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={this.state.selected === 2 ? btnTem : btnCal}
                        onPress={() => this.filtroSelected(2)}>
                        <Text style={this.state.selected === 2 ? texto1 : texto}>Mensual</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={this.state.selected === 3 ? btnTem : btnCal}
                        onPress={() => this.filtroSelected(3)}>
                        <Text style={this.state.selected === 3 ? texto1 : texto}>Todo</Text>
                    </TouchableHighlight>
                </View>
            </View>
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                {this.state.selected === 1 ? 'Actividad de esta semana'
                    : this.state.selected === 2 ? 'Actividad de este mes'
                        : 'Actividad de este ciclo'}
            </Text>
            <ScrollView style={[styles.widthall, {marginTop: 5, marginBottom: 20}]}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                        overScrollMode='always'
                        showsVerticalScrollIndicator={false}>
                {notificaciones}
            </ScrollView>
        </View>);
    }
}
