import React from 'react';
import PropTypes from 'prop-types';
import {Alert, AsyncStorage, Text, TouchableHighlight, View} from 'react-native';
import {responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';

export default class Periodos extends React.Component {
    static propTypes = {
        onListItemPressedPeriodo: PropTypes.func.isRequired
    };

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            selectedIndexPeriodo: -1,
            elPeriodo: '',
            periodos: [],
            elCiclo: ''
        };
        this.onListItemPressedPeriodo = this.onListItemPressedPeriodo.bind(this);
    }

    async componentWillMount() {
        await this.getURL();
        await this.getCiclosPeriodos();
    }

    async getURL() {
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        this.setState({
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            uri: uri,
            token: token
        });
    }

    async onListItemPressedPeriodo(indexPeriodo, itemPeriodo) {
        await this.props.onListItemPressedPeriodo(indexPeriodo, itemPeriodo);
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: itemPeriodo
        });
    }

    async getCiclosPeriodos() {
        await fetch(this.state.uri + '/api/ciclo/periodo/actual/1', {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar periodos',
                        'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Altas-Inscripciones)', [
                            {text: 'Entendido'/*, onPress: () => Actions.pop({refresh: {key: 'Drawer'}})*/}
                        ]);
                } else {
                    this.setState({elCiclo: responseJson.ciclo});
                    this.getPeriodos()
                }
            });
    }

    async getPeriodos() {
        let periodopicker = await fetch(this.state.uri + '/api/periodos/' + this.state.elCiclo, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let periodospicker = await periodopicker.json();
        this.setState({periodos: periodospicker});
    }

    renderPeriodo(itemPeriodo, indexPeriodo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn6,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexPeriodo == indexPeriodo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexPeriodo}
                underlayColor={this.state.secondColor}
                style={[smallButtonStyles, {borderColor: this.state.secondColor}]}
                onPress={() =>
                    this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                }>
                <View style={[styles.listItem]}>
                    <Text style={texto}>{itemPeriodo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderPeriodos() {
        let buttonsPeriodos = [];
        this.state.periodos.forEach((itemPeriodo, indexPeriodo) => {
            buttonsPeriodos.push(this.renderPeriodo(itemPeriodo, indexPeriodo));
        });
        return buttonsPeriodos;
    }

    render() {
        const periodos = this.renderPeriodos();
        return (
            <View style={{height: responsiveHeight(7.5)}}>
                <View>
                    <Text
                        style={[
                            styles.main_title,
                            {color: this.state.thirdColor, marginTop: 10}
                        ]}>
                        Seleccione el periodo
                    </Text>
                </View>
                <View style={styles.buttonsRow}>{periodos}</View>
            </View>
        );
    }
}