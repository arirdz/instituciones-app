import React from 'react';
import PropTypes from 'prop-types';
import {AsyncStorage, ScrollView, Text, TouchableHighlight, View} from 'react-native';
import {responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';

export default class nombresList extends React.Component {
    static propTypes = {
        onSelectedNombre: PropTypes.func,
        role: PropTypes.string.isRequired,
        grado: PropTypes.string,
        grupo: PropTypes.string
    };

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            nombres: []
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async componentDidMount() {
        await this.getUserdata();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({role: nextProps.role > this.props.role});
    }

    async getUserdata() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let role = this.props.role;
        let grado = this.props.grado;
        let grupo = this.props.grupo;
        let dato = await request.json();
        this.setState({nombres: dato});
    }

    async onListPressedName(index, user_id) {
        await this.setState({
            selectedIndexName: index,
            user_id: user_id
        });
    }

    renderName(itemName, indexName) {
        let smallButtonStyles = [
            styles.listButtonNoticia,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexName === indexName) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexName}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() =>
                    this.onListPressedName(indexName, itemName.name, itemName.id)
                }>
                <View
                    style={[
                        styles.listItem,
                        {
                            width: responsiveWidth(85),
                            justifyContent: 'center',
                            alignItems: 'center'
                        }
                    ]}>
                    <Text
                        numberOfLines={1}
                        style={[texto, {width: responsiveWidth(42)}]}>
                        {itemName.name}
                    </Text>
                    <Text
                        numberOfLines={1}
                        style={[texto, {width: responsiveWidth(42)}]}>
                        {itemName.puesto}
                    </Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderNames() {
        let buttonsNames = [];
        this.state.nombres.forEach((itemName, indexName) => {
            buttonsNames.push(this.renderName(itemName, indexName));
        });
        return buttonsNames;
    }

    render() {
        const nombres = this.renderNames();
        return (
            <View style={{}}>
                <ScrollView
                    style={[
                        styles.titulosContainer,
                        {borderColor: this.state.secondColor}
                    ]}
                    horizontal={false}
                    showsVerticalScrollIndicator={false}>
                    {nombres}
                </ScrollView>
            </View>
        );
    }
}
