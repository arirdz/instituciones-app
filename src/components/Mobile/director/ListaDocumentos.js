import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight, Alert, Linking
} from 'react-native';
import styles from '../../styles';
import {responsiveFontSize} from 'react-native-responsive-dimensions';
import moment from 'moment';
import Entypo from 'react-native-vector-icons/Entypo';

export default class ListaDocumentos extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: this.props.listaArchivos,
			archivos: []
		};
	}

	async componentWillMount(){
		await this.getURL();
		await this.setState({
			id: this.props.listaArchivos,
			mostrar: false
		});
		await this.getArchivos(this.state.id);
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async link(url) {
		await Linking.openURL(url);
	}

	getArchivos(id) {
		fetch(this.state.uri + '/api/get/files/' + id,
			{
				method: 'get',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			})
			.then(response => {
				return response.json();

			}).then(responseJson => {
			if (responseJson.error === null) {
				Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Documentos)');
			} else {
				this.setState({archivos: []});
				this.setState({archivos: responseJson});
				if (this.state.archivos.length > 0) {
					this.setState({mostrar: true})

				} else {
					Alert.alert('No existen documentos en esta carpeta');
					this.setState({mostrar: false})
				}
			}
		});
	}

	archivos() {
		let carta = [];
		this.state.archivos.forEach((item, i) => {
			carta.push(
				<TouchableOpacity
					key={i + 'carta'}
					style={[
						styles.widthall,
						// styles.row,
						{
							borderWidth: 0.5,
							borderRadius: 6,
							borderColor: 'lightgray',
							backgroundColor: this.state.fourthColor,
							paddingHorizontal: 6,
							paddingVertical: 10,
							marginTop: 10
						}
					]}>
					<View style={[styles.row, styles.widthall, {justifyContent: "flex-start"}]}>
						<View style={[styles.btn2]}>
							<Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Nombre del archivo
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
								marginTop: 5
							}}>{item.nombre}</Text>
						</View>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Fecha
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 5
							}}>{moment(item.created_at).format('L')}</Text>
						</View>
						<View style={[styles.btn4, {alignItems: 'center', textAlign: 'center'}]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Visualizar
							</Text>
							<Entypo name='eye' size={18} color='#000'
									style={{paddingTop: 2}}
									onPress={() => this.link(item.url)}/>
						</View>
					</View>
				</TouchableOpacity>
			)
		});
		return carta;
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.container}>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione el documento
					</Text>
					<ScrollView>
						{this.state.mostrar === true ? this.archivos() : null}
					</ScrollView>
				</View>
			</View>
		);
	}
}
