import React from 'react';
import {
	Alert, AsyncStorage, RefreshControl, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import styles from '../../styles';
import {Actions} from 'react-native-router-flux';
import {responsiveFontSize} from 'react-native-responsive-dimensions';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class CitasAspirante extends React.Component {
	_onRefresh = () => {
		this.setState({refreshing: true});
		fetch(this.state.uri + '/api/get/citas/inscrip').then(res => res.json()).then(responseJson => {
			this.setState({refreshing: false});
			if (responseJson.error !== undefined) {
				Alert.alert('Error al cargar citas', 'Ha ocurrido un error ' + responseJson.error.status_code +
					' al tratar de cargar las citas, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido',
						onPress: () => Actions.pop({refresh: {key: 'drawer'}})
					}]);
			} else {
				this.setState({citas: responseJson});
			}
		});
	};

	_changeWheelState = (state) => {
		this.setState({
			visible: state
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			citas: [],
			refreshing: false,
			data: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getCitasAspirante();
		await this.getUserdata();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let schoolId = await AsyncStorage.getItem('schoolId');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			schoolId: schoolId
		});
	}

	async getUserdata() {
		 await fetch(this.state.uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json()).then(responseJson => {
			 if (responseJson.error !== undefined) {
				 Alert.alert('Error al cargar citas', 'Ha ocurrido un error ' + responseJson.error.status_code +
					 ' al tratar de cargar las citas, si el error continua pónganse en contacto con soporte (Citas preinscripción)',
					 [{
						 text: 'Entendido',
						 onPress: () => [this._changeWheelState(false), Actions.pop({refresh: {key: 'drawer'}})]
					 }]);
			 } else {
				 this.setState({data: responseJson});
			 }
		 });
	}

	async getCitasAspirante() {
		await this._changeWheelState(true);
		await fetch(this.state.uri + '/api/get/citas/inscrip').then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al cargar citas', 'Ha ocurrido un error ' + responseJson.error.status_code +
					' al tratar de cargar las citas, si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido',
						onPress: () => [this._changeWheelState(false), Actions.pop({refresh: {key: 'drawer'}})]
					}]);
			} else {

				this.setState({citas: responseJson});
				this._changeWheelState(false);
			}
		});
	}

	async aprobarCita(it) {
		Alert.alert('Acpetar cita', 'Está a punto de aceptar una cita ¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.aprobarCitaYa(it, this.state.data.id)}, {text: 'No'}
		])
	}

	async aprobarCitaYa(it, id_admin) {
		await fetch(this.state.uri + '/api/aprobar/cita/asp/' + it.id_aspirante + '/' + id_admin).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al aceptar la cita', 'Ha ocurrido un error ' + responseJson.error.status_code +
					'  si el error continua pónganse en contacto con soporte (Aspirantes)',
					[{
						text: 'Entendido',
						onPress: () => Actions.pop({refresh: {key: 'drawer'}})
					}]);
			} else {
				this.citaAceptada(it);
				this.notifCita(it, this.state.data);
			}
		});
	}

	async notifCita(it, admin) {
		let mensaje = 'La cita del aspirante ' + it.aspirante.nombre_aspirante + ' ha sido aceptada. Recuerde que su cita será para el dia ' + moment(it.fecha_cita).format('ll') + ' a las ' + moment(it.hora_cita, 'hh:mm').format('hh:mh a') + '  y será atendido por el maestro(a) ' + admin.name;
		await fetch(this.state.uri + '/api/notificar/tutor/' + it.id_tutor + '/' + this.state.schoolId + '/' + mensaje, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}
		}).then(res => res.json())
			.then(responseJson => {
				console.log(responseJson);
			});
	}

	async citaAceptada(it) {
		await fetch(this.state.uri + '/api/notificar/cita/' + it.id_tutor + '/' + it.id_aspirante).then(res => res).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al notificar la cita', 'Ha ocurrido un error ' + responseJson.error.status_code +
					'  si el error continua pónganse en contacto con soporte (Aspirantes)',
					[{
						text: 'Entendido',
						onPress: () => Actions.pop({refresh: {key: 'drawer'}})
					}]);
			} else {
				Alert.alert('Cita aceptada', 'Se ha aceptado la cita, se le notificara al tutor del aspirante', [
					{text: 'Entendido', onPress: () => this.getCitasAspirante()}
				])
			}
		});
	}

	rndCitas() {
		let citas = [];
		this.state.citas.forEach((it, ix) => {
			citas.push(
				<View
					key={ix + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View
						style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							backgroundColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}
					>
						<Text style={styles.notifTitle}>Cita de preinscripción</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							Se ha solicitado una cita del Sr(a){' '}
							<Text style={styles.textW}>{it.tutor.nombre + ' ' + it.tutor.apellidos}</Text> tutor
							del aspirante{' '}
							<Text style={styles.textW}>{it.aspirante.nombre_aspirante}.</Text>
						</Text>
						<Text style={styles.textInfo}>
							Agendada para el día{' '}
							<Text style={styles.textW}>{moment(it.fecha_cita).format('DD/MM/YYYY')} </Text>
							a las <Text
							style={styles.textW}>{moment(it.hora_cita, 'hh:mm').format('hh:mm A')}.</Text> {'\n'}para
							tratar sobre{' '}
							<Text style={styles.textW}>la inscripción.</Text>
						</Text>
					</View>

					<View style={[{
						flexDirection: 'row',
						paddingVertical: 5,
						backgroundColor: this.state.fourthColor,
						borderBottomLeftRadius: 5,
						borderBottomRightRadius: 5
					}]}>
						{it.aprobada === '0' ?
							<TouchableOpacity
								onPress={() => this.aprobarCita(it)}
								style={[styles.btnCita, styles.btn3, {backgroundColor: '#3fa345'}]}
							>
								<Text style={{color: '#fff'}}>Aceptar</Text>
							</TouchableOpacity> :
							<Text style={[styles.widthall, {textAlign: 'left', paddingHorizontal: 10}]}>
								Aceptada por <Text style={[styles.textW]}>{it.admin.name}</Text>
							</Text>
						}
					</View>
				</View>
			)
		});
		return citas;
	}

	render() {
		let citas = this.state.citas.length;
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Citas de los aspirantes
				</Text>
				{citas !== 0 ? <ScrollView
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					overScrollMode='always'
					style={{...ifIphoneX({marginBottom: 36}, {marginBottom: 20})}}
					showsVerticalScrollIndicator={false}
				>
					{this.rndCitas()}
				</ScrollView>:<Text style={[styles.main_title, {
					color: '#a8a8a8',
					textAlign: 'center',
					marginTop: 40,
					marginBottom: 40,
					fontSize: responsiveFontSize(3)
				}]}>
					No hay citas de aspirantes registradas aún
				</Text>}
			</View>
		);
	}
}
