import React from 'react';
import {
    Alert,
    AsyncStorage,
    Keyboard,
    Platform,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import styles from '../../styles';
import ModalSelector from "react-native-modal-selector";
import {responsiveFontSize, responsiveHeight, responsiveWidth} from "react-native-responsive-dimensions";
import Periodos from './../Globales/Periodos';
import GradoyGrupo from './../Globales/GradoyGrupo';
import MultiBotonRow from './../Globales/MultiBotonRow';
import ConsultaCalifMtro from './../Admin/ConsultaCalifAdmin';
import Modal from 'react-native-modal';
import Entypo from 'react-native-vector-icons/Entypo';
import Spinner from 'react-native-loading-spinner-overlay';

export default class captCalifcMtro extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            elCiclo: '',
            btnNivel: [{'nivel': 'I'}, {'nivel': 'II'}, {'nivel': 'III'}, {'nivel': 'IV'}],
            datos: [],
            materias: [],
            grupos: [],
            alumnos: [],
            califVal: [],
            visible: false,
            modalCalif: false,
            checkBoxBtns: [],
            lasCalifs: [],
            nombreMat: '',
            elidMaestro: '',
            laMateria: '',
            elGrado: '',
            indxGrado: -1,
            elGrupo: '',
            indxGrupo: -1,
            elPeriodo: '',
            indxPeriodo: -1,
            tipoMat: '',
            aux: 0,
            indexBtnLvl: [],
            BtnLvl: [],
            moveIndx: 0,
            indxSelected: 0
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getUserdata();
    }

    async onListItemPressedPeriodo(indxBtn, itmBtn) {
        await this.setState({elPeriodo: itmBtn, indxPeriodo: indxBtn});
        if (this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.laMateria !== '') {
            await this.getCalificaciones(this.state.elGrado, this.state.elGrupo, this.state.laMateria);
        }
    }

    async ItemPressedGrado(indxBtn, itmBtn) {
        await this.setState({elGrado: itmBtn, indxGrado: indxBtn});
        if (this.state.elPeriodo !== '' && this.state.elGrupo !== '' && this.state.laMateria !== '') {
            await this.getCalificaciones(this.state.elGrado, this.state.elGrupo, this.state.laMateria);
        }
        await this.getCicloActual(itmBtn);
    }

    async ItemPressedGrupo(indxBtn, itmBtn) {
        await this.setState({elGrupo: itmBtn, indxGrupo: indxBtn});
        if (this.state.elPeriodo !== '' && this.state.elGrado !== '' && this.state.laMateria !== '') {
            await this.getCalificaciones(this.state.elGrado, this.state.elGrupo, this.state.laMateria);
        }
        await this.getMaterias(itmBtn);
    }

    _spinner = (state) => {
        this.setState({visible: state});
    };

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async capturarCalif() {
        let found = false;
        let found2 = false;
        if (this.state.tipoMat === 'formacion academica') {
            for (let i = 0; i < this.state.califVal.length; i++) {
                if (Number(this.state.califVal[i]) <= 5 || Number(this.state.califVal[i]) >= 11) {
                    found = true;
                    break;
                }
            }
            if (found === true) {
                Alert.alert('Calificaciones no validas',
                    'Faltan alumnos por asignar calificación o estas no cumplen el requisto de calificación minima', [
                        {text: 'Enterado'}
                    ])
            } else {
                if (this.state.lasCalifs.length !== 0) {
                    Alert.alert('Editar calificaciones', '¿Seguro que desea continuar?', [
                        {text: 'Sí', onPress: () => [this.updateCalificaciones()]}, {text: 'No'}
                    ])
                } else {
                    Alert.alert('Guardar calificaciones', '¿Seguro que desea continuar?', [
                        {text: 'Sí', onPress: () => [this.requestMultipart()]}, {text: 'No'}
                    ])
                }
            }
        } else {
            for (let j = 0; j < this.state.indexBtnLvl.length; j++) {
                if (this.state.indexBtnLvl[j] === -1) {
                    found2 = true;
                    break;
                }
            }
            if (found2 === true) {
                Alert.alert('',
                    'Faltan alumnos por asignar nivel de desempeño', [
                        {text: 'Enterado'}
                    ])
            } else {
                if (this.state.lasCalifs.length !== 0) {
                    Alert.alert('Editar calificaciones', '¿Seguro que desea continuar?', [
                        {text: 'Sí', onPress: () => [this.updateCalificaciones()]}, {text: 'No'}
                    ])
                } else {
                    Alert.alert('Guardar calificaciones', '¿Seguro que desea continuar?', [
                        {text: 'Sí', onPress: () => [this.requestMultipart()]}, {text: 'No'}
                    ])
                }
            }
        }
    }

    //+-+-+-+-+-+ actualizar
    async updateCalificaciones() {
        await this.openModal(false);
        await this.setState({aux: 0});
        await this.setState({visible: true});
        let lasCalifs = [];
        let ArrayAlumn = this.state.lasCalifs;
        if (this.state.tipoMat === 'formacion academica') {
            lasCalifs = this.state.califVal;
        } else {
            lasCalifs = this.state.indexBtnLvl;
        }
        let response = [];
        for (let i = 0; i < ArrayAlumn.length; i++) {
            await fetch('http://127.0.0.1:8000/api/update/calif/mat', {
                method: 'POST', headers: {
                    Accept: 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                }, body: JSON.stringify({
                    id_alumno: ArrayAlumn[i].id,
                    id_materia: this.state.laMateria,
                    grupo: this.state.elGrupo,
                    ciclo: this.state.elCiclo,
                    periodo: this.state.elPeriodo,
                    calificacion: 'S/D',
                    captura_maestro: lasCalifs[i],
                    captura_cord: 'S/D',
                })
            }).then(res => res.json())
                .then(responseJson => {
                    console.log(responseJson);
                    response = responseJson;
                });
        }
        if (response.error !== undefined) {
            Alert.alert('Error al editar las calificaciones',
                'Hubo un error al tratar de guardar las calificaciones', [
                    {text: 'Enterado', onPress: () => [this._spinner(false)]}
                ]);
        } else {
            Alert.alert('Calificaciones editadas',
                'Se han editado las calificaiones correctamente', [
                    {text: 'Enterado', onPress: () => [this._spinner(false)]}
                ]);
        }
    }

    arrayResponse() {
        let lasCalifs = [];
        let ArrayAlumn = [];
        if (this.state.tipoMat === 'formacion academica') {
            lasCalifs = this.state.califVal;
            ArrayAlumn = this.state.alumnos;
        } else {
            lasCalifs = this.state.indexBtnLvl;
            ArrayAlumn = this.state.checkBoxBtns;
        }
        let formData = [];
        for (let i = 0; i < ArrayAlumn.length; i++) {
            console.log(ArrayAlumn[i]);
            console.log(lasCalifs[i]);
            formData.push({
                id_alumno: ArrayAlumn[i].id,
                id_materia: this.state.laMateria,
                grupo: this.state.elGrupo,
                ciclo: this.state.elCiclo,
                periodo: this.state.elPeriodo,
                calificacion: 'S/D',
                captura_maestro: lasCalifs[i],
                captura_cord: 'S/D',
            });
        }
        return formData;
    }

    async requestMultipart() {
        await this.openModal(false);
        await this.setState({aux: 0});
        await this.setState({visible: true});
        let formData = new FormData();
        formData.append('new', JSON.stringify(this.arrayResponse()));
        await fetch('http://127.0.0.1:8000/api/post/capturar/calif', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al guardar las calificaciones',
                        'Hubo un error al tratar de guardar las calificaciones', [
                            {text: 'Etendido', onPress: () => this._spinner(false)}
                        ]);
                } else {
                    Alert.alert('Calificaciones guardadas',
                        'Se han guardado las calificaciones correctamente', [
                            {
                                text: 'Enterado',
                                onPress: () => [this.getCalificaciones(this.state.laMateria)]
                            }
                        ]);
                }
            });
    }

    //+-+-+-+-+-+-+-+-+-+-+-+-+-+ciclos y periodos
    async getCicloActual(grado) {
        await fetch(this.state.uri + '/api/ciclo/periodo/actual/' + grado, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar las materias',
                        'Hubo un error al tratar de cargar las materias');
                } else {
                    this.setState({elCiclo: responseJson.ciclo});
                }
            });
    }

    async getUserdata() {
        await fetch(this.state.uri + '/api/user/data', {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar datos', '');
                } else {
                    this.setState({dataM: responseJson});
                    this.setState({elidMaestro: responseJson.user_id});
                }
            });
    }

    //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-GET Materias-+-+-+-+-+-+-+-+
    async getMaterias(grado) {
        await fetch(this.state.uri + '/api/feed/view/materias/' + grado, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                console.log(responseJson);
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar las materias',
                        'Hubo un error al tratar de cargar las materias');
                } else {
                    this.setState({materias: responseJson});
                }
            });
    }

    async onChange(option) {
        await this.setState({
            nombreMat: option.label,
            tipoMat: option.tipo_materia,
            laMateria: option.materia,
            checkBoxBtns: [],
            indexBtnLvl: [],
            califVal: []
        });
        await this.getCalificaciones(this.state.elGrado, this.state.elGrupo, option.materia);
    }

    //+-+-+--+-+-+-+-+-+-+GET ALUMNOS +--+-+-+-+-+-
    async getCalificaciones(grado, grupo, materia) {
        await this._spinner(true);
        await fetch('http://127.0.0.1:8000/api/calif/maestro/' +
            this.state.elPeriodo + '/' + materia + '/' + grado + '/' + grupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(responseJson => {
                console.log(responseJson);
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar las calificaciones',
                        'Hubo un error al tratar de guardar las calificaciones', [
                            {text: 'Etendido', onPress: () => this._spinner(false)}
                        ]);
                } else {
                    this.setState({lasCalifs: responseJson});
                    this.state.lasCalifs.forEach((itm, i) => {
                        this.state.califVal[i] = itm.calificacion;
                        this.state.indexBtnLvl[i] = Number(itm.calificacion);
                        this.setState({aux: 0});
                    });
                    this._spinner(false);
                }
            });
    }

    rndAlumnos() {
        let alumnos = [];
        if (this.state.lasCalifs.length !== 0) {
            this.state.lasCalifs.forEach((itm, ix) => {
                let a = this.state.lasCalifs.length - 1;
                let tabRow = [styles.rowTabla];
                if (ix !== a) {
                    tabRow.push({borderColor: this.state.secondColor});
                }
                alumnos.push(
                    <View
                        key={ix + 'alumnCal'}
                        underlayColor={'transparent'}
                        style={[tabRow, styles.campoTablaG, styles.row]}
                    >
                        <Text style={{width: responsiveWidth(45)}}>{itm.nombre}</Text>
                        {this.state.tipoMat !== 'formacion academica' ?
                            <View style={[styles.row, {width: responsiveWidth(35)}]}>
                                {this.rndLvlBtn(itm, ix)}
                            </View> : this.values(itm, ix)}
                    </View>
                );
            });
        }
        return alumnos;
    }

    //+-+-+-+-+-+--+-+rnd btn level
    async califValue(text, ix) {
        this.state.califVal[ix] = text;
        await this.setState({aux: 0});
    }

    rndInputs() {
        let inputs = [];
        if (this.state.lasCalifs.length !== 0) {
            this.state.lasCalifs.forEach((itm, ix) => {
                if (this.state.moveIndx === ix) {
                    inputs.push(
                        <View key={ix + 'inputs'} style={[styles.modalWidth, {alignItems: 'center'}]}>
                            <Text
                                style={[
                                    styles.main_title, styles.modalWidth,
                                    {color: this.state.thirdColor, textAlign: 'center'}]}
                            >
                                {itm.nombre}
                            </Text>
                            <Text style={[styles.textW, {marginVertical: 10}]}>{itm.grado + ' ' + itm.grupo}</Text>
                            <TextInput
                                keyboardType='numeric'
                                maxLength={3}
                                multiline={false}
                                onChangeText={text => this.califValue(text, ix)}
                                onEndEditing={() => Keyboard.dismiss()}
                                placeholder={'10'}
                                returnKeyType='done'
                                underlineColorAndroid='transparent'
                                defaultValue={this.state.califVal[ix]}
                                style={[
                                    styles.inputPicker,
                                    styles.btn3,
                                    {
                                        borderColor: this.state.secondColor,
                                        fontSize: responsiveFontSize(2),
                                        padding: 8,
                                        textAlign: 'center'
                                    }
                                ]}
                            />
                        </View>
                    );
                }
            });
        }
        return inputs;
    }

    values(alumno, ix) {
        let califValue = Number(this.state.califVal[ix]);
        let value = '';
        if (califValue === 5) {
            value = 'I';
        } else if (califValue >= 6 && califValue <= 7.49) {
            value = 'II';
        } else if (califValue >= 7.5 && califValue <= 9.49) {
            value = 'III';
        } else if (califValue >= 9.5) {
            value = 'IV';
        } else if (isNaN(califValue) || califValue >= 0 || califValue < 11) {
            value = 'S/D';
        }
        return (
            <View style={[styles.row, {width: responsiveWidth(34)}]}>

                <Text
                    style={[styles.btn3_3, styles.textW,
                        {textAlign: 'center'}]}
                >
                    {this.state.califVal[ix]}
                </Text>
                <Text
                    style={[styles.btn3_3, styles.textW,
                        {textAlign: 'center'}]}
                >
                    {value}
                </Text>
            </View>
        )
    }

    async selectedAlumno(itAl, ixAl, itLvl, ixLvl) {
        this.state.indexBtnLvl[ixAl] = ixLvl;
        if (!this.state.checkBoxBtns.includes(itAl)) {
            this.state.checkBoxBtns.push(itAl);
        }
        await this.setState({aux: 0});
    }

    rndLvlBtn(alumno, indx) {
        const niveles = [];
        if (this.state.btnNivel.length !== 0) {
            this.state.btnNivel.forEach((itm, i) => {
                let indxBtn = this.state.indexBtnLvl[indx] === i;
                niveles.push(
                    <TouchableHighlight
                        key={i + 'btnLvl'}
                        underlayColor={'transparent'}
                        style={[styles.btn5_l,
                            {
                                backgroundColor: indxBtn ? this.state.secondColor : '#fff',
                                borderWidth: 1,
                                borderColor: this.state.secondColor,
                                borderRadius: 6,
                                padding: 4
                            }
                        ]}
                        onPress={() => this.selectedAlumno(alumno, indx, itm, i)}
                    >
                        <Text style={{textAlign: 'center', color: indxBtn ? '#fff' : '#000'}}>{itm.nivel}</Text>
                    </TouchableHighlight>
                )
            });
        }
        return niveles;
    }

    openModal = (state) => {
        this.setState({modalCalif: state, moveIndx: 0});
        if (state === false) {
            this._spinner(true);
        }
    };

    async siguiente() {
        let i = 0;
        if (Number(this.state.lasCalifs.length) - 1 !== Number(this.state.moveIndx)) {
            this.state.moveIndx = this.state.moveIndx + ++i;
        }
        await this.setState({aux: 0});
    }

    async atras() {
        let i = Number(this.state.moveIndx);
        if (i !== 0) {
            this.state.moveIndx = --i;
        }
        await this.setState({aux: 0});
    }

    async faltaPer() {
        if (this.state.elPeriodo === '') {
            Alert.alert('Seleccione un periodo', '', [
                {text: 'Enterado'}
            ])
        } else if (this.state.elGrado === '') {
            Alert.alert('Seleccione un grado', '', [
                {text: 'Enterado'}
            ])
        } else if (this.state.elGrupo === '') {
            Alert.alert('Seleccione un grupo', '', [
                {text: 'Enterado'}
            ])
        }
    }

    rndCalifs() {
        let lasCalifs = this.state.lasCalifs.length;
        let data = this.state.materias.map((item, i) => {
            return {
                key: i,
                label: item.nombre,
                materia: item.id,
                tipo_materia: item.tipo_materia
            };
        });
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.modalCalif}
                    backdropOpacity={0.9}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight: responsiveHeight(80)}]}>
                        {this.rndInputs()}
                        {/*Botones indices*/}
                        <View style={[styles.row, styles.btn1_2, {marginVertical: 10}]}>
                            <TouchableOpacity
                                style={[styles.row]}
                                onPress={() => this.atras()}
                            >
                                <Entypo name='chevron-left' size={30}/>
                                <Text style={[styles.textW, {fontSize: responsiveFontSize(1.8)}]}>
                                    Anterior
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.row]}
                                onPress={() => this.siguiente()}
                            >
                                <Text style={[styles.textW, {fontSize: responsiveFontSize(1.8)}]}>
                                    Siguiente
                                </Text>
                                <Entypo name='chevron-right' size={30}/>
                            </TouchableOpacity>
                        </View>
                        {/*Botones indices*/}
                        <View style={[styles.modalWidth, styles.row, {marginBottom: 10}]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({modalCalif: false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.capturarCalif()}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>
                                    {lasCalifs !== 0 ? 'Editar registro' : 'Guardar registro'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <Periodos
                    onListItemPressedPeriodo={(indxBtn, itmBtn) => this.onListItemPressedPeriodo(indxBtn, itmBtn)}/>
                <GradoyGrupo
                    onListItemPressedGrado={(indxBtn, itmBtn) => this.ItemPressedGrado(indxBtn, itmBtn)}
                    onListItemPressedGrupos={(indxBtn, itmBtn) => this.ItemPressedGrupo(indxBtn, itmBtn)}/>
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 5}]}>
                    Captura de calificaciones
                </Text>
                {this.state.elPeriodo === '' && this.state.elGrado === '' && this.state.elGrupo === '' ?
                    <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor, marginTop: 2}
                        ]}
                        onPress={() => this.faltaPer()}
                    >
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>
                            Seleccione la materia
                        </Text>
                    </TouchableOpacity>
                    : <ModalSelector
                        cancelText='Cancelar'
                        data={data}
                        initValue='Seleccione la materia'
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
                        selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor, marginTop: 2, marginBottom: 10}
                        ]}
                    />}
                {this.state.lasCalifs.length !== 0 && this.state.laMateria !== '' ?
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        overScrollMode={'always'}
                    >
                        <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 0}]}>
                            Lista de alumnos
                        </Text>
                        {this.state.tipoMat === 'formacion academica' ?
                            <TouchableOpacity
                                style={[styles.btn3, {
                                    backgroundColor: lasCalifs !== 0 ? this.state.secondColor : 'red',
                                    borderRadius: 6,
                                    padding: 4,
                                    marginTop: 0,
                                    marginBottom: 5
                                }]}
                                onPress={() => this.setState({modalCalif: true})}
                            >
                                <Text style={[styles.textW, {color: '#fff', textAlign: 'center'}]}>
                                    {lasCalifs !== 0 ? 'Editar' : 'Capturar ahora'}
                                </Text>
                            </TouchableOpacity> : null}
                        {this.state.tipoMat === 'formacion academica' ?
                            <View
                                style={[styles.row, styles.widthall, {paddingHorizontal: 4}]}
                            >
                                <Text
                                    style={{
                                        fontWeight: '500',
                                        width: responsiveWidth(53),
                                        fontSize: responsiveFontSize(1.5)
                                    }}
                                >
                                    Nombre del Alumno
                                </Text>
                                <Text
                                    style={{fontWeight: '500', fontSize: responsiveFontSize(1.5)}}
                                >
                                    Califcación
                                </Text>
                                <Text
                                    style={{fontWeight: '500', fontSize: responsiveFontSize(1.5), textAlign: 'center'}}
                                >
                                    Nivel de{'\n'}desempeño
                                </Text>
                            </View> :
                            <View
                                style={[styles.row, styles.widthall, {paddingHorizontal: 4, marginTop: 10}]}
                            >
                                <Text
                                    style={{
                                        fontWeight: '500',
                                        width: responsiveWidth(45),
                                        fontSize: responsiveFontSize(1.5)
                                    }}
                                >
                                    Nombre del Alumno
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '500',
                                        fontSize: responsiveFontSize(1.5),
                                        textAlign: 'center',
                                        width: responsiveWidth(35)
                                    }}
                                >
                                    Nivel de desempeño
                                </Text>
                            </View>
                        }
                        <View
                            style={[
                                styles.tabla,
                                {
                                    borderColor: this.state.secondColor,
                                    marginTop: 5,
                                }
                            ]}
                        >
                            {this.rndAlumnos()}
                        </View>
                        {this.state.tipoMat !== 'formacion academica' ? <TouchableOpacity
                            style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
                            onPress={() => this.capturarCalif()}
                        >
                            <Text style={styles.textButton}>
                                {lasCalifs !== 0 ? 'Editar registro' : 'Guardar registro'}
                            </Text>
                        </TouchableOpacity> : null}
                    </ScrollView> :
                    <Text style={[styles.main_title, {
                        color: '#a8a8a8',
                        textAlign: 'center',
                        ...Platform.select({
                            ios: {marginTop: 40, marginBottom: 40,},
                            android: {marginTop: 35, marginBottom: 35}
                        }),

                        fontSize: responsiveFontSize(3)
                    }]}>
                        Haga una selección
                    </Text>}
            </View>
        );
    }

    async botonSelected(indexBtn, itemBtn) {
        await this.setState({indxSelected: indexBtn, btnSelected: itemBtn});
    }

    render() {
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} textContent='Cargando, por favor espere...'/>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <MultiBotonRow
                    itemBtns={['Capturar', 'Consultar']}
                    cantidad={2}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                />
                {this.state.indxSelected === 0 ? this.rndCalifs() : <ConsultaCalifMtro/>}
            </View>
        );
    }
}
