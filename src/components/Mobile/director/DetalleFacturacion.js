import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight, Alert
} from 'react-native';
import {
	responsiveFontSize,
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class DetalleFacturacion extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			factura: this.props.factura,
			sFacturar: false
		};
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
	}

	async componentWillMount() {
		await this.getURL();
	}

	async facturar() {
		console.log(this.state.factura.id)
		await this.setState({sFacturar: true});
		await fetch(this.state.uri + '/api/facturas/create', {
			method: 'post',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}, body: JSON.stringify({
				serie: this.state.factura.facturacion.serie,
				cpEmisor: this.state.factura.facturacion.cpEmisor,
				card: this.state.factura.card.card,
				rfcEmisor: this.state.factura.facturacion.rfc,
				nombreEmisor: this.state.factura.facturacion.emisor,
				rfcReceptor: this.state.factura.datos_fac.rfc,
				nombreReceptor: this.state.factura.datos_fac.nombre,
				rvoe: this.state.factura.facturacion.rvoe,
				curp: this.state.factura.hijo_data.curp,
				nivel: this.state.factura.facturacion.nivel,
				estudiante: this.state.factura.hijo.name,
				descripcion: this.state.factura.cargo.descripcion,
				total: this.state.factura.cargo.importe_a_pagar,
				cargo_id: this.state.factura.cargo.id,
				transaction_id: this.state.factura.id,
			})
		}).then(res => res.json())
			.then(responseJson => {
				console.log(responseJson)
				if (responseJson.error === null) {
					Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Facturación)', [{
						text: 'Enterado', onPress: () => [this.setState({sFacturar: false})]
					}]);
				} else {
					Alert.alert('Felicidades', 'Se ha realizado la facturación correctamente', [{
						text: 'Entendido', onPress: () => [this.setState({sFacturar: false}),Actions.menuFacturas()]
					}]);
				}
			});
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.sFacturar} textContent='Realizando factura...'/>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{
						backgroundColor: this.state.secondColor,
						marginTop: 8
					}}>
						<Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
							Emisor
						</Text>
					</View>
					<View style={[styles.widthall, {
						padding: 10,
						justifyContent: 'flex-start',
						borderColor: this.state.secondColor,
						borderWidth: 1,
					}]}>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Nombre del emisor
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.facturacion.emisor}</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								RFC
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.facturacion.rfc}</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Régimen Fiscal
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>601 - Ley General de Personas Morales</Text>
						</View>
					</View>
					<View style={{
						backgroundColor: this.state.secondColor,
						marginTop: 8
					}}>
						<Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
							Receptor
						</Text>
					</View>
					<View style={[styles.widthall, {
						padding: 10,
						justifyContent: 'flex-start',
						borderColor: this.state.secondColor,
						borderWidth: 1
					}]}>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Nombre del receptor
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.datos_fac.nombre}</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								RFC
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.datos_fac.rfc}</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Uso del CFDI
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>D10 - Pagos por servicios educativos (colegiaturas)</Text>
						</View>
					</View>
					<View style={{
						backgroundColor: this.state.secondColor,
						borderRadius: 3,
						marginTop: 8
					}}>
						<Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
							Datos Generales
						</Text>
					</View>
					<View style={[styles.widthall, {
						padding: 10,
						justifyContent: 'flex-start',
						borderColor: this.state.secondColor,
						borderWidth: 1
					}]}>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Lugar de expedición
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.facturacion.cpEmisor}</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Método de pago
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>PUE - Pago en una sola exhibición</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Forma de pago
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.card.card}</Text>
						</View>
					</View>
					<View style={{
						backgroundColor: this.state.secondColor,
						borderRadius: 3,
						marginTop: 8
					}}>
						<Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
							Concepto
						</Text>
					</View>
					<View style={[styles.row, styles.widthall, {
						padding: 10,
						justifyContent: 'flex-start',
						borderColor: this.state.secondColor,
						borderWidth: 1,
						borderBottomWidth: 0
					}]}>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Clave Servicio
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>{this.state.factura.facturacion.ClaveProdServ}</Text>
						</View>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Clave Unidad
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>E48</Text>
						</View>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Cantidad
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>1</Text>
						</View>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Unidad
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>SERVICIO</Text>
						</View>
					</View>
					<View style={[styles.row, styles.widthall, {
						padding: 10,
						justifyContent: 'flex-start',
						borderColor: this.state.secondColor,
						borderWidth: 1,
						borderTopWidth: 0
					}]}>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Descripcion
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>{this.state.factura.cargo.descripcion}</Text>
						</View>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Moneda
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>'MXN'</Text>
						</View>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Valor unitario
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>${this.state.factura.cargo.importe_a_pagar}</Text>
						</View>
						<View style={[styles.btn4]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Total
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>${this.state.factura.cargo.importe_a_pagar}</Text>
						</View>

					</View>
					<View style={{
						backgroundColor: this.state.secondColor,
						borderRadius: 3,
						marginTop: 8
					}}>
						<Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
							Complemento del Concepto
						</Text>
					</View>
					<View style={[styles.widthall, {
						padding: 10,
						justifyContent: 'flex-start',
						borderColor: this.state.secondColor,
						borderWidth: 1
					}]}>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Nombre del Alumno
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.hijo.name}</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								CURP del Alumno
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.hijo_data.curp}</Text>
						</View>
						<View style={[styles.widthall]}>
							<Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								RVOE
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
							}}>{this.state.factura.facturacion.rvoe}</Text>
						</View>
					</View>
				</ScrollView>
				<View style={[styles.row, styles.widthall, {justifyContent: "center", marginBottom: 20}]}>
					<TouchableOpacity
						style={[
							styles.modalBigBtn,
							{backgroundColor: '#fff', borderColor: this.state.secondColor, margin: 5}
						]}
						onPress={() => [Actions.CrearFactura()]}
					>
						<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={[
							styles.modalBigBtn,
							{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor, margin: 5}
						]}
						onPress={() => this.facturar()}
					>
						<Text style={[styles.textW, {color: '#fff'}]}>Facturar</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
