import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight, ImageBackground, Image
} from 'react-native';
import {
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';
import {Actions} from "react-native-router-flux";
import {MediaQuery} from "react-native-responsive";

export default class MiServicio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <View>
                    <ScrollView
                        style={styles.menu}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.row_M}>
                            <TouchableOpacity
                                onPress={() => Actions.PagarServicio()}
                                style={[styles.btnContainer_M, {borderRadius: 6}]}>
                                <ImageBackground
                                    source={require('../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor, borderRadius: 6}]}
                                    imageStyle={{ borderRadius: 6 }}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://png.icons8.com/cotton/48/000000/card-in-use.png'}}
                                    />
                                    <Text style={styles.btnText}>Pagar</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => Actions.ConsultarServicio()}
                                style={[styles.btnContainer_M, {borderRadius: 6}]}>
                                <ImageBackground
                                    source={require('../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor, borderRadius: 6}]}
                                    imageStyle={{ borderRadius: 6 }}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://img.icons8.com/color/48/000000/view-file.png'}}
                                    />
                                    <Text style={styles.btnText}>Consultar</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.row_M}>
                            <TouchableOpacity
                                onPress={() => Actions.FacturarServicio()}
                                style={[styles.btnContainer_M, {borderRadius: 6}]}>
                                <ImageBackground
                                    source={require('../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor, borderRadius: 6}]}
                                    imageStyle={{ borderRadius: 6}}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://img.icons8.com/cotton/48/000000/invoice.png'}}
                                    />
                                    <Text style={styles.btnText}>Facturar</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => Actions.gestionarTarjetasDirector()}
                                style={[styles.btnContainer_M, {borderRadius: 6}]}>
                                <ImageBackground
                                    source={require('../../../images/FONDO7.png')}
                                    style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor, borderRadius: 6}]}
                                    imageStyle={{ borderRadius: 6 }}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: 'https://img.icons8.com/color/48/000000/card-verification-value.png'}}
                                    />
                                    <Text style={styles.btnText}>Gestionar tarjetas</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}
