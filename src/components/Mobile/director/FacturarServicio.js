import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';
import MultiBotonRow from "../Globales/MultiBotonRow";
import {Actions} from 'react-native-router-flux';
import Feather from "react-native-vector-icons/Feather";
import Spinner from "react-native-loading-spinner-overlay";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');


export default class FacturarServicio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pagados: [],
            botonSelected : "Facturar Servicio",
            facturas: [],
            visible: false,
            cargo: [],
            escuela_mexico_id: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getIdEscuela();
    }

    getIdEscuela(){
        this.setState({visible: true})
        let nombreCorto = '';
        let nombreCorto1 = '';
        if(this.state.uri === 'https://controlescolar.pro'){
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.pro', '');
        }else{
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.controlescolar.pro', '');
        }
        console.log('http://127.0.0.1:8080' + '/api/get/idEscuelaMexico/' + nombreCorto1)
        fetch('http://127.0.0.1:8080' + '/api/get/idEscuelaMexico/' + nombreCorto1 ,{
			method: 'GET',
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos',
						'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 7)',
						[{text: 'Entendido'}]
					);
				} else {
                   this.setState({escuela_mexico_id: responseJson.id_escuela_mexico})
                   console.log('id_escuela_mexico', this.state.escuela_mexico_id)
                   this.getCargos();
                   this.getFacturas();
				}
			});
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    getCargos() {
        this.setState({visible: true})
        fetch('http://127.0.0.1:8080/api/get/pagados/' + this.state.escuela_mexico_id, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    this.setState({pagados: responseJson})
                    this.setState({visible: false})
                }
            });
    }

    getDatosFacturacion(item){
        console.log(item)
        fetch(this.state.uri + '/api/get/datosReceptor', {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    console.log(responseJson)
                    this.setState({datos: responseJson})
                    Actions.detalleFacturaServicio({datos: responseJson, cargo: item});
                }
            });
    }

    getFacturas() {
        fetch('http://127.0.0.1:8080/api/get/facturas/' + this.state.escuela_mexico_id, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    console.log(responseJson)
                    this.setState({facturas: responseJson})
                }
            });
    }

    renderHistorial() {
        let cards = [];
        this.state.pagados.forEach((item, i) => {
            cards.push(
                <View>
                    <View
                        key={i + 'carta'}
                        style={[
                            styles.widthall,
                            // styles.row,
                            {
                                borderWidth: 1,
                                borderRadius: 6,
                                borderColor: this.state.secondColor,
                                backgroundColor: this.state.fourthColor,
                                paddingHorizontal: 6,
                                paddingVertical: 10,
                                marginTop: 10,

                            }
                        ]}>
                        <View style={[styles.row, styles.widthall, {justifyContent: "flex-start"}]}>
                            <View style={[styles.btn3]}>
                                <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                    Monto del cargo
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'left',
                                    marginTop: 5
                                }}>{item.monto}</Text>
                            </View>
                            <View style={[styles.btn3]}>
                                <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                    Fecha límite de pago
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center',
                                    marginTop: 5
                                }}>{moment(item.mes).format('ll')}</Text>
                            </View>
                            <View style={[styles.btn3, {alignItems: 'center', textAlign: 'center'}]}>
                                <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                    Pago
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center',
                                    marginTop: 5
                                }}>{item.pago_numero}</Text>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity
                            onPress={() => this.getDatosFacturacion(item)}
                            style={[styles.widthall,{alignItems: 'center', justifyContent: 'center', height: responsiveHeight(3.6),backgroundColor: this.state.secondColor, borderRadius: 5, marginTop: -3}]}>
                            <Text style={{color: 'white'}}>Facturar</Text>
                        </TouchableOpacity>

                </View>
            )
        });
        return cards;
    }

    renderFacturas() {
        let cards = [];
        this.state.facturas.forEach((item, i) => {
            cards.push(
                <View key={i + 'carta'}>
                    <View
                        style={[
                            styles.widthall,
                            // styles.row,
                            {
                                borderWidth: 1,
                                borderRadius: 6,
                                borderColor: this.state.secondColor,
                                backgroundColor: this.state.fourthColor,
                                paddingHorizontal: 6,
                                paddingVertical: 10,
                                marginTop: 10,

                            }
                        ]}>
                        <View style={[styles.row, styles.widthall, {justifyContent: "flex-start"}]}>
                            <View style={[styles.btn3]}>
                                <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                    Monto del cargo
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'left',
                                    marginTop: 5
                                }}>{item.monto}</Text>
                            </View>
                            <View style={[styles.btn3]}>
                                <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                    Fecha límite de pago
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center',
                                    marginTop: 5
                                }}>{moment(item.mes).format('ll')}</Text>
                            </View>
                            <View style={[styles.btn3, {alignItems: 'center', textAlign: 'center'}]}>
                                <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                    Pago
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center',
                                    marginTop: 5
                                }}>{item.pago_numero}</Text>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity
                        // onPress={() => this.pagar(i)}
                        style={[styles.row,{alignItems: 'center', justifyContent: 'center', height: responsiveHeight(3.6),backgroundColor: this.state.secondColor, borderRadius: 5, marginTop: -3}]}>
                        <Text style={{color: 'white'}}>Enviar al correo</Text>
                        <Feather name='mail' size={20} color='#fff'
                                 style={{marginLeft: 10, paddingVertical: 3}}
                        />
                    </TouchableOpacity>

                </View>
            )
        });
        return cards;
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    facturar(){
        return(
            <View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione el pago
                </Text>
                {this.renderHistorial()}
            </View>
        )
    }

    historial(){
        return (
            <View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione la factura
                </Text>
                {this.renderFacturas()}
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Spinner visible={this.state.visible} textContent='Consultando pagos...'/>
                    <View>
                        <MultiBotonRow
                            itemBtns={["Facturar Servicio", "Historial de facturas"]}
                            onSelectedButton={(indexBtn, itemBtn) =>
                                this.botonSelected(indexBtn, itemBtn)
                            }
                            cantidad={2}
                        />
                        {this.state.botonSelected === "Facturar Servicio" ? this.facturar() : null}
                        {this.state.botonSelected === "Historial de facturas" ? this.historial() : null}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
