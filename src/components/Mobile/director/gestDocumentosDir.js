import React, {
	Component
} from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	ScrollView,
	TouchableOpacity,
	StyleSheet,
	Modal,
	Linking,
	WebView
} from 'react-native';
import {deviceHeight, responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import moment from 'moment';
import {Actions} from 'react-native-router-flux';

export default class gestDocumentosDir extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			botonSelected: 'Ver documentos',
			document: null,
			carpetas: [],
			modalVisible: false,
			modalVisible1: false,
			pdf: null
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getCarpetas();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			text: '',
			modalVisible: false,
			archivos: [],
			mostrar: false
		});
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected,
			indexSelected: indexSelected
		});
	}

	setModalVisible(visible) {
		this.setState({modalVisible: visible});
	}

	setModalVisible1(visible) {
		this.setState({modalVisible1: visible});
	}

	async getCarpetas() {
		await fetch(this.state.uri + '/api/get/carpetas',
			{
				method: 'get',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			})
			.then(response => {
				return response.json();

			}).then(responseJson => {
				if (responseJson.error === null) {
					Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Documentos)');
				} else {
					this.setState({carpetas: []});
					this.setState({carpetas: responseJson});
					if (this.state.carpetas.length === 0) {
						Alert.alert('Aún no existen carpetas para visualizar');
					}
				}
			});
	};

	getArchivos(id) {
		fetch(this.state.uri + '/api/get/files/' + id,
			{
				method: 'get',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			})
			.then(response => {
				return response.json();

			}).then(responseJson => {
			if (responseJson.error === null) {
				Alert.alert('¡Ups! Ha ocurrido un error , Sí el error continua pónganse en contacto con soporte (Documentos)');
			} else {
				this.setState({archivos: []});
				this.setState({archivos: responseJson});
				if (this.state.archivos.length > 0) {
					this.setModalVisible(true);

				} else {
					Alert.alert('No existen documentos en esta carpeta');
					this.setModalVisible(false);
				}
			}
		});
	}

	async link(url) {
		await Linking.openURL(url);
	}

	card() {
		let carta = [];
		this.state.carpetas.forEach((item, i) => {
			carta.push(
				<TouchableOpacity
					onPress={() => [Actions.ListaDocumentos({listaArchivos: item.id})]}
					key={i + 'carta'}
					style={[
						styles.widthall,
						// styles.row,
						{
							borderWidth: 0.5,
							marginVertical: 5,
							borderRadius: 6,
							borderColor: 'lightgray',
							backgroundColor: this.state.fourthColor,
							paddingHorizontal: 10,
							paddingVertical: 10
						}
					]}>
					<View style={[styles.row, styles.widthall, {justifyContent: 'flex-start'}]}>
						<View style={[styles.btn3_4, {marginRight: 10}]}>
							<Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Nombre de la carpeta
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
								marginTop: 3
							}}>{item.nombre}</Text>
						</View>
						<View style={[styles.btn5]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Fecha
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>{moment(item.created_at).format('L')}</Text>
						</View>
					</View>
				</TouchableOpacity>
			)
		});
		return carta;
	}





	render() {
		return (
			<View style={styles.container}>
				{this.state.modalVisible === true ? (
					(<ListaDocumentos/>)
				): (
					<View style={styles.container}>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione la carpeta a consultar
					</Text>
					<ScrollView>
						{this.state.carpetas.lenght > 0 ?
							(<View style={[styles.row, {marginTop: 10}]}>
								<Text>Nombre</Text>
								<Text>Fecha de creacion</Text>
							</View>) : null
						}
						{this.card()}
					</ScrollView>
				</View>)}
			</View>
		);
	}
}

