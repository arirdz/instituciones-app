import React from 'react';
import {AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveWidth} from 'react-native-responsive-dimensions';
import MultiBotonRow from './../Globales/MultiBotonRow';
import styles from '../../styles';
import Modal from 'react-native-modal';
import Prospectos from './../director/Prospectos';
import DiaInmersion from './../director/DiaInmersion';
import CitasAspirante from './../director/CitasAspirante';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class newAspirantes extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			altas: [],
			indexSelected: 0,
			modalTutor: [],
			tipoAlta: null,
			NuevoIngreso: [],
			traslados: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getAlumnosByGG();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async botonSelected(indexBtn, itemBtn) {
		await this.setState({
			botonSelected: itemBtn,
			indexSelected: indexBtn
		});
		if (this.state.indexSelected === 3) {
			await this.getAlumnosByGG();
		}
	}

//+-+-+-+--+Validar aspirante


	// +++++++++++++++++++++++++++++ Alumnos grado y grupo +++++++++++++++++++++++
	async getAlumnosByGG() {
		await fetch(this.state.uri + '/api/get/aspirantes/alta', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita aspirante)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					let ingreso = [];
					let traslados = [];
					this.setState({alumnos: responseJson});
					if (this.state.alumnos.length !== 0) {
						this.state.alumnos.forEach((it) => {
							this.state.modalTutor.push(false);
							if (it.tipo_alta === 'ingreso') {
								ingreso.push(it)
							} else if (it.tipo_alta === 'traslado') {
								traslados.push(it);
							}
						})
					}
					this.state.NuevoIngreso = ingreso;
					this.state.traslados = traslados;
				}
			});
		await this.setState({aux: 0});
	}

	async openModalT(i) {
		this.state.modalTutor[i] = true;
		await this.setState({aux: 0});
	}

	async closeModalT(i) {
		this.state.modalTutor[i] = false;
		await this.setState({aux: 0});
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		if (this.state.alumnos.length !== 0) {
			this.state.alumnos.forEach((it, ix) => {
				let a = this.state.alumnos.length - 1;
				let tabRow = [styles.rowTabla];
				if (ix !== a) {
					tabRow.push({borderColor: this.state.secondColor});
				}
				if (it.tipo_alta === this.state.tipoAlta || this.state.tipoAlta === null) {
					buttonsAlumnos.push(
						<View
							key={ix+'altas'}
							underlayColor={this.state.secondColor}
							style={[tabRow]}>
							<Modal
								isVisible={this.state.modalTutor[ix]}
								backdropOpacity={0.8}
								animationIn={'bounceIn'}
								animationOut={'bounceOut'}
								animationInTiming={1000}
								animationOutTiming={1000}
							>
								<View
									style={[styles.container, {borderRadius: 6, flex: 0}]}
								>
									<Text
										style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
										Datos del tutor
									</Text>
									<View style={[styles.modalWidth, {paddingHorizontal: 10}]}>
										<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Nombre:
											<Text style={[styles.textW]}> {it.tutor.nombre}</Text>
										</Text>
										<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Apellido(s):
											<Text style={[styles.textW]}> {it.tutor.apellidos}</Text>
										</Text>
										<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Telefono de
																										 contacto:
											<Text style={[styles.textW]}> {it.tutor.telefono_contacto}</Text>
										</Text>
										<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Email:
											<Text style={[styles.textW]}> {it.tutor.email}</Text>
										</Text>
									</View>
									<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{backgroundColor: '#fff', borderColor: this.state.secondColor}
										]}
										onPress={() => this.closeModalT(ix)}
									>
										<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
									</TouchableOpacity>
								</View>
							</Modal>
							<View style={[styles.campoTablaG]}>
								<View style={[styles.row, styles.ajustMovAlum, {paddingHorizontal: 10}]}>
									<View style={{width: responsiveWidth(52)}}>
										<Text style={[styles.textW]}>{it.nombre_aspirante}</Text>
										<Text>
											Grado al que aplicó: <Text style={[styles.textW]}>{it.grado_interes}</Text>
										</Text>
									</View>
									<TouchableOpacity
										style={[styles.btn6,
											{
												borderRadius: 6,
												backgroundColor: this.state.mainColor,
												paddingVertical: 2
											}]}
										onPress={() => this.openModalT(ix)}
									>
										<Text
											style={{textAlign: 'center', color: '#fff'}}
										>
											Tutor
										</Text>
									</TouchableOpacity>
								</View>
							</View>
						</View>
					);
				}
			});
		}
		return buttonsAlumnos;
	}


	btnsAltas() {
		return (
			<View style={[styles.widthall, styles.row, {marginTop: 3}]}>
				<TouchableOpacity
					style={[styles.btn3,
						{
							borderRadius: 6,
							padding: 5,
							backgroundColor: this.state.tipoAlta === null ? this.state.mainColor : this.state.fourthColor
						}]}
					onPress={() => this.setState({tipoAlta: null})}
				>
					<Text
						style={[
							{color: this.state.tipoAlta === null ? '#fff' : '#000', textAlign: 'center'}]}
					>
						Total altas
					</Text>
					<Text
						style={[styles.textW,
							{
								color: this.state.tipoAlta === null ? '#fff' : '#000',
								textAlign: 'center',
								fontSize: responsiveFontSize(2.5)
							}]}
					>
						{this.state.alumnos.length}
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					style={[styles.btn3,
						{
							borderRadius: 6,
							padding: 5,
							backgroundColor: this.state.tipoAlta === 'ingreso' ? this.state.mainColor : this.state.fourthColor
						}]}
					onPress={() => this.setState({tipoAlta: 'ingreso'})}
				>
					<Text
						style={[
							{color: this.state.tipoAlta === 'ingreso' ? '#fff' : '#000', textAlign: 'center'}]}
					>
						Nuevo ingreso
					</Text>
					<Text
						style={[styles.textW,
							{
								color: this.state.tipoAlta === 'ingreso' ? '#fff' : '#000',
								textAlign: 'center',
								fontSize: responsiveFontSize(2.5)
							}]}
					>
						{this.state.NuevoIngreso.length}
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					style={[styles.btn3,
						{
							borderRadius: 6,
							padding: 5,
							backgroundColor: this.state.tipoAlta === 'traslado' ? this.state.mainColor : this.state.fourthColor
						}]}
					onPress={() => this.setState({tipoAlta: 'traslado'})}
				>
					<Text
						style={[
							{color: this.state.tipoAlta === 'traslado' ? '#fff' : '#000', textAlign: 'center'}]}
					>
						Traslado
					</Text>
					<Text
						style={[styles.textW,
							{
								color: this.state.tipoAlta === 'traslado' ? '#fff' : '#000',
								textAlign: 'center',
								fontSize: responsiveFontSize(2.5)
							}]}
					>
						{this.state.traslados.length}
					</Text>
				</TouchableOpacity>
			</View>
		)
	}

	rndAltas() {
		let lista = 0;
		if (this.state.tipoAlta === null) {
			lista = this.state.alumnos.length;
		} else if (this.state.tipoAlta === 'ingreso') {
			lista = this.state.NuevoIngreso.length;
		} else if (this.state.tipoAlta === 'traslado') {
			lista = this.state.traslados.length;
		}
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione tipo de consulta
				</Text>
				{this.btnsAltas()}
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Lista de altas
				</Text>
				<ScrollView
					overScrollMode='always'
					style={{...ifIphoneX({marginBottom: 36}, {marginBottom: 20})}}
					showsVerticalScrollIndicator={false}
				>
					{lista !== 0 ?
						<View
							style={[
								styles.tabla,
								{borderColor: this.state.secondColor, marginTop: 3}
							]}
						>
							{this.renderAlumnos()}
						</View> : <Text style={[styles.main_title, {
							color: '#a8a8a8',
							textAlign: 'center',
							marginTop: 40,
							marginBottom: 40,
							fontSize: responsiveFontSize(3)
						}]}>
							No hay altas registradas aún
						</Text>}
				</ScrollView>
			</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<MultiBotonRow
					itemBtns={[
						'Ver\nprospectos',
						'Día de inmersión',
						'Citas\nagendadas',
						'Estadística \nde altas'
					]}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelected(indexBtn, itemBtn)
					}
					cantidad={4}
				/>
				{this.state.indexSelected === 0 ? <Prospectos/> :
					this.state.indexSelected === 1 ? <DiaInmersion/> :
						this.state.indexSelected === 2 ? <CitasAspirante/> : this.rndAltas()}
			</View>
		);
	}
}
