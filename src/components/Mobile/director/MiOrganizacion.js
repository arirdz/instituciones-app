import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight,
    KeyboardAvoidingView, Alert, RefreshControl, Image, Platform
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import MultiBotonRow from "../Globales/MultiBotonRow";
import Ionicons from "react-native-vector-icons/Ionicons";
import Fumi from "react-native-textinput-effects/lib/Fumi";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Switch from "react-native-switch-pro";
import Modal from "react-native-modal";
import ModalSelector from "react-native-modal-selector";
import Spinner from "react-native-loading-spinner-overlay";

export default class MiOrganizacion extends React.Component {
    _onRefresh = () => {
        this.setState({refreshing: true});
       this.getAlias();
       this.getPuestos();
    };
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            botonSelected: 'Gestionar puestos',
            elIndexSelected: 0,
            nombre_Subdirector: '',
            correo_Subdirector: '',
            nombre_cordAcad: '',
            correo_cordAcad: '',
            nombre_cordAdmin: '',
            correo_cordAdmin: '',
            nombre_cordConduc: '',
            correo_cordConduc: '',
            nombre_prefecto: '',
            correo_prefecto: '',
            nombre_medico: '',
            correo_medico: '',
            nombre_Psicologo: '',
            correo_Psicologo: '',
            prefectos: ['1'],
            elSubdirector: true,
            elCordAcad: true,
            elCordAdmin: true,
            elCordConduc: true,
            elMedico: true,
            elMedico1: true,
            visible: false,
            elAlias: '',
            nuevoNombre: '',
            tituloSubdirector: 'Subdirector',
            tituloCordAcad: 'Coordinador Académico',
            tituloCordAdmin: 'Coordinador Administrativo',
            tituloCordCond: 'Coordinador de Conducta',
            tituloPrefecto: 'Prefecto',
            tituloMedico: 'Medico',
            tituloMedico1: 'Psicólogo',
            refreshing: false,
            puestos: [],
            funcionalidades: [],
            spinner: false,
            spinnerFuncionalidades: false
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getAlias();
        await this.getPuestos();
        await this.getPersonal();
    }

    getPersonal(){
        fetch('http://127.0.0.1:8000/api/get/personal', {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error', 'Ha ocurrido un error, por favor intente más tarde',
                        [{text: 'Entendido'}]
                    );
                } else {
                    console.log(responseJson)
                    if(responseJson[0] !== null){
                        this.setState({nombre_Subdirector: responseJson[0].name, correo_Subdirector: responseJson[0].email})
                    }
                    if(responseJson[1] !== null){
                        this.setState({nombre_cordAcad: responseJson[1].name, correo_cordAcad: responseJson[1].email})
                    }
                    if(responseJson[2] !== null){
                        this.setState({nombre_cordAdmin: responseJson[1].name, correo_cordAdmin: responseJson[2].email})
                    }
                    if(responseJson[3] !== null){
                        this.setState({nombre_cordConduc: responseJson[1].name, correo_cordConduc: responseJson[3].email})
                    }
                    if(responseJson[4] !== null){
                        this.setState({nombre_prefecto: responseJson[1].name, correo_prefecto: responseJson[4].email})
                    }
                    if(responseJson[5] !== null){
                        this.setState({nombre_medico: responseJson[1].name, correo_medico: responseJson[5].email})
                    }

                }
            });
    }


    getAlias(){
        fetch(this.state.uri + '/api/get/Alias', {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error', 'Ha ocurrido un error, por favor intente más tarde',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({refreshing: false});
                    if (responseJson[0] !== null){
                        if(responseJson[0].alias !== null && responseJson[0].alias !== ''){
                            this.setState({tituloSubdirector: responseJson[0].alias})
                        }
                    }
                    if (responseJson[1] !== null) {
                        if (responseJson[1].alias !== null && responseJson[1].alias !== '') {
                            this.setState({tituloCordAcad: responseJson[1].alias})
                        }
                    }
                    if (responseJson[2] !== null) {
                        if (responseJson[2].alias !== null && responseJson[2].alias !== '') {
                            this.setState({tituloCordAdmin: responseJson[2].alias})
                        }
                    }
                    if (responseJson[3] !== null) {
                        if (responseJson[3].alias !== null && responseJson[3].alias !== '') {
                            this.setState({tituloCordCond: responseJson[3].alias})
                        }
                    }
                    if (responseJson[4] !== null) {
                        if (responseJson[4].alias !== null && responseJson[4].alias !== '') {
                            this.setState({tituloPrefecto: responseJson[4].alias})
                        }
                    }
                    if (responseJson[5] !== null) {
                        if (responseJson[5].alias !== null && responseJson[5].alias !== '') {
                            this.setState({tituloMedico: responseJson[5].alias})
                        }
                    }
                }
            });
    }


    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected, indexSelected: indexSelected
        });
        if(indexSelected === 0){
            this.setState({funcionalidades: []})
        }
    }

    requestMultipart(puesto) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        console.log(puesto)
        if (puesto === 'Subdirector'){
            if (this.state.nombre_Subdirector === ''){
                Alert.alert(
                    'Error', 'Ingrese el nombre del Subdirector',
                    [{text: 'Entendido'}]
                );
            }else if (reg.test(this.state.correo_Subdirector) === false){
                Alert.alert(
                    'Error', 'El e-mail del Subdirector es incorrecto',
                    [{text: 'Entendido'}]
                );
            }else {
                fetch(this.state.uri + '/api/crear/Admins', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: JSON.stringify({
                        nombre: this.state.nombre_Subdirector,
                        email: this.state.correo_Subdirector,
                        role: 'Admin',
                        puesto: 'Subdirector',
                    })
                }).then(res => res.json())
                    .then(responseJson => {
                        console.log(responseJson)
                        if (responseJson.error !== undefined) {
                            Alert.alert(
                                'Error', 'Ha ocurrido un error, intente de nuevo mas tarde',
                                [{text: 'Entendido'}]
                            );
                        } else {
                            console.log(responseJson)
                            Alert.alert(
                                'Felicidades', 'El usuario ya se ha registrado y a continuación recibirá un correo con las credenciales para poder acceder a la plataforma.',
                                [{text: 'Entendido'}]
                            );
                        }
                    });
            }
        }else if(puesto === 'Coordinador Academico'){
            if (this.state.nombre_cordAcad === ''){
                Alert.alert(
                    'Error', 'Ingrese el nombre del Coordinador Academico',
                    [{text: 'Entendido'}]
                );
            }else if (reg.test(this.state.correo_cordAcad) === false){
                Alert.alert(
                    'Error', 'El e-mail del Coordinador Academico es incorrecto',
                    [{text: 'Entendido'}]
                );
            }else {
                fetch(this.state.uri + '/api/crear/Admins', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: JSON.stringify({
                        nombre: this.state.nombre_cordAcad,
                        email: this.state.correo_cordAcad,
                        role: 'Admin',
                        puesto: 'Coordinador Academico',
                    })
                }).then(res => res.json())
                    .then(responseJson => {
                        console.log(responseJson)
                        if (responseJson.error !== undefined) {
                            Alert.alert(
                                'Error', 'Ha ocurrido un error, intente de nuevo mas tarde',
                                [{text: 'Entendido'}]
                            );
                        } else {
                            console.log(responseJson)
                            Alert.alert(
                                'Felicidades', 'El usuario ya se ha registrado y a continuación recibirá un correo con las credenciales para poder acceder a la plataforma.',
                                [{text: 'Entendido'}]
                            );
                        }
                    });
            }
        }else if (puesto === 'Coordinador Administrativo'){
            if (this.state.nombre_cordAdmin === ''){
                Alert.alert(
                    'Error', 'Ingrese el nombre del Coordinador Administrativo',
                    [{text: 'Entendido'}]
                );
            }else if (reg.test(this.state.correo_cordAdmin) === false){
                Alert.alert(
                    'Error', 'El e-mail del Coordinador Administrativo es incorrecto',
                    [{text: 'Entendido'}]
                );
            }else {
                fetch(this.state.uri + '/api/crear/Admins', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: JSON.stringify({
                        nombre: this.state.nombre_cordAdmin,
                        email: this.state.correo_cordAdmin,
                        role: 'Admin',
                        puesto: 'Secretaria',
                    })
                }).then(res => res.json())
                    .then(responseJson => {
                        console.log(responseJson)
                        if (responseJson.error !== undefined) {
                            Alert.alert(
                                'Error', 'Ha ocurrido un error, intente de nuevo mas tarde',
                                [{text: 'Entendido'}]
                            );
                        } else {
                            console.log(responseJson)
                            Alert.alert(
                                'Felicidades', 'El usuario ya se ha registrado y a continuación recibirá un correo con las credenciales para poder acceder a la plataforma.',
                                [{text: 'Entendido'}]
                            );
                        }
                    });
            }
        }else if (puesto === 'Coordinador de Conducta'){
            console.log(this.state.uri + '/api/crear/Admins')
            if (this.state.nombre_cordConduc === ''){
                Alert.alert(
                    'Error', 'Ingrese el nombre del Coordinador de Conducta',
                    [{text: 'Entendido'}]
                );
            }else if (reg.test(this.state.correo_cordConduc) === false){
                Alert.alert(
                    'Error', 'El e-mail del Coordinador de Conducta es incorrecto',
                    [{text: 'Entendido'}]
                );
            }else {
                fetch(this.state.uri + '/api/crear/Admins', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: JSON.stringify({
                        nombre: this.state.nombre_cordConduc,
                        email: this.state.correo_cordConduc,
                        role: 'Admin',
                        puesto: 'Cordisi',
                    })
                }).then(res => res.json())
                    .then(responseJson => {
                        console.log(responseJson)
                        if (responseJson.error !== undefined) {
                            Alert.alert(
                                'Error', 'Ha ocurrido un error, intente de nuevo mas tarde',
                                [{text: 'Entendido'}]
                            );
                        } else {
                            console.log(responseJson)
                            Alert.alert(
                                'Felicidades', 'El usuario ya se ha registrado y a continuación recibirá un correo con las credenciales para poder acceder a la plataforma.',
                                [{text: 'Entendido'}]
                            );
                        }
                    });
            }
        }else if (puesto === 'Prefecto') {
            if (this.state.nombre_prefecto === '') {
                Alert.alert(
                    'Error', 'Ingrese el nombre del Prefecto',
                    [{text: 'Entendido'}]
                );
            } else if (reg.test(this.state.correo_prefecto) === false) {
                Alert.alert(
                    'Error', 'El e-mail del Prefecto es incorrecto',
                    [{text: 'Entendido'}]
                );
            } else {
                fetch(this.state.uri + '/api/crear/Admins', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: JSON.stringify({
                        nombre: this.state.nombre_prefecto,
                        email: this.state.correo_prefecto,
                        role: 'Prefecto',
                        puesto: 'Prefecto',
                    })
                }).then(res => res.json())
                    .then(responseJson => {
                        console.log(responseJson)
                        if (responseJson.error !== undefined) {
                            Alert.alert(
                                'Error', 'Ha ocurrido un error, intente de nuevo mas tarde',
                                [{text: 'Entendido'}]
                            );
                        } else {
                            console.log(responseJson)
                            Alert.alert(
                                'Felicidades', 'El usuario ya se ha registrado y a continuación recibirá un correo con las credenciales para poder acceder a la plataforma.',
                                [{text: 'Entendido'}]
                            );
                        }
                    });
            }
        }else if (puesto === 'Medico') {
            if (this.state.nombre_medico === '') {
                Alert.alert(
                    'Error', 'Ingrese el nombre del Médico',
                    [{text: 'Entendido'}]
                );
            } else if (reg.test(this.state.correo_medico) === false) {
                Alert.alert(
                    'Error', 'El e-mail del Médico es incorrecto',
                    [{text: 'Entendido'}]
                );
            } else {
                fetch(this.state.uri + '/api/crear/Admins', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: JSON.stringify({
                        nombre: this.state.nombre_medico,
                        email: this.state.correo_medico,
                        role: 'Medico',
                        puesto: 'Medico',
                    })
                }).then(res => res.json())
                    .then(responseJson => {
                        console.log(responseJson)
                        if (responseJson.error !== undefined) {
                            Alert.alert(
                                'Error', 'Ha ocurrido un error, intente de nuevo mas tarde',
                                [{text: 'Entendido'}]
                            );
                        } else {
                            console.log(responseJson)
                            Alert.alert(
                                'Felicidades', 'El usuario ya se ha registrado y a continuación recibirá un correo con las credenciales para poder acceder a la plataforma.',
                                [{text: 'Entendido'}]
                            );
                        }
                    });
            }
        }

    }

    agregarAlias(nombre){
        console.log(this.state.elSubdirector)
        if (this.state.elSubdirector === true && nombre === 'Subdirector'){
            this.setState({elAlias: nombre, visible:true})
        }
        if (this.state.elCordAcad  === true && nombre === 'Coordinador Academico'){
            this.setState({elAlias: nombre, visible:true})
        }
        if (this.state.elCordAdmin  === true && nombre === 'Coordinador Administrativo'){
            this.setState({elAlias: nombre, visible:true})
        }
        if (this.state.elCordConduc  === true && nombre === 'Coordinador de Conducta'){
            this.setState({elAlias: nombre, visible:true})
        }
        if (this.state.elMedico  === true && nombre === 'Medico'){
            this.setState({elAlias: nombre, visible:true})
        }
        if (this.state.elMedico1 === true && nombre === 'Medico1'){
            this.setState({elAlias: nombre, visible:true})
        }
    }

    renderPrefectos() {
        let buttonstyle1= [{backgroundColor: this.state.secondColor, width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}]
        let cards = [];
        this.state.prefectos.forEach((item, i) => {
            cards.push(
                <View
                    style={[
                        styles.widthall,
                        // styles.row,
                        {
                            borderWidth: 0,
                            borderRadius: 9,
                            borderColor: 'grey',
                            backgroundColor: this.state.fourthColor,
                            paddingHorizontal: 6,
                            paddingVertical: 10,
                            marginBottom: 10,
                            marginTop: 5,
                            alignItems: 'center'
                        }
                    ]}>
                    <View style={[styles.row, styles.widthall, {marginVertical: 5,justifyContent: "space-between"}]}>
                        <Text style={[{
                            width: responsiveWidth(65), marginLeft: 15, color: "black", fontWeight: '500', fontSize: 20}]}>{this.state.tituloPrefecto} {i+1}</Text>
                        <FontAwesome
                            name='edit'
                            color='black' size={22}
                            style={{marginLeft: 10}}
                            onPress={() => [this.agregarAlias('Prefecto')]}
                        />
                        <Entypo
                            name='circle-with-minus'
                            color='red' size={22}
                            onPress={() => [this.quitarPrefectos()]}
                            style={{marginRight: 10}}
                        />
                    </View>
                    <Fumi
                        ref='apellidoMat'
                        label={'Nombre'}
                        iconClass={Ionicons}
                        style={[{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}]}
                        labelStyle={{color: this.state.mainColor}}
                        iconName={'md-person'}
                        inputStyle={{color: this.state.secondColor}}
                        iconColor={this.state.mainColor}
                        iconSize={20}
                        onChangeText={text => (this.state.prefecto = text)}
                        keyboardType='default'
                        value={this.state.nombre_prefecto}
                        returnKeyType='next'
                        autoCapitalize='none'
                        autoCorrect={false}
                    />
                    <Fumi
                        ref='apellidoMat'
                        label={'Correo'}
                        iconClass={Ionicons}
                        style={[{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}]}
                        labelStyle={{color: this.state.mainColor}}
                        iconName={'md-mail'}
                        inputStyle={{color: this.state.secondColor}}
                        iconColor={this.state.mainColor}
                        iconSize={20}
                        onChangeText={text => (this.state.correo_prefecto = text)}
                        keyboardType='email-address'
                        value={this.state.correo_prefecto}
                        returnKeyType='next'
                        autoCapitalize='none'
                        autoCorrect={false}
                    />
                    <TouchableOpacity
                        onPress={() => this.requestMultipart('Prefecto')}
                        style={buttonstyle1}
                    ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                    </TouchableOpacity>
                </View>
            )
        });
        return cards;
    }


    gestionarPuestos(){
        let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.elIndexSelected === 1) {
            btnCal.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7), fontWeight: '700'
            });
        }
        let texto1 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnTem = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.elIndexSelected === 0) {
            btnTem.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto1.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7), fontWeight: '700'
            });
        }
        let texto2 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnTem2 = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.elIndexSelected === 2) {
            btnTem2.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto2.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7), fontWeight: '700'
            });
        }

        ///Subdirector
        let styleinput1= [{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90), marginTop: 5}];
        if (this.state.elSubdirector === false){
           styleinput1= [{backgroundColor: this.state.fourthColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90), marginTop: 5}];
        }

        let buttonstyle1= [{backgroundColor: this.state.secondColor, width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}]
        if (this.state.elSubdirector === false){
            buttonstyle1 = [{backgroundColor: 'gray', width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}];
        }

        ///CordAcad
        let styleinput2= [{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        if (this.state.elCordAcad === false){
            styleinput2= [{backgroundColor: this.state.fourthColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        }

        let buttonstyle2= [{backgroundColor: this.state.secondColor, width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}]
        if (this.state.elCordAcad === false){
            buttonstyle2 = [{backgroundColor: 'gray', width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}];
        }

        ///CordAdmin
        let styleinput3= [{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        if (this.state.elCordAdmin === false){
            styleinput3= [{backgroundColor: this.state.fourthColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        }

        let buttonstyle3= [{backgroundColor: this.state.secondColor, width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}]
        if (this.state.elCordAdmin === false){
            buttonstyle3 = [{backgroundColor: 'gray', width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}];
        }

        ///CordConducta
        let styleinput4= [{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        if (this.state.elCordConduc === false){
            styleinput4= [{backgroundColor: this.state.fourthColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        }

        let buttonstyle4= [{backgroundColor: this.state.secondColor, width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}]
        if (this.state.elCordConduc === false){
            buttonstyle4 = [{backgroundColor: 'gray', width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}];
        }

        ///Medico
        let styleinput5= [{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        if (this.state.elMedico === false){
            styleinput5= [{backgroundColor: this.state.fourthColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        }

        let buttonstyle5= [{backgroundColor: this.state.secondColor, width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}]
        if (this.state.elMedico === false){
            buttonstyle5 = [{backgroundColor: 'gray', width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}];
        }

        ///Medico1
        let styleinput6= [{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        if (this.state.elMedico1 === false){
            styleinput6= [{backgroundColor: this.state.fourthColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(90),  marginTop: 5}];
        }

        let buttonstyle6= [{backgroundColor: this.state.secondColor, width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}]
        if (this.state.elMedico1 === false){
            buttonstyle6 = [{backgroundColor: 'gray', width: responsiveWidth(60), height: responsiveHeight(3.5), margin: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center'}];
        }

        return(
            <View behavior='padding' keyboardVerticalOffset={5} style={[styles.container,{backgroundColor: "transparent"}]}>
                <Text style={styles.main_title}>Seleccione una categoría</Text>
                <View>
                    <View style={[styles.widthall, {alignItems: 'center'}]}>
                        <View
                            style={[styles.rowsCalif, {
                                width: responsiveWidth(92), marginTop: 5, marginBottom: 0
                            }]}>
                            <TouchableHighlight
                                underlayColor={'transparent'}
                                style={btnTem}
                                onPress={() => this.elBotonSelected(0)}>
                                <Text style={texto1}>Administrativos</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={'transparent'}
                                style={btnCal}
                                onPress={() => this.elBotonSelected(1)}>
                                <Text style={texto}>Prefectos</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={'transparent'}
                                style={btnTem2}
                                onPress={() => this.elBotonSelected(2)}>
                                <Text style={texto2}>Otros</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                    <Text style={styles.main_title}>Edite el puesto que desee:</Text>
                    {this.state.elIndexSelected === 0 ?
                        (<View style={{marginBottom: 20}}>
                            <View
                                style={[
                                    styles.widthall,
                                    // styles.row,
                                    {
                                        borderWidth: 0,
                                        borderRadius: 9,
                                        borderColor: 'grey',
                                        backgroundColor: this.state.fourthColor,
                                        paddingHorizontal: 6,
                                        paddingVertical: 10,
                                        marginBottom: 10,
                                        marginTop: 5,
                                        alignItems: 'center'
                                    }
                                ]}>
                                <View style={[styles.row, styles.widthall, {marginVertical: 5, justifyContent: "space-between"}]}>
                                    <Text style={[{
                                        width: responsiveWidth(65), marginLeft: 15, color: "black", fontWeight: '500', fontSize: 20}]}>{this.state.tituloSubdirector}</Text>
                                    <FontAwesome
                                        name='edit'
                                        color='black' size={22}
                                        style={{marginRight: 0}}
                                        onPress={() => [this.agregarAlias('Subdirector')]}
                                    />
                                    <Switch
                                        style={{marginRight:10}}
                                        value={this.state.elSubdirector}
                                        onSyncPress={() => this.cambiarValor('Subdirector')}
                                    />
                                </View>
                                <Fumi
                                    ref='apellidoMat'
                                    label={'Nombre'}
                                    iconClass={Ionicons}
                                    style={styleinput1}
                                    labelStyle={{color: this.state.mainColor}}
                                    iconName={'md-person'}
                                    inputStyle={{color: this.state.secondColor}}
                                    iconColor={this.state.mainColor}
                                    iconSize={20}
                                    onChangeText={text => (this.state.nombre_Subdirector = text)}
                                    keyboardType='default'
                                    value={this.state.nombre_Subdirector}
                                    returnKeyType='next'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    editable={this.state.elSubdirector}
                                />
                                <Fumi
                                    ref='apellidoMat'
                                    label={'Correo'}
                                    iconClass={Ionicons}
                                    style={styleinput1}
                                    labelStyle={{color: this.state.mainColor}}
                                    iconName={'md-mail'}
                                    inputStyle={{color: this.state.secondColor}}
                                    iconColor={this.state.mainColor}
                                    iconSize={20}
                                    onChangeText={text => (this.state.correo_Subdirector = text)}
                                    keyboardType='email-address'
                                    value={this.state.correo_Subdirector}
                                    returnKeyType='next'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    editable={this.state.elSubdirector}
                                />
                                <TouchableOpacity
                                    disabled={!this.state.elSubdirector}
                                    onPress={() => this.requestMultipart('Subdirector')}
                                    style={buttonstyle1}
                                ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <View
                                    style={[
                                        styles.widthall,
                                        // styles.row,
                                        {
                                            borderWidth: 0,
                                            borderRadius: 9,
                                            borderColor: 'grey',
                                            backgroundColor: this.state.fourthColor,
                                            paddingHorizontal: 6,
                                            paddingVertical: 10,
                                            marginVertical: 10,
                                            alignItems: 'center'
                                        }
                                    ]}>
                                    <View style={[styles.row, styles.widthall, {marginVertical: 5,justifyContent: "space-between"}]}>
                                        <Text style={[{
                                            width: responsiveWidth(65), marginLeft: 15, color: "black", fontWeight: '500', fontSize: 20}]}>{this.state.tituloCordAcad}</Text>
                                        <FontAwesome
                                            name='edit'
                                            color='black' size={22}
                                            style={{marginRight: 0}}
                                            onPress={() => [this.agregarAlias('Coordinador Academico')]}
                                        />
                                       <Switch
                                            style={{marginRight:10}}
                                            value={this.state.elCordAcad}
                                            onSyncPress={() => this.cambiarValor('Coordinador Académico')}
                                        />
                                    </View>
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Nombre'}
                                        iconClass={Ionicons}
                                        style={styleinput2}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-person'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.nombre_cordAcad = text)}
                                        keyboardType='default'
                                        value={this.state.nombre_cordAcad}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elCordAcad}
                                    />
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Correo'}
                                        iconClass={Ionicons}
                                        style={styleinput2}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-mail'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.correo_cordAcad = text)}
                                        keyboardType='email-address'
                                        value={this.state.correo_cordAcad}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elCordAcad}
                                    />
                                    <TouchableOpacity
                                        disabled={!this.state.elCordAcad}
                                        onPress={() => this.requestMultipart('Coordinador Academico')}
                                        style={buttonstyle2}
                                    ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View>
                                <View
                                    style={[
                                        styles.widthall,
                                        // styles.row,
                                        {
                                            borderWidth: 0,
                                            borderRadius: 9,
                                            borderColor: 'grey',
                                            backgroundColor: this.state.fourthColor,
                                            paddingHorizontal: 6,
                                            paddingVertical: 10,
                                            marginVertical: 10,
                                            alignItems: 'center'
                                        }
                                    ]}>
                                    <View style={[styles.row, styles.widthall, {marginVertical: 5, justifyContent: "space-between"}]}>
                                        <Text style={[{
                                            width: responsiveWidth(65), marginLeft: 15, color: "black", fontWeight: '500', fontSize: 20}]}>{this.state.tituloCordAdmin}</Text>
                                        <FontAwesome
                                            name='edit'
                                            color='black' size={22}
                                            style={{marginLeft: 10}}
                                            onPress={() => [this.agregarAlias('Coordinador Administrativo')]}
                                        />
                                       <Switch
                                            style={{marginRight: 10}}
                                            value={this.state.elCordAdmin}
                                            onSyncPress={() => this.cambiarValor('Coordinador Administrativo')}
                                        />
                                    </View>
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Nombre'}
                                        iconClass={Ionicons}
                                        style={styleinput3}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-person'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.nombre_cordAdmin = text)}
                                        keyboardType='default'
                                        value={this.state.nombre_cordAdmin}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elCordAdmin}
                                    />
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Correo'}
                                        iconClass={Ionicons}
                                        style={styleinput3}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-mail'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.correo_cordAdmin = text)}
                                        keyboardType='email-address'
                                        value={this.state.correo_cordAdmin}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elCordAdmin}
                                    />
                                    <TouchableOpacity
                                        disabled={!this.state.elCordAdmin}
                                        onPress={() => this.requestMultipart('Coordinador Administrativo')}
                                        style={buttonstyle3}
                                    ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View>
                                <View
                                    style={[
                                        styles.widthall,
                                        // styles.row,
                                        {
                                            borderWidth: 0,
                                            borderRadius: 9,
                                            borderColor: 'grey',
                                            backgroundColor: this.state.fourthColor,
                                            paddingHorizontal: 6,
                                            paddingVertical: 10,
                                            marginVertical: 10,
                                            alignItems: 'center'
                                        }
                                    ]}>
                                    <View style={[styles.row, styles.widthall, {marginVertical: 5, justifyContent: "space-between"}]}>
                                        <Text style={[{
                                            width: responsiveWidth(65), marginLeft: 15, color: "black", fontWeight: '500', fontSize: 20}]}>{this.state.tituloCordCond}</Text>
                                        <FontAwesome
                                            name='edit'
                                            color='black' size={22}
                                            style={{marginLeft: 10}}
                                            onPress={() => [this.agregarAlias('Coordinador de Conducta')]}
                                        />
                                       <Switch
                                            style={{marginRight: 10}}
                                            value={this.state.elCordConduc}
                                            onSyncPress={() => this.cambiarValor('Coordinador de Conducta')}
                                        />
                                    </View>
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Nombre'}
                                        iconClass={Ionicons}
                                        style={styleinput4}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-person'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.nombre_cordConduc = text)}
                                        keyboardType='default'
                                        value={this.state.nombre_cordConduc}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elCordConduc}
                                    />
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Correo'}
                                        iconClass={Ionicons}
                                        style={styleinput4}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-mail'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.correo_cordConduc = text)}
                                        keyboardType='email-address'
                                        value={this.state.correo_cordConduc}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elCordConduc}
                                    />
                                    <TouchableOpacity
                                        disabled={!this.state.elCordConduc}
                                        onPress={() => this.requestMultipart('Coordinador de Conducta')}
                                        style={buttonstyle4}
                                    ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>): this.state.elIndexSelected === 1 ? (<View>
                            {this.renderPrefectos()}
                            <View style={{marginVertical: 15, alignItems: 'center'}}>
                            <Entypo
                                name='circle-with-plus'
                                color='green' size={45}
                                onPress={() => [this.state.prefectos.push('1'), this.setState({aux: 0})]}
                            />
                            </View>
                        </View>): this.state.elIndexSelected === 2 ? (<View>
                            <View
                                style={[
                                    styles.widthall,
                                    // styles.row,
                                    {
                                        borderWidth: 0,
                                        borderRadius: 9,
                                        borderColor: 'grey',
                                        backgroundColor: this.state.fourthColor,
                                        paddingHorizontal: 6,
                                        paddingVertical: 10,
                                        marginBottom: 10,
                                        marginTop: 5,
                                        alignItems: 'center'
                                    }
                                ]}>
                                <View style={[styles.row, styles.widthall, {marginVertical: 5, justifyContent: "space-between"}]}>
                                    <Text style={[{
                                        width: responsiveWidth(65), marginLeft: 15, color: "black", fontWeight: '500', fontSize: 20}]}>{this.state.tituloMedico}</Text>
                                    <FontAwesome
                                        name='edit'
                                        color='black' size={22}
                                        style={{marginLeft: 10}}
                                        onPress={() => [this.agregarAlias('Medico')]}
                                    />
                                    <Switch
                                        style={{marginRight:10}}
                                        value={this.state.elMedico}
                                        onSyncPress={() => this.cambiarValor('Medico')}
                                    />
                                </View>
                                <Fumi
                                    ref='apellidoMat'
                                    label={'Nombre'}
                                    iconClass={Ionicons}
                                    style={styleinput5}
                                    labelStyle={{color: this.state.mainColor}}
                                    iconName={'md-person'}
                                    inputStyle={{color: this.state.secondColor}}
                                    iconColor={this.state.mainColor}
                                    iconSize={20}
                                    onChangeText={text => (this.state.nombre_medico = text)}
                                    keyboardType='default'
                                    value={this.state.nombre_medico}
                                    returnKeyType='next'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    editable={this.state.elMedico}
                                />
                                <Fumi
                                    ref='apellidoMat'
                                    label={'Correo'}
                                    iconClass={Ionicons}
                                    style={styleinput5}
                                    labelStyle={{color: this.state.mainColor}}
                                    iconName={'md-mail'}
                                    inputStyle={{color: this.state.secondColor}}
                                    iconColor={this.state.mainColor}
                                    iconSize={20}
                                    onChangeText={text => (this.state.correo_medico = text)}
                                    keyboardType='email-address'
                                    value={this.state.correo_medico}
                                    returnKeyType='next'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    editable={this.state.elMedico}
                                />
                                <TouchableOpacity
                                    disabled={!this.state.elMedico}
                                    onPress={() => this.requestMultipart('Medico')}
                                    style={buttonstyle5}
                                ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <View
                                    style={[
                                        styles.widthall,
                                        // styles.row,
                                        {
                                            borderWidth: 0,
                                            borderRadius: 9,
                                            borderColor: 'grey',
                                            backgroundColor: this.state.fourthColor,
                                            paddingHorizontal: 6,
                                            paddingVertical: 10,
                                            marginVertical: 10,
                                            alignItems: 'center'
                                        }
                                    ]}>
                                    <View style={[styles.row, styles.widthall, {marginVertical: 5, justifyContent: "space-between"}]}>
                                        <Text style={[{
                                            width: responsiveWidth(65), marginLeft: 15, color: "black", fontWeight: '500', fontSize: 20}]}>{this.state.tituloMedico1}</Text>
                                        <FontAwesome
                                            name='edit'
                                            color='black' size={22}
                                            style={{marginLeft: 10}}
                                            onPress={() => [this.agregarAlias('Medico1')]}
                                        />
                                        <Switch
                                            style={{marginRight:10}}
                                            value={this.state.elMedico1}
                                            onSyncPress={() => this.cambiarValor('Medico1')}
                                        />
                                    </View>
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Nombre'}
                                        iconClass={Ionicons}
                                        style={styleinput6}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-person'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.nombre_medico = text)}
                                        keyboardType='default'
                                        // value={this.state.datos.apellido_materno}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elMedico1}
                                    />
                                    <Fumi
                                        ref='apellidoMat'
                                        label={'Correo'}
                                        iconClass={Ionicons}
                                        style={styleinput6}
                                        labelStyle={{color: this.state.mainColor}}
                                        iconName={'md-mail'}
                                        inputStyle={{color: this.state.secondColor}}
                                        iconColor={this.state.mainColor}
                                        iconSize={20}
                                        onChangeText={text => (this.state.correo_medico = text)}
                                        keyboardType='email-address'
                                        // value={this.state.datos.apellido_materno}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        editable={this.state.elMedico1}
                                    />
                                    <TouchableOpacity
                                        disabled={!this.state.elMedico1}
                                        onPress={() => this.requestMultipart('Medico')}
                                        style={buttonstyle6}
                                    ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>):null
                    }
                </View>
            </View>
        )
    }

    async cambiarValor(item){
        if(item === 'Subdirector'){
            await this.setState({elSubdirector: !this.state.elSubdirector})
            await fetch(this.state.uri + '/api/cambiar/estado/'+ 'SubDirector', {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert(
                            'Error', 'Ha ocurrido un error, por favor intente más tarde',
                            [{text: 'Entendido'}]
                        );
                    } else {
                        Alert.alert(
                            'Felicidades', 'Se ha cambiado el estatus del Subdirector con éxito',
                            [{text: 'Entendido'}]
                        );
                    }
                });
        }else if(item === 'Coordinador Académico'){
            await this.setState({elCordAcad: !this.state.elCordAcad})
            await fetch(this.state.uri + '/api/cambiar/estado/'+ 'Coordinador Academico', {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert(
                            'Error', 'Ha ocurrido un error, por favor intente más tarde',
                            [{text: 'Entendido'}]
                        );
                    } else {
                        Alert.alert(
                            'Felicidades', 'Se ha cambiado el estatus del Coordinador Academico con éxito',
                            [{text: 'Entendido'}]
                        );
                    }
                });
        }else  if(item === 'Coordinador Administrativo'){
            await this.setState({elCordAdmin: !this.state.elCordAdmin})
            await fetch(this.state.uri + '/api/cambiar/estado/'+ 'Secretaria', {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert(
                            'Error', 'Ha ocurrido un error, por favor intente más tarde',
                            [{text: 'Entendido'}]
                        );
                    } else {
                        Alert.alert(
                            'Felicidades', 'Se ha cambiado el estatus del Coordinador Administrativo con éxito',
                            [{text: 'Entendido'}]
                        );
                    }
                });
        }else if(item === 'Coordinador de Conducta'){
            await this.setState({elCordConduc: !this.state.elCordConduc})
            await fetch(this.state.uri + '/api/cambiar/estado/'+ 'Cordisi', {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert(
                            'Error', 'Ha ocurrido un error, por favor intente más tarde',
                            [{text: 'Entendido'}]
                        );
                    } else {
                        Alert.alert(
                            'Felicidades', 'Se ha cambiado el estatus del Coordinador de conducta con éxito',
                            [{text: 'Entendido'}]
                        );
                    }
                });
        }else  if(item === 'Medico'){
            await this.setState({elMedico: !this.state.elMedico})
            await fetch(this.state.uri + '/api/cambiar/estado/'+ 'Medico', {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert(
                            'Error', 'Ha ocurrido un error, por favor intente más tarde',
                            [{text: 'Entendido'}]
                        );
                    } else {
                        Alert.alert(
                            'Felicidades', 'Se ha cambiado el estatus del Médico con éxito',
                            [{text: 'Entendido'}]
                        );
                    }
                });
        }else  if(item === 'Medico1'){
            await this.setState({elMedico1: !this.state.elMedico1})
        }
    }

     crearAlias(){
        let puesto = '';
        if (this.state.elAlias === 'Subdirector'){
            puesto = 'Subdirector'
        }else if (this.state.elAlias === 'Coordinador Academico'){
            puesto = 'Coordinador Academico'
        }else if (this.state.elAlias === 'Coordinador Administrativo'){
            puesto = 'Secretaria'
        }else if(this.state.elAlias === 'Coordinador de Conducta'){
            puesto = 'Cordisi'
        }else if(this.state.elAlias === 'Prefecto') {
            puesto = 'Prefecto'
        }else if(this.state.elAlias === 'Medico') {
            puesto = 'Medico'
        }
        if (this.state.nuevoNombre == ''){
            Alert.alert(
                'Error', 'Ingrese el nuevo nombre del puesto',
                [{text: 'Entendido'}]
            );
        }else {
            fetch(this.state.uri + '/api/crear/alias/' + puesto + '/' + this.state.nuevoNombre, {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        Alert.alert(
                            'Error', 'Ha ocurrido un error, por favor intente más tarde',
                            [{text: 'Entendido'}]
                        );
                    } else {
                        console.log(responseJson)
                        Alert.alert(
                            'Felicidades', 'Ha cambiado el nombre correctamente',
                            [{text: 'Entendido', onPress: () => [this.setState({visible: false}), this.getAlias()]}]
                        );
                    }
                });
        }
    }

    quitarPrefectos(){
        if(this.state.prefectos.length > 1){
            this.state.prefectos.pop();
            this.setState({aux: 0});
        }

    }

    async elBotonSelected(indexSelected, itemSelected) {
        await this.setState({
            elBotonSelected: itemSelected,
            elIndexSelected: indexSelected,
        });
    }

    /////////-*-*--*-*-*--*-*-*-*-*-*Funcionalidades*-*-*-*-*-*-*--*-**-*--*-*-*-*--*-*
    gestionarFuncionalidades(){
        let estados = this.state.puestos.map((item, i) => {
            if(item.alias === null || item.alias === ''){
                return {
                    key: i,
                    label: item.puesto
                };
            }else{
                return {
                    key: i,
                    label: item.alias
                };
            }

        });
        return(
            <View>
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 10}]}>Seleccione un rol para editar:</Text>
                <ModalSelector
                    data={estados}
                    selectStyle={[
                        [styles.inputPicker, {borderColor: this.state.secondColor, backgroundColor: 'white'}],
                        {marginTop: 0}
                    ]}
                    cancelText='Cancelar'
                    optionTextStyle={{color: '#75745a'}}
                    initValue='Seleccione el tipo de usuario'
                    onChange={option => this.onChangePuestos(option)}
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 10}]}>Active o desactive las funcionalidades:</Text>
                {this.state.funcionalidades.length === 0 ? (
                    <Text style={[styles.main_title, {
                    color: '#a8a8a8',
                    textAlign: 'center',
                    ...Platform.select({
                        ios: {marginTop: 100, marginBottom: 40,},
                        android: {marginTop: 35, marginBottom: 35}
                    }),

                    fontSize: responsiveFontSize(3)
                }]}>
                    Haga una selección
                </Text>) : (this.renderFuncionalidades())

                    }
                <TouchableOpacity
                    onPress={() => this.saveFuncionalidades()}
                    style={[styles.bigButton, {backgroundColor: this.state.secondColor, height: responsiveHeight(6)}]}
                ><Text style={{color: "white", fontWeight: '900', fontSize: responsiveFontSize(1.9)}}>Guardar</Text>
                </TouchableOpacity>
            </View>
        )
    }

    //////Guardar FUNCIONALIDADES
    async saveFuncionalidades() {
        if(this.state.puesto === '' || this.state.funcionalidades.length === 0){
            Alert.alert(
                'Atención', 'Seleccione un usuario para guardar las funcionalidades disponibles',
            );
        }else{
            this.setState({spinnerFuncionalidades: true})
            let arreglo = [];
            let formData = await new FormData();
            for (let i = 0; i < this.state.funcionalidades.length; i++) {
                arreglo.push({
                    nombre_funcionalidad: this.state.funcionalidades[i].nombre,
                    estatus: this.state.funcionalidades[i].estatus,
                });
            }
            formData.append('new', JSON.stringify(arreglo));
            console.log(formData)
            await fetch(this.state.uri + '/api/update/funcionalidades/'+this.state.puesto, {
                method: 'POST', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }, body: formData
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        this.setState({spinnerFuncionalidades: false})
                        Alert.alert(
                            'Error', 'Ha ocurrido un error, por favor intente más tarde',
                            [{text: 'Entendido'}]
                        );
                    } else {
                        Alert.alert(
                            'Felicidades', 'Ha actualizado las funcionalidades con éxito',
                            [{text: 'Entendido', onPress: ()=> [this.getFuncionalidades(), this.setState({spinnerFuncionalidades: false})]}]
                        );
                    }
                });
        }
    }

    async onChangePuestos(option) {
        await this.setState({puesto: this.state.puestos[option.key].puesto});
        console.log(option)
        await this.getFuncionalidades();
    }

    getPuestos(){
        fetch(this.state.uri + '/api/get/puestos/menus', {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error', 'Ha ocurrido un error, por favor intente más tarde',
                        [{text: 'Entendido'}]
                    );
                } else {
                    console.log(responseJson)
                    this.setState({puestos: responseJson})
                }
            });
    }

    getFuncionalidades(){
        let nombreCorto = '';
        let nombreCorto1 = '';
        if(this.state.uri === 'https://controlescolar.pro'){
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.pro', '');
        }else{
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.controlescolar.pro', '');
        }
        this.setState({spinner: true})
        if(this.state.puesto === 'Coordinador Academico'){
            this.setState({puesto: 'Academico'})
        }
        console.log(this.state.uri + '/api/get/funcionalidades/'+ nombreCorto1+ '/'+this.state.puesto);
        fetch(this.state.uri + '/api/get/funcionalidades/'+ 'controlescolar'+ '/'+this.state.puesto, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    this.setState({spinner: false})
                    Alert.alert(
                        'Error', 'Ha ocurrido un error, por favor intente más tarde',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({spinner: false})
                    this.setState({funcionalidades: responseJson})
                }
            });
    }


    changeValor(item){
        let ind = this.state.funcionalidades.indexOf(item)
        this.state.funcionalidades[ind].estatus = !this.state.funcionalidades[ind].estatus;
        console.log(ind)
        console.log(this.state.funcionalidades[ind].estatus)
        console.log('funcionalidades',this.state.funcionalidades)

    }

    renderFuncionalidades() {
        let cards = [];
        this.state.funcionalidades.forEach((item, i) => {
            cards.push(
                <View
                    key={i++}
                    style={[
                        styles.widthall,
                        styles.row,
                        {
                            borderWidth: 0.5,
                            borderRadius: 6,
                            borderColor: 'lightgray',
                            backgroundColor: this.state.fourthColor,
                            paddingHorizontal: 6,
                            paddingLeft: 15,
                            paddingVertical: 10,
                            marginVertical: 3,
                            alignItems: 'flex-start'
                        }
                    ]}>
                    <Image
                        style={{width: 25, height: 25}}
                        source={{uri: item.url}}
                    />
                   <Text style={{fontSize: responsiveFontSize(2)}}>{item.nombre}</Text>
                    <Switch
                        style={{marginRight:10}}
                        value={item.estatus}
                        onSyncPress={() => this.changeValor(item)}
                    />
                </View>
            )
        });
        return cards;
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <Spinner visible={this.state.spinner} textContent='Consultando...'/>
                <Spinner visible={this.state.spinnerFuncionalidades} textContent='Guardando cambios...'/>
                <MultiBotonRow
                    itemBtns={['Gestionar puestos', 'Gestionar funcionalidades']}
                    onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
                    cantidad={2}
                />
                <ScrollView showsVerticalScrollIndicator={false}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }
                >
                    <View>
                        {this.state.botonSelected === 'Gestionar puestos' ? (this.gestionarPuestos()) : null}
                        {this.state.botonSelected === 'Gestionar funcionalidades' ? (this.gestionarFuncionalidades()) : null}

                    </View>
                </ScrollView>
                <Modal
                    isVisible={this.state.visible}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={[styles.container, {width: responsiveWidth(94), height: responsiveHeight(25), borderRadius: 10, flex: 0}]}>
                        <Text style={[{margin: 15, color: this.state.thirdColor, fontWeight: '900'}]}>¿Desea cambiar el nombre al puesto {this.state.elAlias}?</Text>
                        <Fumi
                            ref='apellidoMat'
                            label={this.state.elAlias}
                            iconClass={Ionicons}
                            style={[{borderColor: this.state.secondColor, borderWidth: 1, borderRadius: 6, width: responsiveWidth(80),  marginTop: 5}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.nuevoNombre = text)}
                            keyboardType='default'
                            //value={this.state.datos.apellido_materno}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <View style={[styles.modalWidth, styles.row, {marginTop: 25}]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({visible:false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.crearAlias()}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Guardar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </KeyboardAvoidingView>
        );
    }
}

