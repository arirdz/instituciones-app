import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight,
    TextInput
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';
import MultiBotonRow from "../Globales/MultiBotonRow";
import Feather from "react-native-vector-icons/Feather";
import Spinner from "react-native-loading-spinner-overlay";
import {Actions} from "react-native-router-flux";

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');


export default class detalleFacturaServicio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pagados: [],
            datos: []
        };
    }


    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    facturar(){
        console.log(this.props.datos)
        console.log(this.props.cargo)
    }


    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Emisor
                        </Text>
                    </View>
                    <View style={[styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1,
                    }]}>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Nombre del emisor
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>Desarrolladores de Tecnologías Aplicadas a la Educación S.C.</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                RFC
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>DTA180810FA7</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Régimen Fiscal
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>601 - Ley General de Personas Morales</Text>
                        </View>
                    </View>
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Receptor
                        </Text>
                    </View>
                    <View style={[styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1
                    }]}>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Nombre del receptor
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.props.datos.emisor.option_key}</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                RFC
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>{this.props.datos.rfc.option_key}</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Uso del CFDI
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>G03 - Gastos en General</Text>
                        </View>
                    </View>
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        borderRadius: 3,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Datos Generales
                        </Text>
                    </View>
                    <View style={[styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1
                    }]}>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 0, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Lugar de expedición
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>91080</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Método de pago
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>PUE - Pago en una sola exhibición</Text>
                        </View>
                        <View style={[styles.widthall]}>
                            <Text style={[styles.textW, {marginTop: 7, textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Forma de pago
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                            }}>3 - Transferencia electrónica de fondos</Text>
                        </View>
                    </View>
                    <View style={{
                        backgroundColor: this.state.secondColor,
                        borderRadius: 3,
                        marginTop: 8
                    }}>
                        <Text style={[styles.main_title, {color: 'white', marginTop: 1, padding: 5}]}>
                            Concepto
                        </Text>
                    </View>
                    <View style={[styles.row, styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1,
                        borderBottomWidth: 0
                    }]}>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Clave Servicio
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>81112501</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Clave Unidad
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>MON - Mes</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Cantidad
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>1</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Unidad
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>SERVICIO</Text>
                        </View>
                    </View>
                    <View style={[styles.row, styles.widthall, {
                        padding: 10,
                        justifyContent: 'flex-start',
                        borderColor: this.state.secondColor,
                        borderWidth: 1,
                        borderTopWidth: 0
                    }]}>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Descripcion
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>Servicio mensual Control Escolar</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Moneda
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>'MXN'</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Valor unitario
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>$5000.00</Text>
                        </View>
                        <View style={[styles.btn4]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Total
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 3
                            }}>$5000.00</Text>
                        </View>
                    </View>
                </ScrollView>
                <View style={[styles.row, styles.widthall, {justifyContent: "center", marginBottom: 20}]}>
                    <TouchableOpacity
                        style={[
                            styles.modalBigBtn,
                            {backgroundColor: '#fff', borderColor: this.state.secondColor, margin: 5}
                        ]}
                        onPress={() => [Actions.pop()]}
                    >
                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.modalBigBtn,
                            {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor, margin: 5}
                        ]}
                        onPress={() => this.facturar()}
                    >
                        <Text style={[styles.textW, {color: '#fff'}]}>Facturar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
