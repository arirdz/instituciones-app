import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight
} from 'react-native';
import {
	responsiveHeight,
	responsiveWidth,
	responsiveFontSize
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Modal from 'react-native-modal';
import {Actions} from 'react-native-router-flux';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class DiaInmersion extends React.Component {
	//++++++++++time picker1
	showTimePicker1 = () => this.setState({horaPicker1: true});

	hideTimePicker1 = () => this.setState({horaPicker1: false});

	handleTimePicked1 = (time) => {
		let hora1 = moment(time, 'HH:mm:ss').format('HH:mm');
		this.setState({hora1: hora1});
		this.hideTimePicker1();
	};

	//++++++++++time picker2
	showTimePicker2 = () => this.setState({horaPicker2: true});

	hideTimePicker2 = () => this.setState({horaPicker2: false});

	handleTimePicked2 = (time) => {
		let hora2 = moment(time, 'HH:mm:ss').format('HH:mm');
		this.setState({hora2: hora2});
		this.hideTimePicker2();
	};

//+++-+-+-+-+-+date picker
	_hideDateTimePicker = () => this.setState({fechaPicker: false});

	_showDateTimePicker = () => this.setState({fechaPicker: true});

	_handleDatePicked = (date) => {
		let fecha1 = moment(date).format('YYYY-MM-DD');
		this.setState({laFecha: fecha1});
		this.setState({aux: 0});
		this._hideDateTimePicker();
	};

	constructor(props) {
		super(props);
		this.state = {
			aux: 0,
			fechaPicker: false,
			horaPicker1: false,
			horaPicker2: false,
			laFecha: '',
			hora1: '',
			hora2: '',
			lasFechas: [],
			isModalHra: []
		};
	}


	async componentWillMount() {
		await this.getURL();
		await this.getDiasInmersion();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async requestMultipart() {
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			hora_inicio: this.state.hora1,
			hora_fin: this.state.hora2,
			fecha: this.state.laFecha
		}));
		await fetch(this.state.uri + '/api/post/dia/inmersion', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al crear fecha', 'Ha ocurrido un error ' + responseJson.error.status_code +
					'  si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido'
						// onPress: () => Actions.pop({refresh: {key: 'drawer'}})
					}]);
			} else {
				Alert.alert('Fecha creada', 'Se ha creado la fecha con éxito', [
					{text: 'Entendido', onPress: () => this.getDiasInmersion()}
				])
			}
		});
	}

	async getDiasInmersion() {
		await fetch(this.state.uri + '/api/get/dias/inmersion').then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al cargar las fechas', 'Ha ocurrido un error ' + responseJson.error.status_code +
					'  si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido'
						// onPress: () => Actions.pop({refresh: {key: 'drawer'}})
					}]);
			} else {
				this.setState({lasFechas: responseJson});
				if (this.state.lasFechas.length !== 0) {
					this.state.lasFechas.forEach(() => {
						this.state.isModalHra.push(false);
					})
				}
			}
		});
	}

	async updateFecha(laFecha, ix) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: laFecha.id,
			hora_inicio: this.state.hora1,
			hora_fin: this.state.hora2,
			fecha: this.state.laFecha
		}));
		await fetch(this.state.uri + '/api/update/dia/inmersion', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al editar fecha', 'Ha ocurrido un error ' + responseJson.error.status_code +
					'  si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido'
						// onPress: () => Actions.pop({refresh: {key: 'drawer'}})
					}]);
			} else {
				Alert.alert('Fecha editada', 'Se ha editado la fecha con éxito', [
					{text: 'Entendido', onPress: () => [this.getDiasInmersion(), this.closeModal(ix)]}
				])
			}
		});
	}

	async agregar() {
		if (this.state.fecha === '') {
			Alert.alert('Sin fecha definida',
				'Seleccione una fecha para poder continuar', [
					{text: 'Entendido'}
				])
		} else if (this.state.hora1 === '') {
			Alert.alert('Sin hora de inicio definida',
				'Seleccione una hora de inicio poder continuar', [
					{text: 'Entendido'}
				])
		} else if (this.state.hora2 === '') {
			Alert.alert('Sin hora de finalizado definida',
				'Seleccione una hora de finalizado para poder continuar', [
					{text: 'Entendido'}
				])
		} else {
			await this.requestMultipart();
		}
		await this.setState({aux: 0});
	}

	async borrar(laFecha) {
		Alert.alert('Borrar fecha', 'Está a punto de borrar un fecha ¿seguro que desea borrarla?', [
			{text: 'Sí', onPress: () => this.borrarFecha(laFecha)}, {text: 'No'}
		])
	}

	async borrarFecha(laFecha) {
		await fetch(this.state.uri + '/api/delete/dia/inmersion/' + laFecha.id).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al borrar fecha', 'Ha ocurrido un error ' + responseJson.error.status_code +
					'  si el error continua pónganse en contacto con soporte (Solicitud de informes)',
					[{
						text: 'Entendido',
						onPress: () => Actions.pop({refresh: {key: 'drawer'}})
					}]);
			} else {
				Alert.alert('Fecha borrada', 'Se ha borrado la fecha con éxito', [
					{text: 'Entendido', onPress: () => this.getDiasInmersion()}
				])
			}
		});
	}

	async openModal(fecha, ix) {
		this.state.laFecha = fecha.fecha;
		this.state.hora1 = fecha.hora_inicio;
		this.state.hora2 = fecha.hora_fin;
		this.state.isModalHra[ix] = true;
		await this.setState({aux: 0});
	}

	async closeModal(ix) {
		this.state.isModalHra[ix] = false;
		await this.setState({aux: 0});
	}

	rndFechas() {
		let fechas = [];
		if (this.state.lasFechas.length !== 0) {
			this.state.lasFechas.forEach((it, ix) => {
				fechas.push(
					<View
						key={ix + 'fechas'}
						style={[
							styles.row,
							styles.cardHoras,
							styles.widthall,
							{
								backgroundColor: this.state.fourthColor,
								marginVertical: 4,
								paddingVertical: 8
							}
						]}
					>
						<Modal
							isVisible={this.state.isModalHra[ix]}
							backdropOpacity={0.8}
							animationIn={'bounceIn'}
							animationOut={'bounceOut'}
							animationInTiming={1000}
							animationOutTiming={1000}
						>
							<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
								<Text style={[styles.main_title, styles.modalWidth, {
									color: this.state.thirdColor
								}]}>
									Editar fecha
								</Text>
								<TouchableOpacity
									style={[styles.inputPicker, styles.modalWidth,
										{borderColor: this.state.secondColor, marginVertical: 5}]}
									onPress={this._showDateTimePicker}
								>
									{this.state.laFecha === '' ? (
										<Text style={{fontSize: responsiveFontSize(1.5)}}>
											Seleccione fecha
										</Text>
									) : (
										<Text style={{fontSize: responsiveFontSize(1.5)}}>
											{moment(this.state.laFecha, 'YYYY-MM-DD').format('DD/MM/YYYY')}
										</Text>
									)}
								</TouchableOpacity>
								<DateTimePicker
									locale={'es'}
									isVisible={this.state.fechaPicker}
									onConfirm={this._handleDatePicked}
									onCancel={this._hideDateTimePicker}
									titleIOS={'Seleccione una fecha'}
									confirmTextIOS={'Seleccionar'}
									cancelTextIOS={'Cancelar'}
								/>
								<Text style={[styles.modalWidth, {fontSize: responsiveFontSize(1.5), marginTop: 3}]}>
									Seleccione una hora de inicio y de fin para la fecha indicada
								</Text>
								<View style={[styles.modalWidth, styles.row, {marginVertical: 5}]}>
									{/*+-+-+-+-+-+-hora 1+-+-+-+-+-+-*/}
									<TouchableOpacity
										style={[styles.modalBigBtn, {
											borderColor: this.state.secondColor,
											borderWidth: 1,
											marginVertical: 0
										}]}
										onPress={this.showTimePicker1}
									>
										{this.state.hora1 === '' ? (
											<Text style={{fontSize: responsiveFontSize(1.5)}}>
												Inicio
											</Text>
										) : (
											<Text style={{fontSize: responsiveFontSize(1.5)}}>
												{moment(this.state.hora1, 'HH:mm:ss').format('HH:mm')}
											</Text>
										)}
									</TouchableOpacity>
									<DateTimePicker
										isVisible={this.state.horaPicker1}
										onConfirm={this.handleTimePicked1}
										mode={'time'}
										is24Hour={true}
										onCancel={this.hideTimePicker1}
										titleIOS={'Seleccione una hora'}
										confirmTextIOS={'Seleccionar'}
										cancelTextIOS={'Cancelar'}
									/>
									{/*+-+-+-+-+-+-hora 2+-+-+-+-+-+-*/}
									<TouchableOpacity
										style={[styles.modalBigBtn, {
											borderColor: this.state.secondColor,
											borderWidth: 1,
											marginVertical: 0
										}]}
										onPress={this.showTimePicker2}
									>
										{this.state.hora2 === '' ? (
											<Text style={{fontSize: responsiveFontSize(1.5)}}>
												Fin
											</Text>
										) : (
											<Text style={{fontSize: responsiveFontSize(1.5)}}>
												{moment(this.state.hora2, 'HH:mm:ss').format('HH:mm')}
											</Text>
										)}
									</TouchableOpacity>
									<DateTimePicker
										isVisible={this.state.horaPicker2}
										onConfirm={this.handleTimePicked2}
										mode={'time'}
										is24Hour={true}
										onCancel={this.hideTimePicker2}
										titleIOS={'Seleccione una hora'}
										confirmTextIOS={'Seleccionar'}
										cancelTextIOS={'Cancelar'}
									/>
								</View>
								<View style={[styles.modalWidth, styles.row]}>
									<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{backgroundColor: '#fff', borderColor: this.state.secondColor}
										]}
										onPress={() => this.closeModal(ix)}
									>
										<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
									</TouchableOpacity>
									<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{
												backgroundColor: this.state.secondColor,
												borderColor: this.state.secondColor
											}
										]}
										onPress={() => this.updateFecha(this.state.lasFechas[ix], ix)}
									>
										<Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
									</TouchableOpacity>
								</View>
							</View>
						</Modal>
						<View
							style={[styles.btn4, {alignItems: 'center'}]}
						>
							<Text>Fecha {ix + 1}:</Text>
							<View style={styles.btn4}>
								<Text style={{textAlign: 'center', fontWeight: '800'}}>
									{moment(it.fecha).format('DD/MM/YYYY')}
								</Text>
							</View>
						</View>
						<View style={{alignItems: 'center'}}>
							<Text>Tiempo de duración:</Text>
							<View style={styles.btn2}>
								<Text style={{textAlign: 'center', fontWeight: '800'}}>
									{moment(it.hora_inicio, 'HH:mm:ss').format('HH:mm')} – {moment(it.hora_fin, 'HH:mm:ss').format('HH:mm')}
								</Text>
							</View>
						</View>
						<View
							style={[styles.btn6, styles.row]}
						>
							<MaterialIcons name='edit' size={22} color='grey'
										   onPress={() => this.openModal(this.state.lasFechas[ix], ix)}/>
							<Entypo name='circle-with-minus' size={22} color='red'
									onPress={() => this.borrar(this.state.lasFechas[ix])}/>
						</View>
					</View>
				)
			});
		}
		return fechas;
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Día de inmersión escolar
				</Text>
				<Text style={[styles.widthall, {fontSize: responsiveFontSize(1.5)}]}>
					Edite o configure las fechas de inmersión que tendrá la escuela
				</Text>
				<TouchableOpacity
					style={[styles.inputPicker, {borderColor: this.state.secondColor, marginVertical: 5}]}
					onPress={this._showDateTimePicker}
				>
					{this.state.laFecha === '' ? (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							Seleccione fecha
						</Text>
					) : (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							{moment(this.state.laFecha, 'YYYY-MM-DD').format('DD/MM/YYYY')}
						</Text>
					)}
				</TouchableOpacity>
				<DateTimePicker
					locale={'es'}
					isVisible={this.state.fechaPicker}
					onConfirm={this._handleDatePicked}
					onCancel={this._hideDateTimePicker}
					titleIOS={'Seleccione una fecha'}
					confirmTextIOS={'Seleccionar'}
					cancelTextIOS={'Cancelar'}
				/>
				<Text style={[styles.widthall, {fontSize: responsiveFontSize(1.5), marginTop: 3}]}>
					Seleccione una hora de inicio y de fin para la fecha indicada
				</Text>
				<View style={[styles.widthall, styles.row, {marginVertical: 5}]}>
					{/*+-+-+-+-+-+-hora 1+-+-+-+-+-+-*/}
					<TouchableOpacity
						style={[styles.inputPicker, styles.btn2, {borderColor: this.state.secondColor, marginTop: 0}]}
						onPress={this.showTimePicker1}
					>
						{this.state.hora1 === '' ? (
							<Text style={{fontSize: responsiveFontSize(1.5)}}>
								Inicio
							</Text>
						) : (
							<Text style={{fontSize: responsiveFontSize(1.5)}}>
								{moment(this.state.hora1, 'HH:mm:ss').format('HH:mm')}
							</Text>
						)}
					</TouchableOpacity>
					<DateTimePicker
						isVisible={this.state.horaPicker1}
						onConfirm={this.handleTimePicked1}
						mode={'time'}
						is24Hour={true}
						onCancel={this.hideTimePicker1}
						titleIOS={'Seleccione una hora'}
						confirmTextIOS={'Seleccionar'}
						cancelTextIOS={'Cancelar'}
					/>
					{/*+-+-+-+-+-+-hora 2+-+-+-+-+-+-*/}
					<TouchableOpacity
						style={[styles.inputPicker, styles.btn2, {borderColor: this.state.secondColor, marginTop: 0}]}
						onPress={this.showTimePicker2}
					>
						{this.state.hora2 === '' ? (
							<Text style={{fontSize: responsiveFontSize(1.5)}}>
								Fin
							</Text>
						) : (
							<Text style={{fontSize: responsiveFontSize(1.5)}}>
								{moment(this.state.hora2, 'HH:mm:ss').format('HH:mm')}
							</Text>
						)}
					</TouchableOpacity>
					<DateTimePicker
						isVisible={this.state.horaPicker2}
						onConfirm={this.handleTimePicked2}
						mode={'time'}
						is24Hour={true}
						onCancel={this.hideTimePicker2}
						titleIOS={'Seleccione una hora'}
						confirmTextIOS={'Seleccionar'}
						cancelTextIOS={'Cancelar'}
					/>
				</View>
				<Entypo
					name='circle-with-plus'
					color='green' size={35}
					onPress={() => this.agregar()}
					style={{marginVertical: 10}}
				/>
				<ScrollView>
					{this.rndFechas()}
				</ScrollView>
			</View>
		);
	}
}
