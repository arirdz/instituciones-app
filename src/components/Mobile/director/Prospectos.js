import React from 'react';
import {
	Alert, AsyncStorage, Image, KeyboardAvoidingView, Platform, RefreshControl, ScrollView, StatusBar, Text, TextInput,
	TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Communications from 'react-native-communications';
import Modal from 'react-native-modal';
import StepIndicator from 'rn-step-indicator';
import {Actions} from 'react-native-router-flux';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Spinner from 'react-native-loading-spinner-overlay';

const labels_estForm = [
	'Solicitar\ninfo',
	'Agendar\ncita',
	'Subir\ndocumentos',
	'Acreditar\npreinscripción',
	'Día de\ninmersión',
	'Acreditar\ninscripción'
];

const customStyles = {
	stepIndicatorSize: 20,
	currentStepIndicatorSize: 25,
	separatorStrokeWidth: 1,
	currentStepStrokeWidth: 2,
	stepStrokeCurrentColor: '#777777',
	separatorFinishedColor: '#16a900',
	separatorUnFinishedColor: '#aaaaaa',
	stepIndicatorFinishedColor: '#16a900',
	stepIndicatorUnFinishedColor: '#aaaaaa',
	stepIndicatorCurrentColor: '#ffffff',
	stepIndicatorLabelFontSize: responsiveFontSize(1.3),
	currentStepIndicatorLabelFontSize: responsiveFontSize(1.3),
	stepIndicatorLabelCurrentColor: '#000000',
	stepIndicatorLabelFinishedColor: '#ffffff',
	stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
	labelColor: '#666666',
	labelSize: responsiveFontSize(1),
	currentStepLabelColor: '#777777'
};

export default class Prospectos extends React.Component {
	_changeWheelState = (state) => {
		this.setState({
			visible: state
		});
	};

	_onRefresh = () => {
		this.setState({refreshing: true});
		fetch(this.state.uri + '/api/get/aspirantes/admin').then(res => res.json())
			.then(responseJson => {
				this.setState({refreshing: false});
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{text: 'Entendido'}]);
				} else {
					this.setState({aspirantes: responseJson});
					if (this.state.aspirantes.length !== 0) {
						this.state.aspirantes.forEach(() => {
							this.state.modalTutor.push(false);
						});
					}
				}
			});
	};

	constructor(props) {
		super(props);
		this.state = {
			aux: 0,
			visible: false,
			open: false,
			refreshing: false,
			aspirantes: [],
			modalTutor: [],
			modalAspirante: [],
			modalVisibleG: [],
			elDocumento: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getAspirantesAdmin();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let schoolId = await AsyncStorage.getItem('schoolId');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			schoolId: schoolId
		});
	}

//+-+-+-+---+-+-+-+-+--+validar aspirante
	async notifvalidar(it, validar) {
		let mensaje = '';
		if (validar === 'si') {
			mensaje = '¡Ya casi eres parte de Liceo!  A continuación procederá a realizar el pago de inscripción que podrá realizar a través de la App';
		} else if (validar == 'no') {
			mensaje = 'Proceso de solicitud rechazada  Lo sentimos, tu proceso de inscripción a Liceo Ánimas ha sido rechazada. Para más información le invitamos a que realice otra cita e informar sobre los motivos';
		}
		await fetch(this.state.uri + '/api/notificar/tutor/' + it.id_tutor + '/' + Number(this.state.schoolId) + '/' + mensaje, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}
		}).then(res => res.json())
			.then(responseJson => {
				console.log(responseJson);
			});
	}

	async emailValidarAlumno(aspirante, validar) {
		await fetch(this.state.uri + '/api/validar/aspirante/' + aspirante.id_tutor + '/' + validar, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al validar alumno',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Prospectos)', [{
							text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})
						}]);
				} else {
					if (validar === 'si') {
						Alert.alert('Se ha aceptado la solicitud del aspirante',
							'Se le ha mandado un correo al tutor sobre la validación del aspirante', [{
								text: 'Entendido', onPress: () => this.requestMultiPart(aspirante, 5, 'en proceso')
							}]);
					} else {
						Alert.alert('Se ha rechazado la solicitud del aspirante',
							'Se le ha mandado un correo al tutor sobre la validación del aspirante', [
								{text: 'Entendido', onPress: () => this.requestMultiPart(aspirante, 7, 'rechazado')}
							]);
					}
				}
			});
	}

//+-+-+-+-+-+-+-+-+-+-+Aspirantes
	async getAspirantesAdmin() {
		await this._changeWheelState(true);
		await fetch(this.state.uri + '/api/get/aspirantes/admin').then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{text: 'Entendido', onPress: () => this._changeWheelState(false)}]);
				} else {
					this.setState({aspirantes: responseJson});
					this._changeWheelState(false);
					if (this.state.aspirantes.length !== 0) {
						this.state.aspirantes.forEach(() => {
							this.state.modalTutor.push(false);
						});
					}
				}
			});
	}

	async requestMultiPart(aspirante, numero, estatus) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: aspirante.id,
			paso_registro: numero,
			estatus_alta: estatus
		}));
		await fetch(this.state.uri + '/api/cita/aspirantes', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al enviar cita',
						'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita aspirante)', [
							{text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})}
						]);
				} else {
					Actions.refresh({key: Math.random()})
				}
			});
	}

	async openModalT(i) {
		this.state.modalTutor[i] = true;
		await this.setState({aux: 0});
	}

	async closeModalT(i) {
		this.state.modalTutor[i] = false;
		await this.setState({aux: 0});
	}


	async verDocumentos(documentos) {
		Actions.verDocumentos({doctos: documentos});
	}

	async alertEnviarNotif(itm) {
		Alert.alert('¿Enviar recordatorio?', '', [
			{text: 'Sí', onPress: () => [this.enviarNotif(itm), this.sendNotif(itm)]}, {text: 'No'}
		])
	}

	async enviarNotif(itm) {
		await fetch(this.state.uri + '/api/enviar/recordatorio/' + itm.id_tutor + '/' + itm.id).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al enviar cita',
					'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar cita si el error continua pónganse en contacto con soporte (Agendar Cita aspirante)', [
						{text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})}
					]);
			} else {
				Alert.alert('Recordatorio enviado', 'Se ha enviado el recordatorio con éxito', [{
					text: 'Enterado', onPress: () => Actions.refresh({key: Math.random()})
				}]);
			}
		});
	}

	async sendNotif(itm) {
		let mensaje = '';
		if (itm.paso_registro === '1') {
			mensaje = 'No ha agendado cita aún. ' +
				'Es necesario solicitar una cita para el aspirante ' + itm.nombre_aspirante + ' Le invitamos a realizarla para poder seguir con el proceso.';
		} else if (itm.paso_registro === '2') {
			mensaje = 'Tiene documentación pendiente. Le recordamos que faltan documentos por enviar del aspirante ' + itm.nombre_aspirante + '.';
		} else if (itm.paso_registro === '3') {
			mensaje = 'Pago de preinscripción pendiente. Tiene pendiente el pago de preinscripción del aspirante ' + itm.nombre_aspirante + ' Una vez que lo realice podrá continuar con el proceso.';
		} else if (itm.paso_registro === '3.1') {
			mensaje = 'Pago de preinscripción aprobado. El pago de preinscripción del aspirante ' + itm.nombre_aspirante + ' ha sido aprobado, ahora puede continuar en el siguiente paso del proceso de inscripcón.';
		} else if (itm.paso_registro === '5') {
			mensaje = 'Pago de inscripción pendiente. Tiene pendiente el pago de inscripción del aspirante ' + itm.nombre_aspirante + ' ¡Está a un solo paso para ser de la familia Liceo Ánimas!';
		}if (itm.paso_registro === '5.1') {
			mensaje = 'Pago de inscripción aprobado. El pago de inscripción del aspirante ' + itm.nombre_aspirante + ' ha sido aprobado, ¡Ahora puede darse de alta para ser parte de la comunidad escolar!.';
		}else if (itm.paso_registro === '7.1') {
			mensaje = 'Cita pendiente. recuerde que tiene una cita pendiente para el aspirante '+itm.nombre_aspirante+' ';
		}
		await fetch(this.state.uri + '/api/notificar/tutor/' + itm.id_tutor + '/' + Number(this.state.schoolId) + '/' + mensaje, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}
		}).then(res => console.log(res))
			// .then(responseJson => {
			// 	;
			// });
	}

	async revisarPago(aspirante, tipo_pago) {
		await this._changeWheelState(true);
		await fetch(this.state.uri + '/api/get/estatus/pago/' + aspirante.id + '/' + tipo_pago, {
			method: 'GET', headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los aspirantes', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Solicitud de informes)',
						[{text: 'Entendido'}]);
				} else {
					if (responseJson.result.status === 'in_progress') {
						Alert.alert('Pago en proceso',
							'El pago del aspirante está siendo procesado,\neste será reflejado en un lapso de 24 a 48 horas despues de haber pagado', [
								 {text: 'Enterado', onPress: () => this._changeWheelState(false)}
							]);
					} else if (responseJson.result.status === 'completed') {
							Alert.alert('Pago acreditado',
								'El pago del aspirante ha sido acreditado. ¿Desea notificar al tutor?', [
									{
										text: 'Si',
										onPress: () => [this.sendNotif(aspirante),this._changeWheelState(false)]
									},{text:'No',onPress: () => this._changeWheelState(false)}
								]);
						
					}
				}
			});
	}

	rndAspirantes() {
		let aspirante = [];
		this.state.aspirantes.forEach((it, ix) => {
			let tipo_pago = '';
			if (it.paso_registro ==='3.1'){
				tipo_pago = 'preinscripcion'
			} else if (it.paso_registro === '5.1'){
				tipo_pago = 'inscripcion'
			}
			if (it.estatus_alta !== 'alta')
				aspirante.push(
					<View
						key={ix + 'aspCard'}
						style={[
							styles.cardHoras,
							styles.widthall,
							{
								backgroundColor: this.state.fourthColor,
								marginVertical: 5,
								padding: 10,
								alignItems: 'center',
								justifyContent: 'center'
							}
						]}
					>
						<Modal
							isVisible={this.state.modalTutor[ix]}
							backdropOpacity={0.8}
							animationIn={'bounceIn'}
							animationOut={'bounceOut'}
							animationInTiming={1000}
							animationOutTiming={1000}
						>
							<View
								style={[styles.container, {borderRadius: 6, flex: 0}]}
							>
								<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
									Datos del tutor
								</Text>
								<View style={[styles.modalWidth, {paddingHorizontal: 10}]}>
									<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Nombre:
										<Text style={[styles.textW]}> {it.tutor.nombre}</Text>
									</Text>
									<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Apellido(s):
										<Text style={[styles.textW]}> {it.tutor.apellidos}</Text>
									</Text>
									<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Telefono de
																									 contacto:
										<Text
											style={[styles.textW]}
											onPress={() => Communications.phonecall(it.tutor.telefono_contacto, true)}
										> {it.tutor.telefono_contacto}
										</Text>
									</Text>
									<Text style={{marginTop: 10, fontSize: responsiveFontSize(1.8)}}>Email:
										<Text style={[styles.textW]}> {it.tutor.email}</Text>
									</Text>
								</View>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{
											backgroundColor: '#fff',
											borderColor: this.state.secondColor,
											marginVertical: 15
										}
									]}
									onPress={() => this.closeModalT(ix)}
								>
									<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
								</TouchableOpacity>
							</View>
						</Modal>
						<View style={[styles.row, styles.widthall, {paddingHorizontal: 10}]}>
							<View style={[styles.row, styles.btn2_3, {justifyContent: 'flex-start'}]}>
								<Text style={{width: responsiveWidth(18)}}>Aspirante: </Text>
								<Text
									style={[styles.textW, styles.btn3_5, {textAlign: 'left'}]}> {it.nombre_aspirante}</Text>
							</View>
							{ifIphoneX ? null :
								<TouchableOpacity
									style={[styles.btn6,
										{borderRadius: 6, backgroundColor: this.state.mainColor, paddingVertical: 2}]}
									onPress={() => this.openModalT(ix)}
								>
									<Text
										style={{textAlign: 'center', color: '#fff'}}
									>
										Tutor
									</Text>
								</TouchableOpacity>}
						</View>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Escuela de procedencia: </Text>
							<Text style={[styles.textW, {textAlign: 'left'}]}> {it.escuela_procedente}</Text>
						</View>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Último promedio gral: </Text>
							<Text style={[styles.textW]}> {it.promedio_gral}</Text>
						</View>
						<View style={[styles.row, styles.widthall, {
							justifyContent: 'flex-start',
							paddingHorizontal: 10
						}]}>
							<Text>Grado al que aplica: </Text>
							<Text style={[styles.textW]}> {it.grado_interes}</Text>
						</View>
						{ifIphoneX ? <View style={[styles.widthall, {paddingHorizontal: 10, alignItems: 'center'}]}>
							<TouchableOpacity
								style={[styles.btn3,
									{
										borderRadius: 6,
										backgroundColor: this.state.mainColor,
										paddingVertical: 2,
										marginTop: 10
									}]}
								onPress={() => this.openModalT(ix)}
							>
								<Text
									style={{textAlign: 'center', color: '#fff'}}
								>
									Ver tutor
								</Text>
							</TouchableOpacity>
						</View> : null}
						<View style={[styles.btn1, {marginTop: 15, marginBottom: 10}]}>
							<StepIndicator
								customStyles={customStyles}
								stepCount={6}
								currentPosition={Number(it.paso_registro)}
								labels={labels_estForm}
							/>
						</View>
						{it.paso_registro === '0' ?
							(<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{
										backgroundColor: this.state.secondColor,
										borderColor: this.state.secondColor
									}
								]}
								onPress={() => Communications.phonecall(it.tutor.telefono_contacto, true)}
							>
								<Text
									style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}
								>
									Llamar al tutor
								</Text>
							</TouchableOpacity>)
							: it.paso_registro === '1' ?
								(<View style={[styles.row, styles.modalWidth]}>
									<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{
												backgroundColor: '#fff',
												borderColor: this.state.secondColor
											}
										]}
										onPress={() => Communications.phonecall(it.tutor.telefono_contacto, true)}
									>
										<Text
											style={[styles.textW, {
												color: this.state.secondColor,
												fontSize: responsiveFontSize(1.5)
											}]}>
											Llamar al tutor
										</Text>
									</TouchableOpacity>
									<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{
												backgroundColor: this.state.secondColor,
												borderColor: this.state.secondColor
											}
										]}
										onPress={() => this.alertEnviarNotif(it)}
									>
										<Text
											style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
											Enviar recordatorio
										</Text>
									</TouchableOpacity>
								</View>)
								: it.paso_registro === '2.1' ?
									(<TouchableOpacity
										style={[
											styles.modalBigBtn,
											{
												backgroundColor: this.state.secondColor,
												borderColor: this.state.secondColor
											}
										]}
										onPress={() => this.verDocumentos(it.documentos)}
									>
										<Text
											style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
											Revisar documentos
										</Text>
									</TouchableOpacity>)
									: it.paso_registro === '3.1' || it.paso_registro === '5.1' ?
										(<TouchableOpacity
											style={[
												styles.modalBigBtn,
												{
													backgroundColor: this.state.secondColor,
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.revisarPago(it, tipo_pago)}
										>
											<Text
												style={[styles.textW, {
													color: '#fff',
													fontSize: responsiveFontSize(1.5)
												}]}>
												Estatus de pago
											</Text>
										</TouchableOpacity>)
										: it.paso_registro === '4' ?
											(<View style={[styles.row, styles.modalWidth]}>
												<TouchableOpacity
													style={[
														styles.modalBigBtn,
														{
															backgroundColor: '#fff',
															borderColor: this.state.secondColor
														}
													]}
													onPress={() => [this.emailValidarAlumno(it, 'si'), this.notifvalidar(it, 'si')]}
												>
													<Text
														style={[styles.textW, {
															color: this.state.secondColor,
															fontSize: responsiveFontSize(1.5)
														}]}>
														Aceptar aspirante
													</Text>
												</TouchableOpacity>
												<TouchableOpacity
													style={[
														styles.modalBigBtn,
														{
															backgroundColor: this.state.secondColor,
															borderColor: this.state.secondColor
														}
													]}
													onPress={() => [this.emailValidarAlumno(it, 'no'), this.notifvalidar(it, 'no')]}
												>
													<Text
														style={[styles.textW, {
															color: '#fff',
															fontSize: responsiveFontSize(1.5)
														}]}>
														Rechazar aspirante
													</Text>
												</TouchableOpacity>
											</View>)
											:
											(<TouchableOpacity
												style={[
													styles.modalBigBtn,
													{
														backgroundColor: this.state.secondColor,
														borderColor: this.state.secondColor
													}
												]}
												onPress={() => this.alertEnviarNotif(it)}
											>
												<Text
													style={[styles.textW, {
														color: '#fff',
														fontSize: responsiveFontSize(1.5)
													}]}>
													Enviar recordatorio
												</Text>
											</TouchableOpacity>)}
					</View>
				)
		});
		return aspirante;
	}

	render() {
		let prospectos = this.state.aspirantes.length;
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Lista de aspirantes
				</Text>
				{prospectos !== 0 ? <ScrollView
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{...ifIphoneX({marginBottom: 35}, {marginBottom: 20})}}
				>
					{this.rndAspirantes()}
				</ScrollView> : <Text style={[styles.main_title, {
					color: '#a8a8a8',
					textAlign: 'center',
					marginTop: 40,
					marginBottom: 40,
					fontSize: responsiveFontSize(3)
				}]}>
					No hay solicitudes registradas aún
				</Text>}
			</View>
		);
	}
}
