import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import styles from '../../styles';
import MultiBotonRow from '../Globales/MultiBotonRow';
import {responsiveFontSize, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import Reinscripciones from './Reinscripciones';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Spinner from 'react-native-loading-spinner-overlay';

export default class inscripAdmin extends React.Component {
	_changeWheelState = (state) => {
		this.setState({
			visible: state
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			alumnos: [],
			visible: false,
			elAlumno: '',
			aux: 0,
			indxAlumno: -1,
			btnSelectedTab: 'Asignar altas',
			indxSelectedTab: 0,
			selectBtn: 'ingreso',
			totalGrupo: '',
			losGrupos: [],
			numeroAlumn: [],
			mujeres: [],
			hombres: [],
			selected: '',
			checkBoxBtns: [],
			checkBoxBtnsIndex: [],
			elGrupoPicker: '',
			gradoAlumn: '',
			alumnIngreso: [],
			alumnTraslado: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getAlumnosByGG();
		await this.getGrupos();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async botonSelected(indexBtn, itemBtn) {
		await this.setState({
			btnSelectedTab: itemBtn,
			indxSelectedTab: indexBtn,
			elAlumno: '',
			indxAlumno: -1,
			checkBoxBtnsIndex: [],
			checkBoxBtns: [],
			elGrupoPicker: ''
		});
	}

	async alertCambio(alumno, grado, grupo) {
		if (grupo === '') {
			Alert.alert('Grupo de destino indefinido',
				'Seleccione un grupo de destino para poder continuar', [
					{text: 'Enterado'}
				]);
		} else {
			if (this.state.selectBtn === 'traslado') {
				Alert.alert('¡Importante!',
					'Al realizar esta acción, cambiará el estatus de los alumnos seleccionados de "Inactivo" a "Activo" ' +
					'y aparecerán en la lista de asistencia del grupo que hayan asignado como destino.\n\nPor lo tanto, ' +
					'los alumnos seleccionados ya no aparecerán en ésta lista y para futuros cambios de grupo deberá ir ' +
					'a la sección de "Movimiento de alumnos".\n¿Esta seguro que desea continuar?', [
						{text: 'Sí', onPress: () => this.getCambioDeGrupo(alumno, grupo)}, {text: 'No'}
					]);
			} else {
				Alert.alert('¡Importante!',
					'Al realizar esta acción, cambiará el estatus de los alumnos seleccionados de "Inactivo" a "Activo" ' +
					'y aparecerán en la lista de asistencia del grupo que hayan asignado como destino.\n\nPor lo tanto, ' +
					'los alumnos seleccionados ya no aparecerán en ésta lista y para futuros cambios de grupo deberá ir ' +
					'a la sección de "Movimiento de alumnos".\n¿Esta seguro que desea continuar?', [
						{text: 'Sí', onPress: () => this.getCambioDeGrupo(alumno, grupo)}, {text: 'No'}
					]);
			}
		}
	}

	async getCambioDeGrupo(alumno, grupo) {
		await this._changeWheelState(true);
		if (this.state.selectBtn === 'traslado') {
			await fetch(this.state.uri + '/api/update/alumno/' + alumno.id + '/' + grupo, {
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error en movimiento de alumnos',
							'Ha ocurrido un error ' + responseJson.error.status_code +
							' al tratar de hacer el cambio si el error continua pónganse en contacto con soporte (Cambio de grupo)', [
								{text: 'Enterado', onPress: () => this._changeWheelState(false)}
							]);
					} else {
						Alert.alert('Cambio de grupo completo', 'Se ha cambiado al alumno de grupo correctamente', [
							{
								text: 'Entendido',
								onPress: () => [
									this.setState({
										selected: '',
										elGrupoPicker: '',
										elAlumno: '',
										checkBoxBtns: [],
										checkBoxBtnsIndex: [],
										indxAlumno: -1
									}),
									this.getAlumnosByGG(),
									this._changeWheelState(false),
								]
							}
						])
					}
				});
		} else {
			let request = [];
			for (let i = 0; i < alumno.length; i++) {
				await fetch(this.state.uri + '/api/update/alumno/' + alumno[i].id + '/' + grupo, {
					method: 'GET', headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}).then(res => res.json())
					.then(responseJson => {
						request = responseJson;
					});
			}
			if (request.error !== undefined) {
				Alert.alert('Error en movimiento de alumnos',
					'Ha ocurrido un error ' + request.error.status_code +
					' al tratar de hacer el cambio si el error continua pónganse en contacto con soporte (Cambio de grupo)', [
						{text: 'Enterado', onPress: () => this._changeWheelState(false)}
					]);
			} else {
				Alert.alert('Cambio de grupo completo', 'Se han cambiado a los alumnos de grupo correctamente', [
					{
						text: 'Entendido',
						onPress: () => [
							this.setState({
								selected: '',
								elGrupoPicker: '',
								elAlumno: '',
								checkBoxBtns: [],
								checkBoxBtnsIndex: [],
								indxAlumno: -1
							}),
							this.getAlumnosByGG(),
							this._changeWheelState(false)
						]
					}
				])
			}
		}
	}

//-+-+-+-+-+-+-+--+-+-+-+
	async getGrupos() {
		let grupopicker = await fetch(this.state.uri + '/api/get/grupos', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let grupospicker = await grupopicker.json();
		await this.setState({grupos: grupospicker});
		await this.setState({losGrupos: []});
		this.state.grupos.forEach((it) => {
			if (it.grupo !== 'Todos') {
				this.state.losGrupos.push(it.grupo)
			}
		});
		await this.setState({aux: 0});
	}

	async getAlumnosByGG2(grado, grupo) {
		await fetch(
			this.state.uri + '/api/get/alumnos/by/grado/grupo/' + grado + '/' + grupo,
			{
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					this.setState({mujeres: [], hombres: []});
					this.setState({numeroAlumn: responseJson});
					this.state.numeroAlumn.forEach((it) => {
						if (it.genero === 'femenino' || it.genero === 'Femenino') {
							this.state.mujeres.push(it);
						}
						if (it.genero === 'masculino' || it.genero === 'Masculino') {
							this.state.hombres.push(it);
						}
					});
				}
			});
		await this.setState({aux: 0});
	}

	async getTotalAlumnosByG(grado, grupo) {
		await fetch(this.state.uri + '/api/get/limitegrupo/' + grado + '/' + grupo, {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Inscripciones-Admin)');
				} else {
					this.setState({totalGrupo: responseJson[0]});
				}
			});
	}

//++-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ fin del desgarriate
	async getAlumnosByGG() {
		let ingreso = [];
		let traslado = [];
		await fetch(this.state.uri + '/api/alumnos/sin/grupo', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar aspiranes',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Altas-Inscripciones)', [
							{text: 'Entendido', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})}
						]);
				} else {
					this.setState({alumnos: responseJson});
					if (this.state.alumnos.length !== 0) {
						this.state.alumnos.forEach((it, i) => {
							if (it.estatus === 'ingreso') {
								ingreso.push(it);
							}
							if (it.estatus === 'traslado') {
								traslado.push(it);
							}
							this.state.checkBoxBtnsIndex[i] = 0;
						});
					}
					this.state.alumnIngreso = ingreso;
					this.state.alumnTraslado = traslado;
				}
			});
		await this.setState({aux: 0});
	}

	async selectAlumno(it, ix) {
		if (this.state.selectBtn === 'traslado') {
			await this.setState({elAlumno: it, indxAlumno: ix});
		} else {
			let i = this.state.checkBoxBtns.indexOf(it);
			if (i > -1) {
				this.state.checkBoxBtns.splice(i, 1);
				this.state.checkBoxBtnsIndex[ix] = 0;
			} else {
				this.state.checkBoxBtns.push(it);
				this.state.checkBoxBtnsIndex[ix] = 1;
			}
		}
		await this.setState({gradoAlumn: it.grado});
		await this.setState({aux: 0});
	}

	rndAlumnos2() {
		let alumnos = [];
		if (this.state.alumnTraslado.length !== 0) {
			this.state.alumnTraslado.forEach((itm, ix) => {
				let a = this.state.alumnTraslado.length - 1;
				let tabRow = [styles.rowTabla];
				if (ix !== a) {
					tabRow.push({borderColor: this.state.secondColor});
				}
				alumnos.push(
					<TouchableHighlight
						key={ix + 'inscrip'}
						underlayColor={'transparent'}
						style={[
							tabRow,
							{backgroundColor: this.state.indxAlumno === ix ? this.state.secondColor : '#fff'}
						]}
						onPress={() => this.selectAlumno(itm, ix)}
					>
						<View style={[styles.campoTablaG]}>
							<Text
								style={{color: this.state.indxAlumno === ix ? '#fff' : '#000'}}
							>
								{itm.name}
							</Text>
						</View>
					</TouchableHighlight>);
			});
		}
		return alumnos;
	}

	rndAlumnos1() {
		let alumnos = [];
		if (this.state.alumnIngreso.length !== 0) {
			this.state.alumnIngreso.forEach((itm, ix) => {
				let a = this.state.alumnIngreso.length - 1;
				let tabRow = [styles.rowTabla];
				if (ix !== a) {
					tabRow.push({borderColor: this.state.secondColor});
				}
				alumnos.push(
					<TouchableHighlight
						key={ix + 'inscrip'}
						underlayColor={'transparent'}
						style={[
							tabRow,
							{backgroundColor: this.state.checkBoxBtnsIndex[ix] === 1 ? this.state.secondColor : '#fff'}
						]}
						onPress={() => this.selectAlumno(itm, ix)}
					>
						<View style={[styles.campoTablaG]}>
							<Text
								style={{color: this.state.checkBoxBtnsIndex[ix] === 1 ? '#fff' : '#000'}}
							>
								{itm.name}
							</Text>
						</View>
					</TouchableHighlight>
				);
			});
		}
		return alumnos;
	}

	async selectedBtn(tipo) {
		await this.setState({
			selectBtn: tipo,
			elAlumno: '',
			checkBoxBtnsIndex: [],
			checkBoxBtns: [],
			indxAlumno: -1,
			totalGrupo: '',
			numeroAlumn: [],
			hombres: [],
			mujeres: [],
			elGrupoPicker: ''
		});
		await this.getAlumnosByGG();
	}

	alertD() {
		if (this.state.elAlumno === '') {
			Alert.alert('Sin alumno seleccionado',
				'Seleccione un alumno para escoger el grupo de destino',
				[{text: 'Entendido'}]);
		}
	}

	async onChange(option) {
		await this.setState({
			selected: option.label,
			elGrupoPicker: option.grupo
		});
		await this.getAlumnosByGG2(this.state.gradoAlumn, option.grupo);
		await this.getTotalAlumnosByG(this.state.gradoAlumn, option.grupo);
	}

	rndViewAlta() {
		let textob = [{color: '#000'}];
		let espacio = this.state.totalGrupo - this.state.numeroAlumn.length;
		if (this.state.totalGrupo === '') {
			textob.push({color: '#000'});
		} else if (espacio === 0) {
			textob.push({color: '#e10004'});
		} else {
			textob.push({color: '#1d9018'});
		}
		let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		if (this.state.selectBtn === 'traslado') {
			btnCal.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
			texto.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		let texto1 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnTem = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		if (this.state.selectBtn === 'ingreso') {
			btnTem.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
			texto1.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		let data = this.state.losGrupos.map((it, i) => {
			return {
				key: i, label: this.state.gradoAlumn + ' ' + it, grupo: it
			}
		});
		return (
			<View style={[styles.container]}>
				<ScrollView overScrollMode='always'
							showsVerticalScrollIndicator={false}
							style={{...ifIphoneX({marginBottom: 40}, {marginBottom: 20})}}
				>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione el tipo
					</Text>
					<View style={[styles.widthall, {alignItems: 'center'}]}>
						<View
							style={[styles.rowsCalif, {
								width: responsiveWidth(62), marginTop: 10
							}]}>
							<TouchableHighlight
								underlayColor={'transparent'}
								style={btnTem}
								onPress={() => this.selectedBtn('ingreso')}>
								<Text style={texto1}>Nuevo ingreso</Text>
							</TouchableHighlight>
							<TouchableHighlight
								underlayColor={'transparent'}
								style={btnCal}
								onPress={() => this.selectedBtn('traslado')}>
								<Text style={texto}>Traslado</Text>
							</TouchableHighlight>
						</View>
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione al alumno
					</Text>
					{this.state.selectBtn === 'ingreso' ?
						this.state.alumnIngreso.length !== 0 ?
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 5, padding: 1}
								]}
							>
								{this.rndAlumnos1()}
							</View> :
							<Text
								style={[styles.textW,
									styles.widthall,
									{fontSize: responsiveFontSize(2), marginTop: 10, textAlign: 'center'}]}
							>
								Sin altas pendientes
							</Text>
						: null}
					{this.state.selectBtn === 'traslado' ?
						this.state.alumnTraslado.length !== 0 ?
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 5, padding: 1}
								]}
							>
								{this.rndAlumnos2()}
							</View> :
							<Text
								style={[styles.textW,
									styles.widthall,
									{fontSize: responsiveFontSize(2), marginTop: 10, textAlign: 'center'}]}
							>
								Sin altas pendientes
							</Text>
						: null}
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione el grupo de destino
					</Text>
					{this.state.elAlumno !== '' || this.state.checkBoxBtns.length !== 0 ? (
						<ModalSelector
							cancelText='Cancelar'
							data={data}
							initValue='Seleccione el grupo'
							onChange={option => this.onChange(option)}
							optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
							selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
							selectStyle={[
								styles.inputPicker,
								{borderColor: this.state.secondColor, marginTop: 2}
							]}
						/>
					) : (
						<TouchableOpacity
							style={[
								styles.inputPicker,
								{borderColor: this.state.secondColor, marginTop: 2}
							]}
							onPress={() => this.alertD()}
						>
							<Text style={{fontSize: responsiveFontSize(1.8)}}>Seleccione el grupo</Text>
						</TouchableOpacity>
					)}
					<View
						style={[
							styles.row,
							styles.widthall,
							{marginTop: 15}
						]}>
						<View
							style={{
								width: responsiveWidth(60),
								paddingLeft: 5,
								fontSize: responsiveFontSize(1.6),
								alignItems: 'flex-start'
							}}>
							<Text style={{fontWeight: '700', fontSize: responsiveFontSize(1.8)}}>
								Datos relevantes de "{this.state.selected}"
							</Text>
						</View>
						<View
							style={{width: responsiveWidth(34)}}>
							<Text style={{fontWeight: '700', textAlign: 'left'}}>
								Contenido
							</Text>
						</View>
					</View>
					<View
						style={[styles.widthall, styles.row, {
							paddingVertical: 5,
							borderTopWidth: .5,
							borderBottomWidth: .5,
							borderColor: 'grey',
							marginTop: 5
						}]}>
						<Text style={{width: responsiveWidth(47), paddingLeft: 5}}>Espacios disponibles</Text>
						<Text style={[textob, {
							width: responsiveWidth(47),
							textAlign: 'center',
							justifyContent: 'center'
						}]}>
							{this.state.totalGrupo !== '' ? espacio + '/' + this.state.totalGrupo : '0'}
						</Text>
					</View>

					<View style={[styles.widthall, styles.row, {
						borderBottomWidth: .5,
						borderColor: 'grey',
						paddingVertical: 5
					}]}>
						<Text style={{
							width: responsiveWidth(47),
							paddingLeft: 5,
							textAlign: 'left'
						}}>
							Hombres y mujeres
						</Text>
						<View
							style={{
								width: responsiveWidth(47),
								alignItems: 'center',
								justifyContent: 'center'
							}}>
							<Text>M: {this.state.mujeres.length}{'    '}H: {this.state.hombres.length}</Text>
						</View>
					</View>
					<View style={[styles.widthall, styles.row, {
						borderBottomWidth: .5,
						borderColor: 'grey',
						paddingVertical: 5
					}]}>
						<Text style={{width: responsiveWidth(47), textAlign: 'left', paddingLeft: 5}}>
							Promedio general del grupo
						</Text>
						<Text style={{width: responsiveWidth(47), textAlign: 'center'}}>No disponible</Text>
					</View>
					{this.state.selectBtn === 'traslado' ?
						<TouchableOpacity
							style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
							onPress={() => this.alertCambio(this.state.elAlumno, this.state.gradoAlumn, this.state.elGrupoPicker)}
						>
							<Text style={[styles.textButton]}>
								Activar alumno
							</Text>
						</TouchableOpacity> :
						<TouchableOpacity
							style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
							onPress={() => this.alertCambio(this.state.checkBoxBtns, this.state.gradoAlumn, this.state.elGrupoPicker)}
						>
							<Text style={[styles.textButton]}>
								Activar alumnos
							</Text>
						</TouchableOpacity>
					}
				</ScrollView>
			</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<MultiBotonRow
					itemBtns={[
						'Asignar altas',
						'Reinscripciones'
					]}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelected(indexBtn, itemBtn)
					}
					cantidad={2}
				/>

				{this.state.indxSelectedTab === 0 ? this.rndViewAlta() : <Reinscripciones/>}

			</View>
		);
	}
}
