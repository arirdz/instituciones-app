import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight
} from 'react-native';
import {
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Swipeable from 'react-native-swipeable';

export default class GestionarTarjetas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    render() {
        const rightButtons = [
            <TouchableHighlight
                style={{
                    marginTop: 5,
                    backgroundColor: 'red',
                    justifyContent: 'center',
                    height: responsiveHeight(12),
                    alignItems: 'center',
                    width: responsiveWidth(22)
                }}
            >
                <Text>Borrar</Text>
            </TouchableHighlight>
        ];
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la notificación
                        </Text>
                        <Swipeable rightButtons={rightButtons}>
                            <View
                                style={[
                                    styles.notificaciones,
                                    {borderColor: this.state.fourthColor}
                                ]}
                            >
                                <Text style={styles.notifTitle}>
                                    Notificación
                                </Text>
                                <Text style={styles.textInfo}>
                                    Esta es una notificación para notificarle {'\n'} que esta
                                    notificado de la notificación notificada
                                </Text>
                                <View style={styles.theButtons}>
                                    <TouchableOpacity style={styles.buttonAceptar}>
                                        <Text style={styles.titBtn_GCM}>Aceptar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.buttonMensaje}>
                                        <Text style={styles.titBtn_GCM}>Mensaje</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Swipeable>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
