import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight, Image, Alert
} from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import MultiBotonRow from "../Globales/MultiBotonRow";
import Spinner from 'react-native-loading-spinner-overlay';
import Entypo from "react-native-vector-icons/Entypo";
import {Actions} from "react-native-router-flux";


const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class PagarServicio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            cargos: [],
            pagos: [],
            botonSelected: 'Pagar Servicio',
            visible: false,
            userRef: [],
            escuela_mexico_id: '',
            datos: []
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getIdEscuela();
        await this.setState({visible:true})
        await this.getUserdata();
        console.log(this.state.uri);
    }

    async getUserdata() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data', {
			method: 'GET', headers: {
				Authorization: 'Bearer ' + token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos',
						'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 7)',
						[{text: 'Entendido'}]
					);
				} else {
                    this.setState({datos: responseJson});
                    console.log(this.state.datos)
                    this.registrado();
				}
			});
    }

    async registrado(){
        console.log(this.state.uri)
        let nombreCorto = '';
        let nombreCorto1 = '';
        if(this.state.uri === 'https://controlescolar.pro'){
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.pro', '');
        }else{
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.controlescolar.pro', '');
        }
        await fetch('http://127.0.0.1:8080/api/save/director', {
			method: 'post',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			}, body: JSON.stringify({
				nombre: this.state.datos.nombre,
				apellido: this.state.datos.apellido_paterno + ' ' + this.state.datos.apellido_materno,
				email: this.state.datos.email_tutor,
				calle: this.state.datos.calle,
				numero: this.state.datos.numero,
				colonia: this.state.datos.colonia,
				estado: this.state.datos.estado,
				ciudad: this.state.datos.ciudad,
				codigo_postal: this.state.datos.cp,
                nombre_corto: nombreCorto1
			})
		}).then(res => res.json())
			.then(responseJson => {
				console.log(responseJson)
				if (responseJson.error === null) {
				console.log(responseJson)
				} else {
                    console.log(responseJson)
                    if(responseJson.registrado){
                        console.log('ir a requestm')
                        // this.requestMultipart();
                    }
				}
			});
    }

    getIdEscuela(){
        console.log(this.state.uri)
        let nombreCorto = '';
        let nombreCorto1 = '';
        if(this.state.uri === 'https://controlescolar.pro'){
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.pro', '');
        }else{
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.controlescolar.pro', '');
        }
        console.log('http://127.0.0.1:8080' + '/api/get/idEscuelaMexico/' + nombreCorto1)
        fetch('http://127.0.0.1:8080' + '/api/get/idEscuelaMexico/' + nombreCorto1 ,{
			method: 'GET',
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos',
						'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 7)',
						[{text: 'Entendido'}]
					);
				} else {
                   this.setState({escuela_mexico_id: responseJson.id_escuela_mexico})
                   console.log('id_escuela_mexico', this.state.escuela_mexico_id)
                   this.getCargos();
                   this.getPagos();
				}
			});
    }

    pagar(item, i){
        Actions.pagoServicio({user_data: this.state.datos, pago: item})
        // console.log(item)
        //     try {
        //          AsyncStorage.setItem('itemid', item.id.toString());
        //          AsyncStorage.setItem('itemTotalToPay', item.monto);
        //          AsyncStorage.setItem('itemHijoId', 'xxx');
        //          AsyncStorage.setItem('itemDescripcion', 'Pago Control Escolar '+ item.mes);
        //          AsyncStorage.setItem('item_due_date', item.mes);
        //          AsyncStorage.setItem('userID', this.state.userRef.user_id.toString());
        //     } catch (error) {
        //         console.error('AsyncStorage error: ' + error.message);
        //     }
        // if(this.state.cargos.length === 2){
        //     if (i === 0){
        //         Alert.alert(
        //             'Error', 'Usted tiene un pago pendiente. Debe realizar el pago del mes vencido, para poder pagar el mes actual.',
        //             [{text: 'Entendido'}]
        //         );
        //     }else{
        //         Alert.alert(
        //             'Pago', 'PAGAR',
        //             [{text: 'Entendido'}]
        //         );
        //     }
        // }else{
        //   Actions.formadepagoquenofunciona({FinalPrice: item.monto})
        // }
    }

    renderCards() {
        let cards = [];
        this.state.cargos.forEach((item, i) => {
            cards.push(
                <View key={i + 'carta'}>
                <View
                    key={i + 'carta'}
                    style={[
                        styles.widthall,
                        // styles.row,
                        {
                            borderWidth: 1,
                            borderRadius: 6,
                            borderColor: this.state.secondColor,
                            backgroundColor: this.state.fourthColor,
                            paddingHorizontal: 6,
                            paddingVertical: 10,
                            marginTop: 10,

                        }
                    ]}>
                    <View style={[styles.row, styles.widthall, {justifyContent: "flex-start"}]}>
                        <View style={[styles.btn3]}>
                            <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                Monto del cargo
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'left',
                                marginTop: 5
                            }}>{item.monto}</Text>
                        </View>
                        <View style={[styles.btn3]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Fecha límite de pago
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 5
                            }}>{moment(item.mes).format('ll')}</Text>
                        </View>
                        <View style={[styles.btn3, {alignItems: 'center', textAlign: 'center'}]}>
                            <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                Pago
                            </Text>
                            <Text style={{
                                fontSize: responsiveFontSize(1.5),
                                textAlign: 'center',
                                marginTop: 5
                            }}>{item.pago_numero}</Text>
                        </View>
                    </View>
                </View>
                    {this.state.cargos.length === 2 && i === 0 ?
                        ( <TouchableOpacity
                            onPress={() => this.pagar(item)}
                            style={[styles.widthall,{alignItems: 'center', justifyContent: 'center', height: responsiveHeight(3.6),backgroundColor: 'gray', borderRadius: 5, marginTop: -3}]}>
                            <Text style={{color: 'white', fontWeight: '800'}}>Pagar</Text>
                        </TouchableOpacity>):
                        ( <TouchableOpacity
                            onPress={() => this.pagar(item)}
                            style={[styles.widthall,{alignItems: 'center', justifyContent: 'center', height: responsiveHeight(3.6),backgroundColor: this.state.secondColor, borderRadius: 5, marginTop: -3}]}>
                            <Text style={{color: 'white', fontWeight: '800'}}>Pagar</Text>
                        </TouchableOpacity>)
                    }
                </View>
            )
        });
        return cards;
    }

    renderHistorial() {
        let cards = [];
        this.state.pagos.forEach((item, i) => {
            cards.push(
                <View key={i + 'carta'}>
                    <View
                        key={i + 'carta'}
                        style={[
                            styles.widthall,
                            // styles.row,
                            {
                                borderWidth: 1,
                                borderRadius: 6,
                                borderColor: this.state.secondColor,
                                backgroundColor: this.state.fourthColor,
                                paddingHorizontal: 6,
                                paddingVertical: 10,
                                marginTop: 10,

                            }
                        ]}>
                        <View style={[styles.row, styles.widthall, {justifyContent: "flex-start"}]}>
                            <View style={[styles.btn3]}>
                                <Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
                                    Monto del cargo
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'left',
                                    marginTop: 5
                                }}>{item.monto}</Text>
                            </View>
                            <View style={[styles.btn3]}>
                                <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                    Fecha límite de pago
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center',
                                    marginTop: 5
                                }}>{moment(item.mes).format('ll')}</Text>
                            </View>
                            <View style={[styles.btn3, {alignItems: 'center', textAlign: 'center'}]}>
                                <Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                                    Pago
                                </Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.5),
                                    textAlign: 'center',
                                    marginTop: 5
                                }}>{item.pago_numero}</Text>
                            </View>
                        </View>
                    </View>
                    {item.status === 'Pendiente' ?
                        (<TouchableOpacity
                            onPress={() => this.pagar(i)}
                            style={[styles.widthall,{alignItems: 'center', justifyContent: 'center', height: responsiveHeight(3.6),backgroundColor: '#ff8000', borderRadius: 5, marginTop: -3}]}>
                            <Text style={{color: 'white'}}>Por pagar</Text>
                        </TouchableOpacity>):item.status === 'Pagado'?
                            (<TouchableOpacity
                                onPress={() => this.pagar(i)}
                                style={[styles.widthall,{alignItems: 'center', justifyContent: 'center', height: responsiveHeight(3.6),backgroundColor: 'green', borderRadius: 5, marginTop: -3}]}>
                                <Text style={{color: 'white'}}>{item.status}</Text>
                            </TouchableOpacity>):  (<TouchableOpacity
                                onPress={() => this.pagar(i)}
                                style={[styles.widthall,{alignItems: 'center', justifyContent: 'center', height: responsiveHeight(3.6),backgroundColor: '#3a4e7a', borderRadius: 5, marginTop: -3}]}>
                                <Text style={{color: 'white'}}>{item.status}</Text>
                            </TouchableOpacity>)
                    }

                </View>
            )
        });
        return cards;
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }



    getCargos() {
        fetch('http://127.0.0.1:8080/api/get/pendientes/' + this.state.escuela_mexico_id, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                console.log(responseJson)
                this.setState({visible:false})
                if (responseJson.error !== undefined) {
                    this.setState({visible:false})
                    console.log(responseJson)
                } else {
                    if(responseJson.length !== 0){  
                        this.setState({cargos: responseJson, visible:false})
                    }
                }
            });
    }

    getPagos() {
        fetch('http://127.0.0.1:8080/api/get/cargos/' + this.state.escuela_mexico_id, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    console.log(responseJson)
                } else {
                    console.log(responseJson)
                    if(responseJson.length !== 0){
                        this.setState({pagos: responseJson})
                    }
                }
            });
    }

    pagarServicio(){
        return (
            <View>
                <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 10}]}>Seleccione el pago correspondiente:</Text>
                <ScrollView showsVerticalScrollIndicator={false}>
                {this.renderCards()}
                </ScrollView>
            </View>
        )
    }

    historial(){
        return(
        <View>
            <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 10}]}>Lista de pagos:</Text>
            <ScrollView showsVerticalScrollIndicator={false}>
                {this.renderHistorial()}
            </ScrollView>
        </View>
    )
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <Spinner visible={this.state.visible} textContent='Consultando pagos...'/>
                <MultiBotonRow
                    itemBtns={["Pagar Servicio", "Historial de pagos"]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />
                {this.state.botonSelected === "Pagar Servicio" ? this.pagarServicio() : null}
                {this.state.botonSelected === "Historial de pagos" ? this.historial() : null}
            </View>
        );
    }
}
