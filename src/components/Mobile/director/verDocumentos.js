import React from 'react';
import {
	Alert, AsyncStorage, CameraRoll, Image, Modal, PermissionsAndroid, Platform, ScrollView, StatusBar, Text, TextInput,
	TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Spinner from 'react-native-loading-spinner-overlay';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class verDocumentos extends React.Component {
	_handleButtonPress = () => {
		CameraRoll.getPhotos({
			first: 1,
			assetType: 'All'
		})
			.then(r => {
				this.setState({photos: r.edges});
			})
			.catch((err) => {
				//Error Loading Images
			});
	};

	constructor(props) {
		super(props);
		this.state = {
			losDocumentos: this.props.doctos,
			photos: [],
			open: false,
			aux: 0,
			visible: false,
			modalVisible1: false,
			elActa: 0,
			laCurp: 0,
			laBoleta: 0,
			fotoAspirante: 0,
			indexSelectedImage: -1,
			pictures: [],
			idTutor: '',
			idAspirante: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
		if (this.props.doctos.validar_acta === '2') {
			this.setState({elActa: 2});
		}
		if (this.props.doctos.validar_curp === '2') {
			this.setState({laCurp: 2});
		}
		if (this.props.doctos.validar_boleta === '2') {
			this.setState({laBoleta: 2});
		}
		if (this.props.doctos.validar_foto === '2') {
			this.setState({fotoAspirante: 2});
		}
	}

	async _changeWheelState(state) {
		this.setState({visible2: state});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let schoolId = await AsyncStorage.getItem('schoolId');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor,
			schoolId: schoolId
		});
	}

	async alertAjuste() {
		if (this.state.elActa === 0 || this.state.laCurp === 0 || this.state.laBoleta === 0 || this.state.fotoAspirante === 0) {
			Alert.alert('Documentos por validar',
				'Valide todos los documentos para poder continuar', [
					{text: 'Enterado'}
				])
		} else {
			Alert.alert('Solicitar ajuste',
				'Está a punto de solicitar ajuste en la documentación del aspirante ¿Seguro que desea continuar?', [
					{text: 'Sí', onPress: () => this.updateDocumento(this.state.losDocumentos, 2, 'no')}, {text: 'No'}
				])
		}

	}

	async updateDocumento(document, numero, validacion) {
		await this._changeWheelState(true);
		const fd = new FormData();
		fd.append('update', JSON.stringify({
			id_tutor: document.id_tutor,
			id_aspirante: document.id_aspirante,
			acta_nacimiento: document.acta_nacimiento,
			curp_aspirante: document.curp_aspirante,
			boleta_aspirante: document.boleta_aspirante,
			foto_aspirante: document.foto_aspirante,
			validar_acta: this.state.elActa,
			validar_curp: this.state.laCurp,
			validar_boleta: this.state.laBoleta,
			validar_foto: this.state.fotoAspirante
		}));
		fetch(this.state.uri + '/api/validar/documentos/admin', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data'
			}, body: fd
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al subir documento', 'Ha ocurrido un error ' + responseJson.error.status_code +
					' al tratar de subir el documento, si el error continua pónganse en contacto con soporte (Documentos aspirante)',
					[{
						text: 'Entendido',
						onPress: () => [this._changeWheelState(false), Actions.pop({refresh: {key: 'drawer'}})]
					}]);
			} else {
				this.validarDocumentos(document, numero, validacion)
			}
		});
	}

	async enviarCorreo(document, numer, validacion) {
		await this._changeWheelState(true);
		await fetch(this.state.uri + '/api/editar/documentacion/' + document.id_tutor + '/' + document.id_aspirante + '/' + validacion).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error enviar correo', 'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de enviar un correo, si el error continua pónganse en contacto con soporte (Documentos aspirante)',
						[{
							text: 'Entendido',
							onPress: () => [this._changeWheelState(false), Actions.pop({refresh: {key: 'drawer'}})]
						}]);
				} else {
					if (validacion === 'no') {
						Alert.alert('Ajuste solicitado',
							'Se le ha enviado un correo al tutor del aspirante para el ajuste de los documentos',
							[{
								text: 'Entendido',
								onPress: () => [this._changeWheelState(false), Actions.replace('newAspirantes', {key: Math.random()})]
							}]);
					} else if (validacion === 'si') {
						Alert.alert('Documentación validada',
							'Se le ha enviado un correo al tutor del aspirante para continuar con el proceso de preinscripción',
							[{
								text: 'Entendido',
								onPress: () => [this._changeWheelState(false), Actions.replace('newAspirantes', {key: Math.random()})]
							}]);
					}
				}
			});
	}

	async alertValidarDoctos() {
		Alert.alert('Validar documentos',
			'Está a punto de validar los documentos del aspirante ¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.validarDocumentos(this.state.losDocumentos, 3, 'si')}, {text: 'No'}
			])
	}

	async validarDocumentos(documentos, numero, validar) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id: documentos.id_aspirante,
			paso_registro: numero
		}));
		await fetch(this.state.uri + '/api/cita/aspirantes', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al validar los documentos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' si el error continua pónganse en contacto con soporte (Documentos aspirante)', [
							{
								text: 'Entendido',
								onPress: () => [this._changeWheelState(false), Actions.pop({refresh: {key: 'drawer'}})]
							}]);
				} else {
					this.enviarCorreo(documentos, numero, validar);
					this.sendNotif(documentos, validar);
				}
			});
	}

	async sendNotif(documentos, validar) {
		let mensaje = '';
		if (validar === 'si') {
			mensaje = 'Se han revisado sus documentos de manera exitosa.\nAhora podrá realizar el pago de preinscripción'
		} else {
			mensaje = 'Hemos revisado su documentacion.\nTendra que realizar algunos cambios a los documentos, se le ha enviado un e-mail con las instrucciones a seguir'
		}
		await fetch(this.state.uri + '/api/notificar/tutor/' + documentos.id_tutor + '/' + this.state.schoolId + '/' + mensaje, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}
		}).then(res => res.json())
			.then(responseJson => {
				console.log(responseJson);
			});
	}

	async open(documento) {
		await this.setState({elDocumento: documento, open: true})
	}

	async descargar(docto) {
		await this._handleButtonPress();
		await CameraRoll.saveToCameraRoll(docto);
		await Alert.alert('Imagen', 'Guardada', [{
			text: 'Entendido'
		}]);
	}

	documento() {
		return (
			<Modal
				style={{backgroundColor: 'lightgrey'}}
				animationType='slide'
				transparent={false}
				visible={this.state.open}
				style={[styles.container]}
			>
				{Platform.OS === 'ios' ? (
					<View
						style={{
							backgroundColor: this.state.mainColor,
							...ifIphoneX({height: responsiveHeight(5)}, {height: responsiveHeight(3)}),
							width: responsiveWidth(100)
						}}
					/>
				) : null}
				<View style={[styles.container, {backgroundColor: '#fff'}]}>
					<Image
						style={{
							width: responsiveWidth(100),
							height: responsiveHeight(75),
							marginTop: 10,
							resizeMode: 'contain'
						}}
						defaultSource={require('../../../images/giphy.gif')}
						source={{uri: this.state.elDocumento}}
					/>
					{Platform.OS === 'ios' ?
						<View style={[styles.widthall, {alignItems: 'center'}]}>
							<View style={[styles.modalWidth, styles.row]}>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: '#fff', borderColor: this.state.secondColor}
									]}
									onPress={() => this.setState({open: false})}
								>
									<Text style={[styles.textW, {
										color: this.state.secondColor,
										fontSize: responsiveFontSize(1.5)
									}]}>
										Cerrar
									</Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
									]}
									onPress={() => this.descargar(this.state.elDocumento)}
								>
									<Text style={[styles.textW, {
										color: '#fff',
										fontSize: responsiveFontSize(1.5)
									}]}>
										Descargar documento
									</Text>
								</TouchableOpacity>
							</View>
						</View> :
						<View style={[styles.widthall, {alignItems: 'center'}]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({open: false})}
							>
								<Text style={[styles.textW, {
									color: this.state.secondColor,
									fontSize: responsiveFontSize(1.5)
								}]}>
									Cerrar
								</Text>
							</TouchableOpacity>
						</View>}
				</View>
			</Modal>
		)
	}

	render() {
		return (
			<View style={styles.container}>
				{this.documento()}
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Validación de documentos
				</Text>
				<ScrollView
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{...ifIphoneX({marginBottom: 35}, {marginBottom: 20})}}
				>
					{/*<-+-+---+-+-+-+-+-+-+-+-+aqui empiezan las cards+++-+-+-+-+-+-+-+-+-+-+-->*/}
					<View style={[styles.widthall]}>
						{/*Acta de nacimiento*/}
						<View
							style={[
								styles.widthall,
								styles.row,
								styles.carDocumentos,
								{
									backgroundColor: this.state.fourthColor,
									marginVertical: 5,
									borderRadius: 10,
									alignItems: 'center'
								}
							]}
						>
							<View>
								<Image
									style={{
										width: responsiveWidth(30),
										height: responsiveHeight(17),
										resizeMode: 'cover'
									}}
									source={
										this.state.losDocumentos === '' ?
											require('../../../images/giphy.gif') :
											{uri: this.state.losDocumentos.acta_nacimiento}
									}
								/>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{
											width: responsiveWidth(30),
											height: responsiveHeight(4),
											backgroundColor: this.state.mainColor,
											borderColor: this.state.mainColor,
											marginVertical: 0,
											borderBottomLeftRadius: 10,
											borderRadius: 0
										}
									]}
									onPress={() => this.open(this.state.losDocumentos.acta_nacimiento)}
								>
									<Text
										style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
										Ampliar
									</Text>
								</TouchableOpacity>
							</View>
							<View style={[{width: responsiveWidth(64), paddingHorizontal: 10}]}>
								<Text
									style={[styles.main_title,
										{
											color: this.state.thirdColor,
											width: responsiveWidth(64),
											textAlign: 'left'
										}
									]}
								>
									Acta de nacimiento
								</Text>
								{this.state.losDocumentos.validar_acta === '2' ?
									<View style={[styles.btn7, {alignItems: 'center'}]}>
										<View style={[
											styles.modalBigBtn, styles.btn2,
											{borderColor: this.state.secondColor, backgroundColor: '#fff'}
										]}
										>
											<Text style={[styles.textW, {color: this.state.secondColor}]}>
												Documento validado
											</Text>
										</View>
									</View> : <View style={[styles.row]}>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.elActa === 1 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({elActa: 1})}
										>
											<Text style={[styles.textW,
												{color: this.state.elActa === 1 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.elActa === 1 ? 'Rechazado' : 'Rechazar'}
											</Text>
										</TouchableOpacity>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.elActa === 2 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({elActa: 2})}
										>
											<Text style={[styles.textW,
												{color: this.state.elActa === 2 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.elActa === 2 ? 'Validado' : 'Validar'}
											</Text>
										</TouchableOpacity>
									</View>
								}
							</View>
						</View>
						{/*C.U.R.P*/}
						<View
							style={[
								styles.widthall,
								styles.row,
								styles.carDocumentos,
								{
									backgroundColor: this.state.fourthColor,
									marginVertical: 5,
									borderRadius: 10,
									alignItems: 'center'
								}
							]}
						>
							<View>
								<Image
									style={{
										width: responsiveWidth(30),
										height: responsiveHeight(17),
										resizeMode: 'cover'
									}}
									source={
										this.state.losDocumentos === '' ?
											require('../../../images/giphy.gif') :
											{uri: this.state.losDocumentos.curp_aspirante}
									}
								/>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{
											width: responsiveWidth(30),
											height: responsiveHeight(4),
											backgroundColor: this.state.mainColor,
											borderColor: this.state.mainColor,
											marginVertical: 0,
											borderBottomLeftRadius: 10,
											borderRadius: 0
										}
									]}
									onPress={() => this.open(this.state.losDocumentos.curp_aspirante)}
								>
									<Text
										style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
										Ampliar
									</Text>
								</TouchableOpacity>
							</View>
							<View style={[{width: responsiveWidth(64), paddingHorizontal: 10}]}>
								<Text
									style={[styles.main_title,
										{
											color: this.state.thirdColor,
											width: responsiveWidth(64),
											textAlign: 'left'
										}
									]}
								>
									C.U.R.P
								</Text>
								{this.state.losDocumentos.validar_curp === '2' ?
									<View style={[styles.btn7, {alignItems: 'center'}]}>
										<View style={[
											styles.modalBigBtn, styles.btn2,
											{borderColor: this.state.secondColor, backgroundColor: '#fff'}
										]}
										>
											<Text style={[styles.textW, {color: this.state.secondColor}]}>
												Documento validado
											</Text>
										</View>
									</View> : <View style={[styles.row]}>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.laCurp === 1 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({laCurp: 1})}
										>
											<Text
												style={[
													styles.textW,
													{color: this.state.laCurp === 1 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.laCurp === 1 ? 'Rechazado' : 'Rechazar'}
											</Text>
										</TouchableOpacity>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.laCurp === 2 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({laCurp: 2})}
										>
											<Text style={[
												styles.textW,
												{color: this.state.laCurp === 2 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.laCurp === 2 ? 'Validado' : 'Validar'}
											</Text>
										</TouchableOpacity>
									</View>}
							</View>
						</View>
						{/*Ultima boleta de calificaciones*/}
						<View
							style={[
								styles.widthall,
								styles.row,
								styles.carDocumentos,
								{
									backgroundColor: this.state.fourthColor,
									marginVertical: 5,
									borderRadius: 10,
									alignItems: 'center'
								}
							]}
						>
							<View>
								<Image
									style={{
										width: responsiveWidth(30),
										height: responsiveHeight(17),
										resizeMode: 'cover'
									}}
									source={
										this.state.losDocumentos === '' ?
											require('../../../images/giphy.gif') :
											{uri: this.state.losDocumentos.boleta_aspirante}
									}
								/>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{
											width: responsiveWidth(30),
											height: responsiveHeight(4),
											backgroundColor: this.state.mainColor,
											borderColor: this.state.mainColor,
											marginVertical: 0,
											borderBottomLeftRadius: 10,
											borderRadius: 0
										}
									]}
									onPress={() => this.open(this.state.losDocumentos.boleta_aspirante)}
								>
									<Text
										style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
										Ampliar
									</Text>
								</TouchableOpacity>
							</View>
							<View style={[{width: responsiveWidth(64), paddingHorizontal: 10}]}>
								<Text
									style={[styles.main_title,
										{
											color: this.state.thirdColor,
											width: responsiveWidth(64),
											textAlign: 'left'
										}
									]}
								>
									Última boleta de calificaciones
								</Text>
								{this.state.losDocumentos.validar_boleta === '2' ?
									<View style={[styles.btn7, {alignItems: 'center'}]}>
										<View style={[
											styles.modalBigBtn, styles.btn2,
											{borderColor: this.state.secondColor, backgroundColor: '#fff'}
										]}
										>
											<Text style={[styles.textW, {color: this.state.secondColor}]}>
												Documento validado
											</Text>
										</View>
									</View> : <View style={[styles.row]}>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.laBoleta === 1 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({laBoleta: 1})}
										>
											<Text
												style={[
													styles.textW,
													{color: this.state.laBoleta === 1 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.laBoleta === 1 ? 'Rechazado' : 'Rechazado'}
											</Text>
										</TouchableOpacity>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.laBoleta === 2 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({laBoleta: 2})}
										>
											<Text
												style={[
													styles.textW,
													{color: this.state.laBoleta === 2 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.laBoleta === 2 ? 'Validado' : 'Validar'}
											</Text>
										</TouchableOpacity>
									</View>}
							</View>
						</View>
						{/*foto del aspirante*/}
						<View
							style={[
								styles.widthall,
								styles.row,
								styles.carDocumentos,
								{
									backgroundColor: this.state.fourthColor,
									marginVertical: 5,
									borderRadius: 10,
									alignItems: 'center'
								}
							]}
						>
							<View>
								<Image
									style={{
										width: responsiveWidth(30),
										height: responsiveHeight(17),
										resizeMode: 'cover'
									}}
									source={
										this.state.losDocumentos === '' ?
											require('../../../images/giphy.gif') :
											{uri: this.state.losDocumentos.foto_aspirante}
									}
								/>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{
											width: responsiveWidth(30),
											height: responsiveHeight(4),
											backgroundColor: this.state.mainColor,
											borderColor: this.state.mainColor,
											marginVertical: 0,
											borderBottomLeftRadius: 10,
											borderRadius: 0
										}
									]}
									onPress={() => this.open(this.state.losDocumentos.foto_aspirante)}
								>
									<Text
										style={[styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.5)}]}>
										Ampliar
									</Text>
								</TouchableOpacity>
							</View>
							<View style={[{width: responsiveWidth(64), paddingHorizontal: 10}]}>
								<Text
									style={[styles.main_title,
										{
											color: this.state.thirdColor,
											width: responsiveWidth(64),
											textAlign: 'left'
										}
									]}
								>
									Foto del aspirante
								</Text>
								{this.state.losDocumentos.validar_foto === '2' ?
									<View style={[styles.btn7, {alignItems: 'center'}]}>
										<View style={[
											styles.modalBigBtn, styles.btn2,
											{borderColor: this.state.secondColor, backgroundColor: '#fff'}
										]}
										>
											<Text style={[styles.textW, {color: this.state.secondColor}]}>
												Documento validado
											</Text>
										</View>
									</View> : <View style={[styles.row]}>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.fotoAspirante === 1 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({fotoAspirante: 1})}
										>
											<Text
												style={[
													styles.textW,
													{color: this.state.fotoAspirante === 1 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.fotoAspirante === 1 ? 'Rechazado' : 'Rechazar'}
											</Text>
										</TouchableOpacity>
										<TouchableOpacity
											style={[
												styles.modalBigBtn,
												styles.btn3L,
												{
													height: responsiveHeight(4),
													padding: 0,
													backgroundColor: this.state.fotoAspirante === 2 ? this.state.secondColor : '#fff',
													borderColor: this.state.secondColor
												}
											]}
											onPress={() => this.setState({fotoAspirante: 2})}
										>
											<Text
												style={[
													styles.textW,
													{color: this.state.fotoAspirante === 2 ? '#fff' : this.state.secondColor}]}
											>
												{this.state.fotoAspirante === 2 ? 'Validado' : 'Validar'}
											</Text>
										</TouchableOpacity>
									</View>}
							</View>
						</View>
					</View>
					{/*+-+-+-+-+--+-+-+--+-aqui acaban+-+-+-+-+-+-+-+-+-+-+-+-*/}
					{this.state.elActa === 1 || this.state.laCurp === 1 || this.state.laBoleta === 1 || this.state.fotoAspirante === 1 ?
						<TouchableOpacity
							style={[
								styles.bigButton,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.alertAjuste()}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Solicitar ajuste</Text>
						</TouchableOpacity> :
						this.state.elActa === 2 && this.state.laCurp === 2 && this.state.laBoleta === 2 && this.state.fotoAspirante === 2 ?
							<TouchableOpacity
								style={[
									styles.bigButton,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.alertValidarDoctos()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Validar documentos</Text>
							</TouchableOpacity> : null
					}
				</ScrollView>
			</View>
		);
	}
}
