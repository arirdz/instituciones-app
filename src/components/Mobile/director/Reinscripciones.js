import React from 'react';
import {
	Alert, AsyncStorage, Keyboard, Platform, ScrollView, StatusBar, Text, TextInput, TouchableHighlight,
	TouchableOpacity, View
} from 'react-native';
import styles from '../../styles';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import GradoyGrupo from '../Globales/GradoyGrupo';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {TextInputMask} from 'react-native-masked-text';
import Modal from 'react-native-modal';
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');


export default class Reinscripciones extends React.Component {
	//+++-+-+-+-+-+date picker
	_hideDateTimePicker = () => this.setState({fechaPicker: false});

	_showDateTimePicker = () => this.setState({fechaPicker: true});

	_handleDatePicked = (date) => {
		let fecha1 = moment(date).format('YYYY-MM-DD');
		this.setState({laFecha: fecha1});
		this.setState({aux: 0});
		this._hideDateTimePicker();
	};
	spinner = (state) => {
		this.setState({
			visible: state
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			alumnos: [],
			visible: false,
			fechaPicker: false,
			elGrado: '',
			laFecha: '',
			indexGrado: -1,
			elGrupo: '',
			elMonto: '',
			isModalDate: [],
			indexGrupo: -1,
			checkBoxBtnsIndex: [],
			checkBoxBtns: [],
			aux: 0,
			fechasPago: [],
			alumnosAuth: [],
			selectBtn: 0,
			arrayF: [],
			arrayM: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getFechasPago();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

//+-+--+-+-+-+-+-autorizar reinscrip
	async alertAutorizar(itm) {
		if (itm.length === 0) {
			Alert.alert('Sin alumnos seleccionados', 'Seleccione algunos alumnos para poder continuar', [
				{text: 'Enterado'}
			]);
		} else {
			Alert.alert('Autorizar reinscripción',
				'Está apunto de autorizar la reinscripión a los alumnos seleccionados\n¿Seguro que desea continar?', [
					{
						text: 'Sí',
						onPress: () => this.autorizaReinscrip(itm, this.state.elGrado, this.state.elGrupo, 'Autorizado')
					}, {text: 'No'}
				]);
		}

	}

	async autorizaReinscrip(itm, grado, grupo, autorizo) {
		let request = [];
		await this.spinner(true);
		for (let i = 0; i < itm.length; i++) {
			await fetch(this.state.uri +
				'/api/autorizar/reinscrip/' +
				grado + '/' + grupo + '/' + itm[i].id +
				'/' + autorizo, {
				method: 'GET', headers: {
					'Content-Type': 'application/json'
				}
			}).then(res => res.json())
				.then(responseJson => {
					request = responseJson;
				})
		}
		if (request.error !== undefined) {
			Alert.alert('Error en autorización',
				'Ha ocurrido un error ' + request.error.status_code +
				' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Inscripciones-Admin)', [
					{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
				]);
		} else {
			Alert.alert('Autorizado', 'Se ha autorizado la reinscripción con éxito y se ha notificado al tutor correspondiente', [
				{text: 'Enterado', onPress: () => [this.spinner(false), this.getAlumnosByGG(grado, grupo)]}
			])
		}
	}

	//+-+-+-+-+-+-+ desautorizar reinscrip
	async alertDesAuth(itm, grado, grupo) {
		Alert.alert('Eliminar de autorizados',
			'Está a punto de quitar este alumno de la lista de autorizados para reinscripción\n¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.desautorizar(itm, grado, grupo, ' ')}, {text: 'No'}
			])
	}

	async desautorizar(itm, grado, grupo, autorizo) {
		await this.spinner(true);
		await fetch(this.state.uri + '/api/autorizar/reinscrip/' + grado + '/' + grupo + '/' + itm.id + '/' + autorizo, {
			method: 'GET', headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en autorización',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Inscripciones-Admin)', [
							{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
						]);
				} else {
					Alert.alert('', 'Se ha eliminado al alumno de la lista de autorización con éxito', [
						{text: 'Enterado', onPress: () => [this.spinner(false), this.getAlumnosByGG(grado, grupo)]}
					])
				}
			});

	}

	//+-+-+-+-+-+-+-+--+
	async onListItemPressedGrado(indxBtn, itmBtn) {
		await this.setState({
			elGrado: itmBtn,
			indexGrado: indxBtn,
			checkBoxBtns: [],
			checkBoxBtnsIndex: []
		});
	}

	async onListItemPressedGrupos(indxBtn, itmBtn) {
		await this.setState({
			elGrupo: itmBtn,
			indexGrupo: indxBtn,
			checkBoxBtns: [],
			checkBoxBtnsIndex: []
		});
		await this.getAlumnosByGG(this.state.elGrado, this.state.elGrupo)
	}

	async getAlumnosByGG(grado, grupo) {
		await fetch(
			this.state.uri + '/api/get/alumnos/by/grado/grupo/' + grado + '/' + grupo,
			{
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Inscripciones-Admin)');
				} else {
					this.setState({alumnos: responseJson});
					let alumnosAuth = [];
					if (this.state.alumnos.length !== 0) {
						this.state.alumnos.forEach((it, i) => {
							this.state.checkBoxBtnsIndex[i] = 0;
							if (it.reinscripcion === 'Autorizado') {
								alumnosAuth[i] = it;
							}
						});
						if (this.state.selectBtn === 1) {
							if (alumnosAuth.length === 0) {
								Alert.alert('Sin alumnos',
									'No hay alumnos autorizados en este grupo aun aún', [
										{text: 'Enterado'}
									]);
							}
						}
					} else {
						Alert.alert('Sin alumnos',
							'No hay alumnos en este grupo seleccionado', [
								{text: 'Enterado'}
							])
					}
				}
			});
	}

//+-+-+-+-+-alumnos sin autorizar
	async selectAlumno(it, ix) {
		let i = this.state.checkBoxBtns.indexOf(it);
		if (i > -1) {
			this.state.checkBoxBtns.splice(i, 1);
			this.state.checkBoxBtnsIndex[ix] = 0;
		} else {
			this.state.checkBoxBtns.push(it);
			this.state.checkBoxBtnsIndex[ix] = 1;
		}
		await this.setState({aux: 0});
	}

	rndAlumnos() {
		let alumnos = [];
		if (this.state.alumnos.length !== 0) {
			this.state.alumnos.forEach((itm, ix) => {
				if (itm.reinscripcion !== 'Autorizado') {
					let a = this.state.alumnos.length - 1;
					let tabRow = [styles.rowTabla];
					if (ix !== a) {
						tabRow.push({borderColor: this.state.secondColor});
					}
					alumnos.push(
						<TouchableHighlight
							key={ix + 'altas'}
							underlayColor={'transparent'}
							style={[
								tabRow,
								{backgroundColor: this.state.checkBoxBtnsIndex[ix] === 1 ? this.state.secondColor : '#fff'}
							]}
							onPress={() => this.selectAlumno(itm, ix)}
						>
							<View style={styles.campoTablaG}>
								<Text
									style={{color: this.state.checkBoxBtnsIndex[ix] === 1 ? '#fff' : '#000'}}
								>
									{itm.name}
								</Text>
							</View>
						</TouchableHighlight>
					)
				}
			});
		}
		return alumnos;
	}

	//+-+-+-+-+ alumnos autorizados
	rndAlumnos2() {
		let alumnos = [];
		if (this.state.alumnos.length !== 0) {
			this.state.alumnos.forEach((itm, ix) => {
				if (itm.reinscripcion === 'Autorizado') {
					let a = this.state.alumnos.length - 1;
					let tabRow = [styles.rowTabla];
					if (ix !== a) {
						tabRow.push({borderColor: this.state.secondColor});
					}
					alumnos.push(
						<View
							key={ix + 'auth'}
							underlayColor={'transparent'}
							style={tabRow}
						>
							<View style={[styles.campoTablaG, styles.row]}>
								<Text>
									{itm.name}
								</Text>
								<View style={[styles.btn6_6]}>
									<Entypo name='circle-with-minus' size={22} color='red'
											onPress={() => this.alertDesAuth(itm, this.state.elGrado, this.state.elGrupo)}/>
								</View>
							</View>
						</View>
					)
				}
			});
		}
		return alumnos;
	}

	async selectedBtn(state) {
		await this.setState({
			selectBtn: state,
			checkBoxBtnsIndex: [],
			checkBoxBtns: [],
			alumnos: []
		});
		if (this.state.selectBtn === 2) {
			await this.setState({elGrado: '', elGrupo: ''});
		}
		if (this.state.selectBtn !== 2) {
			if (this.state.elGrado !== '') {
				if (this.state.elGrupo !== '') {
					await this.getAlumnosByGG(this.state.elGrado, this.state.elGrupo);
				}
			}
		}
	}

	rndReinscrip() {
		return (
			<View style={styles.container}>
				<View style={{height: responsiveHeight(21.5)}}>
					<GradoyGrupo
						onListItemPressedGrado={(indxBtn, itmBtn) => this.onListItemPressedGrado(indxBtn, itmBtn)}
						onListItemPressedGrupos={(indxBtn, itmBtn) => this.onListItemPressedGrupos(indxBtn, itmBtn)}
					/>
				</View>
				<Text style={[styles.main_title, {
					color: this.state.thirdColor,
					marginTop: Platform.OS === 'ios' ? 0 : 15
				}]}>
					Lista de alumnos
				</Text>
				{this.state.selectBtn === 0 ?
					<Text
						style={[styles.widthall, {
							fontSize: responsiveFontSize(1.6),
							textAlign: 'left',
							marginBottom: 5
						}]}>
						Seleccione los alumnos a los que desea autorizar la reinscripción.
					</Text>
					: null}
				<ScrollView
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{...ifIphoneX({marginBottom: 40}, {marginBottom: 20})}}
				>
					{this.state.alumnos.length !== 0 ?
						<View
							style={[
								styles.tabla,
								{borderColor: this.state.secondColor, marginTop: 5, padding: .5}
							]}
						>
							{this.state.selectBtn === 0 ? this.rndAlumnos() : this.rndAlumnos2()}
						</View> : <Text style={[styles.main_title, {
							color: '#a8a8a8',
							textAlign: 'center',
							marginTop: 40,
							marginBottom: 40,
							fontSize: responsiveFontSize(3)
						}]}>
							Sin seleccion aún
						</Text>}
					{this.state.selectBtn === 0 ?
						<TouchableOpacity
							style={[
								styles.bigButton,
								{backgroundColor: this.state.secondColor}]
							}
							onPress={() => this.alertAutorizar(this.state.checkBoxBtns)}
						>
							<Text style={[styles.textButton]}>
								Autorizar reinscripción
							</Text>
						</TouchableOpacity> : null}
				</ScrollView>
			</View>
		);
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ fecha límite
	async save(fecha, monto) {
		await this.spinner(true);
		const body = new FormData();
		body.append('new', JSON.stringify({
			option_name: 'Fecha límite',
			option_key: fecha
		}));
		await fetch(this.state.uri + '/api/save/fecha/reinscrip', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: body
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al guardar',
					'Ha ocurrido un error al tratar de guardar la fecha', [
						{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
					]);
			} else {
				//-+-+-+-+-+-+-+-+-+-+-+-+ save monto
				const body2 = new FormData();
				body2.append('new', JSON.stringify({
					option_name: 'Monto',
					option_key: monto
				}));
				fetch(this.state.uri + '/api/save/fecha/reinscrip', {
					method: 'POST', headers: {
						Accept: 'application/json',
						'Content-Type': 'multipart/form-data',
						Authorization: 'Bearer ' + this.state.token
					}, body: body2
				}).then(res => res.json()).then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al guardar',
							'Ha ocurrido un error al tratar de guardar la fecha', [
								{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
							]);
					} else {
						Alert.alert('Se guardo fecha límite de pago con éxito', '', [
							{text: 'Enterado', onPress: () => [this.spinner(false), this.getFechasPago()]}
						])
					}
				});
			}
		});
	}

	async getFechasPago() {
		await fetch(this.state.uri + '/api/get/fechas/reinscrip', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al guardar',
					'Ha ocurrido un error al tratar de guardar la fecha', [
						{text: 'Enterado'}
					]);
			} else {
				this.setState({arrayF: responseJson});
				this.getMontosPago(responseJson);
			}
		});
		await this.setState({aux: 0});
	}

	async getMontosPago(fechas_limite) {
		let fechas_pago = [];
		await fetch(this.state.uri + '/api/get/montos/reinscrip', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al guardar',
					'Ha ocurrido un error al tratar de guardar la fecha', [
						{text: 'Enterado'}
					]);
			} else {
				this.setState({arrayM: responseJson});
				if (this.state.arrayM.length !== 0) {
					if (fechas_limite.length !== 0) {
						for (let i = 0; i < fechas_limite.length; ++i) {
							fechas_pago.push({
								fecha_limite: fechas_limite[i].option_key,
								id_fecha: fechas_limite[i].id,
								monto: this.state.arrayM[i].option_key,
								id_monto: this.state.arrayM[i].id
							});
						}
					}
				}
				this.state.fechasPago = fechas_pago;
			}
		});
		await this.setState({aux: 0});
	}

	async agregar() {
		if (this.state.laFecha === '') {
			Alert.alert('Fecha indefinida',
				'Agregue una fecha para poder continuar', [
					{text: 'Enterado'}
				]);
		} else if (this.state.elMonto === '') {
			Alert.alert('Monto indefinido',
				'Agregue un monto para poder continuar', [
					{text: 'Enterado'}
				]);
		} else {
			await this.save(this.state.laFecha, this.state.elMonto);
		}
		await this.setState({aux: 0})
	}

	async alertBorrar(itm) {
		Alert.alert('Borrar fecha', 'Está a punto de borrar una fecha\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.borrar(itm)}, {text: 'No'}
		])
	}

	async borrar(itm) {
		await this.spinner(true);
		const body = new FormData();
		body.append('new', JSON.stringify({id: itm.id_fecha}));
		await fetch(this.state.uri + '/api/delete/fecha/reinscrip', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: body
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al guardar',
					'Ha ocurrido un error al tratar de borrar la fecha (fecha)', [
						{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
					]);
			} else {
				//-+-+-+-+- save monto
				const body2 = new FormData();
				body2.append('new', JSON.stringify({id: itm.id_monto}));
				fetch(this.state.uri + '/api/delete/fecha/reinscrip', {
					method: 'POST', headers: {
						Accept: 'application/json',
						'Content-Type': 'multipart/form-data',
						Authorization: 'Bearer ' + this.state.token
					}, body: body2
				}).then(res => res.json()).then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al borrar',
							'Ha ocurrido un error al tratar de borrar la fecha(monto)', [
								{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
							]);
					} else {
						Alert.alert('Se borrado la fecha límite de pago con éxito', '', [
							{text: 'Enterado', onPress: () => [this.spinner(false), this.getFechasPago()]}
						])
					}
				});
			}
		});
	}

	async update(itm, ix) {
		await this.spinner(true);
		const body = new FormData();
		body.append('update', JSON.stringify({
			id: itm.id_fecha,
			option_key: this.state.laFecha
		}));
		await fetch(this.state.uri + '/api/update/fecha/reinscrip', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: body
		}).then(res => res.json()).then(responseJson => {
			if (responseJson.error !== undefined) {
				Alert.alert('Error al editar',
					'Ha ocurrido un error al tratar de editar la fecha', [
						{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
					]);
			} else {
				//-+-+-+-+- save monto
				const body2 = new FormData();
				body2.append('update', JSON.stringify({
					id: itm.id_monto,
					option_key: this.state.elMonto
				}));
				fetch(this.state.uri + '/api/update/fecha/reinscrip', {
					method: 'POST', headers: {
						Accept: 'application/json',
						'Content-Type': 'multipart/form-data',
						Authorization: 'Bearer ' + this.state.token
					}, body: body2
				}).then(res => res.json()).then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al borrar',
							'Ha ocurrido un error al tratar de guardar la fecha', [
								{text: 'Enterado', onPress: () => [this.spinner(false), Actions.drawer()]}
							]);
					} else {
						Alert.alert('Se ha actualizado la fecha límite de pago con éxito', '', [
							{
								text: 'Enterado',
								onPress: () => [this.spinner(false), this.closeModal(ix), this.getFechasPago()]
							}
						])
					}
				});
			}
		});
	}

	async openModal(itm, ix) {
		this.state.elMonto = itm.monto;
		this.state.laFecha = itm.fecha_limite;
		this.state.isModalDate[ix] = true;
		await this.setState({aux: 0});
	}

	async closeModal(ix) {
		this.state.isModalDate[ix] = false;
		await this.setState({aux: 0});
	}

	rndFechas() {
		let fecha = [];
		this.state.fechasPago.forEach((it, ix) => {
			fecha.push(
				<View
					key={ix + 'fechas'}
					style={[
						styles.row,
						styles.cardHoras,
						styles.widthall,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 4,
							paddingVertical: 8
						}
					]}
				>
					<Modal
						isVisible={this.state.isModalDate[ix]}
						backdropOpacity={0.8}
						animationIn={'bounceIn'}
						animationOut={'bounceOut'}
						animationInTiming={1000}
						animationOutTiming={1000}
					>
						<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
							<Text style={[styles.main_title, styles.modalWidth, {
								color: this.state.thirdColor
							}]}>
								Editar fecha límite y monto
							</Text>
							<View style={[styles.modalWidth, styles.row]}>
								<TouchableOpacity
									style={[styles.inputPicker, styles.btn8,
										{borderColor: this.state.secondColor, marginVertical: 5}]}
									onPress={this._showDateTimePicker}
								>
									{this.state.laFecha === '' ? (
										<Text style={{fontSize: responsiveFontSize(1.5)}}>
											Seleccione fecha
										</Text>
									) : (
										<Text style={{fontSize: responsiveFontSize(1.5)}}>
											{moment(this.state.laFecha, 'YYYY-MM-DD').format('DD/MM/YYYY')}
										</Text>
									)}
								</TouchableOpacity>
								<DateTimePicker
									locale={'es'}
									isVisible={this.state.fechaPicker}
									onConfirm={this._handleDatePicked}
									onCancel={this._hideDateTimePicker}
									titleIOS={'Seleccione una fecha'}
									confirmTextIOS={'Seleccionar'}
									cancelTextIOS={'Cancelar'}
								/>
								<TextInputMask
									type={'money'}
									options={{
										unit: '$',
										separator: '.',
										delimiter: ','
									}}
									placeholder='Capture el monto'
									style={[styles.inputPicker, styles.btn8,
										{
											fontSize: responsiveFontSize(1.5),
											borderColor: this.state.secondColor,
											paddingVertical: Platform.OS === 'ios' ? 9.5 : 5.5,
											textAlign: 'center'
										}]}
									onChangeText={text => this.ponerImport(text)}
									underlineColorAndroid={'transparent'}
									value={this.state.elMonto}
									keyboardType={'numeric'}
									maxLength={10}
								/>
							</View>
							<View style={[styles.modalWidth, styles.row, {marginVertical: 10}]}>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: '#fff', borderColor: this.state.secondColor}
									]}
									onPress={() => this.closeModal(ix)}
								>
									<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{
											backgroundColor: this.state.secondColor,
											borderColor: this.state.secondColor
										}
									]}
									onPress={() => this.update(this.state.fechasPago[ix], ix)}
								>
									<Text style={[styles.textW, {color: '#fff'}]}>Aceptar</Text>
								</TouchableOpacity>
							</View>
						</View>
					</Modal>
					<View
						style={[styles.btn3, {alignItems: 'center'}]}
					>
						<Text>Fecha límite {ix + 1}:</Text>
						<View style={styles.btn3T}>
							<Text style={{textAlign: 'center', fontWeight: '800'}}>
								{moment(it.fecha_limite).format('DD/MM/YYYY')}
							</Text>
						</View>
					</View>
					<View style={{alignItems: 'center'}}>
						<Text>Monto:</Text>
						<View style={styles.btn6}>
							<Text style={{fontWeight: '800'}}>
								${Number(it.monto).toLocaleString('en-US')}
							</Text>
						</View>
					</View>
					<View
						style={[styles.btn6, styles.row, {marginRight: 7}]}
					>
						<MaterialIcons name='edit' size={22} color='grey'
									   onPress={() => this.openModal(this.state.fechasPago[ix], ix)}/>
						<Entypo name='circle-with-minus' size={22} color='red'
								onPress={() => this.alertBorrar(it)}/>
					</View>
				</View>
			);
		});
		return fecha;
	}

	async ponerImport(value) {
		let patron = /,/gi;
		let nuevoPatron = '';
		let importe1 = value.toString().replace(patron, nuevoPatron);
		let nuevoPatron1 = '';
		let importe2 = importe1.toString().replace('$', nuevoPatron1);
		await this.setState({elMonto: importe2});
	}

	rndFechaPago() {
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Capture la fechas límite de pago
				</Text>
				{this.state.fechasPago.length === 3 ? null :
					<View style={[styles.row, styles.widthall, {marginVertical: 15}]}>
						<TouchableOpacity
							style={[styles.inputPicker, styles.btn2, {
								borderColor: this.state.secondColor
							}]}
							onPress={this._showDateTimePicker}
						>
							{this.state.laFecha === '' ? (
								<Text style={{fontSize: responsiveFontSize(1.8)}}>
									Seleccione fecha
								</Text>
							) : (
								<Text style={{fontSize: responsiveFontSize(1.8)}}>
									{moment(this.state.laFecha, 'YYYY-MM-DD').format('DD/MM/YYYY')}
								</Text>
							)}
						</TouchableOpacity>
						<TextInputMask
							type={'money'}
							options={{
								unit: '$',
								separator: '.',
								delimiter: ','
							}}
							placeholder='Capture el monto'
							style={[styles.inputPicker, styles.btn2,
								{
									borderColor: this.state.secondColor,
									paddingVertical: Platform.OS === 'ios' ? 9.5 : 6.7,
									textAlign: 'center'
								}]}
							onChangeText={text => this.ponerImport(text)}
							underlineColorAndroid={'transparent'}
							value={this.state.elMonto}
							keyboardType={'numeric'}
							maxLength={10}
						/>
					</View>}
				<DateTimePicker
					locale={'es'}
					isVisible={this.state.fechaPicker}
					onConfirm={this._handleDatePicked}
					onCancel={this._hideDateTimePicker}
					titleIOS={'Seleccione una fecha'}
					confirmTextIOS={'Seleccionar'}
					cancelTextIOS={'Cancelar'}
				/>
				{this.state.fechasPago.length === 3 ? null :
					<Entypo
						name='circle-with-plus'
						color='green' size={35}
						onPress={() => [this.agregar(), Keyboard.dismiss()]}
						style={{marginVertical: 10}}
					/>}
				<ScrollView
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{marginBottom: 20}}
				>
					{this.rndFechas()}
				</ScrollView>
			</View>
		);
	}

	render() {
		let fourth = this.state.fourthColor;
		let third = this.state.thirdColor;
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione una acción
				</Text>
				<View
					style={[styles.row, styles.widthall, {marginTop: 10}]}
				>
					<TouchableHighlight
						underlayColor={'transparent'}
						style={[styles.exaCalif,
							{backgroundColor: this.state.selectBtn === 0 ? third : fourth}]}
						onPress={() => this.selectedBtn(0)}
					>
						<Text style={{
							fontSize: responsiveFontSize(1.7),
							fontWeight: '700',
							color: this.state.selectBtn === 0 ? '#fff' : '#000'
						}}
						>
							Por autorizar
						</Text>
					</TouchableHighlight>
					<TouchableHighlight
						underlayColor={'transparent'}
						style={[styles.exaCalif,
							{backgroundColor: this.state.selectBtn === 1 ? third : fourth}]}
						onPress={() => this.selectedBtn(1)}
					>
						<Text style={{
							fontSize: responsiveFontSize(1.7),
							fontWeight: '700',
							color: this.state.selectBtn === 1 ? '#fff' : '#000'
						}}
						>
							Autorizado
						</Text>
					</TouchableHighlight>
					<TouchableHighlight
						underlayColor={'transparent'}
						style={[styles.exaCalif,
							{backgroundColor: this.state.selectBtn === 2 ? third : fourth}]}
						onPress={() => this.selectedBtn(2)}>
						<Text style={{
							fontSize: responsiveFontSize(1.7),
							fontWeight: '700',
							color: this.state.selectBtn === 2 ? '#fff' : '#000'
						}}
						>
							Fecha pago
						</Text>
					</TouchableHighlight>
				</View>
				{this.state.selectBtn === 2 ? this.rndFechaPago() : this.rndReinscrip()}
			</View>
		);
	}
}
