import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TouchableHighlight, Alert, Image
} from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Spinner from 'react-native-loading-spinner-overlay';
const moment = require('moment');

export default class ConsultarServicio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            status: [],
            servicio: false
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getStatus();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getStatus(){
        this.setState({servicio: true})
        let nombreCorto = '';
        let nombreCorto1 = '';
        if(this.state.uri === 'https://controlescolar.pro'){
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.pro', '');
        }else{
            nombreCorto = this.state.uri.replace('https://', '');
            nombreCorto1 = nombreCorto.replace('.controlescolar.pro', '');
        }
        await fetch('http://127.0.0.1:8080/api/get/status/'+nombreCorto1 , {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    this.setState({servicio: false})
                    console.log(responseJson)
                } else {
                    console.log(responseJson)
                    this.setState({servicio: false})
                    this.setState({status: responseJson})
                 }
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.servicio} textContent='Consultado estatus del servicio...'/>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                    <View style={{width: responsiveWidth(90), height: responsiveHeight(60)}}>
                        {this.state.status.status_plan === 'Activo' ? (
                                <View style={{alignItems: 'center'}}>
                                    <Text style={{color: '#4CAE51', fontSize: responsiveFontSize(3), marginTop: 7, fontWeight: '800'}}>ACTIVO</Text>
                                <Image
                                    style={{width: 200, height: 200, marginTop: 0}}
                                    source={{
                                        uri: 'https://img.icons8.com/color/384/000000/ok.png'
                                    }}
                                />
                                    <View style={{width: responsiveWidth(90), height: responsiveHeight(8), alignItems: 'center'}}>
                                    <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), margin: 2}}>{this.state.status.escuela_mexico.nombre_centro_trabajo}</Text>
                                    <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2), margin: 2}}>{this.state.status.escuela_mexico.nivel_educativo}</Text>
                                    </View>
                                    <View style={{width: responsiveWidth(75), height: responsiveHeight(5), alignItems: 'center', marginTop: 0}}>
                                        {this.state.status.plan_id === '1' ?
                                            (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Esencial</Text>) :
                                            this.state.status.plan_id === '2' ?
                                                (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Plan Avanzado</Text>) :
                                                this.state.status.plan_id === '3' ?
                                                    (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Plan Profesional</Text>) : null}
                                    </View>
                                    <View style={[styles.row ,{width: responsiveWidth(90), height: responsiveHeight(5)}]}>
                                        <View style={{width: responsiveWidth(45), height: responsiveHeight(5), alignItems: 'center'}}>
                                            <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2)}}>Fecha inicio</Text>
                                            <Text style={{marginTop: 0}}>{moment(this.state.status.fecha_inicio).format('DD/MM/YY')}</Text>
                                        </View>
                                        <View style={{width: responsiveWidth(45), height: responsiveHeight(5),alignItems: 'center'}}>
                                            <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2)}}>Fecha final</Text>
                                            <Text>{moment(this.state.status.fecha_fin).format('DD/MM/YY')}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.row,{width: responsiveWidth(75), height: responsiveHeight(5), alignItems: 'center'}]}>
                                        <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3)}}>El plan terminará {moment(this.state.status.fecha_fin).endOf('day').fromNow()}</Text>
                                    </View>

                                    <TouchableOpacity
                                        style={[
                                            styles.bigButton,
                                            {
                                                backgroundColor: this.state.secondColor,
                                                borderRadius: 8,
                                                width: responsiveWidth(90),
                                                marginTop: 120,
                                                shadowColor: 'lightgrey',
                                                shadowOpacity: 1,
                                                shadowOffset: {width: 5, height: 5}
                                            }
                                        ]}
                                    >
                                        <Text style={styles.textButton}>Subir de nivel</Text>
                                    </TouchableOpacity>
                            </View>): this.state.status.status_plan === 'Vencido' ?
                            ( <View style={{alignItems: 'center'}}>
                                    <Text style={{color: '#FFC008', fontSize: responsiveFontSize(3), marginTop: 7, fontWeight: '800'}}>VENCIDO</Text>
                                    <Image
                                        style={{width: 200, height: 200, marginTop: 0}}
                                        source={{
                                            uri: 'https://img.icons8.com/color/348/000000/error.png'
                                        }}
                                    />
                                    <View style={{width: responsiveWidth(75), height: responsiveHeight(5), alignItems: 'center', marginTop: 0}}>
                                        {this.state.status.plan_id === '1' ?
                                            (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Esencial</Text>) :
                                            this.state.status.plan_id === '2' ?
                                                (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Plan Avanzado</Text>) :
                                                this.state.status.plan_id === '3' ?
                                                    (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Plan Profesional</Text>) : null}
                                    </View>
                                    <View style={{width: responsiveWidth(90), height: responsiveHeight(8), alignItems: 'center'}}>
                                        <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), margin: 2}}>{this.state.status.escuela_mexico.nombre_centro_trabajo}</Text>
                                        <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2), margin: 2}}>{this.state.status.escuela_mexico.nivel_educativo}</Text>
                                    </View>
                                    <View style={[styles.row ,{width: responsiveWidth(90), height: responsiveHeight(5)}]}>
                                        <View style={{width: responsiveWidth(45), height: responsiveHeight(5), alignItems: 'center'}}>
                                            <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2)}}>Fecha inicio</Text>
                                            <Text style={{marginTop: 0}}>{moment(this.state.status.fecha_inicio).format('DD/MM/YY')}</Text>
                                        </View>
                                        <View style={{width: responsiveWidth(45), height: responsiveHeight(5),alignItems: 'center'}}>
                                            <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2)}}>Fecha final</Text>
                                            <Text>{moment(this.state.status.fecha_fin).format('DD/MM/YY')}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.row,{width: responsiveWidth(75), height: responsiveHeight(5), alignItems: 'center'}]}>
                                        <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3)}}>El plan venció {moment(this.state.status.fecha_fin).endOf('day').fromNow()}</Text>
                                    </View>

                                    <TouchableOpacity
                                        style={[
                                            styles.bigButton,
                                            {
                                                backgroundColor: this.state.secondColor,
                                                borderRadius: 8,
                                                width: responsiveWidth(90),
                                                marginTop: 120,
                                                shadowColor: 'lightgrey',
                                                shadowOpacity: 1,
                                                shadowOffset: {width: 5, height: 5}
                                            }
                                        ]}
                                    >
                                        <Text style={styles.textButton}>Subir de nivel</Text>
                                    </TouchableOpacity>
                                </View>
                            ):  this.state.status.status_plan === 'Inactivo' ? ( <View style={{alignItems: 'center'}}>
                                <Text style={{color: '#F44236', fontSize: responsiveFontSize(3), marginTop: 7, fontWeight: '800'}}>INACTIVO</Text>
                                <Image
                                    style={{width: 200, height: 200, marginTop: 0}}
                                    source={{
                                        uri: 'https://img.icons8.com/color/480/000000/spam.png'
                                    }}
                                />
                                <View style={{width: responsiveWidth(90), height: responsiveHeight(8), alignItems: 'center'}}>
                                    <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), margin: 2}}>{this.state.status.escuela_mexico.nombre_centro_trabajo}</Text>
                                    <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2), margin: 2}}>{this.state.status.escuela_mexico.nivel_educativo}</Text>
                                </View>
                                <View style={{width: responsiveWidth(75), height: responsiveHeight(5), alignItems: 'center', marginTop: 0}}>
                                    {this.state.status.plan_id === '1' ?
                                        (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Esencial</Text>) :
                                        this.state.status.plan_id === '2' ?
                                            (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Avanzado</Text>) :
                                            this.state.status.plan_id === '3' ?
                                                (<Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3), fontWeight: '800'}}>Plan Profesional</Text>) : null}
                                </View>
                                <View style={[styles.row ,{width: responsiveWidth(90), height: responsiveHeight(5)}]}>
                                    <View style={{width: responsiveWidth(45), height: responsiveHeight(5), alignItems: 'center'}}>
                                        <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2)}}>Fecha inicio</Text>
                                        <Text style={{marginTop: 0}}>{moment(this.state.status.fecha_inicio).format('DD/MM/YY')}</Text>
                                    </View>
                                    <View style={{width: responsiveWidth(45), height: responsiveHeight(5),alignItems: 'center'}}>
                                        <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(2)}}>Fecha final</Text>
                                        <Text>{moment(this.state.status.fecha_fin).format('DD/MM/YY')}</Text>
                                    </View>
                                </View>
                                <View style={[styles.row,{width: responsiveWidth(75), height: responsiveHeight(5), alignItems: 'center'}]}>
                                    <Text style={{color: this.state.thirdColor, fontSize: responsiveFontSize(3)}}>El plan terminó {moment(this.state.status.fecha_fin).startOf('day').fromNow()}</Text>
                                </View>
                                <Text style={{textAlign: 'justify'}}>En este momento, ningún usuario puede acceder a su servicio. Alumnos, maestros, padres de familia y personal escolar tendrán sus funciones deshabilitadas hasta que se realice el pago.</Text>
                                <TouchableOpacity
                                    style={[
                                        styles.bigButton,
                                        {
                                            backgroundColor: 'red',
                                            borderRadius: 8,
                                            width: responsiveWidth(90),
                                            marginTop: 70,
                                            shadowColor: 'lightgrey',
                                            shadowOpacity: 1,
                                            shadowOffset: {width: 5, height: 5}
                                        }
                                    ]}>
                                    <Text style={styles.textButton}>Renovar plan</Text>
                                </TouchableOpacity>
                            </View>):null}
                    </View>
            </View>
        );
    }
}
