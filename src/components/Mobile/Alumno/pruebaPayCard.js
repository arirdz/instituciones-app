import React from 'react';
import {Alert, AsyncStorage, FlatList, KeyboardAvoidingView, Text, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {responsiveHeight, responsiveWidth, responsiveFontSize} from 'react-native-responsive-dimensions';
import {CreditCardInput} from 'rn-credit-card-view';
import styles from '../../styles';
import openpay from 'react-native-openpay';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Spinner from 'react-native-loading-spinner-overlay';

export default class pruebaPayCard extends React.Component {
	_onChange = formData => {
		let card = formData.values.number;
		let cardJunto = card.split(' ');
		let expire = formData.values.expiry;
		let expiraSeparado = expire.split('/');
		this.setState({
			cardNumber: cardJunto[0] + cardJunto[1] + cardJunto[2] + cardJunto[3],
			holderName: formData.values.name,
			cvv2: formData.values.cvc,
			month: expiraSeparado[0],
			year: expiraSeparado[1]
		});
	};
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			Numero: [],
			values: [],
			datos: [],
			cardData: [],
			source_id: '',
			singleCard: [],
			cardToken: '',
			session_id: '',
			visible: false,
			recibo: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async createCardTokenGG() {
		openpay.setup('mpb6xcs65mhmahdjv935', 'pk_1c63493c1ce241319bfe713ece85fa26');
		openpay.createCardToken({
			holder_name: this.state.holderName,
			card_number: this.state.cardNumber,
			expiration_month: this.state.month,
			expiration_year: this.state.year,
			cvv2: this.state.cvv2
		}).then(token => this.setState({cardToken: token}));
		openpay.getDeviceSessionId().then(sessionId => this.setState({session_id: sessionId}));
		await this.pagoCardToken();
	}

	async onChange(option, data) {
		await this.setState({
			textInputValue: option.label, brand: option.brand, source_id: option.id, bank_name: option.bank_name
		});
		await this.getCardByUser();
	}

	async pagoCardToken(tutor) {
		let formData = new FormData();
		formData.append('parameters', JSON.stringify({
			method: 'card',
			source_id: this.state.cardToken,
			amount: 10,
			description: 'Pago de preinscripcion liceo',
			device_session_id: this.state.session_id,
			customer: {
				name: tutor.nombre,
				last_name: tutor.apellidos,
				phone_number: tutor.telefono_contacto,
				email: tutor.email
			}
		}));
		await fetch(this.state.uri + '/api/pago/tarjeta/openpay/preinscrip', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(response => response.json())
			.then(responseJson => {
				if (responseJson.response !== 'error') {
					Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
						text: 'Gracias'
					}]);
					this.setState({recibo: responseJson});
				} else {
					if (responseJson.error.code === 3001) {
						Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3002) {
						Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirada', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3003) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3004) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3005) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
							text: 'Entendido'
						}]);

					} else if (responseJson.error.code === 1006) {
						Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
							text: 'Entendido'
						}]);

					} else {
						Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
							text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
						}]);
					}
				}
			}).catch(error => {
				Alert.alert('¡Ups!', 'No se pudo generar el cargo, intente más tarde', [{
					text: 'Gracias'
				}]);
			});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected, indexSelected: indexSelected
		});
	}

	registrarTarjetas() {
		return (<KeyboardAvoidingView style={styles.container}>
			<View
				style={{
					width: responsiveWidth(95), height: responsiveHeight(13), paddingTop: 17
				}}>
				<CreditCardInput
					autoFocus
					requiresName
					requiresCVC
					allowScroll={true}
					labels={{
						number: 'Número de Tarjeta', expiry: 'Expira', cvc: 'CVC/CCV', name: 'Nombre'
					}}
					placeholders={{
						number: '8899 6677 4433 1122', expiry: 'MM/YY', cvc: 'CVC', name: 'Nombre Completo'
					}}
					validColor={'green'}
					invalidColor={'red'}
					placeholderColor={'darkgray'}
					onChange={this._onChange}
				/>
			</View>
		</KeyboardAvoidingView>);
	}

	render() {
		return (<View style={styles.container}>
			<Spinner visible={this.state.visible} textContent='Verificando...'/>

			{this.registrarTarjetas()}
			<TouchableOpacity
				style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
				onPress={() => this.createCardTokenGG(this.props.elAspirante.tutor)}
			>
				<Text style={styles.textButton}>Pagar ahora</Text>
			</TouchableOpacity>
		</View>);
	}
}
