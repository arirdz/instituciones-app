import React from "react";
import {
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import styles from "../../styles";
import Modal from "react-native-modal";
import Periodos from "../Globales/Periodos";

export default class verReportesAlumn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ciclos: [],
            detalles: [],
            elCiclo: "",
            elDetalle: "",
            elHijo: "",
            elPeriodo: "",
            grado: "",
            grupo: "",
            incidenciasCount: [],
            itemBtns: ["FALTAS", "INCIDENCIAS", "FECHA"],
            ninosPicker: [],
            opcion: 0,
            periodos: [],
            selectedIndex: -1,
            selectedIndexBtn: 0,
            selectedIndexCiclo: 0,
            selectedIndexPeriodo: 0
        };
        this.renderList = this.renderList.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getCiclos();
    }

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo
        });
    }

    async getCiclos() {
        let ciclopicker = await fetch(this.state.uri + "/api/ciclos", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let ciclospicker = await ciclopicker.json();
        this.setState({ciclos: ciclospicker});
        this.setState({elCiclo: this.state.ciclos[0]});
        await this.getPeriodos();
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++INCIDENCIAS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getIncidencias() {
        let incidenciaList = await fetch(
            this.state.uri +
            "/api/incidencias/count/" +
            this.state.elHijo +
            "/" +
            this.state.elCiclo +
            "/" +
            this.state.elPeriodo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let incidenciasList = await incidenciaList.json();
        this.setState({incidenciasCount: incidenciasList});
    }

    renderIncidencias() {
        let academicas = this.state.incidenciasCount.IncidenciasAC;
        let conductuales = this.state.incidenciasCount.IncidenciasCC;
        return (
            <View style={styles.row}>
                <View style={styles.cuadro}>
                    <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                        INCIDENCIAS{"\n"}ACADÉMICAS
                    </Text>
                    <View
                        style={[
                            styles.buttonEnterados,
                            {backgroundColor: this.state.fourthColor},
                            styles.cuadro1
                        ]}
                    >
                        <Text style={styles.text_VE}>{academicas}</Text>
                    </View>
                    {this.state.incidenciasCount.IncidenciasAC === 0 ? (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles1()}
                        >
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles("Académicas")}
                        >
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
                <View style={styles.cuadro}>
                    <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                        INCIDENCIAS{"\n"}CONDUCTUALES
                    </Text>
                    <View
                        style={[
                            styles.buttonEnterados,
                            {backgroundColor: this.state.mainColor},
                            styles.cuadro1
                        ]}
                    >
                        <Text style={styles.text_VE}>{conductuales}</Text>
                    </View>
                    {this.state.incidenciasCount.IncidenciasCC === 0 ? (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles1()}
                        >
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles("Conductual")}
                        >
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++MOSTRAR DETALLES++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getDetalles(tipo) {
        let detalleList = await fetch(
            this.state.uri +
            "/api/detalles/incidencias/" +
            this.state.elHijo +
            "/" +
            this.state.elCiclo +
            "/" +
            this.state.elPeriodo +
            "/" +
            tipo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let detallesList = await detalleList.json();
        await this.setState({detalles: detallesList, isModalVisible: true});
    }

    async getDetalles1() {
        let detalleList = await fetch(this.state.uri + "/api/detalles/vacio", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let detallesList = await detalleList.json();
        await this.setState({detalles: detallesList, isModalVisible: true});
    }

    renderDetalle(itemDetalle) {
        return (
            <View
                style={[
                    styles.noticia,
                    styles.modalWidth,
                    {borderColor: "#bdbdbd", borderWidth: 1}
                ]}
            >
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Materia:{" "}
                        <Text style={{color: "#000"}}>{itemDetalle.materia.nombre}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Faltas:{" "}
                        <Text style={{color: "#000"}}>{itemDetalle.tipos_faltas}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Incidencias:{" "}
                        <Text style={{color: "#000"}}>{itemDetalle.extras}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={[styles.modalText, {color: this.state.thirdColor}]}>
                        Fecha:{" "}
                        <Text style={{color: "#000"}}>{itemDetalle.created_at}</Text>
                    </Text>
                </View>
            </View>
        );
    }

    renderDetalles() {
        let buttonsDetalles = [];
        this.state.detalles.forEach((itemDetalle, indexDetalle) => {
            buttonsDetalles.push(this.renderDetalle(itemDetalle, indexDetalle));
        });
        return buttonsDetalles;
    }

    render() {
        const detalles = this.renderDetalles();
        const incidencias = this.renderIncidencias();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Informe de incidencias
                    </Text>
                    {incidencias}
                    <Modal
                        isVisible={this.state.isModalVisible}
                        backdropOpacity={0.5}
                        animationIn={"bounceIn"}
                        animationOut={"bounceOut"}
                        animationInTiming={1000}
                        animationOutTiming={1000}
                    >
                        <View style={[styles.container, {borderRadius: 6}]}>
                            <ScrollView>
                                <Text
                                    style={[
                                        styles.main_title,
                                        styles.modalWidth,
                                        {color: this.state.thirdColor}
                                    ]}
                                >
                                    Incidencias
                                </Text>
                                {detalles}
                            </ScrollView>
                            <TouchableOpacity
                                style={[
                                    styles.bigButton,
                                    styles.modalWidth,
                                    {
                                        backgroundColor: this.state.mainColor,
                                        borderColor: this.state.mainColor
                                    }
                                ]}
                                onPress={() => this.setState({isModalVisible: false})}
                            >
                                <Text style={styles.textButton}>Aceptar</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                </ScrollView>
            </View>
        );
    }
}
