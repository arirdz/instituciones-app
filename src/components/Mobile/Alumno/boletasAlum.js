import React from "react";
import {
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import styles from "../../styles";
import {Actions} from "react-native-router-flux";
import Periodos from "../Globales/Periodos";

export default class boletasAlum extends React.Component {
    _showModal = () => this.setState({isModalVisible: true});

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            items: {},
            materias: [],
            alumnos: [],
            selectedIndexPeriodo: -1,
            elPeriodo: ""
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo
        });
    }

    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    revCalif() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}
                    onPress={() => Actions.pruebaBoleta()}
                >
                    <Text style={styles.textButton}>Enviar recordatorio</Text>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        const calif = this.revCalif();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {calif}
            </View>
        );
    }
}
