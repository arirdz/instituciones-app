import React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';

let moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class reciboTienda extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			reciboTienda: []
		};
	}

	render() {
		return (<View style={styles.container}>
			<ScrollView showsVerticalScrollIndicator={true}>

				<View style={[styles.rowPago, {backgroundColor: '#F8A919', borderColor: '#F8A919'}]}>
					<Image
						style={{
							width: responsiveWidth(35),
							height: responsiveHeight(5),
							marginVertical: 10,
							position: 'absolute',
							top: 0,
							right: 4
						}}
						source={require('../../../images/paynet.png')}
					/>
					<Text style={styles.tituloPago}>
						Total a pagar
					</Text>
					<Text style={styles.infoPago}>
						${' ' + this.props.reciboTienda.amount}
						<Text style={styles.infoPagoP}>{' MXN'}</Text>
					</Text>
					<Text style={styles.infoComision}>(+ 8 pesos de comisión)</Text>
				</View>
				{/*<View style={[styles.row_rT,{backgroundColor: "#d3d3d3"}]}>*/}
				{/*<Image*/}
				{/*style={{*/}
				{/*width: responsiveWidth(90),*/}
				{/*height: 75,*/}
				{/*}}*/}
				{/*source={{*/}
				{/*uri: this.props.reciboTienda.payment_method.barcode_url.toString()*/}
				{/*}}/>*/}
				{/*</View>*/}
				<View style={[styles.row_rT, {marginTop: 20}]}>
					<View style={styles.row}>
						<View style={{width: responsiveWidth(10)}}>
							<View style={{backgroundColor: '#F8A919', width: 30, height: 40}}/>
						</View>
						<View style={{width: responsiveWidth(84), height: 35, justifyContent: 'flex-end'}}>
							<Text style={[styles.info, {fontWeight: '500'}]}>Fecha límite de pago</Text>
						</View>
					</View>

					<View style={[styles.row, {marginTop: 10}]}>
						<View style={{width: responsiveWidth(10)}}>
						</View>
						<View style={{width: responsiveWidth(84)}}>
							<Text style={[styles.info, {
								fontSize: responsiveFontSize(1.6), fontWeight: '500'
							}]}>{

								moment(this.props.reciboTienda.operation_date).add(20, 'days').format('LLL') + '\n'}</Text>
							<View style={styles.row}>
								<View style={{
									backgroundColor: '#F1F1F1',
									borderRadius: 6,
									paddingVertical: 5,
									paddingHorizontal: 10,
									borderWidth: 1,
									borderColor: '#E8E8E8'
								}}>
									<Text style={[styles.info, {fontWeight: '500'}]}>
										{this.props.reciboTienda.payment_method.reference.substr(0, 4)}</Text>
								</View>
								<Text>-</Text>
								<View style={{
									backgroundColor: '#F1F1F1',
									borderRadius: 6,
									paddingVertical: 5,
									paddingHorizontal: 10,
									borderWidth: 1,
									borderColor: '#E8E8E8'
								}}>
									<Text style={[styles.info, {fontWeight: '500'}]}>
										{this.props.reciboTienda.payment_method.reference.substr(4, 4)}</Text>
								</View>
								<Text>-</Text>
								<View style={{
									backgroundColor: '#F1F1F1',
									borderRadius: 6,
									paddingVertical: 5,
									paddingHorizontal: 10,
									borderWidth: 1,
									borderColor: '#E8E8E8'
								}}>
									<Text style={[styles.info, {fontWeight: '500'}]}>
										{this.props.reciboTienda.payment_method.reference.substr(8, 4)}</Text>
								</View>
								<Text>-</Text>
								<View style={{
									backgroundColor: '#F1F1F1',
									borderRadius: 6,
									paddingVertical: 5,
									paddingHorizontal: 10,
									borderWidth: 1,
									borderColor: '#E8E8E8'
								}}>
									<Text style={[styles.info, {fontWeight: '500'}]}>
										{this.props.reciboTienda.payment_method.reference.substr(12, 10)}</Text>
								</View>
							</View>
							<Text style={{textAlign: 'center', color: 'red'}}>Referencia de pago</Text>
						</View>
					</View>
				</View>

				<View style={[styles.row_rT, {marginTop: 30}]}>
					<View style={styles.row}>
						<View style={{width: responsiveWidth(10)}}>
							<View style={{backgroundColor: '#F8A919', width: 30, height: 40}}/>
						</View>
						<View style={{width: responsiveWidth(84), height: 35, justifyContent: 'flex-end'}}>
							<Text style={[styles.info, {fontWeight: '500'}]}>Detalles de la compra</Text>
						</View>
					</View>

					<View style={[styles.row, {marginTop: 10}]}>
						<View style={{
							width: responsiveWidth(38), padding: 10, paddingLeft: 15, backgroundColor: '#F1F1F1'
						}}><Text>Descripción</Text></View>
						<View style={{
							width: responsiveWidth(56), padding: 10, paddingLeft: 15, backgroundColor: '#F1F1F1'
						}}><Text>{this.props.reciboTienda.description}</Text></View>
					</View>
					<View style={[styles.row, {marginTop: 0}]}>
						<View style={{
							width: responsiveWidth(38), padding: 10, paddingLeft: 15, backgroundColor: '#E8E8E8'
						}}><Text>Fecha y hora</Text></View>
						<View style={{
							width: responsiveWidth(56), padding: 10, paddingLeft: 15, backgroundColor: '#E8E8E8'
						}}>
							<Text>{moment(this.props.reciboTienda.due_date).format('lll')}</Text>
						</View>
					</View>
				</View>

				<View style={[styles.row_rT, {marginTop: 30}]}>
					<View style={styles.row}>
						<View style={{width: responsiveWidth(10)}}>
							<View style={{backgroundColor: '#F8A919', width: 30, height: 40}}/>
						</View>
						<View style={{width: responsiveWidth(84), height: 35, justifyContent: 'flex-end'}}>
							<Text style={[styles.info, {fontWeight: '500'}]}>Como realizar el pago</Text>
						</View>
					</View>

					<View style={[styles.row, {marginTop: 10}]}>
						<View style={{width: responsiveWidth(10)}}>
						</View>
						<View style={{width: responsiveWidth(84)}}>
							<View style={styles.row}>
								<Text style={styles.description}>
								1. Acude a cualquier tienda afiliada
							</Text>
							</View>
							<View style={[styles.row, {marginTop: 10}]}>
								<Text
									style={[styles.description, {width: responsiveWidth(80)}]}
								>
									2. Entrega al cajero el número de referencia y menciona que realizarásun pago de
									servicio Paynet
								</Text>
							</View>
							<View style={[styles.row, {marginTop: 10}]}>
								<Text
									style={[styles.description, {width: responsiveWidth(80)}]}
								>
									3.Realizar el pago en efectivo por ${' ' + this.props.reciboTienda.amount + ' MXN '}
									(+8 pesos de comisión)
								</Text>
							</View>
							<View style={[styles.row, {marginTop: 10}]}>
								<Text
									style={[styles.description, {width: responsiveWidth(80)}]}>
									4. Conserva el ticket para cualquier aclaración
								</Text>
							</View>
						</View>
					</View>
				</View>

				<View style={styles.row_rT}>
					<Image
						style={{
							width: responsiveWidth(95), height: responsiveHeight(10), marginVertical: 10
						}}
						source={{
							uri: 'https://s3.amazonaws.com/images.openpay/Horizontal_1.gif'
						}}/>
				</View>
			</ScrollView>
		</View>);
	}
}
