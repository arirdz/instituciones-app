import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	Image,
	TouchableHighlight
} from 'react-native';
import {
	responsiveHeight,
	responsiveWidth,
	responsiveFontSize
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {Actions} from 'react-native-router-flux';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class consultaNotif extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: '',
			today: moment().add(20, 'days').format('LL')
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.setState({data: 'esau@icodestudio.pro'})
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async pagoTienda(data) {
		let formData = new FormData();
		formData.append('parameters', JSON.stringify({
			method: 'store',
			amount: 500,
			description: 'Pago de preinscripcion liceo',
			customer: {
				name: 'Juanchito',
				last_name: 'Perengano',
				phone_number: '2282111111',
				email: 'esau@icodestudio.pro'
			}
		}));
		await fetch(this.state.uri + '/api/pago/tienda/openpay/preinscrip', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(response => response.json())
			.then(responseJson => {
				if (responseJson.response !== 'error') {
					Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
						text: 'Gracias',
						onPress: () => Actions.reciboTienda({reciboTienda: responseJson.result, user: data})
					}]);
					this.setState({recibo: responseJson});
				} else {
					if (responseJson.error.code === 3001) {
						Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3002) {
						Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirado', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3003) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3004) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3005) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
							text: 'Entendido'
						}]);

					} else if (responseJson.error.code === 1006) {
						Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
							text: 'Entendido'
						}]);

					} else {
						Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
							text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
						}]);
					}
				}
			}).catch(error => {
				Alert.alert('¡Ups!', 'No se pudo generar el cargo, intente más tarde', [{
					text: 'Gracias'
				}]);
			});
	}

	async transferencia(data) {
		let formData = new FormData();
		formData.append('parameters', JSON.stringify({
			method: 'bank_account',
			amount: 500,
			description: 'Pago de preinscripcion liceo',
			customer: {
				name: 'Juanchito',
				last_name: 'Perengano',
				phone_number: '2282111111',
				email: 'esau@icodestudio.pro'
			}
		}));
		await fetch(this.state.uri + '/api/pago/transfer/openpay/preinscrip', {
			method: 'POST', headers: {
				'Content-Type': 'multipart/form-data'
			}, body: formData
		}).then(response => response.json())
			.then(responseJson => {
				if (responseJson.response !== 'error') {
					Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
						text: 'Gracias',
						onPress: () => Actions.reciboTransferencia({
							reciboTienda: responseJson.result,
							transaction_id: responseJson.transaction_id,
							user: data
						})
					}]);
					this.setState({recibo: responseJson});
				} else {
					if (responseJson.error.code === 3001) {
						Alert.alert('¡Tarjeta rechazada!', 'Se ha rechazado la tarjeta', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3002) {
						Alert.alert('¡Tarjeta rechazada!', 'la tarjeta ha expirado', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3003) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta no tiene fondos', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3004) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido identificada como una tarjeta robada.', [{
							text: 'Entendido'
						}]);
					} else if (responseJson.error.code === 3005) {
						Alert.alert('¡Tarjeta rechazada!', 'La tarjeta ha sido rechazada por el sistema antifraudes.', [{
							text: 'Entendido'
						}]);

					} else if (responseJson.error.code === 1006) {
						Alert.alert('¡Pago rechazado!', 'El cargo ya ha sido generado anteriormente', [{
							text: 'Entendido'
						}]);
					} else {
						Alert.alert('¡Felicidades!', 'Su cargo se ha generado exitosamente', [{
							text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
						}]);
					}
				}
			}).catch(error => {
				Alert.alert('¡Ups!', 'No se pudo generar el cargo, intente más tarde', [{
					text: 'Gracias'
				}]);
			});
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione una tienda para hacer su pago
				</Text>
				<ScrollView style={styles.menu} showsVerticalScrollIndicator={false}>

					<View style={styles.row_M}>
						<TouchableOpacity onPress={() => this.pagoTienda(this.state.data)}
										  style={styles.btnContainer_M}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/superama.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.state.data)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/walmart.png')}/>
						</TouchableOpacity>
					</View>
					<View style={styles.row_M}>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.state.data)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/fa.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.state.data)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/sams.png')}/>
						</TouchableOpacity>
					</View>
					<View style={styles.row_M}>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.state.data)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/fg.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.state.data)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(9), width: responsiveWidth(35)
								}]}
								source={require('../../../images/waldos.png')}/>
						</TouchableOpacity>
					</View>
					<View style={styles.row_M}>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.state.data)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(9), width: responsiveWidth(35)
								}]}
								source={require('../../../images/prenda.png')}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.pagoTienda(this.state.data)}>
							<Image
								style={[styles.imagen, {
									height: responsiveHeight(10), width: responsiveWidth(35)
								}]}
								source={require('../../../images/aurrera.png')}/>
						</TouchableOpacity>
					</View>
					<View style={styles.row_M}>
						<TouchableOpacity style={styles.btnContainer_M}
										  onPress={() => this.transferencia(this.state.data)}>
							<Text style={[styles.textW, {textAlign: 'center'}]}>
								Transferencia bancaria{'\n'}(CLABE)
							</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</View>
		);
	}
}
