import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    ScrollView,
} from "react-native";
import styles from "../../styles";
import ModalSelector from "react-native-modal-selector";
import Periodos from "../Globales/Periodos";

export default class HistTarea extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            laMateria: [],
            elHistorial: [],
            materias: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getMaterias();
    }

    async onListItemPressedPeriodo(indexPeriodo, itemPeriodo) {
        this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: itemPeriodo
        });
    }

    //+++++++++++++++++++++++++++++HistTarea+++++++++++++++++++++++++++++++++++++
    async getHistTarea() {
        let histTareaa = await fetch(
            this.state.uri +
            "/api/materias/" +
            this.state.elPeriodo +
            "/" +
            this.state.laMateria,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let histTareaas = await histTareaa.json();
        this.setState({elHistorial: histTareaas});
    }

    //+++++++++++++++++++++++++++++Materias++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(
            this.state.uri + "/api/get/materias/alumno",
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia
        });
        await this.getHistTarea();
    }

    render() {
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.materia.materia[0].grado,
                key: i,
                label:
                item.materia.materia[0].nombre + " " + item.materia.materia[0].grado,
                materia: item.materia.id_materia
            };
        });
        return (
            <View sytle={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <View style={[styles.row, {marginTop: 10}]}>
                        <View>
                            <Text style={styles.label}>Campus</Text>
                            <View
                                style={[
                                    styles.listButton,
                                    styles.listButtonSmall,
                                    styles.btn5,
                                    styles.disabled
                                ]}
                            >
                                <Text>01</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.label}>Nivel</Text>
                            <View
                                style={[
                                    styles.listButton,
                                    styles.listButtonSmall,
                                    styles.btn5,
                                    styles.disabled
                                ]}
                            >
                                <Text>SEC</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.label}>Materia</Text>
                            <ModalSelector
                                cancelText="Cancelar"
                                data={data}
                                initValue="Seleccione la materia"
                                onChange={option => this.onChange(option)}
                                optionTextStyle={{color: this.state.thirdColor}}
                                selectStyle={[
                                    styles.listButton,
                                    styles.listButtonSmall,
                                    styles.btn3_5,
                                    {borderColor: this.state.secondColor}
                                ]}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
