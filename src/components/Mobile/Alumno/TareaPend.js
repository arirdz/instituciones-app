import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    ScrollView,
} from "react-native";
import styles from "../../styles";

export default class TareaPend extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            laMateria: [],
            laTarea: [],
            materias: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getTareaPend();
    }

    //++++++++++++++++++++++++++++++++ Tarea pendiente+++++++++++++++++++++++++++++
    async getTareaPend() {
        let pendiente = await fetch(this.state.uri + "/api/get/tareas/por/hacer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let pendientes = await pendiente.json();
        this.setState({laTarea: pendientes});
    }

    renderTarea(itemTar, indexTar) {
        return (
            <View
                key={indexTar}
                style={[
                    styles.contTarea,
                    styles.truContain,
                    {borderColor: this.state.secondColor}
                ]}
            >
                <View style={{marginBottom: 7}}>
                    <Text style={styles.textW}>Tarea:</Text>
                    <Text>{itemTar.tareas[indexTar].descripcion}</Text>
                </View>
                <View style={[styles.row, styles.btn3, {marginBottom: 2}]}>
                    <Text style={styles.textW}>Materia: </Text>
                    <Text>
                        {itemTar.tareas[indexTar].materia.nombre}{" "}
                        {itemTar.tareas[indexTar].materia.grado}
                    </Text>
                </View>
                <View style={[styles.row, styles.btn2]}>
                    <Text style={styles.textW}>Fecha de entrega: </Text>
                    <Text>{itemTar.tareas[indexTar].fecha_entrega}</Text>
                </View>
            </View>
        );
    }

    renderTareas() {
        let cardTarea = [];
        this.state.laTarea.forEach((itemTar, indexTar) => {
            cardTarea.push(this.renderTarea(itemTar, indexTar));
        });
        return cardTarea;
    }

    render() {
        const tarea = this.renderTareas();
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Tareas pendientes
                    </Text>
                    <View style={{marginVertical: 10}}>{tarea}</View>
                </ScrollView>
            </View>
        );
    }
}
