import React from "react";
import {
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    Text,
    View
} from "react-native";
import Periodos from "../Globales/Periodos";
import styles from "../../styles";

export default class examRecAlumn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cososPicker: [],
            selectedIndex: -1,
            periodos: [],
            materias: [],
            ciclos: [],
            selectedIndexCiclo: 0,
            laMateria: "",
            calificacion: [],
            c: "",
            elPeriodo: ""
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getMaterias();
    }

    //++++++++++++++++++++++++++++++++++ Periodos ++++++++++++++++++++++++++++++++

    async onListItemPressedPeriodo(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexPeriodo: indexSelected,
            elPeriodo: itemSelected
        });
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            selectedIndexGrupos: -1,
            urlGp: "/api/grupos/materias/" + option.materia
        });
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Valores +++++++++++++++++++++++++++++++++++++++++++++++
    async getValores() {
        let request = await fetch(
            this.state.uri +
            "/api/get/valor/examen/" +
            this.state.laMateria +
            "/" +
            this.state.elPeriodo +
            "/17-18/" +
            this.state.elGrupo +
            "/" +
            this.state.elExamen,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let res = await request.json();
        await this.setState({valores: res[0].valor});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++++++++++

    render() {
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        let index = 0;
        const data1 = [
            {key: index++, label: "Parcial", id: "Parcial"},
            {key: index++, label: "Bimestral", id: "Bimestral"}
        ];
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior={"padding"}
                keyboardVerticalOffset={60}
            >
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Periodos
                        onListItemPressedPeriodo={(indexBtn, itemBtn) =>
                            this.onListItemPressedPeriodo(indexBtn, itemBtn)
                        }
                    />

                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Materias
                    </Text>
                    <View style={[styles.rowTabla]}>
                        <View style={styles.campoTablaG}>
                            <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                                <View style={[styles.btn3_5, styles.centered_RT]}/>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Primer Calif.</Text>
                                </View>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Segunda Calif.</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View
                        style={[styles.tabla, {borderColor: this.state.secondColor}]}
                    />
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}
