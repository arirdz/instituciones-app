import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../styles';
import Periodos from '../Globales/Periodos';
import Entypo from "react-native-vector-icons/Entypo";
import Modal from "react-native-modal";

export default class encuadresAlmn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            elPeriodo: '',
            selectedIndexPeriodo: -1,
            encuadre: [],
            materiasArray: [],
            encuadresArray: [],
            elEstatus: '',
            incompletoMT: false,
            elCiclo: '1',
            grupos: [],
            laMateria: '',
            nombMate: '',
            data: [],
            elTeacher: '',
            periodos: [],
            modalPonder: [],
            aux: 0
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getUserdata();
        await this.getEncuadresArray();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo,
            encuadre: [],
            elTeacher: '',
            incompletoMT: true
        });
        if (this.state.laMateria !== ''){
            await this.getEncuadres();
        }
        await this.getEncuadresArray();
    }

    async getUserdata() {
        await fetch(this.state.uri + '/api/user/data/v2', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error ' +
                        // responseJson.error.status_code +
                        ' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({data: responseJson});
                    this.getMaterias();
                }
            });
        await this.setState({aux: 0});
    }

//+-+-+-+-+--+--+-+-EncuadreArray
    async getEncuadresArray() {
        await this.setState({incompletoMT: false});
        await fetch('http://127.0.0.1:8000/api/get/encuadre/array/padre/' + this.state.data.grado + '/' + this.state.data.grupo + '/' + this.state.elPeriodo + '/' + this.state.elCiclo,
            {
                method: 'GET', headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar el encuadres', 'Ha ocurrido un error al tratar de cargar el encuadre. Si el error continua pónganse en contacto con soporte (cod. 2)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({materiasArray: []});
                    this.setState({encuadresArray: responseJson});
                    if (this.state.encuadresArray.length === 0) {
                        this.setState({incompletoMT: true})
                    }

                    this.state.encuadresArray.forEach((it, i) => {
                        this.state.materiasArray.push(it.mate);
                    });
                }
            });
        await this.setState({aux: 0});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            nombMate: option.label
        });
        await this.setState({elTeacher: this.state.encuadresArray[option.key].maestro.name});
        await this.getEncuadres();
    }

    //+++++++++++++++++++++++++++++++Encuadres++++++++++++++++++++++++++++++++++++
    async getEncuadres() {
        await fetch('http://127.0.0.1:8000/api/get/encuadre/' +
            this.state.laMateria +
            '/' +
            this.state.elPeriodo +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.data.grupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Encuadre en revisión', 'Este encuadre esta siendo revisado por el coordinador, se le notificara cuando el encuadre sea publicado, Gracias', [
                        {text: 'Entendido'}
                    ]);
                } else {
                    this.setState({encuadre: responseJson});
                    if (this.state.encuadre.length !== 0) {
                        this.state.encuadre.forEach(() => {
                            this.state.modalPonder.push(false);
                        })
                    }
                }
            });
    }

    async openModalP(i, state) {
        this.state.modalPonder[i] = state;
        await this.setState({aux: 0});
    }

    renderEncuadres() {
        let btnEncuadre = [];
        this.state.encuadre.forEach((item, index) => {
            btnEncuadre.push(
                <View
                    key={index + 'encuadreDir'}
                    style={[
                        styles.widthall,
                        styles.row,
                        styles.cardEnc,
                        {backgroundColor: this.state.fourthColor, height: responsiveHeight(4)}
                    ]}
                >
                    <View style={styles.btn2}>
                        <Text style={styles.textW}>{item.descripcion}</Text>
                    </View>
                    <View style={styles.btn6}>
                        <Text style={[styles.textW, {textAlign: 'center'}]}>{item.valor}%</Text>
                    </View>
                    {Number(item.tipo) === 0 ? <View style={styles.btn6_ls}>
                        <Entypo name='magnifying-glass' size={25} color='#000'
                                style={{marginTop: 1}}
                                onPress={() => this.openModalP(index, true)}/>
                    </View>:<View style={styles.btn6_ls}/>}
                    {/*+-+-+-+-+-+-+Pondereaciones +-+-+-+-+-*/}
                    <Modal
                        isVisible={this.state.modalPonder[index]}
                        backdropOpacity={0.5}
                        animationIn={'bounceIn'}
                        animationOut={'bounceOut'}
                        animationInTiming={1000}
                        animationOutTiming={1000}
                    >
                        <View
                            style={[styles.container, {borderRadius: 6, flex: 0, maxHeight: responsiveHeight(80)}]}>
                            <Text
                                style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                            >
                                Ponderaciones de {item.descripcion}
                            </Text>
                            <View style={[styles.modalWidth, {marginVertical: 10, alignItems: 'center'}]}>
                                {this.rndPonderaciones(index)}
                            </View>
                            <View style={[styles.modalWidth, {marginVertical: 5, alignItems: 'center'}]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.openModalP(index, false)}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>
            )
        });
        return btnEncuadre;
    }

    rndPonderaciones(i) {
        let ponderaciones = [];
        if (this.state.encuadre[i].ponderaciones.length !== 0) {
            this.state.encuadre[i].ponderaciones.forEach((itm, i) => {
                ponderaciones.push(
                    <View
                        key={i + 'pondDir'}
                        style={[
                            styles.modalWidth,
                            styles.row,
                            styles.cardEnc,
                            {backgroundColor: this.state.fourthColor, height: responsiveHeight(3.4)}
                        ]}
                    >
                        <View style={styles.btn2}>
                            <Text style={styles.textW}>{itm.descripcion}</Text>
                        </View>
                        <View style={styles.btn6}>
                            <Text style={[styles.textW, {textAlign: 'center'}]}>{itm.valor}</Text>
                        </View>
                    </View>
                )
            });
        }
        return ponderaciones;
    }

    onChangeCriValue(index) {
        let a = 0;
        let b = 0;
        for (let i = 0; i < this.state.encuadre.length; i++) {
            if (this.state.encuadre[i].valor !== '') {
                if (a + parseInt(this.state.encuadre[i].valor) > 100) {
                    b = 100 - a;
                    this.state.encuadre[index].valor = b + '';
                    a = a + b;
                } else {
                    a = a + parseInt(this.state.encuadre[i].valor);
                }
            }
        }
        this.setState({elTotal: a});
        this.setState({aux: 0});
    }

    // ++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++
    async alertModal() {
        if (this.state.elPeriodo === '') {
            Alert.alert('Campo periodo vacío', 'Seleccione un periodo para continuar', [
                {text: 'Enterado'}
            ]);
        }
    }

    async alertModalMt() {
        Alert.alert('Atención',
            'Los encuadres de este grado y grupo estan en revisión', [
                {text: 'Entendido'}
            ])
    }

    render() {
        const verEn = this.renderEncuadres();
        let nombreMate = this.state.nombMate;
        let idMate = this.state.laMateria;
        let data = this.state.materiasArray.map((item, i) => {
            return {
                grado: item.grado,
                key: i,
                label: item.nombre,
                materia: item.id
            };
        });
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <Periodos
                    onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) => this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)}
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione la materia
                </Text>
                {this.state.elPeriodo !== '' && this.state.incompletoMT === false ? (
                    <ModalSelector
                        cancelText='Cancelar'
                        data={data}
                        initValue={idMate === '' ? 'Seleccione la materia' : nombreMate}
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
                        selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                    />
                ) : this.state.incompletoMT === true ? (
                    <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        onPress={() => this.alertModalMt()}
                    >
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>Seleccione la materia</Text>
                    </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        onPress={() => this.alertModal()}
                    >
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>Seleccione la materia</Text>
                    </TouchableOpacity>
                )}
                <Text style={{marginTop: 10}}>Docente a cargo: <Text
                    style={{fontWeight: '600'}}>{this.state.elTeacher}</Text></Text>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Encuadre
                </Text>
                {this.state.encuadre.length === 0 || this.state.elEstatus === 0 || this.state.elEstatus === 0 || this.state.elEstatus === '0' || this.state.elEstatus === '1' ? (
                    <Text style={[styles.main_title, {
                        color: '#a8a8a8',
                        textAlign: 'center',
                        marginTop: 40,
                        fontSize: responsiveFontSize(3)
                    }]}>
                        Encuadre en revisión
                    </Text>
                ) : this.state.encuadre.length !== 0 || this.state.elEstatus === 2 || this.state.elEstatus === '2' ? (
                    <View style={{alignItems: 'center'}}>
                        <View style={{
                            borderTopWidth: .2,
                            borderBottomWidth: .2,
                            height: responsiveHeight(35),
                            paddingVertical: 5
                        }}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {verEn}
                            </ScrollView>
                        </View>
                    </View>
                ) : null}
            </View>
        );
    }
}
