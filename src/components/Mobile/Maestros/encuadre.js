import React from 'react';
import {
    Alert,
    AsyncStorage,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import MultiBotonRow from '../Globales/MultiBotonRow';
import Spinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import styles from '../../styles';
import CorrecionEnc from './CorrecionEnc';
import Modal from 'react-native-modal';

export default class encuadre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newPonderac: [
                {'descripcion': 'No lo hizo', 'valor': '0'},
                {'descripcion': 'Muy mal', 'valor': '1'},
                {'descripcion': 'Regular', 'valor': '2'},
                {'descripcion': 'Bien', 'valor': '3'},
                {'descripcion': 'Muy bien', 'valor': '4'},
                {'descripcion': 'Excelente', 'valor': '5'}
            ],
            selectedIndex1st: 0,
            elTipoP: [],
            newTipoP: 0,
            botonSelec1st: 'Definir\nencuadre',
            criterios: '',
            criteriosValue: '',
            elCiclo: '',
            elGrado: '',
            elGrupo: '',
            elPeriodo: '',
            elRequest: [],
            elTotal: 0,
            encuadre: [],
            encUpdt: [],
            idEncuadre: [],
            grupos: [],
            isModalRubro: [],
            isModalnew: false,
            visible: false,
            grupos2: [],
            checkBoxBtns: [],
            checkBtnsIndex: [],
            checkPeriodo: [],
            checkPeriodoIndex: [],
            lasPonderaciones: [],
            laMateria: '',
            materias: [],
            periodos: [],
            elidMaestro: '',
            selectedIndexPeriodo: -1,
            cambio: 0,
            nuevo: '',
            aux: 0,
            descripPond: [],
            valorPond: [],
        };
    }

    spinner = (state) => {
        this.setState({visible: state});
    };

    async componentWillMount() {
        await this.getURL();
        await this.getMaterias();
        await this.getUserdata();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async getPeriodos() {
        let periodopicker = await fetch(this.state.uri + '/api/periodos/' + this.state.elCiclo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let periodospicker = await periodopicker.json();
        await this.setState({periodos: periodospicker});
    }

    async onListItemPressedPeriodo(indexPeriodo, itemPeriodo) {
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: itemPeriodo,
            checkPeriodo: [],
            checkPeriodoIndex: [],
            criterios: '',
            criteriosValue: ''
        });
        await this.setState({aux: 0});
        await this.getEncuadre();
    }

    async onListItemPressedPeriodo2(indexPeriodo, itemPeriodo) {
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: itemPeriodo
        });
        if (this.state.checkPeriodo.length !== 0) {
            this.state.checkPeriodo = [];
            this.state.checkPeriodoIndex = 0;
        } else {
            this.state.checkPeriodo = this.state.periodos;
            this.state.checkPeriodoIndex = 1;
        }
        await this.setState({aux: 0});
    }

    renderPeriodos() {
        let btnPeriodos = [];
        this.state.periodos.forEach((itemPeriodo, indexPeriodo) => {
            let smallButtonStyles = [
                styles.listButton,
                styles.listButtonSmall,
                styles.btn6,
                {borderColor: this.state.secondColor}
            ];
            let texto = [styles.textoN];
            if (this.state.selectedIndexPeriodo === indexPeriodo) {
                smallButtonStyles.push(styles.listButtonSelected, {
                    backgroundColor: this.state.secondColor
                });
                texto.push(styles.textoB);
            }
            if (this.state.checkPeriodoIndex === 1) {
                smallButtonStyles.push(styles.listButtonSelected, {
                    backgroundColor: this.state.secondColor
                });
                texto.push(styles.textoB);
            }
            btnPeriodos.push(
                <TouchableHighlight
                    key={indexPeriodo}
                    style={smallButtonStyles}
                    underlayColor={this.state.secondColor}
                    onPress={() => this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)}
                >
                    <View style={styles.listItem}>
                        <Text style={texto}>{itemPeriodo}</Text>
                    </View>
                </TouchableHighlight>
            );
        });
        return btnPeriodos;
    }

    //+++++++++++++++++++++++++++++++ Get User +++++++++++++++++++++++++++++++++++
    async getUserdata() {
        let request = await fetch(this.state.uri + '/api/user/data', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let dato = await request.json();
        await this.setState({data: dato});
        await this.setState({elidMaestro: this.state.data.user_id});
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + '/api/materias/maestro', {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            elGrupo: '',
            selectedIndexGrupo: -1,
            elPeriodo: '',
            selectedIndexPeriodo: -1,
            checkPeriodo: [],
            grupos2: [],
            checkPeriodoIndex: [],
            checkBoxBtns: [],
            checkBtnsIndex: [],
            encuadre: []
        });
        await this.getGrupos();
        await this.getCicloActual(option.grado);
    }

    // ++++++++++++++++++++++++++++++++ GRUPOS ++++++++++++++++++++++++++++++++++
    async onListItemPressedGrupo(index, grupo) {
        this.setState({
            selectedIndexGrupo: index,
            elGrupo: grupo,
            checkBoxBtns: [],
            checkBtnsIndex: []
        });
        if (this.state.laMateria !== '' && this.state.elPeriodo !== '') {
            await this.getEncuadre();
        }
        await this.setState({aux: 0});
    }

    async onListItemPressedGrupo2(index, grupo) {
        this.setState({
            selectedIndexGrupo: index,
            elGrupo: grupo
        });
        if (this.state.checkBoxBtns.length !== 0) {
            this.state.checkBoxBtns = [];
            this.state.checkBtnsIndex = 0;
        } else {
            this.state.checkBoxBtns = this.state.grupos2;
            this.state.checkBtnsIndex = 1;
        }
        await this.setState({aux: 0});
    }

    async getGrupos() {
        let grupopicker = await fetch(
            this.state.uri + '/api/grupos/materias/' + this.state.laMateria,
            {
                method: 'GET', headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            });
        let grupospicker = await grupopicker.json();
        this.setState({grupos: grupospicker});
        this.state.grupos.forEach((item, i) => {
            this.state.grupos2.push(item.grupo)
        });
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            let smallButtonStyles = [
                styles.listButton,
                styles.listButtonSmall,
                styles.btn6,
                {borderColor: this.state.secondColor}
            ];
            let texto = [styles.textoN];
            if (this.state.selectedIndexGrupo === indexGrupo) {
                smallButtonStyles.push(styles.listButtonSelected, {
                    backgroundColor: this.state.secondColor
                });
                texto.push(styles.textoB);
            }

            if (this.state.checkBtnsIndex === 1) {
                smallButtonStyles.push(styles.listButtonSelected, {
                    backgroundColor: this.state.secondColor
                });
                texto.push(styles.textoB);
            }
            buttonsGrupos.push(
                <TouchableHighlight
                    key={indexGrupo}
                    style={smallButtonStyles}
                    underlayColor={this.state.secondColor}
                    onPress={() => this.onListItemPressedGrupo(indexGrupo, itemGrupo.grupo)}
                >
                    <View style={styles.listItem}>
                        <Text style={texto}>{itemGrupo.grupo}</Text>
                    </View>
                </TouchableHighlight>
            );
        });
        return buttonsGrupos;
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected,
            elGrupo: '',
            selectedIndexGrupo: -1
        });
    }

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-**-*-encuadre y ponderacion

    async getEncuadre() {
        await this.spinner(true);
        await fetch('http://127.0.0.1:8000/api/get/encuadre/' +
            this.state.laMateria +
            '/' +
            this.state.elPeriodo +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar el encuadres',
                        'Hubo un error al tratar de cargar el encuadre', [
                            {text: 'Entendido', onPress: () => this.spinner(false)}
                        ]);
                } else {
                    this.setState({encuadre: responseJson});
                    if (this.state.encuadre.length === 0) {
                        this.setState({nuevo: 1, cambio: 1})
                    } else {
                        let id_encuadre = [];
                        this.setState({nuevo: 0});
                        this.state.encuadre.forEach((item, index) => {
                            this.state.isModalRubro.push(false);
                            this.state.elTipoP.push(item.tipo);
                            id_encuadre.push(item.id);
                        });
                        this.setState({idEncuadre: id_encuadre});
                    }
                    this.spinner(false);
                }
            });
        await this.setState({aux: 0});
        const y = this.state.encuadre.length;
        for (let i = 0; i < y; i++) {
            for (let j = 0; j < y - 1 - i; j++) {
                if (this.state.encuadre[j].valor < this.state.encuadre[j + 1].valor) {
                    [this.state.encuadre[j], this.state.encuadre[j + 1]] = [
                        this.state.encuadre[j + 1],
                        this.state.encuadre[j]
                    ];
                }
            }
        }

        await this.setState({aux: 0});
        await this.onChangeCriValue();
    }

    async openModal(i, state) {
        this.state.isModalRubro[i] = state;
        this.state.criterios = this.state.encuadre[i].descripcion;
        this.state.criteriosValue = this.state.encuadre[i].valor;
        //+--+-+-+-+-rubro
        await this.setState({encUpdt: [], lasPonderaciones: [], descripPond: [], valorPond: []});
        this.state.encuadre[i].ponderaciones.forEach((it, ix) => {
            this.state.lasPonderaciones.push(it);
            this.state.descripPond.push(it.descripcion);
            this.state.valorPond.push(it.valor);
        });
        this.state.encuadre.forEach((itm, ix) => {
            this.state.encUpdt.push(itm)
        });
        await this.setState({aux: 0});
    }

    async closeModal(i, state, validate) {
        if (validate === 'change') {
            let found = false;
            for (let j = 0; j < this.state.encUpdt[i].ponderaciones.length; j++) {
                if (this.state.encUpdt[i].ponderaciones[j].descripcion === '' || this.state.encUpdt[i].ponderaciones[j].valor === '') {
                    found = true;
                    break;
                }
            }
            if (found === true) {
                Alert.alert('Ponderaciones sin definir',
                    'Defina todos los campos de ponderaciones para poder continuar');
            } else {
                this.state.encuadre[i].descripcion = this.state.criterios;
                this.state.encuadre[i].valor = this.state.criteriosValue;
                if (this.state.criterios !== this.state.encuadre[i].valor
                    || this.state.criteriosValue !== this.state.encuadre[i].descripcion) {
                    this.setState({cambio: 1});
                } else {
                    this.setState({cambio: 0});
                }
                await this.onChangeCriValue(i);
                const y = this.state.encuadre.length;
                for (let i = 0; i < y; i++) {
                    for (let j = 0; j < y - 1 - i; j++) {
                        if (this.state.encuadre[j].valor < this.state.encuadre[j + 1].valor) {
                            [this.state.encuadre[j], this.state.encuadre[j + 1]] = [this.state.encuadre[j + 1], this.state.encuadre[j]];
                        }
                    }
                }
                let encs = [];
                this.state.encUpdt.forEach((itmEnc, ix) => {
                    encs.push(itmEnc);
                });
                let ponderDescrip = [];
                let pondervalue = [];
                this.state.encUpdt[i].ponderaciones.forEach((itm, ix) => {
                    ponderDescrip.push(itm.descripcion);
                    pondervalue.push(itm.valor);
                });
                await this.setState({encuadre: encs, descripPond: ponderDescrip, valorPond: pondervalue});
                this.state.isModalRubro[i] = state;
            }
        } else if (validate === 'reset') {
            let encUpdater = [];
            this.state.lasPonderaciones.forEach((itm, ix) => {
                encUpdater.push(itm);
                this.state.encUpdt[i].ponderaciones[ix].descripcion = this.state.descripPond[ix];
                this.state.encUpdt[i].ponderaciones[ix].valor = this.state.valorPond[ix];
            });
            this.state.encUpdt[i].ponderaciones = encUpdater;
            this.state.isModalRubro[i] = state;
        }
        await this.setState({aux: 0});
    }


    async alertBorrarR(i) {
        Alert.alert('Borrar rubro', 'Está a punto de borrar un rubro\n¿Seguro que quiere continuar?', [
            {text: 'Sí', onPress: () => this.borrar(i)}, {text: 'No'}
        ]);
        await this.setState({aux: 0});
    }

    async borrar(i) {
        this.state.encuadre.splice(i, 1);
        await this.setState({aux: 0});
        await this.setState({cambio: 1});
        await this.onChangeCriValue();
    }

    renderEncuadres() {
        let btnEncuadre = [];
        if (this.state.encuadre.length !== 0) {
            this.state.encuadre.forEach((item, i) => {
                btnEncuadre.push(
                    <View
                        key={i + 'encuadre'}
                        style={[
                            styles.widthall,
                            styles.row,
                            styles.cardEnc,
                            {backgroundColor: this.state.fourthColor, height: responsiveHeight(4)}
                        ]}
                    >
                        <Entypo name='circle-with-minus' size={22} color='red' onPress={() => this.alertBorrarR(i)}/>
                        <Text style={[styles.btn2, styles.textW]}>{item.descripcion}</Text>
                        <Text style={[styles.textW, {textAlign: 'center'}]}>{item.valor}%</Text>
                        <MaterialIcons name='edit' size={21} color='grey' onPress={() => this.openModal(i, true)}/>
                        {/*+-+-+-+-+-+-+-+-+-+-+-+-+-+-Editar rubro+-+-+-+-+-+-+-+-*/}
                        <Modal
                            backdropOpacity={0.5}
                            animationInTiming={1000}
                            animationIn={'bounceIn'}
                            animationOutTiming={1000}
                            animationOut={'bounceOut'}
                            isVisible={this.state.isModalRubro[i]}
                        >
                            <KeyboardAvoidingView
                                behavior={Platform.OS === 'ios' ? 'padding' : null}
                                keyboardVerticalOffset={Platform.OS === 'ios' ? 10 : 0}
                                enabled
                                style={[styles.container, {borderRadius: 10, flex: 0, maxHeight: responsiveHeight(80)}]}
                            >
                                <Text
                                    style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                                >
                                    Defina el rubro
                                </Text>
                                <View style={[styles.row, styles.modalWidth, {marginTop: 10}]}>
                                    <TextInput
                                        keyboardType='default'
                                        maxLength={20}
                                        multiline={false}
                                        onChangeText={text => (this.state.criterios = text)}
                                        onEndEditing={() => Keyboard.dismiss()}
                                        placeholder={'Tareas, Proyectos, Parcial, etc....'}
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent'
                                        defaultValue={this.state.criterios}
                                        style={[
                                            styles.listButton,
                                            styles.listButtonSmall,
                                            styles.btn3_5,
                                            styles.txtCenter,
                                            {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                        ]}
                                    />
                                    <TextInput
                                        keyboardType='numeric'
                                        maxLength={2}
                                        multiline={false}
                                        onChangeText={text => [
                                            (this.state.criteriosValue = text),
                                            this.onChangeCriValue(i)
                                        ]}
                                        onEndEditing={() => Keyboard.dismiss()}
                                        placeholder={'10'}
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent'
                                        defaultValue={this.state.criteriosValue}
                                        style={[
                                            styles.listButton,
                                            styles.listButtonSmall,
                                            styles.btn3T,
                                            styles.txtCenter,
                                            {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                        ]}
                                    />
                                </View>
                                {Number(item.tipo) === 0 ?
                                    <View>
                                        <Text
                                            style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                                            Defina las ponderaciones
                                        </Text>
                                        <ScrollView
                                            overScrollMode={'always'}
                                            style={{maxHeight: responsiveHeight(30), marginVertical: 10}}
                                            showsVerticalScrollIndicator={false}
                                        >
                                            {this.state.encUpdt.length !== 0 ? this.rndPonderaciones(i) : null}
                                            <View style={[styles.modalWidth, {alignItems: 'center'}]}>
                                                <Entypo name='circle-with-plus' size={30} color='green'
                                                        style={{marginTop: 10}}
                                                        onPress={() => this.agregarP(i)}/>
                                            </View>
                                        </ScrollView>
                                    </View> : null}
                                <View style={[styles.modalWidth, styles.row, {marginVertical: 10}]}>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                        ]}
                                        onPress={() => this.closeModal(i, false, 'reset')}
                                    >
                                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {
                                                backgroundColor: this.state.secondColor,
                                                borderColor: this.state.secondColor
                                            }
                                        ]}
                                        onPress={() => this.closeModal(i, false, 'change')}
                                    >
                                        <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                                    </TouchableOpacity>
                                </View>
                            </KeyboardAvoidingView>
                        </Modal>
                    </View>
                )
            });
        }
        return btnEncuadre;
    }

    async agregarP(i) {
        this.state.encUpdt[i].ponderaciones.push({
            descripcion: '',
            valor: ''
        });
        await this.setState({aux: 0});
    }

    async alertDeleteP(i, ix) {
        Alert.alert('Borrar ponderacion', '¿Seguro que quiere continuar?', [
            {text: 'Sí', onPress: () => this.borrarP(i, ix)}, {text: 'No'}
        ]);
        await this.setState({aux: 0});
    }

    async borrarP(i, ix) {
        this.state.encUpdt[i].ponderaciones.splice(ix, 1);
        await this.setState({aux: 0});
        await this.setState({cambio: 1});
    }

    rndPonderaciones(i) {
        let ponderaciones = [];
        if (this.state.encUpdt[i].ponderaciones.length !== 0) {
            this.state.encUpdt[i].ponderaciones.forEach((itm, ix) => {
                ponderaciones.push(
                    <View
                        key={ix + 'ponder'}
                        style={[styles.modalWidth, styles.row, {marginVertical: 2}]}
                    >
                        <Entypo name='circle-with-minus' size={22} color='red'
                                onPress={() => this.alertDeleteP(i, ix)}/>
                        <TextInput
                            keyboardType='default'
                            maxLength={20}
                            multiline={false}
                            onChangeText={text => (this.state.encUpdt[i].ponderaciones[ix].descripcion = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'Mal, Regular, Bien, etc....'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.encUpdt[i].ponderaciones[ix].descripcion}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn2,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                        <TextInput
                            keyboardType='numeric'
                            maxLength={2}
                            multiline={false}
                            onChangeText={text => (this.state.encUpdt[i].ponderaciones[ix].valor = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'0'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.encUpdt[i].ponderaciones[ix].valor}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn3T,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                    </View>
                )
            });
        }
        return ponderaciones
    }

    //+++++++++++++++++++++++++++ PERIODO ++++++++++++++++++++++++++++++++++++++++
    async getCicloActual(grado) {
        await fetch(this.state.uri + '/api/ciclo/periodo/actual/' + grado, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar las materias',
                        'Hubo un error al tratar de cargar las materias');
                } else {
                    this.setState({elCiclo: responseJson.ciclo});
                    this.getPeriodos(responseJson.ciclo)
                }
            });
    }

    //-+*-+*-*+-*+*-+*-*+-*+*-+*-+*-*+-*+-*+  EDITAR  *-+*-+*-*+-*+-*+*-+*-*+-*+-*+*-+*-*+*-+*-+*-*-*

    async cosasLocas2() {
        if (this.state.laMateria === '') {
            Alert.alert('Campo materia vacío', 'No ha seleccionado una materia', [
                {text: 'Entendido'}
            ]);
        } else {
            if (this.state.elGrupo === '') {
                Alert.alert('Campo grupo vacío', 'No ha seleccionado grupo', [
                    {text: 'Entendido'}
                ]);
            } else {
                if (this.state.elTotal !== 100) {
                    Alert.alert('¡Alerta!', 'El total del encuadre no es del 100%', [
                        {text: 'Entendido'}
                    ]);
                } else if (this.state.checkBtnsIndex === 1 && this.state.checkPeriodoIndex === 1) {
                    if (this.state.nuevo === 1) {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.requestMultipart2()},
                            {text: 'No'}
                        ]);
                    } else {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.borrarEncuadre2()},
                            {text: 'No'}
                        ]);
                    }
                } else if (this.state.checkPeriodoIndex === 1) {
                    if (this.state.nuevo === 1) {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.requestMultipart3()},
                            {text: 'No'}
                        ]);
                    } else {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.borrarEncuadre3()},
                            {text: 'No'}
                        ]);
                    }
                } else if (this.state.checkBtnsIndex === 1) {
                    if (this.state.nuevo === 1) {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.requestMultipart4()},
                            {text: 'No'}
                        ]);
                    } else {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.borrarEncuadre4()},
                            {text: 'No'}
                        ]);
                    }
                } else {
                    if (this.state.nuevo === 1) {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.requestMultipart()},
                            {text: 'No'}
                        ]);
                    } else {
                        Alert.alert('¡Guardar!', '¿Seguro que desea continuar?', [
                            {text: 'Sí', onPress: () => this.borrarEncuadre()},
                            {text: 'No'}
                        ]);
                    }
                }
            }

        }
        await this.setState({aux: 0});
    }

//+-+-++-+-+-+-+-+-request1
    async borrarEncuadre() {
        this.spinner(true);
        let request = [];
        for (let i = 0; i < this.state.idEncuadre.length; i++) {
            let formData = new FormData();
            formData.append('new', JSON.stringify({
                grado: this.state.elGrado,
                grupo: this.state.elGrupo,
                id_maestro: this.state.elidMaestro,
                id_materia: this.state.laMateria,
                periodo: this.state.elPeriodo,
                id_encuadre: this.state.idEncuadre[i]
            }));
            await fetch('http://127.0.0.1:8000/api/borrar/encuadre', {
                method: 'POST', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: 'Bearer ' + this.state.token
                },
                body: formData
            }).then(res => res.json())
                .then(responseJson => {
                    request = responseJson
                });
        }
        if (request.error !== undefined) {
            Alert.alert('¡Ups!',
                'Ha ocurrido un error ' + request.error.status_code + ' si el error continua pónganse en contacto con soporte'
                +
                '(Encuadres)',
                [{
                    text: 'Entendido', onPress: () => this.setState({visible: false})
                }]);
        } else {
            await this.setState({aux: 0});
            await this.requestMultipart();
        }
    }

    arreglo() {
        let btnTodos = [];
        for (let i = 0; i < this.state.encuadre.length; i++) {
            btnTodos.push({
                ciclo: this.state.elCiclo,
                descripcion: this.state.encuadre[i].descripcion,
                grado: this.state.elGrado,
                grupo: this.state.elGrupo,
                id_maestro: this.state.data.user_id,
                id_materia: this.state.laMateria,
                periodo: this.state.elPeriodo,
                valor: this.state.encuadre[i].valor,
                ponderaciones: this.state.encuadre[i].ponderaciones,
                director: '',
                subdirector: '',
                cord_academico: '',
                tipo: this.state.encuadre[i].tipo,
                estatus: 0,
            })
        }
        return btnTodos;
    }

    async requestMultipart() {
        const todo = this.arreglo();
        let formData = new FormData();
        formData.append('new', JSON.stringify(todo));
        await fetch('http://127.0.0.1:8000/api/create/encuadre', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Encuadres)',
                        [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                } else {
                    Alert.alert('¡Felicidades!',
                        'Se ha publicado el(los) encuadre(s) con éxito',
                        [{
                            text: 'Entendido',
                            onPress: () => [this.setState({visible: false}), this.getEncuadre()]
                        }]);
                }
            });
    }

//+-+-++-+-+-+-+-+-request2
    async borrarEncuadre2() {
        this.spinner(true);
        let formData = new FormData();
        for (let j = 0; j < this.state.checkPeriodo.length; j++) {
            for (let i = 0; i < this.state.checkBoxBtns.length; i++) {
                for (let k = 0; k < this.state.idEncuadre.length; k++) {
                    formData.append('new', JSON.stringify({
                        grado: this.state.elGrado,
                        grupo: this.state.checkBoxBtns[i],
                        id_maestro: this.state.elidMaestro,
                        id_materia: this.state.laMateria,
                        periodo: this.state.checkPeriodo[j],
                        id_encuadre: this.state.idEncuadre[k]
                    }));
                    await fetch('http://127.0.0.1:8000/api/borrar/encuadre', {
                        method: 'POST', headers: {
                            Accept: 'application/json',
                            'Content-Type': 'multipart/form-data',
                            Authorization: 'Bearer ' + this.state.token
                        }, body: formData
                    }).then(res => res.json())
                        .then(responseJson => {
                            this.setState({elRequest: responseJson});
                        });
                }
            }
        }
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('¡Ups!',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Encuadres)',
                [{
                    text: 'Entendido', onPress: () => this.setState({visible: false})
                }]);
        }
        await this.requestMultipart2();
    }

    arreglo2() {
        let btnTodos = [];
        for (let h = 0; h < this.state.checkPeriodo.length; h++) {
            for (let j = 0; j < this.state.checkBoxBtns.length; j++) {
                for (let i = 0; i < this.state.encuadre.length; i++) {
                    btnTodos.push({
                        ciclo: this.state.elCiclo,
                        descripcion: this.state.encuadre[i].descripcion,
                        grado: this.state.elGrado,
                        grupo: this.state.checkBoxBtns[j],
                        id_maestro: this.state.elidMaestro,
                        id_materia: this.state.laMateria,
                        periodo: this.state.checkPeriodo[h],
                        valor: this.state.encuadre[i].valor,
                        ponderaciones: this.state.encuadre[i].ponderaciones,
                        director: '',
                        subdirector: '',
                        cord_academico: '',
                        tipo: this.state.encuadre[i].tipo,
                        estatus: 0,
                        comentario: ''
                    })
                }
            }
        }
        return btnTodos;
    }

    async requestMultipart2() {
        const todo = this.arreglo2();
        let formData = new FormData();
        formData.append('new', JSON.stringify(todo));
        await fetch('http://127.0.0.1:8000/api/create/encuadre', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({elRequest: responseJson});
            });
        if (responseJson.error !== undefined) {
            Alert.alert('¡Ups!',
                'Ha ocurrido un error '
                +
                responseJson.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Encuadres)',
                [{
                    text: 'Entendido', onPress: () => this.setState({visible: false})
                }]);
        } else {
            Alert.alert('¡Felicidades!',
                'Se ha publicado el(los) encuadre(s) con éxito',
                [{
                    text: 'Entendido',
                    onPress: () => this.setState({visible: false})
                }]);
        }
        await this.setState({aux: 0});
    }

//+-+-++-+-+-+-+-+-request3
    async borrarEncuadre3() {
        this.spinner(true);
        let formData = new FormData();
        for (let i = 0; i < this.state.checkPeriodo.length; i++) {
            for (let k = 0; k < this.state.idEncuadre.length; k++) {
                formData.append('new', JSON.stringify({
                    grado: this.state.elGrado,
                    grupo: this.state.elGrupo,
                    id_maestro: this.state.elidMaestro,
                    id_materia: this.state.laMateria,
                    periodo: this.state.checkPeriodo[i],
                    id_encuadre: this.state.idEncuadre[k]
                }));
                await fetch('http://127.0.0.1:8000/api/borrar/encuadre', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: formData
                }).then(res => res.json())
                    .then(responseJson => {
                        this.setState({elRequest: responseJson});
                    });
            }
        }
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('¡Ups!',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Encuadres)',
                [{
                    text: 'Entendido', onPress: () => this.setState({visible: false})
                }]);
        } else {
            await this.setState({aux: 0});
            await this.requestMultipart3();
        }
    }

    arreglo3() {
        let btnTodos3 = [];
        for (let j = 0; j < this.state.checkPeriodo.length; j++) {
            for (let i = 0; i < this.state.encuadre.length; i++) {
                btnTodos3.push({
                    ciclo: this.state.elCiclo,
                    descripcion: this.state.encuadre[i].descripcion,
                    grado: this.state.elGrado,
                    grupo: this.state.elGrupo,
                    id_maestro: this.state.elidMaestro,
                    id_materia: this.state.laMateria,
                    periodo: this.state.checkPeriodo[j],
                    valor: this.state.encuadre[i].valor,
                    ponderaciones: this.state.encuadre[i].ponderaciones,
                    director: '',
                    subdirector: '',
                    cord_academico: '',
                    tipo: this.state.encuadre[i].tipo,
                    estatus: 0,
                    comentario: ''
                })
            }
        }
        return btnTodos3;
    }

    async requestMultipart3() {
        const todo = this.arreglo3();
        let formData = new FormData();
        formData.append('new', JSON.stringify(todo));
        await fetch('http://127.0.0.1:8000/api/create/encuadre', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            },
            body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Encuadres)',
                        [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                } else {
                    Alert.alert('¡Felicidades!',
                        'Se ha publicado el(los) encuadre(s) con éxito',
                        [{
                            text: 'Entendido',
                            onPress: () => this.setState({visible: false})
                        }]);
                }
            });

    }

//+-+-++-+-+-+-+-+-request4
    async borrarEncuadre4() {
        this.spinner(true);
        let formData = new FormData();
        for (let i = 0; i < this.state.checkBoxBtns.length; i++) {
            for (let k = 0; k < this.state.idEncuadre.length; k++) {
                formData.append('new', JSON.stringify({
                    grado: this.state.elGrado,
                    grupo: this.state.checkBoxBtns[i],
                    id_maestro: this.state.elidMaestro,
                    id_materia: this.state.laMateria,
                    periodo: this.state.elPeriodo,
                    id_encuadre: this.state.idEncuadre[k]
                }));
                await fetch('http://127.0.0.1:8000/api/borrar/encuadre', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: formData
                }).then(res => res.json())
                    .then(responseJson => {
                        this.setState({elRequest: responseJson});
                    });
            }
        }
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('¡Ups!',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Encuadres)',
                [{
                    text: 'Entendido', onPress: () => this.setState({visible: false})
                }]);
        } else {
            await this.setState({aux: 0});
            this.requestMultipart4();
        }
    }

    arreglo4() {
        let btnTodos = [];
        for (let j = 0; j < this.state.checkBoxBtns.length; j++) {
            for (let i = 0; i < this.state.encuadre.length; i++) {
                btnTodos.push({
                    ciclo: this.state.elCiclo,
                    descripcion: this.state.encuadre[i].descripcion,
                    grado: this.state.elGrado,
                    grupo: this.state.checkBoxBtns[j],
                    id_maestro: this.state.elidMaestro,
                    id_materia: this.state.laMateria,
                    periodo: this.state.elPeriodo,
                    director: '',
                    subdirector: '',
                    cord_academico: '',
                    valor: this.state.encuadre[i].valor,
                    ponderaciones: this.state.encuadre[i].ponderaciones,
                    tipo: this.state.encuadre[i].tipo,
                    estatus: 0,
                    comentario: ''
                })
            }
        }
        return btnTodos;
    }

    async requestMultipart4() {
        const todo = this.arreglo4();
        let formData = new FormData();
        formData.append('new', JSON.stringify(todo));
        await fetch('http://127.0.0.1:8000/api/create/encuadre', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Encuadres)',
                        [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                } else {
                    Alert.alert('¡Felicidades!',
                        'Se ha publicado el(los) encuadre(s) con éxito',
                        [{
                            text: 'Entendido',
                            onPress: () => this.setState({visible: false})
                        }]);
                }
            });

    }

    onChangeCriValue(index) {
        let a = 0;
        let b = 0;
        for (let i = 0; i < this.state.encuadre.length; i++) {
            if (this.state.encuadre[i].valor !== '') {
                if (a + parseInt(this.state.encuadre[i].valor) > 100) {
                    b = 100 - a;
                    this.state.encuadre[index].valor = b + '';
                    a = a + b;
                } else {
                    a = a + parseInt(this.state.encuadre[i].valor);
                }
            }
        }
        this.setState({elTotal: a});
        this.setState({aux: 0});
    }

    async captEnc() {
        if (this.state.laMateria === '') {
            Alert.alert('¡Ups!', 'No ha seleccionado una materia', [
                {text: 'Entendido'}
            ]);
        } else {
            if (this.state.elGrupo === '') {
                Alert.alert('¡Ups!', 'No ha seleccionado grupo', [
                    {text: 'Entendido'}
                ]);
            } else {
                if (this.state.elPeriodo === '') {
                    Alert.alert('¡Ups!', 'No ha seleccionado periodo', [
                        {text: 'Entendido'}
                    ]);
                } else {
                    this.openModalNew()
                }
            }

        }
        await
            this.setState({aux: 0});
    }

// ++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++
    async agregar() {
        this.state.encuadre.push({
            descripcion: this.state.criterios,
            valor: this.state.criteriosValue,
            tipo: this.state.newTipoP,
            ponderaciones: this.state.newPonderac
        });
        this.state.encUpdt.push({
            descripcion: this.state.criterios,
            valor: this.state.criteriosValue,
            tipo: this.state.newTipoP,
            ponderaciones: this.state.newPonderac
        });
        this.state.encuadre.forEach(() => {
            this.state.isModalRubro.push(false);
            this.state.elTipoP.push(1);
        });
        await this.setState({aux: 0});
    }

    async onDelete(i) {
        this.state.newPonderac.splice(i, 1);
        await this.setState({aux: 0});
    }

    async addNewPonderacion() {
        this.state.newPonderac.push({descripcion: '', valor: ''});
        await this.setState({aux: 0});
    }

    rndNewArrPond() {
        let arrayPonde = [];
        if (this.state.newPonderac.length !== 0) {
            this.state.newPonderac.forEach((itm, i) => {
                arrayPonde.push(
                    <View
                        key={i + 'ponder'}
                        style={[styles.modalWidth, styles.row, {marginVertical: 2}]}
                    >
                        <Entypo name='circle-with-minus' size={22} color='red'
                                onPress={() => this.onDelete(i)}/>
                        <TextInput
                            keyboardType='default'
                            maxLength={20}
                            multiline={false}
                            onChangeText={text => (this.state.newPonderac[i].descripcion = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'Mal, Regular, Bien, etc....'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.newPonderac[i].descripcion}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn2,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                        <TextInput
                            keyboardType='numeric'
                            maxLength={2}
                            multiline={false}
                            onChangeText={text => (this.state.newPonderac[i].valor = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'0'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.newPonderac[i].valor}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn3T,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                    </View>
                )
            })
        }
        return arrayPonde;
    }

    async agregarNew(index) {
        if (this.state.criterios === '') {
            Alert.alert('Rubro sin definir', 'No ha definido el rubro', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.criteriosValue === '') {
            Alert.alert('Valor del rubro sin definir', 'No ha dado valor al rubro', [
                {text: 'Entendido'}
            ]);
        } else {
            this.closeModalNew(index)
        }
    }

    async openModalNew() {
        this.setState({isModalnew: true, criterios: '', criteriosValue: ''});
    }

    async closeModalNew(index) {
        this.state.isModalnew = await false;
        await this.setState({cambio: 1});
        await this.setState({aux: 0});
        await this.agregar();

        const y = this.state.encuadre.length;
        for (let i = 0; i < y; i++) {
            for (let j = 0; j < y - 1 - i; j++) {
                if (this.state.encuadre[j].valor < this.state.encuadre[j + 1].valor) {
                    [this.state.encuadre[j], this.state.encuadre[j + 1]] = [
                        this.state.encuadre[j + 1],
                        this.state.encuadre[j]
                    ];
                }
            }
        }

        await this.setState({aux: 0});
        await this.onChangeCriValue(index)
    }

    async cancelarNew() {
        let ponderadds = [
            {'descripcion': 'No lo hizo', 'valor': '0'},
            {'descripcion': 'Muy mal', 'valor': '1'},
            {'descripcion': 'Regular', 'valor': '2'},
            {'descripcion': 'Bien', 'valor': '3'},
            {'descripcion': 'Muy bien', 'valor': '4'},
            {'descripcion': 'Excelente', 'valor': '5'}
        ];
        await this.setState({isModalnew: false, newPonderac: ponderadds});
    }

    async btnSelected(num) {
        await this.setState({newTipoP: num});
    }

    rendEnc() {
        let index = this.state.newTipoP;
        const periodos = this.renderPeriodos();
        const verEn = this.renderEncuadres();
        const grupos = this.renderGrupos();
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre,
                materia: item.nombre.id
            };
        });
        return (
            <View style={styles.container}>
                {/*+-+-+-+-+-+-+-+-+-+-+-+-Nuevo rubro-+-+-+-+-+-+-+-+-+-+*/}
                <Modal
                    isVisible={this.state.isModalnew}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <KeyboardAvoidingView
                        behavior={Platform.OS === 'ios' ? 'padding' : null}
                        keyboardVerticalOffset={Platform.OS === 'ios' ? 10 : 0}
                        enabled
                        style={[styles.container, {borderRadius: 10, flex: 0, maxHeight: responsiveHeight(80)}]}>
                        <Text
                            style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                        >
                            Defina el rubro
                        </Text>
                        <View style={[styles.row, styles.modalWidth, {marginTop: 10}]}>
                            <TextInput
                                keyboardType='default'
                                maxLength={20}
                                multiline={false}
                                onChangeText={text => (this.state.criterios = text)}
                                onEndEditing={() => Keyboard.dismiss()}
                                placeholder={'Tareas, Proyectos, Parcial, etc....'}
                                returnKeyType='done'
                                underlineColorAndroid='transparent'
                                defaultValue={this.state.criterios}
                                style={[
                                    styles.listButton,
                                    styles.listButtonSmall,
                                    styles.btn3_5,
                                    styles.txtCenter,
                                    {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                ]}
                            />
                            <TextInput
                                keyboardType='numeric'
                                maxLength={2}
                                multiline={false}
                                onChangeText={text => [
                                    (this.state.criteriosValue = text)
                                ]}
                                onEndEditing={() => Keyboard.dismiss()}
                                placeholder={'10'}
                                returnKeyType='done'
                                underlineColorAndroid='transparent'
                                defaultValue={this.state.criteriosValue}
                                style={[
                                    styles.listButton,
                                    styles.listButtonSmall,
                                    styles.btn3T,
                                    styles.txtCenter,
                                    {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                ]}
                            />
                        </View>
                        <Text
                            style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                        >
                            Defina las ponderaciones
                        </Text>
                        <View style={[styles.widthall, {alignItems: 'center'}]}>
                            <View
                                style={[styles.rowsCalif, {
                                    width: responsiveWidth(52), marginTop: 10, marginBottom: index === 1 ? 10 : null
                                }]}>
                                <TouchableHighlight
                                    underlayColor={'transparent'}
                                    style={{
                                        borderRadius: 10,
                                        alignItems: 'center',
                                        width: responsiveWidth(25),
                                        padding: 6,
                                        backgroundColor: index === 0 ? this.state.thirdColor : this.state.fourthColor
                                    }}
                                    onPress={() => this.btnSelected(0)}>
                                    <Text
                                        style={[styles.textW,
                                            {
                                                fontSize: responsiveFontSize(1.5),
                                                fontWeight: '700',
                                                color: index === 0 ? '#fff' : '#000'
                                            }]}
                                    >
                                        Ponderado
                                    </Text>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    underlayColor={'transparent'}
                                    style={{
                                        borderRadius: 10,
                                        width: responsiveWidth(25),
                                        alignItems: 'center',
                                        padding: 6,
                                        backgroundColor: index === 1 ? this.state.thirdColor : this.state.fourthColor
                                    }}
                                    onPress={() => this.btnSelected(1)}>
                                    <Text
                                        style={[styles.textW,
                                            {
                                                fontSize: responsiveFontSize(1.5),
                                                fontWeight: '700',
                                                color: index === 1 ? '#fff' : '#000'
                                            }]}
                                    >
                                        No ponderado
                                    </Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                        {index === 0 ?
                            <ScrollView
                                overScrollMode={'always'}
                                style={{maxHeight: responsiveHeight(32), marginTop: 10}}
                                showsVerticalScrollIndicator={false}
                            >
                                {this.rndNewArrPond()}
                                <View style={[styles.modalWidth, {alignItems: 'center'}]}>
                                    <Entypo name='circle-with-plus' size={30} color='green' style={{marginTop: 10}}
                                            onPress={() => this.addNewPonderacion()}/>
                                </View>
                            </ScrollView> : null}
                        <View style={[styles.modalWidth, styles.row, {marginBottom: 10}]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.cancelarNew()}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={(index) => this.agregarNew(index)}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </Modal>
                <Text style={[styles.main_title, {color: this.state.thirdColor, paddingTop: 0}]}>
                    Seleccione la materia
                </Text>
                <ModalSelector
                    cancelText='Cancelar'
                    data={data}
                    initValue='Seleccione la materia'
                    onChange={option => this.onChange(option)}
                    optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
                    selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
                    selectStyle={[
                        styles.inputPicker,
                        {borderColor: this.state.secondColor, marginTop: 2}
                    ]}
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione los grupos
                </Text>
                {this.state.grupos.length === 0 ?
                    (<Text
                        style={[styles.widthall, {
                            fontSize: responsiveFontSize(1.5),
                            color: 'grey',
                            marginLeft: 10,
                            textAlign: 'left'
                        }]}>
                        Seleccione primero una materia
                    </Text>) :
                    (<View style={styles.row_v2}>{grupos}</View>)}
                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor}
                    ]}>
                    Seleccione el periodo
                </Text>
                <View style={[styles.row_v2, {paddingTop: 2}]}>{periodos}</View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Define las evidencias
                </Text>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    overScrollMode={'always'}
                >
                    <View style={[{
                        height: Platform.OS === 'ios' ? responsiveHeight(20) : responsiveHeight(15),
                        borderTopWidth: .5,
                        borderBottomWidth: .5,
                        borderWidth: 0,
                        borderRadius: 0,
                        borderColor: 'grey',
                        paddingVertical: 2
                    }]}>
                        {verEn}
                        {this.state.elTotal !== 100 ? (
                            <TouchableOpacity
                                style={[
                                    styles.widthall,
                                    styles.cardEnc,
                                    {backgroundColor: this.state.fourthColor, alignItems: 'center'}
                                ]}
                                onPress={() => this.captEnc()}
                            >
                                <Entypo name='plus' size={32} color='grey'/>
                            </TouchableOpacity>
                        ) : null}
                    </View>
                    <View style={[styles.widthall, {alignItems: 'center'}]}>
                        <View
                            style={[
                                styles.btn4_5,
                                styles.row,
                                styles.cardEnc,
                                {backgroundColor: this.state.fourthColor}
                            ]}
                        >
                            <Text style={[styles.textW, styles.btn3]}>Total</Text>
                            <Text
                                style={[styles.btn8, styles.textW, {textAlign: 'center'}]}>{this.state.elTotal}%</Text>
                        </View>
                    </View>
                    {this.state.cambio === 1 ? (
                        <View style={[styles.widthall, {alignItems: 'center'}]}>
                            <View
                                style={[styles.row,
                                    {marginTop: Platform.OS === 'ios' ? 10 : 5, width: responsiveWidth(75)}
                                ]}>
                                <TouchableOpacity style={[styles.row]}
                                                  onPress={(indxBtn, itmbtn) => this.onListItemPressedPeriodo2(indxBtn, itmbtn)}>
                                    {this.state.checkPeriodoIndex === 1 ? (
                                        <Ionicons name='ios-checkmark-circle' size={17} color='green'
                                                  style={{height: 18, alignItems: 'center', padding: 0}}/>
                                    ) : (
                                        <MaterialIcons name='radio-button-unchecked' size={17} color='black'/>
                                    )}
                                    <Text style={{marginHorizontal: 5}}>Todos los Periodos</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.row]}
                                                  onPress={(indxBtn, itembtn) => this.onListItemPressedGrupo2(indxBtn, itembtn)}>
                                    {this.state.checkBtnsIndex === 1 ? (
                                        <Ionicons name='ios-checkmark-circle' size={17}
                                                  style={{height: 18, alignItems: 'center', padding: 0}} color='green'/>
                                    ) : (
                                        <MaterialIcons name='radio-button-unchecked' size={17} color='black'/>
                                    )}
                                    <Text style={{marginHorizontal: 5}}>Todos los grupos</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    ) : null}
                    {this.state.cambio === 1 ? (
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                {backgroundColor: this.state.secondColor}
                            ]}
                            onPress={() => this.cosasLocas2()}
                        >
                            <Text style={styles.textButton}>Guardar encuadre</Text>
                        </TouchableOpacity>
                    ) : null}
                </ScrollView>
            </View>
        );
    }

    async botonSelected1st(indexBtn, itemBtn) {
        await this.setState({selectedIndex1st: indexBtn, botonSelec1st: itemBtn});
    }

    render() {
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 10 : 0}
                enabled
            >
                <Spinner visible={this.state.visible} textContent='Cargando, Por favor espere...'/>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <MultiBotonRow
                    itemBtns={['Consultar y editar', 'Comentarios']}
                    onSelectedButton={(indexBtn, itemBtn) => this.botonSelected1st(indexBtn, itemBtn)}
                    cantidad={2}
                />
                {this.state.selectedIndex1st === 0 ? this.rendEnc()
                    : (<CorrecionEnc/>)}
            </KeyboardAvoidingView>
        );
    }
}
