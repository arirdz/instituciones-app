import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	StatusBar,
	TextInput,
	TouchableOpacity,
	TouchableHighlight,
	ScrollView,
	KeyboardAvoidingView,
	Keyboard,
	Alert
} from 'react-native';
import {
	responsiveHeight,
	responsiveWidth,
	responsiveFontSize
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import MultiBotonRow from '../Globales/MultiBotonRow';
import ModalSelector from 'react-native-modal-selector';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Switch from 'react-native-switch-pro';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class JustificarFalt extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			alumnos: [],
			elRequest: [],
			fecha1: '',
			aux: 0,
			isDateTimePickerVisible: false,
			data: '',
			elGrado: '',
			elGrupo: '',
			selectedIndexGrupo: -1,
			grupos: [],
			horarios: [],
			laDescripcion: '',
			laMateria: '',
			materias: [],
			tipo: '',
			justif: [],
			faltasJust: [],
			elCiclo: '',
			elPeriodo: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getMaterias();
		await this.getUserdata();
		await this.getCicloActual();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getUserdata() {
		let request = await fetch(this.state.uri + '/api/user/data', {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let dato = await request.json();
		await this.setState({data: dato});
	}

	async requestMultipart() {
		let formData = new FormData();
		for (let i = 0; i < this.state.faltasJust.length; i++) {
			formData.append(
				'update',
				JSON.stringify({
					id_maestro: this.state.data.user_id,
					id_materia: this.state.laMateria,
					id_alumno: this.state.faltasJust[i].id_alumno,
					grupo: this.state.elGrupo,
					ciclo: this.state.elCiclo,
					periodo: this.state.elPeriodo,
					asistencia: '0',
					retardo: '0',
					justificado: this.state.justif[i]
				})
			);
			await fetch(this.state.uri + '/api/justify/asistencia', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				},
				body: formData
			}).then(res => res.json())
				.then(responseJson => {
					this.setState({elRequest: responseJson});
				});
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Asistencias)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('¡Felicidades!', 'Se han guardado las asistencias', [
				{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}
			]);
		}
	}

	async saveAsistencias() {
		Alert.alert('Atencion', '¿Esta seguro que desea guardar?', [
			{
				text: 'Si',
				onPress: () => this.requestMultipart()
			},
			{text: 'No'}
		])
	}

	// ++++++++++++++++++++++++ Ciclo y Periodo Actual ++++++++++++++++++++++++++++
	async getCicloActual() {
		let cicloPicker = await fetch(
			this.state.uri + '/api/ciclo/periodo/actual',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let ciclosPicker = await cicloPicker.json();
		await this.setState({
			elCiclo: ciclosPicker[0].ciclo,
			elPeriodo: ciclosPicker[0].periodo
		});
	}

	//++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
	async getMaterias() {
		let materiapicker = await fetch(this.state.uri + '/api/materias/maestro', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let materiaspicker = await materiapicker.json();
		this.setState({materias: materiaspicker});
	}

	async onChange(option) {
		await this.setState({
			laMateria: option.materia,
			elGrado: option.grado
		});
		await this.getGrupos();
	}

	//+++++++++++++++++++++++++++++++Grupos++++++++++++++++++++++
	async getGrupos() {
		let grupopicker = await fetch(
			this.state.uri + '/api/grupos/materias/' + this.state.laMateria,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let grupospicker = await grupopicker.json();
		await this.setState({grupos: grupospicker});
	}

	async onListItemPressedGrupo(index, grupo) {
		await this.setState({
			selectedIndexGrupo: index,
			elGrupo: grupo
		});
	}

	renderGrupo1(itemGrupo, indexGrupo) {
		let smallButtonStyles = [
			styles.listButton,
			styles.listButtonSmall,
			styles.btn6,
			{borderColor: this.state.secondColor}
		];
		let texto = [styles.textoN];
		if (this.state.selectedIndexGrupo === indexGrupo) {
			smallButtonStyles.push(styles.listButtonSelected, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<TouchableHighlight
				key={indexGrupo + 'gpoJ'}
				style={smallButtonStyles}
				underlayColor={this.state.secondColor}
				onPress={() => this.onListItemPressedGrupo(indexGrupo, itemGrupo.grupo)}
			>
				<View style={styles.listItem}>
					<Text style={texto}>{itemGrupo.grupo}</Text>
				</View>
			</TouchableHighlight>
		);
	}

	renderGrupos() {
		let buttonsGrupos = [];
		this.state.grupos.forEach((itemGrupo, indexGrupo) => {
			buttonsGrupos.push(this.renderGrupo1(itemGrupo, indexGrupo));
		});
		return buttonsGrupos;
	}

	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+--+-+-+-+Justificar falta+-+-+-+-+-+-+-+-+-+-+-+
	async _showDateTimePicker() {
		if (this.state.laMateria === '') {
			Alert.alert('Campo materia vacío', 'Seleccione la materia para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacío', 'Seleccione un grupo para poder continuar', [
				{text: 'Entendido'}
			])
		} else {
			await this.setState({
				isDateTimePickerVisible: true
			});
		}
	}

	async _hideDateTimePicker() {
		await this.setState({isDateTimePickerVisible: false})
	}

	async _handleDatePicked(date) {
		let fecha = moment(date).format('YYYY-MM-DD');
		await this.setState({fecha1: fecha});
		await this._hideDateTimePicker();
		await this.getFaltasJustf();
	}

	async getFaltasJustf() {
		await fetch(
			this.state.uri +
			'/api/get/faltas/fecha/' +
			this.state.laMateria +
			'/' +
			this.state.elGrupo +
			'/' +
			this.state.fecha1,
			{
				method: 'GET',
				headers: {
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(response => {
			return response.json();
		}).then(responseJson => {
			this.setState({faltasJust: responseJson});
		});
		this.state.faltasJust.forEach(() => {
			this.state.justif.push(false);
		});
		await this.setState({aux: 0});
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={tabRow}
			>
				<View style={styles.campoTablaG}>
					<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
						<Text>{itemAlumno.nombre.name}</Text>
						<Switch
							key={indexAlumno}
							value={this.state.justif[indexAlumno]}
							onSyncPress={() => {
								if (this.state.justif[indexAlumno] === false) {
									this.state.justif[indexAlumno] = true;
								} else {
									this.state.justif[indexAlumno] = false;
								}
							}}
						/>
					</View>
				</View>
			</View>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.faltasJust.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	render() {
		let data = this.state.materias.map((item, i) => {
			return {
				grado: item.nombre.grado,
				key: i,
				label: item.nombre.nombre,
				materia: item.nombre.id
			};
		});
		return (
			<View style={styles.container}>

				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione una materia
				</Text>
				<ModalSelector
					cancelText='Cancelar'
					data={data}
					initValue='Seleccione la materia'
					onChange={option => this.onChange(option)}
					optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
					selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
					selectStyle={[
						styles.inputPicker,
						{borderColor: this.state.secondColor, marginTop: 2}
					]}
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor, paddingTop: 4}]}>
					Seleccione los grupos
				</Text>
				{this.state.grupos.length === 0 ?
					(<Text style={{fontSize: responsiveFontSize(1.5), color: 'grey', marginLeft: 10}}>
						Seleccione primero una materia
					</Text>) :
					(<View style={styles.row_v2}>{this.renderGrupos()}</View>)}

				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione una fecha
				</Text>
				<TouchableOpacity
					style={[
						styles.inputPicker,
						{borderColor: this.state.secondColor, marginTop: 2}
					]}
					onPress={() => this._showDateTimePicker()}>
					{this.state.fecha1 === '' ? (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							Seleccione una fecha
						</Text>
					) : (
						<Text style={{fontSize: responsiveFontSize(1.5)}}>
							{moment(this.state.fecha1, 'YYYY-MM-DD').format('DD/MM/YYYY')}
						</Text>
					)}
				</TouchableOpacity>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Alumnos que falta registrada
				</Text>
				<ScrollView>
					<View
						style={[styles.tabla, {borderColor: this.state.secondColor}]}
					>
						{this.renderAlumnos()}
					</View>
				</ScrollView>
				<DateTimePicker
					isVisible={this.state.isDateTimePickerVisible}
					locale={'es'}
					onConfirm={(date) => this._handleDatePicked(date)}
					onCancel={() => this._hideDateTimePicker()}
					titleIOS={'Seleccione una fecha'}
					confirmTextIOS={'Seleccionar'}
					cancelTextIOS={'Cancelar'}
				/>
				<TouchableOpacity
					style={[
						styles.bigButton,
						{backgroundColor: this.state.secondColor}
					]}
					onPress={() => this.saveAsistencias()}
				>
					<Text style={styles.textButton}>Guardar</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
