import React from "react";
import {
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import {Actions} from "react-native-router-flux";
import Entypo from "react-native-vector-icons/Entypo";
import Modal from "react-native-modal";
import MultiBotonRow from "../Globales/MultiBotonRow";
import styles from "../../styles";
import Switch from "react-native-switch-pro";

export default class RevisarTarea extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alumnos: [],
            atiempo: [],
            botonSelected: "",
            comentarios: [],
            dataClass: [],
            dataTareas: [],
            elGrado: "",
            elGrupo: "",
            grupos: [],
            horarios: [],
            indexSelectedTab: 0,
            laDescripcion: "",
            laMateria: "",
            laTarea: "",
            materias: [],
            meh: 0,
            requisitos: [],
            selectedIndexBtn: 0,
            selectedIndexBtnTop: 0,
            tipo: "",
            valor: [],
            value: true
        };
    }

    // ++++++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++
    static bloqueado() {
        Alert.alert("¡No hay tarea!", "No hay tareas que revisar", [
            {
                text: "Entendido"
            }
        ]);
    }

    async componentWillMount() {
        await this.getURL();
        await this.getClaseActual();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let token = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");

        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    // +++++++++++++++++++++ Alumnos grado y grupo +++++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/get/alumnos/by/grado/grupo/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    isATiempo(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <Switch
                            key={itemAlumno.id}
                            value={this.state.atiempo[indexAlumno]}
                            onSyncPress={() => {
                                if (this.state.atiempo[indexAlumno] === false) {
                                    this.state.atiempo[indexAlumno] = true;
                                } else {
                                    this.state.atiempo[indexAlumno] = false;
                                }
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }

    isRequisitos(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <Switch
                            key={itemAlumno.id}
                            value={this.state.requisitos[indexAlumno]}
                            onSyncPress={() => {
                                if (this.state.requisitos[indexAlumno] === false) {
                                    this.state.requisitos[indexAlumno] = true;
                                } else {
                                    this.state.requisitos[indexAlumno] = false;
                                }
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }

    suma(indexAlumno) {
        if (this.state.valor[indexAlumno] < 10) {
            this.state.valor[indexAlumno] = this.state.valor[indexAlumno] + 1;
            this.setState({indexSelectedTab: 1});
            this.setState({indexSelectedTab: 2});
        }
    }

    resta(indexAlumno) {
        if (this.state.valor[indexAlumno] > 0) {
            this.state.valor[indexAlumno] = this.state.valor[indexAlumno] - 1;
            this.setState({indexSelectedTab: 1});
            this.setState({indexSelectedTab: 2});
        }
    }

    isValor(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View style={[styles.miniRow]}>
                        <TouchableOpacity onPress={() => this.resta(indexAlumno)}>
                            <Entypo name="minus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Text>{this.state.valor[indexAlumno]}</Text>
                        <TouchableOpacity onPress={() => this.suma(indexAlumno)}>
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    isComentario(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.setState({isModalVisible: true})}
                        >
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Modal
                            isVisible={this.state.isModalVisible}
                            backdropOpacity={0.5}
                            animationIn={"bounceIn"}
                            animationOut={"bounceOut"}
                            animationInTiming={1000}
                            animationOutTiming={1000}
                        >
                            <View style={[styles.container, {borderRadius: 6}]}>
                                <View style={styles.centered_RT}>
                                    <Text style={[styles.main_title, styles.modalWidth]}>
                                        Agrega un comentario
                                    </Text>
                                </View>
                                <TextInput
                                    keyboardType="default"
                                    maxLength={256}
                                    multiline={true}
                                    onChangeText={text =>
                                        (this.state.comentarios[indexAlumno] = text)
                                    }
                                    returnKeyType="next"
                                    underlineColorAndroid="transparent"
                                    value={this.state.comentarios[indexAlumno]}
                                    style={[
                                        styles.inputComentarios,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <TouchableOpacity
                                    style={[
                                        styles.bigButton,
                                        styles.modalWidth,
                                        {
                                            backgroundColor: this.state.mainColor,
                                            borderColor: this.state.mainColor
                                        }
                                    ]}
                                    onPress={() => this.setState({isModalVisible: false})}
                                >
                                    <Text style={styles.textButton}>Aceptar</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {this.state.indexSelectedTab === 0
                    ? this.isATiempo(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 1
                    ? this.isRequisitos(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 2
                    ? this.isValor(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 3
                    ? this.isComentario(itemAlumno, indexAlumno)
                    : null}
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    // +++++++++++++++++++++++++++++++++ get Clase +++++++++++++++++++++++++++++++
    async getClaseActual() {
        let datoClase = await fetch(this.state.uri + "/api/get/clase/actual", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let datosClase = await datoClase.json();
        this.setState({dataClass: datosClase[0]});
        this.setState({horarios: this.state.dataClass.horarios});
        this.setState({materias: this.state.dataClass.materias});
        this.setState({elGrado: this.state.dataClass.grado});
        this.setState({elGrupo: this.state.dataClass.grupo});
        this.setState({laMateria: this.state.dataClass.materias.id});

        await this.getAlumnosByGG();
        this.state.alumnos.forEach(() => {
            this.state.atiempo.push(true);
            this.state.requisitos.push(true);
            this.state.valor.push(10);
            this.state.comentarios.push(null);
        });
        await this.getTarea();
    }

    renderClass() {
        return (
            <View style={styles.centered_RT}>
                <Text style={styles.main_title}>
                    En este momento usted está asignado a la clase siguiente:
                </Text>
                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor,
                            marginTop: 10
                        }
                    ]}
                >
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}
                    >
                        <Text style={{fontWeight: "800"}}>Materia: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}
                    >
                        <Text style={styles.textCenter}>
                            {/*{this.state.materias.nombre}, {this.state.dataClass.grado}*/}
                            {/*{this.state.dataClass.grupo}*/}
                        </Text>
                    </View>
                </View>
                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {backgroundColor: this.state.fourthColor}
                    ]}
                >
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}
                    >
                        <Text style={{fontWeight: "800"}}>Escuela: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}
                    >
                        <Text>Secundaria, Campus Sur Animas</Text>
                    </View>
                </View>
                <View
                    style={[
                        styles.row,
                        styles.rowInfo,
                        {
                            backgroundColor: this.state.fourthColor,
                            marginBottom: 10
                        }
                    ]}
                >
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}
                    >
                        <Text style={{fontWeight: "800"}}>Hora de clase: </Text>
                    </View>
                    <View
                        style={[
                            styles.listButton,
                            styles.listButtonSmall,
                            styles.colInfo2,
                            {
                                borderColor: this.state.fourthColor,
                                backgroundColor: this.state.fourthColor
                            }
                        ]}
                    >
                        <Text style={styles.textCenter}>aa{/*{this.state.horarios.horas}*/}</Text>
                    </View>
                </View>
            </View>
        );
    }

    // ++++++++++++++++++++++++++++++ get Tareas +++++++++++++++++++++++++++++++++
    async getTarea() {
        let datoTarea = await fetch(
            this.state.uri +
            "/api/get/tarea/" +
            this.state.laMateria +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let datosTareas = await datoTarea.json();
        this.setState({dataTareas: datosTareas[0]});
        this.setState({laTarea: datosTareas[0].id});
    }

    renderTareas() {
        return (
            <View style={styles.centered_RT}>
                <Text style={styles.main_title}>
                    La tarea asignada para esta clase es:
                </Text>
                <Text>{this.state.dataTareas.descripcion}</Text>
                <Text/>
            </View>
        );
    }

    //++++++++++++++++++++++++++++++++ Guardar +++++++++++++++++++++++++++++++++++
    async requestMultipart() {
        try {
            for (let i = 0; i < this.state.alumnos.length; i++) {
                let formData = new FormData();
                formData.append(
                    "new",
                    JSON.stringify({
                        id_tarea: this.state.laTarea,
                        id_alumno: this.state.alumnos[i].id,
                        atiempo: this.state.atiempo[i],
                        requisitos: this.state.requisitos[i],
                        valor: this.state.valor[i],
                        comentarios: this.state.comentarios[i]
                    })
                );
                let request = await fetch(
                    this.state.uri + "/api/revisar/tarea/create",
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "multipart/form-data",
                            Authorization: "Bearer " + this.state.token
                        },
                        body: formData
                    }
                );
            }
            Alert.alert("¡Felicidades!", "Se ha publicado la calificación", [
                {
                    text: "Entendido",
                    onPress: () => Actions.drawer()
                }
            ]);
        } catch (e) {
            console.warn(e);
            Alert.alert("Error", "Intenta más tarde por favor", [
                {
                    text: "Entendido",
                    onPress: () => Actions.drawer()
                }
            ]);
        }
    }

    //++++++++++++++++++++++++++ Selected Button +++++++++++++++++++++++++++++++++
    async botonSelectedTab(indexSelected, itemSelected) {
        await this.setState({
            botonSelectedTab: itemSelected,
            indexSelectedTab: indexSelected
        });
    }

    //+++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++

    render() {
        const tarea = this.renderTareas();
        const alumnos = this.renderAlumnos();
        const clase = this.renderClass();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <KeyboardAvoidingView>
                        {clase}
                        {tarea}

                        <MultiBotonRow
                            itemBtns={[
                                "Hecho a tiempo",
                                "Cumple requisitos",
                                "Asignar un valor",
                                "Añadir comentario"
                            ]}
                            onSelectedButton={(indexBtn, itemBtn) =>
                                this.botonSelectedTab(indexBtn, itemBtn)
                            }
                            cantidad={4}
                        />
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor}]}
                        >
                            {alumnos}
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                {this.state.laTarea !== 0 ? (
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.secondColor,
                                marginBottom: 84
                            }
                        ]}
                        onPress={() => this.requestMultipart()}
                    >
                        <Text style={styles.textButton}>Guardar revisión</Text>
                    </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.secondColor,
                                marginBottom: 84
                            }
                        ]}
                        onPress={() => RevisarTarea.bloqueado()}
                    >
                        <Text style={styles.textButton}>Guardar revisión</Text>
                    </TouchableOpacity>
                )}
            </View>
        );
    }
}
