import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, Text, TextInput, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class AgendarCitamtro extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			selectedIndexDia: -1,
			selectedIndexDia2: -1,
			elNombreD: '',
			elNombreD2: '',
			elDia: '',
			elDia2: '',
			mes: '',
			mes2: '',
			dia: '',
			dia2: '',
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			laHora: '',
			laHora2: '',
			selectedIndexGrupo: -1,
			selectedIndexGrupo2: -1,
			elAlumno: '',
			elAlumno2: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAsunto: '',
			elAsunto2: '',
			dias: [],
			elHijo: '',
			elIdMaestro: '',
			elMaestro: [],
			elReceptor: '',
			eltipo: '',
			selectedIndex: -1,
			horas: [],
			grupos: [],
			elGrupo: '',
			items: {},
			laMateria: '',
			materias: [],
			tipos: [],
			alumnos: [],
			elId: '',
			elId2: ''
		};
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
		await this.getMaterias();
	}

	// +++++++++++++++++++++++++++++++++++++DIAS++++++++++++++++++++++++++++++++++

	async onListItemPressedDia(index, dia, mes, nombreDia, sdia) {
		await this.setState({
			selectedIndexDia: index,
			elNombreD: nombreDia,
			elDia: dia,
			mes: mes,
			dia: sdia,
			selectedIndexHora: -1,
			laHora: ''
		});
		await this.getHoras(this.state.elDia);
	}

	async getDias() {
		let padre = await fetch(this.state.uri + '/api/get/info/padre/' + this.state.elId, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let padres = await padre.json();
		this.setState({padre: padres[0]});
		//+-+-+-+-+-+-+-+-+-+-+-Dias
		await this.setState({dias: []});
		await fetch(
			this.state.uri + '/api/feed/view/dias_dis/' + this.state.padre.id,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar los dias', 'Ha ocurrido un error ' +
						responseJson.error.status_code +
						' al tratar de cargar los dias de disponibilidad si el error continua pónganse en contacto con soporte (Citas)',
						[{text: 'Entendido'}]
					);
				} else {
					if (responseJson.length === 0) {
						this.state.dias.push(
							{dia_atencion: '1'}, {dia_atencion: '2'}, {dia_atencion: '3'}, {dia_atencion: '4'}, {dia_atencion: '5'}
						);
					} else {
						this.setState({dias: responseJson});
					}
				}
			});
	}

	renderDia(itemDia, indexDia) {
		let a = this.state.dias.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexDia !== a) {
			tabRow.push({borderColor: this.state.fourthColor});
		}
		let smallButtonStyles = [
			styles.rowTabla,
			styles.modalWidth,
			{borderColor: this.state.fourthColor}
		];
		let texto = [styles.textoN];
		if (this.state.selectedIndexDia === indexDia) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		let nombreDia = '';
		let num = parseInt(itemDia.dia_atencion) - 1;
		let sdia = 1;
		let mes = 0;
		if (num < moment().weekday()) {
			sdia = moment().weekday(num + 7);
			mes = sdia.format('MMMM');
			sdia = sdia.format('D');
		} else {
			sdia = moment().weekday(num);
			mes = sdia.format('MMMM');
			sdia = sdia.format('D');
		}
		if (itemDia.dia_atencion === '1') {
			nombreDia = 'Lunes';
		}
		if (itemDia.dia_atencion === '2') {
			nombreDia = 'Martes';
		}
		if (itemDia.dia_atencion === '3') {
			nombreDia = 'Miércoles';
		}
		if (itemDia.dia_atencion === '4') {
			nombreDia = 'Jueves';
		}
		if (itemDia.dia_atencion === '5') {
			nombreDia = 'Viernes';
		}
		return (
			<TouchableHighlight
				key={indexDia}
				underlayColor={this.state.secondColor}
				style={[tabRow, smallButtonStyles]}
				onPress={() =>
					this.onListItemPressedDia(indexDia, itemDia.dia_atencion, nombreDia, mes, sdia)
				}
			>
				<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
					<Text style={[texto, {marginRight: 25}]}>
						{nombreDia} {sdia} {mes}
					</Text>
				</View>
			</TouchableHighlight>
		);
	}


	renderDias() {
		let buttonsDias = [];
		this.state.dias.forEach((itemDia, indexDia) => {
			buttonsDias.push(this.renderDia(itemDia, indexDia));
		});
		return buttonsDias;
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++HORAS+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	async getDataUser() {
		let nreceptor = await fetch(
			this.state.uri + '/api/datos/user/' + this.state.elReceptor,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let receptor = await nreceptor.json();
		await this.setState({
			info:
				receptor[0].nombre +
				' ' +
				receptor[0].apellido_paterno +
				' ' +
				receptor[0].apellido_materno
		});
	}

	showInfo() {
		return (
			<View style={{justifyContent: 'center'}}>
				<Text style={{fontWeight: '900', textAlign: 'center'}}>
					{this.state.info}
				</Text>
			</View>
		);
	}

	//+-+-+-+-+-+-+--+-+-+Get horas
	async getHoras(dia) {
		await fetch(this.state.uri + '/api/feed/view/horarios_at/' + this.state.padre.id + '/' + dia,
			{
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en horas de atencion',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					if (responseJson.length === 0) {
						this.getHorasTodas();
					} else {
						this.setState({horas: responseJson});
					}
				}
			});
	}

	async getHorasTodas() {
		await fetch(this.state.uri + '/api/feed/view/horas/disponibles', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en horas de atencion',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					this.setState({horas: responseJson});
				}
			});
	}

	async onListItemPressedHora(index, hora) {
		await this.setState({selectedIndexHora: index, laHora: moment(hora, 'HH:mm:ss').format('HH:mm')});
	}

	renderHora(itemHora, indexHora) {
		let a = this.state.horas.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexHora !== a) {
			tabRow.push({borderColor: this.state.fourthColor});
		}
		let smallButtonStyles = [
			styles.rowTabla,
			styles.modalWidth,
			{borderColor: this.state.fourthColor}
		];
		let texto = [styles.textoN];
		if (this.state.selectedIndexHora === indexHora) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<TouchableHighlight
				key={indexHora}
				underlayColor={this.state.secondColor}
				style={[tabRow, smallButtonStyles]}
				onPress={() => this.onListItemPressedHora(indexHora, itemHora.horas2.inicio)}
			>
				<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
					<Text style={[texto, {marginRight: 25}]}>{itemHora.horas2.horas}</Text>
				</View>
			</TouchableHighlight>
		);
	}

	renderHoras() {
		let buttonsHoras = [];
		this.state.horas.forEach((itemHora, indexHora) => {
			buttonsHoras.push(this.renderHora(itemHora, indexHora));
		});
		return buttonsHoras;
	}

	// ++++++++++++++++++++++++++++++++++++++GUARDAR++++++++++++++++++++++++++++++

	async requestMultipart() {
		if (this.state.elReceptor2 === '' || this.state.elDia2 === '' || this.state.elAsunto2 === '' || this.state.laHora2 === '') {
			Alert.alert('Datos incompletos', 'Asegurece de llenar todos los campos para enviar la cita')
		} else {
			let formData = new FormData();
			formData.append(
				'new',
				JSON.stringify({
					id_receptor: this.state.elReceptor2,
					padre_id: 0,
					hijo_id: this.state.elId2,
					fecha_cita: this.state.dia2 + ' de ' + this.state.mes2,
					hora: this.state.laHora2,
					asunto: this.state.elAsunto2,
					aprobada: 0
				})
			);
			await fetch(this.state.uri + '/api/feed/citas', {
				method: 'POST', headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}, body: formData
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error al enviar cita', 'Ha ocurrido un error ' + responseJson.error.status_code + ' al tratar de enviar la cita si el error continua pónganse en contacto con soporte (Solicitar cita)');
					} else {
						Alert.alert('Solicitud de cita enviada correctamente', 'se ha notificado a el destinatario', [
							{text: 'Entendido', onPress: () => Actions.HomePage()}
						]);
					}
				});
		}
	}

	//++++++++++++++++++++++++Alumnos
	async getAlumnosByGG() {
		let alumnopicker = await fetch(
			this.state.uri +
			'/api/user/alumnos/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let alumnospicker = await alumnopicker.json();
		this.setState({alumnos: alumnospicker});
	}

	async onListPressedAlumno(indexAlumno, itm) {
		await this.setState({
			selectedIndexAlumno: indexAlumno,
			elAlumno: itm.name,
			elReceptor: itm.padre_id,
			elId: itm.id,
			elAsunto: '',
			elAsunto2: '',
			elNombreD: '',
			elNombreD2: '',
			elDia: '',
			elDia2: '',
			mes: '',
			mes2: '',
			dia: '',
			dia2: '',
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			laHora: '',
			laHora2: ''
		});
		await this.getDias();
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.fourthColor});
		}
		let smallButtonStyles = [
			styles.rowTabla,
			styles.modalWidth,
			{borderColor: this.state.fourthColor}
		];
		let texto = [styles.textoN];
		if (this.state.selectedIndexAlumno === indexAlumno) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<TouchableHighlight
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={[tabRow, smallButtonStyles]}
				onPress={() =>
					this.onListPressedAlumno(indexAlumno, itemAlumno)
				}
			>
				<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
					<Text style={[texto, {marginRight: 25}]}>{itemAlumno.name}</Text>
				</View>
			</TouchableHighlight>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	//++++++++++++++++++++++++++++++++++GRUPOS
	async getGrupos() {
		let grupopicker = await fetch(
			this.state.uri + '/api/grupos/materias/' + this.state.laMateria,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let grupospicker = await grupopicker.json();
		this.setState({grupos: grupospicker});
	}

	async onListItemPressedGrupo(index, grupo) {
		await this.setState({
			selectedIndexGrupo: index,
			elGrupo: grupo,
			selectedIndexDia: -1,
			selectedIndexDia2: -1,
			elNombreD: '',
			elNombreD2: '',
			elDia: '',
			elDia2: '',
			mes: '',
			mes2: '',
			dia: '',
			dia2: '',
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			laHora: '',
			laHora2: '',
			selectedIndexGrupo2: -1,
			elAlumno: '',
			elAlumno2: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAsunto: '',
			elAsunto2: ''
		});
		await this.getAlumnosByGG();
	}

	renderGrupo(itemGrupo, indexGrupo) {
		let smallButtonStyles = [
			styles.listButton,
			styles.listButtonSmall,
			styles.btn6,
			{borderColor: this.state.secondColor}
		];
		let texto = [styles.textoN];

		if (this.state.selectedIndexGrupo === indexGrupo) {
			smallButtonStyles.push(styles.listButtonSelected, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<TouchableHighlight
				key={indexGrupo}
				style={smallButtonStyles}
				underlayColor={this.state.secondColor}
				onPress={() => this.onListItemPressedGrupo(indexGrupo, itemGrupo.grupo)}
			>
				<View style={styles.listItem}>
					<Text style={texto}>{itemGrupo.grupo}</Text>
				</View>
			</TouchableHighlight>
		);
	}

	renderGrupos() {
		let buttonsGrupos = [];
		this.state.grupos.forEach((itemGrupo, indexGrupo) => {
			buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
		});
		return buttonsGrupos;
	}

	//++++++++++++++++++++++++++++++++MATERIAS
	async getMaterias() {
		let materiapicker = await fetch(this.state.uri + '/api/materias/maestro', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let materiaspicker = await materiapicker.json();
		this.setState({materias: materiaspicker});
	}

	async onChange(option) {
		await this.setState({
			laMateria: option.materia,
			elGrado: option.grado,
			elGrupo: '',
			selectedIndexDia: -1,
			selectedIndexDia2: -1,
			elNombreD: '',
			elNombreD2: '',
			elDia: '',
			elDia2: '',
			mes: '',
			mes2: '',
			dia: '',
			dia2: '',
			selectedIndexHora: -1,
			selectedIndexHora2: -1,
			laHora: '',
			laHora2: '',
			selectedIndexGrupo: -1,
			selectedIndexGrupo2: -1,
			elAlumno: '',
			elAlumno2: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAsunto: '',
			elAsunto2: ''
		});
		await this.getGrupos();
	}

	//*********************************** Render +++++++++++++++++++++++++++++++++

	async alertCita() {
		Alert.alert('Solicitar cita', 'Está a punto de agender una cita\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.requestMultipart()},
			{text: 'No'}
		])
	}

//-+-+-+-+-+-+-+-+-+-+-+-+-Alumnos
	async listaAlumn() {
		if (this.state.laMateria === '') {
			Alert.alert('Campo materia vacío', 'Seleccione una materia para continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacío', 'Seleccione un grupo para continuar', [
				{text: 'Enterado'}
			])
		} else {
			this.state.elAlumno = this.state.elAlumno2;
			this.state.elId = this.state.elId2;
			this.state.elReceptor = this.state.elReceptor2;
			this.state.selectedIndexAlumno = this.state.selectedIndexAlumno2;
			await this.setState({isModalCateg: true});
		}
	}

	async closeListAlumn() {
		if (this.state.selectedIndexAlumno === -1) {
			Alert.alert('Alumno no seleccionado', 'Seleccione un alumno para poder continuar', [{text: 'Entendido'}]);
		} else {
			this.state.elAlumno2 = this.state.elAlumno;
			this.state.elId2 = this.state.elId;
			this.state.elReceptor2 = this.state.elReceptor;
			this.state.selectedIndexAlumno2 = this.state.selectedIndexAlumno;
			await this.setState({isModalCateg: false});
		}
	}

//+-+-+-+-+-+-+-+-+Asuntos
	async openModalDia() {
		if (this.state.selectedIndexAlumno2 === -1) {
			Alert.alert('Alumno no seleccionado', 'Seleccione un alumno para poder continuar', [{text: 'Entendido'}]);
		} else {
			this.state.selectedIndexHora = this.state.selectedIndexHora2;
			this.state.laHora = this.state.laHora2;
			this.state.elAsunto = this.state.elAsunto2;
			this.state.selectedIndexDia = this.state.selectedIndexDia2;
			this.state.elDia = this.state.elDia2;
			this.state.elNombreD = this.state.elNombreD2;
			this.state.mes = this.state.mes2;
			this.state.dia = this.state.dia2;
			await this.setState({isModalDia: true});
		}
	}

	async closeModalDia() {
		if (this.state.elAsunto === '') {
			Alert.alert('Campo asunto vacío', 'Asegúrese de llenar todos los campos para poder continuar', [{text: 'Enterado'}])
		} else if (this.state.selectedIndexDia === -1) {
			Alert.alert('Campo día vacío', 'Asegúrese de llenar todos los campos para poder continuar', [{text: 'Enterado'}])
		} else if (this.state.selectedIndexHora === -1) {
			Alert.alert('Campo hora vacío', 'Asegúrese de llenar todos los campos para poder continuar', [{text: 'Enterado'}])
		} else {
			this.state.selectedIndexHora2 = this.state.selectedIndexHora;
			this.state.laHora2 = this.state.laHora;
			this.state.elAsunto2 = this.state.elAsunto;
			this.state.selectedIndexDia2 = this.state.selectedIndexDia;
			this.state.elDia2 = this.state.elDia;
			this.state.elNombreD2 = this.state.elNombreD;
			this.state.mes2 = this.state.mes;
			this.state.dia2 = this.state.dia;
			await this.setState({isModalDia: false})
		}
	}

	render() {
		const alumnosList = this.renderAlumnos();
		const dias = this.renderDias();
		const horarios = this.renderHoras();
		const grupos = this.renderGrupos();
		let data = this.state.materias.map((item, i) => {
			return {
				grado: item.nombre.grado,
				key: i,
				label: item.nombre.nombre,
				materia: item.nombre.id
			};
		});
		return (
			<View style={styles.container}>
				<Modal
					isVisible={this.state.isModalCateg}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione el alumno:
						</Text>
						<ScrollView showsVerticalScrollIndicator={false} style={{marginTop: 10}}>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}
							>
								{alumnosList}
							</View>
						</ScrollView>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalCateg: false})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.closeListAlumn()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModalDia}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Escriba el asunto:
						</Text>
						<TextInput
							keyboardType='default'
							maxLength={254}
							onChangeText={text => (this.state.elAsunto = text)}
							defaultValue={this.state.elAsunto}
							placeholder={'Asunto a tratar'}
							returnKeyType='next'
							underlineColorAndroid='transparent'
							style={[
								styles.inputPicker,
								styles.modalWidth,
								{borderColor: this.state.secondColor}
							]}
						/>
						<Text
							style={[
								styles.main_title,
								styles.modalWidth,
								{color: this.state.thirdColor}
							]}
						>
							Seleccione el día:
						</Text>
						<ScrollView
							showsVerticalScrollIndicator={false}
							style={[
								styles.tabla,
								{borderColor: this.state.secondColor, height: responsiveHeight(20)}
							]}
						>
							{dias}
						</ScrollView>
						<Text
							style={[
								styles.main_title,
								styles.modalWidth,
								{color: this.state.thirdColor}
							]}
						>
							Seleccione una hora
						</Text>
						{this.state.selectedIndexDia !== -1 ? (
							<ScrollView
								showsVerticalScrollIndicator={false}
								style={[styles.tabla, {
									borderColor: this.state.secondColor,
									height: responsiveHeight(20)
								}]}
							>
								{horarios}
							</ScrollView>
						) : null}
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalDia: false})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.closeModalDia()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<ScrollView showsVerticalScrollIndicator={false}>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione la materia
					</Text>
					<ModalSelector
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
						selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
						data={data}
						onChange={option => this.onChange(option)}
						selectStyle={[
							styles.inputPicker,
							{borderColor: this.state.secondColor}
						]}
						initValue='Seleccione la materia'
					/>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione un grupo
					</Text>
					{this.state.grupos.length === 0 ?
						(<Text style={{fontSize: responsiveFontSize(1.5), color: 'grey', marginLeft: 10}}>
							Seleccione primero una materia
						</Text>) :
						(<View style={styles.row_v2}>{grupos}</View>)}
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione el alumno
					</Text>
					<View style={{
						alignItems: 'center',
						backgroundColor: this.state.fourthColor,
						padding: 15,
						borderRadius: 6
					}}>
						<TouchableOpacity
							style={[
								styles.bigButton,
								styles.btnShowMod,
								{
									marginTop: 0,
									borderColor: this.state.secondColor,
									backgroundColor: '#fff'
								}
							]}
							onPress={() => this.listaAlumn()}
						>
							<Text>Ver lista</Text>
						</TouchableOpacity>
						<Text>Alumno seleccionado:</Text>
						<Text style={[styles.textButton, {color: 'black'}]}>
							{this.state.elAlumno2}
						</Text>
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Asuntos y fecha
					</Text>
					<View style={{
						alignItems: 'center',
						backgroundColor: this.state.fourthColor,
						padding: 15,
						borderRadius: 6
					}}>
						<TouchableOpacity
							style={[
								styles.bigButton,
								styles.btnShowMod,
								{
									marginTop: 0,
									borderColor: this.state.secondColor,
									backgroundColor: '#fff'
								}
							]}
							onPress={() => this.openModalDia()}
						>
							<Text>Ver horarios y asuntos</Text>
						</TouchableOpacity>
						<Text>Asunto seleccionado:</Text>
						<Text style={[styles.textButton, {color: 'black'}]}>
							{this.state.elAsunto2}
						</Text>
						<View style={{flexDirection: 'row', marginTop: 5}}>
							<Text style={{fontSize: responsiveFontSize(1.8)}}>Para el día </Text>
							<Text style={[styles.textW, {color: 'black', fontSize: responsiveFontSize(1.7)}]}>
								{this.state.mes2} {this.state.dia2} {this.state.elNombreD2}
							</Text>
							<Text style={{fontSize: responsiveFontSize(1.8)}}> a las </Text>
							<Text style={[styles.textW, {color: 'black', fontSize: responsiveFontSize(1.7)}]}>
								{this.state.laHora2}
								<Text style={{fontWeight: '500', fontSize: responsiveFontSize(1.8)}}> hrs</Text>
							</Text>
						</View>
					</View>
					<TouchableOpacity
						style={[
							styles.bigButton,
							{backgroundColor: this.state.secondColor}
						]}
						onPress={() => this.alertCita()}
					>
						<Text style={styles.textButton}>Enviar solicitud</Text>
					</TouchableOpacity>
				</ScrollView>

			</View>
		);
	}
}
