import React from 'react';
import {Alert, AsyncStorage, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Modal from 'react-native-modal';
import {Actions} from 'react-native-router-flux';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class CorrecionEnc extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            encuadres: [],
            isModalRest: [],
            comentarios: [],
            materias: [],
            materia: [],
            lasMaterias: [],
            correcion: [],
            id_materia: [],

            aux: 0,
            id: []
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getUserdata();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async openModal(i) {
        this.state.isModalRest[i] = true;
        this.setState({aux: 0});
    }

    async getUserdata() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(uri + '/api/user/data', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + token
            }
        });
        let dato = await request.json();
        await this.setState({datos: dato});
        await this.setState({id: this.state.datos.user_id});
        await this.getEncuadre();
    }


    async closeModal(i) {
        this.state.isModalRest[i] = false;
        this.setState({aux: 0});
    }


    ////+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-Encuadres+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    async getEncuadre() {
        await fetch('http://127.0.0.1:8000/api/get/encuadre/comentado/' + this.state.id, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar encuadre',
                        'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Encuadres)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({encuadres: responseJson});
                }
            });

    }

    async corregir(item) {
        Actions.correccionEncPt2({
            corregir: item.encuadre,
            materia: item.materia.nombre,
            id_materia: item.materia.id,
            grado: item.grado,
            grupo: item.grupo
        });
    }

    correcciones() {
        let btnCorrecion = [];
        if (this.state.encuadres.length !== 0) {
            this.state.encuadres.forEach((itm, i) => {
                btnCorrecion.push(
                    <View key={i + 'c'}>
                        <Modal
                            isVisible={this.state.isModalRest[i]}
                            backdropOpacity={0.8}
                            animationIn={'bounceIn'}
                            animationOut={'bounceOut'}
                            animationInTiming={1000}
                            animationOutTiming={1000}
                        >
                            <View
                                style={[styles.container, {borderRadius: 6, flex: 0}]}
                            >
                                <Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                                    Comentarios
                                </Text>
                                <Text
                                    style={[styles.modalWidth, {marginTop: 10, textAlign: 'left', fontWeight: '700'}]}>
                                    Comentarios hechos de la materia:{'\n'}
                                    <Text style={{textAlign: 'left', fontWeight: '400'}}>
                                        {itm.materia.nombre}
                                    </Text>
                                </Text>
                                <Text
                                    style={[styles.modalWidth, {textAlign: 'left', fontWeight: '700', marginTop: 10}]}>
                                    Del grado y grupo:{' '}
                                    <Text style={{textAlign: 'left', fontWeight: '400'}}>
                                        "{itm.grado} {itm.grupo}"
                                    </Text>
                                </Text>
                                <Text
                                    style={[styles.modalWidth, {marginTop: 10, textAlign: 'left', fontWeight: '700'}]}>
                                    Los comentarios son los siguientes:
                                </Text>
                                <View style={{height: responsiveHeight(28)}}>
                                    {itm.director !== '' ? (
                                        <View>
                                            <Text style={[styles.modalWidth, styles.textW, {marginTop: 10}]}>
                                                Director:
                                            </Text>
                                            <Text style={[styles.modalWidth]}>
                                                {itm.director}
                                            </Text>
                                        </View>
                                    ) : null}
                                    {itm.subdirector !== '' ? (
                                        <View>
                                            <Text style={[styles.modalWidth, styles.textW, {marginTop: 6}]}>
                                                Sub director:
                                            </Text>
                                            <Text style={[styles.modalWidth]}>
                                                {itm.subdirector}
                                            </Text>
                                        </View>
                                    ) : null}
                                    {itm.cord_academico !== '' ? (
                                        <View>
                                            <Text style={[styles.modalWidth, styles.textW, {marginTop: 6}]}>
                                                Coordinador académico:
                                            </Text>
                                            <Text style={[styles.modalWidth]}>
                                                {itm.cord_academico}
                                            </Text>
                                        </View>
                                    ) : null}
                                </View>
                                <View style={[styles.modalWidth, styles.row]}>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                        ]}
                                        onPress={() => [this.closeModal(i)]}
                                    >
                                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {
                                                backgroundColor: this.state.secondColor,
                                                borderColor: this.state.secondColor
                                            }
                                        ]}
                                        onPress={() => [this.corregir(itm), this.closeModal(i)]}
                                    >
                                        <Text style={[styles.textW, {color: '#fff'}]}>Corregir encuadre</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                        <View
                            style={[styles.widthall, styles.row, {
                                paddingVertical: 8, paddingHorizontal: 15, borderTopWidth: .25
                            }]}
                        >
                            <Text
                                numberOfLines={1}
                                style={[styles.btn2_3, {
                                    color: 'black',
                                    textAlign: 'left',
                                    fontSize: responsiveFontSize(1.5)
                                }]}
                            >
                                {itm.materia.nombre}{'  '}
                            </Text>
                            <TouchableOpacity
                                style={[styles.btn4, {
                                    backgroundColor: this.state.secondColor, padding: 4, borderRadius: 6
                                }]}
                                onPress={() => this.openModal(i)}
                            >
                                <Text style={{textAlign: 'center', color: 'white'}}>Ver</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            });
        }
        return btnCorrecion;
    }

    render() {
        return (<View style={styles.container}>
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                Seleccione la materia
            </Text>
            {this.state.encuadres.length === 0 ? <Text style={[styles.main_title, {
                color: '#a8a8a8',
                textAlign: 'center',
                marginTop: responsiveHeight(10),
                fontSize: responsiveFontSize(3)
            }]}>Sin encuadres comentados
            </Text> : <ScrollView>
                {this.correcciones()}
                <View style={[styles.widthall, {borderBottomWidth: .5}]}/>
            </ScrollView>}
        </View>);
    }
}
