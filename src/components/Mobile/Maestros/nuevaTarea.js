import React from 'react';
import {
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import CheckBox from 'react-native-checkbox';
import styles from '../../styles';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');
export default class nuevaTarea extends React.Component {
    handleContenido = text => {
        this.setState({laDescripcion: text});
    };

    constructor(props) {
        super(props);
        this.state = {
            urlGrupo: '',
            checkeds: [],
            color1: '',
            color2: '',
            elCiclo: '',
            elDia: '',
            elDiaA: '',
            elDiaB: '',
            elPeriodo: '',
            grado: '',
            grupos: [],
            laDescripcion: '',
            laMateria: '',
            materias: [],
            selectedIndexBtn: 0,
            selectedIndexCheckds: [],
            selectedIndexGrupo: -1,
            tipo: '',
            tipos: ['Mañana', 'Sig. clase', 'Otra fecha']
        };
    }

    static fecha() {
        let hoyD = moment().format('dddd' + ' DD');
        let hoyA = moment().format('MMMM ' + 'YYYY');
        return hoyD + ' de ' + hoyA;
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let manana = moment()
            .add(1, 'day')
            .format('DD' + '/' + 'MM' + '/' + 'YYYY');

        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            color1: secondColor,
            color2: '#fff',
            elDia: manana
        });

        await this.getMaterias();
        await this.getCiclosPeriodos();
    }

    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + '/api/materias/maestro', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        this.setState({
            selectedIndexAlumno: -1,
            grado: option.grado,
            selectedIndexGrupo: -1,
            elDiaA: '',
            selectedIndexCheckds: [],
            checkeds: []
        });
        await this.getGrupos(option.materia);
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            this.state.checkeds.push(false);
            this.state.selectedIndexCheckds.push(-1);
        });
    }

    // +++++++++++++++++++++++++++++++ CICLOS ++++++++++++++++++++++++++++++++++++
    async getCiclosPeriodos() {
        let losciclos = await fetch(this.state.uri + '/api/ciclo/periodo/actual', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let ciclosss = await losciclos.json();
        this.setState({elCiclo: ciclosss[0].ciclo});
        this.setState({elPeriodo: ciclosss[0].periodo});
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++ SAVE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async getSigClaseA(gp) {
        let clasedia = await fetch(
            this.state.uri +
            '/api/get/siguientes/clases/' +
            this.state.laMateria +
            '/' +
            gp,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );

        let clasedias = await clasedia.json();
        this.setState({elDiaB: clasedias[0].dia});

        let sdia = 0;
        let mes = 0;

        let num = this.state.elDiaB - 1;
        if (num <= moment().weekday()) {
            sdia = moment().weekday(num + 7);
            mes = sdia.format('MM');
            sdia = sdia.format('DD');
        } else {
            sdia = moment().weekday(num);
            mes = sdia.format('MM');
            sdia = sdia.format('DD');
        }
        let year = moment().format('YYYY');

        this.setState({
            elDiaA: sdia + '/' + mes + '/' + year
        });
    }

    async requestMultipart() {
        let gp = 0;
        let i = 0;

        try {
            while (i < this.state.grupos.length) {
                if (this.state.checkeds[i] === true) {
                    gp = this.state.grupos[i].grupo;
                    if (this.state.selectedIndexCheckds[i] === 0) {
                        let formData = new FormData();
                        formData.append(
                            'new',
                            JSON.stringify({
                                id_materia: this.state.laMateria,
                                grupo: gp,
                                descripcion: this.state.laDescripcion,
                                fecha_entrega: this.state.elDia,
                                ciclo: this.state.elCiclo,
                                periodo: this.state.elPeriodo
                            })
                        );
                        let request = await fetch(this.state.uri + '/api/tarea/create', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'multipart/form-data',
                                Authorization: 'Bearer ' + this.state.token
                            },
                            body: formData
                        });
                    }

                    if (this.state.selectedIndexCheckds[i] === 1) {
                        await this.getSigClaseA(gp);
                        let formData = new FormData();
                        formData.append(
                            'new',
                            JSON.stringify({
                                id_materia: this.state.laMateria,
                                grupo: gp,
                                descripcion: this.state.laDescripcion,
                                fecha_entrega: this.state.elDiaA,
                                ciclo: this.state.elCiclo,
                                periodo: this.state.elPeriodo
                            })
                        );
                        let request = await fetch(this.state.uri + '/api/tarea/create', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'multipart/form-data',
                                Authorization: 'Bearer ' + this.state.token
                            },
                            body: formData
                        });
                    }
                }
                i = i + 1;
            }

            Alert.alert('¡Felicidades!', 'Se ha publicado la tarea con éxito', [
                {
                    text: 'Entendido',
                    onPress: () => Actions.drawer()
                }
            ]);
        } catch (e) {
            console.warn(e);
            Alert.alert('Error', 'Intenta más tarde por favor', [
                {
                    text: 'Entendido',
                    onPress: () => Actions.drawer()
                }
            ]);
        }
        this.setState({descripcion: ''});
    }

    async getGrupos(materia) {
        let grupopicker = await fetch(
            this.state.uri + '/api/grupos/materias/' + materia,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let grupospicker = await grupopicker.json();
        this.setState({grupos: grupospicker});
        this.setState({laMateria: materia});
    }

    renderGrupo(itemGrupo, indexGrupo) {
        const dia = this.renderTipos(indexGrupo);
        return (
            <View style={styles.grups}>
                <CheckBox
                    key={indexGrupo}
                    label={itemGrupo.grupo}
                    onChange={checked => this.onChangeChk(indexGrupo, checked)}
                />
                {dia}
            </View>
        );
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++ TIPOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    async onChangeChk(indexGrupo, checked) {
        this.state.checkeds[indexGrupo] = !checked;
    }

    onListItemPressedTipo(index, tipo, gp) {
        this.state.selectedIndexCheckds[gp] = index;
    }

    renderTipo(itemTipo, indexTipo, gp) {
        let keys = indexTipo + '' + gp;
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn4,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];
        for (let i = 0; i < 6; i++) {
            if (this.state.selectedIndexCheckds[i] + '' + gp === keys) {
                smallButtonStyles.push(styles.listButtonSelected, styles.lessPadding, {
                    backgroundColor: this.state.secondColor
                });
                texto.push(styles.textoB);
            }
        }

        return (
            <TouchableHighlight
                key={keys}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() =>
                    this.onListItemPressedTipo(indexTipo, itemTipo + gp, gp)
                }>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemTipo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderTipos(gp) {
        let buttonsTipos = [];
        this.state.tipos.forEach((itemTipo, indexTipo) => {
            buttonsTipos.push(this.renderTipo(itemTipo, indexTipo, gp));
        });
        return buttonsTipos;
    }

    //++++++++++++++++++++++++++++++ render +++++++++++++++++++++++++++++++
    render() {
        let data = this.state.materias.map((item, i) => {
            return {
                key: i,
                materia: item.nombre.id,
                grado: item.nombre.grado,
                label: item.nombre.nombre + ' ' + item.nombre.grado
            };
        });
        const hoy = nuevaTarea.fecha();
        const grupos = this.renderGrupos();
        return (
            <View style={styles.container} behavior="position">
                <View
                    style={[styles.row_RT, {backgroundColor: this.state.fourthColor}]}>
                    <Text style={styles.blanco}>{hoy}</Text>
                </View>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <KeyboardAvoidingView>
                        <Text style={styles.main_title}>Seleccione la materia</Text>
                        <View style={styles.row}>
                            <View>
                                <Text style={styles.label}>Campus</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>01</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Nivel</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>SEC</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Materia</Text>
                                <ModalSelector
                                    cancelText="Cancelar"
                                    data={data}
                                    initValue="Seleccione la materia"
                                    onChange={option => this.onChange(option)}
                                    optionTextStyle={{color: this.state.thirdColor}}
                                    selectStyle={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn3_5,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                            </View>
                        </View>
                        <View>{grupos}</View>

                        <Text style={styles.main_title}>Descripción de la tarea</Text>
                        <View style={styles.form}>
                            <TextInput
                                keyboardType="default"
                                onEndEditing={() => Keyboard.dismiss()}
                                returnKeyType="done"
                                multiline={true}
                                underlineColorAndroid="transparent"
                                onChangeText={this.handleContenido}
                                style={[
                                    styles.inputContenido,
                                    {borderColor: this.state.secondColor}
                                ]}
                            />
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: this.state.secondColor
                        }
                    ]}
                    onPress={() => this.requestMultipart()}>
                    <Text style={styles.textButton}>Guardar Tarea</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
