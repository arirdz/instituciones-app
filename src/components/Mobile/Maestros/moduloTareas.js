import React from 'react';
import {
    AsyncStorage,
    KeyboardAvoidingView,
    StatusBar,
    Text,
    View
} from 'react-native';
import styles from '../../styles';
import MultiBotonRow from '../Globales/MultiBotonRow';
import EditarTarea from './EditarTarea';
import RevisarTarea from './RevisarTarea';
import AcumuladosTareas from './AcumuladosTareas';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class moduloTareas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            indexSelected: 0
        };
    }

    static fecha() {
        let hoyD = moment().format('dddd' + ' DD');
        let hoyA = moment().format('MMMM ' + 'YYYY');
        return hoyD + ' de ' + hoyA;
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');

        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async componentWillMount() {
        await this.getURL();
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    renderView() {
        return (
            <View>
                {this.state.indexSelected === 0 ? <RevisarTarea/> : null}
                {this.state.indexSelected === 1 ? <EditarTarea/> : null}
                {this.state.indexSelected === 2 ? <AcumuladosTareas/> : null}
            </View>
        );
    }

    render() {
        const hoy = moduloTareas.fecha();
        const laView = this.renderView();
        return (
            <View style={styles.container} behavior="position">
                <View
                    style={[styles.row_RT, {backgroundColor: this.state.fourthColor}]}>
                    <Text style={styles.blanco}>{hoy}</Text>
                </View>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <View style={styles.row}>
                    <MultiBotonRow
                        itemBtns={['Nuevo registro', 'Editar anterior', 'Ver acumulados']}
                        onSelectedButton={(indexBtn, itemBtn) =>
                            this.botonSelected(indexBtn, itemBtn)
                        }
                        cantidad={3}
                    />
                </View>
                <KeyboardAvoidingView>{laView}</KeyboardAvoidingView>
            </View>
        );
    }
}
