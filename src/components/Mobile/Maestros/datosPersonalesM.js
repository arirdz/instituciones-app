import React from 'react';
import {
	Alert,
	AsyncStorage,
	Image,
	KeyboardAvoidingView,
	ScrollView,
	StatusBar,
	Text,
	TouchableOpacity,
	View
} from 'react-native';
import {
	responsiveWidth
} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Fumi} from 'react-native-textinput-effects';
import MultiBotonRow from '../Globales/MultiBotonRow';
import Modal from 'react-native-modal';
import {TextInputMask} from 'react-native-masked-text';

export default class datosPersonalesM extends React.Component {
	_showModal = () => this.setState({isModalVisible: true});
	_hideModal = () => this.setState({isModalVisible: false});

	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			data: [],
			datos: [],
			aux: 0,
			isModalVisible: false,
			botonSelected: 'Generales',
			indexSelected: -1,
			maincolor: '#fff',
			secondColor: '#fff',
			thirdColor: '#fff',
			fourthColor: '#fff',
			elRequest: [],
			datoFaltantes: []
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
		this.getUserdata();
		this.getUserdataV2();
	}

	async botonSelected(indx, itm) {
		await this.setState({
			botonSelected: itm,
			indexSelected: indx
		});
	}

	async getUserdataV2() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data/v2', {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos',
						'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 8)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({datosV2: responseJson});
				}
			});
	}

	async requestMultipart() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let formData = new FormData();
		formData.append(
			'update',
			JSON.stringify({
				user_id: this.state.datosV2.id,
				nombre: this.state.datos.nombre,
				apellido_paterno: this.state.datos.apellido_paterno,
				apellido_materno: this.state.datos.apellido_materno,
				parentezco: this.state.datos.parentezco,
				calle: this.state.datos.calle,
				numero: this.state.datos.numero,
				colonia: this.state.datos.colonia,
				ciudad: this.state.datos.ciudad,
				estado: this.state.datos.estado,
				cp: this.state.datos.cp,
				nombre_alternativo1: this.state.datos.nombre_alternativo1,
				telefono_casa_alternativo1: this.state.datos
					.telefono_casa_alternativo1,
				telefono_celular_alternativo1: this.state.datos
					.telefono_celular_alternativo1,
				telefono_oficina_alternativo1: this.state.datos
					.telefono_oficina_alternativo1,
				email_alternativo1: this.state.datos.email_alternativo1,
				permiso_contacto_alternativo1: this.state.datos
					.permiso_contacto_alternativo1,
				parentesco_alternativo1: this.state.datos.parentesco_alternativo1,
				nombre_alternativo2: this.state.datos.nombre_alternativo2,
				telefono_casa_alternativo2: this.state.datos
					.telefono_casa_alternativo2,
				telefono_celular_alternativo2: this.state.datos
					.telefono_celular_alternativo2,
				telefono_oficina_alternativo2: this.state.datos
					.telefono_oficina_alternativo2,
				email_alternativo2: this.state.datos.email_alternativo2,
				permiso_contacto_alternativo2: this.state.datos
					.permiso_contacto_alternativo2,
				parentesco_alternativo2: this.state.datos.parentesco_alternativo2,
				telefono_casa_tutor: this.state.datos.telefono_casa_tutor,
				telefono_celular_tutor: this.state.datos.telefono_celular_tutor,
				telefono_oficina_tutor: this.state.datos.telefono_oficina_tutor,
				email_tutor: this.state.datos.email_tutor,
				permiso_contacto_tutor: this.state.datos.permiso_contacto_tutor,
				numero_emergencia: this.state.datos.numero_emergencia,
				estudios: this.state.datos.estudios,
				rfc: this.state.datos.rfc,
				curp: this.state.datos.curp
			})
		);
		let reg1 = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (this.state.datos.nombre === null || this.state.datos.nombre === '' || this.state.datos.nombre === undefined) {
			this.state.datoFaltantes.push('Nombre')
		}
		if (this.state.datos.apellido_paterno === null || this.state.datos.apellido_paterno === '') {
			this.state.datoFaltantes.push('Apellido Paterno')
		}
		if (this.state.datos.apellido_materno === null || this.state.datos.apellido_materno === '') {
			this.state.datoFaltantes.push('Apellido Materno')
		}
		if (this.state.datos.calle === null || this.state.datos.calle === '') {
			this.state.datoFaltantes.push('Calle')
		}
		if (this.state.datos.numero === null || this.state.datos.numero === '') {
			this.state.datoFaltantes.push('Número de Calle')
		}
		if (this.state.datos.colonia === null || this.state.datos.colonia === '') {
			this.state.datoFaltantes.push('Colonia')
		}
		if (this.state.datos.ciudad === null || this.state.datos.ciudad === '') {
			this.state.datoFaltantes.push('Ciudad')
		}
		if (this.state.datos.estado === null || this.state.datos.estado === '') {
			this.state.datoFaltantes.push('Estado');
		}
		if (this.state.datos.cp === null || this.state.datos.cp === '') {
			this.state.datoFaltantes.push('Código Postal');
		} else if (this.state.datos.cp.length < 5) {
			this.state.datoFaltantes.push('Código Postal incompleto');
		}
		if (this.state.datos.numero_emergencia === null || this.state.datos.numero_emergencia === '') {
			this.state.datoFaltantes.push('Número de Emergencia')
		} else if (this.state.datos.numero_emergencia.length < 10) {
			this.state.datoFaltantes.push('Número de Emergencia incompleto')
		}
		if (this.state.datos.telefono_casa_tutor === null || this.state.datos.telefono_casa_tutor === '') {
			this.state.datoFaltantes.push('Teléfono de Casa');
		} else if (this.state.datos.telefono_casa_tutor.length < 10) {
			this.state.datoFaltantes.push('Número de Casa incompleto')
		}
		if (this.state.datos.telefono_celular_tutor === null || this.state.datos.telefono_celular_tutor === '') {
			this.state.datoFaltantes.push('Teléfono Celular ');
		} else if (this.state.datos.telefono_celular_tutor.length < 10) {
			this.state.datoFaltantes.push('Número de Celular incompleto')
		}
		if (this.state.datos.email_tutor === null || this.state.datos.email_tutor === '') {
			this.state.datoFaltantes.push('Email');

		} else if (reg1.test(this.state.datos.email_tutor) === false) {
			Alert.alert('Email inválido', 'ingresa un email valido', [{text: 'Enterado'}]);
			return false;
		}
		if (this.state.datos.estudios === null || this.state.datos.estudios === '') {
			this.state.datoFaltantes.push('Ultimo grado de estudios');

		}
		if (this.state.datos.curp === null || this.state.datos.curp === '') {
			this.state.datoFaltantes.push('C.U.R.P');
		} else if (this.state.datos.curp.length < 18) {
			this.state.datoFaltantes.push('C.U.R.P incompleto');
		}
		if (this.state.datos.rfc === null || this.state.datos.rfc === '') {
			this.state.datoFaltantes.push('R.F.C');
		} else if (this.state.datos.rfc.length < 13) {
			this.state.datoFaltantes.push('R.F.C incompleto');
		}
		if (this.state.datoFaltantes.length > 0) {
			Alert.alert('Datos incompletos',
				'Aún faltan datos que son necesarios tales como, ' + this.state.datoFaltantes,
				[{
					text: 'Entendido', onPress: () => this.setState({datoFaltantes: []})
				}]);
		} else {
			await fetch(uri + '/api/user/update', {
				method: 'POST',
				headers: {
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + token
				},
				body: formData
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('¡Ups!',
							'Ha ocurrido un error '
							+
							responseJson.error.status_code
							+
							' si el error continua pónganse en contacto con soporte (Cod. 5)',
							[{
								text: 'Entendido'
							}]);
					} else {
						Alert.alert('¡Felicidades!',
							'Se ha modificado tus datos personales',
							[{
								text: 'Entendido', onPress: () => Actions.drawer()
							}]);
					}
				});
		}
	}

	async onChange(option) {
		await this.setState({
			textInputValue: option.label,
			parentezco: option.parentezco
		});
	}

	async getUserdata() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data', {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos',
						'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 10)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({datos: responseJson});
				}
			});
	}

	async getRole() {
		let role = await AsyncStorage.getItem('role');
		this.setState({admin: role});
	}


	datosPersonales() {
		return (
			<View style={styles.datosBasicos}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Datos personales del maestro
				</Text>
				<Fumi
					ref='Nombre'
					label={'Nombre(s)'}
					iconClass={Ionicons}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'md-person'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.nombre = text)}
					keyboardType='default'
					value={this.state.datos.nombre}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.apellidoPat.focus()}
				/>
				<Fumi
					ref='apellidoPat'
					label={'Apellido paterno'}
					iconClass={Ionicons}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'md-person'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.apellido_paterno = text)}
					keyboardType='default'
					value={this.state.datos.apellido_paterno}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.apellidoMat.focus()}
				/>
				<Fumi
					ref='apellidoMat'
					label={'Apellido materno'}
					iconClass={Ionicons}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'md-person'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.apellido_materno = text)}
					keyboardType='default'
					value={this.state.datos.apellido_materno}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
				/>
			</View>
		);
	}

	async codigoPost(text) {
		let textoCp = text;
		this.state.datos.cp = textoCp;
		await this.setState({aux: 0});
	}

	domicilio() {
		return (
			<View style={styles.datosBasicos}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Domicilio del maestro
				</Text>
				<Fumi
					ref='calle'
					label={'Calle/Avenida'}
					iconClass={Entypo}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'address'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.calle = text)}
					keyboardType='default'
					value={this.state.datos.calle}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.numero.focus()}
				/>
				<Fumi
					ref='numero'
					label={'Número'}
					iconClass={FontAwesome}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'hashtag'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.numero = text)}
					keyboardType='default'
					value={this.state.datos.numero}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.colonia.focus()}
				/>
				<Fumi
					ref='colonia'
					label={'Colonia'}
					iconClass={MaterialIcons}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'my-location'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.colonia = text)}
					keyboardType='default'
					value={this.state.datos.colonia}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.ciudad.focus()}
				/>
				<Fumi
					ref='ciudad'
					label={'Ciudad'}
					iconClass={MaterialIcons}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'location-city'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.ciudad = text)}
					keyboardType='default'
					value={this.state.datos.ciudad}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.estado.focus()}
				/>
				<Fumi
					ref='estado'
					label={'Estado'}
					iconClass={Entypo}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'location'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.estado = text)}
					keyboardType='default'
					value={this.state.datos.estado}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.codigoPost.focus()}
				/>
				<Fumi
					ref='codigoPost'
					label={'Código Postal'}
					iconClass={FontAwesome}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'location-arrow'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => this.codigoPost(text)}
					keyboardType='numeric'
					value={this.state.datos.cp}
					returnKeyType='next'
					maxLength={5}
					autoCapitalize='none'
					autoCorrect={false}
				/>
			</View>
		);
	}

	contacto() {
		return (
			<View style={styles.datosBasicos}>
				<Text style={styles.main_title}>Datos de contacto del maestro</Text>
				<Text style={[styles.subTitulo, {color: 'red'}]}>
					Teléfono de Emergencias
				</Text>
				<View style={styles.datosBasicos}>
					<Fumi
						label={'Escriba el número'}
						iconClass={MaterialCommunityIcons}
						style={[styles.inputDato, {borderColor: this.state.secondColor}]}
						labelStyle={{color: this.state.mainColor}}
						iconName={'heart-pulse'}
						inputStyle={{color: this.state.secondColor}}
						iconColor={this.state.mainColor}
						iconSize={20}
						maxLength={10}
						onChangeText={text => (this.state.datos.numero_emergencia = text)}
						keyboardType='numeric'
						value={this.state.datos.numero_emergencia}
						returnKeyType='next'
						autoCapitalize='none'
						autoCorrect={false}
						onSubmitEditing={() => this.refs.telCasa.focus()}
					/>
				</View>
				<View>
					<Text style={[styles.opcionsitas, {marginTop: 1, marginBottom: 7}]}>
						Escriba un número donde siempre podamos localizarlo
					</Text>
				</View>
				<Fumi
					ref='telCasa'
					label={'Teléfono de casa'}
					iconClass={Entypo}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'old-phone'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					maxLength={10}
					onChangeText={text => (this.state.datos.telefono_casa_tutor = text)}
					keyboardType='phone-pad'
					value={this.state.datos.telefono_casa_tutor}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.telCelular.focus()}
				/>
				<Fumi
					ref='telCelular'
					label={'Teléfono celular'}
					iconClass={Ionicons}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'md-phone-portrait'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					maxLength={10}
					onChangeText={text => (this.state.datos.telefono_celular_tutor = text)}
					keyboardType='phone-pad'
					value={this.state.datos.telefono_celular_tutor}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.telOffice.focus()}
				/>
				<Fumi
					ref='email'
					label={'Email'}
					iconClass={Entypo}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'email'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.email_tutor = text)}
					keyboardType='email-address'
					value={this.state.datos.email_tutor}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
				/>
			</View>
		);
	}

	async rfc(text) {
		let texto = text;
		this.state.datos.rfc = texto.toUpperCase();
		await this.setState({aux: 0});
	}

	async curp(text) {
		let texto = text;
		this.state.datos.curp = texto.toUpperCase();
		await this.setState({aux: 0});
	}

	profesional() {
		return (
			<View style={styles.datosBasicos}>
				<Text style={styles.main_title}>Datos generales</Text>
				<Fumi
					ref='ultimoEst'
					label={'Último grado de Estudios'}
					iconClass={FontAwesome5}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'user-graduate'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => (this.state.datos.estudios = text)}
					keyboardType='default'
					value={this.state.datos.estudios}
					returnKeyType='next'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.rfc.focus()}
				/>
				<Fumi
					ref='rfc'
					label={'R.F.C.'}
					iconClass={FontAwesome}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'id-badge'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					maxLength={13}
					onChangeText={text => this.rfc(text)}
					keyboardType='default'
					value={this.state.datos.rfc}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
					onSubmitEditing={() => this.refs.curp.focus()}
				/>
				<Fumi
					ref='curp'
					label={'C.U.R.P.'}
					iconClass={FontAwesome}
					maxLength={18}
					style={[styles.inputDato, {borderColor: this.state.secondColor}]}
					labelStyle={{color: this.state.mainColor}}
					iconName={'id-card-o'}
					inputStyle={{color: this.state.secondColor}}
					iconColor={this.state.mainColor}
					iconSize={20}
					onChangeText={text => this.curp(text)}
					keyboardType='default'
					value={this.state.datos.curp}
					returnKeyType='next'
					autoCapitalize='none'
					autoCorrect={false}
				/>
			</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<KeyboardAvoidingView
					style={styles.container}
					behavior='padding'
					keyboardVerticalOffset={130}
				>

					<Modal
						isVisible={this.state.isModalVisible}
						backdropOpacity={0.8}
						animationIn={'bounceIn'}
						animationOut={'bounceOut'}
						animationInTiming={1000}
						animationOutTiming={1000}
					>
						<View style={styles.containerMod}>
							<View style={styles.modalClaus}>
								<Text style={styles.titleClausula}>
									CLÁUSULA DE RESPONSABILIDAD PARA LA ACTUALIZACIÓN DE MIS
									DATOS DE CONTACTO
								</Text>
								<Text style={styles.textClaus}>
									Entiendo y acepto que es mi total responsabilidad mantener
									mis datos de contacto actualizados. Deslindo a la
									institución de cualquier responsabilidad derivada de la
									imposibilidad de localizarme en el caso de una emergencia
									debido a que no hubiese mantenido actualizada mi información
									de contacto. Al guardar sus datos estará aceptando la
									presente cláusula de responsabilidad.
								</Text>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: '#fff', borderColor: this.state.secondColor}
									]}
									onPress={() => this.setState({isModalVisible: false})}
								>
									<Text style={[styles.textW, {color: this.state.secondColor}]}>Enterado,
																								  ¡Gracias!</Text>
								</TouchableOpacity>
							</View>
						</View>
					</Modal>
					<View style={[styles.row, styles.widthall, {marginTop: 15}]}>
						<Text
							style={[
								styles.main_title, styles.modalWidth,
								{color: this.state.thirdColor, marginTop: 0}
							]}
						>
							Seleccione la categoría
						</Text>
						<Feather name='alert-circle' size={20} onPress={this._showModal}/>
					</View>
					<View style={styles.widthall}>
						<MultiBotonRow
							itemBtns={['Generales', 'Domicilio', 'Contacto', 'Profesional']}
							onSelectedButton={(indexBtn, itemBtn) =>
								this.botonSelected(indexBtn, itemBtn)
							}
							cantidad={4}
						/>
					</View>
					<ScrollView showsVerticalScrollIndicator={false}>
						{this.state.botonSelected === 'Generales'
							? this.datosPersonales()
							: null}
						{this.state.botonSelected === 'Contacto' ? this.contacto() : null}
						{this.state.botonSelected === 'Domicilio' ? this.domicilio() : null}
						{this.state.botonSelected === 'Profesional'
							? this.profesional()
							: null}
						<TouchableOpacity
							style={[
								styles.bigButton,
								{backgroundColor: this.state.secondColor}
							]}
							onPress={() => this.requestMultipart()}
						>
							<Text
								style={styles.textButton}>{this.state.datos.length > 0 ? 'Guardar' : 'Actualizar'}</Text>
						</TouchableOpacity>
					</ScrollView>
				</KeyboardAvoidingView>

			</View>
		);
	}
}
