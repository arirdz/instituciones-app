import React from "react";
import {
    AsyncStorage,
    Keyboard,
    KeyboardAvoidingView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import Grupo from "../Globales/Grupo";
import Periodos from "../Globales/Periodos";
import ModalSelector from "react-native-modal-selector";
import styles from "../../styles";
import Ionicons from "react-native-vector-icons/Ionicons";
import StepIndicator from "rn-step-indicator";
import {RadioButton, RadioGroup} from "react-native-flexi-radio-button";

const encParam = ["Primer paso", "Segundo paso", "Visualización"];
const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class CrearExmn extends React.Component {
    _showModal = () => this.setState({isModalVisible: true});
    _hideModal = () => this.setState({isModalVisible: false});

    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            textInputValue: "",
            currentPosition: 0,
            selectedIndexGrupos: -1,
            periodos: [],
            materias: [],
            elGrupo: "",
            grupos: [],
            selectedIndexPeriodo: -1,
            laMateria: "",
            maximo: "",
            elPeriodo: ""
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onPageChange(position) {
        await this.setState({
            currentPosition: position
        });
    }

    async onListItemPressedGrupos(indexBtn, itemBtn) {
        await this.setState({
            selectedIndexGrupos: indexBtn,
            elGrupo: itemBtn
        });
    }

    //+++++++++++++++++++++++++++++++ Get User +++++++++++++++++++++++++++++++++++
    async getUserdata() {
        let userdata = await fetch(this.state.uri + "/api/user/data", {
            method: "GET",
            headers: {
                Authorization: "Bearer " + this.state.token
            }
        });
        let dato = await userdata.json();
        await this.setState({data: dato});
    }

    //++++++++++++++++++++++++++++++++++ Periodos ++++++++++++++++++++++++++++++++
    async onListItemPressedPeriodo(indexSelected, itemSelected) {
        this.setState({
            selectedIndexPeriodo: indexSelected,
            elPeriodo: itemSelected
        });
        this.getMaterias();
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(
            this.state.uri + "/api/materias/maestro" + this.state.elPeriodo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            selectedIndexGrupos: -1,
            urlGp: "/api/grupos/materias/" + option.materia
        });
        await this.getGrupos();
    }

    async onChange1(option1) {
        await this.setState({
            elTiempo: option1.label
        });
    }

    // radio button
    async onSelect(index, value) {
        await this.setState({role: value});
    }

    //+++++++++++++++++++++++++++++++ respuesta +++++++++++++++++++++++++++++++++
    async respuesta() {
        let formResp = new FormData();
        formResp.append(
            "new",
            JSON.stringify({
                id_pregunta: this.state.pregunta,
                respuesta: this.state.laRespuesta,
                correcta: this.state.laCorrecta
            })
        );
        let request = await fetch(this.state.uri + "/api/respuestas/examen", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "multipart/form-data",
                Authorization: "Bearer " + this.state.token
            },
            body: formResp
        });
    }

    //+++++++++++++++++++++++++++++++++++ preguntas ++++++++++++++++++++++++++++++
    async preguntas() {
        let formPregunta = new FormData();
        formPregunta.append(
            "new",
            JSON.stringify({
                id_examen: this.state.elExamen,
                numero: this.state.elNumero,
                pregunta: this.state.laPregunta
            })
        );
        let request = await fetch(this.state.uri + "/api/preguntas/examen", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "multipart/form-data",
                Authorization: "Bearer " + this.state.token
            },
            body: formPregunta
        });
    }

    //+++++++++++++++++++++++++++++++++++ examen +++++++++++++++++++++++++++++++
    async examen() {
        let formData = new FormData();
        formData.append(
            "new",
            JSON.stringify({
                id_materia: this.state.laMateria,
                id_maestro: this.state.data,
                grupo: this.state.elGrupo,
                periodo: this.state.elPeriodo,
                reactivos: this.state.numReac,
                nombre: this.state.nombExam,
                tiempo: this.state.elTiempo
            })
        );
        let request = await fetch(this.state.uri + "/api/configuracion/examen", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "multipart/form-data",
                Authorization: "Bearer " + this.state.token
            },
            body: formData
        });
    }

    //+++++++++++++++++++++++++++++++ pasos ++++++++++++++++++++++++++++++++
    pasoSig() {
        if (this.state.currentPosition <= 1) {
            this.setState({
                currentPosition: this.state.currentPosition + 1
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition + 0
            });
        }
    }

    pasoAnt() {
        if (this.state.currentPosition <= 0) {
            this.setState({
                currentPosition: 0
            });
        } else {
            this.setState({
                currentPosition: this.state.currentPosition - 1
            });
        }
    }

    //+++++++++++++++++++++++++++++++++ Render ++++++++++++++++++++++++++++
    render() {
        let customStyles = {
            stepIndicatorSize: 25,
            currentStepIndicatorSize: 30,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: "#777777",
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: "#777777",
            stepStrokeUnFinishedColor: "#aaaaaa",
            separatorFinishedColor: "#777777",
            separatorUnFinishedColor: "#aaaaaa",
            stepIndicatorFinishedColor: "#777777",
            stepIndicatorUnFinishedColor: "#ffffff",
            stepIndicatorCurrentColor: "#ffffff",
            stepIndicatorLabelFontSize: 13,
            currentStepIndicatorLabelFontSize: 13,
            stepIndicatorLabelCurrentColor: "#777777",
            stepIndicatorLabelFinishedColor: "#ffffff",
            stepIndicatorLabelUnFinishedColor: "#aaaaaa",
            labelColor: "#aaaaaa",
            labelSize: responsiveFontSize(1.3),
            currentStepLabelColor: "#777777"
        };
        let index = 0;
        const data2 = [
            {key: index++, label: "10 Minutos"},
            {key: index++, label: "20 Minutos"},
            {key: index++, label: "30 Minutos"},
            {key: index++, label: "40 Minutos"},
            {key: index++, label: "50 Minutos"},
            {key: index++, label: "60 Minutos"},
            {key: index++, label: "70 Minutos"},
            {key: index++, label: "80 Minutos"},
            {key: index++, label: "90 Minutos"},
            {key: index++, label: "100 Minutos"}
        ];
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        return (
            <KeyboardAvoidingView style={styles.container} behavior={"padding"}>
                {/*aqui se hace la estructura de los pasos  */}
                <View style={[styles.btn1, {marginTop: 10}]}>
                    <StepIndicator
                        customStyles={customStyles}
                        stepCount={3}
                        currentPosition={this.state.currentPosition}
                        labels={encParam}
                    />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {/*primer pasoooooooo  */}
                    {this.state.currentPosition === 0 ? (
                        <View>
                            {/*<Periodos*/}
                                {/*onListItemPressedPeriodo={(indexBtn, itemBtn) =>*/}
                                    {/*this.onListItemPressedPeriodo(indexBtn, itemBtn)*/}
                                {/*}*/}
                            {/*/>*/}
                            <Text
                                style={[styles.main_title, {color: this.state.thirdColor}]}
                            >
                                Seleccione la materia
                            </Text>
                            <ModalSelector
                                cancelText="Cancelar"
                                data={data}
                                initValue="Seleccione la materia"
                                onChange={option => this.setState({laMateria: option})}
                                optionTextStyle={{color: this.state.thirdColor}}
                                selectStyle={[
                                    styles.inputPicker,
                                    {borderColor: this.state.secondColor}
                                ]}
                            />
                            <Grupo
                                urlGrupo={this.state.urlGp}
                                onListItemPressedGrupos={(indexBtn, itemBtn) =>
                                    this.onListItemPressedGrupos(indexBtn, itemBtn)
                                }
                            />
                            <Text
                                style={[styles.main_title, {color: this.state.thirdColor}]}
                            >
                                Número de reactivos
                            </Text>
                            <TextInput
                                keyboardType="numeric"
                                maxLength={2}
                                multiline={false}
                                onChangeText={text => this.setState({numReac: text})}
                                onEndEditing={() => Keyboard.dismiss()}
                                returnKeyType="done"
                                underlineColorAndroid="transparent"
                                style={[
                                    styles.btn2,
                                    styles.cuadroess,
                                    styles.txtCenter,
                                    {backgroundColor: this.state.fourthColor}
                                ]}
                            />
                            <Text
                                style={[styles.main_title, {color: this.state.thirdColor}]}
                            >
                                Nombre del examen
                            </Text>
                            <TextInput
                                maxLength={150}
                                multiline={false}
                                onChangeText={text => this.setState({nombExam: text})}
                                onEndEditing={() => Keyboard.dismiss()}
                                returnKeyType="done"
                                underlineColorAndroid="transparent"
                                style={[
                                    styles.btn1,
                                    styles.cuadroEstd,
                                    styles.txtCenter,
                                    {backgroundColor: this.state.fourthColor}
                                ]}
                            />
                            <Text
                                style={[styles.main_title, {color: this.state.thirdColor}]}
                            >
                                Tiempo del examen
                            </Text>
                            <ModalSelector
                                data={data2}
                                cancelText="Cancelar"
                                initValue="Haga una selección"
                                onChange={option1 => this.setState({elTiempo: option1})}
                                optionTextStyle={{color: this.state.thirdColor}}
                                selectStyle={[
                                    styles.inputPicker,
                                    styles.btn1,
                                    {borderColor: this.state.secondColor}
                                ]}
                            />
                        </View>
                    ) : null}
                    {/* segundo pasooooooo */}
                    {this.state.currentPosition === 1 ? (
                        <View>
                            <Text style={styles.main_title}>Banco de Preguntas</Text>
                            <TextInput
                                maxLength={500}
                                multiline={false}
                                onChangeText={text => this.setState({maximo: text})}
                                onEndEditing={() => Keyboard.dismiss()}
                                returnKeyType="done"
                                underlineColorAndroid="transparent"
                                style={[
                                    styles.btn1,
                                    styles.cuadroEstd,
                                    styles.txtCenter,
                                    {backgroundColor: this.state.fourthColor}
                                ]}
                            />
                            <Text style={styles.main_title}>Posibles respuestas</Text>
                            <View style={[styles.row, {width: responsiveWidth(50)}]}>
                                <Text/>
                                <View
                                    style={[
                                        styles.row,
                                        {height: responsiveHeight(5), width: responsiveWidth(46)}
                                    ]}
                                >
                                    <Text
                                        style={{
                                            fontSize: responsiveFontSize(1.2),
                                            width: responsiveWidth(30),
                                            marginTop: 0,
                                            textAlign: "center",
                                            fontWeight: "300"
                                        }}
                                    >
                                        Captura las Respuestas
                                    </Text>
                                </View>
                                <View
                                    style={[
                                        styles.row,
                                        {
                                            height: responsiveHeight(5),
                                            width: responsiveWidth(46)
                                        }
                                    ]}
                                >
                                    <Text
                                        style={{
                                            fontSize: responsiveFontSize(1.2),
                                            width: responsiveWidth(30),
                                            marginTop: 0,
                                            textAlign: "center",
                                            fontWeight: "300"
                                        }}
                                    >
                                        Elija la correcta
                                    </Text>
                                </View>
                            </View>

                            {/* LETRA AAAA */}
                            <View style={{flex: 1, flexDirection: "row"}}>
                                <View
                                    style={{
                                        width: -5,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    A)
                                </Text>

                                <View style={{width: responsiveWidth(-10), height: 50}}/>
                                <TextInput
                                    maxLength={100}
                                    multiline={false}
                                    placeholder={"Respuesta"}
                                    onChangeText={text => this.setState({maximo: text})}
                                    onEndEditing={() => Keyboard.dismiss()}
                                    returnKeyType="done"
                                    underlineColorAndroid="transparent"
                                    style={[
                                        styles.btn1_2,
                                        styles.cuadroEstd,
                                        styles.txtCenter,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                />

                                <View
                                    style={{
                                        width: 10,
                                        height: 50
                                    }}
                                />
                                <RadioGroup
                                    onSelect={(index, value) => this.onSelect1(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>

                            {/* LETRA BBBBB */}
                            <View style={{flex: 1, flexDirection: "row", marginTop: -15}}>
                                <View
                                    style={{
                                        width: -5,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    B)
                                </Text>

                                <View style={{width: -10, height: 50}}/>
                                <TextInput
                                    maxLength={100}
                                    multiline={false}
                                    placeholder={"Respuesta"}
                                    onChangeText={text => this.setState({maximo: text})}
                                    onEndEditing={() => Keyboard.dismiss()}
                                    returnKeyType="done"
                                    underlineColorAndroid="transparent"
                                    style={[
                                        styles.btn1_2,
                                        styles.cuadroEstd,
                                        styles.txtCenter,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                />
                                <RadioGroup
                                    style={{marginLeft: 10}}
                                    onSelect={(index, value) => this.onSelect(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>

                            {/* LETRA CCCCCC */}
                            <View style={{flex: 1, flexDirection: "row", marginTop: -15}}>
                                <View
                                    style={{
                                        width: -5,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    C)
                                </Text>
                                <View style={{width: -10, height: 50}}/>
                                <TextInput
                                    maxLength={100}
                                    multiline={false}
                                    placeholder={"Respuesta"}
                                    onChangeText={text => this.setState({maximo: text})}
                                    onEndEditing={() => Keyboard.dismiss()}
                                    returnKeyType="done"
                                    underlineColorAndroid="transparent"
                                    style={[
                                        styles.btn1_2,
                                        styles.cuadroEstd,
                                        styles.txtCenter,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                />

                                <RadioGroup
                                    style={{marginLeft: 10}}
                                    onSelect={(index, value) => this.onSelect(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>

                            {/* LETRA DDDDDDD */}
                            <View style={{flex: 1, flexDirection: "row", marginTop: -15}}>
                                <View
                                    style={{
                                        width: -5,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    D)
                                </Text>

                                <View style={{width: -10, height: 50}}/>
                                <TextInput
                                    maxLength={100}
                                    multiline={false}
                                    placeholder={"Respuesta"}
                                    onChangeText={text => this.setState({maximo: text})}
                                    onEndEditing={() => Keyboard.dismiss()}
                                    returnKeyType="done"
                                    underlineColorAndroid="transparent"
                                    style={[
                                        styles.btn1_2,
                                        styles.cuadroEstd,
                                        styles.txtCenter,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                />
                                <View
                                    style={{
                                        width: 10,
                                        height: 50
                                    }}
                                />
                                <RadioGroup
                                    onSelect={(index, value) => this.onSelect(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>

                            <View style={styles.container}>
                                <View
                                    style={[
                                        styles.row,
                                        {
                                            width: responsiveWidth(60),
                                            height: responsiveHeight(4),
                                            alignItems: "center",
                                            marginTop: 5,
                                            marginBottom: 10
                                        }
                                    ]}
                                >
                                    <TouchableOpacity onPress={() => this.setState()}>
                                        <Ionicons
                                            name="ios-arrow-dropleft-outline"
                                            size={60}
                                            color={this.state.thirdColor}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.setState()}>
                                        <Ionicons
                                            name="ios-arrow-dropright"
                                            size={60}
                                            color={this.state.thirdColor}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.row, {width: responsiveWidth(68)}]}>
                                    <View style={[styles.row, {width: responsiveWidth(45)}]}>
                                        <Text>Ver Anterior</Text>
                                    </View>
                                    <View>
                                        <Text>Agregar Siguiente</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    ) : null}
                    {/*tercer pasooooooooo */}
                    {this.state.currentPosition === 2 ? (
                        <View>

                            <View
                                style={[
                                    styles.row,
                                    styles.rowInfo,
                                    {
                                        backgroundColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text style={{fontWeight: "800"}}>Materia: </Text>
                                </View>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo2,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text>Español 3</Text>
                                </View>
                            </View>
                            <View
                                style={[
                                    styles.row,
                                    styles.rowInfo,
                                    {
                                        backgroundColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text style={{fontWeight: "800"}}>Reactivos: </Text>
                                </View>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo2,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text>50</Text>
                                </View>
                            </View>
                            <View
                                style={[
                                    styles.row,
                                    styles.rowInfo,
                                    {
                                        backgroundColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text style={{fontWeight: "800"}}>Examen: </Text>
                                </View>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo2,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text>Ecuaciones</Text>
                                </View>
                            </View>
                            <View
                                style={[
                                    styles.row,
                                    styles.rowInfo,
                                    {
                                        backgroundColor: this.state.fourthColor
                                    }
                                ]}
                            >
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text style={{fontWeight: "800"}}>Tiempo: </Text>
                                </View>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.colInfo2,
                                        {
                                            borderColor: this.state.fourthColor,
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                >
                                    <Text>60 minutos</Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    width: 110,
                                    height: -1
                                }}
                            />
                            <Text
                                style={[
                                    styles.main_title,
                                    {
                                        textAlign: "center"
                                    }
                                ]}
                            >
                                1.- ¿Cuanto es 1+1?
                            </Text>
                            <View style={{flex: 1, flexDirection: "row", marginTop: 20}}>
                                <View
                                    style={{
                                        width: 110,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),

                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    A)
                                </Text>

                                <View style={{width: -10, height: 50}}/>
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    5
                                </Text>
                                <View
                                    style={{
                                        width: -10,
                                        height: 50
                                    }}
                                />
                                <RadioGroup
                                    style={{
                                        height: responsiveHeight(5),
                                        marginTop: -5,
                                        width: responsiveWidth(10)
                                    }}
                                    onSelect={(index, value) => this.onSelect(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>
                            <View style={{flex: 1, flexDirection: "row", marginTop: -15}}>
                                <View
                                    style={{
                                        width: 110,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    B)
                                </Text>

                                <View style={{width: -10, height: 50}}/>
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    2
                                </Text>

                                <View
                                    style={{
                                        width: -10,
                                        height: 50
                                    }}
                                />
                                <RadioGroup
                                    style={{
                                        height: responsiveHeight(5),
                                        marginTop: -5,
                                        width: responsiveWidth(10)
                                    }}
                                    onSelect={(index, value) => this.onSelect(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>
                            <View style={{flex: 1, flexDirection: "row", marginTop: -15}}>
                                <View
                                    style={{
                                        width: 110,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    C)
                                </Text>

                                <View style={{width: -10, height: 50}}/>
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    7
                                </Text>

                                <View
                                    style={{
                                        width: -10,
                                        height: 50
                                    }}
                                />
                                <RadioGroup
                                    style={{
                                        height: responsiveHeight(5),
                                        marginTop: -5,
                                        width: responsiveWidth(10)
                                    }}
                                    onSelect={(index, value) => this.onSelect(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>
                            <View style={{flex: 1, flexDirection: "row", marginTop: -15}}>
                                <View
                                    style={{
                                        width: 110,
                                        height: 50
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    D)
                                </Text>

                                <View style={{width: -10, height: 50}}/>
                                <Text
                                    style={{
                                        fontSize: responsiveFontSize(2.3),
                                        width: responsiveWidth(20),
                                        height: responsiveHeight(8),
                                        marginTop: 5,
                                        textAlign: "center",
                                        fontWeight: "600"
                                    }}
                                >
                                    -2
                                </Text>

                                <View
                                    style={{
                                        width: -10,
                                        height: 50
                                    }}
                                />
                                <RadioGroup
                                    style={{
                                        height: responsiveHeight(5),
                                        marginTop: -5,
                                        width: responsiveWidth(10)
                                    }}
                                    onSelect={(index, value) => this.onSelect(index, value)}
                                >
                                    <RadioButton value={"-0"}/>
                                </RadioGroup>
                            </View>
                            <View style={styles.container}>
                                <View style={[styles.row, styles.ajustIcon]}>
                                    <TouchableOpacity onPress={() => this.setState()}>
                                        <Ionicons
                                            name="ios-arrow-dropleft-outline"
                                            size={60}
                                            color={this.state.thirdColor}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.setState()}>
                                        <Ionicons
                                            name="ios-arrow-dropright"
                                            size={60}
                                            color={this.state.thirdColor}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.row, {width: responsiveWidth(76)}]}>
                                    <Text>Pregunta Anterior</Text>
                                    <Text>Siguiente pregunta</Text>
                                </View>
                            </View>
                        </View>
                    ) : null}
                </ScrollView>
                <View
                    style={[
                        styles.rowsCalif,
                        {width: responsiveWidth(100), marginTop: 20}
                    ]}
                >
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {backgroundColor: this.state.secondColor}
                        ]}
                        onPress={() => this.pasoAnt()}
                    >
                        <Text style={styles.textButton}>Paso anterior</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {
                                backgroundColor: this.state.secondColor,
                                borderLeftWidth: 1,
                                borderColor: "white"
                            }
                        ]}
                        onPress={() => this.pasoSig()}
                    >
                        <Text style={styles.textButton}>Paso Siguiente</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
