import React from "react";
import {
    AsyncStorage,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {
    responsiveWidth,
    responsiveHeight
} from "react-native-responsive-dimensions";
import Grupo from "../Globales/Grupo";
import Periodos from "../Globales/Periodos";
import ModalSelector from "react-native-modal-selector";
import Modal from "react-native-modal";
import Entypo from "react-native-vector-icons/Entypo";
import styles from "../../styles";

export default class AcumuladosCalificaciones extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            aciertos: [],
            alumnos: [],
            c: "",
            indice: 0,
            calificacion: [],
            ciclos: [],
            elCiclo: "",
            elExamen: "",
            elGrado: "",
            elGrupo: "",
            elPeriodo: "",
            examenes: [],
            laCalific: [],
            laMateria: "",
            materias: [],
            maximo: "",
            periodos: [],
            selectedIndex: -1,
            selectedIndexGrupos: -1,
            valores: "",
            elEncuadre: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getCalifFinal() {
        let calificacion = await fetch(
            this.state.uri +
            "/api/calificaciones/maestro/" +
            this.state.elPeriodo +
            "/" +
            this.state.laMateria +
            "/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let calific = await calificacion.json();
        await this.setState({laCalific: calific});
        await this.setState({
            long: this.state.laCalific[0].calificaciones.length - 1
        });
    }

    //+++++++++++++++++++++++++++++ Grupos +++++++++++++++++++++++++++++++++++++++
    async onListItemPressedGrupos(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexGrupos: indexSelected,
            elGrupo: itemSelected
        });
        await this.getCalifFinal();
        await this.getAlumnosByGG();
        await this.getVerEncuadres();
    }

    //++++++++++++++++++++++++++++++++++ Periodos ++++++++++++++++++++++++++++++++
    async onListItemPressedPeriodo(indexSelected, itemSelected) {
        this.setState({
            selectedIndexPeriodo: indexSelected,
            elPeriodo: itemSelected
        });
        this.getMaterias();
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(
            this.state.uri + "/api/materias/maestro" + this.state.elPeriodo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            selectedIndexGrupos: -1,
            urlGp: "/api/grupos/materias/" + option.materia
        });
    }

    //+++++++++++++++++++++++++++++++ Get User +++++++++++++++++++++++++++++++++++
    async getUserdata() {
        let userdata = await fetch(this.state.uri + "/api/user/data", {
            method: "GET",
            headers: {
                Authorization: "Bearer " + this.state.token
            }
        });
        let dato = await userdata.json();
        await this.setState({data: dato});
    }

    //++++++++++++++++++++++++++++++++++++Encuadre+++++++++++++++++++++++++++++++++
    async getVerEncuadres() {
        let encuadrepicker = await fetch(
            this.state.uri +
            "/api/list/encuadreV2/" +
            this.state.elPeriodo +
            "/" +
            this.state.laMateria +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let encuadrespicker = await encuadrepicker.json();
        await this.setState({elEncuadre: encuadrespicker});
    }

    renderEncuadre(itemEnc, indexEnc) {
        let b = this.state.elEncuadre.length - 1;
        let tabRowE = [styles.rowTabla];
        if (indexEnc !== b) {
            tabRowE.push(styles.modalWidth, {borderColor: this.state.secondColor});
        }
        const listae = this.isEncuadre(itemEnc, indexEnc);
        return (
            <View key={indexEnc} style={tabRowE}>
                {listae}
            </View>
        );
    }

    isEncuadre(itemEnc, indexEnc) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth]}>
                    <View style={[styles.row, styles.btn2]}>
                        <Text>{itemEnc}</Text>
                    </View>
                    <View style={[styles.row, styles.btn6]}>
                        <Text>
                            {
                                this.state.laCalific[this.state.indice].calificaciones[indexEnc]
                                    .puntos
                            }
                        </Text>
                    </View>
                    <View style={[styles.row, styles.btn3]}>
                        <Text>
                            {
                                this.state.laCalific[this.state.indice].calificaciones[indexEnc]
                                    .obtenido
                            }%
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

    renderEncuadres() {
        let btnEnc = [];
        this.state.elEncuadre.forEach((itemEnc, indexEnc) => {
            btnEnc.push(this.renderEncuadre(itemEnc, indexEnc));
        });
        return btnEnc;
    }

    //+++++++++++++++++++++++++++++++++++ Alumnos +++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/get/alumnos/by/grado/grupo/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        const lista = this.isAlumno(itemAlumno, indexAlumno);
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {lista}
            </View>
        );
    }

    isAlumno(itemAlumno, indexAlumno) {
        const encuadre = this.renderEncuadres();
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View style={[styles.row, {width: responsiveWidth(21)}]}>
                        <TouchableOpacity
                            onPress={() =>
                                this.setState({isModalVisible: true, indice: indexAlumno})
                            }
                        >
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Text>
                            {
                                this.state.laCalific[indexAlumno].calificaciones[
                                    this.state.long
                                    ].obtenido
                            }%
                        </Text>
                    </View>
                </View>
                <Modal
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.5}
                    animationIn={"bounceIn"}
                    animationOut={"bounceOut"}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={{height: responsiveHeight(50)}}>
                        <View style={[styles.container, {borderRadius: 6}]}>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de encuadre
                            </Text>
                            <View style={[styles.alignView, styles.modalWidth]}>
                                <View
                                    style={[
                                        styles.rowsCalif,
                                        styles.modalWidth,
                                        {marginTop: 5}
                                    ]}
                                >
                                    <Text style={{marginLeft: 183}}>Valor</Text>
                                    <Text style={{marginRight: 23}}>Maximo</Text>
                                </View>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    styles.modalWidth,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}
                            >
                                {encuadre}
                            </View>
                            <TouchableOpacity
                                style={[
                                    styles.bigButton,
                                    styles.modalWidth,
                                    {backgroundColor: this.state.mainColor}
                                ]}
                                onPress={() => this.setState({isModalVisible: false})}
                            >
                                <Text style={styles.textButton}>Aceptar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++++++++++

    render() {
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        const alumnos = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Periodos
                        onListItemPressedPeriodo={(indexBtn, itemBtn) =>
                            this.onListItemPressedPeriodo(indexBtn, itemBtn)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione la materia
                    </Text>
                    <ModalSelector
                        cancelText="Cancelar"
                        data={data}
                        initValue="Seleccione la materia"
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                    />
                    <Grupo
                        urlGrupo={this.state.urlGp}
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                    />
                    <View
                        style={[
                            styles.tabla,
                            {borderColor: this.state.secondColor, marginTop: 20}
                        ]}
                    >
                        {alumnos}
                    </View>
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}
                >
                    <Text style={styles.textButton}>Enviar al coordinador</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
