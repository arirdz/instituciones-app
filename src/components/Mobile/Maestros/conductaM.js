import React from 'react';
import {
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import MultipleChoice from 'rn-multiple-choice';
import styles from '../../styles';
import Grupo from '../Globales/Grupo';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class conductaM extends React.Component {
    // +++++++++++++++++++++++++++++ CONTENIDO +++++++++++++++++++++++++++++++++++
    handleContenido = text => {
        this.setState({laDescripcion: text});
    };

    constructor(props) {
        super(props);
        this.state = {
            alumnos: [],
            elAlumno: '',
            elCiclo: '',
            elEdo: '',
            elPeriodo: '',
            extras: ['', ''],
            grado: '',
            elGrupo: '',
            grupos: [],
            incidencias: [],
            laDescripcion: '',
            laMateria: '',
            lasFaltas: ['', '', ''],
            materias: [],
            selectedIndexAlumno: -1,
            selectedIndexGrupo: -1,
            selectedIndexIncidencia: '',
            tipo: '',
            tipos: ['Académica', 'Conductual']
        };
        this.renderAlumnos = this.renderAlumnos.bind(this);
        this.onListPressedAlumno = this.onListPressedAlumno.bind(this);
        this.renderTipos = this.renderTipos.bind(this);
        this.onListItemPressedTipo = this.onListItemPressedTipo.bind(this);
        this.renderIncidencias = this.renderIncidencias.bind(this);
    }

    static fecha() {
        let hoyD = moment().format('dddd' + ' DD');
        let hoyA = moment().format('MMMM ' + 'YYYY');
        return hoyD + ' de ' + hoyA;
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
        await this.getMaterias();
    }

    async componentWillMount() {
        await this.getURL();
    }

    // ++++++++++++++++++++++++++++++++ Materias +++++++++++++++++++++++++++++++++++

    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + '/api/materias/maestro', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            selectedIndexAlumno: -1,
            grado: option.grado,
            laMateria: option.materia,
            selectedIndexGrupos: -1,
            urlGp: '/api/grupos/materias/' + option.materia
        });
    }

    // +++++++++++++++++++++++++++++++ Grupos +++++++++++++++++++++++++++++++++++

    async onListItemPressedGrupos(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexGrupos: indexSelected,
            elGrupo: itemSelected
        });
        await this.getAlumnos();
    }

    // +++++++++++++++++++++++++++++++ ALUMNOS +++++++++++++++++++++++++++++++++++

    async getAlumnos() {
        let alumnoList = await fetch(
            this.state.uri +
            '/api/user/alumnos/' +
            this.state.grado +
            '/' +
            this.state.elGrupo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnosList = await alumnoList.json();
        this.setState({alumnos: alumnosList});
    }

    async onListPressedAlumno(indexAlumno, alumno) {
        await this.setState({selectedIndexAlumno: indexAlumno, elAlumno: alumno});
        await this.getCiclos();
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let smallButtonStyles = [
            styles.listButtonAsunto,
            styles.listButtonSmall,
            styles.btn1,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexAlumno === indexAlumno) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexAlumno}
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.id)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemAlumno.name}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    // ++++++++++++++++++++++++++ get ciclo y periodo ++++++++++++++++++++++++++++
    async getCiclos() {
        let ciclopicker = await fetch(
            this.state.uri + '/api/info/alumnos/' + this.state.elAlumno,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );

        ciclospicker = await ciclopicker.json();
        this.setState({elCiclo: ciclospicker[0].ciclo});
        this.setState({elPeriodo: ciclospicker[0].periodo});
    }

    // ++++++++++++++++++++++++++++++++++ TIPOS ++++++++++++++++++++++++++++++++++

    async onListItemPressedTipo(index, tipo) {
        await this.setState({selectedIndexTipo: index, tipo: tipo});
        await this.getIncidencias();
        await this.getIncidencias();
    }

    renderTipo(itemTipo, indexTipo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn2,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexTipo === indexTipo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexTipo}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedTipo(indexTipo, itemTipo)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemTipo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderTipos() {
        let buttonsTipos = [];
        this.state.tipos.forEach((itemTipo, indexTipo) => {
            buttonsTipos.push(this.renderTipo(itemTipo, indexTipo));
        });
        return buttonsTipos;
    }

    // ++++++++++++++++++++++++++++++ INCIDENCIAS ++++++++++++++++++++++++++++++++

    async getIncidencias() {
        let incidenciapicker = await fetch(
            this.state.uri + '/api/incidencias/' + this.state.tipo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let incidenciaspicker = await incidenciapicker.json();
        this.setState({incidencias: incidenciaspicker});
    }

    renderIncidencias() {
        let lasIncidencias = [];
        this.state.incidencias.forEach((itemIncidencias, indexIncidencias) => {
            lasIncidencias.push(this.state.incidencias[indexIncidencias].falta);
        });
        return lasIncidencias;
    }

    _optionSelected(option) {
        if (this.state.lasFaltas[0] === option) {
            this.state.lasFaltas[0] = this.state.lasFaltas[1];
            this.state.lasFaltas[1] = this.state.lasFaltas[2];
            this.state.lasFaltas[2] = '';
        } else {
            if (this.state.lasFaltas[1] === option) {
                this.state.lasFaltas[1] = this.state.lasFaltas[2];
                this.state.lasFaltas[2] = '';
            } else {
                if (this.state.lasFaltas[2] === option) {
                    this.state.lasFaltas[2] = '';
                } else {
                    if (this.state.lasFaltas[0] === '') {
                        this.state.lasFaltas[0] = option;
                    } else {
                        if (this.state.lasFaltas[1] === '') {
                            this.state.lasFaltas[1] = option;
                        } else {
                            if (this.state.lasFaltas[2] === '') {
                                this.state.lasFaltas[2] = option;
                            } else {
                                if (
                                    this.state.lasFaltas[0] !== option &&
                                    this.state.lasFaltas[1] !== option &&
                                    this.state.lasFaltas[2] !== option
                                ) {
                                    this.state.lasFaltas[0] = this.state.lasFaltas[1];
                                    this.state.lasFaltas[1] = this.state.lasFaltas[2];
                                    this.state.lasFaltas[2] = option;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    _optionSelected1(option) {
        if (this.state.extras[0] === option) {
            this.state.extras[0] = this.state.extras[1];
            this.state.extras[1] = '';
        } else {
            if (this.state.extras[1] === option) {
                this.state.extras[1] = '';
            } else {
                if (this.state.extras[0] === '') {
                    this.state.extras[0] = option;
                } else {
                    if (this.state.extras[1] === '') {
                        this.state.extras[1] = option;
                    } else {
                        if (
                            this.state.extras[0] !== option &&
                            this.state.extras[1] !== option
                        ) {
                            this.state.extras[0] = this.state.extras[1];
                            this.state.extras[1] = option;
                        }
                    }
                }
            }
        }
    }

    async requestMultipart(edo) {
        let lasFaltas2 =
            this.state.lasFaltas[0] +
            ', ' +
            this.state.lasFaltas[1] +
            ', ' +
            this.state.lasFaltas[2];
        let extras2 = this.state.extras[0] + ', ' + this.state.extras[1];
        if (this.state.laMateria === '') {
        } else {
            try {
                let formData = new FormData();
                formData.append(
                    'new',
                    JSON.stringify({
                        ciclo: this.state.elCiclo,
                        descripcion: this.state.laDescripcion,
                        estado: edo,
                        extras: extras2,
                        id_alumno: this.state.elAlumno,
                        id_materia: this.state.laMateria,
                        periodo: this.state.elPeriodo,
                        tipo_incidencia: this.state.tipo,
                        tipos_faltas: lasFaltas2
                    })
                );

                let request = await fetch(this.state.uri + '/api/reporte', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                        Authorization: 'Bearer ' + this.state.token
                    },
                    body: formData
                });
                Alert.alert('¡Felicidades!', 'Se ha mandado su reporte', [
                    {
                        text: 'Entendido',
                        onPress: () => Actions.drawer()
                    }
                ]);
            } catch (e) {
                console.warn(e);
                Alert.alert('Error', 'Intenta más tarde por favor', [
                    {
                        text: 'Entendido',
                        onPress: () => Actions.drawer()
                    }
                ]);
            }
        }
    }

    render() {
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + ' ' + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        const alumnos = this.renderAlumnos();
        const hoy = conductaM.fecha();
        const incidencias = this.renderIncidencias();
        const tipos = this.renderTipos();
        return (
            <View style={styles.container} behavior="position">
                <View
                    style={[
                        styles.row_RT,
                        {
                            backgroundColor: this.state.fourthColor
                        }
                    ]}>
                    <Text style={styles.blanco}>{hoy}</Text>
                </View>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <KeyboardAvoidingView>
                        <Text style={styles.main_title}>Seleccione la materia</Text>
                        <View style={styles.row}>
                            <View>
                                <Text style={styles.label}>Campus</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>01</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Nivel</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>SEC</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Materia</Text>
                                <ModalSelector
                                    cancelText="Cancelar"
                                    data={data}
                                    initValue="Seleccione la materia"
                                    onChange={option => this.onChange(option)}
                                    optionTextStyle={{color: this.state.thirdColor}}
                                    selectStyle={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn3_5,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                            </View>
                        </View>
                        <Grupo
                            urlGrupo={this.state.urlGp}
                            onListItemPressedGrupos={(indexBtn, itemBtn) =>
                                this.onListItemPressedGrupos(indexBtn, itemBtn)
                            }
                            todos={'0'}
                        />
                        <Text style={styles.main_title}>Seleccione el alumno</Text>
                        <View style={styles.contAlumnos}>
                            <ScrollView
                                horizontal={false}
                                showsVerticalScrollIndicator={false}
                                style={[
                                    styles.titulosContainer,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                {alumnos}
                            </ScrollView>
                        </View>
                        <Text
                            style={[
                                styles.main_title,
                                {
                                    color: this.state.thirdColor
                                }
                            ]}>
                            Seleccione el tipo de incidencia a reportar
                        </Text>
                        <View style={styles.botones_C}>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}>
                                {tipos}
                            </ScrollView>
                        </View>
                        <Text
                            style={[
                                styles.main_title,
                                {
                                    color: this.state.thirdColor
                                }
                            ]}>
                            Seleccione los tipos de faltas a reportar
                        </Text>
                        <MultipleChoice
                            maxSelectedOptions={3}
                            onSelection={option => this._optionSelected(option)}
                            options={incidencias}
                        />

                        <Text
                            style={[
                                styles.main_title,
                                {
                                    color: this.state.thirdColor
                                }
                            ]}>
                            Describa la situación a detalle
                        </Text>
                        <View style={styles.form}>
                            <TextInput
                                keyboardType="default"
                                multiline={true}
                                onChangeText={this.handleContenido}
                                returnKeyType="next"
                                style={[
                                    styles.inputContenido,
                                    {borderColor: this.state.secondColor}
                                ]}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                        <Text
                            style={[
                                styles.main_title,
                                {
                                    color: this.state.thirdColor
                                }
                            ]}>
                            Elija las opciones correspondientes
                        </Text>
                        <MultipleChoice
                            options={[
                                'El docente dialogó con el alumno',
                                'Se llegó a un acuerdo',
                                'El alumno se compromete a mejorar',
                                'Se habia dialogado con anterioridad y no ha mostrado mejoría'
                            ]}
                            maxSelectedOptions={2}
                            onSelection={option => this._optionSelected1(option)}
                        />
                        <View style={styles.theButtons}>
                            <TouchableOpacity
                                onPress={() => this.requestMultipart(0)}
                                style={styles.buttonAtencion}>
                                <Text style={styles.titBtnA}>
                                    Enviar como llamada de atención
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.requestMultipart(1)}
                                style={styles.buttonExpediente}>
                                <Text style={styles.titBtnE}>Enviar al expediente</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}
