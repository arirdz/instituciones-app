import React from "react";
import {AsyncStorage, View, Text} from "react-native";

export default class calendarioM extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async componentWillMount() {
        await this.getURL();
    }

    render() {
        return (
            <View>
                <Text>calendarioM</Text>
            </View>
        );
    }
}
