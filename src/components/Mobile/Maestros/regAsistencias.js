import React from 'react';
import {
	Alert,
	AsyncStorage,
	KeyboardAvoidingView,
	ScrollView,
	StatusBar,
	Text,
	TouchableOpacity,
	View
} from 'react-native';
import {Actions} from 'react-native-router-flux'
import MultiBotonRow from '../Globales/MultiBotonRow';
import styles from '../../styles';
import Switch from 'react-native-switch-pro';
import JustificarFalt from './JustificarFalt';
import {responsiveFontSize, responsiveWidth, responsiveHeight} from 'react-native-responsive-dimensions';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class regAsistencias extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			alumnos: [],
			elRequest: [],
			asistio: [],
			fecha1: '',
			aux: 0,
			isDateTimePickerVisible: false,
			botonSelected: '',
			botonSelected2: '',
			data: '',
			dataClass: [],
			dataTareas: [],
			elCiclo: '',
			elGrado: '',
			elGrupo: '',
			elPeriodo: '',
			faltas: [],
			grupos: [],
			justificado: [],
			horarios: [],
			indexSelectedTab: 0,
			indexSelectedTab2: 0,
			laDescripcion: '',
			laMateria: '',
			materias: [],
			retardos: [],
			selectedIndexBtn: 0,
			selectedIndexBtnTop: 0,
			tarde: [],
			tipo: '',
			check: '',
			value: true,
			escuelasDatos: [],
			nombrEsc: '',
			justif: [],
			faltasJust: []
		};
	}

	// ++++++++++++++++++++++++++++++ Render ++++++++++++++++++++++++++++++++++++
	static bloqueado() {
		Alert.alert(
			'¡No hay clase o ya pasó lista!',
			'No hay asistencias por registrar',
			[
				{
					text: 'Entendido'
				}
			]
		);
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		let schoolId = await AsyncStorage.getItem('schoolId');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri,
			schoolId: schoolId
		});
	}

	async componentWillMount() {
		await this.getURL();
		await this.getSchoolData();
		await this.getCicloActual();
		await this.getClaseActual();
		await this.getUserdata();
		await this.getFaltas();
		await this.checkAsistencia();

	}

//+++++++++++++++++++++++++++ Get User +++++++++++++++++++++++++++++++++++++++
	async getUserdata() {
		let request = await fetch(this.state.uri + '/api/user/data', {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let dato = await request.json();
		await this.setState({data: dato});
	}

	// ++++++++++++++++++++++++ Revisar si ya Paso Lista +++++++++++++++++++++++++
	async checkAsistencia() {
		let request = await fetch(
			this.state.uri +
			'/api/check/asistencia/' +
			this.state.laMateria +
			'/' +
			this.state.elPeriodo +
			'/' +
			this.state.elCiclo +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let check = await request.json();
		await this.setState({check: check[0]});
	}

	// ++++++++++++++++++++++++ Faltas de Alumnos ++++++++++++++++++++++++++++
	async getFaltas() {
		for (let i = 0; i < this.state.alumnos.length; i++) {
			let request = await fetch(
				this.state.uri +
				'/api/get/retardos/faltas/' +
				this.state.laMateria +
				'/' +
				this.state.alumnos[i].id +
				'/' +
				this.state.elPeriodo +
				'/' +
				this.state.elCiclo +
				'/' +
				this.state.elGrupo,
				{
					method: 'GET',
					headers: {
						Authorization: 'Bearer ' + this.state.token
					}
				}
			);
			let faltasRetardos = await request.json();
			// await this.setState({ faltas: faltasRetardos.Total });
			this.state.faltas[i] = await faltasRetardos.Faltas;
			this.state.retardos[i] = await faltasRetardos.Retardos;
			this.state.justificado[i] = await faltasRetardos.justificado;
		}
	}

	// ++++++++++++++++++++++++ Ciclo y Periodo Actual ++++++++++++++++++++++++++++
	async getCicloActual() {
		let cicloPicker = await fetch(
			this.state.uri + '/api/ciclo/periodo/actual',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let ciclosPicker = await cicloPicker.json();
		await this.setState({
			elCiclo: ciclosPicker[0].ciclo,
			elPeriodo: ciclosPicker[0].periodo
		});
	}

	// ++++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++++
	async getAlumnosByGG() {
		let alumnopicker = await fetch(
			this.state.uri +
			'/api/get/alumnos/by/grado/grupo/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let alumnospicker = await alumnopicker.json();
		this.setState({alumnos: alumnospicker});
	}

	async retardo(index) {
		if (this.state.tarde[index] === false) {
			this.state.tarde[index] = true;
		} else {
			this.state.tarde[index] = false;
		}
		await this.setState({aux: 0});
	}

	async asistenciar(index) {
		if (this.state.asistio[index] === false) {
			this.state.asistio[index] = true;
		} else {
			this.state.asistio[index] = false;
			this.state.tarde[index] = false;
		}
		await this.setState({aux: 0});
	}

	isAsistio(itemAlumno, indexAlumno) {
		return (
			<View style={styles.campoTablaG}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View style={[styles.btn3_5]}>
						<Text>{itemAlumno.name}</Text>
					</View>
					<View>
						<Switch
							key={itemAlumno.id}
							value={this.state.asistio[indexAlumno]}
							onSyncPress={() => this.asistenciar(indexAlumno)}
						/>
					</View>
					<View>
						<View>
							{this.state.asistio[indexAlumno] === false ? (
								<Text style={[styles.btn6_ls, {textAlign: 'center'}]}>N/A</Text>
							) : (
								<Switch
									key={itemAlumno.id}
									value={this.state.tarde[indexAlumno]}
									onSyncPress={() => this.retardo(indexAlumno)}
								/>
							)}
						</View>
					</View>
				</View>
			</View>
		);
	}

	isFaltas(itemAlumno, indexAlumno) {
		return (
			<View style={styles.campoTablaG}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View>
						<Text>{itemAlumno.name}</Text>
					</View>

					<View style={[styles.row, styles.btn3_3]}>
						<Text>{this.state.justificado[indexAlumno]}</Text>
						<Text>{this.state.faltas[indexAlumno]}</Text>
					</View>

				</View>
			</View>
		);
	}

	isRetardos(itemAlumno, indexAlumno) {
		return (
			<View style={styles.campoTablaG}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View>
						<Text>{itemAlumno.name}</Text>
					</View>
					<View style={styles.btn6_6}>
						<Text>{this.state.retardos[indexAlumno]}</Text>
					</View>
				</View>
			</View>
		);
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={tabRow}
			>
				{this.state.indexSelectedTab === 0
					? this.isAsistio(itemAlumno, indexAlumno)
					: null}
				{this.state.indexSelectedTab === 1
					? this.isFaltas(itemAlumno, indexAlumno)
					: null}
				{this.state.indexSelectedTab === 2
					? this.isRetardos(itemAlumno, indexAlumno)
					: null}
			</View>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}


	async getSchoolData() {
		let schoolData = await fetch(this.state.uri + '/api/get/escuelas/datos/' + this.state.schoolId);
		let scData = await schoolData.json();
		await this.setState({escuelasDatos: scData});
		await this.setState({nombrEsc: this.state.escuelasDatos[0].nombre_escuela});
	}

	// ++++++++++++++++++++++++++++++++ get Clase ++++++++++++++++++++++++++++++++
	async getClaseActual() {
		let datoClase = await fetch(this.state.uri + '/api/get/clase/actual', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let datosClase = await datoClase.json();
		this.setState({dataClass: datosClase[0]});
		if (this.state.dataClass === undefined) {
			Alert.alert('¡Atencion!', 'No hay clases disponibles');
		} else {
			await this.setState({horarios: this.state.dataClass.horarios});
			await this.setState({materias: this.state.dataClass.materias});
			await this.setState({elGrado: this.state.dataClass.grado});
			await this.setState({elGrupo: this.state.dataClass.grupo});
			await this.setState({laMateria: this.state.dataClass.materias.id});
			await this.getAlumnosByGG();
		}
		this.state.alumnos.forEach(() => {
			this.state.asistio.push(true);
			this.state.justif.push(true);
			this.state.faltas.push(0);
			this.state.tarde.push(false);
			this.state.retardos.push(1);
		});
	}

	renderClass() {
		return (
			<View style={styles.centered_RT}>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					En este momento su clase es:
				</Text>
				<View
					style={[
						styles.row,
						styles.rowInfo,
						{
							backgroundColor: this.state.fourthColor,
						}
					]}
				>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text style={{fontWeight: '800'}}>Materia: </Text>
					</View>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo2,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text style={[styles.textCenter,{fontSize:responsiveFontSize(1.5)}]}>
							{this.state.dataClass !== undefined ? this.state.materias.nombre : 'No hay clase'}
						</Text>
					</View>

				</View>
				<View
					style={[
						styles.row,
						styles.rowInfo,
						{
							backgroundColor: this.state.fourthColor,
						}
					]}
				>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text style={{fontWeight: '800'}}>Grupo: </Text>
					</View>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo2,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text style={[styles.textCenter,{fontSize:responsiveFontSize(1.5)}]}>
							{this.state.dataClass !== undefined ? this.state.dataClass.grupo : 'No hay clase'}
						</Text>
					</View>

				</View>
				<View
					style={[
						styles.row,
						styles.rowInfo,
						{
							backgroundColor: this.state.fourthColor
						}
					]}
				>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text style={{fontWeight: '800'}}>Escuela: </Text>
					</View>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo2,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text>{this.state.nombrEsc}</Text>
					</View>
				</View>

				<View
					style={[
						styles.row,
						styles.rowInfo,
						{
							backgroundColor: this.state.fourthColor,
							marginBottom: 10
						}
					]}
				>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text style={{fontWeight: '800'}}>Hora de clase: </Text>
					</View>
					<View
						style={[
							styles.listButton,
							styles.listButtonSmall,
							styles.colInfo2,
							{
								borderColor: this.state.fourthColor,
								backgroundColor: this.state.fourthColor
							}
						]}
					>
						<Text style={styles.textCenter}>{this.state.horarios.horas}</Text>
					</View>
				</View>
			</View>
		);
	}

	//+++++++++++++++++++++++++++++++++ Guardar ++++++++++++++++++++++++++++++++++

	async requestMultipart() {
		let formData = new FormData();
		for (let i = 0; i < this.state.alumnos.length; i++) {
			formData.append(
				'new',
				JSON.stringify({
					id_maestro: this.state.data.user_id,
					id_materia: this.state.laMateria,
					id_alumno: this.state.alumnos[i].id,
					grupo: this.state.elGrupo,
					ciclo: this.state.elCiclo,
					periodo: this.state.elPeriodo,
					asistencia: this.state.asistio[i],
					retardo: this.state.tarde[i],
					justificado: '0'
				})
			);
			await fetch(this.state.uri + '/api/create/asistencia', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data',
					Authorization: 'Bearer ' + this.state.token
				},
				body: formData
			}).then(res => res.json())
				.then(responseJson => {
					this.setState({elRequest: responseJson});
				});
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Ciclos periodos)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('¡Felicidades!', 'Se han guardado las asistencias', [
				{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}
			]);
		}
	}

	//++++++++++++++++++++++++++++ Selected Button +++++++++++++++++++++++++++++++
	async botonSelectedTab(indexSelected, itemSelected) {
		await this.setState({
			botonSelectedTab: itemSelected,
			indexSelectedTab: indexSelected
		});
	}

	async botonSelectedTab2(indexSelected, itemSelected) {
		await this.setState({
			botonSelectedTab2: itemSelected,
			indexSelectedTab2: indexSelected
		});
	}

	//+++++++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++

	renderPr() {
		const alumnos = this.renderAlumnos();
		const clase = this.renderClass();
		return (
			<View style={styles.container} behavior='position'>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				{clase}
				<MultiBotonRow
					itemBtns={[
						'Pasar\nlista',
						'Consultar\nfaltas',
						'Consultar\nretardos'
					]}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelectedTab(indexBtn, itemBtn)
					}
					cantidad={3}
				/>
				{this.state.indexSelectedTab === 1 ? (
					<View style={[styles.row, styles.widthall, {justifyContent: 'flex-end', marginTop: 9}]}>
						<Text style={{fontSize: responsiveFontSize(1.4)}}>Justificadas{'   '}</Text>
						<Text style={{fontSize: responsiveFontSize(1.4)}}>Totales{' '}</Text>
					</View>
				) : null}
				{this.state.indexSelectedTab === 0 ? (
					<View style={[styles.row, styles.widthall, {justifyContent: 'flex-end', marginTop: 9}]}>
						<Text style={{fontSize: responsiveFontSize(1.4)}}>Asistencia{'   '}</Text>
						<Text style={{fontSize: responsiveFontSize(1.4)}}>Retardo{'     '}</Text>
					</View>
				) : null}
				{this.state.indexSelectedTab === 2 ? (
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'flex-end',
						marginTop: 9,

					}]}>
						<Text style={{fontSize: responsiveFontSize(1.4)}}>Acumulados{'     '}</Text>
					</View>
				) : null}
				<ScrollView
					showsVerticalScrollIndicator={false}
					style={[styles.tabla, {
						borderColor: this.state.secondColor,
						height: responsiveHeight(20),
						width: responsiveWidth(94),
						marginBottom: this.state.indexSelectedTab !== 0 ? 15 : null
					}]}
				>
					{alumnos}
				</ScrollView>
				{this.state.indexSelectedTab === 0 ? (
					<View>
						{this.state.check === 'No' ? (
							<TouchableOpacity
								style={[
									styles.bigButton,
									{
										backgroundColor: this.state.secondColor
									}
								]}
								onPress={() => this.requestMultipart()}
							>
								<Text style={styles.textButton}>Guardar asistencias</Text>
							</TouchableOpacity>
						) : (
							<TouchableOpacity
								style={[
									styles.bigButton,
									{
										backgroundColor: this.state.secondColor
									}
								]}
								onPress={() => regAsistencias.bloqueado()}
							>
								<Text style={styles.textButton}>Guardar asistencias</Text>
							</TouchableOpacity>
						)}
					</View>
				) : null}


			</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<MultiBotonRow
					itemBtns={[
						'Registrar asistencia',
						'Justificar falta'
					]}
					onSelectedButton={(indexBtn, itemBtn) =>
						this.botonSelectedTab2(indexBtn, itemBtn)
					}
					cantidad={2}
				/>
				{this.state.indexSelectedTab2 === 0 ? this.renderPr() : (<JustificarFalt/>)}
			</View>
		)
	}
}
