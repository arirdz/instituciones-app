import React from "react";
import {
    AsyncStorage,
    StatusBar,
    View
} from "react-native";
import styles from "../../styles";
import AcumuladosCalificaciones from "./AcumuladosCalificaciones";

export default class modCalificaciones extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            indexSelected: 0
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");

        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async componentWillMount() {
        await this.getURL();
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <AcumuladosCalificaciones/>
            </View>
        );
    }
}
