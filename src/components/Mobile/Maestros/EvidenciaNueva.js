import React from "react";
import {
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import {Actions} from "react-native-router-flux";
import Entypo from "react-native-vector-icons/Entypo";
import ModalSelector from "react-native-modal-selector";
import Modal from "react-native-modal";
import MultiBotonRow from "../Globales/MultiBotonRow";
import styles from "../../styles";
import Grupo from "../Globales/Grupo";

export default class EvidenciaNueva extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alumnos: [],
            atiempo: [],
            botonSelected: "",
            comentarios: [],
            dataClass: [],
            elCiclo: "",
            elGrado: "",
            elGrupo: "",
            elPeriodo: "",
            evidencias: [],
            grupos: [],
            horarios: [],
            indexSelectedTab: 0,
            laDescripcion: "",
            laEvidencia: "",
            laMateria: "",
            materias: [],
            requisitos: [],
            selectedIndexBtn: 0,
            selectedIndexBtnTop: 0,
            calificacion: [],
            ponderaciones: [],
            value: true,
            selectedIndexGrupos: -1
        };
    }

    // ++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++
    static bloqueado() {
        Alert.alert("¡Ups!", "No hay Información", [
            {
                text: "Entendido"
            }
        ]);
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let token = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");

        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
        await this.getMaterias();
        await this.getCicloActual();
    }

    async componentWillMount() {
        await this.getURL();
    }

    //++++++++++++++++++++++++++++++++ Get Ciclo +++++++++++++++++++++++++++++++++
    async getCicloActual() {
        let cicloPicker = await fetch(
            this.state.uri + "/api/ciclo/periodo/actual",
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let ciclosPicker = await cicloPicker.json();
        await this.setState({
            elCiclo: ciclosPicker[0].ciclo,
            elPeriodo: ciclosPicker[0].periodo
        });
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            checkBoxBtns: [],
            checkBoxBtnsIndex: [],
            urlGp: "/api/grupos/materias/" + option.materia
        });
    }

    // ++++++++++++++++++++++++++++++++ GRUPOS +++++++++++++++++++++++++++++++++++

    async onListItemPressedGrupos(indexBtn, itemBtn) {
        await this.setState({
            selectedIndexGrupos: indexBtn,
            elGrupo: itemBtn
        });
        await this.getPonderaciones();
        await this.getAlumnosByGG();
        await this.getEvidencias();
    }

    // ++++++++++++++++++++++++++++++ Evidencias +++++++++++++++++++++++++++++++++

    async onChangeEvidencia(option) {
        await this.setState({
            laEvidencia: option.label
        });
    }

    async getEvidencias() {
        let evidenciapicker = await fetch(
            this.state.uri +
            "/api/get/evidencia/tipos/" +
            this.state.laMateria +
            "/" +
            this.state.elGrupo +
            "/" +
            this.state.elCiclo +
            "/" +
            this.state.elPeriodo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let evidenciaspicker = await evidenciapicker.json();
        this.setState({evidencias: evidenciaspicker});
    }

    // ++++++++++++++++++++++++++++++ Ponderaciones +++++++++++++++++++++++++++++++++

    async getPonderaciones() {
        let pondera = await fetch(
            this.state.uri +
            "/api/get/ponderaciones/" +
            this.state.laMateria +
            "/" +
            this.state.elPeriodo +
            "/" +
            this.state.elCiclo +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let ponderaciones = await pondera.json();
        this.setState({ponderaciones: ponderaciones});
        let l = this.state.ponderaciones.length - 1;
        let max = this.state.ponderaciones[l].valor;
        let min = this.state.ponderaciones[0].valor;
        this.setState({maximo: max, minimo: min});
    }

    // ++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/get/alumnos/by/grado/grupo/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
        this.state.alumnos.forEach(() => {
            this.state.calificacion.push(this.state.maximo);
            this.state.comentarios.push(null);
        });
        this.setState({alumnos: alumnospicker});
    }

    suma(indexAlumno) {
        if (this.state.calificacion[indexAlumno] < this.state.maximo) {
            this.state.calificacion[indexAlumno] =
                this.state.calificacion[indexAlumno] + 1;
            this.setState({indexSelectedTab: 1});
            this.setState({indexSelectedTab: 0});
        }
    }

    resta(indexAlumno) {
        if (this.state.calificacion[indexAlumno] > this.state.minimo) {
            this.state.calificacion[indexAlumno] =
                this.state.calificacion[indexAlumno] - 1;
            this.setState({indexSelectedTab: 1});
            this.setState({indexSelectedTab: 0});
        }
    }

    isValor(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View style={[styles.miniRow]}>
                        <TouchableOpacity onPress={() => this.resta(indexAlumno)}>
                            <Entypo name="minus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Text>{this.state.calificacion[indexAlumno]}</Text>
                        <TouchableOpacity onPress={() => this.suma(indexAlumno)}>
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    isComentario(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.setState({isModalVisible: true})}>
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Modal
                            isVisible={this.state.isModalVisible}
                            backdropOpacity={0.5}
                            animationIn={"bounceIn"}
                            animationOut={"bounceOut"}
                            animationInTiming={1000}
                            animationOutTiming={1000}>
                            <View style={[styles.container, {borderRadius: 6}]}>
                                <View style={styles.centered_RT}>
                                    <Text style={[styles.main_title, styles.modalWidth]}>
                                        Agrega un comentario
                                    </Text>
                                </View>
                                <TextInput
                                    keyboardType="default"
                                    maxLength={256}
                                    multiline={true}
                                    onChangeText={text =>
                                        (this.state.comentarios[indexAlumno] = text)
                                    }
                                    returnKeyType="next"
                                    underlineColorAndroid="transparent"
                                    value={this.state.comentarios[indexAlumno]}
                                    style={[
                                        styles.inputComentarios,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <TouchableOpacity
                                    style={[
                                        styles.bigButton,
                                        styles.modalWidth,
                                        {
                                            backgroundColor: this.state.mainColor,
                                            borderColor: this.state.mainColor
                                        }
                                    ]}
                                    onPress={() => this.setState({isModalVisible: false})}>
                                    <Text style={styles.textButton}>Aceptar</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}>
                {this.state.indexSelectedTab === 0
                    ? this.isValor(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 1
                    ? this.isComentario(itemAlumno, indexAlumno)
                    : null}
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //++++++++++++++++++++++++++++++++ Guardar +++++++++++++++++++++++++++++++++++

    async guardar() {
        if (this.state.laMateria === "") {
            Alert.alert("¡Ups!", "No ha seleccionado materia", [
                {text: "Entendido"}
            ]);
        } else if (this.state.elGrupo === "") {
            Alert.alert("¡Ups!", "No ha seleccionado un grupo", [
                {text: "Entendido"}
            ]);
        } else if (this.state.laEvidencia === "") {
            Alert.alert("¡Ups!", "No ha seleccionado el tipo de evidenica", [
                {text: "Entendido"}
            ]);
        } else if (this.state.laDescripcion === "") {
            Alert.alert("¡Ups!", "No ha descrito la evidencia", [
                {text: "Entendido"}
            ]);
        } else if (this.state.laDescripcion === "") {
            Alert.alert("¡Ups!", "No ha descrito la evidencia", [
                {text: "Entendido"}
            ]);
        } else {
            Alert.alert("¡Guardar!", "¿Seguro que desea continuar?", [
                {
                    text: "Si",
                    onPress: () => [this.requestMultipart()]
                },
                {text: "No"}
            ]);
        }
    }

    async requestMultipart() {
        try {
            for (let i = 0; i < this.state.alumnos.length; i++) {
                let formData = new FormData();
                formData.append(
                    "new",
                    JSON.stringify({
                        id_alumno: this.state.alumnos[i].id,
                        id_materia: this.state.laMateria,
                        grupo: this.state.elGrupo,
                        ciclo: this.state.elCiclo,
                        periodo: this.state.elPeriodo,
                        tipo: this.state.laEvidencia,
                        descripcion: this.state.laDescripcion,
                        calificacion: this.state.calificacion[i],
                        comentarios: this.state.comentarios[i]
                    })
                );
                let request = await fetch(this.state.uri + "/api/create/evidencia", {
                    method: "POST",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "multipart/form-data",
                        Authorization: "Bearer " + this.state.token
                    },
                    body: formData
                });
            }
            Alert.alert("¡Felicidades!", "Se ha publicado la evidencia", [
                {
                    text: "Entendido"
                }
            ]);
        } catch (e) {
            console.warn(e);
            Alert.alert("Error", "Intenta más tarde por favor", [
                {
                    text: "Entendido",
                    onPress: () => Actions.drawer()
                }
            ]);
        }
    }

    //++++++++++++++++++++++++++ Selected Button +++++++++++++++++++++++++++++++++
    async botonSelectedTab(indexSelected, itemSelected) {
        await this.setState({
            botonSelectedTab: itemSelected,
            indexSelectedTab: indexSelected
        });
    }

    //++++++++++++++++++++++++++++++ Input Descripcion ++++++++++++++++++++++++++

    handleContenido = text => {
        this.setState({laDescripcion: text});
    };

    //+++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++

    render() {
        const alumnos = this.renderAlumnos();
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        let dataEvidencia = this.state.evidencias.map((item, i) => {
            return {
                key: i,
                label: item.descripcion
            };
        });
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <KeyboardAvoidingView keyboardVerticalOffset={500}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la materia
                        </Text>
                        <View style={styles.row}>
                            <View>
                                <Text style={styles.label}>Campus</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>01</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Nivel</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>SEC</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Materia</Text>
                                <ModalSelector
                                    cancelText="Cancelar"
                                    data={data}
                                    initValue="Seleccione la materia"
                                    onChange={option => this.onChange(option)}
                                    optionTextStyle={{color: this.state.thirdColor}}
                                    selectStyle={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn3_5,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                            </View>
                        </View>
                        <Grupo
                            urlGrupo={this.state.urlGp}
                            onListItemPressedGrupos={(indexBtn, itemBtn) =>
                                this.onListItemPressedGrupos(indexBtn, itemBtn)
                            }
                            todos={"0"}
                        />
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione el tipo de evidencia
                        </Text>
                        <ModalSelector
                            cancelText="Cancelar"
                            data={dataEvidencia}
                            initValue="Seleccione el tipo de evidencia"
                            onChange={option => this.onChangeEvidencia(option)}
                            optionTextStyle={{color: this.state.thirdColor}}
                            selectStyle={[
                                styles.listButton,
                                styles.listButtonSmall,
                                {borderColor: this.state.secondColor}
                            ]}
                        />
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Descripción de la evidencia
                        </Text>
                        <View style={[styles.form, {marginBottom: 10}]}>
                            <TextInput
                                keyboardType="default"
                                returnKeyType="next"
                                multiline={true}
                                underlineColorAndroid="transparent"
                                onChangeText={this.handleContenido}
                                style={[
                                    styles.inputContenido,
                                    {borderColor: this.state.secondColor, height: 130}
                                ]}
                            />
                        </View>
                        <MultiBotonRow
                            itemBtns={["Asignar un valor", "Añadir comentario"]}
                            onSelectedButton={(indexBtn, itemBtn) =>
                                this.botonSelectedTab(indexBtn, itemBtn)
                            }
                            cantidad={2}
                        />
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor}]}>
                            {alumnos}
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: this.state.secondColor,
                            marginBottom: 84
                        }
                    ]}
                    onPress={() => this.guardar()}>
                    <Text style={styles.textButton}>Guardar revisión</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
