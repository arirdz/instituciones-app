import React from "react";
import {
    AsyncStorage,
    Keyboard,
    KeyboardAvoidingView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import Grupo from "../Globales/Grupo";
import Periodos from "../Globales/Periodos";
import ModalSelector from "react-native-modal-selector";
import styles from "../../styles";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class IRecuperacion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            textInputValue: "",
            elMes: "",
            cososPicker: [],
            mes: moment().month() + 1,
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexGrupos: -1,
            selectedIndex: -1,
            periodos: [],
            materias: [],
            elCiclo: "",
            elGrupo: "",
            ciclos: [],
            selectedIndexCiclo: 0,
            laMateria: "",
            elExamen: "",
            maximo: "",
            alumnos: [],
            valores: "",
            aciertos: [],
            calificacion: [],
            c: "",
            elPeriodo: ""
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getMaterias();
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Grupos +++++++++++++++++++++++++++++++++++++++++++++++

    async onListItemPressedGrupos(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexGrupos: indexSelected,
            elGrupo: itemSelected
        });
        await this.getAlumnosByGG();
        this.state.alumnos.forEach(() => {
            this.state.calificacion.push(0);
            this.state.aciertos.push(0);
        });
    }

    //++++++++++++++++++++++++++++++++++ Periodos ++++++++++++++++++++++++++++++++

    async onListItemPressedPeriodo(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexPeriodo: indexSelected,
            elPeriodo: itemSelected
        });
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            selectedIndexGrupos: -1,
            urlGp: "/api/grupos/materias/" + option.materia
        });
    }

    async onChange1(option) {
        await this.setState({
            elExamen: option.id
        });
        await this.getValores();
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Valores +++++++++++++++++++++++++++++++++++++++++++++++
    async getValores() {
        let request = await fetch(
            this.state.uri +
            "/api/get/valor/examen/" +
            this.state.laMateria +
            "/" +
            this.state.elPeriodo +
            "/17-18/" +
            this.state.elGrupo +
            "/" +
            this.state.elExamen,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let res = await request.json();
        await this.setState({valores: res[0].valor});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Alumnos +++++++++++++++++++++++++++++++++++++++++++++++

    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/get/alumnos/by/grado/grupo/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                <View style={styles.campoTablaG}>
                    <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                        <View style={[styles.btn3_5, styles.centerContent]}>
                            <Text>{itemAlumno.name}</Text>
                        </View>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <TextInput
                                key={indexAlumno}
                                keyboardType="numeric"
                                maxLength={2}
                                multiline={false}
                                value={this.state.aciertos[indexAlumno]}
                                onChangeText={text => [
                                    (this.state.aciertos[indexAlumno] = text),
                                    this.onChangeAciertos(indexAlumno)
                                ]}
                                onEndEditing={() => Keyboard.dismiss()}
                                returnKeyType="done"
                                underlineColorAndroid="transparent"
                                style={[
                                    styles.listButton,
                                    styles.btn6,
                                    styles.txtCenter,
                                    {borderColor: this.state.secondColor}
                                ]}
                            />
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <Text>{this.state.calificacion[indexAlumno]}%</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async onChangeAciertos(indexAlumno) {
        let b = this.state.maximo;
        if (
            parseInt(this.state.aciertos[indexAlumno]) > parseInt(this.state.maximo)
        ) {
            this.state.aciertos[indexAlumno] = b;
        }
        let a =
            parseInt(this.state.aciertos[indexAlumno]) *
            100 /
            parseInt(this.state.maximo) *
            (parseInt(this.state.valores) / 100);
        this.state.calificacion[indexAlumno] = a;
        this.setState({c: 0});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++++++++++

    render() {
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        const alumnos = this.renderAlumnos();
        let index = 0;
        const data1 = [
            {key: index++, label: "Parcial", id: "Parcial"},
            {key: index++, label: "Bimestral", id: "Bimestral"}
        ];
        return (
            <KeyboardAvoidingView style={styles.container} behavior={"padding"}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Periodos
                        onListItemPressedPeriodo={(indexBtn, itemBtn) =>
                            this.onListItemPressedPeriodo(indexBtn, itemBtn)
                        }
                    />
                    <Text style={styles.main_title}>Seleccione la materia</Text>
                    <ModalSelector
                        cancelText="Cancelar"
                        data={data}
                        initValue="Seleccione la materia"
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                    />
                    <Grupo
                        urlGrupo={this.state.urlGp}
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                    />
                    <Text style={styles.main_title}>Seleccione el examen a evaluar</Text>
                    <View style={styles.row}>
                        <ModalSelector
                            cancelText="Cancelar"
                            data={data1}
                            initValue="Seleccione el examen"
                            onChange={option => this.onChange1(option)}
                            optionTextStyle={{color: this.state.thirdColor}}
                            selectStyle={[
                                styles.inputPicker,
                                {borderColor: this.state.secondColor}
                            ]}
                        />
                    </View>

                    <View style={[styles.row, {justifyContent: "center"}]}>
                        <View
                            style={[
                                styles.btn3,
                                {justifyContent: "center", alignItems: "center"}
                            ]}
                        >
                            <Text>Valor</Text>
                        </View>
                        <View
                            style={[
                                styles.btn3,
                                {justifyContent: "center", alignItems: "center"}
                            ]}
                        >
                            <Text>Máximo</Text>
                        </View>
                    </View>
                    <View style={[styles.row_v3, {justifyContent: "center"}]}>
                        <View
                            style={[
                                styles.btn3,
                                styles.cuadroEstd,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    borderColor: "#fff"
                                }
                            ]}
                        >
                            <Text>{this.state.valores}%</Text>
                        </View>
                        <TextInput
                            keyboardType="numeric"
                            maxLength={2}
                            multiline={false}
                            placeholder={"Núm. Aciertos"}
                            onChangeText={text => this.setState({maximo: text})}
                            onEndEditing={() => Keyboard.dismiss()}
                            returnKeyType="done"
                            underlineColorAndroid="transparent"
                            style={[
                                styles.btn3,
                                styles.cuadroEstd,
                                styles.txtCenter,
                                {backgroundColor: this.state.fourthColor, borderColor: "#fff"}
                            ]}
                        />
                    </View>

                    <View
                        underlayColor={this.state.secondColor}
                        style={[styles.rowTabla]}
                    >
                        <View style={styles.campoTablaG}>
                            <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                                <View style={[styles.btn3_5, styles.centered_RT]}/>
                                <View style={[styles.btn5, styles.centered_RT]}>
                                    <Text>Aciertos</Text>
                                </View>
                                <View style={[styles.btn6, styles.centered_RT]}>
                                    <Text>Calif. </Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View
                        style={[
                            styles.tabla,
                            {borderColor: this.state.secondColor, marginBottom: 10}
                        ]}
                    >
                        {alumnos}
                    </View>
                </ScrollView>
                <View style={[styles.row, {marginBottom: 78}]}>
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {backgroundColor: this.state.secondColor}
                        ]}
                    >
                        <Text style={styles.textButton}>Guardar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {
                                backgroundColor: this.state.secondColor,
                                borderLeftWidth: 1,
                                borderColor: "white"
                            }
                        ]}
                    >
                        <Text style={styles.textButton}>Solicitar revisión</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
