import React from "react";
import {
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    View
} from "react-native";
import ModalSelector from "react-native-modal-selector";
import styles from "../../styles";

export default class AcumuladosTareas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alumnos: [],
            dataClass: [],
            dataTareas: [],
            elCiclo: "",
            elGrado: "",
            elGrupo: "",
            elPeriodo: "",
            grupos: [],
            horarios: [],
            laMateria: "",
            laTarea: "",
            materias: [],
            requisitos: [],
            selectedIndexBtn: 0,
            selectedIndexBtnTop: 0,
            tipo: "",
            valor: [],
            value: true
        };
    }

    // ++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++
    static bloqueado() {
        Alert.alert("¡No hay tarea!", "No hay tareas que revisar", [
            {
                text: "Entendido"
            }
        ]);
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let token = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");

        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
        await this.getMaterias();
        await this.getCicloActual();
    }

    async componentWillMount() {
        await this.getURL();
    }

    //+++++++++++++++++++++++++++++++++++all tareas+++++++++++++++++++++++++++++++++
    async getAllTareas() {
        let tarea = await fetch(this.state.uri + "/api/tareas/porcentaje/", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer" + this.state.token
            }
        });
        let alltareas = await tarea.json();
        await this.setState({laTarea: alltareas});
    }

    //++++++++++++++++++++++++++++++++ Get Ciclo +++++++++++++++++++++++++++++++++
    async getCicloActual() {
        let cicloPicker = await fetch(
            this.state.uri + "/api/ciclo/periodo/actual/1",
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let ciclosPicker = await cicloPicker.json();
        await this.setState({
            elCiclo: ciclosPicker[0].ciclo,
            elPeriodo: ciclosPicker[0].periodo
        });
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            checkBoxBtns: [],
            checkBoxBtnsIndex: []
        });
        await this.getGrupos();
    }

    // ++++++++++++++++++++++++++++++++ GRUPOS +++++++++++++++++++++++++++++++++++

    async onListItemPressedGrupo(index, grupo) {
        await this.setState({selectedIndexGrupo: index, elGrupo: grupo});
        await this.getAlumnosByGG();
        await this.getEvidencias();
    }

    async getGrupos() {
        let grupopicker = await fetch(
            this.state.uri + "/api/grupos/materias/" + this.state.laMateria,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let grupospicker = await grupopicker.json();
        this.setState({grupos: grupospicker});
    }

    renderGrupo(itemGrupo, indexGrupo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexGrupo === indexGrupo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexGrupo}
                style={smallButtonStyles}
                underlayColor={this.state.secondColor}
                onPress={() =>
                    this.onListItemPressedGrupo(indexGrupo, itemGrupo.grupo)
                }>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemGrupo.grupo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderGrupos() {
        let buttonsGrupos = [];
        this.state.grupos.forEach((itemGrupo, indexGrupo) => {
            buttonsGrupos.push(this.renderGrupo(itemGrupo, indexGrupo));
        });
        return buttonsGrupos;
    }

    // ++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/get/alumnos/by/grado/grupo/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        const lista = this.isValor(itemAlumno, indexAlumno);
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}>
                {lista}
            </View>
        );
    }

    isValor(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View style={{marginRight: 25}}>
                        <Text>5</Text>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //+++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++

    render() {
        const alumnos = this.renderAlumnos();
        const grupos = this.renderGrupos();
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <KeyboardAvoidingView keyboardVerticalOffset={500}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la materia
                        </Text>
                        <View style={styles.row}>
                            <View>
                                <Text style={styles.label}>Campus</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>01</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Nivel</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}>
                                    <Text>SEC</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Materia</Text>
                                <ModalSelector
                                    cancelText="Cancelar"
                                    data={data}
                                    initValue="Seleccione la materia"
                                    onChange={option => this.onChange(option)}
                                    optionTextStyle={{color: this.state.thirdColor}}
                                    selectStyle={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn3_5,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                            </View>
                        </View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione el grupo
                        </Text>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                            {grupos}
                        </ScrollView>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Concetrado de tareas
                        </Text>
                        <View style={[styles.row, {paddingBottom: 10}]}>
                            <View style={styles.cuadro}>
                                <Text
                                    style={[styles.titBtn, {color: this.state.secondColor}]}>
                                    Valor de las tareas
                                </Text>
                                <View
                                    style={[
                                        styles.buttonEnterados,
                                        {backgroundColor: this.state.fourthColor},
                                        styles.cuadro1
                                    ]}>
                                    <Text
                                        style={[styles.text_VE, {color: this.state.mainColor}]}>
                                        20%
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.cuadro}>
                                <Text
                                    style={[styles.titBtn, {color: this.state.secondColor}]}>
                                    Máximo de grupo
                                </Text>
                                <View
                                    style={[
                                        styles.buttonEnterados,
                                        {backgroundColor: this.state.mainColor},
                                        styles.cuadro1
                                    ]}>
                                    <Text
                                        style={[styles.text_VE, {color: this.state.fourthColor}]}>
                                        15
                                    </Text>
                                </View>
                            </View>
                        </View>

                        <View
                            style={[
                                styles.tabla,
                                {borderColor: this.state.secondColor, marginBottom: 100}
                            ]}>
                            {alumnos}
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}
