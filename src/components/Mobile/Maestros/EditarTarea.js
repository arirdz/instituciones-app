import React from "react";
import {
    Alert,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from "react-native";
import styles from "../../styles";
import ModalSelector from "react-native-modal-selector";
import {Actions} from "react-native-router-flux";
import Switch from "react-native-switch-pro";
import Entypo from "react-native-vector-icons/Entypo";
import MultiBotonRow from "../Globales/MultiBotonRow";
import Grupo from "../Globales/Grupo";
import Modal from "react-native-modal";

export default class EditarTarea extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alumnos: [],
            atiempo: [],
            comentarios: [],
            dataTareas: [],
            elGrado: "",
            elGrupo: "",
            grupos: [],
            laDescripcion: "",
            laMateria: "",
            laTarea: "",
            materias: [],
            requisitos: [],
            selectedIndexBtn: "",
            selectedIndexBtnTop: 1,
            selectedIndexGrupos: "",
            selectedIndexTarea: -1,
            indexSelectedTab: 0,
            indexSelected: 0,
            tareas: [],
            tipo: "",
            tareitas: [],
            valor: []
        };
    }

    static bloqueado() {
        Alert.alert("¡No hay tarea!", "Seeleccione una tarea", [
            {
                text: "Entendido"
            }
        ]);
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let token = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");

        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
        await this.getMaterias();
    }

    async componentWillMount() {
        await this.getURL();
    }

    //++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            selectedIndexGrupos: -1,
            urlGp: "/api/grupos/materias/" + option.materia
        });
    }

    // +++++++++++++++++++++++++++++++++ GRUPOS ++++++++++++++++++++++++++++++++++

    async onListItemPressedGrupos(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexGrupos: indexSelected,
            elGrupo: itemSelected
        });
        await this.getAllTareas();
    }

    //++++++++++++++++++++++++++++++++++ get All Tareas ++++++++++++++++++++++++
    async getAllTareas() {
        let tareaList = await fetch(
            this.state.uri +
            "/api/all/tareas/revisadas/" +
            this.state.laMateria +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let tareasList = await tareaList.json();
        this.setState({tareitas: [tareasList]});
    }

    async onListPressedTarea(indexTarea, tarea, grupo) {
        this.state.elGrupo = grupo;
        this.setState({
            selectedIndexTarea: indexTarea,
            laTarea: tarea,
            elGrupo: grupo
        });
        await this.getAlumnosByGG();
        this.state.alumnos.forEach(() => {
            this.state.atiempo.push(true);
            this.state.requisitos.push(true);
            this.state.valor.push(10);
            this.state.comentarios.push(null);
        });
    }

    renderTarea(itemTarea, indexTarea) {
        let smallButtonStyles = [
            styles.listButtonAsunto,
            styles.listButtonSmall,
            styles.btn1,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexTarea === indexTarea) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexTarea}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() =>
                    this.onListPressedTarea(indexTarea, itemTarea.id, itemTarea.grupo)
                }
            >
                <View style={styles.listItem}>
                    <Text style={texto}>{itemTarea.descripcion}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderTareas() {
        let buttonsTareas = [];
        this.state.tareitas.forEach((itemTarea, indexTarea) => {
            buttonsTareas.push(this.renderTarea(itemTarea, indexTarea));
        });
        return buttonsTareas;
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/get/alumnos/by/grado/grupo/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    isATiempo(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <Switch
                            key={itemAlumno.id}
                            value={this.state.atiempo[indexAlumno]}
                            onSyncPress={() => {
                                if (this.state.atiempo[indexAlumno] === false) {
                                    this.state.atiempo[indexAlumno] = true;
                                } else {
                                    this.state.atiempo[indexAlumno] = false;
                                }
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }

    isRequisitos(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <Switch
                            key={itemAlumno.id}
                            value={this.state.requisitos[indexAlumno]}
                            onSyncPress={() => {
                                if (this.state.requisitos[indexAlumno] == false) {
                                    this.state.requisitos[indexAlumno] = true;
                                } else {
                                    this.state.requisitos[indexAlumno] = false;
                                }
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }

    suma(indexAlumno) {
        if (this.state.valor[indexAlumno] < 10) {
            this.state.valor[indexAlumno] = this.state.valor[indexAlumno] + 1;
            this.setState({indexSelectedTab: 1});
            this.setState({indexSelectedTab: 2});
        }
    }

    resta(indexAlumno) {
        if (this.state.valor[indexAlumno] > 0) {
            this.state.valor[indexAlumno] = this.state.valor[indexAlumno] - 1;
            this.setState({indexSelectedTab: 1});
            this.setState({indexSelectedTab: 2});
        }
    }

    isValor(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View style={[styles.miniRow]}>
                        <TouchableOpacity onPress={() => this.resta(indexAlumno)}>
                            <Entypo name="minus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Text>{this.state.valor[indexAlumno]}</Text>
                        <TouchableOpacity onPress={() => this.suma(indexAlumno)}>
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    isComentario(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.setState({isModalVisible: true})}
                        >
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Modal
                            isVisible={this.state.isModalVisible}
                            backdropOpacity={0.5}
                            animationIn={"bounceIn"}
                            animationOut={"bounceOut"}
                            animationInTiming={1000}
                            animationOutTiming={1000}
                        >
                            <View style={[styles.container, {borderRadius: 6}]}>
                                <View style={styles.centered_RT}>
                                    <Text style={[styles.main_title, styles.modalWidth]}>
                                        Agrega un comentario
                                    </Text>
                                </View>
                                <TextInput
                                    keyboardType="default"
                                    maxLength={256}
                                    multiline={true}
                                    onChangeText={text =>
                                        (this.state.comentarios[indexAlumno] = text)
                                    }
                                    returnKeyType="next"
                                    underlineColorAndroid="transparent"
                                    value={this.state.comentarios[indexAlumno]}
                                    style={[
                                        styles.inputComentarios,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                                <TouchableOpacity
                                    style={[
                                        styles.bigButton,
                                        styles.modalWidth,
                                        {
                                            backgroundColor: this.state.mainColor,
                                            borderColor: this.state.mainColor
                                        }
                                    ]}
                                    onPress={() => this.setState({isModalVisible: false})}
                                >
                                    <Text style={styles.textButton}>Aceptar</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {this.state.indexSelectedTab === 0
                    ? this.isATiempo(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 1
                    ? this.isRequisitos(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 2
                    ? this.isValor(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 3
                    ? this.isComentario(itemAlumno, indexAlumno)
                    : null}
            </View>
        );
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++ get Tareas ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // async getTarea() {
    // 	let datoTarea = await fetch(
    // 		this.state.uri +
    // 		'/api/get/tarea/' +
    // 		this.state.laMateria +
    // 		'/' +
    // 		this.state.elGrupo,
    // 		{
    // 			method: 'GET',
    // 			headers: {
    // 				'Content-Type': 'application/json',
    // 				Authorization: 'Bearer ' + this.state.token
    // 			}
    // 		}
    // 	);
    // 	let datosTareas = await datoTarea.json();
    // 	this.setState( { dataTareas: datosTareas[ 0 ] } );
    // 	this.setState( { laTarea: datosTareas[ 0 ].id } );
    // }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //
    renderTareas2() {
        return (
            <View style={styles.centered_RT}>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Re-evaluar tarea
                </Text>
            </View>
        );
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Guardar ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async requestMultipart() {
        try {
            for (let i = 0; i < this.state.alumnos.length; i++) {
                let formData = new FormData();
                formData.append(
                    "new",
                    JSON.stringify({
                        id_tarea: this.state.laTarea,
                        id_alumno: this.state.alumnos[i].id,
                        atiempo: this.state.atiempo[i],
                        requisitos: this.state.requisitos[i],
                        valor: this.state.valor[i],
                        comentarios: this.state.comentarios[i]
                    })
                );
                let request = await fetch(
                    this.state.uri +
                    "/api/revisar/tarea/update/" +
                    this.state.laTarea +
                    "/" +
                    this.state.alumnos[i].id,
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "multipart/form-data",
                            Authorization: "Bearer " + this.state.token
                        },
                        body: formData
                    }
                );
            }

            Alert.alert("¡Felicidades!", "Se ha publicado la calificación", [
                {
                    text: "Entendido",
                    onPress: () => Actions.drawer()
                }
            ]);
        } catch (e) {
            console.warn(e);
            Alert.alert("Error", "Intenta más tarde por favor", [
                {
                    text: "Entendido",
                    onPress: () => Actions.drawer()
                }
            ]);
        }
    }

    //++++++++++++++++++++++++++++ Selected Button +++++++++++++++++++++++++++++++
    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
        if (this.state.indexSelected === 0) {
            Actions.push("revisarTarea");
        } else if (this.state.indexSelected === 1) {
            Actions.push("editarTarea");
        } else {
            Actions.push("acumuladosTareas");
        }
    }

    async botonSelectedTab(indexSelected, itemSelected) {
        await this.setState({
            botonSelectedTab: itemSelected,
            indexSelectedTab: indexSelected
        });
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    render() {
        const alumnos = this.renderAlumnos();
        const tareas = this.renderTareas();
        const tareas2 = this.renderTareas2();
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        return (
            <View style={styles.container} behavior="position">
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <KeyboardAvoidingView>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la materia
                        </Text>
                        <View style={styles.row}>
                            <View>
                                <Text style={styles.label}>Campus</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}
                                >
                                    <Text>01</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Nivel</Text>
                                <View
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn5,
                                        styles.disabled
                                    ]}
                                >
                                    <Text>SEC</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.label}>Materia</Text>
                                <ModalSelector
                                    cancelText="Cancelar"
                                    data={data}
                                    initValue="Seleccione la materia"
                                    onChange={option => this.onChange(option)}
                                    optionTextStyle={{color: this.state.thirdColor}}
                                    selectStyle={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn3_5,
                                        {borderColor: this.state.secondColor}
                                    ]}
                                />
                            </View>
                        </View>
                        <Grupo
                            urlGrupo={this.state.urlGp}
                            onListItemPressedGrupos={(indexBtn, itemBtn) =>
                                this.onListItemPressedGrupos(indexBtn, itemBtn)
                            }
                            todos={"0"}
                        />
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la tarea
                        </Text>
                        <View style={styles.contAlumnos}>
                            <ScrollView
                                horizontal={false}
                                showsVerticalScrollIndicator={false}
                                style={[
                                    styles.titulosContainer,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                {tareas}
                            </ScrollView>
                        </View>
                        {tareas2}
                        <MultiBotonRow
                            itemBtns={[
                                "Hecho a tiempo",
                                "Cumple requisitos",
                                "Asignar un valor",
                                "Añadir comentario"
                            ]}
                            onSelectedButton={(indexBtn, itemBtn) =>
                                this.botonSelectedTab(indexBtn, itemBtn)
                            }
                            cantidad={4}
                        />
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor}]}
                        >
                            {alumnos}
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                {this.state.laTarea !== 0 ? (
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.secondColor,
                                marginBottom: 84
                            }
                        ]}
                        onPress={() => this.requestMultipart()}
                    >
                        <Text style={styles.textButton}>Guardar edición</Text>
                    </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.secondColor,
                                marginBottom: 84
                            }
                        ]}
                        onPress={() => EditarTarea.bloqueado()}
                    >
                        <Text style={styles.textButton}>Guardar edición</Text>
                    </TouchableOpacity>
                )}
            </View>
        );
    }
}
