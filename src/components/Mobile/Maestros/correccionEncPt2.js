import React from 'react';
import {
    Alert,
    AsyncStorage,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import Modal from 'react-native-modal';
import Spinner from 'react-native-loading-spinner-overlay';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import {Actions} from 'react-native-router-flux';

export default class correccionEncPt2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lasPonderaciones: [],
            descripPond: [],
            valorPond: [],
            data: [],
            encUpdt: [],
            visible: false,
            criterios: '',
            criteriosValue: '',
            elCiclo: '',
            elGrado: '',
            elGrupo: '',
            elPeriodo: '',
            elTotal: 0,
            encuadre: [],
            laMateria: '',
            isModalRubro: [],
            modalPonder: [],
            elRequest: [],
            elIdMateria: '',
            elidMaestro: '',
            idEncuadre: [],
            newPonderac: [
                {'descripcion': 'No lo hizo', 'valor': '0'},
                {'descripcion': 'Muy mal', 'valor': '1'},
                {'descripcion': 'Regular', 'valor': '2'},
                {'descripcion': 'Bien', 'valor': '3'},
                {'descripcion': 'Muy bien', 'valor': '4'},
                {'descripcion': 'Excelente', 'valor': '5'}
            ],
            aux: 0,
            newTipoP: 0,
        };
    }

    _changeWheelState = () => {
        this.setState({visible: !this.state.visible});
    };

    async componentWillMount() {
        await this.getURL();
        await this.getUserdata();
    }

    async getUserdata() {
        let request = await fetch(this.state.uri + '/api/user/data', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let dato = await request.json();
        await this.setState({data: dato});
        await this.setState({elidMaestro: this.state.data.user_id});
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async componentDidMount() {
        await this.changes();
        await this.onChangeCriValue();
    }

    async changes() {
        this.state.encuadre = this.props.corregir;
        const y = this.state.encuadre.length;
        for (let i = 0; i < y; i++) {
            for (let j = 0; j < y - 1 - i; j++) {
                if (this.state.encuadre[j].valor < this.state.encuadre[j + 1].valor) {
                    [this.state.encuadre[j], this.state.encuadre[j + 1]] = [
                        this.state.encuadre[j + 1],
                        this.state.encuadre[j]
                    ];
                }
            }
        }
        this.state.encuadre.forEach((itm, i) => {
            this.state.isModalRubro.push(false);
            this.state.idEncuadre.push(itm.id);
        });
        this.state.elGrado = this.props.grado;
        this.state.elGrupo = this.props.grupo;
        this.state.laMateria = this.props.materia;
        this.state.elIdMateria = this.props.id_materia;
        this.state.elidMaestro = this.state.encuadre[0].id_maestro;
        this.state.elPeriodo = this.state.encuadre[0].periodo;
        this.state.elCiclo = this.state.encuadre[0].ciclo;
        await this.setState({aux: 0});
    }

    async openModal(i, state) {
        this.state.isModalRubro[i] = state;
        this.state.criterios = this.state.encuadre[i].descripcion;
        this.state.criteriosValue = this.state.encuadre[i].valor;
        //+--+-+-+-+-+-+-+rubro
        await this.setState({encUpdt: [], lasPonderaciones: [], descripPond: [], valorPond: []});
        this.state.encuadre[i].ponderaciones.forEach((it, ix) => {
            this.state.lasPonderaciones.push(it);
            this.state.descripPond.push(it.descripcion);
            this.state.valorPond.push(it.valor);
        });
        this.state.encuadre.forEach((itm, ix) => {
            this.state.encUpdt.push(itm)
        });
        await this.setState({aux: 0});
    }


    async closeModal(i, state, validate) {
        if (validate === 'change') {
            let found = false;
            for (let j = 0; j < this.state.encUpdt[i].ponderaciones.length; j++) {
                if (this.state.encUpdt[i].ponderaciones[j].descripcion === '' || this.state.encUpdt[i].ponderaciones[j].valor === '') {
                    found = true;
                    break;
                }
            }
            if (found === true) {
                Alert.alert('Ponderaciones sin definir',
                    'Defina todos los campos de ponderaciones para poder continuar');
            } else {
                this.state.encuadre[i].descripcion = this.state.criterios;
                this.state.encuadre[i].valor = this.state.criteriosValue;
                if (this.state.criterios !== this.state.encuadre[i].valor
                    || this.state.criteriosValue !== this.state.encuadre[i].descripcion) {
                    this.setState({cambio: 1});
                } else {
                    this.setState({cambio: 0});
                }
                await this.onChangeCriValue(i);
                const y = this.state.encuadre.length;
                for (let i = 0; i < y; i++) {
                    for (let j = 0; j < y - 1 - i; j++) {
                        if (this.state.encuadre[j].valor < this.state.encuadre[j + 1].valor) {
                            [this.state.encuadre[j], this.state.encuadre[j + 1]] = [
                                this.state.encuadre[j + 1],
                                this.state.encuadre[j]
                            ];
                        }
                    }
                }
                let encs = [];
                this.state.encUpdt.forEach((itmEnc, ix) => {
                    encs.push(itmEnc);
                });
                let ponderDescrip = [];
                let pondervalue = [];
                this.state.encUpdt[i].ponderaciones.forEach((itm, ix) => {
                    ponderDescrip.push(itm.descripcion);
                    pondervalue.push(itm.valor);
                });
                await this.setState({encuadre: encs, descripPond: ponderDescrip, valorPond: pondervalue});
                this.state.isModalRubro[i] = state;
            }
        } else if (validate === 'reset') {
            this.state.lasPonderaciones.forEach((itm, ix) => {
                this.state.encUpdt[i].ponderaciones[ix] = itm;
                this.state.encUpdt[i].ponderaciones[ix].descripcion = this.state.descripPond[ix];
                this.state.encUpdt[i].ponderaciones[ix].valor = this.state.valorPond[ix];
            });
            this.state.isModalRubro[i] = state;
        }
        await this.setState({aux: 0});
    }

    async alertBorrarR(i) {
        Alert.alert('Borrar rubro', 'Está a punto de borrar un rubro\n¿Seguro que quiere continuar?', [
            {text: 'Sí', onPress: () => this.borrar(i)}, {text: 'No'}
        ]);
        await this.setState({aux: 0});
    }

    async borrar(i) {
        this.state.encuadre.splice(i, 1);
        await this.setState({aux: 0});
        await this.onChangeCriValue();
    }

    renderEncuadres() {
        let btnEncuadre = [];
        this.state.encuadre.forEach((item, i) => {
            btnEncuadre.push(
                <View
                    key={i + 'encuadreCorrec'}
                    style={[
                        styles.widthall,
                        styles.row,
                        styles.cardEnc,
                        {backgroundColor: this.state.fourthColor, height: responsiveHeight(4)}
                    ]}
                >
                    <Entypo name='circle-with-minus' size={22} color='red' onPress={() => this.alertBorrarR(i)}/>
                    <Text style={[styles.btn2, styles.textW]}>{item.descripcion}</Text>
                    <Text style={[styles.textW, {textAlign: 'center'}]}>{item.valor}%</Text>
                    <MaterialIcons name='edit' size={21} color='grey' onPress={() => this.openModal(i, true)}/>
                    {/*+-+-+-+-+-+-+-+--+Editar rubro-+-+-+-+-+-+-+--+-+-+*/}
                    <Modal
                        backdropOpacity={0.5}
                        animationInTiming={1000}
                        animationIn={'bounceIn'}
                        animationOutTiming={1000}
                        animationOut={'bounceOut'}
                        isVisible={this.state.isModalRubro[i]}
                    >
                        <KeyboardAvoidingView
                            behavior={Platform.OS === 'ios' ? 'padding' : null}
                            keyboardVerticalOffset={Platform.OS === 'ios' ? 10 : 0}
                            enabled
                            style={[styles.container, {borderRadius: 10, flex: 0, maxHeight: responsiveHeight(80)}]}
                        >
                            <Text
                                style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                            >
                                Defina el rubro
                            </Text>
                            <View style={[styles.row, styles.modalWidth, {marginTop: 10}]}>
                                <TextInput
                                    keyboardType='default'
                                    maxLength={20}
                                    multiline={false}
                                    onChangeText={text => (this.state.criterios = text)}
                                    onEndEditing={() => Keyboard.dismiss()}
                                    placeholder={'Tareas, Proyectos, Parcial, etc....'}
                                    returnKeyType='done'
                                    underlineColorAndroid='transparent'
                                    defaultValue={this.state.criterios}
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn3_5,
                                        styles.txtCenter,
                                        {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                    ]}
                                />
                                <TextInput
                                    keyboardType='numeric'
                                    maxLength={2}
                                    multiline={false}
                                    onChangeText={text => [
                                        (this.state.criteriosValue = text),
                                        this.onChangeCriValue(i)
                                    ]}
                                    onEndEditing={() => Keyboard.dismiss()}
                                    placeholder={'10'}
                                    returnKeyType='done'
                                    underlineColorAndroid='transparent'
                                    defaultValue={this.state.criteriosValue}
                                    style={[
                                        styles.listButton,
                                        styles.listButtonSmall,
                                        styles.btn3T,
                                        styles.txtCenter,
                                        {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                    ]}
                                />
                            </View>
                            {Number(item.tipo) === 0 ?
                                <View>
                                    <Text
                                        style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                                        Defina las ponderaciones
                                    </Text>
                                    <ScrollView
                                        overScrollMode={'always'}
                                        style={{maxHeight: responsiveHeight(30), marginVertical: 10}}
                                        showsVerticalScrollIndicator={false}
                                    >
                                        {this.state.encUpdt.length !== 0 ? this.rndPonderaciones(i) : null}
                                        <View style={[styles.modalWidth, {alignItems: 'center'}]}>
                                            <Entypo name='circle-with-plus' size={30} color='green'
                                                    style={{marginTop: 10}}
                                                    onPress={() => this.agregarP(i)}/>
                                        </View>
                                    </ScrollView>
                                </View> : null}
                            <View style={[styles.modalWidth, styles.row, {marginVertical: 10}]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.closeModal(i, false, 'reset')}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {
                                            backgroundColor: this.state.secondColor,
                                            borderColor: this.state.secondColor
                                        }
                                    ]}
                                    onPress={() => this.closeModal(i, false, 'change')}
                                >
                                    <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAvoidingView>
                    </Modal>
                </View>
            )
        });
        return btnEncuadre;
    }

    async agregarP(i) {
        this.state.encUpdt[i].ponderaciones.push({
            descripcion: '',
            valor: '',
        });
        await this.setState({aux: 0});
    }

    async alertDeleteP(i, ix) {
        Alert.alert('Borrar ponderacion', '¿Seguro que quiere continuar?', [
            {text: 'Sí', onPress: () => this.borrarP(i, ix)}, {text: 'No'}
        ]);
        await this.setState({aux: 0});
    }

    async borrarP(i, ix) {
        this.state.encUpdt[i].ponderaciones.splice(ix, 1);
        await this.setState({aux: 0});
        await this.setState({cambio: 1});
    }

    rndPonderaciones(i) {
        let ponderaciones = [];
        if (this.state.encUpdt[i].ponderaciones.length !== 0) {
            this.state.encUpdt[i].ponderaciones.forEach((itm, ix) => {
                ponderaciones.push(
                    <View
                        key={ix + 'ponder'}
                        style={[styles.modalWidth, styles.row, {marginVertical: 2}]}
                    >
                        <Entypo name='circle-with-minus' size={22} color='red'
                                onPress={() => this.alertDeleteP(i, ix)}/>
                        <TextInput
                            keyboardType='default'
                            maxLength={20}
                            multiline={false}
                            onChangeText={text => (this.state.encUpdt[i].ponderaciones[ix].descripcion = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'Mal, Regular, Bien, etc....'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.encUpdt[i].ponderaciones[ix].descripcion}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn2,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                        <TextInput
                            keyboardType='numeric'
                            maxLength={2}
                            multiline={false}
                            onChangeText={text => (this.state.encUpdt[i].ponderaciones[ix].valor = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'0'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.encUpdt[i].ponderaciones[ix].valor}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn3T,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                    </View>
                )
            });
        }
        return ponderaciones
    }

    //++++++++++++++++++++++++++++++++ Guardar +++++++++++++++++++++++++++++++++++
    async borrarEncuadre() {
        this._changeWheelState();
        let request = [];
        for (let i = 0; i < this.state.idEncuadre.length; i++) {
            let formData = new FormData();
            formData.append('new', JSON.stringify({
                grado: this.state.elGrado,
                grupo: this.state.elGrupo,
                id_maestro: this.state.data.user_id,
                id_materia: this.state.elIdMateria,
                periodo: this.state.elPeriodo,
                id_encuadre: this.state.idEncuadre[i]
            }));
            await fetch('http://127.0.0.1:8000/api/borrar/encuadre', {
                method: 'POST', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: 'Bearer ' + this.state.token
                },
                body: formData
            }).then(res => res.json())
                .then(responseJson => {
                    request = responseJson
                });
        }
        if (request.error !== undefined) {
            Alert.alert('¡Ups!',
                'Ha ocurrido un error ' + request.error.status_code + ' si el error continua pónganse en contacto con soporte'
                +
                '(Encuadres)',
                [{
                    text: 'Entendido', onPress: () => this.setState({visible: false})
                }]);
        } else {
            await this.setState({aux: 0});
            await this.requestMultipart();
        }
    }

    arreglo() {
        let btnTodos = [];
        for (let i = 0; i < this.state.encuadre.length; i++) {
            btnTodos.push({
                ciclo: this.state.elCiclo,
                descripcion: this.state.encuadre[i].descripcion,
                grado: this.state.elGrado,
                grupo: this.state.elGrupo,
                id_maestro: this.state.data.user_id,
                id_materia: this.state.elIdMateria,
                periodo: this.state.elPeriodo,
                valor: this.state.encuadre[i].valor,
                ponderaciones: this.state.encuadre[i].ponderaciones,
                director: '',
                subdirector: '',
                cord_academico: '',
                tipo: this.state.encuadre[i].tipo,
                estatus: 0,
            })
        }
        return btnTodos;
    }

    async requestMultipart() {
        const todo = this.arreglo();
        let formData = new FormData();
        formData.append('new', JSON.stringify(todo));
        await fetch('http://127.0.0.1:8000/api/create/encuadre', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Encuadres)',
                        [{
                            text: 'Entendido', onPress: () => this.setState({visible: false})
                        }]);
                } else {
                    Alert.alert('¡Felicidades!',
                        'Se ha publicado el(los) encuadre(s) con éxito',
                        [{
                            text: 'Entendido',
                            onPress: () => [this.setState({visible: false}), Actions.popTo('encuadre', {key: Math.random()})]
                        }]);
                }
            });
    }

    async cosasLocas() {
        if (this.state.laMateria === '') {
            Alert.alert('¡Ups!', 'No ha seleccionado una materia', [
                {text: 'Entendido'}
            ]);
        } else {
            if (this.state.elGrupo === '') {
                Alert.alert('¡Ups!', 'No ha seleccionado grupo', [
                    {text: 'Entendido'}
                ]);
            } else {
                if (this.state.elTotal !== 100) {
                    Alert.alert('¡Ups!', 'El total del encuadre no es del 100%', [
                        {text: 'Entendido'}
                    ]);
                } else {
                    Alert.alert('Mandar corrección', 'Está a punto de mandar la corrección del encuandre\n¿Seguro que desea continuar?', [
                        {text: 'Sí', onPress: () => this.borrarEncuadre()},
                        {text: 'No'}
                    ]);
                }
            }

        }
    }

    async onChangeCriValue(index) {
        let a = 0;
        let b = 0;
        for (let i = 0; i < this.state.encuadre.length; i++) {
            if (this.state.encuadre[i].valor !== '') {
                if (a + parseInt(this.state.encuadre[i].valor) > 100) {
                    b = 100 - a;
                    this.state.encuadre[index].valor = b + '';
                    a = a + b;
                } else {
                    a = a + parseInt(this.state.encuadre[i].valor);
                }
            }
        }
        this.setState({elTotal: a});
        this.setState({aux: 0});
    }

    definirEncuadre() {
        return (
            <View>
                {this.renderEncuadres()}
                {this.state.elTotal !== 100 ? (
                    <View
                        style={[
                            styles.widthall,
                            styles.cardEnc,
                            {backgroundColor: this.state.fourthColor, alignItems: 'center'}
                        ]}
                    >
                        <Entypo name='plus' size={32} color='grey' onPress={() => this.openModalNew(true)}/>
                    </View>
                ) : null}
            </View>
        );
    }

    async openModalNew(state) {
        await this.setState({isModalnew: state, criterios: '', criteriosValue: ''});
    }

    async agregarNew(index) {
        if (this.state.criterios === '') {
            Alert.alert('Rubro sin definir', 'No ha definido el rubro', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.criteriosValue === '') {
            Alert.alert('Valor del rubro sin definir', 'No ha dado valor al rubro', [
                {text: 'Entendido'}
            ]);
        } else {
            this.closeModalNew(index)
        }
    }

    async closeModalNew(index) {
        this.state.isModalnew = await false;
        await this.setState({aux: 0});
        await this.agregar();
        await this.onChangeCriValue(index)
    }

    async cancelarNew() {
        let ponderadds = [
            {'descripcion': 'No lo hizo', 'valor': '0'},
            {'descripcion': 'Muy mal', 'valor': '1'},
            {'descripcion': 'Regular', 'valor': '2'},
            {'descripcion': 'Bien', 'valor': '3'},
            {'descripcion': 'Muy bien', 'valor': '4'},
            {'descripcion': 'Excelente', 'valor': '5'}
        ];
        await this.setState({isModalnew: false, newPonderac: ponderadds});
    }

    async agregar() {
        this.state.encuadre.push({
            descripcion: this.state.criterios,
            valor: this.state.criteriosValue,
            ponderaciones: this.state.newPonderac
        });
        this.state.encUpdt.push({
            descripcion: this.state.criterios,
            valor: this.state.criteriosValue,
            tipo: this.state.newTipoP,
            ponderaciones: this.state.newPonderac
        });
        this.state.encuadre.forEach(() => {
            this.state.isModalRubro.push(false);
        });
        const y = this.state.encuadre.length;
        for (let i = 0; i < y; i++) {
            for (let j = 0; j < y - 1 - i; j++) {
                if (this.state.encuadre[j].valor < this.state.encuadre[j + 1].valor) {
                    [this.state.encuadre[j], this.state.encuadre[j + 1]] = [
                        this.state.encuadre[j + 1],
                        this.state.encuadre[j]
                    ];
                }
            }
        }
        await this.setState({aux: 0});
    }

    async onDelete(i) {
        this.state.newPonderac.splice(i, 1);
        await this.setState({aux: 0});
    }

    async addNewPonderacion() {
        this.state.newPonderac.push({descripcion: '', valor: ''});
        await this.setState({aux: 0});
    }

    rndNewArrPond() {
        let arrayPonde = [];
        if (this.state.newPonderac.length !== 0) {
            this.state.newPonderac.forEach((itm, i) => {
                arrayPonde.push(
                    <View
                        key={i + 'ponder'}
                        style={[styles.modalWidth, styles.row, {marginVertical: 2}]}
                    >
                        <Entypo name='circle-with-minus' size={22} color='red'
                                onPress={() => this.onDelete(i)}/>
                        <TextInput
                            keyboardType='default'
                            maxLength={20}
                            multiline={false}
                            onChangeText={text => (this.state.newPonderac[i].descripcion = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'Mal, Regular, Bien, etc....'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.newPonderac[i].descripcion}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn2,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                        <TextInput
                            keyboardType='numeric'
                            maxLength={2}
                            multiline={false}
                            onChangeText={text => (this.state.newPonderac[i].valor = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            placeholder={'0'}
                            returnKeyType='done'
                            underlineColorAndroid='transparent'
                            defaultValue={this.state.newPonderac[i].valor}
                            style={[
                                styles.listButton,
                                styles.listButtonSmall,
                                styles.btn3T,
                                styles.txtCenter,
                                {borderColor: this.state.secondColor, height: responsiveHeight(3.5)}
                            ]}
                        />
                    </View>
                )
            })
        }
        return arrayPonde;
    }

    async btnSelected(num) {
        await this.setState({newTipoP: num})
    }

    //-+--+-+--+-+-+-+-+-+-+-+-+-+--+-+-+-+-+-+-+-+-+-+-+-+-+-+
    render() {
        let index = this.state.newTipoP;
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} textContent='Guardando...'/>
                <Modal
                    isVisible={this.state.isModalnew}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <KeyboardAvoidingView
                        behavior={Platform.OS === 'ios' ? 'padding' : null}
                        keyboardVerticalOffset={Platform.OS === 'ios' ? 10 : 0}
                        enabled
                        style={[styles.container, {borderRadius: 10, flex: 0, maxHeight: responsiveHeight(80)}]}>
                        <Text
                            style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                        >
                            Defina el rubro
                        </Text>
                        <View style={[styles.row, styles.modalWidth, {marginTop: 10}]}>
                            <TextInput
                                keyboardType='default'
                                maxLength={20}
                                multiline={false}
                                onChangeText={text => (this.state.criterios = text)}
                                onEndEditing={() => Keyboard.dismiss()}
                                placeholder={'Tareas, Proyectos, Parcial, etc....'}
                                returnKeyType='done'
                                underlineColorAndroid='transparent'
                                defaultValue={this.state.criterios}
                                style={[
                                    styles.listButton,
                                    styles.listButtonSmall,
                                    styles.btn3_5,
                                    styles.txtCenter,
                                    {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                ]}
                            />
                            <TextInput
                                keyboardType='numeric'
                                maxLength={2}
                                multiline={false}
                                onChangeText={text => [
                                    (this.state.criteriosValue = text)
                                ]}
                                onEndEditing={() => Keyboard.dismiss()}
                                placeholder={'10'}
                                returnKeyType='done'
                                underlineColorAndroid='transparent'
                                defaultValue={this.state.criteriosValue}
                                style={[
                                    styles.listButton,
                                    styles.listButtonSmall,
                                    styles.btn3T,
                                    styles.txtCenter,
                                    {borderColor: this.state.secondColor, height: responsiveHeight(5)}
                                ]}
                            />
                        </View>
                        <Text
                            style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                        >
                            Defina las ponderaciones
                        </Text>
                        <View style={[styles.widthall, {alignItems: 'center'}]}>
                            <View
                                style={[styles.rowsCalif, {
                                    width: responsiveWidth(52), marginTop: 10, marginBottom: index === 1 ? 10 : null
                                }]}>
                                <TouchableHighlight
                                    underlayColor={'transparent'}
                                    style={{
                                        borderRadius: 10,
                                        alignItems: 'center',
                                        width: responsiveWidth(25),
                                        padding: 6,
                                        backgroundColor: index === 0 ? this.state.thirdColor : this.state.fourthColor
                                    }}
                                    onPress={() => this.btnSelected(0)}>
                                    <Text
                                        style={[styles.textW,
                                            {
                                                fontSize: responsiveFontSize(1.5),
                                                fontWeight: '700',
                                                color: index === 0 ? '#fff' : '#000'
                                            }]}
                                    >
                                        Ponderado
                                    </Text>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    underlayColor={'transparent'}
                                    style={{
                                        borderRadius: 10,
                                        width: responsiveWidth(25),
                                        alignItems: 'center',
                                        padding: 6,
                                        backgroundColor: index === 1 ? this.state.thirdColor : this.state.fourthColor
                                    }}
                                    onPress={() => this.btnSelected(1)}>
                                    <Text
                                        style={[styles.textW,
                                            {
                                                fontSize: responsiveFontSize(1.5),
                                                fontWeight: '700',
                                                color: index === 1 ? '#fff' : '#000'
                                            }]}
                                    >
                                        No ponderado
                                    </Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                        {index === 0 ?
                            <ScrollView
                                overScrollMode={'always'}
                                style={{maxHeight: responsiveHeight(32), marginTop: 10}}
                                showsVerticalScrollIndicator={false}
                            >
                                {this.rndNewArrPond()}
                                <View style={[styles.modalWidth, {alignItems: 'center'}]}>
                                    <Entypo name='circle-with-plus' size={30} color='green' style={{marginTop: 10}}
                                            onPress={() => this.addNewPonderacion()}/>
                                </View>
                            </ScrollView> : null}
                        <View style={[styles.modalWidth, styles.row, {marginBottom: 10}]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.cancelarNew()}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={(index) => this.agregarNew(index)}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </Modal>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Usted esta en el encuadre de la materia:{' '}
                </Text>
                <View
                    style={[styles.widthall, {
                        backgroundColor: this.state.fourthColor,
                        borderRadius: 6,
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: 15
                    }]}
                >
                    <Text
                        style={[
                            styles.widthall,
                            {
                                fontSize: responsiveFontSize(1.8),
                                fontWeight: '700',
                                color: 'black',
                                textAlign: 'center'
                            }
                        ]}
                    >
                        {this.state.laMateria}
                    </Text>
                    <Text style={{fontSize: responsiveFontSize(1.7), fontWeight: '200'}}>
                        Del grado y grupo: "{this.state.elGrado} {this.state.elGrupo}"
                    </Text>
                </View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Defina los criterios del encuadre del periodo{' '} {this.state.elPeriodo}
                </Text>
                <View style={{
                    borderTopWidth: .5,
                    borderBottomWidth: .5,
                    borderColor: 'grey',
                    height: responsiveHeight(30),
                    paddingVertical: 5,
                    marginTop: 5
                }}>
                    <ScrollView>
                        {this.definirEncuadre()}
                    </ScrollView>
                </View>
                <View
                    style={[
                        styles.btn4_5,
                        styles.row,
                        styles.cardEnc,
                        {backgroundColor: this.state.fourthColor}
                    ]}
                >
                    <Text style={[styles.textW, styles.btn3]}>Total</Text>
                    <Text
                        style={[styles.btn8, styles.textW, {textAlign: 'center'}]}>{this.state.elTotal}%</Text>
                </View>
                <TouchableOpacity
                    style={[styles.bigButton, {backgroundColor: this.state.secondColor, marginTop: 38}]}
                    onPress={() => this.cosasLocas()}
                >
                    <Text style={styles.textButton}>Mandar corrección</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
