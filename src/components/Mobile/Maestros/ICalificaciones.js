import React from "react";
import {
    Alert,
    AsyncStorage,
    Keyboard,
    KeyboardAvoidingView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import {Actions} from "react-native-router-flux";
import Grupo from "../Globales/Grupo";
import Periodos from "../Globales/Periodos";
import ModalSelector from "react-native-modal-selector";
import styles from "../../styles";
import Modal from "react-native-modal";
import Ionicons from "react-native-vector-icons/Ionicons";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class ICalificaciones extends React.Component {
    _showModal = () => this.setState({isModalVisible: true});
    _hideModal = () => this.setState({isModalVisible: false});

    constructor(props) {
        super(props);
        this.state = {
            textInputValue: "",
            elMes: "",
            cososPicker: [],
            mes: moment().month() + 1,
            cargos: [],
            selectedIndexAlumno: -1,
            selectedIndexGrupos: -1,
            selectedIndex: -1,
            periodos: [],
            materias: [],
            elCiclo: "",
            elGrupo: "",
            ciclos: [],
            selectedIndexCiclo: 0,
            laMateria: "",
            elExamen: "",
            elCiclo: "17-18",
            maximo: "",
            alumnos: [],
            valores: "",
            aciertos: [],
            calificacion: [],
            c: "",
            elPeriodo: ""
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getMaterias();
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++ Grupos +++++++++++++++++++++++++++++++++++++++++++++++

    async onListItemPressedGrupos(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexGrupos: indexSelected,
            elGrupo: itemSelected
        });
        await this.getAlumnosByGG();
        this.state.alumnos.forEach(() => {
            this.state.calificacion.push(0);
            this.state.aciertos.push(0);
        });
    }

    //++++++++++++++++++++++++++++++++++ Periodos ++++++++++++++++++++++++++++++++

    async onListItemPressedPeriodo(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexPeriodo: indexSelected,
            elPeriodo: itemSelected
        });
    }

    //++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            elGrado: option.grado,
            selectedIndexGrupos: -1,
            urlGp: "/api/grupos/materias/" + option.materia
        });
    }

    async onChange1(option) {
        await this.setState({
            elExamen: option.id
        });
        await this.getValores();
    }

    //++++++++++++++++++++++++++++++++++ Valores +++++++++++++++++++++++++++++
    async getValores() {
        let request = await fetch(
            this.state.uri +
            "/api/get/valor/examen/" +
            this.state.laMateria +
            "/" +
            this.state.elPeriodo +
            "/17-18/" +
            this.state.elGrupo +
            "/" +
            this.state.elExamen,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let res = await request.json();
        await this.setState({valores: res[0].valor});
    }

    //+++++++++++++++++++++++++++++++ Alumnos +++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/get/alumnos/by/grado/grupo/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View key={indexAlumno} style={tabRow}>
                <View style={styles.campoTablaG}>
                    <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                        <View style={styles.btn2}>
                            <Text>{itemAlumno.name}</Text>
                        </View>
                        <View style={[styles.btn3, {alignItems: "center"}]}>
                            <TextInput
                                key={indexAlumno}
                                keyboardType="numeric"
                                maxLength={2}
                                multiline={false}
                                value={this.state.aciertos[indexAlumno]}
                                onChangeText={text => [
                                    (this.state.aciertos[indexAlumno] = text),
                                    this.onChangeAciertos(indexAlumno)
                                ]}
                                onEndEditing={() => Keyboard.dismiss()}
                                returnKeyType="done"
                                style={[
                                    styles.listButton,
                                    styles.btn5_lT,
                                    {borderColor: this.state.secondColor}
                                ]}
                            />
                        </View>
                        <View>
                            <Text>{this.state.calificacion[indexAlumno]}%</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //saca las calificaciojnes
    async onChangeAciertos(indexAlumno) {
        let b = this.state.maximo;
        if (
            parseInt(this.state.aciertos[indexAlumno]) > parseInt(this.state.maximo)
        ) {
            this.state.aciertos[indexAlumno] = b;
        }
        let a =
            parseInt(this.state.aciertos[indexAlumno]) *
            100 /
            parseInt(this.state.maximo) *
            (parseInt(this.state.valores) / 100);
        a = a.toFixed(2);
        this.state.calificacion[indexAlumno] = a;
        this.setState({c: 0});
    }

    //++++++++++++++++++++++++++++++guardar++++++++++++++++++++++++++++++++++++++
    async cal() {
        try {
            for (let i = 0; i < this.state.alumnos.length; i++) {
                let formData = new FormData();
                formData.append(
                    "new",
                    JSON.stringify({
                        id_alumno: this.state.alumnos[i].id,
                        id_materia: this.state.laMateria,
                        grupo: this.state.elGrupo,
                        ciclo: this.state.elCiclo,
                        periodo: this.state.elPeriodo,
                        examen: this.state.elExamen,
                        valor: this.state.valores,
                        max: this.state.maximo,
                        aciertos: this.state.aciertos[i],
                        calificacion: this.state.calificacion[i]
                    })
                );
                let request = await fetch(this.state.uri + "/api/calificar/examen", {
                    method: "POST",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "multipart/form-data",
                        Authorization: "Bearer " + this.state.token
                    },
                    body: formData
                });
            }
            Alert.alert("¡Felicidades!", "Se han guardado las asistencias", [
                {
                    text: "Entendido",
                    onPress: () => Actions.drawer()
                }
            ]);
        } catch (e) {
            console.warn(e);
            Alert.alert("Error", "Intenta más tarde por favor", [
                {
                    text: "Entendido",
                    onPress: () => Actions.drawer()
                }
            ]);
        }
    }

    //+++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++

    render() {
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        const alumnos = this.renderAlumnos();
        let index = 0;
        const data1 = [
            {key: index++, label: "Parcial", id: "Parcial"},
            {key: index++, label: "Bimestral", id: "Bimestral"}
        ];
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.5}
                    animationIn={"bounceIn"}
                    animationOut={"bounceOut"}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View
                        style={{
                            height: responsiveHeight(99),
                            alignItems: "center",
                            justifyContent: "center",
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                    >
                        <Text
                            style={{
                                fontSize: responsiveFontSize(1.5),
                                fontWeight: "100"
                            }}
                        >
                            Nombre del Alumno
                        </Text>
                        {/*aquiiiii  */}
                        <Text
                            style={{
                                fontSize: responsiveFontSize(2.3),
                                textAlign: "center",
                                fontWeight: "600"
                            }}
                        >
                            RAMÍREZ JUÁREZ{"\n"}
                            JOSÉ DE JESÚS
                        </Text>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(2.2),
                                textAlign: "center",
                                fontWeight: "800"
                            }}
                        >
                            2 A
                        </Text>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(1.2),
                                marginTop: 0,
                                textAlign: "center",
                                fontWeight: "300"
                            }}
                        >
                            INGRESE EL NÚMERO DE ACIERTOS
                        </Text>
                        <TextInput
                            keyboardType="numeric"
                            maxLength={2}
                            multiline={false}
                            onChangeText={text => this.setState({maximo: text})}
                            onEndEditing={() => Keyboard.dismiss()}
                            returnKeyType="done"
                            underlineColorAndroid="transparent"
                            style={[
                                styles.btn2,
                                styles.cuadroEstd,
                                styles.txtCenter,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    width: responsiveWidth(50),
                                    padding: 10,
                                    height: responsiveHeight(10),
                                    fontSize: responsiveFontSize(6.5)
                                }
                            ]}
                        />
                        <View
                            style={[
                                styles.row_v2,
                                styles.btn2,
                                {
                                    justifyContent: "space-between",
                                    marginBottom: 15
                                }
                            ]}
                        >
                            <TouchableOpacity
                                style={{justifyContent: "center"}}
                                onPress={() => this.setState({isModalVisible: false})}
                            >
                                <Ionicons
                                    name="ios-arrow-dropleft-outline"
                                    size={50}
                                    color={this.state.thirdColor}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{justifyContent: "center"}}
                                onPress={() => this.setState({isModalVisible: false})}
                            >
                                <Ionicons
                                    name="ios-arrow-dropright"
                                    size={50}
                                    color={this.state.thirdColor}
                                />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                            style={[
                                styles.guardarCal,
                                {backgroundColor: this.state.mainColor}
                            ]}
                            onPress={() => this.setState({isModalVisible: false})}
                        >
                            <Text style={styles.textButton}>GUARDAR CAPTURA</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <KeyboardAvoidingView behavior={"padding"}>
                        <Periodos
                            onListItemPressedPeriodo={(indexBtn, itemBtn) =>
                                this.onListItemPressedPeriodo(indexBtn, itemBtn)
                            }
                        />
                        <Text style={styles.main_title}>Seleccione la materia</Text>
                        <ModalSelector
                            cancelText="Cancelar"
                            data={data}
                            initValue="Seleccione la materia"
                            onChange={option => this.onChange(option)}
                            optionTextStyle={{color: this.state.thirdColor}}
                            selectStyle={[
                                styles.inputPicker,
                                {borderColor: this.state.secondColor}
                            ]}
                        />
                        <Grupo
                            urlGrupo={this.state.urlGp}
                            onListItemPressedGrupos={(indexBtn, itemBtn) =>
                                this.onListItemPressedGrupos(indexBtn, itemBtn)
                            }
                            todos={"0"}
                        />
                        <Text style={styles.main_title}>
                            Seleccione el examen a evaluar
                        </Text>
                        <ModalSelector
                            cancelText="Cancelar"
                            data={data1}
                            initValue="Seleccione el examen"
                            onChange={option => this.onChange1(option)}
                            optionTextStyle={{color: this.state.thirdColor}}
                            selectStyle={[
                                styles.inputPicker,
                                {borderColor: this.state.secondColor}
                            ]}
                        />
                        <View
                            style={[styles.row, styles.widthall, {paddingHorizontal: 35}]}
                        >
                            <View style={[styles.btn6, {alignItems: "center"}]}>
                                <Text>Valor</Text>
                            </View>
                            <View style={[styles.btn6, {alignItems: "center"}]}>
                                <Text>Máximo</Text>
                            </View>
                            <View style={[styles.btn6, {alignItems: "center"}]}>
                                <Text>Aciertos</Text>
                            </View>
                        </View>
                        <View style={[styles.row, {justifyContent: "center"}]}>
                            <View
                                style={[
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {
                                        backgroundColor: this.state.fourthColor,
                                        borderColor: "#fff"
                                    }
                                ]}
                            >
                                <Text>{this.state.valores}%</Text>
                            </View>
                            <TextInput
                                keyboardType="numeric"
                                maxLength={2}
                                multiline={false}
                                placeholder={"Núm. Aciertos"}
                                onChangeText={text => this.setState({maximo: text})}
                                onEndEditing={() => Keyboard.dismiss()}
                                returnKeyType="done"
                                underlineColorAndroid="transparent"
                                style={[
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    styles.txtCenter,
                                    {
                                        backgroundColor: this.state.fourthColor,
                                        borderColor: "#fff"
                                    }
                                ]}
                            />
                            <TouchableOpacity
                                onPress={this._showModal}
                                style={[
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    styles.centered_RT,
                                    {backgroundColor: this.state.fourthColor}
                                ]}
                            >
                                <Text>CAPTURAR</Text>
                            </TouchableOpacity>
                        </View>

                        <View
                            style={[
                                styles.tabla,
                                {borderColor: this.state.secondColor, marginTop: 10}
                            ]}
                        >
                            {alumnos}
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                <TouchableOpacity
                    onPress={() => this.cal()}
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}
                >
                    <Text style={styles.textButton}>Capturar</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
