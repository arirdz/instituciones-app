import React from "react";
import {
    AsyncStorage,
    StatusBar,
    View
} from "react-native";
import styles from "../../styles";
import MultiBotonRow from "../Globales/MultiBotonRow";
import ICalificaciones from "./ICalificaciones";
import CrearExmn from "./CrearExmn";

export default class gestExam extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            botonSelected: "Calificar examen"
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");

        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <MultiBotonRow
                    itemBtns={["Calificar examen", "Crear examen en linea"]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />
                {this.state.botonSelected === "Calificar examen" ? (
                    <ICalificaciones/>
                ) : null}
                {this.state.botonSelected === "Crear examen en linea" ? (
                    <CrearExmn/>
                ) : null}
            </View>
        );
    }
}
