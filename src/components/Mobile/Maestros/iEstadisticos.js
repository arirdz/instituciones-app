import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    ScrollView,
    TouchableHighlight
} from 'react-native';
import {responsiveFontSize} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Periodos from '../Globales/Periodos';
import ModalSelector from 'react-native-modal-selector';

export default class iEstadisticos extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: '',
            solicitudes: [],
            botonSelected: 'Aprobados',
            selectedIndexGrupos: -1,
            elGrupo: '',
            selectedIndexGrados: -1,
            elGrado: '',
            alumnos: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    aprobados() {
        const alumnos = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor, marginTop: 10}
                    ]}>
                    Seleccione la materia
                </Text>
                <ModalSelector
                    cancelText="Cancelar"
                    initValue="Seleccione la materia"
                    onChange={option => this.onChange(option)}
                    optionTextStyle={{color: this.state.thirdColor}}
                    selectStyle={[
                        styles.inputPicker,
                        {borderColor: this.state.secondColor, marginTop: 1}
                    ]}
                />
                <Text
                    style={[
                        styles.textButton,
                        {
                            marginTop: 10,
                            marginBottom: 8,
                            color: 'black'
                        }
                    ]}>
                    Aprobados
                </Text>
                <View
                    style={[
                        styles.row_v3,
                        styles.materiaCord,
                        {borderColor: this.state.secondColor}
                    ]}>
                    <View style={styles.porcentNum}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(5)
                            }}>
                            240
                        </Text>
                    </View>

                    <View
                        style={[
                            styles.porcentNum,
                            {borderLeftWidth: 1, borderColor: this.state.secondColor}
                        ]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(5)
                            }}>
                            80%
                        </Text>
                    </View>
                </View>

                <View>
                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text> </Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Alumnos</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Faltas</Text>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Incidencias</Text>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Incidencias</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>9.5{'\n'}o más</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {'\n'}9.0 y 9.4</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {'\n'}8.0 y 8.9</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {'\n'}7.9 y 7.0</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {'\n'}6.0 y 6.9</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Menos de {'\n'}5.9</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Totales</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text style={{color: 'white'}}>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {
                                            borderColor: '#fff'
                                        }
                                    ]}>
                                    <Text style={{color: 'white'}}>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <Text style={{color: 'white'}}>666</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <Text style={{color: 'white'}}>666</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor, marginTop: 10}
                    ]}>
                    Alumnos
                </Text>
                <View style={[styles.contAlumnos, {marginBottom: 10}]}>
                    <ScrollView
                        horizontal={false}
                        style={[
                            styles.titulosContainer,
                            {borderColor: this.state.secondColor}
                        ]}>
                        {alumnos}
                    </ScrollView>
                </View>
                <Text style={[styles.main_title, {marginTop: 10}]}>
                    Materias con mejores promedio, en la seleccion actual
                </Text>
                <View style={styles.campoTablaG}>
                    <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                        <View>
                            <Text style={styles.centeredTxt}>Materia 1</Text>
                        </View>
                        <View>
                            <Text style={styles.centeredTxt}>promedio</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    //++++++++++++++++++++++++++++ Grado y Grupos ++++++++++++++++++++++++++++++++
    async onListItemPressedGrupos(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexGrupos: indexSelected,
            elGrupo: itemSelected
        });
        await this.getAlumnos();
    }

    async onListItemPressedGrado(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexGrados: indexSelected,
            elGrado: itemSelected
        });
        await this.getAlumnos();
    }

    //++++++++++++++++++++++++++++ ALumnos List ++++++++++++++++++++++++++++++++++
    async getAlumnos() {
        let alumnoList = await fetch(
            this.state.uri +
            '/api/user/alumnos/' +
            this.state.grado +
            '/' +
            this.state.grupo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnosList = await alumnoList.json();
        this.setState({alumnos: alumnosList});
    }

    async onListPressedAlumno(indexAlumno, alumno) {
        await this.setState({selectedIndexAlumno: indexAlumno, elAlumno: alumno});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let smallButtonStyles = [
            styles.listButtonAsunto,
            styles.listButtonSmall,
            styles.btn1,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexAlumno === indexAlumno) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexAlumno}
                underlayColor={'transparent'}
                style={smallButtonStyles}
                onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.id)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemAlumno.name}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //++++++++++++++++++++++++++++++++++ Periodos ++++++++++++++++++++++++++++++++

    async onListItemPressedPeriodo(indexSelected, itemSelected) {
        await this.setState({
            selectedIndexPeriodo: indexSelected,
            elPeriodo: itemSelected
        });
    }

    //+++++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++

    render() {
        const informacion = this.aprobados();
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <StatusBar
                        backgroundColor={this.state.mainColor}
                        barStyle="light-content"
                    />
                    <Periodos
                        onListItemPressedPeriodo={(indexBtn, itemBtn) =>
                            this.onListItemPressedPeriodo(indexBtn, itemBtn)
                        }
                    />
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                        todos={'0'}
                    />
                    {informacion}
                </ScrollView>
            </View>
        );
    }
}
