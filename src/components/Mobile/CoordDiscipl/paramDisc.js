import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
} from "react-native";
import {
    responsiveFontSize
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import Ionicons from "react-native-vector-icons/Ionicons";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class paramDisc extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            isDateTimePickerVisibleF1: false,
            isDateTimePickerVisibleF2: false,
            fechaF1: [],
            fechaF2: [],
            solicitudes: [],
            friends: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <TouchableOpacity
                    style={[
                        styles.row_PC,
                        {borderColor: this.state.fourthColor, marginVertical: 0}
                    ]}
                >
                    <View style={styles.btn10}>
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>Conducta</Text>
                        <Text style={[styles.textParam, {color: this.state.mainColor}]}>
                            Tipos de incidiencias
                        </Text>
                        <Text style={{color: this.state.thirdColor}}>
                            Ingrese los tipos de incidiencias
                        </Text>
                    </View>
                    <View style={[styles.btn6, {alignItems: "center"}]}>
                        <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
