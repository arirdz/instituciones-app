import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    TouchableOpacity,
    ScrollView
} from "react-native";
import {
    responsiveFontSize,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import GradoyGrupo from "../Globales/GradoyGrupo";
import ModalSelector from "react-native-modal-selector";
import MultiBotonRow from "../Globales/MultiBotonRow";
import Entypo from "react-native-vector-icons/Entypo";
import Modal from "react-native-modal";

export default class examRecupCord extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: "",
            solicitudes: [],
            botonSelected: "Por cantidad",
            nombres: [],
            laMateria: "",
            materias: [],
            indexSelectedTab: 0,
            alumnos: [],
            comentarios: [],
            selectedIndexGrados: -1,
            grado: "",
            selectedIndexGrupos: -1,
            grupo: ""
        };
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado
        });
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getAlumnosByGG();
    }

    // +++++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri + "/api/get/alumnos/by/grado/grupo/3/A",
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {this.state.botonSelected === "Otros movimientos"
                    ? this.isATiempo(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab == 0
                    ? this.isComentario(itemAlumno, indexAlumno)
                    : null}
            </View>
        );
    }

    isComentario(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.setState({isModalVisible: true})}
                        >
                            <Entypo name="plus" size={20} color="black"/>
                        </TouchableOpacity>
                        <Modal
                            isVisible={this.state.isModalVisible}
                            backdropOpacity={0.5}
                            animationIn={"bounceIn"}
                            animationOut={"bounceOut"}
                            animationInTiming={1000}
                            animationOutTiming={1000}
                        >
                            <View style={[styles.container, {borderRadius: 6}]}>
                                <View style={[styles.campoTablaG, styles.modalWidth]}>
                                    <Text style={[styles.main_title, styles.modalWidth]}>
                                        Datos generales del alumno
                                    </Text>
                                    <View style={styles.ftoContain}/>
                                </View>
                                <View style={styles.modalDAtos}>
                                    <Text style={{fontWeight: "700", textAlign: "center"}}>
                                        Josefino Guadalupe Hernandez Rodriguez
                                    </Text>
                                </View>
                                <View
                                    style={[
                                        styles.campoTablaG,
                                        {marginBottom: -8, marginTop: 10}
                                    ]}
                                >
                                    <View style={[styles.row, styles.modalWidth]}>
                                        <View style={styles.varDato1}>
                                            <Text>Grado y grupo actual: </Text>
                                        </View>
                                        <View style={styles.varDato2}>
                                            <Text> 2A</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={[styles.campoTablaG, {marginBottom: -10}]}>
                                    <View style={[styles.row, styles.modalWidth]}>
                                        <View style={styles.varDato1}>
                                            <Text>Autonomia curricular: </Text>
                                        </View>
                                        <View style={styles.varDato2}>
                                            <Text> Educación fianciera</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={[styles.campoTablaG, {marginBottom: -10}]}>
                                    <View style={[styles.row, styles.modalWidth]}>
                                        <View style={styles.varDato1}>
                                            <Text>Nivel de ingles: </Text>
                                        </View>
                                        <View style={styles.varDato2}>
                                            <Text> Intermedio</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={[styles.campoTablaG, {marginBottom: -10}]}>
                                    <View style={[styles.row, styles.modalWidth]}>
                                        <View style={styles.varDato1}>
                                            <Text>Promedio general actual: </Text>
                                        </View>
                                        <View style={styles.varDato2}>
                                            <Text> 8.8</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={[styles.campoTablaG, {marginBottom: -10}]}>
                                    <View style={[styles.row, styles.modalWidth]}>
                                        <View style={styles.varDato1}>
                                            <Text>Inicidencias de conducta: </Text>
                                        </View>
                                        <View style={styles.varDato2}>
                                            <Text> 22</Text>
                                        </View>
                                    </View>
                                </View>
                                <TouchableOpacity
                                    style={[
                                        styles.bigButton,
                                        styles.modalWidth,
                                        {backgroundColor: this.state.mainColor}
                                    ]}
                                    onPress={() => this.setState({isModalVisible: false})}
                                >
                                    <Text style={styles.textButton}>Aceptar</Text>
                                </TouchableOpacity>
                            </View>
                        </Modal>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    cantidad() {
        const alumnos = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <View
                    style={{
                        width: responsiveWidth(94),
                        alignItems: "center",
                        marginVertical: 10
                    }}
                >
                    <Text
                        style={{
                            fontSize: responsiveFontSize(2),
                            fontWeight: "700"
                        }}
                    >
                        Periodos
                    </Text>
                </View>
                <View>
                    <View style={[styles.row, styles.widthall]}>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text> </Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>II</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>III</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>IV</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>V</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>1 Materia{"\n"}reprobada</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>2 Materia{"\n"}reprobada</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>3 Materia{"\n"}reprobada</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>4 Materia{"\n"}reprobada</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}
                            >
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Totales</Text>
                        </View>

                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}
                            >
                                <Text style={{color: "white"}}>666</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}
                            >
                                <Text style={{color: "white"}}>666</Text>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}
                            >
                                <Text style={{color: "white"}}>666</Text>
                            </View>
                        </View>

                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}
                            >
                                <Text style={{color: "white"}}>666</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Lista de alumnos
                </Text>
                <View style={[styles.tabla, {borderColor: this.state.secondColor}]}>
                    {alumnos}
                </View>
            </View>
        );
    }

    materia() {
        const alumnos = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione la materia
                </Text>
                <ModalSelector
                    cancelText="Cancelar"
                    initValue="Seleccione la materia"
                    onChange={option => this.onChange(option)}
                    optionTextStyle={{color: this.state.thirdColor}}
                    selectStyle={[
                        styles.inputPicker,
                        {borderColor: this.state.secondColor, marginTop: 1}
                    ]}
                />
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Lista de alumnos
                </Text>
                <View style={[styles.tabla, {borderColor: this.state.secondColor}]}>
                    {alumnos}
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione tipo de consulta
                    </Text>
                    <MultiBotonRow
                        itemBtns={["Por cantidad", "Por materia"]}
                        onSelectedButton={(indexBtn, itemBtn) =>
                            this.botonSelected(indexBtn, itemBtn)
                        }
                        cantidad={2}
                    />
                    {this.state.botonSelected === "Por cantidad" ? this.cantidad() : null}
                    {this.state.botonSelected === "Por materia" ? this.materia() : null}
                </ScrollView>
            </View>
        );
    }
}
