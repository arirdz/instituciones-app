import React from "react";
import {
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import GradoyGrupo from "../Globales/GradoyGrupo";
import {Actions} from "react-native-router-flux";
import MultiBotonRow from "../Globales/MultiBotonRow";
import Periodos from "../Globales/Periodos";
import Modal from "react-native-modal";
import Table from "../Globales/Table";

export default class boletasCord extends React.Component {
    _showModal = () => this.setState({isModalVisible: true});

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            items: {},
            laMateria: "",
            materias: [],
            botonSelected: "Revisar calificaciones",
            indexSelectedTab: 0,
            alumnos: [],
            selectedIndexGrados: -1,
            grado: "",
            selectedIndexGrupos: -1,
            grupo: "",
            selectedIndexPeriodo: -1,
            elPeriodo: "",
            porcentajes: [89, 70, 60, 45, 67, 78, 56, 78, 45, 67, 90, 78, 56, 78, 56],
            grados: [1, 2, 3, 4, 5],
            grupos: ["A", "B", "C"]
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getAlumnosByGG();
        await this.getMaterias();
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    revCalif() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <Table
                        arreglo={this.state.porcentajes}
                        filas={this.state.grados}
                        columnas={this.state.grupos}
                    />

                    <View style={{marginTop: 5, alignItems: "center"}}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la materia
                        </Text>
                        <View
                            style={[
                                styles.contAlumnos2,
                                {borderColor: this.state.secondColor}
                            ]}
                        />
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Lista de alumnos
                        </Text>
                        <View
                            style={[
                                styles.contAlumnos2,
                                {borderColor: this.state.secondColor}
                            ]}
                        />
                    </View>
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}
                    onPress={() => Actions.pruebaBoleta()}
                >
                    <Text style={styles.textButton}>Enviar recordatorio</Text>
                </TouchableOpacity>
            </View>
        );
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri + "/api/get/alumnos/by/grado/grupo/3/A",
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    isATiempo(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <Text>asdasda</Text>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {this.state.indexSelectedTab === 0
                    ? this.isATiempo(itemAlumno, indexAlumno)
                    : null}
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    revBoleta() {
        let index = 0;
        const calificaciones = this.renderAlumnos();
        const data = [
            {key: index++, label: "1er Periodo", id: "sReinscrip"},
            {key: index++, label: "2do Periodo", id: "estSimp"},
            {key: index++, label: "3er Periodo", id: "estForm"},
            {key: index++, label: "4to Periodo", id: "crBeca"},
            {key: index++, label: "5to Periodo", id: "sBeca"},
            {key: index++, label: "Promedio", id: "sBeca"}
        ];
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <Text
                        style={[
                            styles.main_title,
                            {color: this.state.thirdColor, marginTop: 10}
                        ]}
                    >
                        Seleccione la materia
                    </Text>
                    <View
                        style={[
                            styles.contAlumnos,
                            styles.truContain,
                            {borderColor: this.state.secondColor}
                        ]}
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el alumno
                    </Text>
                    <View
                        style={[
                            styles.contAlumnos,
                            styles.truContain,
                            {borderColor: this.state.secondColor}
                        ]}
                    />
                    <View style={{paddingTop: 20}}/>
                    <View style={[styles.tabla, {borderWidth: 0}]}>
                        <View style={styles.campoTablaG}>
                            <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                                <View>
                                    <Text style={{fontWeight: "700"}}>Materias Cursadas</Text>
                                </View>
                                <View>
                                    <Text style={{fontWeight: "700"}}>Calificación</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.tabla, {borderColor: this.state.secondColor}]}>
                        {calificaciones}
                    </View>
                    <View style={{height: responsiveHeight(5), alignItems: "center"}}>
                        <TouchableOpacity onPress={this._showModal}>
                            <Text style={[styles.noEscuela, {marginTop: 15}]}>
                                Ver más detalles
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <Modal
                        isVisible={this.state.isModalVisible}
                        backdropOpacity={0.5}
                        animationIn={"bounceIn"}
                        animationOut={"bounceOut"}
                        animationInTiming={1000}
                        animationOutTiming={1000}
                    >
                        <View
                            style={{
                                flex: 1,
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <View
                                style={{
                                    width: responsiveWidth(85),
                                    height: responsiveHeight(75),
                                    alignItems: "center",
                                    justifyContent: "center",
                                    borderRadius: 10,
                                    backgroundColor: "white"
                                }}
                            >
                                <Text style={styles.titleClausula}>
                                    CLÁUSULA DE RESPONSABILIDAD PARA LA ACTUALIZACIÓN DE MIS DATOS
                                    DE CONTACTO
                                </Text>
                                <Text style={styles.contClausula}>
                                    Entiendo y acepto que es mi total responsabilidad mantener mis
                                    datos de contacto actualizados. Deslindo a la institución de
                                    cualquier responsabilidad derivada de la imposibilidad de
                                    localizarme en el caso de una emergencia debido a que no
                                    hubiese mantenido actualizada mi información de contacto. Al
                                    guardar sus datos estará aceptando la presente cláusula de
                                    responsabilidad.
                                </Text>
                                <TouchableOpacity
                                    style={[
                                        styles.btnAceptClaus,
                                        {
                                            backgroundColor: this.state.secondColor,
                                            borderColor: this.state.secondColor
                                        }
                                    ]}
                                    onPress={() => this.setState({isModalVisible: false})}
                                >
                                    <Text style={{color: "white"}}>Aceptar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </ScrollView>
                <View
                    style={[
                        styles.rowsCalif,
                        {width: responsiveWidth(100), marginTop: 20}
                    ]}
                >
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {backgroundColor: this.state.secondColor}
                        ]}
                    >
                        <Text style={styles.textButton}>Enviar a papas</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            styles.btnCalifs,
                            {
                                backgroundColor: this.state.secondColor,
                                borderLeftWidth: 1,
                                borderColor: "white"
                            }
                        ]}
                    >
                        <Text style={styles.textButton}>Imprimir boletas</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <MultiBotonRow
                    itemBtns={["Revisar calificaciones", "Entregar boletas"]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />

                {this.state.botonSelected === "Revisar calificaciones"
                    ? this.revCalif()
                    : null}

                {this.state.botonSelected === "Entregar boletas"
                    ? this.revBoleta()
                    : null}
            </View>
        );
    }
}
