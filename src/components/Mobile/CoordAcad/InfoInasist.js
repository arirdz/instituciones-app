import React from 'react';
import {
	Alert, AsyncStorage, RefreshControl, ScrollView, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Modal from 'react-native-modal';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class InfoInasist extends React.Component {
	_showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});
	_hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});
	_handleDatePicked = date => {
		let fecha = moment(date).format('YYYY-MM-DD');
		this.setState({fecha1: fecha});
		this.date();
	};
	_onRefresh = () => {
		this.setState({refreshing: true});
		this.getGruposAsist();
		if (this.state.elGrado !== '' && this.state.elGrupo !== '') {
			this.getHoy()
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			isDateTimePickerVisible: false,
			cososPicker: [],
			laAsistencia: [],
			refreshing: false,
			alumnos: [],
			fecha1: '',
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexGrupos: -1,
			elGrupo: '',
			faltaGrupal: [],
			asistenciaGrupal: [],
			fechaH: true,
			selectedIndexLista: -1,
			elTaller: '',
			hoyDia: '',
			hoy: [],
			columnas: ['0', '0', '0'],
			filas: ''
		};
	}

	static fechaHoy() {
		return moment().format('YYYY-MM-DD');
	}

	async componentWillMount() {
		await this.getURL();
		await this.setState({hoyDia: InfoInasist.fechaHoy()});
		await this.getGruposAsist();
	}

	async date() {
		await this.setState({fechaH: false});
		await this._hideDateTimePicker();
		await this.getGruposAsist();
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});

	}

	async onListItemPressedGrupos(indexGrupo, itemGrupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: itemGrupo
		});
		await this.getHoy();
		await this.getAlumnosByGG();
	}

	async onListItemPressedGrado(indexGrado, itemGrado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: itemGrado,
			selectedIndexGrupos: -1,
			elGrupo: ''
		});
	}

	async onListItemPressedLista(indexLista, taller) {
		await this.setState({
			selectedIndexLista: indexLista,
			elTaller: taller
		});
	}

	//+++++++++++++++++++++++++++++++++Hoy asistencia++++++++++++++++++++++++++++++
	async getHoy() {
		let elHoy = '';
		if (this.state.fechaH === true) {
			elHoy = await fetch(
				this.state.uri + '/api/porcentaje/asistencias/hoy/'
				+
				this.state.elGrado
				+ '/' +
				this.state.elGrupo +
				'/' +
				this.state.hoyDia,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}
			);
		} else {
			elHoy = await fetch(
				this.state.uri + '/api/porcentaje/asistencias/hoy/'
				+
				this.state.elGrado
				+ '/' +
				this.state.elGrupo +
				'/' +
				this.state.hoyDia,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}
			);
		}
		let hoyyyy = await elHoy.json();
		this.setState({hoy: hoyyyy});
		this.setState({refreshing: false});
	}

	//+++++++++++++++++++++++++++++++Grupos con asistencia+++++++++++++++++++++++
	async getGruposAsist() {
		let asistGrupo = '';
		if (this.state.fechaH === true) {
			asistGrupo = await fetch(
				this.state.uri +
				'/api/get/asistencias/by/grupos/' + this.state.hoyDia,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer' + this.state.token
					}
				}
			);
		} else {
			asistGrupo = await fetch(
				this.state.uri +
				'/api/get/asistencias/by/grupos/' + this.state.fecha1,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer' + this.state.token
					}
				}
			);
		}
		let asistGrupoo = await asistGrupo.json();
		this.setState({asistenciaGrupal: asistGrupoo});
		this.setState({refreshing: false});
		const l = this.state.asistenciaGrupal.length;
		for (let i = 0; i < l; i++) {
			for (let j = 0; j < l - 1 - i; j++) {
				if (
					this.state.asistenciaGrupal[j].asistencias <
					this.state.asistenciaGrupal[j + 1].asistencias
				) {
					[this.state.asistenciaGrupal[j], this.state.asistenciaGrupal[j + 1]] = [
						this.state.asistenciaGrupal[j + 1],
						this.state.asistenciaGrupal[j]
					];
				}
			}
		}

		this.state.filas = this.state.asistenciaGrupal.length / 3;

		await this.setState({aux: 0});
	}

	rndGAsist(itmGpoasis, indxGpoasis) {
		return (
			<View key={indxGpoasis + 'gpoAsistM'}>
				<View
					style={[
						styles.rows_tit,
						styles.rowBtnGpo,
						{backgroundColor: this.state.fourthColor}]}
				>
					<Text style={{fontWeight: '600', fontSize: responsiveFontSize(1.7)}}>
						{' '}{itmGpoasis.grado}{itmGpoasis.grupo}
					</Text>
					<View style={[
						styles.red_btn,
						styles.btnPorcent,
						{borderColor: this.state.secondColor}
					]}
					>
						<Text style={{fontWeight: '800', color: '#000'}}>
							{itmGpoasis.asistencias}%
						</Text>
					</View>
				</View>
			</View>
		);
	}

	rndGAsists(indx) {
		let btnAny = [];
		let aux = 0;
		let aux1 = 0;
		let aux2 = this.state.columnas.length;
		let aux3 = this.state.asistenciaGrupal.length;
		for (let i = 0; i < this.state.filas; i++) {
			if (i !== 0) {
				aux1 = aux1 + this.state.columnas.length;
				aux2 = aux2 + this.state.columnas.length;
			}
			let grupoasis = [];
			this.state.asistenciaGrupal.forEach((itemGrupoasis, indexGrupoasis) => {
				grupoasis.push(this.rndGAsist(itemGrupoasis, indexGrupoasis));
			});
			let grupoasis2 = grupoasis;
			grupoasis2.splice(aux2, aux3);
			grupoasis2.splice(aux, aux1);
			btnAny.push(grupoasis2);
		}

		return (
			<View key={indx + 'gAsist'}>
				<View
					style={[{
						width: responsiveWidth(94),
						flexDirection: 'row',
						justifyContent: 'space-between',
						paddingHorizontal: 35
					}]}
				>
					{btnAny[indx]}
				</View>
			</View>
		);
	}


	rndFilas() {
		let btnFila = [];
		for (let i = 0; i < this.state.filas; i++) {
			btnFila.push(this.rndGAsists(i))
		}
		return btnFila;
	}

	// +++++++++++++++++++++ Alumnos asistencia ++++++++++++++++++++++++
	async getAlumnosByGG() {
		let alumnopicker = '';
		if (this.state.fechaH === true) {
			alumnopicker = await fetch(
				this.state.uri +
				'/api/get/asistencias/fecha/' +
				this.state.hoyDia +
				'/' +
				this.state.elGrado +
				'/' +
				this.state.elGrupo,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}
			);
		} else {
			alumnopicker = await fetch(
				this.state.uri +
				'/api/get/asistencias/fecha/' +
				this.state.fecha1 +
				'/' +
				this.state.elGrado +
				'/' +
				this.state.elGrupo,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}
			);
		}
		let alumnospicker = await alumnopicker.json();
		this.setState({alumnos: alumnospicker});
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla, {width: responsiveWidth(85)}];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}

		return (
			<View
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={tabRow}
			>
				{this.state.isModalalumno === true
					? this.isValor(itemAlumno, indexAlumno)
					: null}
			</View>
		);
	}

	isValor(itemAlumno, indexAlumno) {
		return (
			<View style={styles.campoTablaG}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<Text style={styles.btn8}>{itemAlumno.nombre}</Text>
					<Text>{itemAlumno.grado}{itemAlumno.grupo}</Text>
					<View style={{marginRight: 50}}>
						<Text>{itemAlumno.inasistencias}</Text>
					</View>
				</View>
			</View>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	async verLista() {
		if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacío', 'Seleccione un grado para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacío', 'Seleccione un grupo para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.alumnos.length === 0) {
			Alert.alert('Sin alumnos', 'No hay alumnos con faltas registrada en esta fecha', [
				{text: 'Enterado'}
			])
		} else {
			this.setState({isModalalumno: true})
		}
	}

	render() {
		const alumnos = this.renderAlumnos();
		const hoy = InfoInasist.fechaHoy();
		let btnHoy = [
			styles.mediumBtn,
			{
				borderColor: this.state.secondColor,
				height: responsiveHeight(5),
				justifyContent: 'center',
				alignItems: 'center'
			}
		];
		let texto = [
			styles.textoN,
			{fontSize: responsiveFontSize(1.75), fontWeight: '700'}
		];
		if (this.state.fechaH === true) {
			btnHoy.push(styles.mediumBtn, {
				borderColor: this.state.secondColor,
				backgroundColor: this.state.secondColor,
				height: responsiveHeight(5),
				justifyContent: 'center',
				alignItems: 'center'
			});
			texto.push(styles.textoB, {
				fontSize: responsiveFontSize(1.75),
				fontWeight: '700'
			});
		}
		return (
			<View style={styles.container}>
				<Modal
					isVisible={this.state.isModalalumno}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<View
							style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}
						>
							<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
							<Text style={{marginRight: 15}}>Faltas</Text>
						</View>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}
							>
								{alumnos}
							</View>
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModalalumno: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<ScrollView
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{...ifIphoneX({marginBottom: 20}, {marginBottom: 10}), width: responsiveWidth(94)}}>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione la fecha
					</Text>
					<View style={styles.rowsCalif}>
						<TouchableHighlight
							underlayColor={this.state.secondColor}
							style={btnHoy}
							onPress={() => {
								if (this.state.fechaH === false) {
									this.setState({fechaH: true});
								}
							}}
						>
							<Text style={texto}>Hoy {moment(hoy, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Text>
						</TouchableHighlight>
						<TouchableOpacity
							style={[styles.mediumBtn,
								{
									borderColor: this.state.secondColor,
									height: responsiveHeight(5),
									justifyContent: 'center',
									alignItems: 'center'
								}]}
							onPress={this._showDateTimePicker}
						>
							{this.state.fecha1 === '' ? (
								<Text style={{fontSize: responsiveFontSize(1.5)}}>
									Seleccione una opción
								</Text>
							) : (
								<Text style={{fontSize: responsiveFontSize(1.5)}}>
									{moment(this.state.fecha1, 'YYYY-MM-DD').format('DD/MM/YYYY')}
								</Text>
							)}
						</TouchableOpacity>
					</View>
					<DateTimePicker
						locale={'es'}
						isVisible={this.state.isDateTimePickerVisible}
						onConfirm={this._handleDatePicked}
						onCancel={this._hideDateTimePicker}
						titleIOS={'Seleccione una fecha'}
						confirmTextIOS={'Seleccionar'}
						cancelTextIOS={'Cancelar'}
					/>
					<GradoyGrupo
						onListItemPressedGrupos={(indexGrupo, grupo) =>
							this.onListItemPressedGrupos(indexGrupo, grupo)
						}
						onListItemPressedGrado={(indexGrado, grado) =>
							this.onListItemPressedGrado(indexGrado, grado)
						}
						onListItemPressedLista={(indexLista, taller) =>
							this.onListItemPressedLista(indexLista, taller)
						}
						listaVar={false}
						todos={'1'}
					/>
					<View style={[styles.rows_tit, {width: responsiveWidth(94)}]}>
						<View>
							<View
								style={[
									styles.btn_blue,
									{
										paddingHorizontal: 10,
										width: responsiveWidth(45),
										height: responsiveHeight(11),
										backgroundColor: this.state.fourthColor
									}
								]}
							>
								<Text
									style={{
										fontSize: responsiveFontSize(1.5),
										textAlign: 'center'
									}}
								>
									Alumnos con al menos 1 falta en 1 clase:
								</Text>
								<Text style={{fontSize: 25, marginTop: 5, fontWeight: '800'}}>
									{this.state.hoy.faltas === undefined ? 0 : this.state.hoy.faltas}
								</Text>
							</View>
							<TouchableOpacity
								style={[
									styles.btn_blue,
									{
										borderColor: this.state.secondColor,
										width: responsiveWidth(45),
										height: responsiveHeight(5),
										borderWidth: 1,
										borderRadius: 6,
										backgroundColor: 'white'
									}
								]}
								onPress={() => this.verLista()}
							>
								<Text style={[styles.textButton, {color: 'black'}]}>
									Ver lista
								</Text>
							</TouchableOpacity>
						</View>
						<View>
							<View
								style={[
									styles.btn_blue,
									{
										paddingHorizontal: 10,
										height: responsiveHeight(11),
										backgroundColor: this.state.fourthColor,
										width: responsiveWidth(45)
									}
								]}
							>
								<Text
									style={{
										fontSize: responsiveFontSize(1.5),
										textAlign: 'center'
									}}
								>
									Alumnos con el 100% de asistencia hoy:
								</Text>
								<Text style={{fontSize: 25, marginTop: 5, fontWeight: '800'}}>
									{this.state.hoy.porcentaje === undefined ? '0.0' : this.state.hoy.porcentaje}%
								</Text>
							</View>
							<View style={[
								styles.btn_blue,
								{
									width: responsiveWidth(45),
									height: responsiveHeight(5),
									borderRadius: 6,
									backgroundColor: 'white'
								}
							]}/>
						</View>
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Detalles de grupos
					</Text>
					<Text>Porcentaje total de asistencia en cada grupo, hasta la hora actual:</Text>
					<View style={{marginBottom: 20, marginTop: 10}}>
						{this.rndFilas()}
					</View>
				</ScrollView>
			</View>
		);
	}
}
