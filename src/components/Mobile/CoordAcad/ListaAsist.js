import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	ScrollView,
	TouchableOpacity
} from 'react-native';
import {deviceHeight, responsiveFontSize} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import ModalSelector from 'react-native-modal-selector';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class ListaAsist extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			alumnos: [],
			materias: [],
			laMateria: '',
			botonSelected: 'Lista de \n asistencias',
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexLista: -1,
			elTaller: '',
			selectedIndexGrupos: -1,
			elGrupo: ''
		};
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
	}

	async onChange(option) {
		await this.setState({
			laMateria: option.materia
		});
		await this.getAlumnosByGG();
	}

	async onListItemPressedLista(indexLista, taller) {
		await this.setState({
			selectedIndexLista: indexLista,
			elTaller: taller
		});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo
		});
		await this.getMaterias();
	}

	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado,
			selectedIndexGrupos: -1,
			elGrupo: '',
			alumnos: []
		});
	}

	//+++++++++++++++++++++++++++++Materias++++++++++++++++++++++++++++++++
	async getMaterias() {
		let materiapicker = await fetch(
			this.state.uri +
			'/api/cordacad/get/materias/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let materiaspicker = await materiapicker.json();
		this.setState({materias: materiaspicker});
	}

	// +++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++
	async getAlumnosByGG() {
		let alumnopicker = await fetch(
			this.state.uri +
			'/api/user/alumnos/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let alumnospicker = await alumnopicker.json();
		this.setState({alumnos: alumnospicker});
	}

	isValor(itemAlumno, indexAlumno) {
		return (
			<View style={styles.campoTablaG}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View>
						<Text>{itemAlumno}</Text>
					</View>
				</View>
			</View>
		);
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		const lista = this.isValor(itemAlumno, indexAlumno);
		return (
			<View
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={tabRow}
			>
				{lista}
			</View>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno.name, indexAlumno));
		});
		return buttonsAlumnos;
	}

	async alertFalt() {
		if (this.state.selectedIndexGrados === -1) {
			Alert.alert('Campo grado vacío', 'Seleccione un grado para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.selectedIndexGrupos === -1) {
			Alert.alert('Campo grupo vacío', 'Seleccione un grupo para poder continuar', [
				{text: 'Entendido'}
			])
		}
	}

	render() {
		const alumnos = this.renderAlumnos();
		let data = this.state.materias.map((item, i) => {
			return {
				grado: item.nombre.grado,
				key: i,
				label: item.nombre.nombre,
				materia: item.nombre.id
			};
		});
		return (
			<View style={styles.container}>
				<ScrollView style={{...ifIphoneX({marginBottom: 40}, {marginBottom: 20})}}>
					<GradoyGrupo
						onListItemPressedGrupos={(indexGrupo, grupo) =>
							this.onListItemPressedGrupos(indexGrupo, grupo)
						}
						onListItemPressedGrado={(indexGrado, grado) =>
							this.onListItemPressedGrado(indexGrado, grado)
						}
						onListItemPressedLista={(indexLista, taller) =>
							this.onListItemPressedLista(indexLista, taller)
						}
						listaVar={false}
						todos={'0'}
					/>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione una materia
					</Text>
					{this.state.selectedIndexGrados !== -1 && this.state.selectedIndexGrupos !== -1 ? (
						<ModalSelector
							cancelText='Cancelar'
							data={data}
							initValue='Seleccione una opción'
							onChange={option => this.onChange(option)}
							optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
							selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
							selectStyle={[
								styles.inputPicker,
								{borderColor: this.state.secondColor}
							]}
						/>
					) : (
						<TouchableOpacity
							style={[
								styles.inputPicker,
								{borderColor: this.state.secondColor}
							]}
							onPress={() => this.alertFalt()}
						>
							<Text style={{fontSize: responsiveFontSize(1.8)}}>Seleccione una opción</Text>
						</TouchableOpacity>
					)}
					<View style={[styles.rows_tit, {marginTop: 10}]}>
						<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
					</View>
					<View
						style={[
							styles.tabla,
							{borderColor: this.state.secondColor, marginTop: 5}
						]}
					>
						{alumnos}
					</View>
				</ScrollView>
			</View>
		);
	}
}
