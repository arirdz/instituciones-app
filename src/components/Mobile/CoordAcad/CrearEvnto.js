import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    TextInput
} from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import ModalSelector from "react-native-modal-selector";
import Switch from "react-native-switch-pro";
import DateTimePicker from "react-native-modal-datetime-picker";
import styles from "../../styles";
import GradoyGrupo from "../Globales/GradoyGrupo";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class CrearEvnto extends React.Component {
    _showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});
    _hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});
    _handleDatePicked = date => {
        let fecha = moment(date).format("LL");
        let fechadb = moment(date).format("YYYY-MM-DD HH:mm:ss");
        this.setState({fecha1: fecha, datee: fechadb});
        this._hideDateTimePicker();
    };

    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: "",
            solicitudes: [],
            fecha: [],
            fecha1: "",
            fecha2: "",
            fechadb: "",
            fechadb2: "",
            selectedIndexGrados: -1,
            grado: "",
            selectedIndexGrupos: -1,
            grupo: ""
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado
        });
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            selected: option.id
        });
    }

    render() {
        let index = 0;
        const data = [
            {key: index++, label: "7:00"},
            {key: index++, label: "8:00"},
            {key: index++, label: "9:00"},
            {key: index++, label: "10:00"},
            {key: index++, label: "11:00"}
        ];
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Nombre del evento
                    </Text>
                    <TextInput
                        keyboardType="default"
                        maxLength={256}
                        placeholder={"Titulo del evento"}
                        returnKeyType="next"
                        underlineColorAndroid="transparent"
                        style={[
                            styles.inputPicker,
                            {
                                borderColor: this.state.secondColor
                            }
                        ]}
                    />
                    <Text
                        style={{
                            marginTop: 5,
                            fontWeight: "600",
                            fontSize: responsiveFontSize(2),
                            alignItems: "flex-start",
                            width: responsiveWidth(94)
                        }}
                    />
                    <TextInput
                        keyboardType="default"
                        maxLength={256}
                        multiline={true}
                        placeholder={"Descripción del evento"}
                        returnKeyType="next"
                        underlineColorAndroid="transparent"
                        style={[
                            styles.inputComentarios,
                            {
                                borderColor: this.state.secondColor,
                                width: responsiveWidth(94),
                                height: responsiveHeight(15)
                            }
                        ]}
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Fecha de evento
                    </Text>
                    <View style={styles.rowsCalif}>
                        <View>
                            <Text style={{textAlign: "center"}}>Del:</Text>
                            <TouchableOpacity
                                style={[
                                    [
                                        styles.inputPicker,
                                        {
                                            borderColor: this.state.secondColor,
                                            width: responsiveWidth(46),
                                            height: responsiveHeight(5)
                                        }
                                    ],
                                    {marginTop: 3}
                                ]}
                                onPress={this._showDateTimePicker}>
                                {this.state.fecha1 === "" ? (
                                    <Text>Seleccione una fecha</Text>
                                ) : (
                                    <Text>{this.state.fecha1}</Text>
                                )}
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{textAlign: "center"}}>Al:</Text>
                            <TouchableOpacity
                                style={[
                                    [
                                        styles.inputPicker,
                                        {
                                            borderColor: this.state.secondColor,
                                            width: responsiveWidth(46),
                                            height: responsiveHeight(5)
                                        }
                                    ],
                                    {marginTop: 3}
                                ]}
                                onPress={this._showDateTimePicker}>
                                {this.state.fecha1 === "" ? (
                                    <Text>Seleccione una fecha</Text>
                                ) : (
                                    <Text>{this.state.fecha1}</Text>
                                )}
                            </TouchableOpacity>
                        </View>
                    </View>
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        titleIOS={"Seleccione una fecha"}
                        confirmTextIOS={"Seleccionar"}
                        cancelTextIOS={"Cancelar"}
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Tiempo de duración
                    </Text>
                    <View style={styles.rowsCalif}>
                        <View>
                            <Text style={{textAlign: "center"}}>De:</Text>
                            <ModalSelector
                                data={data}
                                cancelText="Cancelar"
                                initValue="Hora"
                                onChange={option => this.onChange(option)}
                                optionTextStyle={{color: this.state.thirdColor}}
                                selectStyle={[
                                    styles.inputPicker,
                                    {
                                        borderColor: this.state.secondColor,
                                        width: responsiveWidth(46),
                                        height: responsiveHeight(5)
                                    }
                                ]}
                            />
                        </View>
                        <View>
                            <Text style={{textAlign: "center"}}>A:</Text>
                            <ModalSelector
                                data={data}
                                cancelText="Cancelar"
                                initValue="Hora"
                                onChange={option => this.onChange(option)}
                                optionTextStyle={{color: this.state.thirdColor}}
                                selectStyle={[
                                    styles.inputPicker,
                                    {
                                        borderColor: this.state.secondColor,
                                        width: responsiveWidth(46),
                                        height: responsiveHeight(5)
                                    }
                                ]}
                            />
                        </View>
                    </View>
                    <View style={{width: responsiveWidth(94)}}>
                        <GradoyGrupo
                            onListItemPressedGrupos={(indexBtn, itemBtn) =>
                                this.onListItemPressedGrupos(indexBtn, itemBtn)
                            }
                            onListItemPressedGrado={(indexBtn, itemBtn) =>
                                this.onListItemPressedGrado(indexBtn, itemBtn)
                            }
                            todos={"1"}
                        />
                    </View>

                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Notificar a:
                    </Text>
                    <View
                        style={[
                            styles.rowsPlan,
                            {
                                borderTopWidth: 1.5,
                                borderColor: this.state.fourthColor,
                                marginTop: 5
                            }
                        ]}>
                        <Text
                            style={{fontWeight: "500", fontSize: responsiveFontSize(2.05)}}>
                            Maestros
                        </Text>
                        <Switch/>
                    </View>
                    <View
                        style={[styles.rowsPlan, {borderColor: this.state.fourthColor}]}>
                        <Text
                            style={{fontWeight: "500", fontSize: responsiveFontSize(2.05)}}>
                            Padres
                        </Text>
                        <Switch/>
                    </View>
                    <View
                        style={[styles.rowsPlan, {borderColor: this.state.fourthColor}]}>
                        <Text
                            style={{fontWeight: "500", fontSize: responsiveFontSize(2.05)}}>
                            Alumnos
                        </Text>
                        <Switch/>
                    </View>
                    <View
                        style={[styles.rowsPlan, {borderColor: this.state.fourthColor}]}>
                        <Text
                            style={{fontWeight: "500", fontSize: responsiveFontSize(2.05)}}>
                            Director
                        </Text>
                        <Switch/>
                    </View>
                    <View
                        style={[styles.rowsPlan, {borderColor: this.state.fourthColor}]}>
                        <Text
                            style={{fontWeight: "500", fontSize: responsiveFontSize(2.05)}}>
                            Subdirector
                        </Text>
                        <Switch/>
                    </View>
                    <View
                        style={[styles.rowsPlan, {borderColor: this.state.fourthColor}]}>
                        <Text
                            style={{fontWeight: "500", fontSize: responsiveFontSize(2.05)}}>
                            Administrativos
                        </Text>
                        <Switch/>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor, marginBottom: 10}
                    ]}>
                    <Text style={styles.textButton}>Programar evento</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
