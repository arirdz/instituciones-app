import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	ScrollView,
	TouchableOpacity,
	Alert
} from 'react-native';
import {responsiveWidth, responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import {Actions} from 'react-native-router-flux'
import Periodos from '../Globales/Periodos';
import Modal from 'react-native-modal';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class EstadAsist extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			cososPicker: [],
			alumnos: [],
			selectedIndexPeriodo: -1,
			elPeriodo: '',
			alumnosCero: [],
			alumnosCinco: [],
			alumnosDiez: [],
			alumnosOnce: [],
			alumnosVeinte: [],
			historico: '',
			elCiclo: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
	}

	async onListItemPressedPeriodo(indexPeriodo, itemPeriodo) {
		await this.setState({
			selectedIndexPeriodo: indexPeriodo,
			elPeriodo: itemPeriodo
		});
		await this.getAlumnosByGG();
		await this.get0falta();
		await this.get5falta();
		await this.get10falta();
		await this.get11falta();
		await this.get20falta();
		await this.getHistorico();
	}

	//+++++++++++++++++++++++++++++++++ Historico ++++++++++++++++++++++++++++++++++
	async getHistorico() {
		await fetch(
			this.state.uri +
			'/api/porcentaje/asistencias/historico/' +
			this.state.elPeriodo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Barer' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Sin registros',
						'No hay registros en este periodo aun',
						[{
							text: 'Entendido', onPress: () => this.setState({visible:false})
						}]);
				} else {
					this.setState({historico: responseJson});
					this.setState({visible:false});
				}
			});
	}

	// +++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++
	async getAlumnosByGG() {
		await this._changeWheelState();
		await fetch(
			this.state.uri + '/api/get/top/faltantes/' + this.state.elPeriodo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'No hay registros en este periodo aun',
						[{
							text: 'Entendido', onPress: () => this.setState({visible: false})
						}]);
				} else {
					this.setState({alumnos: responseJson});
				}
			});

		const l = this.state.alumnos.length;
		for (let i = 0; i < l; i++) {
			for (let j = 0; j < l - 1 - i; j++) {
				if (this.state.alumnos[j].faltas < this.state.alumnos[j + 1].faltas) {
					[this.state.alumnos[j], this.state.alumnos[j + 1]] = [
						this.state.alumnos[j + 1],
						this.state.alumnos[j]
					];
				}
			}
		}
		await this.setState({aux: 0});
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}

		const alumno = this.isValor(itemAlumno, indexAlumno);
		return (
			<View
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={tabRow}>
				{alumno}
			</View>
		);
	}

	isValor(itemAlumno, indexAlumno) {
		return (
			<View style={styles.campoTablaG}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View>
						<Text>{itemAlumno.nombre}</Text>
					</View>
					<View style={{marginRight: 10}}>
						<Text>{itemAlumno.faltas}</Text>
					</View>
				</View>
			</View>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	//++++++++++++++++++++++++++++++++top cero faltas+++++++++++++++
	async get0falta() {
		await fetch(
			this.state.uri + '/api/faltantes/sinfaltas/' + this.state.elPeriodo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'No hay registros en este periodo aun',
						[{
							text: 'Entendido', onPress: () => this.setState({visible: false})
						}]);
				} else {
					this.setState({alumnosCero: responseJson});
				}
			});
	}

	render0Falta(itemAlumno0, indexAlumno0) {
		let a = this.state.alumnosCero.length - 1;
		let tabRow1 = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno0 !== a) {
			tabRow1.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno0}
				underlayColor={this.state.secondColor}
				style={tabRow1}>
				{this.state.isModal0 === true
					? this.isModal0(itemAlumno0, indexAlumno0)
					: null}
			</View>
		);
	}

	isModal0(itemAlumno0, indexAlumno0) {
		return (
			<View style={[styles.campoTablaG, styles.modalWidth]}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View
						underlayColor={this.state.secondColor}
						style={[
							styles.rowTabla,
							{width: responsiveWidth(74), paddingHorizontal: 10}
						]}>
						<Text>{itemAlumno0.nombre}</Text>
					</View>
					<View style={{marginRight: 100}}>
						<Text>{itemAlumno0.faltas}</Text>
					</View>
				</View>
			</View>
		);
	}

	render0Faltas() {
		let btnAlumnos0 = [];
		this.state.alumnosCero.forEach((itemAlumno0, indexAlumno0) => {
			btnAlumnos0.push(this.render0Falta(itemAlumno0, indexAlumno0));
		});
		return btnAlumnos0;
	}

	//++++++++++++++++++++++++++++ 1 a 5 faltas ++++++++++++++++++++++++++++++++++
	async get5falta() {
		await fetch(
			this.state.uri + '/api/faltantes/cinco/' + this.state.elPeriodo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'No hay registros en este periodo aun',
						[{
							text: 'Entendido', onPress: () => this.setState({visible: false})
						}]);
				} else {
					this.setState({alumnosCinco: responseJson});
				}
			});

		const l = this.state.alumnosCinco.length;
		for (let i = 0; i < l; i++) {
			for (let j = 0; j < l - 1 - i; j++) {
				if (
					this.state.alumnosCinco[j].faltas <
					this.state.alumnosCinco[j + 1].faltas
				) {
					[this.state.alumnosCinco[j], this.state.alumnosCinco[j + 1]] = [
						this.state.alumnosCinco[j + 1],
						this.state.alumnosCinco[j]
					];
				}
			}
		}
		await this.setState({aux: 0});
	}

	render5Falta(itemAlumno5, indexAlumno5) {
		let a = this.state.alumnosCinco.length - 1;
		let tabRow1 = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno5 !== a) {
			tabRow1.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno5}
				underlayColor={this.state.secondColor}
				style={tabRow1}>
				{this.state.isModal5 === true
					? this.isModal1a5(itemAlumno5, indexAlumno5)
					: null}
			</View>
		);
	}

	isModal1a5(itemAlumno5, indexAlumno5) {
		return (
			<View style={[styles.campoTablaG, styles.modalWidth]}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View
						underlayColor={this.state.secondColor}
						style={[
							styles.rowTabla,
							{width: responsiveWidth(74), paddingHorizontal: 10}
						]}>
						<Text>{itemAlumno5.nombre}</Text>
					</View>
					<View style={{marginRight: 100}}>
						<Text>{itemAlumno5.faltas}</Text>
					</View>
				</View>
			</View>
		);
	}

	render1a5Faltas() {
		let btnAlumnos5 = [];
		this.state.alumnosCinco.forEach((itemAlumno5, indexAlumno5) => {
			btnAlumnos5.push(this.render5Falta(itemAlumno5, indexAlumno5));
		});
		return btnAlumnos5;
	}

	//+++++++++++++++++++++++++++++++ 6 a 10 faltas ++++++++++++++++++++++++++++++
	async get10falta() {
		await fetch(
			this.state.uri + '/api/faltantes/diez/' + this.state.elPeriodo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'No hay registros en este periodo aun',
						[{
							text: 'Entendido', onPress: () => this.setState({visible: false})
						}]);
				} else {
					this.setState({alumnosDiez: responseJson});
				}
			});

		const l = this.state.alumnosDiez.length;
		for (let i = 0; i < l; i++) {
			for (let j = 0; j < l - 1 - i; j++) {
				if (
					this.state.alumnosDiez[j].faltas <
					this.state.alumnosDiez[j + 1].faltas
				) {
					[this.state.alumnosDiez[j], this.state.alumnosDiez[j + 1]] = [
						this.state.alumnosDiez[j + 1],
						this.state.alumnosDiez[j]
					];
				}
			}
		}
		await this.setState({aux: 0});
	}

	render10Falta(itemAlumno10, indexAlumno10) {
		let a = this.state.alumnosDiez.length - 1;
		let tabRow1 = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno10 !== a) {
			tabRow1.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno10}
				underlayColor={this.state.secondColor}
				style={tabRow1}>
				{this.state.isModal10 === true
					? this.isModal10(itemAlumno10, indexAlumno10)
					: null}
			</View>
		);
	}

	isModal10(itemAlumno10, indexAlumno10) {
		return (
			<View style={[styles.campoTablaG, styles.modalWidth]}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View
						underlayColor={this.state.secondColor}
						style={[
							styles.rowTabla,
							{width: responsiveWidth(74), paddingHorizontal: 10}
						]}>
						<Text>{itemAlumno10.nombre}</Text>
					</View>
					<View style={{marginRight: 100}}>
						<Text>{itemAlumno10.faltas}</Text>
					</View>
				</View>
			</View>
		);
	}

	render10Faltas() {
		let btnAlumnos10 = [];
		this.state.alumnosDiez.forEach((itemAlumno10, indexAlumno10) => {
			btnAlumnos10.push(this.render10Falta(itemAlumno10, indexAlumno10));
		});
		return btnAlumnos10;
	}

	//++++++++++++++++++++++++++++++++11 a 20 faltas +++++++++++++++++++++++++++
	async get11falta() {
		await fetch(
			this.state.uri + '/api/faltantes/veinte/' + this.state.elPeriodo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'No hay registros en este periodo aun',
						[{
							text: 'Entendido', onPress: () => this.setState({visible: false})
						}]);
				} else {
					this.setState({alumnosOnce: responseJson});
				}
			});

		const l = this.state.alumnosOnce.length;
		for (let i = 0; i < l; i++) {
			for (let j = 0; j < l - 1 - i; j++) {
				if (
					this.state.alumnosOnce[j].faltas <
					this.state.alumnosOnce[j + 1].faltas
				) {
					[this.state.alumnosOnce[j], this.state.alumnosOnce[j + 1]] = [
						this.state.alumnosOnce[j + 1],
						this.state.alumnosOnce[j]
					];
				}
			}
		}
		await this.setState({aux: 0});
	}

	render11Falta(itemAlumno11, indexAlumno11) {
		let a = this.state.alumnosOnce.length - 1;
		let tabRow1 = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno11 !== a) {
			tabRow1.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno11}
				underlayColor={this.state.secondColor}
				style={tabRow1}>
				{this.state.isModal11 === true ? this.isModal11() : null}
			</View>
		);
	}

	isModal11(itemAlumno11, indexAlumno11) {
		return (
			<View style={[styles.campoTablaG, styles.modalWidth]}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View
						underlayColor={this.state.secondColor}
						style={[
							styles.rowTabla,
							{width: responsiveWidth(74), paddingHorizontal: 10}
						]}>
						<Text>{itemAlumno11.nombre}</Text>
					</View>
					<View style={{marginRight: 100}}>
						<Text>{itemAlumno11.faltas}</Text>
					</View>
				</View>
			</View>
		);
	}

	render11Faltas() {
		let btnAlumnos11 = [];
		this.state.alumnosOnce.forEach((itemAlumno11, indexAlumno11) => {
			btnAlumnos11.push(this.render10Falta(itemAlumno11, indexAlumno11));
		});
		return btnAlumnos11;
	}

	//+++++++++++++++++++++++++++++++ 20 o mas faltas +++++++++++++++++++++++++++++
	async get20falta() {
		await fetch(
			this.state.uri + '/api/faltantes/veinte/' + this.state.elPeriodo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!',
						'No hay registros en este periodo aun',
						[{
							text: 'Entendido', onPress: () => this.setState({visible:false})
						}]);
				} else {
					this.setState({alumnosVeinte: responseJson});
				}
			});

		const l = this.state.alumnosVeinte.length;
		for (let i = 0; i < l; i++) {
			for (let j = 0; j < l - 1 - i; j++) {
				if (
					this.state.alumnosVeinte[j].faltas <
					this.state.alumnosVeinte[j + 1].faltas
				) {
					[this.state.alumnosVeinte[j], this.state.alumnosVeinte[j + 1]] = [
						this.state.alumnosVeinte[j + 1],
						this.state.alumnosVeinte[j]
					];
				}
			}
		}
		await this.setState({aux: 0});
	}

	render20Falta(itemAlumno20, indexAlumno20) {
		let a = this.state.alumnosVeinte.length - 1;
		let tabRow1 = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno20 !== a) {
			tabRow1.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno20}
				underlayColor={this.state.secondColor}
				style={tabRow1}>
				{this.state.isModal20 === true
					? this.isModal20(itemAlumno20, indexAlumno20)
					: null}
			</View>
		);
	}

	isModal20(itemAlumno20, indexAlumno20) {
		return (
			<View style={[styles.campoTablaG, styles.modalWidth]}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View
						underlayColor={this.state.secondColor}
						style={[
							styles.rowTabla,
							{width: responsiveWidth(74), paddingHorizontal: 10}
						]}>
						<Text>{itemAlumno20.nombre}</Text>
					</View>
					<View style={{marginRight: 100}}>
						<Text>{itemAlumno20.faltas}</Text>
					</View>
				</View>
			</View>
		);
	}

	render20Faltas() {
		let btnAlumnos20 = [];
		this.state.alumnosVeinte.forEach((itemAlumno20, indexAlumno20) => {
			btnAlumnos20.push(this.render20Falta(itemAlumno20, indexAlumno20));
		});
		return btnAlumnos20;
	}

	//+++++++++++++++++++++++++++++++++++++++++++++++
	async modal0Faltas() {
		if (this.state.elPeriodo === '') {
			Alert.alert('Campo periodo vacío', 'Seleccione un periodo para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.alumnosCero.length === 0) {
			Alert.alert('Sin registro de alumnos', 'No hay registro de alumnos con 0 faltas en el periodo seleccionado', [
				{text: 'Entendido'}
			])
		} else {
			this.setState({isModal0: true})
		}
	}

	async modal1a5Faltas() {
		if (this.state.elPeriodo === '') {
			Alert.alert('Campo periodo vacío', 'Seleccione un periodo para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.alumnosCinco.length === 0) {
			Alert.alert('Sin registro de alumnos', 'No hay registro de alumnos con 1 a 5 faltas en el periodo seleccionado', [
				{text: 'Entendido'}
			])
		} else {
			this.setState({isModal5: true});
		}
	}

	async modal10Faltas() {
		if (this.state.elPeriodo === '') {
			Alert.alert('Campo periodo vacío', 'Seleccione un periodo para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.alumnosDiez.length === 0) {
			Alert.alert('Sin registro de alumnos', 'No hay registro de alumnos con 6 a 10 faltas en el periodo seleccionado', [
				{text: 'Entendido'}
			])
		} else {
			this.setState({isModal10: true});
		}
	}

	async modal11Faltas() {
		if (this.state.elPeriodo === '') {
			Alert.alert('Campo periodo vacío', 'Seleccione un periodo para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.alumnosOnce.length === 0) {
			Alert.alert('Sin registro de alumnos', 'No hay registro de alumnos con 11 a 20 faltas en el periodo seleccionado', [
				{text: 'Entendido'}
			])
		} else {
			this.setState({isModal11: true});
		}
	}

	async modal20Faltas() {
		if (this.state.elPeriodo === '') {
			Alert.alert('Campo periodo vacío', 'Seleccione un periodo para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.alumnosVeinte.length === 0) {
			Alert.alert('Sin registro de alumnos', 'No hay registro de alumnos con mas de 20 faltas en el periodo seleccionado', [
				{text: 'Entendido'}
			])
		} else {
			this.setState({isModal20: true});
		}
	}

	render() {
		const modal0 = this.render0Faltas();
		const modal5 = this.render1a5Faltas();
		const modal10 = this.render10Faltas();
		const modal11 = this.render11Faltas();
		const alumnos = this.renderAlumnos();
		const modal20 = this.render20Faltas();
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<Modal
					isVisible={this.state.isModal0}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80) }]}>

						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<View
							style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
							<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
							<Text style={{marginRight: 15}}>Faltas</Text>
						</View>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}>
								{modal0}
							</View>
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModal0: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModal5}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>

						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<View
							style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
							<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
							<Text style={{marginRight: 15}}>Faltas</Text>
						</View>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}>
								{modal5}
							</View>
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModal5: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModal10}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>

						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<View
							style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
							<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
							<Text style={{marginRight: 15}}>Faltas</Text>
						</View>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}>
								{modal10}
							</View>
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModal10: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModal11}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>

						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<View
							style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
							<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
							<Text style={{marginRight: 15}}>Faltas</Text>
						</View>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}>
								{modal11}
							</View>
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModal11: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModal20}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>

						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<View
							style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
							<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
							<Text style={{marginRight: 15}}>Faltas</Text>
						</View>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}>
								{modal20}
							</View>
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModal20: false})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 20}}>
					<Periodos
						onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
							this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
						}
					/>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Los 5 alumnos con más faltas
					</Text>
					<View style={styles.rows_tit}>
						<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
						<Text style={{marginRight: 15}}>Faltas</Text>
					</View>
					<View
						style={[
							styles.tabla,
							{borderColor: this.state.secondColor, marginTop: 5}
						]}>
						{alumnos}
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Total de alumnos con faltas por categoría en el periodo seleccionado
					</Text>
					<View style={[styles.rows_tit, {paddingHorizontal: 40}]}>
						<Text style={[styles.btn5, {textAlign: 'right'}]}>0 Faltas</Text>
						<View style={styles.first_btn}>
							<Text style={styles.txt_botons}>
								{this.state.alumnosCero.length}
							</Text>
						</View>
						<TouchableOpacity
							style={[styles.ver_lista, {borderColor: this.state.secondColor}]}
							onPress={() => this.modal0Faltas()}>
							<Text>Ver lista</Text>
						</TouchableOpacity>
					</View>
					<View style={[styles.rows_tit, {paddingHorizontal: 40}]}>
						<Text style={[styles.btn5, {textAlign: 'right'}]}>1 - 5</Text>
						<View style={[styles.sec_btn]}>
							<Text style={styles.txt_botons}>
								{this.state.alumnosCinco.length}
							</Text>
						</View>
						<TouchableOpacity
							style={[styles.ver_lista, {borderColor: this.state.secondColor}]}
							onPress={() => this.modal1a5Faltas()}>
							<Text>Ver lista</Text>
						</TouchableOpacity>
					</View>
					<View style={[styles.rows_tit, {paddingHorizontal: 40}]}>
						<Text style={[styles.btn5, {textAlign: 'right'}]}>6 - 10</Text>
						<View style={[styles.third_btn]}>
							<Text style={styles.txt_botons}>
								{this.state.alumnosDiez.length}
							</Text>
						</View>
						<TouchableOpacity
							style={[styles.ver_lista, {borderColor: this.state.secondColor}]}
							onPress={() => this.modal10Faltas()}>
							<Text>Ver lista</Text>
						</TouchableOpacity>
					</View>
					<View style={[styles.rows_tit, {paddingHorizontal: 40}]}>
						<Text style={[styles.btn5, {textAlign: 'right'}]}>11 - 20</Text>
						<View style={[styles.fourth_btn]}>
							<Text style={styles.txt_botons}>
								{this.state.alumnosOnce.length}
							</Text>
						</View>
						<TouchableOpacity
							style={[styles.ver_lista, {borderColor: this.state.secondColor}]}
							onPress={() => this.modal11Faltas()}>
							<Text>Ver lista</Text>
						</TouchableOpacity>
					</View>
					<View style={[styles.rows_tit, {paddingHorizontal: 40}]}>
						<Text style={[styles.btn5, {textAlign: 'right'}]}>Más de 20</Text>
						<View style={[styles.fiveth_btn]}>
							<Text style={styles.txt_botons}>
								{this.state.alumnosVeinte.length}
							</Text>
						</View>
						<TouchableOpacity
							style={[styles.ver_lista, {borderColor: this.state.secondColor}]}
							onPress={() => this.modal20Faltas()}>
							<Text>Ver lista</Text>
						</TouchableOpacity>
					</View>
					<View style={[styles.widthall, {alignItems: 'center', marginTop: 10}]}>
						<View
							style={[
								styles.btn_blue,
								{
									backgroundColor: this.state.fourthColor,
									width: responsiveWidth(55)
								}
							]}>
							<Text>Promedio histórico</Text>
							<Text style={{
								fontSize: 30,
								fontWeight: '800'
							}}>{this.state.historico === '' ? '0' : this.state.historico}%</Text>
						</View>
					</View>
					<View style={{height: responsiveHeight(3)}}/>
				</ScrollView>
			</View>
		);
	}
}
