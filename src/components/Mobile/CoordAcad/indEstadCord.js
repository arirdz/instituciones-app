import React from 'react';
import {Text, View, AsyncStorage, StatusBar, ScrollView} from 'react-native';
import {responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Periodos from '../Globales/Periodos';
import MultiBotonRow from '../Globales/MultiBotonRow';
import EstadAprobados from '../Globales/EstadAprobados';
import EstadReprobados from '../Globales/EstadReprobados';

export default class indEstadCord extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: '',
            solicitudes: [],
            botonSelected: 'Aprobados',
            selectedIndexGrados: -1,
            grado: '',
            selectedIndexGrupos: -1,
            grupo: '',
            selectedIndexPeriodo: -1,
            elPeriodo: ''
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <StatusBar
                        backgroundColor={this.state.mainColor}
                        barStyle="light-content"
                    />

                    <View
                        style={{
                            marginTop: 5,
                            flexDirection: 'row',
                            width: responsiveWidth(94)
                        }}>
                        <Periodos
                            onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                                this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                            }
                        />
                    </View>
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <View style={{width: responsiveWidth(94)}}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione tipo de consulta
                        </Text>
                        <MultiBotonRow
                            itemBtns={['Aprobados', 'Reprobados']}
                            onSelectedButton={(indexBtn, itemBtn) =>
                                this.botonSelected(indexBtn, itemBtn)
                            }
                            cantidad={2}
                        />
                        {this.state.botonSelected === 'Aprobados' ? (
                            <EstadAprobados/>
                        ) : null}
                        {this.state.botonSelected === 'Reprobados' ? (
                            <EstadReprobados/>
                        ) : null}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
