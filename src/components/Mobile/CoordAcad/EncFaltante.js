import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight, Alert
} from 'react-native';
import {
	responsiveHeight,
	responsiveWidth,
	responsiveFontSize
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Periodo from '../Globales/Periodos';

export default class EncFaltante extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			elCiclo: '1',
			lasMaterias: [],
			materiasEnc: [],
			materiasArray: [],
			materiasFalt: [],
			laSolicitud: '',
			encuadre: [],
			solicitudes: [],
			indexSelectedGrado: -1,
			elGrado: '',
			indexSelectedGrupo: -1,
			elGrupo: '',
			indexSelectedPeriodo: -1,
			elPeriodo: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

///+-+-+-+-+Periodo, Grado y Grupos
	async onListItemPressedPeriodo(indxBtn, itmBtn) {
		await this.setState({
			indexSelectedPeriodo: indxBtn,
			elPeriodo: itmBtn
		});
	}

	async onListItemPressedGrado(indxBtn, itmBtn) {
		await this.setState({
			indexSelectedGrado: indxBtn,
			elGrado: itmBtn
		});

	}

	async onListItemPressedGrupos(indxBtn, itmBtn) {
		await this.setState({
			indexSelectedGrupo: indxBtn,
			elGrupo: itmBtn
		});
		await this.getMateriaPorGrado();
	}

	///+-+-+-+-+-Materias
	async getMateriaPorGrado() {
		await fetch(this.state.uri + '/api/cordacad/get/materias/' + this.state.elGrado + '/' + this.state.elGrupo,
			{
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar el encuadres', 'Ha ocurrido un error al tratar de cargar el encuadre. Si el error continua pónganse en contacto con soporte (cod. 2)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({lasMaterias: responseJson});
					this.setState({materiasArray: []});
					this.state.lasMaterias.forEach((it, i) => {
						if (this.state.lasMaterias[i] !== null) {
							this.state.materiasArray.push(it);
						}
					});
					this.getEncuadres();
				}
			});
	}

	//+-+-+-+-+-Encuadres
	async getEncuadres() {
		await fetch(
			this.state.uri +
			'/api/get/encuadre/array/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo +
			'/' +
			this.state.elPeriodo +
			'/' +
			this.state.elCiclo,

			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar el encuadres', 'Ha ocurrido un error al tratar de cargar el encuadre. Si el error continua pónganse en contacto con soporte (cod. 2)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({encuadre: responseJson});
				}
			});
		await this.comprobarEncuadre();
	}


	async comprobarEncuadre() {
		for (let i = 0; i < this.state.encuadre.length; ++i) {
			for (let j = 0; j < this.state.materiasArray.length; ++j) {
				if (this.state.encuadre[i].id_materia === this.state.materiasArray[j].id_materia) {
					this.state.materiasArray.splice(j, 1);
				}
			}
		}
		this.state.materiasFalt = this.state.materiasArray;
		await this.setState({aux: 0});
	}

	rndEncuadres() {
		let btnEnc = [];
		if (this.state.materiasFalt.length !== 0) {
			this.state.materiasFalt.forEach((itm, ix) => {
				btnEnc.push(
					<View
						key={ix + 'encuadreDir'}
						style={[
							styles.widthall,
							styles.row,
							styles.cardEnc,
							{backgroundColor: this.state.fourthColor, height: responsiveHeight(4),paddingHorizontal: 10}
						]}
					>
						<View style={styles.btn2}>
							<Text numberOfLines={1}
								  style={{fontSize: responsiveFontSize(1.3)}}>{itm.nombre.nombre}</Text>
						</View>
						<View style={styles.btn2}>
							<Text numberOfLines={1} style={{
								fontSize: responsiveFontSize(1.3),
								textAlign: 'left',
								marginLeft: 15
							}}>{itm.maestro.name}</Text>
						</View>
					</View>
				)
			});
		}
		return btnEnc;
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Periodo
					onListItemPressedPeriodo={(indxBtn, itmBtn) => this.onListItemPressedPeriodo(indxBtn, itmBtn)}/>
				<GradoyGrupo
					onListItemPressedGrado={(indxBtn, itmBtn) => this.onListItemPressedGrado(indxBtn, itmBtn)}
					onListItemPressedGrupos={(indxBtn, itmBtn) => this.onListItemPressedGrupos(indxBtn, itmBtn)}
				/>

				<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 0}]}>
					Materias <Text style={[styles.main_title, {color: 'red', marginTop: 0}]}>sin</Text> encuadre capturado
				</Text>
				<View style={[styles.widthall, styles.row,{paddingHorizontal: 10, marginTop:10}]}>
					<Text style={[styles.btn2,{fontWeight:'800', fontSize:responsiveFontSize(1.3)}]}>Materia</Text>
				<Text style={[styles.btn2,{fontWeight:'800', fontSize:responsiveFontSize(1.3), marginLeft: 15}]}>Maestro asignado</Text>
				</View>
				<View style={{
					borderTopWidth: .5,
					borderBottomWidth: .5,
					borderColor: 'grey',
					height: responsiveHeight(35),
					paddingVertical: 5
				}}>
					<ScrollView showsVerticalScrollIndicator={false}>
						{this.rndEncuadres()}
					</ScrollView>
				</View>
			</View>
		);
	}
}
