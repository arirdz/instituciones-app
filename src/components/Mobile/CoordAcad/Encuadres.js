import React from 'react';
import {
    Alert,
    AsyncStorage,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Periodos from '../Globales/Periodos';
import Modal from 'react-native-modal';
import MultiBotonRow from '../Globales/MultiBotonRow';
import EncFaltante from './EncFaltante';
import Entypo from 'react-native-vector-icons/Entypo';

export default class Encuadres extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isModalRest: false,
            modalPonder: [],
            elCiclo: '1',
            elGrado: '',
            encuadresArray: [],
            elGrupo: '',
            nombMate: '',
            encuadre: [],
            elTeacher: '',
            indexSelectedTab: 0,
            botonSelected: 'Consultar',
            laMateria: '',
            elEstatus: '',
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selectedIndexPeriodo: -1,
            elPeriodo: '',
            selectedIndexLista: -1,
            laLista: '',
            visible: false,
            incompletoMT: false,
            materiasArray: []
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
        await this.getUserdata();
        await this.getCiclosPeriodos();
    }

    //-+/-/+/-/+/-/+/-/+/-/+/-/+/-/+/-/+/-/+Comentarios y autorizacion/-/+/-/+/-/+/-/+/-/+/-/+/-/+/-/+/-/+/-/+/

    async autorizarEncuadre() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let formData = new FormData();
        formData.append('update', JSON.stringify({
            id_maestro: this.state.encuadre[0].id_maestro,
            id_materia: this.state.encuadre[0].id_materia,
            grado: this.state.elGrado,
            grupo: this.state.elGrupo,
            periodo: this.state.elPeriodo,
            user_id: this.state.data.user_id
        }));
        await fetch('http://127.0.0.1:8000/api/autorizar/encuadre/admin', {
            method: 'POST', headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al enviar autorizar',
                        'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Encuadres)',
                        [{text: 'Entendido', onPress: () => this.setState({isModalRest: false})}]
                    );
                } else {
                    Alert.alert('Encuadre autorizado',
                        'Se ha notificado a los tutores del encuadre publicado',
                        [{text: 'Entendido', onPress: () => [this.getEncuadres(), this.setState({isModalRest: false})]}]
                    );
                }
            });
    }

    async requestWithComent() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let formData = new FormData();
        formData.append('update', JSON.stringify({
            id_maestro: this.state.encuadre[0].id_maestro,
            id_materia: this.state.encuadre[0].id_materia,
            grado: this.state.elGrado,
            grupo: this.state.elGrupo,
            periodo: this.state.elPeriodo,
            comentario: this.state.elComentario,
            user_id: this.state.data.user_id
        }));
        await fetch('http://127.0.0.1:8000/api/encuadre/director/comentar', {
            method: 'POST', headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al enviar comentario',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Encuadres)',
                        [{
                            text: 'Entendido', onPress: () => this.setState({isModalRest: false})
                        }]);
                } else {
                    Alert.alert('Comentario enviado',
                        'Se ha comentado el encuadre con éxito',
                        [{
                            text: 'Entendido',
                            onPress: () => [this.getEncuadres(), this.setState({isModalRest: false})]
                        }]);
                }
            });
    }

//+-+-+-+-+-+-+-+-+-+-+-

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo,
            encuadre: [],
        });
        if (this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.laMateria !== '') {
            await this.getEncuadres();
            await this.getEncuadresArray();
        }
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            elGrupo: grupo,
            encuadre: [],
            incompletoMT: true,
            laMateria: '',
            elTeacher: ''
        });
        if (this.state.elPeriodo !== '' && this.state.elGrado !== '') {
            await this.getEncuadresArray();
        }
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            elGrado: grado,
            encuadre: [],
            incompletoMT: true,
            elTeacher: '',
        });
        if (this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.laMateria) {
            await this.getEncuadresArray();
        }
    }

    //+++++++++++++++++++++++++++++++ Get User +++++++++++++++++++++++++++++++++++
    async getUserdata() {
        await fetch(this.state.uri + '/api/user/data', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error ' +
                        // responseJson.error.status_code +
                        ' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    // +++++++++++++++++++++++++++++++ CICLOS ++++++++++++++++++++++++++++++++++++
    async getCiclosPeriodos() {
        let losciclos = await fetch(this.state.uri + '/api/ciclo/periodo/actual/1', {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let ciclosss = await losciclos.json();
        this.setState({elCiclo: ciclosss.ciclo});
    }

    async getEncuadresArray() {
        await fetch('http://127.0.0.1:8000/api/get/encuadre/array/' + this.state.elGrado + '/' + this.state.elGrupo + '/' + this.state.elPeriodo + '/' + this.state.elCiclo,
            {
                method: 'GET', headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar el encuadres', 'Ha ocurrido un error al tratar de cargar el encuadre. Si el error continua pónganse en contacto con soporte (cod. 2)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({materiasArray: []});
                    this.setState({encuadresArray: responseJson});
                    if (this.state.encuadresArray.length === 0) {
                        this.setState({incompletoMT: true, elTeacher: ''})
                    } else {
                        this.setState({incompletoMT: false});
                    }
                    this.state.encuadresArray.forEach((it, i) => {
                        this.state.materiasArray.push(it.mate);
                    });
                }
            });
        await this.setState({aux: 0});
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia,
            nombMate: option.label
        });
        await this.setState({elTeacher: this.state.encuadresArray[option.key].maestro.name});
        await this.getEncuadres();
    }

    //+++++++++++++++++++++++++++++++Encuadres++++++++++++++++++++++++++++++++++++
    async getEncuadres() {
        await fetch('http://127.0.0.1:8000/api/get/encuadre/' +
            this.state.laMateria +
            '/' +
            this.state.elPeriodo +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar el encuadres',
                        'Ha ocurrido un error al tratar de cargar el encuadre. Si el error continua pónganse en contacto con soporte (cod. 2)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({encuadre: responseJson});
                    if (this.state.encuadre.length === 0) {
                        Alert.alert('Encuadre sin definir',
                            'El maestro aún no ha capturado el encuadre correspondiente al periodo, grado y grupo seleccionado', [
                                {text: 'Entendido'}
                            ])
                    } else {
                        this.setState({elEstatus: this.state.encuadre[0].estatus});
                        this.state.encuadre.forEach(() => {
                            this.state.modalPonder.push(false);
                        })
                    }
                }
            });
    }

    async openModalP(i, state) {
        this.state.modalPonder[i] = state;
        await this.setState({aux: 0});
    }

    renderEncuadres() {
        let btnEncuadre = [];
        this.state.encuadre.forEach((item, index) => {
            btnEncuadre.push(
                <View
                    key={index + 'encuadreDir'}
                    style={[
                        styles.widthall,
                        styles.row,
                        styles.cardEnc,
                        {backgroundColor: this.state.fourthColor, height: responsiveHeight(4)}
                    ]}
                >
                    <View style={styles.btn2}>
                        <Text style={styles.textW}>{item.descripcion}</Text>
                    </View>
                    <View style={styles.btn6}>
                        <Text style={[styles.textW, {textAlign: 'center'}]}>{item.valor}%</Text>
                    </View>
                    {Number(item.tipo) === 0 ? <View style={styles.btn6_ls}>
                        <Entypo name='magnifying-glass' size={25} color='#000' style={{marginTop: 1}}
                                onPress={() => this.openModalP(index, true)}/>
                    </View> : <View style={styles.btn6_ls}/>}
                    {/*+-+-+-+-+-+-+Pondereaciones +-+-+-+-+-*/}
                    <Modal
                        isVisible={this.state.modalPonder[index]}
                        backdropOpacity={0.5}
                        animationIn={'bounceIn'}
                        animationOut={'bounceOut'}
                        animationInTiming={1000}
                        animationOutTiming={1000}
                    >
                        <View
                            style={[styles.container, {borderRadius: 6, flex: 0, maxHeight: responsiveHeight(80)}]}>
                            <Text
                                style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}
                            >
                                Ponderaciones de {item.descripcion}
                            </Text>
                            <View style={[styles.modalWidth, {marginVertical: 10, alignItems: 'center'}]}>
                                {this.rndPonderaciones(index)}
                            </View>
                            <View style={[styles.modalWidth, {marginVertical: 5, alignItems: 'center'}]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => this.openModalP(index, false)}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                </View>
            )
        });
        return btnEncuadre;
    }

    rndPonderaciones(i) {
        let ponderaciones = [];
        if (this.state.encuadre[i].ponderaciones.length !== 0) {
            this.state.encuadre[i].ponderaciones.forEach((itm, i) => {
                ponderaciones.push(
                    <View
                        key={i + 'pondDir'}
                        style={[
                            styles.modalWidth,
                            styles.row,
                            styles.cardEnc,
                            {backgroundColor: this.state.fourthColor, height: responsiveHeight(3.4)}
                        ]}
                    >
                        <View style={styles.btn2}>
                            <Text style={styles.textW}>{itm.descripcion}</Text>
                        </View>
                        <View style={styles.btn6}>
                            <Text style={[styles.textW, {textAlign: 'center'}]}>{itm.valor}</Text>
                        </View>
                    </View>
                )
            });
        }
        return ponderaciones;
    }

    // ++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++
    async alertAutorizar() {
        Alert.alert('Autorizar encuadre', 'Está a punto de autorizar un encuadre\n¿Seguro que desea continuar?', [
            {text: 'Sí', onPress: () => this.autorizarEncuadre()},
            {text: 'No'}
        ]);
    }

    async alertComentar() {
        Alert.alert('Solicitud de ajuste', 'Está a punto de mandar una solicitud de ajuste en en el encuadre\n¿Seguro que desea continuar?', [
            {text: 'Sí', onPress: () => this.requestWithComent()},
            {text: 'No'}
        ])
    }

    async alertModal() {
        if (this.state.elPeriodo === '') {
            Alert.alert('Campo periodo vacío', 'Seleccione un periodo para continuar', [
                {text: 'Enterado'}
            ])
        } else if (this.state.elGrado === '') {
            Alert.alert('Campo grado vacío', 'Seleccione un periodo para continuar', [
                {text: 'Enterado'}
            ])
        } else if (this.state.elGrupo === '') {
            Alert.alert('Campo grupo vacío', 'Seleccione un periodo para continuar', [
                {text: 'Enterado'}
            ])
        }
    }

    async alertModalMT() {
        if (this.state.elGrupo === '') {
            Alert.alert('Campo grupo vacío', 'Seleccione un periodo para continuar', [
                {text: 'Enterado'}
            ])
        } else {
            Alert.alert('Sin encuadres en esta selección', 'El grado y grupo seleccionado no tienen ningun encuadre capturado aún en ninguna materia', [
                {text: 'Enterado'}
            ])
        }

    }

    renderEncP() {
        let id_mate = this.state.laMateria;
        let nombMate = this.state.nombMate;
        let data = this.state.materiasArray.map((item, i) => {
            return {
                grado: item.grado,
                key: i,
                label: item.nombre,
                materia: item.id
            };
        });
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.isModalRest}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <KeyboardAvoidingView style={[styles.container, {borderRadius: 6, flex: 0}]}
                                          keyboardVerticalOffset={150} behavior='padding'>
                        <Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                            Indique el motivo de la corrección.
                        </Text>
                        <TextInput
                            keyboardType='default'
                            returnKeyType='next'
                            multiline={true}
                            placeholder='Este texto va dirigido al docente a cargo. Sea lo más detallado posible...'
                            underlineColorAndroid='transparent'
                            onChangeText={text => (this.state.elComentario = text)}
                            onEndEditing={() => Keyboard.dismiss()}
                            style={[
                                styles.inputContenido,
                                styles.modalWidth,
                                {borderColor: this.state.secondColor, padding: 5, height: responsiveHeight(20)}
                            ]}
                        />
                        <View style={[styles.modalWidth, styles.row]}>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.setState({isModalRest: false})}
                            >
                                <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.modalBigBtn,
                                    {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                ]}
                                onPress={() => this.alertComentar()}
                            >
                                <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </Modal>
                <Periodos
                    onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                        this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                    }
                />
                <GradoyGrupo
                    onListItemPressedGrupos={(indexBtn, itemBtn) =>
                        this.onListItemPressedGrupos(indexBtn, itemBtn)
                    }
                    onListItemPressedGrado={(indexBtn, itemBtn) =>
                        this.onListItemPressedGrado(indexBtn, itemBtn)
                    }
                />
                <Text style={[styles.main_title,
                    {
                        color: this.state.thirdColor,
                        ...Platform.select({
                            android: {marginTop: 15},
                            ios: {marginTop: 10},
                        }),
                    }]
                }>
                    Materias con encuadre definido
                </Text>
                {this.state.elPeriodo !== '' && this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.incompletoMT === false ? (
                    <ModalSelector
                        cancelText='Cancelar'
                        data={data}
                        initValue={id_mate === '' ? 'Seleccione la materia' : nombMate}
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
                        selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                    />) : this.state.incompletoMT === true ? (
                    <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        onPress={() => this.alertModalMT()}
                    >
                        <Text
                            style={{fontSize: responsiveFontSize(1.5)}}>Seleccione la materia</Text>
                    </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        onPress={() => this.alertModal()}
                    >
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>Seleccione la materia</Text>
                    </TouchableOpacity>
                )}
                <Text style={{
                    ...Platform.select({
                        android: {marginTop: 5},
                        ios: {marginTop: 10},
                    })
                }}>Docente a cargo: <Text
                    style={{fontWeight: '600'}}>{this.state.elTeacher}</Text></Text>
                <Text
                    style={[styles.main_title, {
                        color: this.state.thirdColor,
                        ...Platform.select({
                            android: {marginTop: 0}
                        })
                    }]}
                >
                    Encuadre
                </Text>
                {this.state.encuadre.length !== 0 ? (
                    <View style={{
                        ...Platform.select({
                            ios: {height: responsiveHeight(19)},
                            android: {height: responsiveHeight(14)}
                        }),
                        marginTop: 5,
                        borderBottomWidth: .5,
                        borderTopWidth: .5,
                        paddingVertical: 5
                    }}>
                        <ScrollView
                            overScrollMode={'never'}
                            showsVerticalScrollIndicator={false}
                        >
                            {this.renderEncuadres()}
                        </ScrollView>
                    </View>
                ) : this.state.encuadre.length === 0 && this.state.laMateria !== '' ? (
                    <Text style={[styles.main_title, {
                        color: '#a8a8a8',
                        textAlign: 'center',
                        ...Platform.select({
                            ios: {marginTop: 40, marginBottom: 40,},
                            android: {marginTop: 35, marginBottom: 35}
                        }),
                        fontSize: responsiveFontSize(3)
                    }]}>
                        Sin encuadre aún
                    </Text>
                ) : (
                    <Text style={[styles.main_title, {
                        color: '#a8a8a8',
                        textAlign: 'center',
                        ...Platform.select({
                            ios: {marginTop: 40, marginBottom: 40,},
                            android: {marginTop: 35, marginBottom: 35}
                        }),

                        fontSize: responsiveFontSize(3)
                    }]}>
                        Haga una selección
                    </Text>
                )}
                {this.state.elPeriodo !== '' && this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.laMateria !== '' ? (
                    <View>
                        {this.state.elEstatus === '0' ? (
                            <View style={[styles.row, styles.widthall, {marginTop: 10}]}>
                                <TouchableOpacity
                                    style={[styles.mediumBtn, {
                                        height: responsiveHeight(6),
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }]}
                                    onPress={() => this.setState({isModalRest: true})}
                                >
                                    <Text style={styles.textButton}>Solicitar ajuste</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.mediumBtn, {
                                        height: responsiveHeight(6),
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }]}
                                    onPress={() => this.alertAutorizar()}
                                >
                                    <Text style={styles.textButton}>Autorizar</Text>
                                </TouchableOpacity>
                            </View>
                        ) : (
                            <View
                                style={[styles.bigButton, {
                                    backgroundColor: this.state.elEstatus === '2' ? '#1d9018' : '#e10004',
                                    marginTop: 5
                                }]}
                            >
                                <Text
                                    style={styles.textButton}>{this.state.elEstatus === '2' ? 'Autorizado' : 'En proceso de ajuste'}</Text>
                            </View>
                        )}
                    </View>
                ) : (
                    <View style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}>
                        <Text style={styles.textButton}>Haga una seleccion</Text>
                    </View>
                )}
            </View>
        );
    }

    async botonSelected(indexBtn, itemBtn) {
        await this.setState({
            indexSelectedTab: indexBtn,
            botonSelected: itemBtn,
            elPeriodo: '',
            elGrupo: '',
            elGrado: '',
            laMateria: '',
            encuadre: [],
            incompletoMT: false,
            elTeacher: ''
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <MultiBotonRow
                    itemBtns={['Consultar', 'Informe']}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />
                {this.state.indexSelectedTab === 0 ? this.renderEncP() : (<EncFaltante/>)}
            </View>
        );
    }
}
