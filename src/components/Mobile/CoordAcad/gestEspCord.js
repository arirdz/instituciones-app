import React from "react";
import {
    Text,
    View,
    TextInput,
    AsyncStorage,
    StatusBar,
    TouchableOpacity,
    ScrollView,
} from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import Modal from "react-native-modal";
import DateTimePicker from "react-native-modal-datetime-picker";
import Switch from "react-native-switch-pro";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class gestEspCord extends React.Component {
    _showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});
    _hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});
    _handleDatePicked = date => {
        let fecha = moment(date).format("LL");
        let fechadb = moment(date).format("YYYY-MM-DD HH:mm:ss");
        this.setState({fecha1: fecha, datee: fechadb});
        this._hideDateTimePicker();
    };

    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            parametro: false,
            indicador: false,
            parametro2: false,
            indicador2: false,
            fecha: [],
            fecha1: "",
            fecha2: "",
            fechadb: "",
            fechadb2: ""
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            selected: option.id
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    renderHorario() {
        return (
            <View>
                <View
                    style={{
                        width: responsiveWidth(94),
                        alignItems: "center",
                        marginTop: 10
                    }}>
                    <View
                        style={[
                            styles.rowsCalif,
                            styles.modalWidthEsp,
                            {width: responsiveWidth(70)}
                        ]}>
                        <Text>7:00 - 7:50</Text>
                        <Switch/>
                    </View>
                    <View
                        style={[
                            styles.rowsCalif,
                            styles.modalWidthEsp,
                            {width: responsiveWidth(70)}
                        ]}>
                        <Text>8:00 - 8:50</Text>
                        <Switch/>
                    </View>
                    <View
                        style={[
                            styles.rowsCalif,
                            styles.modalWidthEsp,
                            {width: responsiveWidth(70)}
                        ]}>
                        <Text>9:00 - 9:50</Text>
                        <Switch/>
                    </View>
                    <View
                        style={[
                            styles.rowsCalif,
                            styles.modalWidthEsp,
                            {width: responsiveWidth(70)}
                        ]}>
                        <Text>10:00 - 10:50</Text>
                        <Switch/>
                    </View>
                    <View
                        style={[
                            styles.rowsCalif,
                            styles.modalWidthEsp,
                            {width: responsiveWidth(70)}
                        ]}>
                        <Text>11:00 - 11:50</Text>
                        <Switch/>
                    </View>
                    <View
                        style={[
                            styles.rowsCalif,
                            styles.modalWidthEsp,
                            {width: responsiveWidth(70)}
                        ]}>
                        <Text>12:00 - 12:50</Text>
                        <Switch/>
                    </View>
                </View>
            </View>
        );
    }

    parametros() {
        let bottomSP = [
            styles.rowPPI,
            {
                paddingHorizontal: 0,
                height: responsiveHeight(54),
                marginBottom: 10,
                justifyContent: "space-between",
                alignItems: "flex-start",
                flexDirection: "column",
                marginTop: -5
            }
        ];
        if (this.state.horario === true) {
            bottomSP.push(styles.rowPPI, {
                paddingHorizontal: 0,
                height: responsiveHeight(54),
                marginBottom: 145,
                justifyContent: "space-between",
                alignItems: "flex-start",
                flexDirection: "column",
                marginTop: -5
            });
        }
        const horas = this.renderHorario();
        return (
            <View>
                <Modal
                    isVisible={this.state.isModalEsp}
                    backdropOpacity={0.5}
                    animationIn={"bounceIn"}
                    animationOut={"bounceOut"}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View>
                        <Text>afasas</Text>
                    </View>
                </Modal>
                {this.state.parametro === true ? (
                    <View style={bottomSP}>
                        <Text style={{fontWeight: "600"}}> Disponibilidad actual: </Text>
                        <View style={[styles.row, {paddingHorizontal: 10}]}>
                            <Text style={{fontSize: responsiveFontSize(2)}}>Lunes</Text>
                            <Switch
                                onSyncPress={() => {
                                    if (this.state.horario === false) {
                                        this.setState({horario: true});
                                    } else {
                                        this.setState({horario: false});
                                    }
                                }}
                            />
                        </View>
                        {this.state.horario === true ? horas : null}
                        <View style={[styles.row, {paddingHorizontal: 10}]}>
                            <Text style={{fontSize: responsiveFontSize(2)}}>Martes</Text>
                            <Switch/>
                        </View>
                        <View style={[styles.row, {paddingHorizontal: 10}]}>
                            <Text style={{fontSize: responsiveFontSize(2)}}>Miércoles</Text>
                            <Switch/>
                        </View>
                        <View style={[styles.row, {paddingHorizontal: 10}]}>
                            <Text style={{fontSize: responsiveFontSize(2)}}>Jueves</Text>
                            <Switch/>
                        </View>
                        <View style={[styles.row, {paddingHorizontal: 10}]}>
                            <Text style={{fontSize: responsiveFontSize(2)}}>Viernes</Text>
                            <Switch/>
                        </View>
                        <Text
                            style={[
                                styles.main_title,
                                styles.modalWidth,
                                {color: this.state.thirdColor}
                            ]}>
                            Tiempo de duración
                        </Text>
                        <View style={styles.rowsCalif}>
                            <View>
                                <Text style={{textAlign: "center"}}>Del:</Text>
                                <TouchableOpacity
                                    style={[
                                        [
                                            styles.inputPicker,
                                            {
                                                borderColor: this.state.secondColor,
                                                width: responsiveWidth(45),
                                                height: responsiveHeight(6)
                                            }
                                        ],
                                        {marginTop: 3}
                                    ]}
                                    onPress={this._showDateTimePicker}>
                                    {this.state.fecha1 === "" ? (
                                        <Text>Seleccione una fecha</Text>
                                    ) : (
                                        <Text>{this.state.fecha1}</Text>
                                    )}
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text style={{textAlign: "center"}}>Al:</Text>
                                <TouchableOpacity
                                    style={[
                                        [
                                            styles.inputPicker,
                                            {
                                                borderColor: this.state.secondColor,
                                                width: responsiveWidth(45),
                                                height: responsiveHeight(6)
                                            }
                                        ],
                                        {marginTop: 3}
                                    ]}
                                    onPress={this._showDateTimePicker}>
                                    {this.state.fecha2 === "" ? (
                                        <Text>Seleccione una fecha</Text>
                                    ) : (
                                        <Text>{this.state.fecha2}</Text>
                                    )}
                                </TouchableOpacity>
                            </View>
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                            titleIOS={"Seleccione una fecha"}
                            confirmTextIOS={"Seleccionar"}
                            cancelTextIOS={"Cancelar"}
                        />
                        <View style={{width: responsiveWidth(94), alignItems: "center"}}>
                            <TouchableOpacity
                                style={[
                                    styles.exaCalif,
                                    {
                                        marginTop: 5,
                                        backgroundColor: this.state.thirdColor,
                                        height: responsiveHeight(4),
                                        width: responsiveWidth(35)
                                    }
                                ]}
                                onPress={() => this.setState({parametro: false})}>
                                <Text
                                    style={{
                                        fontWeight: "500",
                                        color: "white"
                                    }}>
                                    Guardar
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                ) : null}
                {this.state.parametro2 === true ? (
                    <View
                        style={[
                            styles.rowPPI,
                            {marginBottom: 5, justifyContent: "center", marginTop: -9}
                        ]}>
                        <TouchableOpacity
                            style={styles.btnEsp}
                            onPress={() => this.setState({isModalEsp: true})}>
                            <Text
                                style={{fontWeight: "500", fontSize: responsiveFontSize(2)}}>
                                Editar disponibilidad
                            </Text>
                        </TouchableOpacity>
                    </View>
                ) : null}
                {this.state.parametro3 === true ? (
                    <View
                        style={[
                            styles.rowPPI,
                            {marginBottom: 5, justifyContent: "center", marginTop: -9}
                        ]}>
                        <TouchableOpacity
                            style={styles.btnEsp}
                            onPress={() => this.setState({isModalEsp: true})}>
                            <Text
                                style={{fontWeight: "500", fontSize: responsiveFontSize(2)}}>
                                Editar disponibilidad
                            </Text>
                        </TouchableOpacity>
                    </View>
                ) : null}
                {this.state.parametro4 === true ? (
                    <View
                        style={[
                            styles.rowPPI,
                            {marginBottom: 5, justifyContent: "center", marginTop: -9}
                        ]}>
                        <TouchableOpacity
                            style={styles.btnEsp}
                            onPress={() => this.setState({isModalEsp: true})}>
                            <Text
                                style={{fontWeight: "500", fontSize: responsiveFontSize(2)}}>
                                Editar disponibilidad
                            </Text>
                        </TouchableOpacity>
                    </View>
                ) : null}
                {this.state.parametro5 === true ? (
                    <View
                        style={[
                            styles.rowPPI,
                            {marginBottom: 5, justifyContent: "center", marginTop: -9}
                        ]}>
                        <TouchableOpacity
                            style={styles.btnEsp}
                            onPress={() => this.setState({isModalEsp: true})}>
                            <Text
                                style={{fontWeight: "500", fontSize: responsiveFontSize(2)}}>
                                Editar disponibilidad
                            </Text>
                        </TouchableOpacity>
                    </View>
                ) : null}
            </View>
        );
    }

    render() {
        const parametro = this.parametros();
        const horas1 = this.renderHorario();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <Modal
                    isVisible={this.state.isModalNvoEsp}
                    backdropOpacity={0.5}
                    animationIn={"bounceIn"}
                    animationOut={"bounceOut"}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <View style={styles.centered_RT}>
                                <Text
                                    style={[
                                        styles.main_title,
                                        styles.modalWidth,
                                        {color: this.state.thirdColor}
                                    ]}>
                                    Agregar espacio
                                </Text>
                            </View>
                            <Text style={{fontWeight: "500"}}>
                                Nombre del nuevo espacio
                            </Text>
                            <TextInput
                                keyboardType="default"
                                maxLength={256}
                                placeholder={"Titulo del espacio"}
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                style={[
                                    styles.inputComentarios,
                                    {
                                        borderColor: this.state.secondColor,
                                        height: responsiveHeight(6)
                                    }
                                ]}
                            />
                            <View
                                style={[
                                    styles.rowsCalif,
                                    styles.modalWidthEsp,
                                    {marginTop: 10}
                                ]}>
                                <Text>Lunes</Text>
                                <Switch
                                    onSyncPress={() => {
                                        if (this.state.horario === false) {
                                            this.setState({horario: true});
                                        } else {
                                            this.setState({horario: false});
                                        }
                                    }}
                                />
                            </View>
                            {this.state.horario === true ? horas1 : null}
                            <View
                                style={[
                                    styles.rowsCalif,
                                    styles.modalWidthEsp,
                                    {marginTop: 6}
                                ]}>
                                <Text>Martes</Text>
                                <Switch/>
                            </View>
                            <View
                                style={[
                                    styles.rowsCalif,
                                    styles.modalWidthEsp,
                                    {marginTop: 6}
                                ]}>
                                <Text>Miércoles</Text>
                                <Switch/>
                            </View>
                            <View
                                style={[
                                    styles.rowsCalif,
                                    styles.modalWidthEsp,
                                    {marginTop: 6}
                                ]}>
                                <Text>Jueves</Text>
                                <Switch/>
                            </View>
                            <View
                                style={[
                                    styles.rowsCalif,
                                    styles.modalWidthEsp,
                                    {marginTop: 6}
                                ]}>
                                <Text>Viernes</Text>
                                <Switch/>
                            </View>
                            <Text
                                style={[
                                    styles.main_title,
                                    styles.modalWidth,
                                    {color: this.state.thirdColor}
                                ]}>
                                Tiempo de duración
                            </Text>
                            <View style={[styles.rowsCalif, styles.modalWidth]}>
                                <View>
                                    <Text style={{textAlign: "center"}}>Del:</Text>
                                    <TouchableOpacity
                                        style={[
                                            [
                                                styles.inputPicker,
                                                {
                                                    borderColor: this.state.secondColor,
                                                    width: responsiveWidth(40),
                                                    height: responsiveHeight(5)
                                                }
                                            ],
                                            {marginTop: 3}
                                        ]}
                                        onPress={this._showDateTimePicker}>
                                        {this.state.fecha1 === "" ? (
                                            <Text>Seleccione una fecha</Text>
                                        ) : (
                                            <Text>{this.state.fecha1}</Text>
                                        )}
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Text style={{textAlign: "center"}}>Al:</Text>
                                    <TouchableOpacity
                                        style={[
                                            [
                                                styles.inputPicker,
                                                {
                                                    borderColor: this.state.secondColor,
                                                    width: responsiveWidth(40),
                                                    height: responsiveHeight(5)
                                                }
                                            ],
                                            {marginTop: 3}
                                        ]}
                                        onPress={this._showDateTimePicker}>
                                        {this.state.fecha2 === "" ? (
                                            <Text>Seleccione una fecha</Text>
                                        ) : (
                                            <Text>{this.state.fecha2}</Text>
                                        )}
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    borderBottomLeftRadius: 6,
                                    borderBottomRightRadius: 6,
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor,
                                    width: responsiveWidth(90)
                                }
                            ]}
                            onPress={() => this.setState({isModalNvoEsp: false})}>
                            <Text style={styles.textButton}>Guardar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <ScrollView>
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.parametro === false) {
                                this.setState({
                                    parametro: true,
                                    parametro5: false,
                                    parametro2: false,
                                    parametro4: false,
                                    parametro3: false
                                });
                            } else {
                                this.setState({parametro: false});
                            }
                        }}>
                        <View style={[styles.rowEsp, {width: responsiveWidth(94)}]}>
                            <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                                Sala audiovisual
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                    {this.state.parametro === true ? parametro : null}
                    {this.state.indicador === true ? indicador : null}
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.parametro2 === false) {
                                this.setState({
                                    parametro2: true,
                                    parametro4: false,
                                    parametro5: false,
                                    parametro: false,
                                    parametro3: false
                                });
                            } else {
                                this.setState({parametro2: false});
                            }
                        }}>
                        <View style={styles.rowEsp}>
                            <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                                Biblioteca
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                    {this.state.parametro2 === true ? parametro : null}
                    {this.state.indicador2 === true ? indicador : null}
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.parametro3 === false) {
                                this.setState({
                                    parametro3: true,
                                    parametro4: false,
                                    parametro5: false,
                                    parametro2: false,
                                    parametro: false
                                });
                            } else {
                                this.setState({parametro3: false});
                            }
                        }}>
                        <View style={styles.rowEsp}>
                            <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                                Auditorio
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                    {this.state.parametro3 === true ? parametro : null}
                    {this.state.indicador3 === true ? indicador : null}
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.parametro4 === false) {
                                this.setState({
                                    parametro4: true,
                                    parametro3: false,
                                    parametro2: false,
                                    parametro5: false,
                                    parametro: false
                                });
                            } else {
                                this.setState({parametro4: false});
                            }
                        }}>
                        <View style={styles.rowEsp}>
                            <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                                Sala de juntas
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                    {this.state.parametro4 === true ? parametro : null}
                    {this.state.indicador4 === true ? indicador : null}
                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.parametro5 === false) {
                                this.setState({
                                    parametro5: true,
                                    parametro4: false,
                                    parametro3: false,
                                    parametro2: false,
                                    parametro: false
                                });
                            } else {
                                this.setState({parametro5: false, indicador5: false});
                            }
                        }}>
                        <View style={styles.rowEsp}>
                            <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                                Proyectores
                            </Text>
                            <Ionicons name="ios-arrow-down-outline" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                    {this.state.parametro5 === true ? parametro : null}
                    {this.state.indicador5 === true ? indicador : null}
                    <TouchableOpacity
                        onPress={() => this.setState({isModalNvoEsp: true})}>
                        <View style={styles.rowEsp}>
                            <Text style={[styles.titlePPI, {color: this.state.thirdColor}]}>
                                Agregar espacio
                            </Text>
                            <Entypo name="plus" size={20} color="black"/>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}>
                    <Text style={styles.textButton}>Enviar espacios</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
