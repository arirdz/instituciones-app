import React from 'react';
import {
	Text,
	View,
	AsyncStorage,
	TouchableOpacity,
	Platform,
	TouchableHighlight,
	ScrollView, Alert, KeyboardAvoidingView, TextInput, Keyboard
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';
import {
	responsiveFontSize,
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Spinner from 'react-native-loading-spinner-overlay';
import Switch from 'react-native-switch-pro';
import Entypo from 'react-native-vector-icons/Entypo';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');


export default class OtrosMov extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};
	_changeWheelState2 = () => {
		this.setState({
			visible2: !this.state.visible2
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			aux: 0,
			updateG: true,
			grados: [],
			indexSelected: 0,
			alumnosBaja: [],
			isDateTimePickerVisible: false,
			laFecha: '',
			modalMotivo: false,
			visible: false,
			visible2: false,
			textInputValue: 'Promovidos',
			selected: 'promovido',
			alumnos: [],
			modalInfo: [],
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexGrupos: -1,
			elGrupo: '',
			califs: [],
			totalCalifs: [],
			totalReprob: [],
			promocion: [],
			noPromocion: [],
			elRequest: [],
			losFaltantes: [],
			periodosP: [],
			periodosB: [],
			materiasP: [],
			materiasB: [],
			selectedIndexAlumno: -1,
			elAlumno: '',
			alumnosId: '',
			padreId: '',
			elMotivo: '',
			indxMotivo: -1,
			elAño: '',
			yearPicker: [],
			motivoBaja: [
				{'motivo': 'Baja por traslado'},
				{'motivo': 'Baja por indisciplina'},
				{'motivo': 'Baja por adeudo'},
				{'motivo': 'Baja por rendimiento'}
			],
			comentario: '',
			losEgresados: []
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getYearPicker();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getYearPicker() {
		await fetch(this.state.uri + '/api/picker/year', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de promover a los alumnos de grado, si el error continua pónganse en contacto con soporte (Mov alumnos)', [
							{text: 'Entendido', onPress: () => this.setState({visible2: false})}
						]);
				} else {
					this.setState({yearPicker: responseJson});
				}
			});
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+Promover alumnos+-+-+-+-+-+-+-+-+-+-+asd

	async alertPromover() {
		let gradpicker = await fetch(this.state.uri + '/api/get/grados');
		let gradospicker = await gradpicker.json();
		let grados = [];
		gradospicker.forEach((it) => {
			if (it.grado !== 'Todos') {
				grados.push(it.grado);
			}
		});
		if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacio', 'Seleccione un grado para poder continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacio', 'Seleccione un grupo para poder continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.elGrado.toString() === grados.length.toString()) {
			Alert.alert('Egresar alumnos', 'Está a punto de promover de grado a los alumnos marcados. ¿Seguro que desea continuar?\n\n' +
				'Nota: Los alumnos que están en el último grado cambiarán su estatus a egresado', [
				{text: 'Sí', onPress: () => this.promoverAlumo(grados)}, {text: 'No'}
			])
		} else {
			Alert.alert('Promover alumnos', 'Está a punto de promover de grado a los alumnos marcados ¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.promoverAlumo(grados)}, {text: 'No'}
			])
		}
	}

	async promoverAlumo(grados) {
		await this._changeWheelState2();
		for (let i = 0; i < this.state.totalCalifs.length; i++) {
			if (this.state.promocion[i] === true) {
				await fetch(this.state.uri + '/api/promover/alumno/' + this.state.alumnos[i].id +
					'/' + this.state.alumnos[i].padre_id + '/' + this.state.elGrado, {
					method: 'GET', headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}).then(res => res.json())
					.then(responseJson => {
						this.setState({elRequest: responseJson})
					});
			}
		}
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error en movimiento de alumnos',
				'Ha ocurrido un error ' + this.state.elRequest.error.status_code +
				' al tratar de promover a los alumnos de grado, si el error continua pónganse en contacto con soporte (Mov alumnos)', [
					{text: 'Entendido', onPress: () => this.setState({visible2: false})}
				]);
		} else {
			Alert.alert('Alumnos promovidos',
				'Se han promovido a los alumnos seleccionados con éxito',
				[{text: 'Entendido', onPress: () => [this.setState({visible2: false}), this.getAlumnosByGG()]}]);

		}
		if (this.state.elGrado.toString() === grados.length.toString()) {
			await this.getYearPicker();
		}
	}

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+baja alumno
	async modaBAja() {
		if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacio', 'Seleccione un grado para poder continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacio', 'Seleccione un grupo para poder continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.elAlumno === '') {
			Alert.alert('Alumnos no seleccionado', '',
				[{text: 'Entendido'}]);
		} else {
			this.setState({modalMotivo: true})
		}
	}

	async bajaAlumno() {
		if (this.state.elMotivo === '') {
			Alert.alert('Campo motivo vacio', 'Capture el motivo de la baja para poder continuar', [
				{text: 'Enterado'}
			]);
		} else if (this.state.comentario === '') {
			Alert.alert('Baja alumno', 'Está a punto de dar de baja a un alumno y no ha puesto ningún comentario. ¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.darDeBajaAlumno()}, {text: 'No'}
			]);
		} else {
			Alert.alert('Baja alumno', 'Está a punto de dar de baja a un alumno ¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.darDeBajaAlumno()}, {text: 'No'}
			]);
		}
	}

	async darDeBajaAlumno() {
		await this._changeWheelState2();
		await fetch(
			this.state.uri +
			'/api/baja/alumno/' +
			this.state.alumnosId +
			'/' +
			this.state.padreId +
			'/' +
			this.state.elMotivo +
			'/' +
			this.state.comentario, {
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de promover a los alumnos de grado, si el error continua pónganse en contacto con soporte (Mov alumnos)', [
							{text: 'Entendido', onPress: () => this.setState({visible2: false})}
						]);
				} else {
					Alert.alert('Alumno dado de baja',
						'Se ha dado de baja al alumno correctamente. Se notificará a los maestros y al personal administrativo.\n\n' +
						' A partir de ahora el alumno ya no aparecerá en las listas de asistencia. Podrá consultar los datos del alumno en consulta de bajas',
						[{
							text: 'Entendido',
							onPress: () => [this.setState({visible2: false}), this.getAlumnosByGG()]
						}]);

				}
			});
	}

	async onListPressedMotivo(it, ix) {
		await this.setState({
			elMotivo: it,
			indxMotivo: ix
		});
	}

	rndMotivos() {
		let btnMotivo = [];
		if (this.state.motivoBaja.length !== 0) {
			this.state.motivoBaja.forEach((it, i) => {
				let a = this.state.motivoBaja.length - 1;
				let tabRow = [styles.rowTabla, styles.modalWidth];
				if (i !== a) {
					tabRow.push({borderColor: this.state.secondColor});
				}
				let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
				let texto = [styles.textoN];
				if (this.state.indxMotivo === i) {
					smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
						backgroundColor: this.state.secondColor
					});
					texto.push(styles.textoB);
				}
				btnMotivo.push(
					<TouchableHighlight
						key={i + 'Al'}
						underlayColor={this.state.secondColor}
						style={[tabRow, smallButtonStyles]}
						onPress={() => this.onListPressedMotivo(it.motivo, i)}>
						<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
							<Text style={[texto, {marginRight: 25}]}>{it.motivo}</Text>
						</View>
					</TouchableHighlight>
				);
			});
		}
		return btnMotivo;
	}

//+-+-+-+--+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado,
			selectedIndexGrupos: -1,
			elGrupo: ''
		});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo
		});
		if (this.state.elGrado !== '') {
			await this.getAlumnosByGG();
		}
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected,
			indexSelected: indexSelected,
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexGrupos: -1,
			elGrupo: '',
			alumnos: []
		});
		if (this.state.indexSelected === 1) {
			await this.getAlumnosBaja();
		}
	}

	// +++++++++++++++++++++++++++++ Alumnos grado y grupo +++++++++++++++++++++++
	async getAlumnosByGG() {
		await this._changeWheelState();
		await fetch(
			this.state.uri + '/api/get/alumnos/by/grado/grupo/' + this.state.elGrado + '/' + this.state.elGrupo,
			{
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar la lista de alumnos, si el error continua pónganse en contacto con soporte (Mov alumnos)', [
							{text: 'Entendido', onPress: () => this.setState({visible: false})}
						]);
				} else {
					this.setState({alumnos: responseJson});
					if (this.state.alumnos.length !== 0) {
						if (this.state.selected === 'promovido') {
							let btnSwitch = [];
							let btnNoSwitch = [];
							this.state.alumnos.forEach(() => {
								btnSwitch.push(true);
								btnNoSwitch.push(false);
							});
							this.state.promocion = btnSwitch;
							this.state.noPromocion = btnNoSwitch;
							this.getCalifPromedioMat();
							this.getCalifPromedioPer();
						} else {
							this.setState({visible: false, modalMotivo: false})
						}
					} else {
						Alert.alert('Sin alumnos',
							'No hay alumnos en el grado y grupo seleccionado',
							[{text: 'Entendido', onPress: () => this.setState({visible: false, modalMotivo: false})}]);
					}
				}
			});
	}

	async getCalifPromedioPer() {
		for (let i = 0; i < this.state.alumnos.length; i++) {
			await fetch(this.state.uri + '/api/periodo/alumnos/' + this.state.alumnos[i].id + '/' + this.state.elGrado, {
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error en movimiento de alumnos',
							'Ha ocurrido un error ' + responseJson.error.status_code +
							' si el error continua pónganse en contacto con soporte (Mov de alumnos)', [
								{text: 'Enterado', onPress: () => this.setState({visible: false})}
							]);
					} else {
						this.state.periodosB[i] = responseJson.periodosB;
						this.state.periodosP[i] = responseJson.periodos;
						this.state.materiasP[i] = responseJson.materias;
						this.state.materiasB[i] = responseJson.materiasB;
						this.state.losFaltantes[i] = responseJson.faltante;
					}
				});
		}

	}

	async getCalifPromedioMat() {
		for (let i = 0; i < this.state.alumnos.length; i++) {
			await fetch(this.state.uri + '/api/promedio/materias/alumnos/' + this.state.alumnos[i].id + '/' + this.state.elGrado, {
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}).then(res => res.json())
				.then(responseJson => {
					if (responseJson.error !== undefined) {
						Alert.alert('Error en movimiento de alumnos',
							'Ha ocurrido un error ' + responseJson.error.status_code +
							' al tratar de cargar la lista de alumnos, si el error continua pónganse en contacto con soporte (Mov de alumnos)', [
								{text: 'Enterado', onPress: () => this.setState({visible: false})}
							]);
					} else {
						this.state.califs[i] = responseJson;
						if (this.state.califs[i] !== 0) {
							let total = 0;
							let a = 0;
							let califs = this.state.califs[i];
							let reprobadas = [];
							califs.forEach(function (itm) {
								total += Number(itm);
								a++;
								if (itm <= 5.9) {
									reprobadas.push(itm);
								}
							});
							let totalCalif = total / a;
							this.state.totalCalifs[i] = totalCalif.toFixed(10).slice(0, -9);
							this.state.totalReprob[i] = reprobadas.length;
						} else {
							this.state.totalCalifs[i] = null;
							this.state.totalReprob[i] = null;
						}
					}
				});
		}
		await this.alumnosSinPasar();
	}

	async alumnosSinPasar() {
		for (let j = 0; j < this.state.alumnos.length; j++) {
			if (this.state.totalReprob[j] >= 5 ||
				this.state.totalCalifs[j] === null ||
				this.state.periodosB[j] < this.state.periodosP[j] ||
				this.state.materiasB[j] < this.state.materiasP[j]
			) {
				this.state.promocion[j] = false;
			}
		}
		this.setState({visible: false});
	}

	async noPromover(i) {
		if (this.state.noPromocion[i] === false) {
			this.state.noPromocion[i] = true;
		} else {
			this.state.noPromocion[i] = false;
		}
		await this.setState({aux: 0});
		await this.alertSwitch(i)
	}

	async alertSwitch(i) {
		if (this.state.noPromocion[i] === true && this.state.totalCalifs[i] === null) {
			Alert.alert('Sin calificación registradas', 'El alumno no cuenta con calificaciones registradas ', [
				{text: 'Enterado', onPress: () => this.noPromover(i)}
			]);
		} else if (this.state.noPromocion[i] === true && this.state.periodosB[i] < this.state.periodosP[i]) {
			Alert.alert('Periodos incompletos', 'Hay uno o mas calificaciones', [
				{text: 'Enterado', onPress: () => this.noPromover(i)}
			]);
		} else if (this.state.noPromocion[i] === true && this.state.materiasB[i] < this.state.materiasP[i]) {
			Alert.alert('Materias sin capturar aun', 'las materias son: ' + this.state.losFaltantes[i] + '', [
				{text: 'Enterado', onPress: () => this.noPromover(i)}
			]);
		} else if (this.state.noPromocion[i] === true && this.state.totalReprob[i] >= 5) {
			Alert.alert('Materias reprobadas', 'El alumno excede el numero de materias reprobadas permitidas', [
				{text: 'Enterado', onPress: () => this.noPromover(i)}
			]);
		}
		await this.setState({aux: 0});
	}

	async promover(i) {
		if (this.state.promocion[i] === false) {
			this.state.promocion[i] = true;
		} else {
			this.state.promocion[i] = false;
		}
		await this.setState({aux: 0});
	}

	async onListPressedAlumno(indexAlumno, alumno, padreId, id) {
		await this.setState({
			selectedIndexAlumno: indexAlumno,
			elAlumno: alumno,
			alumnosId: id,
			padreId: padreId
		});
	}

	renderAlumno(itemAlumno, indx) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla];
		if (indx !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexAlumno === indx) {
			smallButtonStyles.push(styles.rowTabla, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		if (this.state.selected === 'promovido') {
			return (
				<View
					key={indx + 'Al'}
					underlayColor={this.state.secondColor}
					style={tabRow}
				>
					<View style={[styles.campoTablaG, styles.modalWidth, styles.row]}>
						<Text style={{width: responsiveWidth(45)}}>{itemAlumno.name}</Text>
						<View style={{width: responsiveWidth(45), alignItems: 'center'}}>
							{this.state.selected === 'promovido' ? (
								<View style={[styles.row]}>
									<Text style={{width: responsiveWidth(15), textAlign: 'center'}}>
										{this.state.totalReprob[indx] === null ? '0' : this.state.totalReprob[indx]}
									</Text>
									<Text style={{width: responsiveWidth(15), textAlign: 'center'}}>
										{this.state.totalCalifs[indx] === null ? '0' : this.state.totalCalifs[indx]}
									</Text>
									<View style={{width: responsiveWidth(15), alignItems: 'center'}}>
										{this.state.totalReprob[indx] >= 5 ||
										this.state.totalCalifs[indx] === null ||
										this.state.periodosB[indx] < this.state.periodosP[indx] ||
										this.state.materiasB[indx] < this.state.materiasP[indx] ? (
											<Switch value={this.state.noPromocion[indx]}
													onSyncPress={() => this.noPromover(indx)}/>
										) : (
											<Switch value={this.state.promocion[indx]}
													onSyncPress={() => this.promover(indx)}/>
										)}
									</View>
								</View>
							) : null}
						</View>
					</View>
				</View>
			);
		} else {
			return (<TouchableHighlight
				key={indx + 'Al'}
				underlayColor={this.state.secondColor}
				style={[tabRow, smallButtonStyles]}
				onPress={() => this.onListPressedAlumno(indx, itemAlumno.name, itemAlumno.padre_id, itemAlumno.id)}>
				<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
					<Text style={[texto]}>{itemAlumno.name}</Text>
				</View>
			</TouchableHighlight>);
		}
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}


	async onChange(option) {
		await this.setState({
			textInputValue: option.label,
			selected: option.id,
			elAño: '',
			updateG: false,
			indexSelected: 0,
			alumnos: [],
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexGrupos: -1,
			elGrupo: ''
		});
		await this.setState({updateG: true});
	}

	//+-+-+--+-+-+-+-+-+-+-+-+-+-+listas de egresados
	async onChangeY(option) {
		await this.setState({
			elAño: option.label
		});
		await this.getAlumnosEgresados();
	}

	async getAlumnosEgresados() {
		await fetch(this.state.uri + '/api/alumno/egresado/' + this.state.elAño, {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar a los alumnos egresados, si el error continua pónganse en contacto con soporte (Mov alumnos)', [
							{text: 'Entendido', onPress: () => this.setState({visible2: false})}
						]);
				} else {
					this.setState({losEgresados: responseJson});
				}
			});
	}

	rndEgresados() {
		let egresados = [];
		this.state.losEgresados.forEach((it, ix) => {
			let a = this.state.losEgresados.length - 1;
			let tabRow = [styles.rowTabla];
			if (ix !== a) {
				tabRow.push({borderColor: this.state.secondColor});
			}
			egresados.push(
				<View
					key={ix + 'egre'}
					underlayColor={this.state.secondColor}
					style={[tabRow]}>
					<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
						<Text>{it.name}</Text>
					</View>
				</View>
			)
		});
		return egresados;
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-alumnos baja
	async getAlumnosBaja() {
		await fetch(this.state.uri + '/api/alumno/baja', {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar a los alumnos egresados, si el error continua pónganse en contacto con soporte (Mov alumnos)', [
							{text: 'Entendido', onPress: () => this.setState({visible2: false})}
						]);
				} else {
					this.setState({alumnosBaja: responseJson});
					let modalFalse = [];
					if (this.state.alumnosBaja.length !== 0) {
						this.state.alumnosBaja.forEach(() => {
							modalFalse.push(false);
						});
					}
					this.state.modalInfo = modalFalse;
					if (this.state.alumnosBaja.length === 0) {
						Alert.alert('Sin alumnos',
							'No hay alumnos dados de baja para consultar', [
								{text: 'Entendido'}
							]);
					}
				}
			});
	}

	async openModal(i) {
		this.state.modalInfo[i] = true;
		await this.setState({aux: 0});
	}

	async closeModal(i) {
		this.state.modalInfo[i] = false;
		await this.setState({aux: 0});
	}

	rndBajas() {
		let egresados = [];
		if (this.state.alumnosBaja.length !== 0) {
			this.state.alumnosBaja.forEach((it, ix) => {
				let a = this.state.alumnosBaja.length - 1;
				let tabRow = [styles.rowTabla];
				if (ix !== a) {
					tabRow.push({borderColor: this.state.secondColor});
				}
				egresados.push(
					<View
						key={ix + 'bajas'}
						underlayColor={this.state.secondColor}
						style={[tabRow]}>
						<View style={[styles.campoTablaG, styles.row, {alignItems: 'flex-start'}]}>
							<Text style={{width: responsiveWidth(46)}}>{it.name}</Text>
							<Text style={{width: responsiveWidth(33), textAlign: 'left'}}>{it.estatus}</Text>
							<View style={{width: responsiveWidth(10), alignItems: 'center'}}>
								<Entypo name='plus' size={19} onPress={() => this.openModal(ix)}/>
							</View>
						</View>
						<Modal
							isVisible={this.state.modalInfo[ix]}
							backdropOpacity={0.5}
							animationIn={'bounceIn'}
							animationOut={'bounceOut'}
							animationInTiming={1000}
							animationOutTiming={1000}
						>
							<KeyboardAvoidingView style={[styles.container, {borderRadius: 6, flex: 0}]}
												  keyboardVerticalOffset={Platform.OS === 'ios' ? 150 : 0}
												  behavior='padding'>
								<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
									Detalles de la baja
								</Text>
								<Text style={[styles.main_title, styles.modalWidth, {
									fontWeight: '500',
									fontSize: responsiveFontSize(1.8),
									color: '#000',
									marginTop: 15,
									textAlign: 'center'
								}]}>
									{it.name}
								</Text>
								<View style={[styles.row]}>
									<Text>Último grado y grupo:</Text>
									<Text style={[styles.textW]}> {it.grado}{it.grupo}</Text>
								</View>
								<View style={[styles.row]}>
									<Text>Fecha de la baja:</Text>
									<Text style={[styles.textW]}> {moment(it.updated_at).format('DD/MM/YYYY')}</Text>
								</View>
								<Text style={[styles.textW, {marginTop: 10}]}>Comentario:</Text>
								<View style={{
									paddingHorizontal: 25,
									marginBottom: 15
								}}>
									<Text style={[{textAlign: 'center'}]}>
										{it.comentario === '' ? 'No hay comentario registrado' : it.comentario}
									</Text>
								</View>
								<TouchableOpacity
									style={[
										styles.modalBigBtn,
										{backgroundColor: '#fff', borderColor: this.state.secondColor, marginTop: 0}
									]}
									onPress={() => this.closeModal(ix)}
								>
									<Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
								</TouchableOpacity>
							</KeyboardAvoidingView>
						</Modal>
					</View>
				)
			});
		}
		return egresados;
	}

	render() {
		let index = 0;
		const data = [
			{key: index++, label: 'Promovidos', id: 'promovido'},
			{key: index++, label: 'Egresados', id: 'egresado'},
			{key: index++, label: 'Bajas', id: 'baja'}
		];
		let year = this.state.yearPicker.map((it, i) => {
			return {key: i, label: it};
		});
		let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		if (this.state.indexSelected === 1) {
			btnCal.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
			texto.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		let texto1 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
		let btnTem = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
		if (this.state.indexSelected === 0) {
			btnTem.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
			texto1.push(styles.textoB, {
				fontSize: responsiveFontSize(1.7), fontWeight: '700'
			});
		}
		return (
			<View style={styles.container}>
				<Modal
					isVisible={this.state.modalMotivo}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<KeyboardAvoidingView style={[styles.container, {borderRadius: 6, flex: 0}]}
										  keyboardVerticalOffset={Platform.OS === 'ios' ? 150 : 0} behavior='padding'>
						<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
							Indique el motivo de la baja del alumno
						</Text>
						<View
							style={[styles.tabla, {
								borderColor: this.state.secondColor,
								marginTop: 10
							}]}>
							{this.rndMotivos()}
						</View>
						<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
							Escriba un comentario
						</Text>
						<TextInput
							keyboardType='default'
							returnKeyType='next'
							multiline={true}
							placeholder={'Escriba el motivo por el cual este dando de baja al alumno ' + this.state.elAlumno + '. Sea lo más detallado posible...'}
							underlineColorAndroid='transparent'
							onChangeText={text => (this.state.comentario = text)}
							onEndEditing={() => Keyboard.dismiss()}
							style={[
								styles.inputContenido,
								styles.modalWidth,
								{borderColor: this.state.secondColor, padding: 5, height: responsiveHeight(20)}
							]}
						/>
						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({
									modalMotivo: false,
									comentario: '',
									elMotivo: '',
									indxMotivo: -1
								})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.bajaAlumno()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
							</TouchableOpacity>
						</View>
					</KeyboardAvoidingView>
				</Modal>
				<Spinner visible={this.state.visible} textContent='Cargando...'/>
				<Spinner visible={this.state.visible2} textContent='Guardando...'/>

				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione tipo de movimiento
				</Text>
				<ModalSelector
					data={data}
					selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor}}
					initValue={this.state.textInputValue}
					onChange={option => this.onChange(option)}
				/>
				<ScrollView>
					{this.state.selected === 'promovido' || this.state.selected === 'baja' ? (
						<View>
							{this.state.selected === 'baja' ? (
								<View style={[styles.widthall, {alignItems: 'center'}]}>
									<View
										style={[styles.rowsCalif, {
											width: responsiveWidth(62), marginTop: 10
										}]}>
										<TouchableHighlight
											underlayColor={'transparent'}
											style={btnTem}
											onPress={() => this.botonSelected(0)}>
											<Text style={texto1}>Dar de baja</Text>
										</TouchableHighlight>
										<TouchableHighlight
											underlayColor={'transparent'}
											style={btnCal}
											onPress={() => this.botonSelected(1)}>
											<Text style={texto}>Consulta</Text>
										</TouchableHighlight>
									</View>
								</View>) : null}
							{this.state.indexSelected === 0 && this.state.updateG === true ? (
								<GradoyGrupo
									onListItemPressedGrupos={(indexBtn, itemBtn) =>
										this.onListItemPressedGrupos(indexBtn, itemBtn)
									}
									onListItemPressedGrado={(indexBtn, itemBtn) =>
										this.onListItemPressedGrado(indexBtn, itemBtn)
									}
								/>
							) : null}
						</View>
					) : null}
					{this.state.selected === 'egresado' ? (
						<View>
							<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
								Seleccione una fecha
							</Text>
							{this.state.yearPicker.length !== 0 ? (
								<ModalSelector
									data={year}
									selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
									cancelText='Cancelar'
									optionTextStyle={{color: this.state.thirdColor}}
									initValue='Seleccione una opción'
									onChange={option => this.onChangeY(option)}
								/>
							) : (
								<TouchableOpacity
									style={[styles.inputPicker, {borderColor: this.state.secondColor}]}
									onPress={() => Alert.alert('No hay egresados', 'No tiene alumnos egresados por el momento', [
										{text: 'Entendido'}
									])}
								>
									<Text>Seleccione una opción</Text>
								</TouchableOpacity>
							)}
						</View>
					) : null}
					<Text
						style={[styles.main_title, {
							color: this.state.thirdColor,
							marginTop: this.state.selected === 'egresado' || this.state.indexSelected === 1 ? 10 : 0
						}]}>
						Lista de alumnos
					</Text>
					{this.state.selected === 'promovido' ? (
						this.state.elGrado !== '' && this.state.elGrupo !== '' ? (
							<View style={{width: responsiveWidth(94), alignItems: 'center'}}>
								<View style={styles.row}>
									<View style={{width: responsiveWidth(46)}}>
										<Text style={styles.textW}>Alumno</Text>
									</View>
									<View
										style={{width: responsiveWidth(15)}}>
										<Text style={[styles.textW, {
											textAlign: 'center',
											fontSize: responsiveFontSize(1.2)
										}]}>Materias{'\n'}reprobadas</Text>
									</View>
									<View
										style={{width: responsiveWidth(15)}}>
										<Text style={[styles.textW, {
											textAlign: 'center',
											fontSize: responsiveFontSize(1.2)
										}]}>Promedio{'\n'}general</Text>
									</View>
									<View
										style={{width: responsiveWidth(15)}}>
										<Text style={[styles.textW, {
											textAlign: 'center',
											fontSize: responsiveFontSize(1.2)
										}]}>Promover{'\n'}de grado</Text>
									</View>
								</View>

							</View>
						) : null
					) : null}
					{this.state.selected === 'promovido' || this.state.selected === 'baja' && this.state.indexSelected === 0 ? (
						this.state.elGrado !== '' && this.state.elGrupo !== '' ? (
							<View
								style={[
									styles.tabla,
									styles.widthall,
									{
										borderColor: this.state.secondColor,
										marginTop: 5,
										paddingVertical: this.state.selected === 'promovido' ? 5 : 0
									}
								]}>
								{this.renderAlumnos()}
							</View>
						) : (
							<Text style={{color: 'grey'}}>Haga primero una selección</Text>
						)
					) : this.state.selected === 'egresado' ? (
						this.state.elAño !== '' ? (
							<View
								style={[
									styles.tabla,
									styles.widthall,
									{
										borderColor: this.state.secondColor,
										marginTop: 5,
										paddingVertical: this.state.selected === 'promovido' ? 5 : 0
									}
								]}>
								{this.rndEgresados()}
							</View>
						) : (
							<Text style={{color: 'grey'}}>Haga una seleccion</Text>
						)
					) : null}
					{this.state.indexSelected === 1 ? (
						<View style={{width: responsiveWidth(94), alignItems: 'flex-start'}}>
							<View style={styles.row}>
								<View style={{width: responsiveWidth(46)}}>
									<Text style={[styles.textW, {textAlign: 'left'}]}>Alumno</Text>
								</View>
								<View
									style={{width: responsiveWidth(32)}}>
									<Text style={[styles.textW, {
										textAlign: 'center',
										fontSize: responsiveFontSize(1.2)
									}]}>Motivo de baja</Text>
								</View>
								<View
									style={{width: responsiveWidth(18)}}>
									<Text style={[styles.textW, {
										textAlign: 'center',
										fontSize: responsiveFontSize(1.2)
									}]}>Info.</Text>
								</View>
							</View>
							<View
								style={[
									styles.tabla,
									styles.widthall,
									{
										borderColor: this.state.secondColor,
										marginTop: 5,
										paddingVertical: this.state.selected === 'promovido' ? 5 : 0
									}
								]}>
								{this.rndBajas()}
							</View>
						</View>
					) : null}
					{this.state.selected === 'promovido' ? (<TouchableOpacity
						style={[
							styles.bigButton,
							{backgroundColor: this.state.secondColor}
						]}
						onPress={() => this.alertPromover()}
					>
						<Text style={styles.textButton}>Promover</Text>
					</TouchableOpacity>) : this.state.selected === 'baja' && this.state.indexSelected === 0 ? (
						<TouchableOpacity
							style={[
								styles.bigButton,
								{backgroundColor: this.state.secondColor}
							]}
							onPress={() => this.modaBAja()}
						>
							<Text style={styles.textButton}>Dar de baja</Text>
						</TouchableOpacity>
					) : null}
				</ScrollView>
			</View>
		);
	}
}
