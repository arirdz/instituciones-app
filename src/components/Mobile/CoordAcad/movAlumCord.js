import React from "react";
import {
    View,
    AsyncStorage,
    StatusBar,
} from "react-native";
import styles from "../../styles";
import MultiBotonRow from "../Globales/MultiBotonRow";
import CambioGrupo from "./CambioGrupo";
import OtrosMov from "./OtrosMov";

export default class movAlumCord extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            botonSelected: "Cambio de grupo"
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <MultiBotonRow
                    itemBtns={["Cambio de grupo", "Otros movimientos"]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />
                {this.state.botonSelected === "Cambio de grupo" ? (
                    <CambioGrupo/>
                ) : null}
                {this.state.botonSelected === "Otros movimientos" ? <OtrosMov/> : null}
            </View>
        );
    }
}
