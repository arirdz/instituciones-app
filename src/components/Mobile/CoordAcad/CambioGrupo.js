import React from 'react';
import {
	Text,
	View,
	Alert,
	AsyncStorage,
	TouchableOpacity,
	ScrollView,
	TouchableHighlight
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';
import {
	responsiveFontSize,
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';
import {Actions} from 'react-native-router-flux';
import Entypo from 'react-native-vector-icons/Entypo';

export default class cambioGrup extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mujeres: [],
			hombres: [],
			grupos: [],
			aux: 0,
			selected: '',
			losGrupos: [],
			botonSelected: 'Cambio de grupo',
			indexSelectedTab: 0,
			alumnos: [],
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAlumno: '',
			elAlumno2: '',
			alumnosId: '',
			alumnosId2: '',
			elReceptor: '',
			elReceptor2: '',
			selectedIndexGrupos: -1,
			numeroAlumn: [],
			elGrupoPicker: '',
			elGrupo: '',
			isModalCateg: false,
			totalGrupo: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		await this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async alertNotificacion() {
		if (this.state.selected === '') {
			Alert.alert('Sin grupo de destino definido', 'Seleccione un grupo de destino para poder continuar', [
				{text: 'Entendido'}
			]);
		} else {
			Alert.alert('Cambio de grupo',
				'Está a punto de notificar el cambio de grupo a ' + this.state.elAlumno2 + ' de ' + this.state.elGrado + '' + this.state.elGrupo + ' al grupo ' + this.state.elGrado + '' + this.state.elGrupoPicker + '\n¿Seguro que desea continuar?', [
					{text: 'Sí', onPress: () => this.NotificarCambioGrupo()}, {text: 'No'}
				]);
		}
	}

	async NotificarCambioGrupo() {
		await fetch(this.state.uri + '/api/notificacion/cambio/almuno/' +
			this.state.elReceptor2 +
			'/' +
			this.state.alumnosId2 +
			'/' +
			this.state.elGrupoPicker + '' +
			'/' +
			this.state.elGrupo, {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de enviar la notificación si el error continua pónganse en contacto con soporte (Cambio de grupo)');
				} else {
					Alert.alert('Notificación enviada', 'Se ha notificado al personal administrativo de la escuela y al padre de familia o tutor sobre el cambio del alumno', [
						{text: 'Entendido'}
					])
				}
			});
	}

	async alertCambio() {
		if (this.state.selected === '') {
			Alert.alert('Sin grupo de destino definido', 'Seleccione un grupo de destino para poder continuar', [
				{text: 'Entendido'}
			]);
		} else {
			Alert.alert('Cambio de grupo',
				'Está a punto de cambiar a ' + this.state.elAlumno2 + ' de ' + this.state.elGrado + '' + this.state.elGrupo + ' a ' + this.state.elGrado + '' + this.state.elGrupoPicker + '\n¿Seguro que desea continuar?', [
					{text: 'Sí', onPress: () => this.getCambioDeGrupo()}, {text: 'No'}
				]);
		}
	}

	async getCambioDeGrupo() {
		await fetch(this.state.uri + '/api/update/alumno/' + this.state.alumnosId2 + '/' + this.state.elGrupoPicker, {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de hacer el cambio si el error continua pónganse en contacto con soporte (Cambio de grupo)');
				} else {
					Alert.alert('Cambio de grupo completo', 'Se ha cambiado al alumno de grupo correctamente', [
						{text: 'Entendido', onPress: () => Actions.HomePage()}
					])
				}
			});
	}

	async getGrupos() {
		let grupopicker = await fetch(this.state.uri + '/api/get/grupos', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let grupospicker = await grupopicker.json();
		await this.setState({grupos: grupospicker});
		await this.setState({losGrupos: []});
		this.state.grupos.forEach((it) => {
			if (it.grupo !== 'Todos') {
				if (this.state.elGrupo !== it.grupo) {
					this.state.losGrupos.push(it.grupo)
				}
			}
		});
		await this.setState({aux: 0});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo,
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAlumno: '',
			elAlumno2: '',
			alumnosId: '',
			alumnosId2: '',
			elReceptor: '',
			elReceptor2: ''
		});
		await this.getAlumnosByGG();
		await this.getGrupos();
	}

	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado,
			selectedIndexGrupos: -1,
			elGrupo: '',
			selectedIndexAlumno: -1,
			selectedIndexAlumno2: -1,
			elAlumno: '',
			elAlumno2: '',
			alumnosId: '',
			alumnosId2: '',
			elReceptor: '',
			elReceptor2: ''
		});
	}

	//-+-+-+-+-+-+-+--+-+-+-+
	async getAlumnosByGG2(grupo) {
		await fetch(
			this.state.uri + '/api/get/alumnos/by/grado/grupo/' + this.state.elGrado + '/' + grupo,
			{
				method: 'GET', headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					this.setState({mujeres: [], hombres: []});
					this.setState({numeroAlumn: responseJson});
					this.state.numeroAlumn.forEach((it) => {
						if (it.genero === 'femenino' || it.genero === 'Femenino') {
							this.state.mujeres.push(it);
						}
						if (it.genero === 'masculino' || it.genero === 'Masculino') {
							this.state.hombres.push(it);
						}
					});
				}
			});
		await this.setState({aux: 0});
	}

	async getTotalAlumnosByG(grupo) {
		await fetch(this.state.uri + '/api/get/limitegrupo/' + this.state.elGrado + '/' + grupo, {
			method: 'GET', headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error en movimiento de alumnos',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar las horas si el error continua pónganse en contacto con soporte (Citas)');
				} else {
					this.setState({totalGrupo: responseJson[0]});
				}
			});
	}

	//++++++++++++++++++++++++++ Alumnos grado y grupo++++++++++++++++++++++++++++++
	async getAlumnosByGG() {
		let alumnopicker = await fetch(
			this.state.uri + '/api/get/alumnos/by/grado/grupo/' + this.state.elGrado + '/' + this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let alumnospicker = await alumnopicker.json();
		this.setState({alumnos: alumnospicker});
	}

	async onListPressedAlumno(indexAlumno, alumno, padreId, id) {
		await this.setState({
			selectedIndexAlumno: indexAlumno,
			elAlumno: alumno,
			alumnosId: id,
			elReceptor: padreId,
			selected:'',
		});
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexAlumno === indexAlumno) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (<TouchableHighlight
			key={indexAlumno + 'Al'}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.name, itemAlumno.padre_id, itemAlumno.id)}>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>{itemAlumno.name}</Text>
			</View>
		</TouchableHighlight>);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-
	async openListAlumn() {
		this.state.elAlumno = this.state.elAlumno2;
		this.state.alumnosId = this.state.alumnosId2;
		this.state.elReceptor = this.state.elReceptor2;
		this.state.selectedIndexAlumno = this.state.selectedIndexAlumno2;
		if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacío', 'Seleccione un grado para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacío', 'Seleccione un grupo para poder continuar', [
				{text: 'Entendido'}
			])
		} else if (this.state.alumnos.length === 0) {
			Alert.alert('Sin alumnos', 'No hay alumnos en este grupo',
				[{text: 'Enterado'}])
		} else {
			this.setState({isModalCateg: true, selectedIndexAlumno: this.state.selectedIndexAlumno2});
		}
	}

	async closeListAlumn() {
		if (this.state.selectedIndexAlumno === -1) {
			Alert.alert('Alumno no seleccionado', '', [{text: 'Entendido'}]);
		} else {
			this.state.elAlumno2 = this.state.elAlumno;
			this.state.alumnosId2 = this.state.alumnosId;
			this.state.elReceptor2 = this.state.elReceptor;
			this.state.selectedIndexAlumno2 = this.state.selectedIndexAlumno;
			this.setState({isModalCateg: false});
		}
	}

	async onChange(option) {
		await this.setState({
			selected: option.label,
			elGrupoPicker: option.grupo
		});
		await this.getAlumnosByGG2(option.grupo);
		await this.getTotalAlumnosByG(option.grupo);
	}

	alertD() {
		if (this.state.elAlumno2 ===''){
			Alert.alert('Sin alumno seleccionado', 'Seleccione un alumno para escoger el grupo de destino', [{text: 'Entendido'}]);
		} else if (this.state.losGrupos.length === 0){
			Alert.alert('Sin grupos', 'No hay otros grupos de destino', [{text: 'Entendido'}]);
		}

	}

	render() {
		let data = this.state.losGrupos.map((it, i) => {
			return {
				key: i, label: this.state.elGrado + ' ' + it, grupo: it
			}
		});
		let texto = [{color: '#000'}];
		let espacio = this.state.totalGrupo - this.state.numeroAlumn.length;
		if (this.state.totalGrupo === '') {
			texto.push({color: '#000'});
		} else if (espacio === 0) {
			texto.push({color: '#e10004'});
		} else {
			texto.push({color: '#1d9018'});
		}
		return (
			<View style={styles.container}>
				<Modal
					isVisible={this.state.isModalCateg}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione el alumno:
						</Text>
						<View
							style={[styles.tabla, {
								borderColor: this.state.secondColor,
								marginTop: 10,
								height: responsiveHeight(60)
							}]}>
							<ScrollView>
								{this.renderAlumnos()}
							</ScrollView>
						</View>

						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalCateg: false})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.closeListAlumn()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<ScrollView style={{marginBottom: 20}}>
					<GradoyGrupo
						onListItemPressedGrupos={(indexBtn, itemBtn) =>
							this.onListItemPressedGrupos(indexBtn, itemBtn)
						}
						onListItemPressedGrado={(indexBtn, itemBtn) =>
							this.onListItemPressedGrado(indexBtn, itemBtn)
						}
					/>
					<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 0, paddingTop: 0}]}>
						Seleccione al alumno
					</Text>
					<View style={{
						alignItems: 'center',
						backgroundColor: this.state.fourthColor,
						padding: 15,
						borderRadius: 6
					}}
					>
						<TouchableOpacity
							style={[
								styles.bigButton,
								styles.btnShowMod,
								{
									marginTop: 5,
									marginBottom: 5,
									borderColor: this.state.secondColor,
									backgroundColor: '#fff'
								}
							]}
							onPress={() => this.openListAlumn()}>
							<Text>Ver lista</Text>
						</TouchableOpacity>
						<Text style={{marginTop: 10}}>Alumno seleccionado:</Text>
						<Text style={[styles.textButton, {color: 'black'}]}>
							{this.state.elAlumno2}
						</Text>
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione el grupo de destino
					</Text>
					{this.state.elAlumno2 !== '' && this.state.losGrupos.length !== 0 ? (
						<ModalSelector
							cancelText='Cancelar'
							data={data}
							initValue='Seleccione el grupo'
							onChange={option => this.onChange(option)}
							optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
							selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
							selectStyle={[
								styles.inputPicker,
								{borderColor: this.state.secondColor, marginTop: 2}
							]}
						/>
					) : (
						<TouchableOpacity
							style={[
								styles.inputPicker,
								{borderColor: this.state.secondColor, marginTop: 2}
							]}
							onPress={() => this.alertD()}
						>
							<Text style={{fontSize: responsiveFontSize(1.8)}}>Seleccione el grupo</Text>
						</TouchableOpacity>
					)}
					<View
						style={[
							styles.row,
							styles.widthall,
							{marginTop: 15}
						]}>
						<View
							style={{
								width: responsiveWidth(47),
								paddingLeft: 5,
								alignItems: 'flex-start'
							}}>
							<Text style={{fontWeight: '700'}}>Datos relevantes de "{this.state.selected}"</Text>
						</View>
						<View
							style={{width: responsiveWidth(47), alignItems: 'center'}}>
							<Text style={{fontWeight: '700'}}>Contenido</Text>
						</View>
					</View>
					<View
						style={[styles.widthall, styles.row, {
							paddingVertical: 5,
							borderTopWidth: .5,
							borderBottomWidth: .5,
							borderColor: 'grey',
							marginTop: 5
						}]}>
						<Text style={{width: responsiveWidth(47), paddingLeft: 5}}>Espacios disponibles</Text>
						<Text style={[texto, {
							width: responsiveWidth(47),
							textAlign: 'center',
							justifyContent: 'center'
						}]}>
							{this.state.totalGrupo !== '' ? espacio + '/' + this.state.totalGrupo : '0'}
						</Text>
					</View>

					<View style={[styles.widthall, styles.row, {
						borderBottomWidth: .5,
						borderColor: 'grey',
						paddingVertical: 5
					}]}>
						<Text style={{
							width: responsiveWidth(47),
							paddingLeft: 5,
							textAlign: 'left'
						}}>
							Hombres y mujeres
						</Text>
						<View
							style={{
								width: responsiveWidth(47),
								alignItems: 'center',
								justifyContent: 'center'
							}}>
							<Text>M: {this.state.mujeres.length}{'    '}H: {this.state.hombres.length}</Text>
						</View>
					</View>
					<View style={[styles.widthall, styles.row, {
						borderBottomWidth: .5,
						borderColor: 'grey',
						paddingVertical: 5
					}]}>
						<Text style={{width: responsiveWidth(47), textAlign: 'left', paddingLeft: 5}}>
							Promedio general del grupo
						</Text>
						<Text style={{width: responsiveWidth(47), textAlign: 'center'}}>No disponible</Text>
					</View>
					<View style={[styles.row, styles.widthall, {marginTop: 15}]}>
						<TouchableOpacity
							style={[styles.mediumBtn, {
								borderColor: this.state.secondColor,
								backgroundColor: this.state.secondColor
							}]}
							onPress={() => this.alertNotificacion()}
						>
							<Text style={styles.textButton}>Enviar notificación</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[styles.mediumBtn, {
								borderColor: this.state.secondColor,
								backgroundColor: this.state.secondColor
							}]}
							onPress={() => this.alertCambio()}
						>
							<Text style={styles.textButton}>Aplicar ahora</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</View>
		);
	}
}
