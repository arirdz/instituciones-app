import React from 'react';
import {
	Alert, AsyncStorage, RefreshControl, ScrollView, StatusBar, Text, TouchableHighlight, TouchableOpacity, View
} from 'react-native';
import styles from '../../styles';
import {responsiveFontSize} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import {Actions} from 'react-native-router-flux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class HistorialEvent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			refreshing: false,
			eventos: [],
			cososPicker: [],
			mes: moment().month() + 1
		};
	}

	_onRefresh = () => {
		this.setState({refreshing: true});
		this.getEventsByMonth(this.state.mes);

	};

	async componentWillMount() {
		await this.getURL();
		await this.getMeses();
		await this.getEventsByMonth(this.state.mes);
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getMeses() {
		let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses', {
			method: 'GET', headers: {
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let datosPicker = await dataPicker.json();
		this.setState({cososPicker: datosPicker});
	}

	async onChange(option) {
		await this.setState({mes: option.mes});
		await this.getEventsByMonth(this.state.mes);
	}

	async getEventsByMonth(mes) {
		await fetch(this.state.uri + '/api/historial/event/' + mes)
			.then(response => response.json())
			.then(responseJson => {
				this.setState({refreshing: false});
				if (responseJson.error === null) {
					Alert.alert('Error al cargar eventos, Sí el error continua pónganse en contacto con soporte (Calendario de eventos)', [
						{text: 'Enterado', onPress: () => Actions.pop({refresh: {key: 'Drawer'}})}
					]);
				} else {
					this.setState({eventos: responseJson});
				}
			});
	}

	async alertEditar(itm) {
		Alert.alert('Editar evento', 'Está a punto de editar este evento\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => Actions.editarEvento({elEvento: itm, roles: [
						{label: 'Padres', role: 'Padre'},
						{label: 'Alumnos', role: 'Alumno'},
						{label: 'Admón.', role: 'Admin'},
						{label: 'Maestros', role: 'Maestro'}
					]})}, {text: 'No'}
		]);
	}

	rndEvents() {
		let events = [];
		if (this.state.eventos.length !== 0) {
			this.state.eventos.forEach((itm, i) => {
				events.push(
					<View
						key={i + 'evento'}
						style={[
							styles.widthall,
							styles.row,
							{
								borderWidth: 0.5,
								borderRadius: 6,
								borderColor: 'lightgray',
								marginVertical: 5,
								backgroundColor: this.state.fourthColor,
								paddingHorizontal: 10,
								paddingVertical: 10,
								justifyContent: 'flex-start'
							}
						]}>
						<View style={[styles.btn3]}>
							<Text style={[styles.textW, {textAlign: 'left', fontSize: responsiveFontSize(1.3)}]}>
								Nombre del evento
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
								marginTop: 3
							}}>{itm.titulo}</Text>
						</View>
						<View style={[styles.btn2]}>
							<Text style={[styles.textW, {textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
								Fecha inicio
							</Text>
							<Text style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'center',
								marginTop: 3
							}}>{moment(itm.fecha_inicio).format('ll').toUpperCase()}</Text>
						</View>
						<View style={[styles.btn6, {alignItems: 'center'}]}>
							<Text style={[styles.textW, {
								textAlign: 'center',
								fontSize: responsiveFontSize(1.3),
								marginTop: 3
							}]}>
								Editar
							</Text>
							<MaterialIcons name='edit' size={21} color='#000' onPress={() => this.alertEditar(itm)}/>
						</View>
					</View>
				)
			})
		}
		return events;
	}

	render() {
		let data = this.state.cososPicker.map((item, i) => {
			return {key: i, mes: item.mes, label: item.nombre_mes};
		});
		let mess = 'Enero';
		if (this.state.cososPicker !== '') {
			if (this.state.cososPicker.length !== 0) {
				mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
			}
		}
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Elija un mes a consultar
				</Text>
				<ModalSelector
					data={data}
					selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
					selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
					initValue={mess}
					onChange={option => this.onChange(option)}
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Estas son los eventos del mes seleccionado
				</Text>
				<ScrollView
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}
					overScrollMode='always'
					showsVerticalScrollIndicator={false}
					style={{...ifIphoneX({marginBottom: 35}, {marginBottom: 20})}}
				>
					{this.rndEvents()}
				</ScrollView>
			</View>
		);
	}
}
