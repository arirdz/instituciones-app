import React from 'react';
import {Text, View} from 'react-native';
import {Agenda, LocaleConfig} from 'react-native-calendars';
import {responsiveHeight, responsiveWidth, responsiveFontSize} from 'react-native-responsive-dimensions';
import styles from '../../styles';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

LocaleConfig.locales['es_MX'] = {
	monthNames: [
		'Enero',
		'Febrero',
		'Marzo',
		'Abril',
		'Mayo',
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre'
	],
	monthNamesShort: [
		'Ene.',
		'Feb.',
		'Mar',
		'Abr',
		'May',
		'Jun',
		'Jul.',
		'Ago',
		'Sept.',
		'Oct.',
		'Nov.',
		'Dec.'
	],
	dayNames: [
		'Domingo',
		'Lunes',
		'Martes',
		'Miércoles',
		'Jueves',
		'Viernes',
		'Sábado'
	],
	dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vier.', 'Sab.']
};

LocaleConfig.defaultLocale = 'es_MX';

const vacation = {key: 'vacation', color: 'red', selectedColor: 'blue'};
const massage = {key: 'massage', color: 'blue', selectedColor: 'blue'};
const workout = {key: 'workout', color: 'green'};
export default class CalendarCord extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			items: {}
		};
	}

	loadItems(day) {
		setTimeout(() => {
			for (let i = -15; i < 85; i++) {
				const time = day.timestamp + i * 24 * 60 * 60 * 1000;
				const strTime = this.timeToString(time);
				if (!this.state.items[strTime]) {
					this.state.items[strTime] = [];
					const numItems = Math.floor(Math.random() * 5);
					for (let j = 0; j < numItems; j++) {
						this.state.items[strTime].push({
							name: 'Evento el dia ' + strTime,
							height: Math.max(50, Math.floor(Math.random() * 75))
						});
					}
				}
			}
			const newItems = {};
			Object.keys(this.state.items).forEach(key => {
				newItems[key] = this.state.items[key];
			});
			this.setState({
				items: newItems
			});
		}, 1000);
	}

	renderItem(item) {
		return (
			<View
				style={[
					styles.item,
					{
						height: item.height,
						backgroundColor: 'white',
						flex: 1,
						borderRadius: 5,
						padding: 10,
						marginRight: 10,
						marginTop: 17
					}
				]}>
				<Text>{item.name}</Text>
			</View>
		);
	}

	renderEmptyDate() {
		return (
			<View
				style={{
					height: responsiveHeight(10),
					flex: 1,
					paddingTop: 30
				}}>
				<Text>No hay eventos</Text>
			</View>
		);
	}

	rowHasChanged(r1, r2) {
		return r1.name !== r2.name;

	}

	timeToString(time) {
		const date = new Date(time);
		return date.toISOString().split('T')[0];
	}

	render() {
		return (
				<Agenda
					items={this.state.items}
					loadItemsForMonth={this.loadItems.bind(this)}
					selected={moment().format('YYYY/MM/DD')}
					renderItem={this.renderItem.bind(this)}
					renderEmptyDate={this.renderEmptyDate.bind(this)}
					rowHasChanged={this.rowHasChanged.bind(this)}
					markedDates={{
						'2017-12-13': {dots: [vacation, massage, workout], selected: false},
						'2017-12-16': {dots: [massage, workout], disabled: false}
					}}
					markingType={'multi-dot'}
				/>
		);
	}
}
