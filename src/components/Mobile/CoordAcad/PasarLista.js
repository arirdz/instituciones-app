import React from "react";
import {AsyncStorage, ScrollView, StatusBar, Text, TouchableOpacity, View} from "react-native";
import styles from "../../styles";
import GradoyGrupo from '../Globales/GradoyGrupo'
import MultiBotonRow from "../Globales/MultiBotonRow";
import Switch from 'react-native-switch-pro';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from "react-native-responsive-dimensions";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class PasarLista extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            elGrado: '',
            elGrupo: '',
            alumnos: [],
            horarios: '',
            materias: '',
            laMateria: '',
            indexSelectedTab: 0,
            botonSelectedTab: 'Pasar lista',
            asistio: [],
            justif: [],
            faltas: [],
            tarde: [],
            retardos: [],
            justifcado: []
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let mainColor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let token = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");
        this.setState({
            fourthColor: fourthColor,
            mainColor: mainColor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async botonSelectedTab(indexSelected, itemSelected) {
        await this.setState({
            botonSelectedTab: itemSelected,
            indexSelectedTab: indexSelected
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo, elGrupo: grupo
        });
        if (this.state.elGrado !== '') {
            await this.getPeriodo();
            await this.getAlumnos();
        }
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado, elGrado: grado
        });
        if (this.state.elGrupo !== '') {
            await this.getPeriodo();
            await this.getAlumnos();
        }
    }

    async getPeriodo() {
        await fetch(this.state.uri + '/api/ciclo/periodo/actual/' + this.state.elGrado, {
            method: 'get',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({elPeriodo: responseJson.periodo, elCiclo: responseJson.ciclo});
            });
    }


    // ++++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++++
    async getAlumnos() {
        let alumnopicker = await fetch(
            this.state.uri +
            '/api/get/alumnos/by/grado/grupo/' +
            this.state.elGrado +
            '/' +
            this.state.elGrupo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});

        alumnospicker.forEach(() => {
            this.state.asistio.push(true);
        });
        this.setState({aux: 0})
    }


    async asistenciar(index) {
        if (this.state.asistio[index] === false) {
            this.state.asistio[index] = true;
        } else {
            this.state.asistio[index] = false;
            this.state.tarde[index] = false;
        }
        await this.setState({aux: 0});
    }

    isAsistio(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View style={[styles.btn3_5]}>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <Switch
                            key={itemAlumno.id}
                            value={this.state.asistio[indexAlumno]}
                            onSyncPress={() => this.asistenciar(indexAlumno)}
                        />
                    </View>
                </View>
            </View>
        );
    }


    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {this.state.indexSelectedTab === 0
                    ? this.isAsistio(itemAlumno, indexAlumno)
                    : null}
                {this.state.indexSelectedTab === 1
                    ? null
                    : null}
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async requestMultipart() {
        let objeto = [];
        let a = 1;
        for (let i = 0; i < this.state.alumnos.length; i++) {
            if (this.state.asistio[i]) {
                a = 1
            } else {
                a = 0
            }
            objeto.push({
                id_alumno: this.state.alumnos[i].id,
                ciclo: this.state.elCiclo,
                periodo: this.state.elPeriodo,
                asistencia: a
            })
        }
        let formData = new FormData();
        formData.append(
            'new',
            JSON.stringify(objeto));
        await fetch(this.state.uri + '/api/pasar/lista/admin/' + this.state.elGrado + '/' + this.state.elGrupo, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            },
            body: formData
        }).then(res => res.json())
            .then(responseJson => {
                this.setState({elRequest: responseJson});
            });
    }

    render() {
        const alumnos = this.renderAlumnos();
        // const clase = this.renderClass();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <MultiBotonRow
                    itemBtns={[
                        'Pasar lista',
                        'Justificar falta',
                    ]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelectedTab(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />

                {this.state.indexSelectedTab === 0 ? (
                    <ScrollView
                        overScrollMode={'always'}
                        style={{height: responsiveHeight(71)}}>
                        <GradoyGrupo
                            onListItemPressedGrupos={(indexBtn, itemBtn) => this.onListItemPressedGrupos(indexBtn, itemBtn)}
                            onListItemPressedGrado={(indexBtn, itemBtn) => this.onListItemPressedGrado(indexBtn, itemBtn)}
                            todos={'0'}/>
                        <View style={[styles.row, styles.widthall, {justifyContent: 'flex-end', marginTop: 9}]}>
                            <Text style={{fontSize: responsiveFontSize(1.4)}}>Asistencia{'   '}</Text>
                        </View>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            style={[styles.tabla, {
                                borderColor: this.state.secondColor,
                                height: responsiveHeight(30),
                                width: responsiveWidth(94),
                                marginBottom: this.state.indexSelectedTab !== 0 ? 15 : null
                            }]}
                        >
                            {alumnos}
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                {
                                    backgroundColor: this.state.secondColor
                                }
                            ]}
                            onPress={() => this.requestMultipart()}
                        >
                            <Text style={styles.textButton}>Guardar asistencias</Text>
                        </TouchableOpacity>
                    </ScrollView>
                ) : <View><Text style={{marginTop: 15}}>Se está trabajando en esta funcionalidad</Text>
                    <Text style={{marginTop: 15}}>Gracias por su comprensión</Text></View>}
            </View>
        );
    }
}
