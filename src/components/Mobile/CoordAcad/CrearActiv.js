import React from 'react';
import {
	Alert, AsyncStorage, ScrollView, StatusBar, Text, TextInput, TouchableHighlight, TouchableOpacity,KeyboardAvoidingView, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import styles from '../../styles';
import GradoyGrupo from './../Globales/GradoyGrupo';
import Spinner from 'react-native-loading-spinner-overlay';
import ModalSelector from 'react-native-modal-selector';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class CrearActiv extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			roles: [{label: 'Padres', role: 'Padre'}, {label: 'Alumnos', role: 'Alumno'}, {
				label: 'Admón.', role: 'Admin'
			}, {label: 'Maestros', role: 'Maestro'}],
			data: [],
			datePickerF: false,
			datePickerI: false,
			timePickerI: false,
			timePickerF: false,
			horas: [],
			tipoEvento: '',
			fecha_inicio: '',
			fecha_fin: '',
			hora_inicio: '',
			hora_fin: '',
			tituloActiv: '',
			descripActiv: '',
			lugarActiv: '',
			checkBoxBtnsIndex: [],
			checkBoxBtns: [],
			indxRole: -1,
			elRole: '',
			indxGrado: -1,
			elGrado: '',
			indxGrupo: -1,
			elGrupo: '',
			indexLista: -1,
			laLista: '',
			tipoAviso: '',
			losRoles: '',
			aux: 0
		};
	}

	showDatePickerI = () => this.setState({datePickerI: true});

	hideDatePickerI = () => this.setState({datePickerI: false});

	_handleDatePickedI = date => {
		let fecha = moment(date).format('YYYY-MM-DD');
		this.setState({fecha_inicio: fecha});
		this.hideDatePickerI();
	};

	showDatePickerF = () => this.setState({datePickerF: true});

	hideDatePickerF = () => this.setState({datePickerF: false});

	_handleDatePickedF = date => {
		let fecha = moment(date).format('YYYY-MM-DD');
		this.setState({fecha_fin: fecha});
		this.hideDatePickerF();
	};

//+-+-+-+-+-+-+-+-+-+-+Tiempo
	showTimePickerI = () => this.setState({timePickerI: true});

	hideTimePickerI = () => this.setState({timePickerI: false});

	_handleTimePickedI = time => {
		let hora = moment(time, 'HH:mm').format('HH:mm');
		this.setState({hora_inicio: hora});
		this.hideTimePickerI();
	};

	showTimePickerF = () => this.setState({timePickerF: true});

	hideTimePickerF = () => this.setState({timePickerF: false});

	_handleTimePickedF = time => {
		let hora = moment(time, 'HH:mm').format('HH:mm');
		this.setState({hora_fin: hora});
		this.hideTimePickerF();
	};

	spinner = (state) => {
		this.setState({
			visible: state
		});
	};

	async componentWillMount() {
		await this.getURL();
		await this.getUserdata();
		this.state.roles.forEach(() => {
			this.state.checkBoxBtnsIndex.push(0);
		});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getUserdata() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		await fetch(uri + '/api/user/data/v2', {
			method: 'GET', headers: {
				Authorization: 'Bearer ' + token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 4)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({data: responseJson});
				}
			});
	}

	async alertActiv() {
		if (this.state.checkBoxBtns.length === 0) {
			Alert.alert('Sin destinatarios', 'Elija a los destinatarios del evento', [
				{text: 'Enterado'}
			])
		} else if (this.state.elGrado === '') {
			Alert.alert('Campo grado vacio', 'Seleccione un grado para poder continuar o bien puede elegir una lista personalizada', [
				{text: 'Enterado'}
			])
		} else if (this.state.elGrupo === '') {
			Alert.alert('Campo grupo vacio', 'Seleccione un grupo para poder continuar o bien puede elegir una lista personalizada', [
				{text: 'Enterado'}
			])
		} else if (this.state.tituloActiv === '') {
			Alert.alert('Nombre evento vacío', 'Escriba el nombre del evento para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.descripActiv === '') {
			Alert.alert('Descripción evento vacío', 'Escriba la descripción del evento para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.tipoEvento === '') {
			Alert.alert('Tipo evento indefinido', 'Seleccione el tipo de evento para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.lugarActiv === '') {
			Alert.alert('Lugar evento vacío', 'Escriba el lugar del evento para poder continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.fecha_inicio === '' || this.state.fecha_fin === '') {
			Alert.alert('Fechas indefinidas', 'Defina una fecha de inicio y una fecha de finalizado para continuar', [
				{text: 'Enterado'}
			])
		} else if (this.state.hora_inicio === '' || this.state.hora_fin === '') {
			Alert.alert('Horas indefinidas', 'Defina una hora de inicio y una hora de finalizado para continuar', [
				{text: 'Enterado'}
			])
		} else {
			await this.acomodoRoles();
			Alert.alert('Crear evento', 'Está a punto de crear un evento\n¿Seguro que desea continuar?', [
				{text: 'Sí', onPress: () => this.requestMultiparte()}, {text: 'No'}
			]);
		}
		await this.setState({aux: 0});
	}

	async requestMultiparte() {
		await this.spinner(true);
		await fetch(this.state.uri + '/api/create/new/activdad', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}, body: JSON.stringify({
				titulo: this.state.tituloActiv,
				descripcion: this.state.descripActiv,
				lugar: this.state.lugarActiv,
				fecha_inicio: this.state.fecha_inicio,
				fecha_fin: this.state.fecha_fin,
				hora_inicio: this.state.hora_inicio,
				hora_fin: this.state.hora_fin,
				destinatarios: this.state.losRoles.toString(),
				grado: this.state.elGrado,
				grupo: this.state.elGrupo,
				user_id: this.state.data.id,
				tipo_evento: this.state.tipoEvento,
				tipo_aviso: this.state.tipoAviso
			})
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al crear evento', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Crear evento)', [{
						text: 'Entendido', onPress: () => [this.spinner(false), Actions.refresh({key:Math.random()})]
					}]);
				} else {
					Alert.alert('¡Felicidades!', 'Se ha publicado el evento con éxito', [{
						text: 'Entendido', onPress: () => [this.spinner(false), Actions.refresh({key:Math.random()})]
					}]);
				}
			});
	}

	async acomodoRoles() {
		await this.setState({losRoles: ''});
		await this.state.checkBoxBtns.forEach((item) => {
			if (this.state.losRoles === '') {
				this.setState({losRoles: this.state.losRoles + item});
			} else {
				this.setState({losRoles: this.state.losRoles + ',' + item});
			}
		});
	}

	async onListPressedRole(itm, indx) {
		await this.setState({indxRole: indx, elRole: itm});
		let i = this.state.checkBoxBtns.indexOf(itm);
		if (i > -1) {
			this.state.checkBoxBtns.splice(i, 1);
			this.state.checkBoxBtnsIndex[indx] = 0;
		} else {
			this.state.checkBoxBtns.push(itm.toString());
			this.state.checkBoxBtnsIndex[indx] = 1;
		}
		if (this.state.checkBoxBtnsIndex[0] !== 1 && this.state.checkBoxBtnsIndex[1] !== 1) {
			this.setState({elGrado: 'Todos', elGrupo: 'Todos',  tipoAviso: '1'});
		} else {
			this.setState({elGrado: '', elGrupo: ''});
		}
		await this.setState({aux: 0});
	}

	renderRoles() {
		let rolesBtn = [];
		if (this.state.roles.length !== 0) {
			this.state.roles.forEach((itemRole, indexRole) => {
				let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn_2, {borderColor: this.state.secondColor}];
				let texto = [styles.textoN];
				if (this.state.checkBoxBtnsIndex[indexRole] === 1) {
					smallButtonStyles.push(styles.listButtonSelected, {
						backgroundColor: this.state.secondColor
					});
					texto.push(styles.textoB);
				}
				rolesBtn.push(
					<TouchableHighlight
						key={indexRole + 'roles'}
						style={[smallButtonStyles]}
						underlayColor={'transparent'}
						onPress={() => this.onListPressedRole(itemRole.role, indexRole)}
					>
						<Text style={texto}>{itemRole.label}</Text>
					</TouchableHighlight>
				);
			});
		}
		return rolesBtn;
	}

//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
	async onListItemPressedGrado(indx, itm) {
		await this.setState({indxGrado: indx, elGrado: itm, tipoAviso: '1'});
	}

	async onListItemPressedGrupos(indx, itm) {
		await this.setState({indxGrupo: indx, elGrupo: itm, tipoAviso: '1'});
		this.setState({elRole: ''});
	}

	async onListItemPressedLista(indexLista, taller, id) {
		await this.setState({indexLista: indx, elGrupo: taller, elGrado: id, tipoAviso: '2'});
	}

	async onChangeEvento(option) {
		await this.setState({tipoEvento: option.tipo})
	}

	render() {
		const losRoles = this.renderRoles();
		let index = 0;
		const data = [
			{key: index++, label: 'Convivencia', tipo: 'conviviencia'},
			{key: index++, label: 'Periodo de examenes', tipo: 'evaluacion'},
			{key: index++, label: 'Vacaciones', tipo: 'vacaciones'},
			{key: index++, label: 'Suspensión oficial', tipo: 'suspension'},
			{key: index++, label: 'Periodo de preinscripción', tipo: 'preinscripcion'},
			{key: index++, label: 'Inicio de curso', tipo: 'curso'},
			{key: index++, label: 'Fin de curso', tipo: 'curso'},
			{key: index++, label: 'Examen PLANEA', tipo: 'examen'},
			{key: index++, label: 'Actividades académicas', tipo: 'academicas'},
			{key: index++, label: 'Entrega de boletas', tipo: 'reporte'},
			{key: index++, label: 'Consejo Técnico Escolar', tipo: 'cte'}
		];
		return (
			<View style={styles.container}>
				<Spinner visible={this.state.visible} textContent='Cargando, Por favor espere...'/>
				<ScrollView
					showsVerticalScrollIndicator={false}
				>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Elija los Destinatarios
					</Text>
					<View style={[styles.widthall, {alignItems: 'center'}]}>
						<View style={[styles.buttonsRow, {marginTop: 3, marginBottom: 0}]}>{losRoles}</View>
					</View>
					{this.state.checkBoxBtnsIndex[0] === 1 || this.state.checkBoxBtnsIndex[1] === 1 ?
						<GradoyGrupo
							onListItemPressedGrupos={(indexGrupo, grupo) => this.onListItemPressedGrupos(indexGrupo, grupo)}
							onListItemPressedGrado={(indexGrado, grado) => this.onListItemPressedGrado(indexGrado, grado)}
							onListItemPressedLista={(indexLista, taller, id) => this.onListItemPressedLista(indexLista, taller, id)}
							listaVar={true}
							todos={'1'}
						/> : null}
					<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 9}]}>
						Nombre del evento
					</Text>
					<TextInput
						keyboardType='default'
						maxLength={30}
						placeholder={'Titulo de la actividad'}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						style={[
							styles.inputPicker,
							{borderColor: this.state.secondColor, width: responsiveWidth(93.5)}
						]}
						onChangeText={text => (this.state.tituloActiv = text)}
						defaultValue={this.state.tituloActiv}
					/>
					<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 8}]}>
						Descripción del evento
					</Text>
					<TextInput
						keyboardType='default'
						maxLength={256}
						multiline={true}
						placeholder={'Descripción del actividad'}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						style={[
							styles.inputComentarios,
							{
								borderColor: this.state.secondColor,
								width: responsiveWidth(93.5),
								height: responsiveHeight(15)
							}
						]}
						onChangeText={text => (this.state.descripActiv = text)}
						defaultValue={this.state.descripActiv}
					/>
					<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 8}]}>
						Lugar del evento
					</Text>
					<TextInput
						keyboardType='default'
						maxLength={30}
						placeholder={'Titulo de la actividad'}
						returnKeyType='next'
						underlineColorAndroid='transparent'
						style={[
							styles.inputPicker,
							{borderColor: this.state.secondColor, width:responsiveWidth(93.5)}
						]}
						onChangeText={text => (this.state.lugarActiv = text)}
						defaultValue={this.state.lugarActiv}
					/>
					<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 8}]}>
						Tipo de evento
					</Text>
					<ModalSelector
						data={data}
						selectStyle={[
							styles.inputPicker,
							{borderColor: this.state.secondColor}
						]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.7)}}
						selectTextStyle={{fontSize: responsiveFontSize(1.75)}}
						initValue='Seleccione tipo de evento'
						onChange={option => this.onChangeEvento(option)}
					/>
					{/*-+-+-+-+-+-+-+-+-+FECHAS+-+-+-+-+-+-+-+-+-+-+-+-*/}
					<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 8}]}>
						Fecha del evento
					</Text>
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'center',
						marginVertical: 5,
						padding: 3
					}]}>
						<View style={[styles.row, {width: responsiveWidth(38)}]}>
							<Text styles={{textAlign: 'auto'}}>Del</Text>
							<TouchableOpacity
								style={[styles.modalBtnMed, styles.btn3, {
									borderColor: this.state.secondColor,
									marginTop: 3,
									justifyContent: 'center',
									height: responsiveHeight(4)
								}]}
								onPress={this.showDatePickerI}
							>
								{this.state.fecha_inicio === '' ? (
									<Text style={[{textAlign: 'center', fontSize: responsiveFontSize(1.5)}]}>
										Fecha inicial
									</Text>
								) : (
									<Text style={[{
										textAlign: 'center',
										fontSize: responsiveFontSize(1.5)
									}]}>{moment(this.state.fecha_inicio, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Text>
								)}
							</TouchableOpacity>
						</View>
						<View style={[styles.row, {width: responsiveWidth(36.5), marginLeft: 14}]}>
							<Text styles={{textAlign: 'center'}}>al</Text>
							<TouchableOpacity
								style={[styles.modalBtnMed, styles.btn3, {
									borderColor: this.state.secondColor,
									marginTop: 3,
									justifyContent: 'center',
									height: responsiveHeight(4)
								}]}
								onPress={this.showDatePickerF}
							>
								{this.state.fecha_fin === '' ? (
									<Text style={[{textAlign: 'center', fontSize: responsiveFontSize(1.5)}]}>
										Fecha final
									</Text>
								) : (
									<Text style={[{
										textAlign: 'center',
										fontSize: responsiveFontSize(1.5)
									}]}>{moment(this.state.fecha_fin, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Text>
								)}
							</TouchableOpacity>
						</View>
						<DateTimePicker
							isVisible={this.state.datePickerI}
							onConfirm={this._handleDatePickedI}
							onCancel={this.hideDatePickerI}
							titleIOS={'Seleccione una fecha'}
							confirmTextIOS={'Seleccionar'}
							cancelTextIOS={'Cancelar'}
						/>
						<DateTimePicker
							isVisible={this.state.datePickerF}
							onConfirm={this._handleDatePickedF}
							onCancel={this.hideDatePickerF}
							titleIOS={'Seleccione una fecha'}
							confirmTextIOS={'Seleccionar'}
							cancelTextIOS={'Cancelar'}
						/>
					</View>
					{/*-+-+-+-+-+-+-+-+-+-+-+-HORAS-+-+-+-+-+-+-+-+-+-+-+-*/}
					<Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 5}]}>
						Tiempo de duración
					</Text>
					<View style={[styles.row, styles.widthall, {
						justifyContent: 'center',
						marginTop: 10,
						marginBottom: 15,
						padding: 3
					}]}>
						<View style={[styles.row, {width: responsiveWidth(38)}]}>
							<Text styles={{textAlign: 'auto'}}>De</Text>
							<TouchableOpacity
								style={[styles.modalBtnMed, styles.btn3, {
									borderColor: this.state.secondColor,
									marginTop: 3,
									justifyContent: 'center',
									height: responsiveHeight(4)
								}]}
								onPress={this.showTimePickerI}
							>
								{this.state.hora_inicio === '' ? (
									<Text style={[{textAlign: 'center', fontSize: responsiveFontSize(1.5)}]}>
										Hora inicial
									</Text>
								) : (
									<Text style={[{
										textAlign: 'center',
										fontSize: responsiveFontSize(1.5)
									}]}>{moment(this.state.hora_inicio, 'HH:mm').format('HH:mm')}</Text>
								)}
							</TouchableOpacity>
						</View>
						<View style={[styles.row, {width: responsiveWidth(36.5), marginLeft: 14}]}>
							<Text styles={{textAlign: 'center'}}>al</Text>
							<TouchableOpacity
								style={[styles.modalBtnMed, styles.btn3, {
									borderColor: this.state.secondColor,
									marginTop: 3,
									justifyContent: 'center',
									height: responsiveHeight(4)
								}]}
								onPress={this.showTimePickerF}
							>
								<Text style={[{textAlign: 'center', fontSize: responsiveFontSize(1.5)}]}>
									{this.state.hora_fin === '' ? 'Hora final' : moment(this.state.hora_fin, 'HH:mm').format('HH:mm')}
								</Text>
							</TouchableOpacity>
						</View>
						<DateTimePicker
							isVisible={this.state.timePickerI}
							onConfirm={this._handleTimePickedI}
							onCancel={this.hideTimePickerI}
							mode={'time'}
							is24Hour={true}
							titleIOS={'Seleccione una fecha'}
							confirmTextIOS={'Seleccionar'}
							cancelTextIOS={'Cancelar'}
						/>
						<DateTimePicker
							isVisible={this.state.timePickerF}
							onConfirm={this._handleTimePickedF}
							onCancel={this.hideTimePickerF}
							mode={'time'}
							is24Hour={true}
							titleIOS={'Seleccione una fecha'}
							confirmTextIOS={'Seleccionar'}
							cancelTextIOS={'Cancelar'}
						/>
					</View>
					<TouchableOpacity
						style={[
							styles.bigButton,
							{backgroundColor: this.state.secondColor}
						]}
						onPress={() => this.alertActiv()}
					>
						<Text style={styles.textButton}>Programar evento</Text>
					</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}
