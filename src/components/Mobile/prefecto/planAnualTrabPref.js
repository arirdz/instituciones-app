import React from "react";
import {
    View,
    AsyncStorage,
    StatusBar,
} from "react-native";
import {
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../../styles";
import MultiBotonRow from "../Globales/MultiBotonRow";
import CalendarPref from "./CalendarPref";
import CrearActivPref from "./CrearActivPref";

export default class planAnualTrabPref extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: "",
            solicitudes: [],
            indexSelected: 0
        };
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    renderView() {
        return (
            <View
                style={{
                    width: responsiveWidth(94),
                    height: responsiveHeight(80)
                }}>
                {this.state.indexSelected === 0 ? <CalendarPref/> : null}
                {this.state.indexSelected === 1 ? <CrearActivPref/> : null}
            </View>
        );
    }

    render() {
        const laview = this.renderView();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <MultiBotonRow
                    itemBtns={["Calendario", "Crear actividad"]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />
                {laview}
            </View>
        );
    }
}
