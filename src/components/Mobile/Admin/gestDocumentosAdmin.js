import React from "react";
import {
    Text,
    View,
    Alert,
    AsyncStorage,
    ScrollView,
    TouchableOpacity,
    WebView,
    TextInput
} from "react-native";
import {
    responsiveWidth,
    responsiveHeight
} from "react-native-responsive-dimensions";
import MultiBotonRow from "../Globales/MultiBotonRow";
import styles from "../../styles";

export default class gestDocumentosAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            botonSelected: "Ver documentos",
            document: null
        };
    }

    componentWillMount() {
        this.getURL();
    }

    publication() {
        Alert.alert("¡Atencion!", "Has publicado un nuevo documento", [
            {
                text: "Entendido"
            }
        ]);
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            url: uri,
            token: token
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    verDocument() {
        return (
            <View style={styles.container}>
                <Text style={styles.main_title}>
                    Seleccione el documento a consultar
                </Text>
                <View style={[styles.cargos, {marginRight: 15}]}>
                    <Text style={[styles.subTitleMain_PP, {marginRight: 185}]}>
                        Documentos
                    </Text>
                    <Text style={[styles.subTitleMain_PP, {marginleft: 98}]}>
                        Estatus
                    </Text>
                </View>
                <ScrollView>
                    <View style={styles.ButtonsRow}>
                        <View style={{marginRight: 5}}>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}>
                                <View>
                                    <Text numberOfLines={1}>Manual de Convivencia Escolar</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}>
                                <View>
                                    <Text numberOfLines={1}>Directivas de Responsabilidad</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}>
                                <View>
                                    <Text numberOfLines={1}>Manual de Seguridad Escolar</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}>
                                <View>
                                    <Text numberOfLines={1}>Ley General de Educación</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}>
                                <View>
                                    <Text numberOfLines={1}>
                                        Lineamiento Para Asignación de Becas
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnlistadoc,
                                    {
                                        borderColor: this.state.fourthColor,
                                        paddingHorizontal: 15
                                    }
                                ]}>
                                <View>
                                    <Text numberOfLines={1}>Política de Privacidad</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.mainColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}>
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.secondColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}>
                                <View>
                                    <Text style={styles.textoB}>Pendiente</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.mainColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}>
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        backgroundColor: this.state.secondColor,
                                        borderColor: this.state.fourthColor
                                    }
                                ]}>
                                <View>
                                    <Text style={styles.textoB}>Pendiente</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        borderColor: this.state.fourthColor,
                                        backgroundColor: this.state.mainColor
                                    }
                                ]}>
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[
                                    styles.btnstatus,
                                    {
                                        borderColor: this.state.fourthColor,
                                        backgroundColor: this.state.mainColor
                                    }
                                ]}>
                                <View>
                                    <Text style={styles.textoB}>Aceptado</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

    newDocument() {
        return (
            <View style={styles.container}>
                <Text style={styles.main_title}>Subir nuevo documento</Text>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: "white",
                            marginTop: 1
                        }
                    ]}
                    onPress={this._pickDocument}>
                    <Text style={[styles.textButton, {color: this.state.secondColor}]}>
                        Seleccione un documento
                    </Text>
                </TouchableOpacity>
                <Text style={styles.main_title}>Escribe el título del documento</Text>
                <TextInput
                    placeholder={"Este es el título que verán los destinatarios"}
                    underlineColorAndroid="transparent"
                    style={{
                        height: responsiveHeight(7),
                        width: responsiveWidth(94),
                        borderWidth: 1,
                        borderBottomWidth: 0,
                        borderRadius: 6,
                        borderColor: this.state.fourthColor,
                        paddingHorizontal: 15
                    }}
                    onChangeText={text => this.setState({text})}
                    value={this.state.text}
                />
                <Text style={styles.main_title}>Vista preliminar</Text>
                <View
                    style={[
                        styles.containerCord,
                        {
                            borderWidth: 1,
                            borderColor: this.state.fourthColor
                        }
                    ]}>
                    <WebView source={{uri: document}}/>
                </View>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}
                    onPress={this.publication}>
                    <Text style={styles.textButton}>Publicar</Text>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        return (
            <View style={[styles.container, {marginTop: 10}]}>
                <MultiBotonRow
                    itemBtns={["Ver documentos", "Nuevo documento"]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={2}
                />
                {this.state.botonSelected === "Ver documentos"
                    ? this.verDocument()
                    : null}

                {this.state.botonSelected === "Nuevo documento"
                    ? this.newDocument()
                    : null}
            </View>
        );
    }
}
