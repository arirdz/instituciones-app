import React from 'react';
import {
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import styles from '../../styles';
import GradoyGrupo from '../Globales/GradoyGrupo';

export default class gestReportes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alumnos: [],
            ciclos: [],
            detalles: [],
            elAlumno: '',
            elCiclo: '',
            elDetalle: '',
            elHijo: '',
            elPeriodo: '',
            incidenciasCount: [],
            itemBtns: ['FALTAS', 'INCIDENCIAS', 'FECHA'],
            ninosPicker: [],
            opcion: 0,
            periodos: [],
            selectedIndexAlumno: -1,
            selectedIndexBtn: 0,
            selectedIndexCiclo: 0,
            selectedIndexGrados: -1,
            elGrado: '',
            elGrupo: '',
            selectedIndexGrupos: -1,
            selectedIndexPeriodo: 0
        };
    }

    static isIncidencia(itemDetalle) {
        return (
            <View style={styles.campoTablaG}>
                <Text>{itemDetalle.extras}</Text>
            </View>
        );
    }

    static isFaltas(itemDetalle) {
        return (
            <View style={styles.campoTablaG}>
                <Text>{itemDetalle.tipos_faltas}</Text>
            </View>
        );
    }

    static isFecha(itemDetalle) {
        return (
            <View style={styles.campoTablaG}>
                <Text>{itemDetalle.created_at}</Text>
            </View>
        );
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getCiclos();
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            elGrupo: grupo
        });
        await this.getAlumnos();
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            elGrado: grado
        });
    }

    async componentWillMount() {
        await this.getURL();
    }

    onListItemPressedCiclo(index, ciclo) {
        this.setState({
            selectedIndexCiclo: index,
            elCiclo: ciclo,
            selectedIndexBtn: 0,
            selectedIndexPeriodo: -1,
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selectedIndexAlumno: -1
        });
    }

    // ++++++++++++++++++++++++++++++PERIODO++++++++++++++++++++++++++++++++++++++

    async getCiclos() {
        let ciclopicker = await fetch(this.state.uri + '/api/ciclos', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let ciclospicker = await ciclopicker.json();
        await this.setState({ciclos: ciclospicker});
        await this.setState({elCiclo: this.state.ciclos[0]});
        await this.getPeriodos();
    }

    renderCiclo(itemCiclo, indexCiclo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexCiclo === indexCiclo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexCiclo}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() => this.onListItemPressedCiclo(indexCiclo, itemCiclo)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemCiclo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderCiclos() {
        let buttonsCiclos = [];
        this.state.ciclos.forEach((itemCiclo, indexCiclo) => {
            buttonsCiclos.push(this.renderCiclo(itemCiclo, indexCiclo));
        });
        return buttonsCiclos;
    }

    async onListItemPressedPeriodo(index, ciclo) {
        this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo,
            selectedIndexBtn: 0,
            selectedIndexGrados: -1,
            selectedIndexGrupos: -1,
            selectedIndexAlumno: -1
        });
    }

    // +++++++++++++++++++++++++++++++ALUMNOS+++++++++++++++++++++++++++++++++++
    async getPeriodos() {
        let periodopicker = await fetch(
            this.state.uri + '/api/periodos/' + this.state.elCiclo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        periodospicker = await periodopicker.json();
        this.setState({periodos: periodospicker, elPeriodo: periodospicker[0]});
    }

    renderPeriodo(itemPeriodo, indexPeriodo) {
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn5,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexPeriodo === indexPeriodo) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={indexPeriodo}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() =>
                    this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                }>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemPeriodo}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderPeriodos() {
        let buttonsPeriodos = [];
        this.state.periodos.forEach((itemPeriodo, indexPeriodo) => {
            buttonsPeriodos.push(this.renderPeriodo(itemPeriodo, indexPeriodo));
        });
        return buttonsPeriodos;
    }

    async getAlumnos() {
        let alumnoList = await fetch(
            this.state.uri +
            '/api/user/alumnos/' +
            this.state.grado +
            '/' +
            this.state.grupo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnosList = await alumnoList.json();
        this.setState({alumnos: alumnosList});
    }

    // +++++++++++++++++++++++++++++++++INCIDENCIAS+++++++++++++++++++++++++++++++

    onListPressedAlumno(indexAlumno, alumno) {
        this.setState({selectedIndexAlumno: indexAlumno, elAlumno: alumno});
        this.getIncidencias();
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let smallButtonStyles = [
            styles.listButtonNoticia,
            styles.listButtonSmallName,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];
        if (this.state.selectedIndexAlumno === indexAlumno) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() => this.onListPressedAlumno(indexAlumno, itemAlumno.id)}>
                <View style={styles.listItem}>
                    <Text style={texto}>{itemAlumno.name}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    async getIncidencias() {
        let incidenciaList = await fetch(
            this.state.uri +
            '/api/incidencias/count/' +
            this.state.elAlumno +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let incidenciasList = await incidenciaList.json();
        this.setState({incidenciasCount: incidenciasList});
    }

    renderIncidencias() {
        let academicas = this.state.incidenciasCount.IncidenciasAC;
        let conductuales = this.state.incidenciasCount.IncidenciasCC;
        return (
            <View style={styles.row}>
                <View style={styles.cuadro}>
                    <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                        INCIDENCIAS{'\n'}ACADÉMICAS
                    </Text>
                    <View
                        style={[
                            styles.buttonEnterados,
                            {backgroundColor: this.state.fourthColor},
                            styles.cuadro1
                        ]}>
                        <Text style={styles.text_VE}>{academicas}</Text>
                    </View>
                    {this.state.incidenciasCount.IncidenciasAC === 0 ? (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles1()}>
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles('Académicas')}>
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
                <View style={styles.cuadro}>
                    <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                        INCIDENCIAS{'\n'}CONDUCTUALES
                    </Text>
                    <View
                        style={[
                            styles.buttonEnterados,
                            {backgroundColor: this.state.mainColor},
                            styles.cuadro1
                        ]}>
                        <Text style={styles.text_VE}>{conductuales}</Text>
                    </View>
                    {this.state.incidenciasCount.IncidenciasCC === 0 ? (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles1()}>
                            <Text
                                style={([styles.titBtn], {color: this.state.secondColor})}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                            style={styles.buttonDetalle}
                            onPress={() => this.getDetalles('Conductual')}>
                            <Text style={[styles.titBtn, {color: this.state.secondColor}]}>
                                VER DETALLE
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }

    // ++++++++++++++++++++++++++MOSTRAR DETALLES+++++++++++++++++++++++++++
    async getDetalles(tipo) {
        let detalleList = await fetch(
            this.state.uri +
            '/api/detalles/incidencias/' +
            this.state.elAlumno +
            '/' +
            this.state.elCiclo +
            '/' +
            this.state.elPeriodo +
            '/' +
            tipo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let detallesList = await detalleList.json();
        this.setState({detalles: detallesList.reportes});
    }

    async getDetalles1() {
        let detalleList = await fetch(this.state.uri + '/api/detalles/vacio', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let detallesList = await detalleList.json();
        this.setState({detalles: detallesList});
    }

    renderDetalle(itemDetalle, indexDetalle) {
        return (
            <View
                key={indexDetalle}
                underlayColor={this.state.secondColor}
                style={styles.rowTabla}>
                <View style={styles.campoTabla}>
                    <Text>{itemDetalle.materia.nombre}</Text>
                </View>
                {this.state.opcion === 0
                    ? gestReportes.isIncidencia(itemDetalle)
                    : null}
                {this.state.opcion === 1 ? gestReportes.isFaltas(itemDetalle) : null}
                {this.state.opcion === 2 ? gestReportes.isFecha(itemDetalle) : null}
            </View>
        );
    }

    renderDetalles() {
        let buttonsDetalles = [];
        this.state.detalles.forEach((itemDetalle, indexDetalle) => {
            buttonsDetalles.push(this.renderDetalle(itemDetalle, indexDetalle));
        });
        return buttonsDetalles;
    }

    onListItemPressedBtn(indexBtn) {
        this.setState({selectedIndexBtn: indexBtn, opcion: indexBtn});
    }

    renderBtn(itemBtn, indexBtn) {
        let campo = [styles.campoTabla1];
        let texto = [styles.textoN];

        if (this.state.selectedIndexBtn === indexBtn) {
            campo.push(styles.titulosC, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableOpacity
                style={campo}
                key={indexBtn}
                onPress={() => this.onListItemPressedBtn(indexBtn, itemBtn)}>
                <Text style={[styles.titBtn, texto]}>{itemBtn}</Text>
            </TouchableOpacity>
        );
    }

    renderHoras() {
        let buttonsBtns = [];
        this.state.itemBtns.forEach((itemBtn, indexBtn) => {
            buttonsBtns.push(this.renderBtn(itemBtn, indexBtn));
        });
        return buttonsBtns;
    }

    render() {
        const alumnos = this.renderAlumnos();
        const btn = this.renderHoras();
        const ciclos = this.renderCiclos();
        const detalles = this.renderDetalles();
        const incidencias = this.renderIncidencias();
        const periodos = this.renderPeriodos();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el ciclo escolar
                    </Text>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        {ciclos}
                    </ScrollView>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el período
                    </Text>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        {periodos}
                    </ScrollView>
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el alumno(s)
                    </Text>
                    <View style={styles.contAlumnos}>
                        <ScrollView
                            style={[
                                styles.titulosContainer,
                                {borderColor: this.state.secondColor}
                            ]}
                            horizontal={false}
                            showsVerticalScrollIndicator={false}>
                            {alumnos}
                        </ScrollView>
                    </View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Análisis de incidencias
                    </Text>
                    {incidencias}
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Detalle de incidencias
                    </Text>
                    <View style={[styles.rowTabla, styles, styles.topTabla]}>
                        <View style={styles.campoTabla}>
                            <Text>MATERIA</Text>
                        </View>
                        {btn}
                    </View>
                    {detalles}
                </ScrollView>
            </View>
        );
    }
}
