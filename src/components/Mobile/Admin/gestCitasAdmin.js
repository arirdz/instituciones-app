import React from 'react';
import {AsyncStorage, StatusBar, View} from 'react-native';
import styles from '../../styles';
import GestionarCitas from './GestionarCitas';
import MultiBotonRow from '../Globales/MultiBotonRow';
import AgendarCitasAdmin from './AgendarCitaAdmin';
import EditarDisp from '../Globales/EditarDisp';

export default class gestCitasAdmin extends React.Component {
   constructor(props) {
	  super(props);
	  this.state = {
		 data: [], items: {}, botonSelected: 'Atender citas', laSolicitud: '', selected: 'Recientes', solicitudes: []
	  };
   }

   async getURL() {
	  let uri = await AsyncStorage.getItem('uri');
	  let token = await AsyncStorage.getItem('token');
	  let maincolor = await AsyncStorage.getItem('mainColor');
	  let secondColor = await AsyncStorage.getItem('secondColor');
	  let thirdColor = await AsyncStorage.getItem('thirdColor');
	  let fourthColor = await AsyncStorage.getItem('fourthColor');
	  this.setState({
		 uri: uri,
		 token: token,
		 mainColor: maincolor,
		 secondColor: secondColor,
		 thirdColor: thirdColor,
		 fourthColor: fourthColor
	  });
   }

   async componentWillMount() {
	  await this.getURL();
   }

   async botonSelected(indexSelected, itemSelected) {
	  await this.setState({
		 botonSelected: itemSelected, indexSelected: indexSelected
	  });
   }

   render() {
	  return (<View style={styles.container}>
		 <StatusBar
		   backgroundColor={this.state.mainColor}
		   barStyle='light-content'
		 />
		 <MultiBotonRow
		   itemBtns={['Atender citas', 'Solicitar cita', 'Editar disponibilidad']}
		   onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
		   cantidad={3}
		 />
		 {this.state.botonSelected === 'Atender citas' ? (<GestionarCitas/>) : null}
		 {this.state.botonSelected === 'Solicitar cita' ? (<AgendarCitasAdmin/>) : null}
		 {this.state.botonSelected === 'Editar disponibilidad' ? (<EditarDisp/>) : null}
	  </View>);
   }
}
