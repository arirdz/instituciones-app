import React from 'react';
import {
	AsyncStorage,
	ScrollView,
	StatusBar,
	Text,
	Alert,
	TouchableHighlight,
	TouchableOpacity,
	View, TextInput, Keyboard
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../../styles';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class HistorialMenu extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			elMenu: [],
			productos: [],
			isModalMenu: []
		};
	}


	async componentWillMount() {
		await this.getURL();
		await this.getMenuEscolarAdmin();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		await this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+Actualizar
	async alertUpdate(i) {
		Alert.alert('Publicar menú', 'Está a punto de publicar este menú\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.requestMultipartG(i)},
			{text: 'No'}
		]);
	}

	async requestMultipartG(i) {
		let formData = new FormData();
		formData.append('update', JSON.stringify({
			id_menu:this.state.elMenu[i].id_menu,
			publicado: '1'
		}));
		await fetch(this.state.uri + '/api/update/menu/escolar', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al publicar menu',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Menú escolar)',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		} else {
			this.notificacionPadres();
		}
	}

	async notificacionPadres() {
		await fetch(this.state.uri + '/api/enviar/notificacion/personalizada/Se ha publicado un nuevo menu escolar. Entre para ver detalles', {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al publicar menu',
						'Ha ocurrido un error '
						+
						responseJson.error.status_code
						+
						' si el error continua pónganse en contacto con soporte'
						+
						'(Menú escolar)',
						[{
							text: 'Entendido',
							onPress: () => Actions.drawer()
						}]);
				} else {
					Alert.alert('Menú escolar publicado', 'Se le notificara a los padres de familia sobre la publicacion del nuevo menú', [
						{text: 'Entendido', onPress: () => this.getMenuEscolarAdmin()}
					])
				}
			});
	}

	//+-+-+-+-+-+-+-+-+- Borrar menu
	async alertDelete(i) {
		Alert.alert('Borrar menú', 'Está a punto de borrar este menú\n¿Seguro que desea continuar?', [
			{text: 'Sí', onPress: () => this.borrarMenu(i)},
			{text: 'No'}
		]);
	}

	async borrarMenu(i) {
		let formData = new FormData();
		formData.append('new', JSON.stringify({
			id_menu: this.state.elMenu[i].id_menu
		}));
		await fetch(this.state.uri + '/api/delete/menu/escolar', {
			method: 'POST', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}, body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al borrar menu',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Menú escolar)',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('Menú escolar borra', 'Se ha borrado el menú escolar con éxito', [
				{text: 'Entendido', onPress: () => this.getMenuEscolarAdmin()}
			])
		}
	}

	//+-+-+-+-+-+-+-+Get menu cards
	async getMenuEscolarAdmin() {
		await fetch(this.state.uri + '/api/get/menu/escolar/admin', {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data'
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar las horas', 'Ha ocurrido un error ' +
						responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Menú escolar)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({elMenu: responseJson});
				}
			});
	}

	async alertEditar(i) {
		Alert.alert('¡Atención!', '¿Seguro que quieres editar este menu?', [
			{text: 'Sí', onPress: () => Actions.editarMenuEsc({editar: this.state.elMenu[i]})}, {text: 'No'}
		])
	}

	async openModal(i) {
		this.state.isModalMenu[i] = true;
		await this.setState({aux: 0});
	}

	async closeModal(i) {
		this.state.isModalMenu[i] = false;
		await this.setState({aux: 0});
	}

	rndMenus() {
		let btnMenu = [];
		this.state.elMenu.forEach((itm, i) => {
			btnMenu.push(
				<View
					key={i + 'cantGrupo'}
					style={[
						styles.widthall,
						styles.row,
						styles.cardEnc,
						{
							height: responsiveHeight(5.5),
							backgroundColor: this.state.fourthColor,
							marginVertical: 4,
							paddingHorizontal: 6
						}
					]}
				>
					<Modal
						isVisible={this.state.isModalMenu[i]}
						backdropOpacity={0.8}
						animationIn={'bounceIn'}
						animationOut={'bounceOut'}
						animationInTiming={1000}
						animationOutTiming={1000}
					>
						<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
							<Text
								style={[styles.main_title, styles.modalWidth, {
									color: this.state.thirdColor,
									textAlign: 'center'
								}]}
							>
								Menu de la semana
							</Text>
							<Text style={{marginTop: 3, fontSize: responsiveFontSize(1.6)}}>{itm.duracion}</Text>
							{itm.publicado === '1' ? (
								<Text style={[styles.textW, {color: 'green', marginTop: 5, marginBottom: 10}]}>
									PUBLICADO
								</Text>
							) : (
								<Text style={[styles.textW, {color: 'red', marginTop: 5, marginBottom: 10}]}>
									NO PUBLICADO
								</Text>
							)}
							<View style={{height: responsiveHeight(60)}}>
								<ScrollView showsVerticalScrollIndicator={false}
											style={[styles.modalWidth, {marginBottom: 10}]}>
									{this.rndDias(i)}
									<Text style={{marginTop: 15}}>Comentarios adicionales:</Text>
									<Text style={[
										styles.inputPicker,
										styles.modalWidth,
										{
											marginTop: 3,
											padding: 8,
											height: responsiveHeight(13),
											borderColor: this.state.secondColor,
											backgroundColor: '#f1f1f1',
											borderBottomWidth: .5,
											fontStyle: 'italic'
										}
									]}>
										{itm.comentario}
									</Text>
								</ScrollView>
							</View>

							{itm.publicado === '1' ? (
								<TouchableOpacity
									style={[styles.modalBigBtn, {
										borderColor: this.state.secondColor,
										backgroundColor: '#fff',
										marginVertical: 20
									}]}
									onPress={() => this.closeModal(i)}
								>
									<Text style={[styles.textButton, {color: this.state.secondColor}]}>Cerrar</Text>
								</TouchableOpacity>
							) : (
								<View style={[styles.row, styles.modalWidth, {marginVertical: 20}]}>
									<TouchableOpacity
										style={[styles.modalBtnMed, {
											borderColor: this.state.secondColor,
											backgroundColor: this.state.secondColor
										}]}
										onPress={() => this.alertUpdate(i)}
									>
										<Text style={styles.textButton}>Publicar ahora</Text>
									</TouchableOpacity>
									<TouchableOpacity
										style={[styles.modalBtnMed, {
											borderColor: this.state.secondColor,
											backgroundColor: '#fff'
										}]}
										onPress={() => this.closeModal(i)}
									>
										<Text
											style={[styles.textButton, {color: this.state.secondColor}]}>Cerrar</Text>
									</TouchableOpacity>
								</View>
							)}


						</View>
					</Modal>
					<View style={{marginLeft: 10}}>
						<Entypo name='circle-with-minus' size={22} color='red' onPress={() => this.alertDelete(i)}/>
					</View>
					<View style={{width: responsiveWidth(47), alignItems: 'flex-start'}}>
						<Text style={[styles.textW]}>{itm.titulo}</Text>
						<Text style={{fontSize: responsiveFontSize(1.3)}}>{itm.duracion}</Text>
					</View>

					<View style={[styles.row, {width: responsiveWidth(27)}]}>
						{itm.publicado !== '1' ? (
							<MaterialIcons name='edit' size={22} color='grey' onPress={() => this.alertEditar(i)}/>
						) : (<Text>{'      '}</Text>)}
						{itm.publicado === '1' ? (<Ionicons name='md-checkmark-circle' size={22} color='green'/>) :
							(<Entypo name='circle-with-cross' size={21} color='red'/>)}
						<View style={{marginRight: 4}}>
							<Entypo name='info-with-circle' size={19} color='black' onPress={() => this.openModal(i)}/>
						</View>
					</View>
				</View>
			)
		});
		return btnMenu;
	}

//+-+-+-+-+-+-+-+-+ get Dias card
	rndDias(i) {
		let btnDiaMenu = [];
		this.state.elMenu[i].producto.forEach((it, j) => {
			btnDiaMenu.push(
				<View
					key={j + 'dia'}
					style={[styles.modalWidth, styles.cardMat, {
						backgroundColor: '#f1f1f1',
						padding: 6,
						alignItems: 'center'
					}]}
				>
					<View style={[styles.modalWidth, styles.row, {
						alignItems: 'flex-start',
						paddingHorizontal: 8,
						marginBottom: 8,
						marginTop: 2
					}]}>
						<View
							style={[
								styles.inputPicker,
								styles.btn2,
								{
									borderRadius: 7,
									borderWidth: .5,
									paddingVertical: 4,
									alignItems: 'flex-start',
									backgroundColor: '#f7f7f7',
									marginTop: 0
								}
							]}
						>
							<Text style={styles.textW}>{it.dia}</Text>
						</View>
					</View>
					{this.rndProductos(i, j)}
				</View>
			)
		});
		return btnDiaMenu;
	}

	//+-+-+-+-+--+-+-+-+-+-+-+get productosCard

	rndProductos(i, j) {
		let btnProduct = [];
		this.state.elMenu[i].producto[j].producto.forEach((it, h) => {
			btnProduct.push(
				<View
					key={h + 'producto'}
					style={[styles.row, styles.btn10, {alignItems: 'center', marginVertical: 3}]}
				>
					<View
						style={[styles.inputPicker, styles.row, {
							width: responsiveWidth(60),
							padding: 4,
							marginTop: 0,
							backgroundColor: '#f7f7f7'
						}]}
					>
						<Text style={[{fontSize: responsiveFontSize(1.5)}]}>
							{it.nombre_producto}
						</Text>
					</View>
					<View
						style={[styles.inputPicker, styles.btn5,
							{padding: 4, marginTop: 0, backgroundColor: '#f7f7f7'}
						]}
					>
						<Text style={[{fontSize: responsiveFontSize(1.5)}]}>
							${it.precio}
						</Text>
					</View>
				</View>
			)
		});
		return btnProduct;
	}

	render() {
		return (
			<View style={styles.container}>
				<Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 5}]}>
					Historial de menus
				</Text>
				<View style={[styles.widthall, styles.row, {paddingHorizontal: 5}]}>
					<Text
						style={[styles.textW, styles.btn6_ls, {fontSize: responsiveFontSize(1.2), marginLeft: 5}]}
					>Borrar</Text>
					<Text style={[styles.textW, {
						fontSize: responsiveFontSize(1.2),
						width: responsiveWidth(47),
						textAlign: 'left'
					}]}>Titulo</Text>

					<View style={[styles.row, {width: responsiveWidth(28)}]}>
						<Text style={[styles.textW, {
							fontSize: responsiveFontSize(1.2)
						}]}>Editar</Text>
						<Text style={[styles.textW, {fontSize: responsiveFontSize(1.2)}]}>Estatus</Text>
						<Text style={[styles.textW, {fontSize: responsiveFontSize(1.2)}]}>Info.</Text>
					</View>
				</View>
				{this.rndMenus()}
			</View>
		);
	}
}
