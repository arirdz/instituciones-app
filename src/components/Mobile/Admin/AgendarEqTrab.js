import React from 'react';
import {
	Alert, AsyncStorage, KeyboardAvoidingView, Platform, ScrollView, Text, TextInput, TouchableHighlight,
	TouchableOpacity, View
} from 'react-native';
import {responsiveHeight, responsiveFontSize, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../styles';
import Modal from 'react-native-modal';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {ifIphoneX} from 'react-native-iphone-x-helper/index';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class AgendarEqTrab extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			asuntos: [],
			data: [],
			elAsunto: '',
			elIdMaestro: '',
			isModalCateg: false,
			elMaestro: [],
			isDateTimePickerVisible: false,
			elReceptor: '',
			receptor: '',
			fechaF1: [],
			fechagg: '',
			selectedIndexGrados: -1,
			elGrado: '',
			grados: [],
			laHora: '',
			horas: [],
			tipos: [],
			maestros: [],
			elMAestro: '',
			selectedIndexMaestro: -1,
			checkBoxBtns: [],
			seleccionados: [],
			checkBoxBtnsIndex: [],
			indexSeleccionados: [],
			losMaestros: [],
			maestrosName: [],
			elLugar: [],
			elRequest: [],
			losPuestos: [],
			losRoles: [
				{'role': 'Administración', 'key': 'Admin'},
				{'role': 'Maestros', 'key': 'Maestro'},
				{'role': 'Prefecto', 'key': 'Prefecto'},
				{'role': 'Médico', 'key': 'Medico'}
			]
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getHorasAt();
	}

	_showDateTimePicker() {
		this.setState({isDateTimePickerVisible: true});
	}

	_hideDateTimePicker() {
		this.setState({isDateTimePickerVisible: false});
	}

	_handleDatePicked(date) {
		let fecha = moment(date).format('YYYY-MM-DD');
		this.setState({fechagg: fecha});
		this._hideDateTimePicker();
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let role = await AsyncStorage.getItem('role');
		let puesto = await AsyncStorage.getItem('puesto');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			puesto: puesto,
			role: role,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
	}

	async getHorasAt() {
		let switchHora = await fetch(this.state.uri + '/api/get/horarios', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer' + this.state.token
			}
		});
		let switchHoras = await switchHora.json();
		await this.setState({horas: switchHoras});
	}

	// ++++++++++++++++++++++++++++++++++++++GUARDAR++++++++++++++++++++++++++++++

	async requestMultipart() {
		if (this.state.checkBoxBtns.length === 0 || this.state.fechagg === '' || this.state.elAsunto === '' || this.state.laHora === '') {
			Alert.alert('Faltan datos', 'Asegurese de llenar todos los campos para enviar la cita', [{text: 'Entendido'}]
			);
		} else {
			for (let i = 0; i < this.state.checkBoxBtns.length; i++) {
				await fetch(this.state.uri + '/api/citas/personal/web', {
					method: 'POST', headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}, body: JSON.stringify({
						id_receptor: this.state.checkBoxBtns[i].id.toString(),
						padre_id: 0,
						hijo_id: this.state.elLugar,
						fecha_cita: this.state.fechagg,
						hora: this.state.laHora,
						asunto: this.state.elAsunto,
						aprobada: 0
					})
				}).then(res => res.json())
					.then(responseJson => {
						this.setState({elRequest: responseJson});
					});
			}
			if (this.state.elRequest.error !== undefined) {
				Alert.alert('Error al enviar cita', 'Ha ocurrido un error ' + this.state.elRequest.error.status_code + ' al tratar de enviar la cita si el error continua pónganse en contacto con soporte (Solicitar cita)');
			} else {
				Alert.alert('Solicitud de cita enviada correctamente', 'Se ha notificado a los destinatarios', [{
					text: 'Enterado',
					onPress: () => Actions.HomePage()
				}]);
			}
		}
	}

	// +++++++++++++++++++++++++++++++++++++Maestro+++++++++++++++++++++++++++++
	async getPersonal() {
		await fetch(this.state.uri + '/api/get/personal/' + this.state.receptor, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar los maestros', 'Ha ocurrido un error ' +
						responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Citas - Admin)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({maestros: responseJson});
					this.setState({indexSeleccionados: []});
					this.state.maestros.forEach(() => {
						this.state.indexSeleccionados.push(0);
					});
				}
			});
	}

	async selectMaestro(indx, itm) {
		await this.setState({
			indxMaestro: indx,
			elMaestro: itm.name,
			elIdProf: itm.id
		});
		let i = this.state.seleccionados.indexOf(itm);
		if (i > -1) {
			this.state.seleccionados.splice(i, 1);
			this.state.indexSeleccionados[indx] = 0;
		} else {
			this.state.seleccionados.push(itm);
			this.state.indexSeleccionados[indx] = 1;
		}
		await this.setState({aux: 0});
	}

	renderMaestro(itmMaestro, indx) {
		let a = this.state.maestros.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indx !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {borderColor: this.state.secondColor}];
		let texto = [styles.textoN];
		if (this.state.indexSeleccionados[indx] === 1) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (<TouchableHighlight
			key={indx + 'Maestro'}
			underlayColor={this.state.secondColor}
			style={[tabRow, smallButtonStyles]}
			onPress={() => this.selectMaestro(indx, itmMaestro)}
		>
			<View style={[styles.campoTablaG, {alignItems: 'center'}]}>
				<Text style={[texto, {marginRight: 25}]}>{itmMaestro.name}</Text>
			</View>
		</TouchableHighlight>);
	}

	renderMaestros() {
		let buttonsAlumnos = [];
		this.state.maestros.forEach((itemMaestro, indexMaestro) => {
			buttonsAlumnos.push(this.renderMaestro(itemMaestro, indexMaestro));
		});
		return buttonsAlumnos;
	}

	async onChangeR(option) {
		await this.setState({
			receptor: option.puesto,
			checkBtnsIndex: [],
			indexSeleccionados: [],
			seleccionados: [],
			checkBoxBtns: [],
			elAsunto: '',
			elLugar: '',
			fechagg: '',
			laHora: ''
		});
		await this.getPersonal();
	}

	//*********************************** Render +++++++++++++++++++++++++++++++++

	async opnModalMaestro() {
		if (this.state.receptor === '') {
			Alert.alert('Tipo de personal no definido', 'Seleccione al personal que quiere citar para continuar', [
				{text: 'Enterado'}
			])
		} else {
			await this.setState({isModalCateg: true});
			await this.funcion1();
		}
		await this.setState({aux: 0});
	}

	async funcion1() {
		await this.setState({seleccionados: []});
		if (this.state.checkBoxBtns.length !== 0) {
			await this.setState({indexSeleccionados: []});
			this.state.checkBoxBtns.forEach((it) => {
				this.state.seleccionados.push(it);
			});
			this.state.checkBoxBtnsIndex.forEach((it) => {
				this.state.indexSeleccionados.push(it);
			});
		}
		await this.setState({aux: 0});
	}

	async closeModalMaestro() {
		if (this.state.seleccionados.length === 0) {
			if (this.state.receptor === 'Admin') {
				Alert.alert('No ha seleccionado a ningún admistrador', ' ', [
					{text: 'Enterado'}
				])
			} else if (this.state.receptor === 'Maestro') {
				Alert.alert('No ha seleccionado a ningún maestro', ' ', [
					{text: 'Enterado'}
				])
			} else if (this.state.receptor === 'Prefecto') {
				Alert.alert('No ha seleccionado a ningún prefecto', ' ', [
					{text: 'Enterado'}
				])
			} else if (this.state.receptor === 'Medico') {
				Alert.alert('No ha seleccionado a ningún médico', ' ', [
					{text: 'Enterado'}
				])
			}
		} else {
			await this.setState({isModalCateg: false});
			await this.funcion2();
		}
		await this.setState({aux: 0});
	}

	async funcion2() {
		await this.setState({checkBoxBtns: [], checkBoxBtnsIndex: []});
		this.state.seleccionados.forEach((it) => {
			this.state.checkBoxBtns.push(it);
		});
		this.state.indexSeleccionados.forEach((it) => {
			this.state.checkBoxBtnsIndex.push(it);
		});
		await this.setState({aux: 0});
	}

	async closeModalMaestro2() {
		await this.setState({isModalCateg: false});
		await this.setState({seleccionados: []});
		if (this.state.checkBoxBtns.length !== 0) {
			await this.setState({indexSeleccionados: []});
			this.state.checkBoxBtns.forEach((it) => {
				this.state.seleccionados.push(it);
			});
			this.state.checkBoxBtnsIndex.forEach((it) => {
				this.state.indexSeleccionados.push(it);
			});
		} else {
			await this.getPersonal();
		}
		await this.setState({aux: 0});
	}

	render() {
		let data = this.state.losRoles.map((itm, i) => {
			return {
				key: i, label: itm.role, puesto: itm.key
			};
		});
		let dataH = this.state.horas.map((item, i) => {
			return {
				key: i, label: moment(item.inicio, 'HH:mm:ss').format('H:mm')
			};
		});

		const alumnosList = this.renderMaestros();
		return (<View style={styles.container}>
			<Modal
				isVisible={this.state.isModalCateg}
				backdropOpacity={0.5}
				animationIn={'bounceIn'}
				animationOut={'bounceOut'}
				animationInTiming={1000}
				animationOutTiming={1000}
			>
				<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
					{this.state.receptor === 'Admin' ? (
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione al administrador
						</Text>
					) : this.state.receptor === 'Maestro' ? (
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione al maestro
						</Text>
					) : this.state.receptor === 'Prefecto' ? (
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione al prefecto
						</Text>
					) : this.state.receptor === 'Medico' ? (
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione al médico
						</Text>
					) : null}
					<View
						style={[styles.tabla, {
							borderColor: this.state.secondColor,
							marginTop: 10,
							height: responsiveHeight(60)
						}]}>
						<ScrollView>
							{alumnosList}
						</ScrollView>
					</View>
					<View style={[styles.modalWidth, styles.row]}>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.closeModalMaestro2()}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
							]}
							onPress={() => this.closeModalMaestro()}
						>
							<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
			<ScrollView showsVerticalScrollIndicator={false}>
				<KeyboardAvoidingView
					behavior={Platform.OS === 'ios' ? 'position' : null}
					keyboardVerticalOffset={Platform.OS === 'ios' ? 180 : 0}
				>
					<Text
						style={[styles.main_title, {color: this.state.thirdColor, marginTop: 0, paddingTop: 0}]}
					>
						Seleccione el personal a citar
					</Text>
					<ModalSelector
						data={data}
						selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
						selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
						initValue='Seleccione personal'
						onChange={option => this.onChangeR(option)}
					/>
					<Text
						style={[styles.main_title, {color: this.state.thirdColor}]}
					>
						Seleccione al personal
					</Text>
					<View style={{
						alignItems: 'center',
						backgroundColor: this.state.fourthColor,
						justifyContent: 'center',
						padding: 15,
						borderRadius: 6
					}}
					>
						<TouchableOpacity
							style={[
								styles.bigButton,
								styles.btnShowMod,
								{
									marginTop: 0,
									marginBottom: 5,
									borderColor: this.state.secondColor,
									backgroundColor: '#fff'
								}
							]}
							onPress={() => this.opnModalMaestro()}>
							<Text>Ver lista</Text>
						</TouchableOpacity>
						<Text style={{marginTop: 10}}>Docentes seleccionados:</Text>
						<Text style={[styles.textButton, {color: 'black'}]}>
							{this.state.checkBoxBtns.length}
						</Text>
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Asunto y fecha
					</Text>
					<View style={{alignItems: 'center'}}>
						<View>
							<Text style={{marginTop: 3}}>Motivo de cita</Text>
							<TextInput
								keyboardType='default'
								maxLength={254}
								onChangeText={text => (this.state.elAsunto = text)}
								placeholder={'Escriba el asunto'}
								returnKeyType='next'
								underlineColorAndroid='transparent'
								style={[styles.inputPicker, {
									borderColor: this.state.secondColor, paddingHorizontal: 10, marginTop: 3
								}]}
							/>
						</View>
						<View style={[styles.row, styles.widthall, {marginTop: 10}]}>
							<View style={{alignItems: 'center'}}>
								<Text>Para el día</Text>
								<TouchableOpacity
									style={[styles.pickerParam1, styles.btn3, {
										borderColor: this.state.secondColor,
										marginTop: 3
									}]}
									onPress={() => this.setState({isDateTimePickerVisible: true})}
								>
									{this.state.fechagg === '' ? (
										<Text style={{fontSize: responsiveFontSize(1.7)}}>Seleccione fecha</Text>
									) : (
										<Text
											style={{fontSize: responsiveFontSize(1.7)}}>{moment(this.state.fechagg).format('DD/MM/YYYY')}</Text>
									)}
								</TouchableOpacity>
								<DateTimePicker
									locale={'es'}
									isVisible={this.state.isDateTimePickerVisible}
									onConfirm={(date) => this._handleDatePicked(date)}
									onCancel={() => this._hideDateTimePicker()}
									titleIOS={'Seleccione una fecha'}
									confirmTextIOS={'Seleccionar'}
									cancelTextIOS={'Cancelar'}
								/>
							</View>
							<View style={{alignItems: 'center'}}>
								<Text>En la hora de</Text>
								<ModalSelector
									data={dataH}
									selectStyle={[styles.pickerParam1, styles.btn3, {
										borderColor: this.state.secondColor,
										marginTop: 3
									}]}
									cancelText='Cancelar'
									optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.7)}}
									selectTextStyle={{fontSize:responsiveFontSize(1.7)}}
									initValue='Horas'
									onChange={option => (this.state.laHora = option.label)}
								/>
							</View>
							<View style={{alignItems: 'center'}}>
								<Text>Lugar</Text>
								<TextInput
									keyboardType='default'
									maxLength={254}
									onChangeText={text => (this.state.elLugar = text)}
									placeholder={'Lugar'}
									returnKeyType='next'
									underlineColorAndroid='transparent'
									style={[styles.pickerParam1, styles.btn3, {
										borderColor: this.state.secondColor, paddingHorizontal: 10, marginTop: 3,
										textAlign: 'center',fontSize:responsiveFontSize(1.7)
									}]}
								/>
							</View>
						</View>
					</View>
					<TouchableOpacity
						style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
						onPress={() => this.requestMultipart()}
					>
						<Text style={styles.textButton}>Enviar la solicitud</Text>
					</TouchableOpacity>
				</KeyboardAvoidingView>
			</ScrollView>
		</View>);
	}
}
