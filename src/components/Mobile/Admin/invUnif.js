import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    TextInput,
    Keyboard,
} from "react-native";
import styles from "../../styles";
import ModalSelector from "react-native-modal-selector";

export default class invUnif extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {}
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onChange1(option1) {
        await this.setState({
            elTiempo: option1.label
        });
    }

    render() {
        let index = 0;
        const data2 = [
            {key: index++, label: "Gala/Diario"},
            {key: index++, label: "Deportivo"},
            {key: index++, label: "Otro"}
        ];
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <Text style={styles.main_title}>Agregue uniformes al inventario</Text>
                <Text style={[styles.widthall, styles.invUnifText]}>
                    Nombre y tipo de la prenda
                </Text>
                <View style={[styles.row, styles.widthall]}>
                    <TextInput
                        keyboardType="default"
                        maxLength={100}
                        multiline={false}
                        onChangeText={text => this.setState({laPrenda: text})}
                        onEndEditing={() => Keyboard.dismiss()}
                        returnKeyType="done"
                        underlineColorAndroid="transparent"
                        style={[
                            styles.cuadroess,
                            styles.btn2,
                            {backgroundColor: this.state.fourthColor}
                        ]}
                    />
                    <ModalSelector
                        data={data2}
                        cancelText="Cancelar"
                        initValue="Seleccione tipo"
                        onChange={option => this.setState({elTipo: option})}
                        optionTextStyle={{color: this.state.thirdColor}}
                        selectStyle={[
                            styles.cuadroess,
                            styles.btn2,
                            {borderColor: this.state.secondColor}
                        ]}
                    />
                </View>
                <Text style={[styles.widthall, styles.invUnifText]}>
                    Cantidad por talla y precio
                </Text>
                <View style={[styles.row, styles.btn1]}>
                    <Text style={[styles.textW, styles.fontUn]}>Tallas</Text>
                    <Text style={[styles.textW, styles.fontUn]}>Cantidad</Text>
                    <Text style={[styles.textW, styles.fontUn]}>Precio</Text>
                </View>
            </View>
        );
    }
}
