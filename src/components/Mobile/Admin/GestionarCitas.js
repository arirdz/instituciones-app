import React from 'react';
import {Alert, AsyncStorage, ScrollView, Text, TouchableOpacity, View, RefreshControl} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import styles from '../../styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {responsiveWidth} from 'react-native-responsive-dimensions';
import Communications from 'react-native-communications';
import {Actions} from 'react-native-router-flux';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class GestionarCitas extends React.Component {
	_onRefresh = () => {
		this.setState({refreshing: true});
		if (this.state.selected === 'Citas finalizadas') {
			this.getHistorial();
		}else if (this.state.selected ==='Citas recibidas'){
			this.getSolicitudes();
		} else if (this.state.selected ==='Citas realizadas') {
			this.getCitasHechas();
		}
	};
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			laSolicitud: '',
			selected: 'Citas finalizadas',
			solicitudes: [],
			historial: [],
			refreshing: false,
			citasHechas: [],
			numTel: ''
		};
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async componentWillMount() {
		await this.getURL();
		await this.getSolicitudes();
		await this.getHistorial();
		await this.getCitasHechas();
	}

	async getHistorial() {
		let solicitudesList = await fetch(this.state.uri + '/api/feed/citas/getCitas/aceptadas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let listSolicitudes = await solicitudesList.json();
		this.setState({historial: listSolicitudes});
		this.setState({refreshing: false});
	}

	historial(itm, i) {
		if (itm.user.role === 'Padre') {
			if (itm.aprobada === '1') {
				return (
					<View
						key={i + 'cita'}
						style={[styles.cardCita, {
							borderColor: this.state.fourthColor,
							shadowColor: this.state.fourthColor
						}]}
					>
						<View
							style={{
								borderBottomWidth: 1,
								borderColor: this.state.fourthColor,
								padding: 5,
								alignItems: 'center'
							}}
						>
							<Text style={styles.notifTitle}>Solicitud de Cita</Text>
						</View>
						<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
							<Text style={styles.textInfo}>
								Se ha solicitado una cita del Sr(a).{' '}
								<Text style={styles.textW}>{itm.user.name}.</Text> tutor
								del alumno(a):{' '}
								<Text style={styles.textW}>{itm.hijos.name}.</Text>{' '}
								{'\n'}del Grado y grupo:{' '}
								<Text style={styles.textW}>
									{itm.hijos.grado} {itm.hijos.grupo}.
								</Text>
							</Text>
							{itm.aprobada === '2' ? (
								<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>*Esta cita fue
																									 cancelada</Text>) : null}
							<Text style={styles.textInfo}>
								Agendada para el día{' '}
								<Text style={styles.textW}>{itm.fecha_cita}</Text>
								a las {moment(itm.hora, 'HH:mm:ss').format('HH:mm')}. {'\n'}para tratar sobre:{' '}
								<Text style={styles.textW}>{itm.asunto}.</Text>
							</Text>
						</View>
					</View>
				);
			}
		} else if (itm.user.role === 'Admin') {
			if (itm.aprobada === '1') {
				return (
					<View
						key={i + 'cita'}
						style={[styles.cardCita, {
							borderColor: this.state.fourthColor,
							shadowColor: this.state.fourthColor
						}]}
					>
						<View
							style={{
								borderBottomWidth: 1,
								borderColor: this.state.fourthColor,
								padding: 5,
								alignItems: 'center'
							}}
						>
							<Text style={styles.notifTitle}>Solicitud de Cita</Text>
						</View>
						<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
							<Text style={styles.textInfo}>
								El maestro(a).{' '}
								<Text style={styles.textW}>{itm.user.name}.</Text>
								ha solicitado su asistencias en:{' '}
								<Text style={styles.textW}>{itm.hijo_id}.</Text>{' '}
							</Text>
							{itm.aprobada === '2' ? (
								<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>
									*Esta cita fue cancelada por el maestro
								</Text>) : null}
							<Text style={styles.textInfo}>
								Agendada para el día {' '}
								<Text style={styles.textW}>{itm.fecha_cita}{' '}</Text>
								{' '}a las {moment(itm.hora, 'HH:mm:ss').format('HH:mm')} hrs. {'\n'}para tratar
								sobre:{' '}
								<Text style={styles.textW}>{itm.asunto}.</Text>
							</Text>
						</View>
					</View>
				);
			} else {
				if (itm.receptor.role !== 'Padre') {
					return (
						<View
							key={i + 'cita'}
							style={[styles.cardCita, {
								borderColor: this.state.fourthColor,
								shadowColor: this.state.fourthColor
							}]}
						>
							<View
								style={{
									borderBottomWidth: 1,
									borderColor: this.state.fourthColor,
									padding: 5,
									alignItems: 'center'
								}}
							>
								<Text style={styles.notifTitle}>Solicitud de Cita</Text>
							</View>
							<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
								<Text style={styles.textInfo}>
									Has solicitado la asistencia de:<Text
									style={styles.textW}> {itm.receptor.name}.</Text>{'\n'}
									para tratar el asunto de:{' '}<Text style={styles.textW}>{itm.asunto}.</Text>{'\n'}
									el cual fue citado en:{' '}
									<Text style={styles.textW}>{itm.hijo_id}.</Text>{' '}
								</Text>
								<Text style={styles.textInfo}>
									Agendada para el día{' '}
									<Text style={styles.textW}>{itm.fecha_cita}{' '}</Text>
									{' '}a las <Text style={styles.textW}>{moment(itm.hora, 'HH:mm:ss').format('HH:mm')}</Text> hrs.
								</Text>
								{itm.aprobada === '2' ? (
									<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>
										*Cancelaste esta cita
									</Text>) : null}
							</View>
						</View>
					);
				} else {
					return (
						<View
							key={i + 'cita'}
							style={[styles.cardCita, {
								borderColor: this.state.fourthColor,
								shadowColor: this.state.fourthColor
							}]}>
							<View style={{
								borderBottomWidth: 1,
								borderColor: this.state.fourthColor,
								padding: 5,
								alignItems: 'center'
							}}>
								<Text style={styles.notifTitle}>Solicitud de Cita</Text>
							</View>
							<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
								<Text style={styles.textInfo}>
									Has solicitado una cita para tratar el asunto de: <Text
									style={{fontWeight: '700', textAlign: 'center'}}>{itm.asunto}</Text>{'\n'}
									con el tutor del alumno:{' '}
									<Text style={{fontWeight: '700'}}>
										{itm.hijos.name}{'\n'} del
															   Grado: {itm.hijos.grado} Grupo: {itm.hijos.grupo}{' '}
									</Text>{'\n'}
									Agendada para el día el <Text style={{fontWeight: '700', fontStyle: 'italic'}}>{itm.fecha_cita}</Text> a las <Text style={{fontWeight: '700', fontStyle: 'italic'}}>{moment(itm.hora, 'HH:mm:ss').format('HH:mm')}</Text> hrs
								</Text>

								{itm.aprobada === '2' ? (
									<Text style={{fontStyle: 'italic', marginVertical: 3, color: 'red'}}>
										*Cancelaste esta cita
									</Text>) : null}
							</View>
						</View>
					);
				}
			}
		}
	}

	historiales() {
		let btnSolicitudes = [];
		this.state.historial.forEach((item, index) => {
			btnSolicitudes.push(this.historial(item, index));
		});
		return btnSolicitudes;
	}

//+-+-+-+--+-+--+-+-+Citas hechas
	async getCitasHechas() {
		let btnHecho = await fetch(this.state.uri + '/api/feed/citas/hechas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let btnHechos = await btnHecho.json();
		this.setState({citasHechas: btnHechos});
		this.setState({refreshing: false});
	}

	async cancelarLaCita(id) {
		fetch(this.state.uri + '/api/feed/citas/cancelar/' + id, {
			method: 'POST', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Aceptar Citas)', [{text: 'Entendido' /*onPress: () => Actions.drawer()*/}]);
				} else {
					Alert.alert('Cita cancelada', 'Has cancelado la cita', [{
						text: 'Entendido', onPress: () => Actions.refresh({key: Math.random()})
					}]);
				}
			});
	}

	async cancelarCitaAgend(id) {

		Alert.alert('Cancelar cita', '¿Está seguro que desea cancelar la cita?', [{
			text: 'Si', onPress: () => this.cancelarLaCita(id)
		}, {text: 'No'}
		]);
	}

	rndHecho(itemSolicitud, i) {
		if (itemSolicitud.hijos !== null) {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}>
					<View style={{
						borderBottomWidth: 1,
						borderColor: this.state.fourthColor,
						padding: 5,
						alignItems: 'center'
					}}>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							Has solicitado una cita para tratar el asunto de: <Text
							style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.asunto}</Text>{'\n'}
							con el tutor del alumno:{' '}
							<Text style={{fontWeight: '700'}}>
								{itemSolicitud.hijos.name}{'\n'}
																 Grado: {itemSolicitud.hijos.grado} Grupo: {itemSolicitud.hijos.grupo}{' '}
							</Text>
						</Text>
						<Text style={{fontWeight: '700'}}>{'\n'}
							Agendada para el día {itemSolicitud.fecha_cita} a
							las {moment(itemSolicitud.hora, 'HH:mm:ss').format('HH:mm')}
						</Text>
					</View>
					<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
						<TouchableOpacity
							onPress={() => this.cancelarCitaAgend(itemSolicitud.id)}
							style={[styles.btnCita, {backgroundColor: '#fc001e', paddingVertical: 4}]}
						>
							<Text style={{color: '#fff'}}>Cancelar Cita</Text>
						</TouchableOpacity>
					</View>
				</View>
			);
		} else {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}>
					<View style={{
						borderBottomWidth: 1,
						borderColor: this.state.fourthColor,
						padding: 5,
						alignItems: 'center'
					}}>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							Has solicitado la asistencia de:<Text
							style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.receptor.name}</Text>{'\n'}
							Para tratar el asunto de:<Text
							style={{fontWeight: '700', textAlign: 'center'}}>{itemSolicitud.asunto}</Text>{'\n'}
							El cual fue citado en el lugar:{' '}
							<Text style={{fontWeight: '700'}}>
								{itemSolicitud.hijo_id}{'\n'} {' '}
							</Text>
						</Text>
						<Text style={{fontWeight: '700'}}>{'\n'}
							Agendada para el día {itemSolicitud.fecha_cita} a
							las {moment(itemSolicitud.hora, 'HH:mm:ss').format('HH:mm')}
						</Text>
					</View>
					<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
						<TouchableOpacity
							onPress={() => this.cancelarCitaAgend(itemSolicitud.id)}
							style={[styles.btnCita, {backgroundColor: '#fc001e', paddingVertical: 4}]}
						>
							<Text style={{color: '#fff'}}>Cancelar Cita</Text>
						</TouchableOpacity>
					</View>
				</View>
			);
		}
	}

	rndHechos() {
		let btnCitaH = [];
		this.state.citasHechas.forEach((it, i) => {
			btnCitaH.push(this.rndHecho(it, i))
		});
		return btnCitaH;
	}

	async getSolicitudes() {
		let solicitudesList = await fetch(this.state.uri + '/api/feed/citas/getCitas', {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		});
		let listSolicitudes = await solicitudesList.json();
		this.setState({solicitudes: listSolicitudes});
		this.setState({refreshing: false});
	}

	async onListPressedSolicitud(indexSolicitud, solicitud) {
		this.setState({selectedIndexSolicitud: indexSolicitud, laSolicitud: solicitud});
	}

	async llamar(id) {
		await fetch(this.state.uri + '/api/datos/user/' + id, {
			method: 'GET', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (LLamada en citas)', [{
						text: 'Entendido', onPress: () => Actions.drawer()
					}]);
				} else {
					this.setState({numTel: responseJson[0].telefono_casa_tutor});
					Communications.phonecall(this.state.numTel, true);
				}
			});
	}

	renderSolicitudes() {
		let btnSolicitudes = [];
		this.state.solicitudes.forEach((itemSolicitud, indexSolicitud) => {
			btnSolicitudes.push(this.renderSolicitud(itemSolicitud, indexSolicitud));
		});
		return btnSolicitudes;
	}

	renderSolicitud(itm, i) {
		if (itm.user.role === 'Padre') {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View
						style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}
					>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							Se ha solicitado una cita del Sr(a).{' '}
							<Text style={styles.textW}>{itm.user.name}.</Text> tutor
							del alumno(a):{' '}
							<Text style={styles.textW}>{itm.hijos.name}.</Text>{' '}
							{'\n'}del Grado y grupo:{' '}
							<Text style={styles.textW}>
								{itm.hijos.grado} {itm.hijos.grupo}.
							</Text>
						</Text>
						<Text style={styles.textInfo}>
							Agendada para el día{' '}
							<Text style={styles.textW}>{itm.fecha_cita}</Text>
							a las {moment(itm.hora, 'HH:mm:ss').format('HH:mm')}. {'\n'}para tratar sobre:{' '}
							<Text style={styles.textW}>{itm.asunto}.</Text>
						</Text>
					</View>

					<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
						<TouchableOpacity
							onPress={() => this.requestMultipart(itm.id)}
							style={[styles.btnCita, {backgroundColor: '#3fa345'}]}
						>
							<Text style={{color: '#fff'}}>Aceptar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnCita, {backgroundColor: '#fc001e'}]}
						>
							<Text style={{color: '#fff'}}>Declinar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor, paddingHorizontal:10}]}
						>
							<Text>Mensaje</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.llamar(itm.user.id)}
							style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor, paddingHorizontal:10}]}
						>
							<Text>Llamar</Text>
						</TouchableOpacity>
					</View>
				</View>
			);
		} else if (itm.user.role === 'Admin') {
			return (
				<View
					key={i + 'cita'}
					style={[styles.cardCita, {
						borderColor: this.state.fourthColor,
						shadowColor: this.state.fourthColor
					}]}
				>
					<View
						style={{
							borderBottomWidth: 1,
							borderColor: this.state.fourthColor,
							padding: 5,
							alignItems: 'center'
						}}
					>
						<Text style={styles.notifTitle}>Solicitud de Cita</Text>
					</View>
					<View style={{borderBottomWidth: 1, borderColor: this.state.fourthColor, padding: 12}}>
						<Text style={styles.textInfo}>
							El maestro(a).{' '}
							<Text style={styles.textW}>{itm.user.name}.</Text>
							ha solicitado su asistencias en:{' '}
							<Text style={styles.textW}>{itm.hijo_id}.</Text>{' '}
						</Text>
						<Text style={styles.textInfo}>
							Agendada para el día{' '}
							<Text style={styles.textW}>{itm.fecha_cita}{' '}</Text>
							{' '}a las {moment(itm.hora, 'HH:mm:ss').format('HH:mm')} hrs. {'\n'}para tratar sobre:{' '}
							<Text style={styles.textW}>{itm.asunto}.</Text>
						</Text>
					</View>

					<View style={[{flexDirection: 'row', paddingVertical: 5}]}>
						<TouchableOpacity
							onPress={() => this.requestMultipart(itm.id)}
							style={[styles.btnCita, {backgroundColor: '#3fa345'}]}
						>
							<Text style={{color: '#fff'}}>Aceptar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnCita, {backgroundColor: '#fc001e'}]}
						>
							<Text style={{color: '#fff'}}>Declinar</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.requestMultipart1()}
							style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor, paddingHorizontal:10}]}
						>
							<Text>Mensaje</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => this.llamar(itm.user.id)}
							style={[styles.btnIconCita, {backgroundColor: this.state.fourthColor, paddingHorizontal:10}]}
						>
							<Text>Llamar</Text>
						</TouchableOpacity>
					</View>
				</View>
			);
		}
	}

	async aceptarCita(id) {
		fetch(this.state.uri + '/api/feed/citas/update/' + id, {
			method: 'POST', headers: {
				'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res)
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Aceptar Citas)', [{text: 'Entendido'}]);
				} else {
					Alert.alert('¡Excelente!', 'Has Aceptado la cita', [{
						text: 'Entendido', onPress: () => Actions.refresh({key: 'gestCitasMed'})
					}]);
				}
			});
	}

	async requestMultipart(id) {
		Alert.alert('Aceptar cita',
			'¿Está seguro de aceptar la cita?',
			[{text: 'Si', onPress: () => this.aceptarCita(id)}, {text: 'No'}]
		);
	}

	async requestMultipart1() {
		Alert.alert('Funcionalidad no disponible', 'Por ahora no disponibles', [{
			text: 'Entendido'
		}]);
	}

	async onChange(option) {
		this.setState({selected: option.label});
	}

	render() {
		let index = 0;
		const data = [
			{key: index++, label: 'Citas finalizadas'},
			{key: index++, label: 'Citas recibidas'},
			{key: index++,label: 'Citas realizadas'}
			];
		return (<View style={styles.container}>
			<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
				Revisar solicitudes
			</Text>
			<ModalSelector
				data={data}
				selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
				cancelText='Cancelar'
				optionTextStyle={{color: this.state.thirdColor}}
				initValue={this.state.selected}
				onChange={option => this.onChange(option)}/>
			{this.state.selected === 'Citas recibidas' ? (
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Solicitudes recientes
				</Text>
			) : this.state.selected === 'Citas finalizadas' ? (
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Historial de solicitudes
				</Text>
			) : (
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Citas creadas
				</Text>
			)}
			<ScrollView
				style={[styles.widthall, {marginTop: 10, ...ifIphoneX({marginBottom: 40}, {marginBottom: 20})}]}
				refreshControl={
					<RefreshControl
						refreshing={this.state.refreshing}
						onRefresh={this._onRefresh}
					/>
				}
				overScrollMode='always'
				showsVerticalScrollIndicator={false}
			>
				{this.state.selected === 'Citas recibidas' ? this.renderSolicitudes()
					: this.state.selected === 'Citas finalizadas' ? this.historiales() : this.rndHechos()}
			</ScrollView>
		</View>);
	}
}

