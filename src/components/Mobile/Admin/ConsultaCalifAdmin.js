import React from 'react';
import {
    Alert,
    AsyncStorage,
    Platform,
    ScrollView,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import styles from '../../styles';
import GradoyGrupo from './../Globales/GradoyGrupo';
import Periodos from './../Globales/Periodos';
import {responsiveFontSize, responsiveWidth} from "react-native-responsive-dimensions";
import ModalSelector from 'react-native-modal-selector';
import Spinner from 'react-native-loading-spinner-overlay';

export default class ConsultaCalifAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lasCalifs: [],
            allCalifs: [],
            materias: [],
            visible: false,
            change: true,
            elTipo: 'calif',
            indexSelected: 0,
            indxPeriodo: -1,
            elPeriodo: '',
            indxGrado: -1,
            elGrado: '',
            indxGrupo: -1,
            elGrupo: '',
            laMateria: '',
            aux: 0
        };
    }

    _spinner = (state) => {
        this.setState({visible: state});
    };

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        await this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    //+--+-+-+-+-+-+-+-+-+-+ Periodo, Grado y Grupo -+-+-+-+-+-+-+-+-+-+
    async selectedPeriodo(indxBtn, itmBtn) {
        await this.setState({indxPeriodo: indxBtn, elPeriodo: itmBtn});
        if (this.state.elGrado !== '' && this.state.elGrupo !== '' && this.state.laMateria !== '') {
            await this.getCalificaciones();
        }
    }

    async selectedGrado(indxBtn, itmBtn) {
        if (this.state.elGrado !== itmBtn) {
            await this.setState({indxGrado: indxBtn, elGrado: itmBtn});
            if (this.state.indexSelected === 0 && this.state.elGrupo !== '' && this.state.laMateria !== '') {
                await this.getCalificacionesPeriodos()
            } else if (this.state.elPeriodo !== '' && this.state.elGrupo !== '' && this.state.laMateria !== '') {
                await this.getCalificaciones();
            }
        } else {
            await this.setState({indxGrado: indxBtn, elGrado: itmBtn});
        }
    }

    async selectedGrupo(indxBtn, itmBtn) {
        if (this.state.elGrupo !== itmBtn) {
            await this.setState({indxGrupo: indxBtn, elGrupo: itmBtn});
            if (this.state.indexSelected === 0 && this.state.elGrado !== '' && this.state.laMateria !== '') {
                await this.getCalificacionesPeriodos()
            } else if (this.state.elPeriodo !== '' && this.state.elGrado !== '' && this.state.laMateria !== '') {
                await this.getCalificaciones();
            }
        } else {
            await this.setState({indxGrupo: indxBtn, elGrupo: itmBtn});
        }
        await this.getMaterias(itmBtn);
    }

    //+-+-+-+-+-+-+-+- get Materias +-+-+-+-+-+-+-+-
    async getMaterias(grado) {
        await fetch(this.state.uri + '/api/feed/view/materias/' + grado, {
            method: 'GET', headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar las materias',
                        'Hubo un error al tratar de cargar las materias');
                } else {
                    this.setState({materias: responseJson});
                }
            });
    }

    async onChange(option) {
        await this.setState({laMateria: option.id, tipoMat: option.tipo_materia});
        if (this.state.indexSelected === 0) {
            await this.getCalificacionesPeriodos();
        } else {
            await this.getCalificaciones()
        }
    }

    //-+-+-+-+-+-+- las califs
    async getCalificaciones() {
        await this._spinner(true);
        await fetch('http://127.0.0.1:8000/api/calif/maestro/' +
            this.state.elPeriodo +
            '/' +
            this.state.laMateria +
            '/' +
            this.state.elGrado +
            '/' +
            this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar las calificaciones',
                        'Hubo un error al tratar de guardar las calificaciones', [
                            {text: 'Etendido', onPress: () => this._spinner(false)}
                        ]);
                } else {
                    this.setState({lasCalifs: responseJson});
                    this._spinner(false);
                }
            });
    }

    rndAlumnos() {
        let alumnos = [];
        if (this.state.lasCalifs.length !== 0) {
            this.state.lasCalifs.forEach((itm, ix) => {
                let a = this.state.lasCalifs.length - 1;
                let tabRow = [styles.rowTabla];
                if (ix !== a) {
                    tabRow.push({borderColor: this.state.secondColor});
                }
                if (this.state.tipoMat === 'formacion academica') {
                    let califValue = Number(itm.calificacion);
                    let value = '';
                    if (califValue === 5) {
                        value = 'I';
                    } else if (califValue >= 6 && califValue <= 7.49) {
                        value = 'II';
                    } else if (califValue >= 7.5 && califValue <= 9.49) {
                        value = 'III';
                    } else if (califValue >= 9.5) {
                        value = 'IV';
                    } else if (isNaN(califValue) || califValue >= 0 || califValue < 11) {
                        value = 'N/D';
                    }
                    alumnos.push(
                        <View
                            key={ix + 'alumnCal'}
                            underlayColor={'transparent'}
                            style={[tabRow, styles.campoTablaG, styles.row]}
                        >
                            <Text style={{width: responsiveWidth(45)}}>{itm.nombre}</Text>
                            <Text style={{width: responsiveWidth(20), textAlign: 'center'}}>{itm.calificacion}</Text>
                            <Text style={{width: responsiveWidth(10)}}>{value}</Text>
                        </View>
                    );
                } else {
                    let califValue = Number(itm.calificacion);
                    let value = '';
                    if (califValue === 0) {
                        value = 'I';
                    } else if (califValue === 1) {
                        value = 'II';
                    } else if (califValue === 2) {
                        value = 'III';
                    } else if (califValue === 3) {
                        value = 'IV';
                    } else if (isNaN(califValue)) {
                        value = 'N/D';
                    }
                    alumnos.push(
                        <View
                            key={ix + 'alumnCal'}
                            underlayColor={'transparent'}
                            style={[tabRow, styles.campoTablaG, styles.row]}
                        >
                            <Text style={{width: responsiveWidth(55)}}>{itm.nombre}</Text>
                            <Text style={{width: responsiveWidth(35), textAlign: 'center'}}>{value}</Text>
                        </View>
                    );
                }
            });
        }
        return alumnos;
    }

    async getCalificacionesPeriodos() {
        await this._spinner(true);
        await fetch('http://127.0.0.1:8000/api/calif/allPeriodos/maestro/' +
            this.state.laMateria +
            '/' +
            this.state.elGrado +
            '/' +
            this.state.elGrupo, {
            method: 'GET', headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al cargar las calificaciones',
                        'Hubo un error al tratar de guardar las calificaciones', [
                            {text: 'Etendido', onPress: () => this._spinner(false)}
                        ]);
                } else {
                    this.setState({allCalifs: responseJson});
                    this._spinner(false);
                }
            });
    }

    rndAllAlumno() {
        let allAlumnos = [];
        if (this.state.allCalifs.length !== 0) {
            this.state.allCalifs.forEach((itm, i) => {
                let a = this.state.allCalifs.length - 1;
                let tabRow = [styles.rowTabla];
                if (i !== a) {
                    tabRow.push({borderColor: this.state.secondColor});
                }
                allAlumnos.push(
                    <View
                        key={i + 'alumnCal'}
                        underlayColor={'transparent'}
                        style={[tabRow, styles.campoTablaG, styles.row]}
                    >
                        <Text style={{width: responsiveWidth(55)}}>{itm.nombre}</Text>
                        <View style={[styles.row, {width: responsiveWidth(30.5)}]}>{this.rndAllCalifs(i)}</View>
                    </View>
                )
            });
        }
        return allAlumnos;
    }

    rndAllCalifs(i) {
        let tipo = this.state.elTipo;
        let allCalifs = [];
        if (this.state.allCalifs[i].calificaciones.length !== 0) {
            this.state.allCalifs[i].calificaciones.forEach((itm, j) => {
                if (this.state.tipoMat === 'formacion academica') {
                    let califValue = Number(itm.calificacion);
                    let value = '';
                    if (califValue === 5) {
                        value = 'I';
                    } else if (califValue >= 6 && califValue <= 7.49) {
                        value = 'II';
                    } else if (califValue >= 7.5 && califValue <= 9.49) {
                        value = 'III';
                    } else if (califValue >= 9.5) {
                        value = 'IV';
                    } else if (isNaN(califValue) || califValue >= 0 || califValue < 11) {
                        value = 'N/D';
                    }
                    allCalifs.push(
                        <Text
                            key={j + 'asd'}
                            style={{
                                fontWeight: '500',
                                fontSize: responsiveFontSize(1.5),
                                width: responsiveWidth(10),
                                textAlign: 'center'
                            }}
                        >
                            {tipo === 'calif' ? itm.calificacion : value}
                        </Text>
                    )
                } else {
                    let califValue = Number(itm.calificacion);
                    let value = '';
                    if (califValue === 0) {
                        value = 'I';
                    } else if (califValue === 1) {
                        value = 'II';
                    } else if (califValue === 2) {
                        value = 'III';
                    } else if (califValue === 3) {
                        value = 'IV';
                    } else if (isNaN(califValue)) {
                        value = 'N/D';
                    }
                    allCalifs.push(
                        <Text
                            key={j + 'asd'}
                            style={{
                                fontWeight: '500',
                                fontSize: responsiveFontSize(1.5),
                                width: responsiveWidth(10),
                                textAlign: 'center'
                            }}
                        >
                            {value}
                        </Text>
                    )
                }
            });
        }
        return allCalifs;
    }

    ///+-+-+-+-+-+-+-+-+-
    async botonSelected(numero) {
        if (this.state.indexSelected !== numero) {
            await this.setState({change: false});
            await this.setState({elGrado: '', elGrupo: '', indxGrado: -1, indxGrupo: -1, laMateria: ''});
            await this.setState({change: true});
        }
        await this.setState({indexSelected: numero, elPeriodo: ''});
        if (numero !== 0) {
            await this.setState({allCalifs: []});
        } else {
            await this.setState({lasCalifs: []});
        }
    }

    async changeTipo(value) {
        await this.setState({elTipo: value});
    }

    async faltante() {
        if (this.state.indexSelected === 0) {
            if (this.state.elGrado === '') {
                Alert.alert('Seleccione un grado', '');
            } else if (this.state.elGrupo === '') {
                Alert.alert('Seleccione un grupo', '');
            }
        } else {
            if (this.state.elPeriodo === '') {
                Alert.alert('Seleccione un periodo', '');
            } else if (this.state.elGrado === '') {
                Alert.alert('Seleccione un grado', '');
            } else if (this.state.elGrupo === '') {
                Alert.alert('Seleccione un grupo', '');
            }
        }
    }

    render() {
        let tipo = this.state.elTipo;
        let periodo = this.state.elPeriodo;
        let grado = this.state.elGrado;
        let grupo = this.state.elGrupo;
        let materia = this.state.laMateria;
        let index = this.state.indexSelected;
        let data = this.state.materias.map((it, i) => {
            return {key: i, label: it.nombre, id: it.id, tipo_materia: it.tipo_materia}
        });
        return (
            <View style={styles.container}>
                <Spinner visible={this.state.visible} textContent='Cargando, por favor espere...'/>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione un tipo
                </Text>
                <View style={[styles.widthall, {alignItems: 'center'}]}>
                    <View
                        style={[styles.rowsCalif, {
                            width: responsiveWidth(62), marginTop: 10, marginBottom: 0
                        }]}>
                        <TouchableHighlight
                            underlayColor={'transparent'}
                            style={{
                                borderRadius: 6,
                                alignItems: 'center',
                                width: responsiveWidth(30),
                                padding: 6,
                                backgroundColor: index === 0 ? this.state.thirdColor : this.state.fourthColor
                            }}
                            onPress={() => this.botonSelected(0)}>
                            <Text
                                style={[styles.textW,
                                    {
                                        fontSize: responsiveFontSize(1.7),
                                        fontWeight: '700',
                                        color: index === 0 ? '#fff' : '#000'
                                    }]}
                            >
                                Todos
                            </Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                            underlayColor={'transparent'}
                            style={{
                                borderRadius: 6,
                                width: responsiveWidth(30),
                                alignItems: 'center',
                                padding: 6,
                                backgroundColor: index === 1 ? this.state.thirdColor : this.state.fourthColor
                            }}
                            onPress={() => this.botonSelected(1)}>
                            <Text
                                style={[styles.textW,
                                    {
                                        fontSize: responsiveFontSize(1.7),
                                        fontWeight: '700',
                                        color: index === 1 ? '#fff' : '#000'
                                    }]}
                            >
                                Por periodo
                            </Text>
                        </TouchableHighlight>
                    </View>
                </View>
                {index !== 0 ? <Periodos
                    onListItemPressedPeriodo={(indxBtn, itmBtn) => this.selectedPeriodo(indxBtn, itmBtn)}
                /> : null}
                {this.state.change === true ?
                    <GradoyGrupo
                        onListItemPressedGrado={(indxBtn, itmBtn) => this.selectedGrado(indxBtn, itmBtn)}
                        onListItemPressedGrupos={(indxBtn, itmBtn) => this.selectedGrupo(indxBtn, itmBtn)}
                    /> : null}
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione una materia
                </Text>
                {periodo !== '' && grado !== '' && grupo !== ''
                || index !== 1 && grado !== '' && grupo !== '' ?
                    <ModalSelector
                        cancelText='Cancelar'
                        data={data}
                        initValue='Seleccione la materia'
                        onChange={option => this.onChange(option)}
                        optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.5)}}
                        selectTextStyle={{fontSize: responsiveFontSize(1.5)}}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor, marginTop: 2, marginBottom: 15}
                        ]}
                    />
                    : <TouchableOpacity
                        style={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor, marginTop: 2}
                        ]}
                        onPress={() => this.faltante()}
                    >
                        <Text style={{fontSize: responsiveFontSize(1.5)}}>
                            Seleccione la materia
                        </Text>
                    </TouchableOpacity>}
                {this.state.lasCalifs.length === 0 && this.state.allCalifs.length === 0 ?
                    <Text
                        style={[styles.main_title, {
                            color: '#a8a8a8',
                            textAlign: 'center',
                            ...Platform.select({
                                ios: {marginTop: 50, marginBottom: 40,},
                                android: {marginTop: 40, marginBottom: 35}
                            }),
                            fontSize: responsiveFontSize(3)
                        }]}
                    >
                        {index === 0 ?
                            (grado === '' ? 'Selccione un grado' : grupo === '' ? 'Seleccione un grupo' : materia === '' ? 'Seleccione una materia' : 'Sin alumnos')
                            : index === 1 ?
                                (periodo === '' ? 'Seleccione un periodo' : grado === '' ? 'Seleccione un grado' : grupo === '' ? 'Seleccione un grupo' : materia === '' ? 'Seleccione una materia' : 'Sin alumnos')
                                : null}
                    </Text> :
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        overScrollMode={'always'}
                        style={{marginBottom: 20}}
                    >
                        <Text style={[styles.main_title, {color: this.state.thirdColor, marginTop: 0}]}>
                            Lista de alumnos
                        </Text>
                        {index === 0 && this.state.tipoMat === 'formacion academica' ?
                            <View style={[styles.widthall, {alignItems: 'flex-end'}]}>
                                <View
                                    style={[styles.row, {width: responsiveWidth(31.5), marginVertical: 5}]}
                                >
                                    <TouchableHighlight
                                        underlayColor={'transparent'}
                                        style={{
                                            borderRadius: 10,
                                            width: responsiveWidth(15),
                                            alignItems: 'center',
                                            padding: 3,
                                            backgroundColor: tipo === 'calif' ? this.state.thirdColor : this.state.fourthColor
                                        }}
                                        onPress={() => this.changeTipo('calif')}>
                                        <Text
                                            style={[styles.textW,
                                                {
                                                    fontSize: responsiveFontSize(1),
                                                    fontWeight: '700',
                                                    color: tipo === 'calif' ? '#fff' : '#000'
                                                }]}
                                        >
                                            Calificación
                                        </Text>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        underlayColor={'transparent'}
                                        style={{
                                            borderRadius: 10,
                                            alignItems: 'center',
                                            width: responsiveWidth(15),
                                            padding: 3,
                                            backgroundColor: tipo === 'nivel' ? this.state.thirdColor : this.state.fourthColor
                                        }}
                                        onPress={() => this.changeTipo('nivel')}>
                                        <Text
                                            style={[styles.textW,
                                                {
                                                    fontSize: responsiveFontSize(1),
                                                    fontWeight: '700',
                                                    color: tipo === 'nivel' ? '#fff' : '#000'
                                                }]}
                                        >
                                            Nivel
                                        </Text>
                                    </TouchableHighlight>
                                </View>
                                <View style={{
                                    width: responsiveWidth(31.5),
                                    borderBottomWidth: .5,
                                    borderColor: this.state.secondColor,
                                    marginBottom: 2
                                }}/>
                            </View> : null}
                        {index === 0 ?
                            <View
                                style={[styles.row, styles.widthall, {paddingHorizontal: 4}]}
                            >
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        width: responsiveWidth(60),
                                        fontSize: responsiveFontSize(1.5)
                                    }}
                                >
                                    Nombre del Alumno
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P1
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P2
                                </Text>
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        fontSize: responsiveFontSize(1.5),
                                        width: responsiveWidth(10),
                                        textAlign: 'center'
                                    }}
                                >
                                    P3
                                </Text>
                            </View>
                            : this.state.tipoMat === 'formacion academica' ?
                                <View
                                    style={[styles.row, styles.widthall, {paddingHorizontal: 4}]}
                                >
                                    <Text
                                        style={{
                                            fontWeight: '500',
                                            width: responsiveWidth(53),
                                            fontSize: responsiveFontSize(1.5)
                                        }}
                                    >
                                        Nombre del Alumno
                                    </Text>
                                    <Text
                                        style={{fontWeight: '500', fontSize: responsiveFontSize(1.5)}}
                                    >
                                        Califcación
                                    </Text>
                                    <Text
                                        style={{
                                            fontWeight: '500',
                                            fontSize: responsiveFontSize(1.5),
                                            textAlign: 'center'
                                        }}
                                    >
                                        Nivel de{'\n'}desempeño
                                    </Text>
                                </View>
                                : <View
                                    style={[styles.row, styles.widthall, {paddingHorizontal: 4, marginTop: 10}]}
                                >
                                    <Text
                                        style={{
                                            fontWeight: '500',
                                            width: responsiveWidth(45),
                                            fontSize: responsiveFontSize(1.5)
                                        }}
                                    >
                                        Nombre del Alumno
                                    </Text>
                                    <Text
                                        style={{
                                            fontWeight: '500',
                                            fontSize: responsiveFontSize(1.5),
                                            textAlign: 'center',
                                            width: responsiveWidth(35)
                                        }}
                                    >
                                        Nivel de desempeño
                                    </Text>
                                </View>
                        }
                        <View
                            style={[
                                styles.tabla,
                                {
                                    borderColor: this.state.secondColor,
                                    marginTop: 5,
                                }
                            ]}
                        >
                            {this.state.indexSelected === 0 ? this.rndAllAlumno() : this.rndAlumnos()}
                        </View>
                    </ScrollView>}
            </View>
        );
    }

}
