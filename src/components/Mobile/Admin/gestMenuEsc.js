import React from 'react';
import {
	Alert, AsyncStorage, KeyboardAvoidingView, Platform, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View
} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../../styles';
import DateTimePicker from 'react-native-modal-datetime-picker';
import MultiBotonRow from '../Globales/MultiBotonRow';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterailCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import HistorialMenu from './HistorialMenu';
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class gestMenuEsc extends React.Component {
    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            aux: 0,
            visible: false,
            dia1: false,
            dia2: false,
            fecha1: '',
            fecha2: '',
            botonSelected: 'Crear nuevo',
            indexSelected: 0,
            titleMenu: false,
            menuTitulo: 'Menu de la semana',
            menuDia: [{'dia': '', 'producto': [{'nombre_producto': '', 'precio': ''}]}],
            nombreDia: '',
            diaMenu: [],
            productoMenu: [],
            productName: '',
            precioProduct: '',
            elRequest: [],
            publicado: 0,
            elComentario: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
        this.state.menuDia.forEach(() => {
            this.state.diaMenu.push(false)
        });
        await this.setState({aux: 0})
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    //+-+-+-+-+-+-+-+-+-+-+-+-+-Guardar menu
    async AlertRequestG() {
        if (this.state.menuTitulo === '') {
            Alert.alert('Campo titulo menu vacio', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.fecha1 === '' && this.state.fecha2 === '') {
            Alert.alert('Duracion del menu indefinida', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.menuDia[0].dia === '') {
            Alert.alert('Productos de menu sin definir', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.menuDia[0].producto[0].nombre_producto === '') {
            Alert.alert('Productos de menu sin definir', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.menuDia[0].producto[0].precio === '') {
            Alert.alert('Productos de menu sin definir', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        }  else {
            Alert.alert('Guardar menú escolar', 'Está a punto de guardar un menú\n¿Seguro que desea continuar?', [
                {text: 'Sí', onPress: () => this.requesTituloMenuG()},
                {text: 'No'}
            ]);
        }
    }

    async requesTituloMenuG() {
        await this._changeWheelState();
        let formData = new FormData();
        formData.append('new', JSON.stringify({
            titulo: this.state.menuTitulo
        }));
        await fetch(this.state.uri + '/api/crear/menu/escolar', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al guardar menu',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Menú escolar)',
                        [{
                            text: 'Entendido',
                            onPress: () => [this.setState({visible: false}), Actions.drawer()]
                        }]);
                } else {
                    this.setState({elIdMenu: responseJson});
                    this.requestMultipartG(responseJson.id)
                }
            });
    }

    async requestMultipartG(id) {
		let comentario = '';
		if (this.state.elComentario === ''){
			comentario = 'Sin comentarios';
		} else {
			comentario = this.state.elComentario;
		}
        let fecha1 = this.state.fecha1;
        let fecha2 = this.state.fecha2;
        let formData = new FormData();
        for (let i = 0; i < this.state.menuDia.length; ++i) {
            for (let j = 0; j < this.state.menuDia[i].producto.length; ++j) {
                formData.append('new', JSON.stringify({
                    id_menu: id.toString(),
                    titulo_menu: this.state.menuTitulo,
                    fecha1: this.state.fecha1,
                    fecha2: this.state.fecha2,
                    duracion: 'Del ' +
                        moment(fecha1, 'YYYY-MM-DD').format('DD') + ' de ' + moment(fecha1, 'YYYY-MM-DD').format('MMM').toUpperCase() +
                        ' al ' +
                        moment(fecha2, 'YYYY-MM-DD').format('DD') + ' de ' + moment(fecha2, 'YYYY-MM-DD').format('MMM').toUpperCase(),
                    dia: this.state.menuDia[i].dia,
                    nombre_producto: this.state.menuDia[i].producto[j].nombre_producto,
                    precio: this.state.menuDia[i].producto[j].precio,
                    comentario: comentario,
                    publicado: '0'
                }));

                await fetch(this.state.uri + '/api/post/menu/escolar', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: formData
                }).then(res => res.json())
                    .then(responseJson => {
                        this.setState({elIdMenu: responseJson});
                    });
            }
        }
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('Error al guardar menu',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Menú escolar)',
                [{
                    text: 'Entendido',
                    onPress: () => [this.setState({visible: false}), Actions.drawer()]
                }]);
        } else {
            Alert.alert('Menú escolar guardado', 'Se ha guardado el menú escolar con éxito', [
                {text: 'Entendido', onPress: () => [this.setState({visible: false}), Actions.drawerClose()]}
            ])
        }
    }

    //+-+-+-+-+-++-+-+-+-+-+-+-+ Publicar
    async AlertRequestP() {
        if (this.state.menuTitulo === '') {
            Alert.alert('Campo titulo menu vacio', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.fecha1 === '' && this.state.fecha2 === '') {
            Alert.alert('Duracion del menu indefinida', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.menuDia[0].dia === '') {
            Alert.alert('Productos de menu sin definir', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.menuDia[0].producto[0].nombre_producto === '') {
            Alert.alert('Productos de menu sin definir', 'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else if (this.state.menuDia[0].producto[0].precio === '') {
            Alert.alert('Productos de menu sin definir', '' +
                'Asegurese de llenar todos los datos antes de guardar', [
                {text: 'Entendido'}
            ]);
        } else {
            Alert.alert('Publicar menú escolar',
                'Esta a punto de publicar un menú\n¿Seguro que desea continuar?', [
                {text: 'Sí', onPress: () => this.requesTituloMenuP()},{text: 'No'}
            ]);
        }
    }

    async requesTituloMenuP() {
        await this._changeWheelState();
        let formData = new FormData();
        formData.append('new', JSON.stringify({
            titulo: this.state.menuTitulo
        }));
        await fetch(this.state.uri + '/api/crear/menu/escolar', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al guardar menu',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Menú escolar)',
                        [{
                            text: 'Entendido',
                            onPress: () => [this.setState({visible: false}), Actions.drawer()]
                        }]);
                } else {
                    this.setState({elIdMenu: responseJson});
                    this.requestMultipartP(responseJson.id);
                }
            });
    }

    async requestMultipartP(id) {
        let comentario = '';
        if (this.state.elComentario === ''){
            comentario = 'Sin comentarios';
        } else {
            comentario = this.state.elComentario;
        }
        await this._changeWheelState();
        let fecha1 = this.state.fecha1;
        let fecha2 = this.state.fecha2;
        let formData = new FormData();
        for (let i = 0; i < this.state.menuDia.length; ++i) {
            for (let j = 0; j < this.state.menuDia[i].producto.length; ++j) {
                formData.append('new', JSON.stringify({
                    id_menu: id,
                    titulo_menu: this.state.menuTitulo,
                    fecha1: this.state.fecha1,
                    fecha2: this.state.fecha2,
                    duracion: 'Del ' +
                        moment(fecha1, 'YYYY-MM-DD').format('DD') + ' de ' + moment(fecha1, 'YYYY-MM-DD').format('MMM').toUpperCase() +
                        ' al ' +
                        moment(fecha2, 'YYYY-MM-DD').format('DD') + ' de ' + moment(fecha2, 'YYYY-MM-DD').format('MMM').toUpperCase(),
                    dia: this.state.menuDia[i].dia,
                    nombre_producto: this.state.menuDia[i].producto[j].nombre_producto,
                    precio: this.state.menuDia[i].producto[j].precio,
                    comentario: comentario,
                    publicado: '1'
                }));
                await fetch(this.state.uri + '/api/post/menu/escolar', {
                    method: 'POST', headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                        Authorization: 'Bearer ' + this.state.token
                    }, body: formData
                }).then(res => res.json())
                    .then(responseJson => {
                        this.setState({elRequest: responseJson});
                    });
            }
        }
        if (this.state.elRequest.error !== undefined) {
            Alert.alert('Error al publicar menu',
                'Ha ocurrido un error '
                +
                this.state.elRequest.error.status_code
                +
                ' si el error continua pónganse en contacto con soporte'
                +
                '(Menú escolar)',
                [{
                    text: 'Entendido',
                    onPress: () => [Actions.drawer(), this.setState({visible: false})]
                }]);
        }
        await this.notificacionPadres();
    }

    async notificacionPadres() {
        await fetch(
            this.state.uri + '/api/enviar/notificacion/personalizada/Se ha publicado un nuevo menu escolar. Entre para ver detalles', {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: 'Bearer ' + this.state.token
                }
            }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al notificar menu',
                        'Ha ocurrido un error '
                        +
                        responseJson.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Menu escolar)',
                        [{
                            text: 'Entendido',
                            onPress: () => [Actions.drawer(), this.setState({visible: false})]
                        }]);
                } else {
                    Alert.alert('Menu escolar publicado', 'Se le notificara a los padres de familia sobre la publicacion del nuevo menu', [
                        {text: 'Entendido', onPress: () => [Actions.HomePage(), this.setState({visible: false})]}
                    ])
                }
            });
    }

    //+-+-+-+-+--+--+-+-+Date range picker
    async hideDateTimePicker() {
        await this.setState({dia1: false})
    }

    async handleDatePicked(date) {
        let fecha = moment(date).format('YYYY-MM-DD');
        await this.setState({fecha1: fecha});
        await this.hideDateTimePicker();
    }

    async hideDateTimePicker2() {
        await this.setState({dia2: false})
    }

    async handleDatePicked2(date) {
        let fecha = moment(date).format('YYYY-MM-DD');
        await this.setState({fecha2: fecha});
        await this.hideDateTimePicker2();
    }

//+-+-+-+-+-+-+-+-+ Menu escolar titulo
    async titleMenuTrue() {
        this.setState({titleMenu: true});
    }

    async titleMenuFalse() {
        this.setState({titleMenu: false});
    }

    //+-+-+-+-+-+-+-+-+ render menu por dia +-+-+-+-+
    async agregarDias() {
        await this.setState({nombreDia: '', productName: '', precioProduct: ''});
        this.state.menuDia.push({
            dia: '',
            producto: [{'nombre_producto': '', 'precio': ''}]
        });
        this.state.menuDia.forEach(() => {
            this.state.diaMenu.push(false)
        });
        await this.setState({aux: 0});
    }

    async diaMenuTrue(i) {
        this.state.nombreDia = this.state.menuDia[i].dia;
        this.state.diaMenu[i] = true;
        await this.setState({aux: 0});
    }

    async diaMenuFalse(i) {
        this.state.menuDia[i].dia = this.state.nombreDia;
        this.state.diaMenu[i] = false;
        await this.setState({aux: 0});

    }

    async alertDeleteDia(i) {
        Alert.alert('Borrar día', 'Está a punto de borrar un día, ¿seguro que desea continuar?', [
            {text: 'Sí', onPress: () => this.deleteDia(i)},
            {text: 'No'}
        ])
    }

    async deleteDia(i) {
        this.state.menuDia.splice(i, 1);
        await this.setState({aux: 0})
    }

    rndMenuDia(i) {
        return (
            <View
                key={i + 'dia'}
                style={[styles.widthall, styles.cardMat, {
                    backgroundColor: '#f1f1f1',
                    alignItems: 'center'
                }]}
            >
                <View style={[styles.widthall, styles.row, {
                    alignItems: 'flex-start',
                    paddingHorizontal: 8,
                    marginBottom: 5
                }]}>
                    {this.state.diaMenu[i] === false ? (
                        <TouchableOpacity
                            style={[
                                styles.inputPicker,
                                styles.btn2,
                                {
                                    borderRadius: 7,
                                    borderWidth: .5,
                                    paddingVertical: 4,
                                    alignItems: 'flex-start',
                                    backgroundColor: '#b3b3b3',
                                    marginTop: 0
                                }
                            ]}
                            onPress={() => this.diaMenuTrue(i)}
                        >
                            {this.state.menuDia[i].dia !== '' ? (
                                <Text style={[styles.textW, {textAlign: 'left'}]}>{this.state.menuDia[i].dia}</Text>

                            ) : (
                                <Text style={[styles.textW, {textAlign: 'left'}]}>Escriba el día</Text>
                            )}
                        </TouchableOpacity>
                    ) : (
                        <TextInput
                            keyboardType='default'
                            maxLength={200}
                            onChangeText={text => (this.state.nombreDia = text)}
                            placeholder={'Escriba el día'}
                            onEndEditing={() => this.diaMenuFalse(i)}
                            returnKeyType='next'
                            defaultValue={this.state.nombreDia}
                            underlineColorAndroid={'transparent'}
                            style={[
                                styles.inputPicker,
                                styles.btn2,
                                {
                                    borderRadius: 6,
                                    borderWidth: .5,
                                    padding: 4,
                                    backgroundColor: '#fff',
                                    marginTop: 0
                                }
                            ]}
                        />
                    )}
                    <FontAwesome name='trash-o' size={20} color='#000' onPress={() => this.alertDeleteDia(i)}/>
                </View>
                {this.rndProductos(i)}
                <View style={[styles.widthall, {alignItems: 'center', marginVertical: 10}]}>
                    <TouchableOpacity
                        style={[styles.modalBtnMed, styles.btn3, styles.row, {
                            borderColor: this.state.secondColor,
                            marginTop: 3,
                            padding: 0,
                            justifyContent: 'center',
                            height: responsiveHeight(4)
                        }]}
                        onPress={() => this.agregarProducto(i)}
                    >
                        <MaterailCommunityIcons name='food-apple' size={20} color='#FF6633'/>
                        <Text style={{fontSize: responsiveFontSize(1.5), marginLeft: 3}}>Añadir opción</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    rndMenuDias() {
        let btnDias = [];
        for (let i = 0; i < this.state.menuDia.length; ++i) {
            btnDias.push(this.rndMenuDia(i))
        }
        return btnDias
    }

    //+--+-+-+-+-+-+-+-+-+- render producto menu

    async agregarProducto(i) {
        await this.setState({productName: '', precioProduct: ''});
        this.state.menuDia[i].producto.push({
            nombre_producto: '',
            precio: ''
        });
        this.state.menuDia[i].producto.forEach(() => {
            this.state.productoMenu.push(false)
        });
        await this.setState({aux: 0});
    }

    async deleteProduct(i, j) {
        this.state.menuDia[i].producto.splice(j, 1);
        await this.setState({aux: 0});
    }

    async productoTrue(i, j) {
        this.state.productName = this.state.menuDia[i].producto[j].nombre_producto;
        this.state.precioProduct = this.state.menuDia[i].producto[j].precio;
        this.state.productoMenu[i + '' + j] = true;
        await this.setState({aux: 0});
    }

    async productoFalse(i, j) {
        this.state.menuDia[i].producto[j].nombre_producto = this.state.productName;
        this.state.menuDia[i].producto[j].precio = this.state.precioProduct;
        this.state.productoMenu[i + '' + j] = false;
        await this.setState({aux: 0});
    }

    rndProducto(i, j) {
        return (
            <View
                key={j + 'producto'}
                style={[styles.row, styles.btn1, {
                    alignItems: 'center',
                    marginVertical: 3,
                    backgroundColor: 'transparent'
                }]}
            >
                {this.state.productoMenu[i + '' + j] === false ? (
                    <TouchableOpacity
                        style={[styles.inputPicker, styles.btn2_3, styles.row, {
                            padding: 4, marginTop: 0, backgroundColor: '#b3b3b3'
                        }]}
                        onPress={() => this.productoTrue(i, j)}
                    >
                        {this.state.menuDia[i].producto[j].nombre_producto === '' ? (
                            <Text style={[{fontSize: responsiveFontSize(1.5)}]}>
                                Nombre del producto
                            </Text>
                        ) : (
                            <Text style={[{fontSize: responsiveFontSize(1.5)}]}>
                                {this.state.menuDia[i].producto[j].nombre_producto}
                            </Text>
                        )}
                    </TouchableOpacity>
                ) : (
                    <TextInput
                        keyboardType='default'
                        maxLength={200}
                        onChangeText={text => (this.state.productName = text)}
                        placeholder={'Escriba el producto'}
                        onEndEditing={() => this.productoFalse(i, j)}
                        returnKeyType='next'
                        defaultValue={this.state.productName}
                        underlineColorAndroid={'transparent'}
                        style={[styles.inputPicker, styles.btn2_3, styles.row, {
                            padding: 4, marginTop: 0, backgroundColor: '#fff'
                        }]}
                    />
                )}
                {this.state.productoMenu[i + '' + j] === false ? (
                    <TouchableOpacity
                        style={[styles.inputPicker, styles.btn4,
                            {padding: 4, marginTop: 0, backgroundColor: '#b3b3b3'}
                        ]}
                        onPress={() => this.productoTrue(i, j)}
                    >
                        {this.state.menuDia[i].producto[j].precio === '' ? (
                            <Text style={[{fontSize: responsiveFontSize(1.5)}]}>
                                Precio
                            </Text>
                        ) : (
                            <Text style={[{fontSize: responsiveFontSize(1.5)}]}>
                                ${this.state.menuDia[i].producto[j].precio}
                            </Text>
                        )}
                    </TouchableOpacity>
                ) : (
                    <TextInput
                        keyboardType='numeric'
                        maxLength={5}
                        onChangeText={text => (this.state.precioProduct = text)}
                        placeholder={'Precio'}
                        onEndEditing={() => this.productoFalse(i, j)}
                        returnKeyType='next'
                        defaultValue={this.state.precioProduct}
                        underlineColorAndroid={'transparent'}
                        style={[styles.inputPicker, styles.btn4,
                            {padding: 4, marginTop: 0, backgroundColor: '#fff', textAlign: 'center'}
                        ]}
                    />
                )}
                <Entypo
                    name='circle-with-minus'
                    style={{marginTop: 0}}
                    onPress={() => this.deleteProduct(i, j)}
                    size={20}
                    color='red'
                />
            </View>
        );
    }

    rndProductos(i) {
        let btnProducto = [];
        for (let j = 0; j < this.state.menuDia[i].producto.length; ++j) {
            btnProducto.push(this.rndProducto(i, j))
        }
        return btnProducto;
    }

    rndCrearMenu() {
        return (
            <View
                style={styles.container}>
                {this.state.titleMenu === false ? (
                    <TouchableOpacity style={{marginTop: 10}} onPress={() => this.titleMenuTrue()}>
                        {this.state.menuTitulo === '' ? (
                            <Text
                                style={[
                                    styles.main_title,
                                    styles.btn1,
                                    {
                                        color: this.state.thirdColor,
                                        textAlign: 'center',
                                        fontSize: responsiveFontSize(3),
                                        paddingTop: 0,
                                        marginTop: 0
                                    }
                                ]}
                            >
                                Título de menú
                            </Text>
                        ) : (
                            <Text
                                style={[
                                    styles.main_title,
                                    styles.btn1,
                                    {
                                        color: this.state.thirdColor,
                                        textAlign: 'center',
                                        fontSize: responsiveFontSize(3),
                                        paddingTop: 0,
                                        marginTop: 0
                                    }
                                ]}
                            >
                                {this.state.menuTitulo}
                            </Text>
                        )}
                    </TouchableOpacity>
                ) : (
                    <TextInput
                        keyboardType='default'
                        maxLength={200}
                        onChangeText={text => (this.state.menuTitulo = text)}
                        placeholder={'Escriba el título del menú'}
                        onEndEditing={() => this.titleMenuFalse()}
                        returnKeyType='next'
                        defaultValue={this.state.menuTitulo}
                        underlineColorAndroid={'transparent'}
                        style={[
                            styles.main_title, styles.btn1,
                            {
                                marginTop: 10,
                                paddingTop: 0,
                                textAlign: 'center',
                                color: this.state.thirdColor,
                                fontSize: responsiveFontSize(3)
                            }
                        ]}
                    />
                )}
                <View style={[styles.row, styles.widthall, {
                    justifyContent: 'center',
                    marginTop: 10,
                    marginBottom: 15,
                    padding: 3
                }]}>

                    <View style={[styles.row, {width: responsiveWidth(38)}]}>
                        <Text styles={{textAlign: 'auto'}}>del</Text>
                        <TouchableOpacity
                            style={[styles.modalBtnMed, styles.btn3, {
                                borderColor: this.state.secondColor,
                                marginTop: 3,
                                justifyContent: 'center',
                                height: responsiveHeight(4)
                            }]}
                            onPress={() => this.setState({dia1: true})}
                        >
                            {this.state.fecha1 === '' ? (
                                <Text style={[{textAlign: 'center', fontSize: responsiveFontSize(1.5)}]}>
                                    Fecha inicial
                                </Text>
                            ) : (
                                <Text style={[{
                                    textAlign: 'center',
                                    fontSize: responsiveFontSize(1.5)
                                }]}>{moment(this.state.fecha1, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Text>
                            )}
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.row, {width: responsiveWidth(36.5), marginLeft: 14}]}>
                        <Text styles={{textAlign: 'center'}}>al</Text>
                        <TouchableOpacity
                            style={[styles.modalBtnMed, styles.btn3, {
                                borderColor: this.state.secondColor,
                                marginTop: 3,
                                justifyContent: 'center',
                                height: responsiveHeight(4)
                            }]}
                            onPress={() => this.setState({dia2: true})}
                        >
                            {this.state.fecha2 === '' ? (
                                <Text style={[{textAlign: 'center', fontSize: responsiveFontSize(1.5)}]}>
                                    Fecha final
                                </Text>
                            ) : (
                                <Text style={[{
                                    textAlign: 'center',
                                    fontSize: responsiveFontSize(1.5)
                                }]}>{moment(this.state.fecha2, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Text>
                            )}
                        </TouchableOpacity>
                    </View>
                    <DateTimePicker
                        locale={'es'}
                        isVisible={this.state.dia1}
                        onConfirm={(date) => this.handleDatePicked(date)}
                        onCancel={() => this.hideDateTimePicker()}
                        titleIOS={'Seleccione una fecha'}
                        confirmTextIOS={'Seleccionar'}
                        cancelTextIOS={'Cancelar'}
                    />
                    <DateTimePicker
                        locale={'es'}
                        isVisible={this.state.dia2}
                        onConfirm={(date) => this.handleDatePicked2(date)}
                        onCancel={() => this.hideDateTimePicker2()}
                        titleIOS={'Seleccione una fecha'}
                        confirmTextIOS={'Seleccionar'}
                        cancelTextIOS={'Cancelar'}
                    />
                </View>
                <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 10}}>
                    {this.rndMenuDias()}
                    <View style={[styles.widthall, {alignItems: 'center'}]}>
                        <TouchableOpacity
                            style={[styles.mediumBtn, styles.row, {
                                borderColor: this.state.secondColor,
                                marginVertical: 20,
                                backgroundColor: '#fff',
                                padding: 0,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]}
                            onPress={() => this.agregarDias()}
                        >
                            <Entypo name='circle-with-plus' style={{marginTop: 3}} size={25} color='green'/>
                            <Text style={[styles.textW, {marginLeft: 5}]}>Añadir otro día</Text>
                        </TouchableOpacity>
                        <Text style={[styles.widthall, {textAlign: 'left'}]}>
                            Comentarios adicionales
                        </Text>
                        <TextInput
                            keyboardType='default'
                            maxLength={250}
                            multiline={true}
                            onChangeText={text => (this.state.elComentario = text)}
                            placeholder={'Escriba el comentarios'}
                            onEndEditing={() => this.titleMenuFalse()}
                            returnKeyType='next'
                            defaultValue={this.state.elComentario}
                            underlineColorAndroid={'transparent'}
                            style={[
                                styles.inputPicker,
                                {
                                    marginTop: 3,
                                    padding: 8,
                                    height: responsiveHeight(13),
                                    borderColor: this.state.secondColor
                                }
                            ]}
                        />
                        <View style={[styles.row, styles.widthall, {marginTop: 15}]}>
                            <TouchableOpacity
                                style={[styles.mediumBtn, {
                                    borderColor: this.state.secondColor,
                                    backgroundColor: this.state.secondColor
                                }]}
                                onPress={() => this.AlertRequestG()}
                            >
                                <Text style={styles.textButton}>Guardar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.mediumBtn, {
                                    borderColor: this.state.secondColor,
                                    backgroundColor: this.state.secondColor
                                }]}
                                onPress={() => this.AlertRequestP()}
                            >
                                <Text style={styles.textButton}>Publicar ahora</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({botonSelected: itemSelected, indexSelected: indexSelected});
    }

    render() {
        return (
            <KeyboardAvoidingView
                keyboardVerticalOffset={Platform.OS === 'ios' ? 10 : 0}
                behavior={Platform.OS === 'ios' ? 'position' : null}
                style={styles.container} enabled>
				<Spinner visible={this.state.visible} textContent='Guardando...'/>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle='light-content'
                />
                <MultiBotonRow
                    itemBtns={['Crear nuevo', 'Ver historial']}
                    onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
                    cantidad={2}
                />
                {this.state.indexSelected === 0 ? this.rndCrearMenu() : (<HistorialMenu/>)}
            </KeyboardAvoidingView>
        );
    }
}
