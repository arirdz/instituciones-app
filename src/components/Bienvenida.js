import React from 'react';
import {
    Alert,
    AsyncStorage,
    BackHandler,
    Image,
    ImageBackground,
    Keyboard,
    Linking,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles, {secondColor,} from './styles';
import OneSignal from 'react-native-onesignal';
import {MediaQuery} from 'react-native-responsive';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import ModalSelector from 'react-native-modal-selector';
import {ifIphoneX} from "react-native-iphone-x-helper";

let moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class Bienvenida extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            textInputValue: '',
            uri: 'https://controlescolar.pro',
            cososPicker: ['Seleccione Escuela'],
            configData: [],
            itemBtn: '',
            selectedIndexBtn: 0,
            estados: [],
            escuelas: [''],
            aux: 0,
            email: ''
        };
    }

    handleEmail = text => {
        this.setState({email: text});
    };

    handlePassword = text => {
        this.setState({password: text});
    };

    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible, dismiss: 0
        });
    };

    async getUserData() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        await fetch(uri + '/api/user/data', {
            method: 'GET', headers: {Authorization: 'Bearer ' + token}
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error ' +
                        // responseJson.error.status_code +
                        ' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({datos: responseJson});
                }
            });
        {
            this.state.datos.passwordUpdated !== '0' || this.state.datos.passwordUpadated !== 0
                ? await Actions.replace('drawer', {key: Math.random()})
                : await Actions.PwdRest()
        }
    }

    _fetchLoginData = (mail, pass, spinner) => {
        if (mail === '' || pass === '') {
            Alert.alert('Campos vacíos', 'Es necesario llenar los campos de usuario y contraseña para realizar esta operación.');
        } else {
            spinner._changeWheelState();
            return fetch(this.state.uri + '/api/auth/login', {
                method: 'POST', headers: {
                    Accept: 'application/json', 'Content-Type': 'application/json'
                }, body: JSON.stringify({
                    email: mail, password: pass
                })
            })
                .then(response => {
                    return response.json()
                })
                .then(responseJson => {
                    if (responseJson.token === undefined) {
                        switch (responseJson.error.status_code) {
                            case 422:
                                Alert.alert('Formato inválido', 'Es necesario poner un email válido.', [{
                                    text: 'OK', onPress: () => spinner._changeWheelState()
                                }]);
                                break;
                            case undefined:
                                Alert.alert('Usuario o contraseña Incorrectos', 'Ingresa los datos correctos', [{
                                    text: 'OK', onPress: () => spinner._changeWheelState()
                                }]);
                                break;
                            case 403:
                                Alert.alert('Usuario o contraseña incorrectos', 'Si el error persiste solicite ayuda.', [{
                                    text: 'OK', onPress: () => spinner._changeWheelState()
                                }]);
                                break;
                            case 500:
                                Alert.alert('Error interno', 'Por el momento no se puede procesar su solicitud, intente más tarde.', [{
                                    text: 'OK', onPress: () => spinner._changeWheelState()
                                }]);
                                break;
                            default:
                                alert('Error:' + responseJson.error.status_code, 'Solicite ayuda por favor', [{
                                    text: 'OK', onPress: () => spinner._changeWheelState()
                                }]);
                        }
                    } else {
                        // console.log('debería funcionar hasta aquí')
                        this.saveItem('token', responseJson.token);
                        this.saveItem('id', responseJson.id.toString());
                        this.saveItem('role', responseJson.role);
                        this.saveItem('puesto', responseJson.puesto);
                        this.saveItem('grado', responseJson.grado);
                        this.saveItem('grupo', responseJson.grupo);
                        this.saveItem('name', responseJson.name);
                        this.getUserData();
                    }
                })
                .catch(error => {
                    Alert.alert('ERROR DE CONEXIÓN', 'Revise su conexión a internet. Si el problema persiste solicite ayuda.', [{
                        text: 'OK', onPress: () => spinner._changeWheelState()
                    }]);
                });
        }

    };

    async saveItem(item, selectedValue) {
        try {
            await AsyncStorage.setItem(item, selectedValue);
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    async componentWillMount() {
        OneSignal.init('02510db7-3e82-4308-a15d-f81486901635');
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
        Linking.addEventListener('url', this.handleOpenURL);
        OneSignal.inFocusDisplaying(2);
        OneSignal.configure();
    }

    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleOpenURL);
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('ids', this.onIds);
        BackHandler.removeEventListener('hardwareBackPress');
    }

    async onOpened(openResult) {
        let tutor = await AsyncStorage.getItem('idTutor');
        let id_tutor = openResult.notification.payload.additionalData.id_tutor;
        let id_school = openResult.notification.payload.additionalData.id_escuela;
        if (openResult.notification.payload.launchURL === 'ce://aspirantesTutor') {
            try {
                if (tutor !== null) {
                    Actions.push('aspirantesTutor', {aspirantes: id_tutor});
                } else {
                    Actions.push('loginPreinscrip', {idschool: id_school});
                }
            } catch (error) {
                console.error('AsyncStorage error: ' + error.message);
            }
        }
    }


    async getEscuelas() {
        await this.setState({
            textInputValue: '',
        });
        let escuela = await fetch('https://gestion-dev.controlescolar.pro/api/get/url/' + this.state.email);
        let escuelas = await escuela.json();
        await this.setState({escuelas: escuelas});
        if (this.state.escuelas.length === 1) {
            await this.setState({
                textInputValue: this.state.escuelas[0].nombre,
                uri: this.state.escuelas[0].url,
                ide: this.state.escuelas[0].id,
                fase: this.state.escuelas[0].plan
            });

            await this.globalConfigs();
            await this.saveData(this.state.escuelas[0].url, this.state.escuelas[0].id);
            await this.refs.password.focus()
        } else if (this.state.escuelas.length === 0) {
            await this.refs.email.focus();
            Alert.alert('Atención', 'Correo no encontrado', [{
                text: 'OK'
            }]);
        }
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            uri: option.url,
            ide: option.aydii,
            fase: option.fase
        });

        await this.globalConfigs();
        await this.saveData(option.url, option.aydii);
        await this.refs.password.focus()
    }

    async globalConfigs() {
        let keys = await AsyncStorage.getAllKeys();
        let getConfig = await fetch(this.state.uri + '/api/global/config/values');
        let configSave = await getConfig.json();
        await this.setState({configData: configSave});
        await this.setState({aux: 0});
    }


    async saveData(uri, ide) {
        await this.setState({aux: 0});
        try {
            let ide2 = await ide.toString();
            await AsyncStorage.setItem('uri', uri);
            await AsyncStorage.setItem('schoolId', ide2);
            let mc1 = await this.state.configData[0].second_option.toString();
            let sc1 = await this.state.configData[1].second_option.toString();
            let tc1 = await this.state.configData[2].second_option.toString();
            let fc1 = await this.state.configData[3].second_option.toString();
            await AsyncStorage.setItem('mainColor', mc1);
            await AsyncStorage.setItem('secondColor', sc1);
            await AsyncStorage.setItem('thirdColor', tc1);
            await AsyncStorage.setItem('fourthColor', fc1);
            await AsyncStorage.setItem('fase', this.state.fase);
            await this.setState({aux: 0});
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    render() {

        let escuelas = this.state.escuelas.map((item, i) => {
            return {
                key: i,
                url: item.url,
                label: item.nombre,
                aydii: item.id,
                fase: item.plan
            };
        });
        return (
            <ImageBackground
                style={Styles.container}
                source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
            >
                <StatusBar
                    backgroundColor='#126775'
                    barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
                />
                <MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
                    {Platform.OS === 'ios' ? <View style={[Styles.logoContainer, {marginTop: 60}]}>
                        <Image
                            style={Styles.logo}
                            source={{
                                uri: 'https://controlescolar.pro/images/logotipo.png'
                            }}
                        />
                    </View> : <View style={[Styles.logoContainer, {marginTop: 85}]}>
                        <Image
                            style={Styles.logo}
                            source={{
                                uri: 'https://controlescolar.pro/images/logotipo.png'
                            }}
                        />
                    </View>}
                </MediaQuery>
                <MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
                    {Platform.OS === 'ios' ? <View
                        style={[Styles.logoContainer, {...ifIphoneX({marginTop: responsiveHeight(12)}, {marginTop: responsiveHeight(8)})}]}>
                        <Image
                            style={Styles.logo}
                            source={{
                                uri: 'https://controlescolar.pro/images/logotipo.png'
                            }}
                        />
                    </View> : <View style={[Styles.logoContainer, {marginTop: responsiveHeight(10)}]}>
                        <Image
                            style={Styles.logo}
                            source={{
                                uri: 'https://controlescolar.pro/images/logotipo.png'
                            }}
                        />
                    </View>}
                </MediaQuery>
                <Text style={[styles.jelow, {
                    color: this.state.mainColor,
                    marginBottom: 5
                }]}>
                    ¡Bienvenido!
                </Text>
                <Spinner visible={this.state.visible} textContent='Verificando...'/>
                {this.state.escuelas.length > 1 ?
                    <ModalSelector
                        data={escuelas}
                        selectStyle={[
                            [styles.textInputV3, {
                                alignItems: 'center',
                                borderColor: secondColor,
                                backgroundColor: 'transparent'
                            }],
                        ]}
                        selectTextStyle={{fontSize: responsiveFontSize(1.7)}}
                        cancelText='Cancelar'
                        optionTextStyle={{color: secondColor, fontSize: responsiveFontSize(1.7)}}
                        initValue='Seleccione la Escuela'
                        onChange={option => this.onChange(option)}
                    /> : null}
                {Platform.OS === 'ios' ?
                    (<View style={[styles.textInputV2, styles.row, {
                        borderColor: this.state.escuelas.length === 0 ? 'red' : secondColor,
                        backgroundColor: 'transparent'
                    }]}>

                        <TextInput
                            ref='email'
                            placeholder='Email'
                            onChangeText={this.handleEmail}
                            keyboardType='email-address'
                            returnKeyType='next'
                            underlineColorAndroid='transparent'
                            autoCapitalize='none'
                            autoCorrect={false}
                            defaultValue={this.state.email}
                            style={[styles.inputDismmis, {backgroundColor: 'transparent'}]}
                            onSubmitEditing={() => this.getEscuelas()}
                        />
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <MaterialCommunityIcons color={'grey'} name='keyboard-close' size={25}/>
                        </TouchableWithoutFeedback>

                    </View>) : (<TextInput
                        ref='email'
                        placeholder='Email'
                        onChangeText={this.handleEmail}
                        keyboardType='email-address'
                        returnKeyType='next'
                        defaultValue={this.state.email}
                        underlineColorAndroid='transparent'
                        autoCapitalize='none'
                        autoCorrect={false}
                        style={[styles.textInput, {
                            borderColor: secondColor,
                            backgroundColor: 'transparent'
                        }]}
                        onSubmitEditing={() => this.getEscuelas()}
                    />)}

                {this.state.escuelas.length === 1 && this.state.escuelas[0] !== '' && this.state.escuelas.length !== 0 ?
                    <TextInput
                        ref='password'
                        placeholder='Contraseña'
                        secureTextEntry={true}
                        underlineColorAndroid='transparent'
                        onChangeText={this.handlePassword}
                        returnKeyType='go'
                        autoCapitalize='none'
                        style={[styles.textInput, {
                            borderColor: secondColor,
                            backgroundColor: 'transparent'
                        }]}
                        onSubmitEditing={() => this._fetchLoginData(this.state.email, this.state.password, this)}
                    /> : this.state.textInputValue !== '' && this.state.escuelas.length !== 0 ?
                        <TextInput
                            ref='password'
                            placeholder='Contraseña'
                            secureTextEntry={true}
                            underlineColorAndroid='transparent'
                            onChangeText={this.handlePassword}
                            returnKeyType='go'
                            autoCapitalize='none'
                            style={[styles.textInput, {
                                borderColor: secondColor,
                                backgroundColor: 'transparent'
                            }]}
                            onSubmitEditing={() => this._fetchLoginData(this.state.email, this.state.password, this)}
                        /> : null}


                {this.state.escuelas.length === 1 && this.state.escuelas[0] !== '' ?
                    <TouchableOpacity
                        onPress={() => this._fetchLoginData(this.state.email, this.state.password, this)}
                        style={[styles.bigButton, {borderColor: secondColor}]}
                    >
                        <Text style={styles.textButton}>Iniciar Sesión</Text>
                    </TouchableOpacity> : this.state.textInputValue !== '' ?
                        <TouchableOpacity
                            onPress={() => this._fetchLoginData(this.state.email, this.state.password, this)}
                            style={[styles.bigButton, {borderColor: secondColor}]}
                        >
                            <Text style={styles.textButton}>Iniciar Sesión</Text>
                        </TouchableOpacity> : <TouchableOpacity
                            onPress={() => this.getEscuelas()}
                            style={[styles.bigButton, {borderColor: secondColor}]}
                        >
                            <Text style={styles.textButton}>Continuar</Text>
                        </TouchableOpacity>}
                <View style={[styles.row, {marginTop: responsiveHeight(2)}]}>
                    <TouchableOpacity style={{width: responsiveWidth(52), paddingVertical: responsiveHeight(1)}}
                                      onPress={() => this.openModal()}>
                        <Text style={[styles.noEscuela, {
                            textAlign: 'center',
                            fontSize: responsiveFontSize(1.5)
                        }]}>¿Olvidó su contraseña?</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.row]}>
                    <TouchableOpacity style={{width: responsiveWidth(52), paddingVertical: responsiveHeight(1)}}
                                      onPress={() => Actions.preinscrip()}>
                        <Text style={[styles.noEscuela, {
                            textAlign: 'center',
                            fontSize: responsiveFontSize(1.5)
                        }]}>Iniciar preinscripción</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: responsiveWidth(52), paddingVertical: responsiveHeight(1)}}
                                      onPress={() => Actions.loginPreinscrip()}>
                        <Text style={[styles.noEscuela, {
                            textAlign: 'center',
                            fontSize: responsiveFontSize(1.5)
                        }]}>Continuar preinscripción</Text>
                    </TouchableOpacity>
                </View>
                {/*<Modal*/}
                {/*isVisible={this.state.isModalComent}*/}
                {/*backdropOpacity={0.5}*/}
                {/*animationIn={'bounceIn'}*/}
                {/*animationOut={'bounceOut'}*/}
                {/*animationInTiming={1000}*/}
                {/*animationOutTiming={1000}*/}
                {/*>*/}
                {/*<KeyboardAvoidingView behavior='position'>*/}
                {/*<View style={{height: responsiveHeight(40), alignItems: 'center'}}>*/}
                {/*<View style={[styles.container, {*/}
                {/*width: responsiveWidth(94),*/}
                {/*borderRadius: 10,*/}
                {/*alignItems: 'center'*/}
                {/*}]}>*/}
                {/*<Text*/}
                {/*style={[styles.main_title, styles.modalWidth, {*/}
                {/*color: this.state.thirdColor,*/}
                {/*textAlign: 'center'*/}
                {/*}]}>*/}
                {/*Ingrese el correo con el que se dio de alta*/}
                {/*</Text>*/}

                {/*<View style={{height: responsiveHeight(4)}}/>*/}
                {/*<TextInput*/}
                {/*ref='correo'*/}
                {/*placeholder='Correo'*/}
                {/*underlineColorAndroid='transparent'*/}
                {/*onChangeText={this.handleCorreo}*/}
                {/*returnKeyType='go'*/}
                {/*autoCapitalize='none'*/}
                {/*style={[styles.textInput, styles.modalWidth, {borderColor: this.state.secondColor}]}*/}
                {/*/>*/}
                {/*<TouchableOpacity*/}
                {/*onPress={() => this.requestCorreo(this.state.correo)}*/}
                {/*style={[styles.bigButtonLogin, styles.modalWidth, {*/}
                {/*backgroundColor: this.state.secondColor,*/}
                {/*borderColor: this.state.secondColor*/}
                {/*}]}>*/}
                {/*<Text style={styles.textButton}>Restablecer contraseña</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity onPress={() => this.closeModal()}*/}
                {/*style={[styles.textButton, {marginTop: responsiveHeight(2)}]}><Text>Cancelar</Text></TouchableOpacity>*/}
                {/*</View>*/}
                {/*</View>*/}
                {/*</KeyboardAvoidingView>*/}
                {/*</Modal>*/}
                <View style={{
                    position: 'relative',
                    bottom: 0,
                    height: Platform.OS === 'ios' ? 55 : 40,
                    justifyContent: 'flex-end'
                }}>
                    <Text style={styles.footer}>Todos los derechos reservados.</Text>
                    <Text style={styles.footersito}>
                        Control Escolar Profesional &copy;2017 - {moment().year()}
                    </Text>
                </View>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                />
            </ImageBackground>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1
    },
    logoContainer: {
        marginTop: 200
    },
    inputPicker: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        padding: 10,
        marginTop: 10,
        width: responsiveWidth(94),
        alignItems: 'center',
        justifyContent: 'center'
    },
    textButton: {
        color: 'white',
        fontSize: responsiveFontSize(2),
        textAlign: 'center',
        fontWeight: '700'
    },
    logo: {
        height: responsiveHeight(20),
        marginBottom: 30,
        resizeMode: 'contain',
        width: responsiveWidth(70)
    },
    marTop: {
        marginTop: responsiveHeight(10)
    },
    marTopp: {
        marginTop: responsiveHeight(5)
    }
    , marTop2: {
        marginTop: responsiveHeight(2)
    }
});
