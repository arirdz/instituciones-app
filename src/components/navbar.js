import {Alert, AsyncStorage, Image, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Feather from 'react-native-vector-icons/Feather';

const styles = StyleSheet.create({
	container: {
		...ifIphoneX(
			{
				paddingTop: Platform.OS === 'ios' ? responsiveHeight(2.5) : 4
			},
			{
				paddingTop: Platform.OS === 'ios' ? 4 : 0
			}
		)
	},
	navTitle: {
		color: '#e5e4da',
		width: responsiveWidth(90),
		paddingVertical: Platform.OS === 'ios' ? 4 : 4,
		...ifIphoneX(
			{
				marginTop: Platform.OS === 'ios' ? -25 : 0
			},
			{
				marginTop: Platform.OS === 'ios' ? 2 : -3
			}
		),
		fontSize: responsiveFontSize(3.5),
		fontWeight: '800',
		marginLeft: responsiveWidth(4),
		textAlign: 'left'
	},
	navTitleT: {
		color: '#e5e4da',
		width: responsiveWidth(90),
		height: Platform.OS === 'ios' ? responsiveHeight(5) : responsiveHeight(5),
		...ifIphoneX(
			{
				marginTop: Platform.OS === 'ios' ? -15 : 0
			},
			{
				marginTop: Platform.OS === 'ios' ? -3 : 0
			}
		),
		fontSize: responsiveFontSize(3),
		fontWeight: '800',
		marginLeft: responsiveWidth(4),
		textAlign: 'left'
	},
	navTitle1: {
		color: '#e5e4da',
		width: responsiveWidth(90),
		height: Platform.OS === 'ios' ? responsiveHeight(7) : responsiveHeight(7),
		...ifIphoneX(
			{
				marginTop: Platform.OS === 'ios' ? -15 : 0
			},
			{
				marginTop: Platform.OS === 'ios' ? 24 : 0
			}
		),
		fontSize: responsiveFontSize(3.5),
		fontWeight: '800',
		marginLeft: responsiveWidth(1),
		textAlign: 'left'
	},
	navBarItem: {
		height: 0
	}
});

export default class CustomNavBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			escuelasDatos: [],
			tutor: ''
		};
	}

	static async userLogout() {
		try {
			await AsyncStorage.clear();
			Actions.replace('Bienvenida', {type: 'reset'});
		} catch (error) {
			console.log('Ocurrió un error ' + error);
		}
	}

	async getSchoolData() {
		let scid = await AsyncStorage.getItem('schoolId');
		if (scid !== undefined) {
			let schoolData = await fetch(
				'https://controlescolar.pro/api/get/escuelas/datos/' + scid
			);
			let scData = await schoolData.json();
			this.setState({escuelasDatos: scData[0]});
			await AsyncStorage.setItem('fase', scData[0].plan);
		}
	}

	async componentWillMount() {
		await this.getSchoolData();
		let token = await AsyncStorage.getItem('token');
		await this.setState({token: token});
	}

	back() {
		return (
			<Feather name='chevron-left' size={40} color='#fff'
					 style={{
						 marginTop: responsiveHeight(2.7),
						 marginLeft: responsiveWidth(-2.2)
					 }}
					 onPress={() => Actions.push('HomePage')}/>
		);
	}

	async alertBack() {
		Alert.alert('¡Alerta!',
			'Si regresa se perderá el todo el avance de preinscripción teniendo que capturar todo de nuevo, ¿Seguro que desea salir?', [
				{text: 'Sí', onPress: () => Actions.pop()}, {text: 'No'}
			]);
	}

	async logOutAspirantes() {
		Alert.alert('Cerrar sesión', 'Está a punto de serrar la sesión de preinscripción\n¿seguro que desea salir?', [
			{text: 'Sí', onPress: () => CustomNavBar.userLogout()}, {text: 'No'}
		])
	}

	async backRecibo() {
		if (this.state.token === null) {
			Actions.replace('aspirantesTutor', {key: Math.random()})
		} else {
			Actions.replace('aspirantePendiente', {key: Math.random()})
		}
	}

	backcito() {
		return (<View>
				<MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
					{Actions.currentScene === 'preinscrip' || Actions.currentScene === 'newAsp' ?
						<Feather name='chevron-left' size={responsiveHeight(5)} color='#fff'
								 style={{
									 marginTop: responsiveHeight(2.7),
									 marginLeft: responsiveWidth(-2.2)
								 }}
								 onPress={() => this.alertBack()}/>
						: Actions.currentScene === 'reciboTienda' || Actions.currentScene === 'reciboTransferencia' ?
							<Feather name='chevron-left' size={responsiveHeight(5)} color='#fff'
									 style={{
										 marginTop: responsiveHeight(2.7),
										 marginLeft: responsiveWidth(-2.2)
									 }}
									 onPress={() => this.backRecibo()}/>
							: Actions.currentScene === 'aspirantesTutor' ?
								<Feather name='chevron-left' size={responsiveHeight(5)} color='#fff'
										 style={{marginTop: 18, padding: 0}}
										 onPress={() => this.logOutAspirantes()}/> :
								<Feather name='chevron-left'
										 size={responsiveHeight(5)} color='#fff'
										 style={{
											 marginTop: responsiveHeight(2.7),
											 marginLeft: responsiveWidth(-2.2)
										 }}
										 onPress={() => Actions.pop()}/>}
				</MediaQuery>
				<MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
					{Actions.currentScene === 'preinscrip' || Actions.currentScene === 'newAsp' ?
						<Feather name='chevron-left' size={responsiveHeight(5)} color='#fff'
								 style={{
									 marginTop: responsiveHeight(2.7),
									 marginLeft: responsiveWidth(-2.2)
								 }}
								 onPress={() => this.alertBack()}/>
						: Actions.currentScene === 'reciboTienda' || Actions.currentScene === 'reciboTransferencia' ?
							<Feather name='chevron-left' size={responsiveHeight(5)} color='#fff'
									 style={{
										 marginTop: responsiveHeight(2.7),
										 marginLeft: responsiveWidth(-2.2)
									 }}
									 onPress={() => this.backRecibo()}/>
							: Actions.currentScene === 'aspirantesTutor' ?
								<Feather name='chevron-left' size={responsiveHeight(5)} color='#fff'
										 style={{marginTop: 18, padding: 0, width: responsiveWidth(20)}}
										 onPress={() => this.logOutAspirantes()}/> :
								<Feather name='chevron-left' size={responsiveHeight(5)} color='#fff'
										 style={{
											 marginTop: responsiveHeight(2.7),
											 marginLeft: responsiveWidth(-2.2)
										 }}
										 onPress={() => Actions.pop()}/>}
				</MediaQuery>
			</View>

		);
	}

	_renderLeft() {
		if (Actions.currentScene === 'HomePage') {
			return (<View>
					<MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
						{Actions.currentScene !== 'preinscrip' || Actions.currentScene !== 'SolicitudEsc' || Platform.OS === 'ios' ?
							<Feather name='menu' size={responsiveHeight(5)} color='#fff'
									 style={{
										 marginTop: responsiveHeight(2.7),
										 paddingLeft: responsiveWidth(3.2)
									 }}
									 onPress={() => Actions.drawerOpen()}/> : null}
					</MediaQuery>
					<MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
						{Actions.currentScene !== 'preinscrip' || Actions.currentScene !== 'SolicitudEsc' || Platform.OS === 'ios' ?
							<Feather name='menu' size={responsiveHeight(5)} color='#fff'
									 style={{
										 marginTop: responsiveHeight(2.7),
										 paddingLeft: responsiveWidth(3.2)
									 }}
									 onPress={() => Actions.drawerOpen()}/> : null}
					</MediaQuery>
				</View>

			);
		} else {
			return (
				<View
					style={{
						flexDirection: 'row',
						width: responsiveWidth(94),
						marginLeft: 12,
						justifyContent: 'space-between'
					}}>
					{Actions.currentScene === 'revisarTarea' ||
					Actions.currentScene === 'editarTarea' ||
					Actions.currentScene === 'acumuladosTareas'
						? this.back()
						: this.backcito()}
					{Actions.currentScene !== 'preinscrip' &&
					Actions.currentScene !== 'loginPreinscrip' &&
					Actions.currentScene !== 'citaPreinscrip' &&
					Actions.currentScene !== 'pagoPreinscrip' &&
					Actions.currentScene !== 'pagoTarjetaR' &&
					Actions.currentScene !== 'pagoEfectivoR' &&
					Actions.currentScene !== 'diaInmersion' &&
					Actions.currentScene !== 'subirDoctos' &&
					Actions.currentScene !== 'pagoInscrip' &&
					Actions.currentScene !== 'aspirantesTutor' &&
					Actions.currentScene !== 'SolicitudEsc' &&
					Actions.currentScene !== 'InformacionPrivadas' &&
					Actions.currentScene !== 'InformacionPublicas' &&
					Actions.currentScene !== 'Planes' &&
					Actions.currentScene !== 'Comentario' &&
					Platform.OS === 'ios' &&
					Actions.currentScene !== 'recomendar' ?
						<Feather name='menu' size={responsiveHeight(5)} color='#fff'
								 style={{
									 marginTop: responsiveHeight(2.7),
									 paddingLeft: responsiveWidth(3.2)
								 }}
								 onPress={() => Actions.drawerOpen()}/> : null}
				</View>
			);
		}
	}

	_renderMiddle() {
		return (
			<View>
				<MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
					<View>
						<Text
							numberOfLines={1}
							style={[
								styles.navTitleT,
								{
									color: '#e6e6e6'
								}
							]}>
							{this.props.title}
						</Text>
					</View>
				</MediaQuery>
				<MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
					<View>
						<View style={styles.container}>
							<Text
								numberOfLines={1}
								style={[
									styles.navTitle,
									{
										color: '#e6e6e6'
									}
								]}>
								{this.props.title}
							</Text>
						</View>
					</View>
				</MediaQuery>

				<MediaQuery maxDeviceWidth={1024} minDeviceHeight={897}>
					<View>
						<View style={styles.container}>
							<Text
								numberOfLines={1}
								style={[
									styles.navTitle,
									{
										color: '#e6e6e6'
									}
								]}>
								{this.props.title}
							</Text>
						</View>
					</View>
				</MediaQuery>
			</View>
		);
	}

	render() {
		let dinamicStyle = {};
		if (Actions.currentScene === 'customNavBar') {
			dinamicStyle = {
				backgroundColor: '#116876',
				...ifIphoneX(
					{
						height: responsiveHeight(15)
					},
					{height: responsiveHeight(15)}
				)
			};
		} else {
			dinamicStyle = {
				backgroundColor: '#116876',
				...ifIphoneX(
					{
						height: responsiveHeight(15)
					},
					{height: responsiveHeight(15)}
				)
			};
		}

		return (
			<View style={[styles.container, dinamicStyle]}>
				{this._renderLeft()}
				{this._renderMiddle()}
			</View>
		);
	}
}
