import React from 'react';
import {Alert, AsyncStorage, Keyboard, KeyboardAvoidingView, Platform, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from './Tstyles';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Communications from 'react-native-communications';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default class sideBarT extends React.Component {
    handlePassword = text => {
        this.setState({password: text});
    };
    handlePasswordconfirmation = text => {
        this.setState({Passwordconfirmation: text});
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            data1: [],
            escuelasDatos: [],
            noCampus: [],
            configData: [],
            nivelEscuela: [],
            elId_Esc: [],
            selectIndxCampus: -1,
            elCampus: '',
            indxSelectLvl: -1,
            elNivel: '',
            aux: 0,
            isModalComent: false,
            elComentario: '',
            password: '',
            Passwordconfirmation: ''
        };
    }

    static async userLogout() {
        try {
            await AsyncStorage.clear();
            Actions.Bienvenida({type: 'reset'});
        } catch (error) {
            console.warn('Ocurrió un error ' + error);
        }
    }

    async requestMultipart(password, Passwordconfirmation) {
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        if (password !== '' && Passwordconfirmation !== '') {
            if (password === Passwordconfirmation) {
                if (password.length >= 6) {
                    try {
                        let formData = new FormData();
                        formData.append('password', JSON.stringify({password: this.state.password}));
                        let request = await fetch(uri + '/api/password/update/first/login', {
                            method: 'POST', headers: {
                                Accept: 'application/json',
                                'Content-Type': 'multipart/form-data',
                                Authorization: 'Bearer ' + token
                            }, body: formData
                        });
                        Alert.alert('Contraseña actualizada', 'Se ha cambiado la contraseña con éxito', [{
                            text: 'Entendido',
                            onPress: () => this.closeModal1()
                        }]);
                    } catch (e) {
                        Alert.alert('Error en el cambio de contraseña', 'Intenta más tarde por favor', [{
                            text: 'Entendido'
                        }]);
                    }
                } else {
                    Alert.alert('Contraseña invalida', 'La contaseña es muy corta', [{
                        text: 'Entendido'
                    }]);
                }
            } else {
                Alert.alert('Contraseñas no coinciden', 'Las contraseñas no coinciden', [{
                    text: 'Entendido'
                }]);
            }
        } else {
            Alert.alert('Datos incompletos', 'Asegúrate de llenar los dos campos', [{
                text: 'Entendido'
            }]);
        }
    }

    async componentWillMount() {
        await this.getURL();
    }

    async alertUserLogOut() {
        Alert.alert(
            'Cerrar sesión', '¿Esta seguro que desea cerrar esta sesión?',
            [
                {text: 'Sí', onPress: sidebar.userLogout},
                {text: 'No'}
            ]
        )
    }

    //+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-Pruebas de cambio de escuela
    async globalConfigs() {
        let getConfig = await fetch(this.state.url + '/api/global/config/values');
        let configSave = await getConfig.json();
        await this.setState({configData: configSave});
    }

    async userChangeEsc(uri, ide) {
        try {
            await AsyncStorage.clear();
            let ide2 = await ide.toString();
            await AsyncStorage.setItem('uri', uri);
            await AsyncStorage.setItem('schoolId', ide2);
            let mc1 = await this.state.configData[0].second_option.toString();
            let sc1 = await this.state.configData[1].second_option.toString();
            let tc1 = await this.state.configData[2].second_option.toString();
            let fc1 = await this.state.configData[3].second_option.toString();
            await AsyncStorage.setItem('mainColor', mc1);
            await AsyncStorage.setItem('secondColor', sc1);
            await AsyncStorage.setItem('thirdColor', tc1);
            await AsyncStorage.setItem('fourthColor', fc1);
            let toMain = Actions.push('Authentication', {email_user: this.state.data1.email});
        } catch (error) {
            console.warn('Ocurrió un error ' + error);
        }

    }


    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        this.setState({
            url: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: await AsyncStorage.getItem('token')
        });
        await this.getSchoolData();
        await this.getUserData();
        await this.getUserData1();
    }

    async getUserData1() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');

        await fetch(uri + '/api/user/data/v2', {
            method: 'GET', headers: {Authorization: 'Bearer ' + token}
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({data1: responseJson});
                }
            });
    }

    async getUserData() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        await fetch(uri + '/api/user/data', {
            method: 'GET', headers: {Authorization: 'Bearer ' + token}
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error ' +
                        // responseJson.error.status_code +
                        ' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    async getSchoolData() {
        let schoolId = await AsyncStorage.getItem('schoolId');
        await fetch('https://controlescolar.pro/api/get/escuelas/datos/' + schoolId, {
            method: 'GET', headers: {Authorization: 'Bearer ' + this.state.token}
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({escuelasDatos: responseJson[0]});
                    this.getCampusByEsc();
                }
            });
    }

    //+-+-+-+-+-+-+-+-+-+-+-+-+-+ Campus de escuelas
    async getCampusByEsc() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        await fetch(uri + '/api/get/campus/' + this.state.escuelasDatos.institucion, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({noCampus: responseJson});
                }
            });
        await this.elCampus();
    }

    async elCampus() {
        for (let i = 0; i < this.state.noCampus.length; i++) {
            if (this.state.noCampus[i].campus === this.state.escuelasDatos.campus) {
                await this.setState({
                    elCampus: this.state.escuelasDatos.campus,
                    selectIndxCampus: i
                });
            }
        }
        await this.setState({aux: 0});
        await this.getNivelesByCamp();
    }

    async onSelectCampus(indx, campus) {
        await this.setState({
            selectIndxCampus: indx,
            elCampus: campus
        });
        await this.getNivelesByCamp();

    }


    rndCamp(itm, indx) {
        let btnCamp = [{
            borderWidth: 1,
            backgroundColor: 'lightgrey',
            borderRadius: 6,
            padding: 1
        }];
        let texto = [
            styles.bottomMenuElement,
            styles.textW,
            {color: this.state.thirdColor, fontSize: responsiveFontSize(1.7)}
        ];
        if (this.state.selectIndxCampus === indx) {
            btnCamp.push([{
                borderWidth: 1,
                backgroundColor: '#fff',
                borderRadius: 6,
                padding: 1
            }]);
            texto.push([
                styles.bottomMenuElement,
                styles.textW,
                {color: this.state.mainColor}
            ]);
        }
        if (this.state.noCampus.length === 1) {
            return (
                <TouchableOpacity
                    key={indx + 'camp'}
                    style={[styles.btn3_5, btnCamp]}
                    onPress={() => this.onSelectCampus(indx, itm.campus)}
                >
                    <Text style={[
                        styles.bottomMenuElement,
                        styles.textW,
                        texto,
                        {fontSize: responsiveFontSize(1.7)}
                    ]}>
                        Unico campus
                    </Text>
                </TouchableOpacity>
            );
        } else if (this.state.noCampus.length === 2) {
            return (
                <TouchableOpacity
                    key={indx + 'camp'}
                    style={[styles.btn3L, btnCamp, {width: responsiveWidth(27)}]}
                    onPress={() => this.onSelectCampus(indx, itm.campus)}
                >
                    <Text style={[
                        styles.bottomMenuElement,
                        styles.textW,
                        texto,
                        {fontSize: responsiveFontSize(1.7)}
                    ]}
                    >
                        Campus {indx + 1}
                    </Text>
                </TouchableOpacity>
            );
        } else if (this.state.noCampus.length === 3) {
            return (
                <TouchableOpacity
                    key={indx + 'camp'}
                    style={[styles.btn5, btnCamp]}
                    onPress={() => this.onSelectCampus(indx, itm.campus)}
                >
                    <Text style={[
                        styles.bottomMenuElement,
                        styles.textW,
                        texto,
                        {fontSize: responsiveFontSize(1.6)}
                    ]}
                    >
                        Campus {indx + 1}
                    </Text>
                </TouchableOpacity>
            );
        }
    }

    rndCampus() {
        let btnCamp = [];
        this.state.noCampus.forEach((itm, i) => {
            btnCamp.push(this.rndCamp(itm, i))
        });
        return btnCamp;
    }

//+-+-+-+-+-+-+-++-+-+ niveles escolares
    async getNivelesByCamp() {
        if (this.state.escuelasDatos.institucion !== null) {
            let uri = await AsyncStorage.getItem('uri');
            let token = await AsyncStorage.getItem('token');
            await fetch(uri + '/api/get/niveles/' + this.state.escuelasDatos.institucion + '/' + this.state.elCampus, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {

                    } else {
                        this.setState({nivelEscuela: responseJson});
                    }
                });
            await this.elNivelDeCamp();
        }
    }

    async onSelectLvlEsc(indx, nivel) {
        await this.setState({
            indxSelectLvl: indx,
            elNivel: nivel.nivel,
            elId_Esc: nivel.id,
            url: nivel.url_escuela
        });
        await this.alertLogOut(indx);
    }

    async elNivelDeCamp() {
        for (let i = 0; i < this.state.noCampus.length; i++) {
            for (let j = 0; j < this.state.nivelEscuela.length; j++) {
                if (this.state.noCampus[i].campus === this.state.elCampus) {
                    if (this.state.escuelasDatos.nivel === this.state.nivelEscuela[j].nivel) {
                        await this.setState({
                            elNivel: this.state.escuelasDatos.nivel,
                            indxSelectLvl: j
                        });
                    }
                } else {
                    await this.setState({
                        elNivel: '',
                        indxSelectLvl: -1
                    });
                }
            }
        }
        await this.setState({aux: 0});
    }

    async alertLogOut() {
        await this.globalConfigs();
        Alert.alert(
            'Cambio de escuela',
            'Esta a punto de cambiar de nivel escolar a  ' + this.state.elNivel.toUpperCase() + '\n¿Esta seguro?',
            [
                {text: 'Si', onPress: () => this.userChangeEsc(this.state.url, this.state.elId_Esc)},
                {text: 'No'}
            ]
        );
    }

    rndLvl(itm, indx) {
        let btnLvl = [
            styles.btn3_5,
            {marginVertical: 1, borderWidth: 1, backgroundColor: '#f1f1f1', borderRadius: 6, padding: 2}
        ];
        let txtLvl = [
            styles.bottomMenuElement, styles.textW, {color: '#000', fontSize: responsiveFontSize(1.8)}
        ];

        if (this.state.indxSelectLvl === indx) {
            btnLvl.push([
                styles.btn3_5,
                {
                    marginVertical: 1,
                    borderWidth: 1,
                    backgroundColor: this.state.secondColor,
                    borderRadius: 6,
                    padding: 2
                }
            ]);
            txtLvl.push([
                styles.bottomMenuElement, styles.textW, {color: '#fff', fontSize: responsiveFontSize(1.8)}
            ]);
        }
        return (
            <TouchableOpacity
                key={indx + 'lvl'}
                style={btnLvl}
                onPress={() => this.onSelectLvlEsc(indx, itm)}
            >
                <Text style={txtLvl}>
                    {itm.nivel}
                </Text>
            </TouchableOpacity>
        );
    }

    rndLvls() {
        let btnLvl = [];
        this.state.nivelEscuela.forEach((itm, i) => {
            btnLvl.push(this.rndLvl(itm, i))
        });
        return btnLvl;
    }

    //+-+-+-+-+-+--+-+-+-+-+-+-+-+Get Comentarios
    async postEnviarComentarios() {
        await fetch(this.state.url + '/api/get/enviar/comentario/' +
            this.state.data1.id +
            '/' +
            this.state.elComentario +
            '/' +
            this.state.escuelasDatos.id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al enviar comentario',
                        'Ha ocurrido un error '
                        +
                        this.state.elRequest.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Enviar comentarios)',
                        [{
                            text: 'Entendido', onPress: () => Actions.drawer()
                        }]);
                } else {
                    Alert.alert('Comentario enviado',
                        'Se ha comentado el encuadre con éxito',
                        [{
                            text: 'Entendido',
                            onPress: () => this.closeModal()
                        }]);

                }
            });
    }

    async openModal() {
        await this.setState({isModalComent: true});
    }

    async closeModal() {
        await this.setState({isModalComent: false});
    }

    async openModal1() {
        await this.setState({contraM: true});
    }

    async closeModal1() {
        await this.setState({contraM: false});
    }

    render() {
        return (<View
                style={[
                    styles.container,
                    {backgroundColor: this.state.secondColor}
                ]}>
                <Modal
                    isVisible={this.state.contraM}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <KeyboardAvoidingView behavior='position'>
                        <View style={{height: responsiveHeight(40), alignItems: 'center'}}>
                            <View style={[styles.container, {
                                width: responsiveWidth(94),
                                borderRadius: 10,
                                alignItems: 'center',
                                flex: 0
                            }]}>
                                <Text
                                    style={[styles.main_title, styles.modalWidth, {
                                        color: this.state.thirdColor,
                                        textAlign: 'center'
                                    }]}>
                                    Ingrese su nueva contraseña
                                </Text>

                                {Platform.OS === 'ios' ? (
                                    <View
                                        style={[styles.textInputV2, styles.row, styles.modalWidth, {
                                            borderColor: this.state.secondColor,
                                            marginTop: 10
                                        }]}>
                                        <TextInput
                                            ref='email'
                                            placeholder='Contraseña nueva'
                                            secureTextEntry={true}
                                            returnKeyType='next'
                                            underlineColorAndroid='transparent'
                                            autoCapitalize='none'
                                            onChangeText={this.handlePassword}
                                            autoCorrect={false}
                                            style={[
                                                styles.inputDismmis,
                                                {width: responsiveWidth(70), textAlign: 'left'}
                                            ]}
                                            onSubmitEditing={() => this.refs.password.focus()}
                                        />
                                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                                            <MaterialCommunityIcons color={'grey'} name='keyboard-close' size={25}/>
                                        </TouchableWithoutFeedback>

                                    </View>) : (<TextInput
                                    ref='email'
                                    placeholder='Contraseña nueva'
                                    secureTextEntry={true}
                                    returnKeyType='next'
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    onChangeText={this.handlePassword}
                                    autoCorrect={false}
                                    style={[
                                        styles.textInput,
                                        styles.modalWidth,
                                        {borderColor: this.state.secondColor, textAlign: 'left'}
                                    ]}
                                    onSubmitEditing={() => this.refs.password.focus()}
                                />)}
                                <TextInput
                                    ref='password'
                                    placeholder='Confirmar contraseña'
                                    secureTextEntry={true}
                                    underlineColorAndroid='transparent'
                                    onChangeText={this.handlePasswordconfirmation}
                                    returnKeyType='go'
                                    autoCapitalize='none'
                                    style={[
                                        styles.textInput,
                                        styles.modalWidth,
                                        {borderColor: this.state.secondColor, marginTop: 7, textAlign: 'left'}
                                    ]}
                                    onSubmitEditing={() => this.requestMultipart(this.state.password, this.state.Passwordconfirmation)}
                                />
                                <View style={[styles.modalWidth, styles.row]}>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                        ]}
                                        onPress={() => this.closeModal1()}
                                    >
                                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {
                                                backgroundColor: this.state.secondColor,
                                                borderColor: this.state.secondColor
                                            }
                                        ]}
                                        onPress={() => this.requestMultipart(this.state.password, this.state.Passwordconfirmation)}
                                    >
                                        <Text style={[styles.textW, {color: '#fff'}]}>Cambiar contraseña</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </View>
                    </KeyboardAvoidingView>

                </Modal>
                <Modal
                    isVisible={this.state.isModalComent}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <KeyboardAvoidingView behavior='position'>
                        <View style={[styles.container, {width: responsiveWidth(94), borderRadius: 10, flex: 0}]}>
                            <Text
                                style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                                Coméntanos tú experiencia con la App ¿Como podemos mejorar?
                            </Text>
                            <TextInput
                                keyboardType='default'
                                returnKeyType='next'
                                multiline={true}
                                maxLength={250}
                                placeholder='Sea lo mas detallado posible...'
                                underlineColorAndroid='transparent'
                                onChangeText={text => (this.state.elComentario = text)}
                                style={[
                                    styles.inputContenido,
                                    styles.modalWidth,
                                    {borderColor: this.state.secondColor, padding: 10}
                                ]}
                            />
                            <View style={[styles.modalWidth, styles.row]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => [this.closeModal(), Keyboard.dismiss()]}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => [this.postEnviarComentarios(), Keyboard.dismiss()]}
                                >
                                    <Text style={[styles.textW, {color: '#fff'}]}>Enviar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </Modal>
                <View style={{width: responsiveWidth(10), alignItems: 'center'}}>
                    <View style={{alignItems: 'center'}}>
                        <Text style={[styles.welcome, {
                            textAlign: 'center',
                            ...ifIphoneX({marginTop: responsiveHeight(2)}, {marginTop: responsiveHeight(0)}),
                            width: responsiveWidth(60)
                        }]}>
                            Iniciaste sesión como:
                        </Text>
                        <Text style={[styles.user, styles.btn2, {
                            marginBottom: 0,
                            height: responsiveHeight(3),
                            fontStyle: 'italic'
                        }]}>
                            {this.state.data1.role === 'Padre' || this.state.data1.role === 'Alumno' || this.state.data1.role === 'Maestro' ?
                                this.state.data1.role : this.state.data1.role === 'Medico' ?
                                    this.state.data1.role + ' escolar' : null}
                            {this.state.data1.role === 'Admin' ?
                                this.state.data1.puesto === 'Cordisi' ? 'Coordinador de Disciplina' : this.state.data1.puesto :
                                null}{': '}

                        </Text>
                        <Text style={[styles.user, {
                            marginTop: 0,
                            fontSize: responsiveFontSize(2),
                            width: responsiveWidth(60)
                        }]}>
                            {this.state.data.nombre} {'\n'}
                            {this.state.data.apellido_paterno}
                            {' '}
                            {this.state.data.apellido_materno}
                            {'\n'}
                        </Text>
                    </View>
                    <View style={{
                        borderBottomColor: '#fff',
                        borderBottomWidth: 1,
                        width: responsiveWidth(45),
                        marginVertical: 5
                    }}/>
                    <View style={[{
                        width: responsiveWidth(45),
                        ...ifIphoneX({paddingVertical: responsiveHeight(3)}, {paddingVertical: responsiveHeight(1)}),
                        paddingLeft: responsiveWidth(1.5)
                    }]}>
                        <TouchableOpacity
                            style={[styles.btn2, {
                                flexDirection: 'row',
                                marginVertical: 5,
                                width: responsiveWidth(60),

                            }]}
                            onPress={() => Actions.push('HomePage')}>
                            <Ionicons name='ios-home' size={25} color='#ffff'/>
                            <Text style={[styles.menuElementTitle, {marginHorizontal: 20, marginTop: 1}]}>
                                Menú principal
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.btn2, {
                                flexDirection: 'row',
                                marginVertical: 5,
                                width: responsiveWidth(60)
                            }]}
                            onPress={() => this.alertUserLogOut()}>
                            <Ionicons name='ios-log-out' size={25} color='#ffff'/>
                            <Text style={[styles.menuElementTitle, {marginHorizontal: 20, marginTop: 1}]}>
                                Cerrar Sesión
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.btn2, {
                                flexDirection: 'row',
                                marginVertical: 5,
                                width: responsiveWidth(60)
                            }]}
                            onPress={() => this.openModal()}
                        >
                            <FontAwesome name='commenting' size={25} color='#ffff'/>
                            <Text style={[styles.menuElementTitle, {marginHorizontal: 15, marginTop: 1}]}>
                                Enviar Sugerencias
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.btn2, {
                                flexDirection: 'row',
                                marginVertical: 5,
                                width: responsiveWidth(60)
                            }]}
                            onPress={() => this.openModal1()}
                        >
                            <MaterialCommunityIcons name='textbox-password' size={25} color='#ffff'/>
                            <Text style={[styles.menuElementTitle, {marginHorizontal: 15, marginTop: 1}]}>
                                Cambiar Contraseña
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        borderBottomColor: '#fff',
                        borderBottomWidth: 1,
                        width: responsiveWidth(45),
                        marginVertical: 5
                    }}/>
                    {/*Aqui va el cambio de escuela*/}
                    {this.state.data1.role === 'Padre' && this.state.escuelasDatos.gestion === 'Privada' ? (
                        <View style={[styles.borderTop2, {alignItems: 'center', paddingVertical: responsiveHeight(.5)}]}>
                            <View>
                                <Text style={[styles.user, {width: responsiveWidth(45)}]}>
                                    Cambio de escuela
                                </Text>
                                <View style={[styles.btn3_5, {
                                    flexDirection: 'row',
                                    marginVertical: 5,
                                    justifyContent: 'space-between'
                                }]}>
                                    {this.rndCampus()}
                                </View>
                                {this.rndLvls()}
                            </View>
                        </View>
                    ) : null}
                    <View style={{
                        borderBottomColor: '#fff',
                        borderBottomWidth: 1,
                        width: responsiveWidth(60),
                        marginVertical: 5
                    }}>
                    </View>

                    <View style={{
                        width: responsiveWidth(60), alignItems: 'center',
                        ...ifIphoneX({paddingVertical: responsiveHeight(2)}, {paddingVertical: responsiveHeight(.5)})
                    }}>
                        <View style={{width: responsiveWidth(60), flexDirection: 'row'}}>
                            <View style={{width: responsiveWidth(10), alignItems: 'center', justifyContent: 'center'}}>
                                <Ionicons name='md-school' size={26} color='#ffff'/>
                            </View>
                            <View style={{width: responsiveWidth(50)}}>
                                <Text
                                    style={[
                                        styles.menuElementTitle,
                                        {
                                            marginHorizontal: 18,
                                            marginTop: 3,
                                            fontSize: responsiveFontSize(2),
                                            fontWeight: '700'
                                        }
                                    ]}>
                                    {this.state.escuelasDatos.nombre_escuela}
                                </Text>
                            </View>
                        </View>
                        <Text style={[styles.bottomMenuElement, {marginTop: 0, width: responsiveWidth(35), textAlign: 'left'}]}>
                            {'  ' + this.state.escuelasDatos.rvoe}
                        </Text>
                        <View style={{width: responsiveWidth(60), flexDirection: 'row', marginTop: responsiveHeight(3)}}>
                            <View style={{width: responsiveWidth(10), alignItems: 'center', justifyContent: 'center'}}>
                                <Ionicons name='ios-call' size={26} color='#ffff'/>
                            </View>
                            <View style={{width: responsiveWidth(50)}}>
                                <Text
                                    style={[
                                        styles.menuElementTitle,
                                        {
                                            marginHorizontal: 18,
                                            marginTop: 3,
                                            fontSize: responsiveFontSize(2),
                                            fontWeight: '700'
                                        }
                                    ]}>
                                    Teléfono:
                                </Text>
                            </View>
                        </View>
                        <TouchableOpacity
                            style={[styles.btn3, {
                                marginHorizontal: 18,
                                backgroundColor: '#fff',
                                borderRadius: 6,
                                padding: 3.5,
                                paddingVertical: 10,
                                marginTop: 3
                            }]}
                            onPress={() => Communications.phonecall(this.state.escuelasDatos.telefono, true)}>
                            <Text style={[styles.bottomMenuElement, {color: this.state.mainColor}]}>
                                {this.state.escuelasDatos.telefono}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}
