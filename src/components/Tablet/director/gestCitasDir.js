import React from "react";
import {
    Alert,
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {Actions} from "react-native-router-flux";
import styles from "../Tstyles";

export default class gestCitasDir extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            items: {},
            laSolicitud: "",
            solicitudes: []
        };

        this.renderSolicitudes = this.renderSolicitudes.bind(this);
        this.onListPressedSolicitud = this.onListPressedSolicitud.bind(this);
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getSolicitudes();
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getSolicitudes() {
        let solicitudesList = await fetch(
            this.state.uri + "/api/feed/citas/getCitas",
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        listSolicitudes = await solicitudesList.json();
        this.setState({solicitudes: listSolicitudes});
    }

    async onListPressedSolicitud(indexSolicitud, solicitud) {
        this.setState({
            selectedIndexSolicitud: indexSolicitud,
            laSolicitud: solicitud
        });
    }

    renderSolicitud(itemSolicitud, indexSolicitud) {
        return (
            <View
                style={[
                    styles.notificaciones,
                    {borderBottomColor: this.state.fourthColor}
                ]}>
                <Text style={styles.notifTitle}>Solicitud de Cita</Text>
                <View>
                    <Text style={styles.textInfo}>
                        Se ha solicitado una cita por {itemSolicitud.user.name} tutor de{" "}
                        {itemSolicitud.hijos.name} del Grado: {itemSolicitud.hijos.grado},
                        Grupo: {itemSolicitud.hijos.grupo}{" "}
                    </Text>
                    <Text style={styles.textInfo}>
                        Agendada el {itemSolicitud.fecha_cita} a las 7:30 para{" "}
                        {itemSolicitud.asunto}
                    </Text>
                </View>
                <View style={styles.theButtons}>
                    <TouchableOpacity
                        style={styles.buttonAceptar}
                        onPress={() => this.requestMultipart(itemSolicitud.id)}>
                        <Text style={styles.titBtn_GCM}>Aceptar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.buttonMensaje}
                        onPress={() => this.requestMultipart1()}>
                        <Text style={styles.titBtn_GCM}>Mensaje</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    async requestMultipart(id) {
        let solicitudesList = await fetch(
            this.state.uri + "/api/feed/citas/update/" + id,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );

        Alert.alert("¡Excelente!", "Has Aceptado la cita", [
            {
                text: "Entendido",
                onPress: () => Actions.refresh({key: "gestCitasMtro"})
            }
        ]);
    }

    async requestMultipart1() {
        Alert.alert("Lo sentimos", "Por ahora no disponibles", [
            {
                text: "Entendido"
            }
        ]);
    }

    renderSolicitudes() {
        let buttonsSolicitudes = [];
        this.state.solicitudes.forEach((itemSolicitud, indexSolicitud) => {
            buttonsSolicitudes.push(
                this.renderSolicitud(itemSolicitud, indexSolicitud)
            );
        });
        return buttonsSolicitudes;
    }

    render() {
        const solicitudes = this.renderSolicitudes();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la solicitud
                        </Text>
                        {solicitudes}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
