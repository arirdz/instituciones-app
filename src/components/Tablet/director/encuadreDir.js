import React from 'react';
import {
	AsyncStorage,
	StatusBar,
	TouchableOpacity,
	Text,
	Alert,
	KeyboardAvoidingView,
	View,
	TextInput,
	Keyboard,
	Platform
} from 'react-native';
import {
	responsiveHeight,
	responsiveWidth
} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import styles from '../Tstyles';
import {Actions} from 'react-native-router-flux';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Periodos from '../Globales/Periodos';
import Modal from 'react-native-modal';

export default class encuadreDir extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			elCiclo: '',
			isModalRest: false,
			elGrado: '',
			elComentario: '',
			elGrupo: '',
			elPeriodo: '',
			encuadre: [],
			elRequest: [],
			grupos: [],
			elTeacher: '',
			indexSelected: 0,
			indexSelectedTab: 0,
			laMateria: '',
			materias: [],
			periodos: [],
			ponderacion: [],
			selectedIndexPeriodo: '',
			selectedIndexGrados: -1,
			grado: '',
			selectedIndexGrupos: -1,
			grupo: '',
			selectedIndexPeriodo: -1,
			elPeriodo: ''
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
		await this.getUserdata();
		await this.getCiclosPeriodos();
	}

	// async borrarEncuadre() {
	// 	let uri = await AsyncStorage.getItem('uri');
	// 	let token = await AsyncStorage.getItem('token');
	// 	let formData = new FormData();
	// 	for (let i = 0; i < this.state.encuadres.length; i++) {
	// 		formData.append(
	// 			'update',
	// 			JSON.stringify({
	// 				id_maestro: this.state.encuadre[i].id_maestro,
	// 				id_materia: this.state.encuadre[i].id_materia,
	// 				grado: this.state.elGrado,
	// 				grupo: this.state.elGrupo,
	// 				periodo: this.state.elPeriodo
	// 			})
	// 		);
	// 		fetch(uri + '/api/borrar/encuadre', {
	// 			method: 'POST',
	// 			headers: {
	// 				'Content-Type': 'multipart/form-data',
	// 				Authorization: 'Bearer ' + token
	// 			},
	// 			body: formData
	// 		}).then(res => res.json())
	// 			.then(responseJson => {
	// 				this.setState({elRequest: responseJson});
	// 			});
	// 	}
	// 	if (this.state.elRequest.error !== undefined) {
	// 		Alert.alert('¡Ups!',
	// 			'Ha ocurrido un error '
	// 			+
	// 			this.state.elRequest.error.status_code
	// 			+
	// 			' si el error continua pónganse en contacto con soporte'
	// 			+
	// 			'(Encuadres)',
	// 			[{
	// 				text: 'Entendido', onPress: () => Actions.drawer()
	// 			}]);
	// 	}
	// 	await this.requestWithComent();
	// }



	async requestWithComent() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let formData = new FormData();
		formData.append(
			'update',
			JSON.stringify({id_materia: this.state.encuadre[0].id_materia,
			   grado: this.state.elGrado,
			   grupo: this.state.elGrupo,
			   periodo: this.state.elPeriodo,
			   comentario: this.state.elComentario
			})
		);
		await fetch(uri + '/api/encuadre/director/comentar', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('Error al enviar comentario',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Encuadres)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('Comentario enviado',
				'Se ha comentado el encuadre con éxito',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		}
	}

	// async comentario() {
	// 	if (this.state.elComentario === '') {
	// 		Alert.alert('Ups', 'No has escrito ningún comentario', [
	// 			{text: 'Entendido'}
	// 		]);
	// 	} else {
	// 		Alert.alert('¡Atencion!', '¿Seguro que deseas mandar un comentario?', [
	// 			{text: 'No'},
	// 			{
	// 				text: 'Si',
	// 				onPress: () => this.borrarEncuadre()
	// 			}
	// 		]);
	// 	}
	// }

	async onListItemPressedPeriodo(index, ciclo) {
		await this.setState({
			selectedIndexPeriodo: index,
			elPeriodo: ciclo
		});
	}

	async onListItemPressedLista(indexLista, taller) {
		await this.setState({
			selectedIndexLista: indexLista,
			laLista: taller
		});
	}

	async onListItemPressedGrupos(indexGrupo, grupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: grupo
		});
		await this.getMaterias();
	}

	async onListItemPressedGrado(indexGrado, grado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: grado
		});
	}

	//+++++++++++++++++++++++++++++++ Get User +++++++++++++++++++++++++++++++++++
	async getUserdata() {
		let request = await fetch(this.state.uri + '/api/user/data', {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({data: responseJson});
				}
			});
	}

	// +++++++++++++++++++++++++++++++ CICLOS ++++++++++++++++++++++++++++++++++++
	async getCiclosPeriodos() {
		let losciclos = await fetch(this.state.uri + '/api/ciclo/periodo/actual', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		});
		let ciclosss = await losciclos.json();
		this.setState({elCiclo: ciclosss[0].ciclo});
	}

	//+++++++++++++++++++++++++++++++Encuadres++++++++++++++++++++++++++++++++++++
	async getEncuadres() {
		let encuadresList = await fetch(
			this.state.uri +
			'/api/get/encuadre/admin/' +
			this.state.laMateria +
			'/' +
			this.state.elPeriodo +
			'/' +
			this.state.elCiclo +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let encuadre = await encuadresList.json();
		this.setState({encuadre: encuadre});
	}

	//++++++++++++++++++++++++++++++ Get Materias ++++++++++++++++++++++++++++++++
	async getMaterias() {
		let materiapicker = await fetch(
			this.state.uri +
			'/api/cordacad/get/materias/' +
			this.state.elGrado +
			'/' +
			this.state.elGrupo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		);
		let materiaspicker = await materiapicker.json();
		await this.setState({materias: materiaspicker});
		await this.setState({elTeacher: this.state.materias[0].maestro.name});
	}

	async onChange(option) {
		await this.setState({
			laMateria: option.materia
		});
		await this.setState({
			elTeacher: this.state.materias[option.key].maestro.name
		});
		await this.getEncuadres();
	}


	renderEncuadre(itemAlumno, indexAlumno) {
		let a = this.state.encuadre.length - 1;
		let tabRow = [styles.rowTabla, styles.btn4_5];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		return (
			<View
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={tabRow}
			>
				<View style={[styles.campoTablaG, styles.btn4_5, styles.row, {paddingHorizontal: 10}]}>
					<Text>{itemAlumno.descripcion}</Text>
					<Text>{itemAlumno.valor}</Text>
				</View>
			</View>
		);
	}

	renderEncuadres() {
		let buttonsEncuadres = [];
		this.state.encuadre.forEach((itemEncuadre, indexEncuadre) => {
			buttonsEncuadres.push(this.renderEncuadre(itemEncuadre, indexEncuadre));
		});
		return buttonsEncuadres;
	}

	// ++++++++++++++++++++++++++++ Render +++++++++++++++++++++++++++++++++++++++
	render() {
		const verEn = this.renderEncuadres();
		let data = this.state.materias.map((item, i) => {
			return {
				grado: item.nombre.grado,
				key: i,
				label: item.nombre.nombre + ' ' + item.nombre.grado,
				materia: item.nombre.id
			};
		});

		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<Modal
					isVisible={this.state.isModalRest}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>

					<View style={{height: responsiveHeight(52), alignItems: 'center'}}>

							<View
								style={[
									styles.container,styles.modalWidth
								]}
							>
								<Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
									Escriba aquí el comentario.
								</Text>
								<TextInput
									keyboardType='default'
									returnKeyType='next'
									multiline={true}
									underlineColorAndroid='transparent'
									onChangeText={text => (this.state.elComentario = text)}
									onEndEditing={() => Keyboard.dismiss()}
									style={[
										styles.inputContenido,
										styles.modalWidth,
										{borderColor: this.state.secondColor}
									]}
								/>
								<View style={[styles.row, styles.modalWidth, {paddingHorizontal: 20}]}>
									<TouchableOpacity
										style={[
											styles.bigButton,
											styles.btn3,
											{backgroundColor: this.state.mainColor, borderRadius: 6}
										]}
										onPress={() => this.setState({isModalRest: false})}
									>
										<Text style={styles.textButton}>Cancelar</Text>
									</TouchableOpacity>
									<TouchableOpacity
										style={[
											styles.bigButton,
											styles.btn3,
											{backgroundColor: this.state.mainColor, borderRadius: 6}
										]}
										onPress={() => this.requestWithComent()}
									>
										<Text style={styles.textButton}>Enviar</Text>
									</TouchableOpacity>
								</View>
							</View>

					</View>
				</Modal>
				<Modal
					isVisible={this.state.isModalalumno}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={{height: responsiveHeight(65), alignItems: 'center'}}>
						<View
							style={[
								styles.container,
								{borderRadius: 6, width: responsiveWidth(94)}
							]}
						>
							<View style={{marginTop: 30, marginBottom: 10}}>
								<Text style={{fontWeight: '700', textAlign: 'center'}}>
									Maestro:
								</Text>
								<Text style={{fontWeight: '400', textAlign: 'center'}}>
									{this.state.elTeacher}
								</Text>
							</View>
							<View
								style={[
									styles.tabla,
									styles.btn4_5,
									{borderColor: this.state.secondColor, marginTop: 15}
								]}
							>
								{verEn}
							</View>
							<TouchableOpacity
								style={[
									styles.bigButton,
									styles.btn2,
									{
										backgroundColor: this.state.mainColor,
										borderRadius: 6,
										marginTop: 20
									}
								]}
								onPress={() => this.setState({isModalalumno: false})}
							>
								<Text style={styles.textButton}>Aceptar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<View style={{height: responsiveHeight(13)}}>
					<Periodos
						onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
							this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
						}
					/>
				</View>
				<GradoyGrupo
					onListItemPressedGrupos={(indexBtn, itemBtn) =>
						this.onListItemPressedGrupos(indexBtn, itemBtn)
					}
					onListItemPressedGrado={(indexBtn, itemBtn) =>
						this.onListItemPressedGrado(indexBtn, itemBtn)
					}
					onListItemPressedLista={(indexBtn, itemBtn) =>
						this.onListItemPressedLista(indexBtn, itemBtn)
					}
				/>
				<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
					Seleccione la materia
				</Text>

				<ModalSelector
					cancelText='Cancelar'
					data={data}
					initValue='Seleccione la materia'
					onChange={option => this.onChange(option)}
					optionTextStyle={{color: this.state.thirdColor}}
					selectStyle={[
						styles.inputPicker,
						{borderColor: this.state.secondColor}
					]}
				/>
				<Text
					style={[
						styles.main_title,
						{marginTop: 25, textAlign: 'center', color: this.state.thirdColor}
					]}
				>
					Presione para ver el encuadre
				</Text>
				<View style={[styles.widthall, {alignItems: 'center'}]}>
					<TouchableOpacity
						style={[
							styles.bigButton,
							{
								backgroundColor: this.state.secondColor,
								width: responsiveWidth(47),
								height: responsiveHeight(5),
								borderRadius: 6
							}
						]}
						onPress={() => this.setState({isModalalumno: true})}
					>
						<Text style={[styles.textButton]}>Consultar</Text>
					</TouchableOpacity>
				</View>
				<TouchableOpacity
					style={[
						styles.bigButton,
						{backgroundColor: this.state.secondColor, marginTop: Platform.OS === 'ios' ? 89 : 67}
					]}
					onPress={() => this.setState({isModalRest: true})}
				>
					<Text style={styles.textButton}>Comentarios</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
