import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    StatusBar,
    ScrollView,
    TouchableHighlight
} from "react-native";
import {
    responsiveFontSize,
    responsiveWidth
} from "react-native-responsive-dimensions";
import styles from "../Tstyles";
import GradoyGrupo from "./../Globales/GradoyGrupo";
import Periodos from "./../Globales/Periodos";
import MultiBotonRow from "./../Globales/MultiBotonRow";
import EstadAprobados from "./../Globales/EstadAprobados";
import EstadReprobados from "./../Globales/EstadReprobados";
import EstadConduct from "./../Globales/EstadConduct";

export default class indEstadDir extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: "",
            solicitudes: [],
            botonSelected: "Aprobados",
            botonSelected1: "Academicos",
            selectedIndexGrados: -1,
            grado: "",
            selectedIndexGrupos: -1,
            grupo: "",
            selectedIndexPeriodo: -1,
            elPeriodo: "",
            aprobados: true
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    async botonSelected1(indexSelected, itemSelected) {
        await this.setState({
            botonSelected1: itemSelected,
            indexSelected: indexSelected
        });
    }

    academicos() {
        let btnApr = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        let texto = [
            styles.textoN,
            {fontSize: responsiveFontSize(1.7), fontWeight: "700"}
        ];
        if (this.state.aprobados === true) {
            btnApr.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7),
                fontWeight: "700"
            });
        }
        let btnRep = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        let texto1 = [
            styles.textoN,
            {fontSize: responsiveFontSize(1.7), fontWeight: "700"}
        ];
        if (this.state.reprobados === true) {
            btnRep.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto1.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7),
                fontWeight: "700"
            });
        }
        return (
            <View>
                <View style={{width: responsiveWidth(94), alignItems: "center"}}>
                    <View
                        style={[
                            styles.rowsCalif,
                            {width: responsiveWidth(62.5), marginTop: 9}
                        ]}>
                        <TouchableHighlight
                            underlayColor={this.state.thirdColor}
                            style={btnApr}
                            onPress={() => {
                                if (this.state.aprobados === false) {
                                    this.setState({aprobados: true, reprobados: false});
                                } else {
                                    this.setState({aprobados: true, reprobados: false});
                                }
                            }}>
                            <Text style={texto}>Aprobados</Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                            underlayColor={this.state.thirdColor}
                            style={btnRep}
                            onPress={() => {
                                if (this.state.reprobados === false) {
                                    this.setState({reprobados: true, aprobados: false});
                                } else {
                                    this.setState({reprobados: true, aprobados: false});
                                }
                            }}>
                            <Text style={texto1}>Reprobados</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                {this.state.aprobados === true ? <EstadAprobados/> : null}
                {this.state.reprobados === true ? <EstadReprobados/> : null}
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <StatusBar
                        backgroundColor={this.state.mainColor}
                        barStyle="light-content"
                    />

                    <View
                        style={{
                            marginTop: 5,
                            flexDirection: "row",
                            width: responsiveWidth(94)
                        }}>
                        <Periodos
                            onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                                this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                            }
                        />
                    </View>
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <View style={{width: responsiveWidth(94)}}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione tipo de consulta
                        </Text>
                        <MultiBotonRow
                            itemBtns={["Academicos", "Conductuales"]}
                            onSelectedButton={(indexBtn, itemBtn) =>
                                this.botonSelected1(indexBtn, itemBtn)
                            }
                            cantidad={2}
                        />
                        {this.state.botonSelected1 === "Academicos"
                            ? this.academicos()
                            : null}
                        {this.state.botonSelected1 === "Conductuales" ? (
                            <EstadConduct/>
                        ) : null}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
