import React from "react";
import {
    Text,
    View,
    AsyncStorage,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    TextInput
} from "react-native";
import {
    responsiveWidth
} from "react-native-responsive-dimensions";
import GradoyGrupo from "./../Globales/GradoyGrupo";
import Periodos from "./../Globales/Periodos";
import ModalSelector from "react-native-modal-selector";
import styles from "../Tstyles";
import Modal from "react-native-modal";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class califDirec extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            alumnos: [],
            materias: [],
            evidencias: [],
            laMateria: [],
            laEvidencia: "",
            selectedIndexGrados: -1,
            elGrado: "",
            selectedIndexGrupos: -1,
            elGrupo: "",
            selectedIndexPeriodo: -1,
            elPeriodo: "",
            laEvidencia: [],
            valor: [],
            elValor: "",
            textValue: ""
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async onChangeText(option) {
        await this.setState({
            textValue: option
        });
    }

    async onListItemPressedPeriodo(indexPeriodo, ciclo) {
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: ciclo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            elGrado: grado
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            elGrupo: grupo
        });
        await this.getMaterias();
    }

    async onChange(option) {
        await this.setState({
            laMateria: option.materia
        });
        await this.getEvidencias();
    }

    //+++++++++++++++++++++++++++++++valor & maximo +++++++++++++++++++++++++++++++++
    async getValorMax() {
        let valorMax = await fetch(
            this.state.uri +
            "/api/valores/admin/" +
            this.state.laEvidencia +
            "/" +
            this.state.laMateria +
            "/" +
            this.state.elGrupo +
            "/" +
            this.state.elPeriodo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer" + this.state.token
                }
            }
        );
        let valorMaximo = await valorMax.json();
        this.setState({elValor: valorMaximo});
    }

    // ++++++++++++++++++++++++++++++ Evidencias +++++++++++++++++++++++++++++++++

    async onChangeEvidencia(option) {
        await this.setState({
            laEvidencia: option.label,
            btncito: true
        });
        await this.getAlumnosByGG();
        await this.getValorMax();
    }

    async getEvidencias() {
        let evidenciapicker = await fetch(
            this.state.uri +
            "/api/get/all/evidencia/tipos/" +
            this.state.laMateria +
            "/" +
            this.state.elGrupo +
            "/" +
            this.state.elPeriodo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let evidenciaspicker = await evidenciapicker.json();
        this.setState({evidencias: evidenciaspicker});
    }

    //+++++++++++++++++++++++++++++Alumnos grado y grupo +++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri +
            "/api/acumulados/admin/" +
            this.state.laEvidencia +
            "/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo +
            "/" +
            this.state.laMateria +
            "/" +
            this.state.elPeriodo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        const lista = this.isAlumno(itemAlumno, indexAlumno);
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {lista}
            </View>
        );
    }

    isAlumno(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth]}>
                    <View>
                        <Text>{itemAlumno.nombre}</Text>
                    </View>
                    <View style={[styles.row, {width: responsiveWidth(21)}]}>
                        <Text>{itemAlumno.puntos}</Text>
                        <Text>{itemAlumno.obtenido}%</Text>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //+++++++++++++++++++++++++++++Materias++++++++++++++++++++++++++++++++
    async getMaterias() {
        let materiapicker = await fetch(
            this.state.uri +
            "/api/cordacad/get/materias/" +
            this.state.elGrado +
            "/" +
            this.state.elGrupo,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    render() {
        const alumnos = this.renderAlumnos();
        let data = this.state.materias.map((item, i) => {
            return {
                grado: item.nombre.grado,
                key: i,
                label: item.nombre.nombre + " " + item.nombre.grado,
                materia: item.nombre.id
            };
        });
        let dataEvidencia = this.state.evidencias.map((item, i) => {
            return {
                key: i,
                label: item.descripcion,
                id: item.id
            };
        });
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.isModalVisible}
                    backdropOpacity={0.5}
                    animationIn={"bounceIn"}
                    animationOut={"bounceOut"}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={styles.modalCalif}>
                        <View style={{marginBottom: 35, alignItems: "center"}}>
                            <Text style={styles.subtitCalif}>
                                Especifique el motivo de la revisión
                            </Text>
                            <TextInput
                                keyboardType="default"
                                placeholder={
                                    "Exprese lo más detalladamente posible el motivo de la revisión"
                                }
                                maxLength={254}
                                maxHeight={250}
                                onChangeText={option => this.onChangeText(option)}
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                style={[
                                    styles.solRev,
                                    {marginTop: 3, borderColor: this.state.secondColor}
                                ]}
                            />
                        </View>
                        <TouchableOpacity
                            style={[
                                styles.btnAceptClaus,
                                {backgroundColor: this.state.secondColor}
                            ]}
                            onPress={() => this.setState({isModalVisible: false})}
                        >
                            <Text style={{color: "white"}}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <GradoyGrupo
                        onListItemPressedGrupos={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrupos(indexBtn, itemBtn)
                        }
                        onListItemPressedGrado={(indexBtn, itemBtn) =>
                            this.onListItemPressedGrado(indexBtn, itemBtn)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione la materia
                    </Text>
                    <ModalSelector
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        data={data}
                        onChange={option => this.onChange(option)}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        initValue="Seleccione la materia"
                    />
                    <Text
                        style={[
                            styles.main_title,
                            {marginTop: 7, color: this.state.thirdColor}
                        ]}
                    >
                        Seleccione la información a consultar
                    </Text>
                    <ModalSelector
                        data={dataEvidencia}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        initValue="Evaluación"
                        onChange={option => this.onChangeEvidencia(option)}
                    />
                    <View style={styles.alignView}>
                        <View style={[styles.rowsCalif, {marginTop: 5}]}>
                            <Text style={{marginLeft: 100}}>Valor</Text>
                            <Text style={{marginRight: 95}}>Maximo</Text>
                        </View>
                        <View style={[styles.rowsCalif, {width: responsiveWidth(62.5)}]}>
                            <View
                                style={[
                                    styles.exaCalif,
                                    {backgroundColor: this.state.fourthColor}
                                ]}
                            >
                                <Text>{this.state.elValor.valor}%</Text>
                            </View>
                            <View
                                style={[
                                    styles.exaCalif,
                                    {backgroundColor: this.state.fourthColor}
                                ]}
                            >
                                <Text>{this.state.elValor.maximo}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.califList}>
                        <View style={[styles.rowsCalif, {marginTop: 5}]}>
                            <Text style={{marginLeft: 245}}>Cant.</Text>
                            <Text style={{marginRight: 20}}>Valor</Text>
                        </View>
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor}]}
                        >
                            {alumnos}
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
