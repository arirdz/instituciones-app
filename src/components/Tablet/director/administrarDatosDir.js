import React from 'react';
import {
	View,
	ScrollView,
	Text,
	KeyboardAvoidingView,
	AsyncStorage,
	TouchableHighlight,
	StatusBar,
	TouchableOpacity,
	Alert
} from 'react-native';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Communications from 'react-native-communications';
import ModalSelector from 'react-native-modal-selector';
import styles from '../Tstyles';
import MultiBotonRow from '../Globales/MultiBotonRow';
import DatosPersonalesDir from '../director/datosPersonalesDir';
import GradoyGrupo from '../Globales/GradoyGrupo';
import Modal from 'react-native-modal';

export default class administrarDatosDir extends React.Component {
	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			grados: [],
			NamesitosPickers: [],
			grupos: [],
			botonSelected: 'Hacer una consulta',
			dataa: [],
			selGrad: null,
			isModalCateg: false,
			selGrup: null,
			selectedIndex: -1,
			selectedIndexGrupo: -1,
			selectedIndexName: -1,
			selectedIndexName2: -1,
			names: [],
			selName: '',
			textInputValue: '',
			id: '',
			elId: '',
			elId2: '',
			userId: '',
			userId2: '',
			elNombre: ''
		};
	}

	onSelect(data) {
		this.setState({selRole: data});
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getNames() {
		let role = await this.state.selRole;
		let gradoo = await this.state.selGrad;
		let grupoo = await this.state.selGrup;
		await this.setState({names: []});
		await fetch(
			this.state.uri +
			'/api/datos/contacto/' +
			role +
			'/' +
			gradoo +
			'/' +
			grupoo,
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer ' + this.state.token
				}
			}
		).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 11)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({names: responseJson.map(item => item)});
				}
			});

	}

	async getDatos() {
		let id_usuarios = '';
		if (this.state.selRole === 'Padre' || this.state.selRole === 'Alumno') {
			id_usuarios = this.state.userId2;
		} else {
			id_usuarios = this.state.elId2;
		}
		await fetch(this.state.uri + '/api/datos/user/' + id_usuarios, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('Error al cargar los datos de usuario',
						'Ha ocurrido un error ' + responseJson.error.status_code +
						' al tratar de cargar los datos del usuario si el error continua pónganse en contacto con soporte (Cod. 12)');
				} else {
					this.setState({dataa: responseJson[0]});
				}
			});
	}

	async onListItemPressed(index, grado) {
		await this.setState({selectedIndex: index, selGrad: grado, selGrup: null});
	}

	async onListItemPressedGrupo(index, grupo) {
		await this.setState({selectedIndexGrupo: index, selGrup: grupo});
		await this.getNames();
	}

	async onListPressedName(index, nomvres, aaydi, elId) {
		await this.setState({selectedIndexName: index, selName: nomvres, userId: aaydi, elId: elId});
	}

	renderName(itemName, indexName) {
		let a = this.state.names.length - 1;
		let tabRow = [styles.rowTabla, styles.modalWidth];
		if (indexName !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}
		let smallButtonStyles = [styles.rowTabla, styles.modalWidth, {
			borderColor: this.state.secondColor,
			alignItems: 'center', justifyContent: 'center'
		}];
		let texto = [styles.textoN];
		if (this.state.selectedIndexName === indexName) {
			smallButtonStyles.push(styles.rowTabla, styles.modalWidth, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		return (
			<TouchableHighlight
				key={indexName}
				underlayColor={this.state.secondColor}
				style={[
					smallButtonStyles,
					{
						borderColor: this.state.secondColor,
						height: responsiveHeight(4)
					}
				]}
				onPress={() => this.onListPressedName(indexName, itemName.name, itemName.padre_id, itemName.id)}
			>
				<View style={styles.listItem}>
					<Text style={texto}>{itemName.name}</Text>
				</View>
			</TouchableHighlight>
		);
	}

	renderNames() {
		let buttonsNames = [];
		this.state.names.forEach((itemName, indexName) => {
			buttonsNames.push(this.renderName(itemName, indexName));
		});
		return buttonsNames;
	}

	isMami() {
		let parentesco = '';
		if (this.state.dataa.parentesco === 1) {
			parentesco = 'Papá'
		}
		if (this.state.dataa.parentesco === 2) {
			parentesco = 'Mamá'
		}
		if (this.state.dataa.parentesco === 3) {
			parentesco = 'Abuelo'
		}
		if (this.state.dataa.parentesco === 4) {
			parentesco = 'Abuela'
		}
		if (this.state.dataa.parentesco === 5) {
			parentesco = 'Tio'
		}
		if (this.state.dataa.parentesco === 6) {
			parentesco = 'Tia'
		}
		if (this.state.dataa.parentesco === 7) {
			parentesco = 'Primo'
		}
		if (this.state.dataa.parentesco === 8) {
			parentesco = 'Prima'
		}
		if (this.state.dataa.parentesco === 9) {
			parentesco = 'Hermano'
		}
		if (this.state.dataa.parentesco === 10) {
			parentesco = 'Hermano'
		}
		return (
			<View style={[styles.widthall, {flexDirection: 'row'}]}>
				<View style={[styles.izquierda, {width: responsiveWidth(30)}]}>
					<Text style={styles.textData}>Nombre:</Text>
					{this.state.selRole === 'Padre' || this.state.selRole === 'Alumno' ? (
						<Text style={styles.textData}>Parentesco:</Text>) : null}
					<Text style={styles.textData}>Teléfono Casa:</Text>
					<Text style={styles.textData}>Teléfono Oficina:</Text>
					<Text style={styles.textData}>Teléfono Celular:</Text>
				</View>
				<View style={[styles.derecha, {width: responsiveWidth(64)}]}>
					<Text style={styles.textData}>
						{this.state.dataa.nombre !== undefined ?
							this.state.dataa.nombre === null || this.state.dataa.nombre === '' ? 'Aún sin capturar' :
								this.state.dataa.nombre + ' ' + this.state.dataa.apellido_paterno + ' ' + this.state.dataa.apellido_materno
							: null}
					</Text>
					{this.state.selRole === 'Padre' || this.state.selRole === 'Alumno' ? (
						<Text style={styles.textData}>
							{this.state.dataa.parentesco === null || this.state.dataa.parentesco === '' ?
								'Aún sin capturar' : parentesco}
						</Text>
					) : null}
					{this.state.dataa.telefono_casa_tutor === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_casa_tutor, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_casa_tutor}
							</Text>
						</TouchableOpacity>)}
					{this.state.dataa.telefono_casa_tutor === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_oficina_tutor, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_oficina_tutor}
							</Text>
						</TouchableOpacity>)}
					{this.state.dataa.telefono_casa_tutor === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_celula_tutor, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_celular_tutor}
							</Text>
						</TouchableOpacity>)}
				</View>
			</View>
		);
	}

	isPapi() {
		let parentesco = '';
		if (this.state.dataa.parentesco_alternativo1 === 1) {
			parentesco = 'Papá'
		}
		if (this.state.dataa.parentesco_alternativo1 === 2) {
			parentesco = 'Mamá'
		}
		if (this.state.dataa.parentesco_alternativo1 === 3) {
			parentesco = 'Abuelo'
		}
		if (this.state.dataa.parentesco_alternativo1 === 4) {
			parentesco = 'Abuela'
		}
		if (this.state.dataa.parentesco_alternativo1 === 5) {
			parentesco = 'Tio'
		}
		if (this.state.dataa.parentesco_alternativo1 === 6) {
			parentesco = 'Tia'
		}
		if (this.state.dataa.parentesco_alternativo1 === 7) {
			parentesco = 'Primo'
		}
		if (this.state.dataa.parentesco_alternativo1 === 8) {
			parentesco = 'Prima'
		}
		if (this.state.dataa.parentesco_alternativo1 === 9) {
			parentesco = 'Hermano'
		}
		if (this.state.dataa.parentesco_alternativo1 === 10) {
			parentesco = 'Hermano'
		}
		return (
			<View style={[styles.widthall, {flexDirection: 'row'}]}>
				<View style={[styles.izquierda, {width: responsiveWidth(30)}]}>
					<Text style={styles.textData}>Nombre:</Text>
					<Text style={styles.textData}>Parentesco:</Text>
					<Text style={styles.textData}>Teléfono Casa:</Text>
					<Text style={styles.textData}>Teléfono Oficina:</Text>
					<Text style={styles.textData}>Teléfono Celular:</Text>
				</View>
				<View style={[styles.derecha, {width: responsiveWidth(64)}]}>
					<Text style={styles.textData}>
						{this.state.dataa.nombre_alternativo1 === '' || this.state.dataa.nombre_alternativo1 === null ?
							'Aún sin capturar' : this.state.dataa.nombre_alternativo1}
					</Text>
					<Text style={styles.textData}>
						{this.state.dataa.parentesco_alternativo1 === '' || this.state.dataa.parentesco_alternativo1 === null ?
							'Aún sin capturar' : parentesco}
					</Text>
					{this.state.dataa.telefono_casa_alternativo1 === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_casa_alternativo1, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_casa_alternativo1}
							</Text>
						</TouchableOpacity>)}
					{this.state.dataa.telefono_oficina_alternativo1 === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_oficina_alternativo1, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_oficina_alternativo1}
							</Text>
						</TouchableOpacity>)}
					{this.state.dataa.telefono_celular_alternativo1 === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_celular_alternativo1, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_celular_alternativo1}
							</Text>
						</TouchableOpacity>)}
				</View>
			</View>
		);
	}

	isOtro() {
		let parentesco = '';
		if (this.state.dataa.parentesco_alternativo2 === 1) {
			parentesco = 'Papá'
		}
		if (this.state.dataa.parentesco_alternativo2 === 2) {
			parentesco = 'Mamá'
		}
		if (this.state.dataa.parentesco_alternativo2 === 3) {
			parentesco = 'Abuelo'
		}
		if (this.state.dataa.parentesco_alternativo2 === 4) {
			parentesco = 'Abuela'
		}
		if (this.state.dataa.parentesco_alternativo2 === 5) {
			parentesco = 'Tio'
		}
		if (this.state.dataa.parentesco_alternativo2 === 6) {
			parentesco = 'Tia'
		}
		if (this.state.dataa.parentesco_alternativo2 === 7) {
			parentesco = 'Primo'
		}
		if (this.state.dataa.parentesco_alternativo2 === 8) {
			parentesco = 'Prima'
		}
		if (this.state.dataa.parentesco_alternativo2 === 9) {
			parentesco = 'Hermano'
		}
		if (this.state.dataa.parentesco_alternativo2 === 10) {
			parentesco = 'Hermano'
		}
		return (
			<View style={[styles.widthall, {flexDirection: 'row'}]}>
				<View style={[styles.izquierda, {width: responsiveWidth(30)}]}>
					<Text style={styles.textData}>Nombre:</Text>
					<Text style={styles.textData}>Parentesco:</Text>
					<Text style={styles.textData}>Teléfono Casa:</Text>
					<Text style={styles.textData}>Teléfono Oficina:</Text>
					<Text style={styles.textData}>Teléfono Celular:</Text>
				</View>
				<View style={[styles.derecha, {width: responsiveWidth(64)}]}>
					<Text style={styles.textData}>
						{this.state.dataa.nombre_alternativo2 === '' || this.state.dataa.nombre_alternativo2 === null ?
							'Aún sin capturar' : this.state.dataa.nombre_alternativo2}
					</Text>
					<Text style={styles.textData}>
						{this.state.dataa.parentesco_alternativo2 === '' || this.state.dataa.parentesco_alternativo2 === null
							? 'Aún sin capturar' : parentesco}
					</Text>
					{this.state.dataa.telefono_casa_alternativo2 === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_casa_alternativo2, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_casa_alternativo2}
							</Text>
						</TouchableOpacity>)}
					{this.state.dataa.telefono_oficina_alternativo2 === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_oficina_alternativo2, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_oficina_alternativo2}
							</Text>
						</TouchableOpacity>)}
					{this.state.dataa.telefono_celular_alternativo2 === null ? (
						<Text style={styles.textData}>Aún sin capturar</Text>) : (
						<TouchableOpacity
							onPress={() => Communications.phonecall(this.state.dataa.telefono_celular_alternativo2, true)}
						>
							<Text style={styles.textData}>
								{this.state.dataa.telefono_celular_alternativo2}
							</Text>
						</TouchableOpacity>)}
				</View>
			</View>
		);
	}


	async onChange(option) {
		await this.setState({
			textInputValue: option.label,
			selRole: option.id,
			selectedIndexName: -1,
			selectedIndexName2: -1,
			selGrad: null,
			selGrup: null,
			selName: '',
			userId: '',
			elNombre: '',
			userId2: '',
			dataa: []
		});
		await this.getNames();
	}

	async botonSelected(indexSelected, itemSelected) {
		await this.setState({
			botonSelected: itemSelected,
			indexSelected: indexSelected
		});
	}

	async openModal() {
		this.state.selName = this.state.elNombre;
		this.state.selectedIndexName = this.state.selectedIndexName2;
		this.state.userId = this.state.userId2;
		this.state.elId = this.state.elId2;
		if (this.state.textInputValue === '') {
			Alert.alert('Tipo usuario no seleccionado', 'Seleccione un tipo de usuario a consultar por favor', [{text: 'Entendido'}])
		} else {
			if (this.state.selRole === 'Alumno') {
				if (this.state.selGrad === '' || this.state.selGrad === null) {
					Alert.alert('Campo grado indefinido', 'El grupo no esta definido seleccione uno para continuar',
						[{text: 'Enterado'}])
				} else if (this.state.selGrup === '' || this.state.selGrup === null) {
					Alert.alert('Campo grupo indefinido', 'El grado no esta definido seleccione uno para continuar',
						[{text: 'Enterado'}])
				} else {
					if (this.state.names.length === 0) {
						Alert.alert('Sin alumnos', 'No hay alumnos en este grupo',
							[{text: 'Enterado'}])
					} else {
						await this.setState({isModalCateg: true, selectedIndexName: this.state.selectedIndexName2});
					}
				}
			} else {
				await this.setState({isModalCateg: true, selectedIndexName: this.state.selectedIndexName2});
			}
		}
	}


	async closeModal() {
		if (this.state.selectedIndexName === -1) {
			Alert.alert('Usuario no seleccionado', '', [{text: 'Entendido'}]);
		} else {
			this.state.elNombre = this.state.selName;
			this.state.selectedIndexName2 = this.state.selectedIndexName;
			this.state.userId2 = this.state.userId;
			this.state.elId2 = this.state.elId;
			await this.setState({isModalCateg: false});
			await this.getDatos();
		}
	}

	gestionarDatos() {
		const names = this.renderNames();
		let dex = 0;
		const data = [
			{key: dex++, label: 'Padre o Tutor', id: 'Alumno'},
			{key: dex++, label: 'Maestro', id: 'Maestro'},
			{key: dex++, label: 'Administrativo', id: 'Admin'}
		];
		return (
			<ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 20}}>
				<Modal
					isVisible={this.state.isModalCateg}
					backdropOpacity={0.5}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6, flex: 0, maxHeight:responsiveHeight(80)}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Seleccione una opción
						</Text>


						<View
							style={[styles.tabla, {borderColor: this.state.secondColor, height: responsiveHeight(60)}]}
							showsHorizontalScrollIndicator={false}
						>
							<ScrollView>
								{names}
							</ScrollView>
						</View>

						<View style={[styles.modalWidth, styles.row]}>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: '#fff', borderColor: this.state.secondColor}
								]}
								onPress={() => this.setState({isModalCateg: false})}
							>
								<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={[
									styles.modalBigBtn,
									{backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
								]}
								onPress={() => this.closeModal()}
							>
								<Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
				<Text
					style={[
						styles.main_title,
						{color: this.state.thirdColor, marginBottom: 2, marginTop: 0}
					]}
				>
					Elija el tipo de usuario a consultar
				</Text>
				<ModalSelector
					data={data}
					selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor, marginTop: 1}]}
					initValue='Lista de usuarios'
					cancelText='Cancelar'
					optionTextStyle={{color: this.state.thirdColor}}
					onChange={option => this.onChange(option)}
				/>
				{this.state.selRole === 'Alumno' || this.state.selRole === 'Padre' ? (
					<GradoyGrupo
						onListItemPressedGrupos={(indx, itm) => this.onListItemPressedGrupo(indx, itm)}
						onListItemPressedGrado={(indx, itm) => this.onListItemPressed(indx, itm)}
					/>
				) : null}
				<Text
					style={[
						styles.main_title,
						{color: this.state.thirdColor}
					]}
				>
					{this.state.textInputValue === '' ? 'Listas de usuarios' : 'Consultar datos del ' + this.state.textInputValue}
				</Text>
				<View style={{
					alignItems: 'center',
					backgroundColor: this.state.fourthColor,
					padding: 15,
					borderRadius: 6
				}}>
					<TouchableOpacity
						style={[
							styles.bigButton,
							styles.btnShowMod,
							{
								marginTop: 5,
								marginBottom: 5,
								borderColor: this.state.secondColor,
								backgroundColor: '#fff',
								height: responsiveHeight(5)
							}
						]}
						onPress={() => this.openModal()}>
						<Text>Ver lista</Text>
					</TouchableOpacity>
					<Text style={{marginTop: 10}}>Usuario seleccionado</Text>
					<Text style={[styles.textButton, {color: 'black'}]}>
						{this.state.elNombre}
					</Text>
				</View>
				<View>
					<Text style={[styles.emergenciasTit, {marginTop: 15}]}>Teléfono de Emergencias</Text>
					{this.state.dataa.numero_emergencia === null ? (
						<TouchableOpacity
							style={[styles.emergenciasInput_AD, {marginTop: 2}]}
						>
							<Text
								style={styles.tituloEmergencias}
							>
								Aún sin capturar
							</Text>
						</TouchableOpacity>
					) : (
						<TouchableOpacity
							onPress={() =>
								Communications.phonecall(this.state.dataa.numero_emergencia, true)
							}
							style={[styles.emergenciasInput_AD, {marginTop: 2}]}
						>
							<Text
								style={styles.tituloEmergencias}
							>
								{this.state.dataa.numero_emergencia}
							</Text>
						</TouchableOpacity>
					)}
				</View>
				<View style={styles.datosContainer}>
					<View>
						{this.state.selRole === 'Padre' || this.state.selRole === 'Alumno' ? (
								<Text style={styles.tituloData}>Datos del tutor</Text>) :
							(
								<Text style={styles.tituloData}>Datos de contacto </Text>
							)}
						{this.isMami()}
					</View>
					<View>
						<Text style={styles.tituloData}>Alternativo 1</Text>
						{this.isPapi()}
					</View>
					<View>
						<Text style={styles.tituloData}>Alternativo 2</Text>
						{this.isOtro()}
					</View>
				</View>
			</ScrollView>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor={this.state.mainColor}
					barStyle='light-content'
				/>
				<View style={{marginTop: 5, marginBottom: 10}}>
					<MultiBotonRow
						itemBtns={['Hacer una consulta', 'Actualizar mis datos']}
						onSelectedButton={(indexBtn, itemBtn) =>
							this.botonSelected(indexBtn, itemBtn)
						}
						cantidad={2}
					/>
				</View>
				{this.state.botonSelected === 'Hacer una consulta'
					? this.gestionarDatos()
					: null}
				{this.state.botonSelected === 'Actualizar mis datos' ? (
					<DatosPersonalesDir/>
				) : null}
			</View>
		);
	}
}
