import React from "react";
import {View, AsyncStorage, StatusBar, Text} from "react-native";
import styles from "../Tstyles";
import InfoInasistDirec from "./../director/InfoInasistDirec";
import EstadAsistDirec from "./../director/EstadAsistDirec";
import ListaAsist from "./../CoordAcad/ListaAsist";
import MultiBotonRow from "./../Globales/MultiBotonRow";

const moment = require("moment");
require("moment/locale/es");
moment.locale("es");

export default class asistDirec extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cososPicker: [],
            botonSelected: "Listas de asistencia"
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let token = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <MultiBotonRow
                    itemBtns={[
                        "Listas",
                        "Informes",
                        "Estadísticas",
                        "Pasar lista"
                    ]}
                    onSelectedButton={(indexBtn, itemBtn) =>
                        this.botonSelected(indexBtn, itemBtn)
                    }
                    cantidad={4}
                />
                {this.state.botonSelected === "Listas" ? (
                    <ListaAsist/>
                ) : null}
                {this.state.botonSelected === "Informes" ? (
                    <InfoInasistDirec/>
                ) : null}
                {this.state.botonSelected === "Estadísticas" ? (
                    <EstadAsistDirec/>
                ) : null}
                {this.state.botonSelected === "Pasar lista" ? (
                    <EstadAsistDirec/>
                ) : null}
            </View>
        );
    }
}
