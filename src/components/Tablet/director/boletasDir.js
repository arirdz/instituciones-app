import React from "react";
import {
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import styles from "../Tstyles";
import {Actions} from "react-native-router-flux";
import Periodos from "../Globales/Periodos";
import Table from "../Globales/Table";

export default class boletasDir extends React.Component {
    _showModal = () => this.setState({isModalVisible: true});

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            items: {},
            laMateria: "",
            materias: [],
            botonSelected: "Revisar calificaciones",
            indexSelectedTab: 0,
            alumnos: [],
            selectedIndexGrados: -1,
            grado: "",
            selectedIndexGrupos: -1,
            grupo: "",
            selectedIndexPeriodo: -1,
            elPeriodo: ""
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getAlumnosByGG();
        await this.getMaterias();
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, selected: option.id});
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo,
            grupo: grupo
        });
    }

    async onListItemPressedPeriodo(index, ciclo) {
        await this.setState({
            selectedIndexPeriodo: index,
            elPeriodo: ciclo
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado,
            grado: grado
        });
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected,
            indexSelected: indexSelected
        });
    }

    async getMaterias() {
        let materiapicker = await fetch(this.state.uri + "/api/materias/maestro", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + this.state.token
            }
        });
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    revCalif() {
        const listAlum = this.renderAlumnos();
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <Table
                        arreglo={this.state.porcentajes}
                        filas={this.state.grados}
                        columnas={this.state.grupos}
                    />

                    <View style={{marginTop: 5, alignItems: "center"}}>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Seleccione la materia
                        </Text>
                        <View
                            style={[
                                styles.contAlumnos2,
                                {borderColor: this.state.secondColor}
                            ]}
                        />
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Lista de alumnos
                        </Text>
                        <View
                            style={[styles.tabla, {borderColor: this.state.secondColor}]}
                        >
                            {listAlum}
                        </View>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.secondColor}
                    ]}
                    onPress={() => Actions.pruebaBoleta()}
                >
                    <Text style={styles.textButton}>Enviar recordatorio</Text>
                </TouchableOpacity>
            </View>
        );
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri + "/api/get/alumnos/by/grado/grupo/3/A",
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        this.setState({alumnos: alumnospicker});
    }

    isATiempo(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.name}</Text>
                    </View>
                    <View>
                        <Text>asdasda</Text>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}
            >
                {this.state.indexSelectedTab === 0
                    ? this.isATiempo(itemAlumno, indexAlumno)
                    : null}
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    render() {
        const verBolt = this.revCalif();
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {verBolt}
            </View>
        );
    }
}
