import React from 'react';
import {
    Text,
    View,
    AsyncStorage,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import {responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../Tstyles';
import Periodos from './../Globales/Periodos';
import Modal from 'react-native-modal';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class EstadAsistDirec extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cososPicker: [],
            alumnos: [],
            selectedIndexPeriodo: -1,
            elPeriodo: '',
            alumnosCero: [],
            alumnosCinco: [],
            alumnosDiez: [],
            alumnosOnce: [],
            alumnosVeinte: [],
            historico: [],
            elCiclo: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            token: token,
            uri: uri
        });
    }

    async onListItemPressedPeriodo(indexPeriodo, itemPeriodo) {
        await this.setState({
            selectedIndexPeriodo: indexPeriodo,
            elPeriodo: itemPeriodo
        });

        await this.getAlumnosByGG();
        this.get0falta();
        this.get5falta();
        this.get10falta();
        this.get11falta();
        this.get20falta();
        this.getHistorico();
    }

    //+++++++++++++++++++++++++++++++++ Historico ++++++++++++++++++++++++++++++++++
    async getHistorico() {
        let elHistorico = await fetch(
            this.state.uri +
            '/api/porcentaje/asistencias/historico/' +
            this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Barer' + this.state.token
                }
            }
        );
        let historicooo = await elHistorico.json();
        this.setState({historico: historicooo});
    }

    // +++++++++++++++++++++ Alumnos grado y grupo ++++++++++++++++++++++++
    async getAlumnosByGG() {
        let alumnopicker = await fetch(
            this.state.uri + '/api/get/top/faltantes/' + this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnospicker = await alumnopicker.json();
        await this.setState({alumnos: alumnospicker});

        const l = this.state.alumnos.length;
        for (let i = 0; i < l; i++) {
            for (let j = 0; j < l - 1 - i; j++) {
                if (this.state.alumnos[j].faltas < this.state.alumnos[j + 1].faltas) {
                    [this.state.alumnos[j], this.state.alumnos[j + 1]] = [
                        this.state.alumnos[j + 1],
                        this.state.alumnos[j]
                    ];
                }
            }
        }
        await this.setState({aux: 0});
    }

    renderAlumno(itemAlumno, indexAlumno) {
        let a = this.state.alumnos.length - 1;
        let tabRow = [styles.rowTabla];
        if (indexAlumno !== a) {
            tabRow.push({borderColor: this.state.secondColor});
        }

        const alumno = this.isValor(itemAlumno, indexAlumno);
        return (
            <View
                key={indexAlumno}
                underlayColor={this.state.secondColor}
                style={tabRow}>
                {alumno}
            </View>
        );
    }

    isValor(itemAlumno, indexAlumno) {
        return (
            <View style={styles.campoTablaG}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View>
                        <Text>{itemAlumno.nombre}</Text>
                    </View>
                    <View style={{marginRight: 10}}>
                        <Text>{itemAlumno.faltas}</Text>
                    </View>
                </View>
            </View>
        );
    }

    renderAlumnos() {
        let buttonsAlumnos = [];
        this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
            buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
        });
        return buttonsAlumnos;
    }

    //++++++++++++++++++++++++++++++++top cero faltas+++++++++++++++
    async get0falta() {
        let alumno0 = await fetch(
            this.state.uri + '/api/faltantes/sinfaltas/' + this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnos0 = await alumno0.json();
        this.setState({alumnosCero: alumnos0});
    }

    render0Falta(itemAlumno0, indexAlumno0) {
        let a = this.state.alumnosCero.length - 1;
        let tabRow1 = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno0 !== a) {
            tabRow1.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno0}
                underlayColor={this.state.secondColor}
                style={tabRow1}>
                {this.state.isModal0 === true
                    ? this.isModal0(itemAlumno0, indexAlumno0)
                    : null}
            </View>
        );
    }

    isModal0(itemAlumno0, indexAlumno0) {
        return (
            <View style={[styles.campoTablaG, styles.modalWidth]}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View
                        underlayColor={this.state.secondColor}
                        style={[
                            styles.rowTabla,
                            {width: responsiveWidth(74), paddingHorizontal: 10}
                        ]}>
                        <Text>{itemAlumno0.nombre}</Text>
                    </View>
                    <View style={{marginRight: 100}}>
                        <Text>{itemAlumno0.faltas}</Text>
                    </View>
                </View>
            </View>
        );
    }

    render0Faltas() {
        let btnAlumnos0 = [];
        this.state.alumnosCero.forEach((itemAlumno0, indexAlumno0) => {
            btnAlumnos0.push(this.render0Falta(itemAlumno0, indexAlumno0));
        });
        return btnAlumnos0;
    }

    //++++++++++++++++++++++++++++ 1 a 5 faltas ++++++++++++++++++++++++++++++++++
    async get5falta() {
        let alumno5 = await fetch(
            this.state.uri + '/api/faltantes/cinco/' + this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnos5 = await alumno5.json();
        await this.setState({alumnosCinco: alumnos5});

        const l = this.state.alumnosCinco.length;
        for (let i = 0; i < l; i++) {
            for (let j = 0; j < l - 1 - i; j++) {
                if (
                    this.state.alumnosCinco[j].faltas <
                    this.state.alumnosCinco[j + 1].faltas
                ) {
                    [this.state.alumnosCinco[j], this.state.alumnosCinco[j + 1]] = [
                        this.state.alumnosCinco[j + 1],
                        this.state.alumnosCinco[j]
                    ];
                }
            }
        }
        await this.setState({aux: 0});
    }

    render5Falta(itemAlumno5, indexAlumno5) {
        let a = this.state.alumnosCinco.length - 1;
        let tabRow1 = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno5 !== a) {
            tabRow1.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno5}
                underlayColor={this.state.secondColor}
                style={tabRow1}>
                {this.state.isModal5 === true
                    ? this.isModal1a5(itemAlumno5, indexAlumno5)
                    : null}
            </View>
        );
    }

    isModal1a5(itemAlumno5, indexAlumno5) {
        return (
            <View style={[styles.campoTablaG, styles.modalWidth]}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View
                        underlayColor={this.state.secondColor}
                        style={[
                            styles.rowTabla,
                            {width: responsiveWidth(74), paddingHorizontal: 10}
                        ]}>
                        <Text>{itemAlumno5.nombre}</Text>
                    </View>
                    <View style={{marginRight: 100}}>
                        <Text>{itemAlumno5.faltas}</Text>
                    </View>
                </View>
            </View>
        );
    }

    render1a5Faltas() {
        let btnAlumnos5 = [];
        this.state.alumnosCinco.forEach((itemAlumno5, indexAlumno5) => {
            btnAlumnos5.push(this.render5Falta(itemAlumno5, indexAlumno5));
        });
        return btnAlumnos5;
    }

    //+++++++++++++++++++++++++++++++ 6 a 10 faltas ++++++++++++++++++++++++++++++
    async get10falta() {
        let alumno10 = await fetch(
            this.state.uri + '/api/faltantes/diez/' + this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnos10 = await alumno10.json();
        await this.setState({alumnosDiez: alumnos10});

        const l = this.state.alumnosDiez.length;
        for (let i = 0; i < l; i++) {
            for (let j = 0; j < l - 1 - i; j++) {
                if (
                    this.state.alumnosDiez[j].faltas <
                    this.state.alumnosDiez[j + 1].faltas
                ) {
                    [this.state.alumnosDiez[j], this.state.alumnosDiez[j + 1]] = [
                        this.state.alumnosDiez[j + 1],
                        this.state.alumnosDiez[j]
                    ];
                }
            }
        }
        await this.setState({aux: 0});
    }

    render10Falta(itemAlumno10, indexAlumno10) {
        let a = this.state.alumnosDiez.length - 1;
        let tabRow1 = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno10 !== a) {
            tabRow1.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno10}
                underlayColor={this.state.secondColor}
                style={tabRow1}>
                {this.state.isModal10 === true
                    ? this.isModal10(itemAlumno10, indexAlumno10)
                    : null}
            </View>
        );
    }

    isModal10(itemAlumno10, indexAlumno10) {
        return (
            <View style={[styles.campoTablaG, styles.modalWidth]}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View
                        underlayColor={this.state.secondColor}
                        style={[
                            styles.rowTabla,
                            {width: responsiveWidth(74), paddingHorizontal: 10}
                        ]}>
                        <Text>{itemAlumno10.nombre}</Text>
                    </View>
                    <View style={{marginRight: 100}}>
                        <Text>{itemAlumno10.faltas}</Text>
                    </View>
                </View>
            </View>
        );
    }

    render10Faltas() {
        let btnAlumnos10 = [];
        this.state.alumnosDiez.forEach((itemAlumno10, indexAlumno10) => {
            btnAlumnos10.push(this.render10Falta(itemAlumno10, indexAlumno10));
        });
        return btnAlumnos10;
    }

    //++++++++++++++++++++++++++++++++11 a 20 faltas +++++++++++++++++++++++++++
    async get11falta() {
        let alumno11 = await fetch(
            this.state.uri + '/api/faltantes/veinte/' + this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnos11 = await alumno11.json();
        await this.setState({alumnosOnce: alumnos11});

        const l = this.state.alumnosOnce.length;
        for (let i = 0; i < l; i++) {
            for (let j = 0; j < l - 1 - i; j++) {
                if (
                    this.state.alumnosOnce[j].faltas <
                    this.state.alumnosOnce[j + 1].faltas
                ) {
                    [this.state.alumnosOnce[j], this.state.alumnosOnce[j + 1]] = [
                        this.state.alumnosOnce[j + 1],
                        this.state.alumnosOnce[j]
                    ];
                }
            }
        }
        await this.setState({aux: 0});
    }

    render11Falta(itemAlumno11, indexAlumno11) {
        let a = this.state.alumnosOnce.length - 1;
        let tabRow1 = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno11 !== a) {
            tabRow1.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno11}
                underlayColor={this.state.secondColor}
                style={tabRow1}>
                {this.state.isModal11 === true ? this.isModal11() : null}
            </View>
        );
    }

    isModal11(itemAlumno11, indexAlumno11) {
        return (
            <View style={[styles.campoTablaG, styles.modalWidth]}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View
                        underlayColor={this.state.secondColor}
                        style={[
                            styles.rowTabla,
                            {width: responsiveWidth(74), paddingHorizontal: 10}
                        ]}>
                        <Text>{itemAlumno11.nombre}</Text>
                    </View>
                    <View style={{marginRight: 100}}>
                        <Text>{itemAlumno11.faltas}</Text>
                    </View>
                </View>
            </View>
        );
    }

    render11Faltas() {
        let btnAlumnos11 = [];
        this.state.alumnosOnce.forEach((itemAlumno11, indexAlumno11) => {
            btnAlumnos11.push(this.render10Falta(itemAlumno11, indexAlumno11));
        });
        return btnAlumnos11;
    }

    //+++++++++++++++++++++++++++++++ 20 o mas faltas +++++++++++++++++++++++++++++
    async get20falta() {
        let alumno20 = await fetch(
            this.state.uri + '/api/faltantes/veinte/' + this.state.elPeriodo,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let alumnos20 = await alumno20.json();
        await this.setState({alumnosVeinte: alumnos20});

        const l = this.state.alumnosVeinte.length;
        for (let i = 0; i < l; i++) {
            for (let j = 0; j < l - 1 - i; j++) {
                if (
                    this.state.alumnosVeinte[j].faltas <
                    this.state.alumnosVeinte[j + 1].faltas
                ) {
                    [this.state.alumnosVeinte[j], this.state.alumnosVeinte[j + 1]] = [
                        this.state.alumnosVeinte[j + 1],
                        this.state.alumnosVeinte[j]
                    ];
                }
            }
        }
        await this.setState({aux: 0});
    }

    render20Falta(itemAlumno20, indexAlumno20) {
        let a = this.state.alumnosVeinte.length - 1;
        let tabRow1 = [styles.rowTabla, styles.modalWidth];
        if (indexAlumno20 !== a) {
            tabRow1.push({borderColor: this.state.secondColor});
        }
        return (
            <View
                key={indexAlumno20}
                underlayColor={this.state.secondColor}
                style={tabRow1}>
                {this.state.isModal20 === true
                    ? this.isModal20(itemAlumno20, indexAlumno20)
                    : null}
            </View>
        );
    }

    isModal20(itemAlumno20, indexAlumno20) {
        return (
            <View style={[styles.campoTablaG, styles.modalWidth]}>
                <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                    <View
                        underlayColor={this.state.secondColor}
                        style={[
                            styles.rowTabla,
                            {width: responsiveWidth(74), paddingHorizontal: 10}
                        ]}>
                        <Text>{itemAlumno20.nombre}</Text>
                    </View>
                    <View style={{marginRight: 100}}>
                        <Text>{itemAlumno20.faltas}</Text>
                    </View>
                </View>
            </View>
        );
    }

    render20Faltas() {
        let btnAlumnos20 = [];
        this.state.alumnosVeinte.forEach((itemAlumno20, indexAlumno20) => {
            btnAlumnos20.push(this.render20Falta(itemAlumno20, indexAlumno20));
        });
        return btnAlumnos20;
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++
    render() {
        const modal0 = this.render0Faltas();
        const modal5 = this.render1a5Faltas();
        const modal10 = this.render10Faltas();
        const modal11 = this.render11Faltas();
        const alumnos = this.renderAlumnos();
        const modal20 = this.render20Faltas();
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.isModal0}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de alumnos
                            </Text>
                            <View
                                style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
                                <Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
                                <Text style={{marginRight: 15}}>Faltas</Text>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}>
                                {modal0}
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor
                                }
                            ]}
                            onPress={() => this.setState({isModal0: false})}>
                            <Text style={styles.textButton}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal
                    isVisible={this.state.isModal5}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de alumnos
                            </Text>
                            <View
                                style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
                                <Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
                                <Text style={{marginRight: 15}}>Faltas</Text>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}>
                                {modal5}
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor
                                }
                            ]}
                            onPress={() => this.setState({isModal5: false})}>
                            <Text style={styles.textButton}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal
                    isVisible={this.state.isModal10}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de alumnos
                            </Text>
                            <View
                                style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
                                <Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
                                <Text style={{marginRight: 15}}>Faltas</Text>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}>
                                {modal10}
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor
                                }
                            ]}
                            onPress={() => this.setState({isModal10: false})}>
                            <Text style={styles.textButton}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal
                    isVisible={this.state.isModal11}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de alumnos
                            </Text>
                            <View
                                style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
                                <Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
                                <Text style={{marginRight: 15}}>Faltas</Text>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}>
                                {modal11}
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor
                                }
                            ]}
                            onPress={() => this.setState({isModal11: false})}>
                            <Text style={styles.textButton}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal
                    isVisible={this.state.isModal20}
                    backdropOpacity={0.5}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}>
                    <View style={[styles.container, {borderRadius: 6}]}>
                        <ScrollView>
                            <Text style={[styles.main_title, styles.modalWidth]}>
                                Lista de alumnos
                            </Text>
                            <View
                                style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}>
                                <Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
                                <Text style={{marginRight: 15}}>Faltas</Text>
                            </View>
                            <View
                                style={[
                                    styles.tabla,
                                    {borderColor: this.state.secondColor, marginTop: 10}
                                ]}>
                                {modal11}
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={[
                                styles.bigButton,
                                styles.modalWidth,
                                {
                                    backgroundColor: this.state.mainColor,
                                    borderColor: this.state.mainColor
                                }
                            ]}
                            onPress={() => this.setState({isModal20: false})}>
                            <Text style={styles.textButton}>Aceptar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <ScrollView>
                    <Periodos
                        onListItemPressedPeriodo={(indexPeriodo, itemPeriodo) =>
                            this.onListItemPressedPeriodo(indexPeriodo, itemPeriodo)
                        }
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Los 5 alumnos con más faltas en el periodo seleccionado
                    </Text>
                    <View style={styles.rows_tit}>
                        <Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
                        <Text style={{marginRight: 15}}>Faltas</Text>
                    </View>
                    <View
                        style={[
                            styles.tabla,
                            {borderColor: this.state.secondColor, marginTop: 5}
                        ]}>
                        {alumnos}
                    </View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Total de alumnos con faltas por categoría en el periodo seleccionado
                    </Text>
                    <View style={styles.rows_tit}>
                        <Text>0 Faltas</Text>
                        <View style={styles.first_btn}>
                            <Text style={styles.txt_botons}>
                                {this.state.alumnosCero.length}
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={styles.ver_lista}
                            onPress={() => this.setState({isModal0: true})}>
                            <Text>Ver lista</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rows_tit}>
                        <Text>1 - 5</Text>
                        <View style={[styles.sec_btn, {marginLeft: 20}]}>
                            <Text style={styles.txt_botons}>
                                {this.state.alumnosCinco.length}
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={styles.ver_lista}
                            onPress={() => this.setState({isModal5: true})}>
                            <Text>Ver lista</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rows_tit}>
                        <Text>6 - 10</Text>
                        <View style={[styles.third_btn, {marginLeft: 10}]}>
                            <Text style={styles.txt_botons}>
                                {this.state.alumnosDiez.length}
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={styles.ver_lista}
                            onPress={() => this.setState({isModal10: true})}>
                            <Text>Ver lista</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rows_tit}>
                        <Text>11 - 20</Text>
                        <View style={[styles.fourth_btn, {marginLeft: 5}]}>
                            <Text style={styles.txt_botons}>
                                {this.state.alumnosOnce.length}
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={styles.ver_lista}
                            onPress={() => this.setState({isModal11: true})}>
                            <Text>Ver lista</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rows_tit}>
                        <Text>Más de 20</Text>
                        <View style={[styles.fiveth_btn, {marginRight: 16}]}>
                            <Text style={styles.txt_botons}>
                                {this.state.alumnosVeinte.length}
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={styles.ver_lista}
                            onPress={() => this.setState({isModal20: true})}>
                            <Text>Ver lista</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.rows_tit, {width: responsiveWidth(94)}]}>
                        <View
                            style={[
                                styles.btn_blue,
                                {
                                    width: responsiveWidth(45),
                                    backgroundColor: this.state.fourthColor
                                }
                            ]}>
                            <Text style={{fontWeight: '700'}}>
                                Porcentajes de asistencia:
                            </Text>
                        </View>
                        <View
                            style={[
                                styles.btn_blue,
                                {
                                    backgroundColor: this.state.fourthColor,
                                    width: responsiveWidth(45)
                                }
                            ]}>
                            <Text>Histórico</Text>
                            <Text style={{fontSize: 30}}>{this.state.historico}%</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
