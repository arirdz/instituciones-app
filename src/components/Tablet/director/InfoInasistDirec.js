import React from 'react';
import {
	AsyncStorage,
	ScrollView,
	Text,
	TouchableHighlight,
	TouchableOpacity,
	View
} from 'react-native';
import {
	responsiveFontSize,
	responsiveWidth,
	responsiveHeight
} from 'react-native-responsive-dimensions';
import styles from '../Tstyles';
import GradoyGrupo from './../Globales/GradoyGrupo';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Modal from 'react-native-modal';

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class InfoInasistDirec extends React.Component {
	_showDateTimePicker = () => this.setState({isDateTimePickerVisible: true});
	_hideDateTimePicker = () => this.setState({isDateTimePickerVisible: false});
	_handleDatePicked = date => {
		let fecha = moment(date).format('YYYY-MM-DD');
		this.setState({fecha1: fecha});
		this.date();
	};

	constructor(props) {
		super(props);
		this.state = {
			isDateTimePickerVisible: false,
			cososPicker: [],
			laAsistencia: '',
			alumnos: [],
			fecha1: '',
			selectedIndexGrados: -1,
			elGrado: '',
			selectedIndexGrupos: -1,
			elGrupo: '',
			faltaGrupal: [],
			asistenciaGrupal: [],
			fechaH: true,
			hoyDia: '',
			hoy: ''
		};
	}

	static fechaHoy() {
		return moment().format('YYYY-MM-DD');
	}

	async date() {
		await this.setState({fechaH: false});
		await this._hideDateTimePicker();
		await this.getGruposFaltas();
		await this.getGruposAsist();
	}

	async componentWillMount() {
		await this.getURL();
		await this.setState({hoyDia: InfoInasistDirec.fechaHoy()});
		await this.getGruposFaltas();
		await this.getGruposAsist();
	}

	async getURL() {
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let token = await AsyncStorage.getItem('token');
		let uri = await AsyncStorage.getItem('uri');
		this.setState({
			fourthColor: fourthColor,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			token: token,
			uri: uri
		});
		await this.getHoy();
		await this.getHoy2();
	}

	async onListItemPressedGrupos(indexGrupo, itemGrupo) {
		await this.setState({
			selectedIndexGrupos: indexGrupo,
			elGrupo: itemGrupo
		});
		await this.getAlumnosByGG();
	}

	async onListItemPressedGrado(indexGrado, itemGrado) {
		await this.setState({
			selectedIndexGrados: indexGrado,
			elGrado: itemGrado
		});
	}

	//+++++++++++++++++++++++++++++++hoy asistencia 2+++++++++++++++++++++++++++++++

	async getHoy2() {
		let hoyAsist = await fetch(this.state.uri + '/api/asistencia/hoy', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Bearer' + this.state.token
			}
		});
		let hoyAsists = await hoyAsist.json();
		this.setState({laAsistencia: hoyAsists});
	}

	//+++++++++++++++++++++++++++++++++Hoy asistencia++++++++++++++++++++++++++++++
	async getHoy() {
		let elHoy = await fetch(
			this.state.uri + '/api/porcentaje/asistencias/hoy',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: 'Bearer' + this.state.token
				}
			}
		);
		let hoyyyy = await elHoy.json();
		this.setState({hoy: hoyyyy});
	}

	//+++++++++++++++++++++++++++++Grupos con Faltas+++++++++++++++++++++++++++++
	async getGruposFaltas() {
		let faltaGrupo = '';
		if (this.state.fechaH === true) {
			faltaGrupo = await fetch(
				this.state.uri +
				'/api/get/inasistencias/by/grupos/' +
				this.state.hoyDia +
				'/' +
				this.state.hoyDia,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer' + this.state.token
					}
				}
			);
		} else {
			faltaGrupo = await fetch(
				this.state.uri +
				'/api/get/inasistencias/by/grupos/' +
				this.state.fecha1 +
				'/' +
				this.state.fecha1,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer' + this.state.token
					}
				}
			);
		}
		let faltagrup = await faltaGrupo.json();
		this.setState({faltaGrupal: faltagrup});
	}

	renderGFalta(itemGrupofal, indexGrupofal) {
		return (
			<View
				key={indexGrupofal}
				style={[
					styles.rows_tit,
					{width: responsiveWidth(30), paddingHorizontal: 10}
				]}
			>
				<Text style={{fontWeight: '600', fontSize: responsiveFontSize(2)}}>
					{itemGrupofal.grado} {itemGrupofal.grupo}
				</Text>
				<View style={styles.red_btn}>
					<Text style={{fontWeight: '800', color: 'white'}}>
						{itemGrupofal.inasistencias}
					</Text>
				</View>
			</View>
		);
	}

	renderGFaltas() {
		let gruposfal = [];
		this.state.faltaGrupal.forEach((itemGrupofal, indexGrupofal) => {
			gruposfal.push(this.renderGFalta(itemGrupofal, indexGrupofal));
		});
		return gruposfal;
	}

	//+++++++++++++++++++++++++++++++Grupos con asistencia+++++++++++++++++++++++
	async getGruposAsist() {
		let asistGrupo = '';
		if (this.state.fechaH === true) {
			asistGrupo = await fetch(
				this.state.uri +
				'/api/get/asistencias/by/grupos/' +
				this.state.hoyDia +
				'/' +
				this.state.hoyDia,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer' + this.state.token
					}
				}
			);
		} else {
			asistGrupo = await fetch(
				this.state.uri +
				'/api/get/asistencias/by/grupos/' +
				this.state.fecha1 +
				'/' +
				this.state.fecha1,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer' + this.state.token
					}
				}
			);
		}
		let asistGrupoo = await asistGrupo.json();
		this.setState({asistenciaGrupal: asistGrupoo});
	}

	renderGAsist(itemGrupoasis, indexGrupoasis) {
		return (
			<View
				key={indexGrupoasis}
				style={[
					styles.rows_tit,
					{width: responsiveWidth(30), paddingHorizontal: 10}
				]}
			>
				<Text style={{fontWeight: '600', fontSize: responsiveFontSize(2)}}>
					{itemGrupoasis.grado} {itemGrupoasis.grupo}
				</Text>
				<View style={[styles.red_btn, {backgroundColor: 'green'}]}>
					<Text style={{fontWeight: '800', color: 'white'}}>
						{itemGrupoasis.asistencias}
					</Text>
				</View>
			</View>
		);
	}

	renderGAsists() {
		let grupoasis = [];
		this.state.asistenciaGrupal.forEach((itemGrupoasis, indexGrupoasis) => {
			grupoasis.push(this.renderGAsist(itemGrupoasis, indexGrupoasis));
		});
		return grupoasis;
	}

	// +++++++++++++++++++++ Alumnos asistencia ++++++++++++++++++++++++
	async getAlumnosByGG() {
		let alumnopicker = '';
		if (this.state.fechaH === true) {
			alumnopicker = await fetch(
				this.state.uri +
				'/api/get/asistencias/fecha/' +
				this.state.hoyDia +
				'/' +
				this.state.elGrado +
				'/' +
				this.state.elGrupo,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}
			);
		} else {
			alumnopicker = await fetch(
				this.state.uri +
				'/api/get/asistencias/fecha/' +
				this.state.fecha1 +
				'/' +
				this.state.elGrado +
				'/' +
				this.state.elGrupo,
				{
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'Bearer ' + this.state.token
					}
				}
			);
		}
		let alumnospicker = await alumnopicker.json();
		this.setState({alumnos: alumnospicker});
	}

	renderAlumno(itemAlumno, indexAlumno) {
		let a = this.state.alumnos.length - 1;
		let tabRow = [styles.rowTabla, {width: responsiveWidth(85)}];
		if (indexAlumno !== a) {
			tabRow.push({borderColor: this.state.secondColor});
		}

		return (
			<View
				key={indexAlumno}
				underlayColor={this.state.secondColor}
				style={tabRow}
			>
				{this.state.isModalalumno === true
					? this.isValor(itemAlumno, indexAlumno)
					: null}
			</View>
		);
	}

	isValor(itemAlumno, indexAlumno) {
		return (
			<View style={styles.campoTablaG}>
				<View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
					<View>
						<Text>{itemAlumno.nombre}</Text>
					</View>
					<View style={{marginRight: 50}}>
						<Text>{itemAlumno.inasistencias}</Text>
					</View>
				</View>
			</View>
		);
	}

	renderAlumnos() {
		let buttonsAlumnos = [];
		this.state.alumnos.forEach((itemAlumno, indexAlumno) => {
			buttonsAlumnos.push(this.renderAlumno(itemAlumno, indexAlumno));
		});
		return buttonsAlumnos;
	}

	render() {
		const asistencia = this.renderGAsists();
		const inasistencia = this.renderGFaltas();
		const alumnos = this.renderAlumnos();
		const hoy = InfoInasistDirec.fechaHoy();
		let btnHoy = [
			styles.mediumBtn,
			{
				borderColor: this.state.secondColor,
				height: responsiveHeight(5),
				justifyContent: 'center',
				alignItems: 'center'
			}
		];
		let texto = [
			styles.textoN,
			{fontSize: responsiveFontSize(1.75), fontWeight: '700'}
		];
		if (this.state.fechaH === true) {
			btnHoy.push(styles.mediumBtn, {
				borderColor: this.state.secondColor,
				backgroundColor: this.state.secondColor,
				height: responsiveHeight(5),
				justifyContent: 'center',
				alignItems: 'center'
			});
			texto.push(styles.textoB, {
				fontSize: responsiveFontSize(1.75),
				fontWeight: '700'
			});
		}
		return (
			<View style={styles.container}>
				<Modal
					isVisible={this.state.isModalalumno}
					backdropOpacity={0.8}
					animationIn={'bounceIn'}
					animationOut={'bounceOut'}
					animationInTiming={1000}
					animationOutTiming={1000}
				>
					<View style={[styles.container, {borderRadius: 6,flex:0}]}>
						<Text style={[styles.main_title, styles.modalWidth]}>
							Lista de alumnos
						</Text>
						<View
							style={[styles.rows_tit, styles.modalWidth, {marginTop: 10}]}
						>
							<Text style={{marginLeft: 10}}>Nombre del Alumno</Text>
							<Text style={{marginRight: 15}}>Faltas</Text>
						</View>
						<ScrollView>
							<View
								style={[
									styles.tabla,
									{borderColor: this.state.secondColor, marginTop: 10}
								]}
							>
								{alumnos}
							</View>
						</ScrollView>
						<TouchableOpacity
							style={[
								styles.modalBigBtn,
								{backgroundColor: '#fff', borderColor: this.state.secondColor}
							]}
							onPress={() => this.setState({isModalalumno: false,})}
						>
							<Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
						</TouchableOpacity>
					</View>
				</Modal>
				<ScrollView>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione la fecha
					</Text>
					<View style={styles.rowsCalif}>
						<TouchableHighlight
							underlayColor={this.state.secondColor}
							style={btnHoy}
							onPress={() => {
								if (this.state.fechaH === false) {
									this.setState({fechaH: true});
								}
							}}
						>
							<Text style={texto}>Hoy {hoy}</Text>
						</TouchableHighlight>
						<TouchableOpacity
							style={[styles.mediumBtn,
								{
									borderColor: this.state.secondColor,
									height: responsiveHeight(5),
									justifyContent: 'center',
									alignItems: 'center'
								}]}
							onPress={this._showDateTimePicker}
						>
							{this.state.fecha1 === '' ? (
								<Text style={{fontSize: responsiveFontSize(1.5)}}>
									Seleccione una fecha
								</Text>
							) : (
								<Text style={{fontSize: responsiveFontSize(1.5)}}>
									{this.state.fecha1}
								</Text>
							)}
						</TouchableOpacity>
					</View>
					<DateTimePicker
						locale={'es'}
						isVisible={this.state.isDateTimePickerVisible}
						onConfirm={this._handleDatePicked}
						onCancel={this._hideDateTimePicker}
						titleIOS={'Seleccione una fecha'}
						confirmTextIOS={'Seleccionar'}
						cancelTextIOS={'Cancelar'}
					/>
					<GradoyGrupo
						onListItemPressedGrupos={(indexGrupo, itemGrupo) =>
							this.onListItemPressedGrupos(indexGrupo, itemGrupo)
						}
						onListItemPressedGrado={(indexGrado, itemGrado) =>
							this.onListItemPressedGrado(indexGrado, itemGrado)
						}
						todos={'1'}
					/>
					<View style={[styles.rows_tit, {width: responsiveWidth(94)}]}>
						<View
							style={[
								styles.btn_blue,
								{
									paddingHorizontal: 10,
									width: responsiveWidth(45),
									height: responsiveHeight(11),
									backgroundColor: this.state.fourthColor
								}
							]}
						>
							<Text
								style={{
									fontSize: responsiveFontSize(1.5),
									textAlign: 'center'
								}}
							>
								Cantidad de inasistencias del día de hoy:
							</Text>
							<Text style={{fontSize: 25, marginTop: 5, fontWeight: '800'}}>
								{this.state.laAsistencia}
							</Text>
						</View>
						<View
							style={[
								styles.btn_blue,
								{
									paddingHorizontal: 10,
									height: responsiveHeight(11),
									backgroundColor: this.state.fourthColor,
									width: responsiveWidth(45)
								}
							]}
						>
							<Text
								style={{
									fontSize: responsiveFontSize(1.5),
									textAlign: 'center'
								}}
							>
								Porcentaje de asistencia del día de hoy:
							</Text>
							<Text style={{fontSize: 25, marginTop: 5, fontWeight: '800'}}>
								{this.state.hoy.porcentaje}%
							</Text>
						</View>
					</View>
					<View
						style={{
							width: responsiveWidth(94),
							alignItems: 'center',
							marginTop: 5
						}}
					>
						<TouchableOpacity
							style={[
								styles.bigButton,
								{
									borderColor: this.state.secondColor,
									width: responsiveWidth(47),
									height: responsiveHeight(5),
									borderWidth: 1,
									borderRadius: 6,
									backgroundColor: 'white'
								}
							]}
							onPress={() => this.setState({isModalalumno: true})}
						>
							<Text style={[styles.textButton, {color: 'black'}]}>
								Ver lista
							</Text>
						</TouchableOpacity>
					</View>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Detalles de grupos
					</Text>
					<View
						style={{
							marginTop: 10,
							width: responsiveWidth(94),
							flexDirection: 'row',
							justifyContent: 'space-between'
						}}
					>
						<Text
							style={{
								marginLeft: 40,
								fontWeight: '700',
								textAlign: 'center'
							}}
						>
							Grupos con{'\n'}más asistencia
						</Text>
						<Text
							style={{
								marginRight: 40,
								fontWeight: '700',
								textAlign: 'center'
							}}
						>
							Grupos con{'\n'}más inasistencia
						</Text>
					</View>
					<View
						style={{
							width: responsiveWidth(94),
							flexDirection: 'row',
							justifyContent: 'space-between'
						}}
					>
						<View style={{width: responsiveWidth(47), alignItems: 'center'}}>
							{asistencia}
						</View>
						<View
							style={{
								width: responsiveWidth(47),
								borderLeftWidth: 1,
								alignItems: 'center',
								borderColor: this.state.fourthColor
							}}
						>
							{inasistencia}
						</View>
					</View>
				</ScrollView>
				{/*<TouchableOpacity*/}
				{/*style={[*/}
				{/*styles.bigButton,*/}
				{/*{*/}
				{/*backgroundColor: this.state.mainColor,*/}
				{/*width: responsiveWidth(100)*/}
				{/*}*/}
				{/*]}*/}
				{/*>*/}
				{/*<Text style={styles.textButton}>Imprimir informe</Text>*/}
				{/*</TouchableOpacity>*/}
			</View>
		);
	}
}
