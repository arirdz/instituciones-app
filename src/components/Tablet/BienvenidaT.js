import React from 'react';
import {
    AsyncStorage, Image, ImageBackground, Platform, StatusBar, StyleSheet, Text, TouchableHighlight, TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import styles from './Tstyles';

export default class BienvenidaT extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            textInputValue: '',
            uri: 'https://controlescolar.pro',
            cososPicker: ['Seleccione Escuela'],
            configData: [],
            itemBtn: 'Primaria',
            itemBtns: ['Primaria', 'Secundaria', 'Preparatoria', 'Universidad'],
            selectedIndexBtn: 0,
            estados: [],
            estado: '',
            ciudades: [''],
            ciudad: '',
            escuelas: [''],
            aux: 0
        };
    }

    async componentDidMount() {
        await this.getEstados();
        await this.reload;
    }

    async getEstados() {
        let estado = await fetch('https://controlescolar.pro/api/get/estados/escuelas');
        let estados = await estado.json();
        await this.setState({estados: estados});
    }

    async onChangeEstados(option) {
        await this.setState({estado: option.label});
        await this.getCiudades();
    }

    async getCiudades() {
        let ciudad = await fetch('https://controlescolar.pro/api/get/ciudades/escuelas/' + this.state.estado);
        let ciudades = await ciudad.json();
        await this.setState({ciudades: ciudades});
    }

    async onChangeCiudades(option) {
        await this.setState({ciudad: option.label});
        await this.getEscuelas();
    }

    async getEscuelas() {
        let escuela = await fetch('https://controlescolar.pro/api/get/escuelas/escuelas/' + this.state.estado + '/' + this.state.ciudad + '/' + this.state.itemBtn);
        let escuelas = await escuela.json();
        await this.setState({escuelas: escuelas});
    }

    async onChange(option) {
        await this.setState({
            textInputValue: option.label,
            uri: option.url,
            ide: option.aydii,
            fase:option.fase
        });
        await this.globalConfigs();
    }

    async globalConfigs() {
        let keys = await AsyncStorage.getAllKeys();
        let getConfig = await fetch(this.state.uri + '/api/global/config/values');
        let configSave = await getConfig.json();
        await this.setState({configData: configSave});
        await this.setState({aux: 0});
    }

    renderbtns() {
        let buttonsBtns = [];
        this.state.itemBtns.forEach((itemBtn, indexBtn) => {
            buttonsBtns.push(this.renderBtn(itemBtn, indexBtn));
        });
        return buttonsBtns;
    }

    renderBtn(itemBtnTop, indexBtnTop) {
        let texto = [styles.textoN];
        let btn = [
            styles.btncuatro,
            {height: responsiveHeight(5.4)}
        ];

        if (this.state.selectedIndexBtn === indexBtnTop) {
            texto.push([styles.textoB, {textAlign: 'center'}]);
            btn.push({
                backgroundColor: '#126775',
                borderRadius: 5,
                padding: 0,
                width: responsiveWidth(15.5)
            });
        }
        if (indexBtnTop === 0) {
            return (<TouchableHighlight
                style={[styles.btnIzq, btn]}
                key={indexBtnTop}
                onPress={() => this.onListItemPressedBtn(indexBtnTop, itemBtnTop)}
                underlayColor={'transparent'}>
                <Text style={[styles.txtCenter, texto]}>{itemBtnTop}</Text>
            </TouchableHighlight>)
        } else if (indexBtnTop === 1) {
            return (<TouchableHighlight
                style={[styles.btnCentro, btn]}
                key={indexBtnTop}
                onPress={() => this.onListItemPressedBtn(indexBtnTop, itemBtnTop)}
                underlayColor={'transparent'}>
                <Text style={[styles.txtCenter, texto]}>{itemBtnTop}</Text>
            </TouchableHighlight>)
        } else if (indexBtnTop === 2) {
            return (<TouchableHighlight
                style={[styles.btnDer, btn]}
                key={indexBtnTop}
                onPress={() => this.onListItemPressedBtn(indexBtnTop, itemBtnTop)}
                underlayColor={'transparent'}>
                <Text style={[styles.txtCenter, texto]}>{itemBtnTop}</Text>
            </TouchableHighlight>)
        } else if (indexBtnTop === 3) {
            return (<TouchableHighlight
                style={[styles.btnDer, btn]}
                key={indexBtnTop}
                onPress={() => this.onListItemPressedBtn(indexBtnTop, itemBtnTop)}
                underlayColor={'transparent'}>
                <Text style={[styles.txtCenter, texto]}>{itemBtnTop}</Text>
            </TouchableHighlight>)
        }
    }

    async onListItemPressedBtn(indexBtn, itemBtn) {
        await this.setState({selectedIndexBtn: indexBtn, itemBtn: itemBtn});
        await this.getEscuelas();
    }

    async saveData(uri, ide) {
        await this.setState({aux: 0});
        try {
            let ide2 = await ide.toString();
            await AsyncStorage.setItem('uri', uri);
            await AsyncStorage.setItem('schoolId', ide2);
            let mc1 = await this.state.configData[0].second_option.toString();
            let sc1 = await this.state.configData[1].second_option.toString();
            let tc1 = await this.state.configData[2].second_option.toString();
            let fc1 = await this.state.configData[3].second_option.toString();
            await AsyncStorage.setItem('mainColor', mc1);
            await AsyncStorage.setItem('secondColor', sc1);
            await AsyncStorage.setItem('thirdColor', tc1);
            await AsyncStorage.setItem('fourthColor', fc1);
            await AsyncStorage.setItem('fase', this.state.fase);
            let toMain = Actions.push('AuthenticationT');
            await this.setState({aux: 0});
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    async continuar() {
        if (this.state.escuela === 0) {
        }
    }

    async componentWillUnmount() {
        await this.reload;
    }

    render() {
        let estados = this.state.estados.map((item, i) => {
            return {
                key: i,
                label: item
            };
        });
        let ciudades = this.state.ciudades.map((item, i) => {
            return {
                key: i,
                label: item
            };
        });
        let escuelas = this.state.escuelas.map((item, i) => {
            return {
                key: i,
                url: item.url_escuela,
                label: item.nombre_escuela,
                aydii: item.id,
                fase: item.plan
            };
        });
        const btn123 = this.renderbtns();
        return (
            <ImageBackground
                style={{width:responsiveWidth(100), height:responsiveHeight(100),alignItems: 'center'} }
                imageStyle={{ resizeMode: 'stretch' }}
                source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
            >

                <StatusBar
                    backgroundColor='#126775'
                    barStyle={'light-content'}
                />



                    {Platform.OS === 'ios' ? <View style={[Styles.logoContainer, {marginTop: responsiveHeight(15)}]}>
                        <Image
                            style={Styles.logo}
                            source={{
                                uri: 'https://controlescolar.pro/images/logotipo.png'
                            }}
                        />
                    </View> : <View style={[Styles.logoContainer, {marginTop: 85}]}>
                        <Image
                            style={Styles.logo}
                            source={{
                                uri: 'https://controlescolar.pro/images/logotipo.png'
                            }}
                        />
                    </View>}

                    <View style={Styles.marTop}/>


                <ModalSelector
                    data={estados}
                    selectStyle={[
                        [styles.inputPicker, {borderColor: '#126775', backgroundColor: 'transparent'}],
                        {marginTop: 0}
                    ]}
                    cancelText='Cancelar'
                    optionTextStyle={{color: '#75745a'}}
                    initValue='Seleccione el Estado'
                    onChange={option => this.onChangeEstados(option)}
                />
                <View style={Styles.marTop2}/>
                <ModalSelector
                    data={ciudades}
                    selectStyle={[
                        [styles.inputPicker, {borderColor: '#126775', backgroundColor: 'transparent'}],
                        {marginTop: 0}
                    ]}
                    cancelText='Cancelar'
                    optionTextStyle={{color: '#75745a'}}
                    initValue='Seleccione la Ciudad'
                    onChange={option => this.onChangeCiudades(option)}
                />
                <View style={Styles.marTop2}/>
                <View
                    style={[
                        styles.row,
                        {
                            paddingTop: 0,
                            borderWidth: 1,
                            borderColor: '#126775',
                            height: responsiveHeight(6.1),
                            alignItems: 'center',
                            borderRadius: 6,
                            justifyContent: 'center',
                            width: responsiveWidth(63)
                        }
                    ]}>
                    {btn123}
                </View>
                <View style={Styles.marTop2}/>
                    <ModalSelector
                        data={escuelas}
                        selectStyle={[
                            [styles.inputPicker, {borderColor: '#126775', backgroundColor: 'transparent'}],
                            {marginTop: 0}
                        ]}
                        cancelText='Cancelar'
                        optionTextStyle={{color: '#75745a'}}
                        initValue='Seleccione la Escuelas'
                        onChange={option => this.onChange(option)}
                    />
                <View style={{marginTop: 8}}/>
                    <TouchableOpacity
                        onPress={() => this.saveData(this.state.uri, this.state.ide)}
                        style={[styles.bigButtonLogin, {borderColor: '#126775'}]}
                    >
                        <Text style={Styles.textButton}>Continuar</Text>
                    </TouchableOpacity>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                />
            </ImageBackground>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1
    },
    logoContainer: {
        marginTop: 200
    },
    inputPicker: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        padding: 10,
        marginTop: 10,
        width: responsiveWidth(94),
        alignItems: 'center',
        justifyContent: 'center'
    },
    textButton: {
        color: 'white',
        fontSize: responsiveFontSize(2),
        textAlign: 'center',
        fontWeight: '700'
    },
    logo: {
        height: responsiveHeight(20),
        marginBottom: 30,
        resizeMode: 'contain',
        width: responsiveWidth(70)
    },
    marTop: {
        marginTop: responsiveHeight(10)
    },
    marTopp: {
        marginTop: responsiveHeight(5)
    }
    , marTop2: {
        marginTop: responsiveHeight(2)
    }
});
