import React from 'react';import {
    StyleSheet,
    Image,
    Button,
    PixelRatio,
    TouchableOpacity,
    NativeModules,
    Alert,
    TextInput,
    Modal
} from 'react-native';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';
import {AsyncStorage, Text, View, TouchableHighlight} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Gallery from 'react-native-image-gallery';
import {Actions} from "react-native-router-flux";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import Entypo from "react-native-vector-icons/Entypo";
import ImageBrowser from 'react-native-interactive-image-gallery';

import styles from '../Tstyles'
import FontAwesome from "react-native-vector-icons/FontAwesome";


export default class galeria extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null,
            images: null,
            modalVisible: false,
            modalVisible1: false,
            albumes: [],
            row: false,
            agregar: true,
            imagenes: [],
            imageURLs: []
        }
    }

    async componentWillMount() {
        await this.getURL();
        await this.getAlbumes();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            text: ''
        });
    }

    async getAlbumes(){
        await fetch(this.state.uri + '/api/get/albumes',
            {
                method: 'get',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();

            }).then(responseJson => {
                if (responseJson.error === null) {
                    Alert.alert('¡Ups! Ha ocurrido un error , si el error continua pónganse en contacto con soporte (Noticias)');
                } else {
                    this.setState({albumes: []});
                    this.setState({albumes: responseJson});
                }
            });
    };

    pickMultiple() {
        ImagePicker.openPicker({
            multiple: true,
            waitAnimationEnd: false,
            includeExif: true,
            forceJpg: true,
        }).then(images => {
            this.setState({
                image: null,
                images: images
                // images: images.map(i => {
                //     console.log('received image', i);
                //     return {uri: i.path, width: i.width, height: i.height, mime: i.mime};
                // })
            });
        }).catch(e => alert(e));
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    setModalVisible1(visible) {
        this.setState({modalVisible1: visible});
    }

    saveImages() {
        if(this.state.images === null){
            Alert.alert('Error', 'No se han seleccionado las imágenes');
        }else if (this.state.text === '') {
            Alert.alert('Error', 'Ingrese el nombre del álbum');
        }else {
        for (let i = 0; i < this.state.images.length; i++) {
            const fd = new FormData();
            let imagen = this.state.images[i];
            fd.append('image',
                {
                    uri: this.state.images[i].path,
                    type: 'file',
                    name: 'photo',

                });
            fd.append('album', this.state.text)
            fetch(this.state.uri + '/api/post/imagenes', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: 'Bearer ' + this.state.token
                },
                body: fd
            }).then(res => {
            });
            if (i + 1 === this.state.images.length) {
                this.rndr()
            }
        }
        }

    };

    async rndr() {
        await Alert.alert('alert', 'Se han guardado las imágenes con éxito', [{text: 'Entereado'}]);

    }
    async borrar(img){
        await Alert.alert('Desea borrar la imágen', '',[{text: 'Si', onPress:() => this.deleteImg()}, {text: 'No'}]);
    }

    async deleteImg(item){
        // await Alert.alert('Felicidades', 'Se ha borrado la imágen con éxito', [{text: 'Entereado'}]);
        await this.setModalVisible(false)
    }



    card(){
        let carta = [];
        this.state.albumes.forEach((item) => {
            carta.push(
                <View
                    style={[
                        styles.widthall,
                        // styles.row,
                        {
                            height: responsiveHeight(8),
                            marginVertical: 5,
                            borderWidth: 0.5,
                            borderRadius: 6,
                            borderColor: 'lightgray',
                            backgroundColor: this.state.fourthColor,
                            paddingHorizontal: 6,
                        }
                    ]}>
                   <View style={[styles.row]}>
                       <View style={[styles.btn3]}>
                           <Text style={[styles.textW,{textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                               Nombre del álbum
                           </Text>
                           <Text style={{fontSize: responsiveFontSize(1.5), textAlign: 'center', marginTop: 10}}>{item.nombre}</Text>
                       </View>
                       <View style={[styles.btn3]}>
                           <Text style={[styles.textW,{textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                               Fecha
                           </Text>
                           <Text style={{fontSize: responsiveFontSize(1.5), textAlign: 'center', marginTop: 10}}>{moment(item.created_at).format('L')}</Text>
                       </View>
                       <View style={[styles.btn3, {alignItems: 'center'}]}>
                           <Text style={[styles.textW,{textAlign: 'center', fontSize: responsiveFontSize(1.3)}]}>
                               Acciones
                           </Text>
                           <View style={[styles.row,styles.btn5, {marginTop: 10}]}>
                           <FontAwesome name='trash' size={20} color='#000' onPress={() =>this.setState(this.borrarAlbum(item))}/>
                           <MaterialIcons name='add-a-photo' size={20} color='#000'/>
                           <Entypo name="eye" size={20} color="#000" onPress={() =>this.setState([{row:false, agregar: true}, this.getPictures(item.id)])} />
                           </View>
                       </View>
                   </View>
                </View>
            )
        });
        return carta;
    }

    borrarAlbum(item){
        Alert.alert('Alerta', '¿Está seguro que desea borrar el álbum?',[{text: 'Si', onPress:() => this.deleteAlbum(item)}, {text: 'No'}]);
    }


    deleteAlbum(item){
        fetch(this.state.uri+ '/api/delete/albumes/'+item.id,
            {
                method: 'get',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();
            }).then(responseJson => {
            if (responseJson.error === null) {
                Alert.alert('¡Ups! Ha ocurrido un error , si el error continua pónganse en contacto con soporte (Noticias)');
            } else {
                Alert.alert('', 'Álbum eliminado con éxito',[{text: 'Enterado', onPress:() => this.getAlbumes()}]);
            }
        });
    }

    getPictures(id){
        fetch(this.state.uri+ '/api/get/pictures/'+ id,
            {
                method: 'get',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: 'Bearer ' + this.state.token
                }
            })
            .then(response => {
                return response.json();

            }).then(responseJson => {
            if (responseJson.error === null) {
                Alert.alert('¡Ups! Ha ocurrido un error , si el error continua pónganse en contacto con soporte (Noticias)');
            } else {
                this.setState({pictures: []});
                this.setState({pictures: responseJson});
                if(this.state.pictures.length > 0){
                    this.state.imageURLs = this.state.pictures.map(
                        (img: Object, index: number) => ({
                            URI: img.url,
                            thumbnail: img.url,
                            id: img.id,
                        })
                    );
                    this.setModalVisible(true)
                }else{null}
            }
        });
    };


    render() {
        return (
            <View style={styles.container}>
                {this.card()}
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible1}
                    onRequestClose={() => {
                        Alert.alert('Modal se ha cerrado');
                    }}>
                    <View  style={{backgroundColor: 'black'}}>
                        <TouchableHighlight style={{ paddingTop: 30}}>
                            <Text onPress={() => this.setModalVisible1(false)}
                                  style={{color: 'white', fontSize:25, textAlign:'right', margin: 20, marginBottom: 0}}
                            >X</Text>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <Text>MODAL PARA AGREGAR UN ALBUM Y SUS FOTOS</Text>
                        <TextInput
                            editable = {true}
                            maxLength = {40}
                            onChangeText={(text) => this.setState({text})}
                            style={{backgroundColor: 'red'}}
                        />
                        <TouchableOpacity onPress={this.pickMultiple.bind(this)} style={styles.button}>
                            <Text style={styles.text}>Seleccionar Fotos</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.saveImages()}  style={styles.button}>
                            <Text style={{color: 'white'}}>GUARDAR</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                {this.state.agregar === true ?
                    (<MaterialIcons name='add-circle' size={35} color={this.state.secondColor} onPress={() =>this.setState({modalVisible1:true})}/>):null
                }

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal se ha cerrado');
                    }}>
                    <View  style={{backgroundColor: 'black'}}>
                        <TouchableHighlight style={{ paddingTop: 30}}>
                            <Text onPress={() => this.setModalVisible(false)}
                            style={{color: 'white', fontSize:25, textAlign:'right', margin: 10, marginBottom: 0}}
                            >X</Text>
                        </TouchableHighlight>
                    </View>
                    <ImageBrowser closeText={'Cerrar'} images={this.state.imageURLs} onPressImage={() => console.log('asas')}
                    />
                </Modal>

            </View>
        );
    }

}
