import React from 'react';
import PropTypes from 'prop-types';
import {AsyncStorage, Text, TouchableHighlight, View} from 'react-native';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../Tstyles';

export default class MultiBotonRow extends React.Component {
	static propTypes = {
		itemBtns: PropTypes.array.isRequired,
		cantidad: PropTypes.number.isRequired,
		onSelectedButton: PropTypes.func.isRequired
	};
	static defaultProps = {
		itemBtns: ['Boton 1', 'Boton 2', 'Boton 3', 'Boton 4'],
		cantidad: 2
	};

	constructor(props) {
		super(props);
		this.getURL();
		this.state = {
			itemBtns: [],
			uri: '',
			token: '',
			selectedIndexBtn: 0
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	renderUno(itemBtn, indexBtn) {
		let texto = [styles.textoN];
		let btn = [
			styles.btnUno,
			{
				height: responsiveHeight(5.2),
				borderColor: this.state.secondColor
			}
		];

		if (this.state.selectedIndexBtn === indexBtn) {
			texto.push(styles.textoB);
			btn.push({
				backgroundColor: this.state.secondColor,
				width: responsiveWidth(92.8),
				borderRadius: 5,
				borderWidth: 0
			});
		}
		if (indexBtn === 0) {
			return (<TouchableHighlight
				style={[styles.btnIzq, btn]}
				key={indexBtn + 'uno'}
				underlayColor={'transparent'}
				onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
				<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
			</TouchableHighlight>)
		}
	}

	renderDos(itemBtn, indexBtn) {
		let texto = [styles.textoN];
		let btn = [
			styles.btndos,
			{
				height: responsiveHeight(5.2),
				borderColor: this.state.secondColor
			}
		];

		if (this.state.selectedIndexBtn === indexBtn) {
			texto.push(styles.textoB);
			btn.push({
				backgroundColor: this.state.secondColor,
				width: responsiveWidth(45.7),
				borderRadius: 5,
				borderWidth: 0
			});
		}
		if (indexBtn === 0) {
			return (<TouchableHighlight
				style={[styles.btnIzq, btn]}
				key={indexBtn + 'dos'}
				underlayColor={'transparent'}
				onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
				<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
			</TouchableHighlight>)
		}
		else if (indexBtn === 1) {
			return (
				<TouchableHighlight
					style={[styles.btnDer, btn]}
					key={indexBtn + 'dos'}
					underlayColor={'transparent'}
					onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
					<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
				</TouchableHighlight>
			)
		}
	}

	renderTres(itemBtn, indexBtn) {
		let texto = [styles.textoN];
		let btn = [
			styles.btntres,
			{
				height: responsiveHeight(5.4),
				borderColor: this.state.secondColor
			}
		];

		if (this.state.selectedIndexBtn === indexBtn) {
			texto.push(styles.textoB);
			btn.push({
				backgroundColor: this.state.secondColor,
				width: responsiveWidth(30.9),
				borderRadius: 5,
				borderWidth: 0
			});
		}
		if (indexBtn === 0) {
			return (<TouchableHighlight
				style={[styles.btnIzq, btn]}
				key={indexBtn + 'tres'}
				underlayColor={'transparent'}
				onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
				<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
			</TouchableHighlight>)
		}
		else if (indexBtn === 1) {
			return (<TouchableHighlight
				style={[styles.btnCentro, btn]}
				key={indexBtn + 'tres'}
				underlayColor={'transparent'}
				onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
				<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
			</TouchableHighlight>)
		}
		else if (indexBtn === 2) {
			return (
				<TouchableHighlight
					style={[styles.btnDer, btn]}
					key={indexBtn + 'tres'}
					underlayColor={'transparent'}
					onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
					<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
				</TouchableHighlight>
			)
		}
	}

	renderbtns() {
		let buttonsBtns = [];
		this.props.itemBtns.forEach((itemBtn, indexBtn) => {
			buttonsBtns.push(this.renderBtn(itemBtn, indexBtn));
		});
		return buttonsBtns;
	}

	renderTwo() {
		let buttonsBtns = [];
		this.props.itemBtns.forEach((itemBtn, indexBtn) => {
			buttonsBtns.push(this.renderDos(itemBtn, indexBtn));
		});
		return buttonsBtns;
	}

	renderOne() {
		let buttonsBtns = [];
		this.props.itemBtns.forEach((itemBtn, indexBtn) => {
			buttonsBtns.push(this.renderUno(itemBtn, indexBtn));
		});
		return buttonsBtns;
	}

	rederThreeeta() {
		let buttonsBtns = [];
		this.props.itemBtns.forEach((itemBtn, indexBtn) => {
			buttonsBtns.push(this.renderTres(itemBtn, indexBtn));
		});
		return buttonsBtns;
	}

	renderBtn(itemBtn, indexBtn) {
		let texto = [styles.textoN];
		let btn = [
			styles.btncuatro,
			{
				height: responsiveHeight(5.4),
				borderColor: this.state.secondColor
			}
		];

		if (this.state.selectedIndexBtn === indexBtn) {
			texto.push(styles.textoB);
			btn.push({
				backgroundColor: this.state.secondColor,
				width: responsiveWidth(23.2),
				borderRadius: 5,
				borderWidth: 0
			});
		}
		if (indexBtn === 0) {
			return (<TouchableHighlight
				style={[styles.btnIzq, btn]}
				key={indexBtn + 'cuatro'}
				underlayColor={'transparent'}
				onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
				<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
			</TouchableHighlight>)
		} else if (indexBtn === 1) {
			return (
				<TouchableHighlight
					style={[styles.btnCentro, btn]}
					key={indexBtn + 'cuatro'}
					underlayColor={'transparent'}
					onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
					<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
				</TouchableHighlight>
			)
		} else if (indexBtn === 2) {
			return (
				<TouchableHighlight
					style={[styles.btnCentro1, btn]}
					key={indexBtn + 'cuatro'}
					underlayColor={'transparent'}
					onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
					<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
				</TouchableHighlight>
			)
		} else if (indexBtn === 3) {
			return (
				<TouchableHighlight
					style={[styles.btnDer, btn]}
					key={indexBtn + 'cuatro'}
					underlayColor={'transparent'}
					onPress={() => this.onSelectedButton(indexBtn, itemBtn)}>
					<Text style={[styles.txtCenter, texto]}>{itemBtn}</Text>
				</TouchableHighlight>
			)
		}
	}

	async onSelectedButton(indexBtn, itemBtn) {
		await this.props.onSelectedButton(indexBtn, itemBtn);
		await this.setState({selectedIndexBtn: indexBtn, itemBtn: itemBtn});
	}

	render() {
		let btn123 = this.renderbtns();
		if (this.props.cantidad === 2) {
			btn123 = this.renderTwo();
		} else if (this.props.cantidad === 3) {
			btn123 = this.rederThreeeta();
		} else if (this.props.cantidad === 1) {
			btn123 = this.renderOne();
		}
		return (
			<View
				style={[
					styles.row,
					{
						marginTop: 5,
						marginBottom: 2,
						backgroundColor: '#f2f2f2',
						borderWidth: 1,
						borderColor: this.state.secondColor,
						height: responsiveHeight(6),
						alignItems: 'center',
						borderRadius: 6,
						justifyContent: 'center',
						width: responsiveWidth(94)
					}
				]}>
				{btn123}
			</View>
		);
	}
}
