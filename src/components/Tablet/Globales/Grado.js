import React from 'react';
import PropTypes from 'prop-types';
import {AsyncStorage, Text, View, TouchableHighlight} from 'react-native';
import {responsiveHeight} from 'react-native-responsive-dimensions';
import styles from '../Tstyles';
import {MediaQuery} from 'react-native-responsive';

export default class Grados extends React.Component {
	static propTypes = {
		urlGrado: PropTypes.string,
		objetoGrado: PropTypes.string,
		onListItemPressedGrado: PropTypes.func.isRequired
	};

	static defaultProps = {
		urlGrado: '/api/get/grados',
		objetoGrado: 'grados',
		todos: '0'
	};

	constructor(props) {
		super(props);
		this.state = {
			selectedIndexGrados: 0,
			grado: 1,
			grados: [],
			uri: '',
			token: '',
			todos: '0'
		};
	}

	async componentWillMount() {
		await this.getURL();
		await this.getGrados();
	}

	async getURL() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let maincolor = await AsyncStorage.getItem('mainColor');
		let secondColor = await AsyncStorage.getItem('secondColor');
		let thirdColor = await AsyncStorage.getItem('thirdColor');
		let fourthColor = await AsyncStorage.getItem('fourthColor');
		this.setState({
			uri: uri,
			token: token,
			mainColor: maincolor,
			secondColor: secondColor,
			thirdColor: thirdColor,
			fourthColor: fourthColor
		});
	}

	async getGrados() {
		let gradpicker = await fetch(this.state.uri + this.props.urlGrado);
		let gradospicker = await gradpicker.json();
		this.setState({
			grados: gradospicker
		});
	}

	async onListItemPressedGrado(indexGrado, grado) {
		await this.props.onListItemPressedGrado(indexGrado, grado);
		await this.setState({
			selectedIndexGrados: indexGrado,
			grado: grado
		});
	}

	renderList() {
		let buttons = [];
		this.state.grados.forEach((item, index) => {
			buttons.push(this.renderItem(item, index));
		});
		return buttons;
	}

	renderItem(listItem, index) {
		let smallButtonStyles = [
			styles.listButton, styles.listButtonSmall, styles.btn6,{borderColor: this.state.secondColor}
		];
		let texto = [styles.textoN];

		if (this.state.selectedIndexGrados === index) {
			smallButtonStyles.push(styles.listButtonSelected, {
				backgroundColor: this.state.secondColor
			});
			texto.push(styles.textoB);
		}
		if (listItem.grado !== 'Todos') {
			return (
				<TouchableHighlight
					key={index + 'gr'}
					underlayColor={this.state.secondColor}
					style={smallButtonStyles}
					onPress={() => this.onListItemPressedGrado(index, listItem.grado)}>
					<View style={styles.listItem}>
						<Text style={texto}>{listItem.grado}</Text>
					</View>
				</TouchableHighlight>
			);
		}
	}

	render() {
		const list = this.renderList();
		return (
			<View>
				{/* para iphone */}
				<MediaQuery maxDeviceHeight={896}>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Seleccione el grado
					</Text>
					<View style={{height: responsiveHeight(9)}}>
						<Text style={styles.subTitulo}>Grado</Text>
						<View style={styles.buttonsRow}>{list}</View>
					</View>
				</MediaQuery>
				{/* para ipad */}
				{/* <MediaQuery maxDeviceWidth={1024} minDeviceHeight={897}>
          <View>
            <View style={altura}>
              <View>
                <Text
                  style={[styles.main_title, { color: this.state.thirdColor }]}
                >
                  Seleccione grado
                </Text>
              </View>
              <Text style={styles.subTitulo}>Grado</Text>
              <View style={styles.buttonsRow}>{list}</View>
            </View>
          </View>
        </MediaQuery> */}
			</View>
		);
	}
}
