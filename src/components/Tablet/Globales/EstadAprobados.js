import React from "react";
import {
    Text,
    View,
    AsyncStorage,
} from "react-native";
import {
    responsiveFontSize,
} from "react-native-responsive-dimensions";
import styles from "../Tstyles";
import ModalSelector from "react-native-modal-selector";

export default class EstadAprobados extends React.Component {
    constructor(props) {
        super(props);
        this.getURL();
        this.state = {
            data: [],
            items: {},
            laSolicitud: "",
            solicitudes: [],
            botonSelected: "Aprobados",
            botonSelected1: "Academicos",
            selectedIndexGrados: -1,
            grado: "",
            selectedIndexGrupos: -1,
            grupo: "",
            selectedIndexPeriodo: -1,
            elPeriodo: ""
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor, marginTop: 10}
                    ]}>
                    Seleccione la materia
                </Text>
                <ModalSelector
                    cancelText="Cancelar"
                    initValue="Seleccione la materia"
                    onChange={option => this.onChange(option)}
                    optionTextStyle={{color: this.state.thirdColor}}
                    selectStyle={[
                        styles.inputPicker,
                        {borderColor: this.state.secondColor, marginTop: 1}
                    ]}
                />
                <Text
                    style={[
                        styles.textButton,
                        {
                            marginTop: 10,
                            marginBottom: 8,
                            color: "black"
                        }
                    ]}>
                    Aprobados
                </Text>
                <View
                    style={[
                        styles.row_v3,
                        styles.materiaCord,
                        {borderColor: this.state.secondColor}
                    ]}>
                    <View style={styles.porcentNum}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(5)
                            }}>
                            240
                        </Text>
                    </View>

                    <View
                        style={[
                            styles.porcentNum,
                            {borderLeftWidth: 1, borderColor: this.state.secondColor}
                        ]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(5)
                            }}>
                            80%
                        </Text>
                    </View>
                </View>

                <View>
                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text> </Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Alumnos</Text>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Faltas</Text>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Incidencias</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>9.5{"\n"}o más</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {"\n"}9.0 y 9.4</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {"\n"}8.0 y 8.9</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {"\n"}7.9 y 7.0</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Entre {"\n"}6.0 y 6.9</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Menos de {"\n"}5.9</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {borderColor: this.state.secondColor}
                                    ]}>
                                    <Text>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {borderColor: this.state.secondColor}
                                ]}>
                                <Text>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={[styles.btn5, styles.centered_RT]}>
                            <Text style={styles.centeredTxt}>Totales</Text>
                        </View>
                        <View style={[styles.btn3, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.row_v3,
                                    styles.btn3,
                                    styles.cuadroEstd,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <View style={[styles.cuadPorcent2, styles.btn6]}>
                                    <Text style={{color: "white"}}>240</Text>
                                </View>
                                <View
                                    style={[
                                        styles.porcentNum2,
                                        styles.btn6,
                                        {
                                            borderColor: "#fff"
                                        }
                                    ]}>
                                    <Text style={{color: "white"}}>80%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.btn6, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn6,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <Text style={{color: "white"}}>666</Text>
                            </View>
                        </View>
                        <View style={[styles.btn4, styles.centered_RT]}>
                            <View
                                style={[
                                    styles.cuadroEstd2,
                                    styles.btn4,
                                    {
                                        borderColor: this.state.secondColor,
                                        backgroundColor: this.state.secondColor
                                    }
                                ]}>
                                <Text style={{color: "white"}}>666</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <Text
                    style={[
                        styles.main_title,
                        {color: this.state.thirdColor, marginTop: 10}
                    ]}>
                    Alumnos
                </Text>
                <View
                    style={[
                        styles.contAlumnos,
                        {
                            marginTop: 3,
                            borderRadius: 6,
                            borderWidth: 1,
                            borderColor: "black",
                            borderColor: this.state.secondColor
                        }
                    ]}
                />
                <Text style={[styles.main_title, {marginTop: 10}]}>
                    Materias con mejores promedio, en la seleccion actual
                </Text>
                <View style={styles.campoTablaG}>
                    <View style={[styles.row, styles.modalWidth, {paddingTop: 0}]}>
                        <View>
                            <Text style={{fontWeight: "700"}}>Materia 1</Text>
                        </View>
                        <View>
                            <Text style={{fontWeight: "700"}}>promedio</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
