import React from 'react';
import {Alert, AsyncStorage, Text, TouchableOpacity, View} from 'react-native'
import {Actions} from 'react-native-router-flux';
import TimeAgo from 'react-native-timeago';
import styles from '../Tstyles';

export default class Noticias extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            enterados: ''
        };
    }

    async getURL() {
        let auth = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let role = await AsyncStorage.getItem('role');
        let hijo = await AsyncStorage.getItem('hijo');
        await this.setState({
            uri: uri,
            token: auth,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            role: role,
            hijo: hijo
        });
    }

    async componentWillMount() {
        await this.getURL();
        await this.getEnterado();
    }


    async getEnterado() {
        let id = this.props.id;
        let url = this.state.uri + '/api/feed/view/enterados/' + id;
        if (this.state.role === 'Padre') {
            url = this.state.uri + '/api/feed/view/enterados/' + id + '/' + this.state.hijo;
        }
        await fetch(url, {
            method: 'GET', headers: {
                Accept: 'application/json', 'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Card Noticia)', [{
                        text: 'Entendido', onPress: () => Actions.drawer()
                    }]);
                } else {
                    this.setState({enterados: responseJson});
                }
            });
    }

    isEnterado() {
        return (<View
            style={[styles.enteradoBtnOutside, {backgroundColor: this.state.mainColor}]}>
            <Text style={styles.textEnterado}>Leído</Text>
        </View>);
    }

    noEnterado() {
        return (<View
            style={[styles.enteradoBtnOutside, {backgroundColor: this.state.secondColor}]}>
            <Text style={styles.textEnterado}>Por leer</Text>
        </View>);
    }

    render() {
        const onPressButton = () => Actions.newsSingle({data: this.props});
        return (<TouchableOpacity onPress={onPressButton} style={styles.card}>
            <View
                style={styles.noticia}>
                <View style={styles.head}>
                    <Text numberOfLines={1} style={styles.titulo_noticia}>
                        {this.props.titulo}
                    </Text>
                    <TimeAgo
                        style={styles.fecha}
                        interval={20000}
                        time={this.props.created_at}
                    />
                </View>
                <View
                    style={styles.descripcion}
                ><Text>{this.props.resumen}</Text></View>
                {this.state.enterados !== 0 ? this.isEnterado() : this.noEnterado()}
            </View>
        </TouchableOpacity>);
    }
}
