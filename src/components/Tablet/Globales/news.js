import React from 'react';
import {AsyncStorage, FlatList, StatusBar, Text, View, Platform} from 'react-native';
import Noticias from './NewsCards';
import ModalSelector from 'react-native-modal-selector';
import Hijos from './hijos';
import styles from '../Tstyles';
import MultiBotonRow from './MultiBotonRow';
import VerEnterados from './verEnterados';
import NuevaPublicacion from './NuevaPublicacion';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {responsiveHeight, responsiveFontSize} from 'react-native-responsive-dimensions'

const moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class News extends React.Component {
    _keyExtractor = (item) => item.id.toString();

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            textInputValue: '',
            puesto: '',
            grado: 'Todos',
            grupo: 'Todos',
            cososPicker: [],
            selectedIndex: null,
            ninosPicker: [],
            mes: moment().month() + 1,
            botonSelected: 'Noticias',
            elHijo: ''
        };
        // this.getFeed = this.getFeed.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    async componentWillMount() {
        await this.getURL();
        await this.getMeses();
        if (this.state.admin === 'Padre' || this.state.admin === 'Alumno') {
            await this.showPadreNoticias();
        } else if ((this.state.admin === 'Admin') || (this.state.admin === 'Maestro')) {
            await this.getFeed();
        }
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let token = await AsyncStorage.getItem('token');
        let role = await AsyncStorage.getItem('role');
        let puesto = await AsyncStorage.getItem('puesto');
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            token: token,
            admin: role,
            puesto: puesto
        });
    }

    async onChange(option) {
        this.setState({data:[]});
        await this.setState({mes: option.mes});
        if ((this.state.admin === 'Admin') || (this.state.admin === 'Maestro')) {
            await this.getFeed();
        }
        else {
            await this.showPadreNoticias();
        }
    }

    async getMeses() {
        let dataPicker = await fetch(this.state.uri + '/api/config/extra/meses', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let datosPicker = await dataPicker.json();
        this.setState({cososPicker: datosPicker});
    }

    async getFeed() {
        this.setState({data:[]});
        let request = await fetch(this.state.uri + '/api/feed/show/' + this.state.mes, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let data = await request.json();
        this.setState({data: data});
    }

    async botonSelected(indexSelected, itemSelected) {
        await this.setState({
            botonSelected: itemSelected, indexSelected: indexSelected
        });
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index, selGrad: grado, selGrup: grupo, elHijo: id
        });
        await AsyncStorage.setItem('hijo', id.toString());
        await this.showPadreNoticias();
    }

	renderView() {
		let data = this.state.cososPicker.map((item, i) => {
			return {key: i, mes: item.mes, label: item.nombre_mes};
		});
		let mess = 'Enero';
		if (this.state.cososPicker !== '') {
			if (this.state.cososPicker.length !== 0) {
				mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
			}
		}
		return (<View>
			{this.state.botonSelected === 'Noticias' ? (
				<View style={{...ifIphoneX({height: responsiveHeight(75)},{height: responsiveHeight(77)})}}>
					<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
						Elija el período a consultar
					</Text>
					<ModalSelector
						data={data}
						selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
						cancelText='Cancelar'
						optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
						selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
						initValue={mess}
						onChange={option => this.onChange(option)}
					/>
					<Text
						style={[styles.main_title, {color: this.state.thirdColor}]}>
						Estas son las últimas noticias:
					</Text>
					{this.state.data !== null ? (
						<FlatList
							keyExtractor={this._keyExtractor}
							data={this.state.data}
							renderItem={({item}) => <Noticias {...item} />}
						/>
					) : null}
				</View>) : null}
			{this.state.botonSelected === 'Publicar\nalgo nuevo' ? (<NuevaPublicacion/>) : null}
			{this.state.botonSelected === 'Revisar\nenterados' ? (<VerEnterados/>) : null}
		</View>);
	}

    isAdmin() {
        const laview = this.renderView();
        return (<View style={styles.container}>
            {this.state.puesto !== 'Prefecto' && this.state.puesto !== 'Cordisi' ? (<MultiBotonRow
                itemBtns={['Noticias', 'Publicar\nalgo nuevo', 'Revisar\nenterados']}
                onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
                cantidad={3}
            />) : null}
            {laview}
        </View>);
    }

    async showPadreNoticias() {
        this.setState({data:[]});
        await fetch(this.state.uri + '/api/feed/show/' + this.state.mes + '/' + this.state.selGrad + '/' + this.state.selGrup + '/' + this.state.elHijo, {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    async showAlumnoNoticias() {
        let request = await fetch(this.state.uri + '/api/feed/show/' + this.state.mes + '/' + this.state.selGrad + '/' + this.state.selGrup + '/' + this.state.elHijo, {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        });
        let data = await request.json();
        this.setState({data: data});
    }

    isNotPapa() {
        let data = this.state.cososPicker.map((item, i) => {
            return {key: i, mes: item.mes, label: item.nombre_mes};
        });
        let mess = 'Enero';
        if (this.state.cososPicker !== '') {
            if (this.state.cososPicker.length !== 0) {
                mess = this.state.cososPicker[this.state.mes - 1].nombre_mes;
            }
        }

        return (<View style={styles.container}>
            {this.state.admin === 'Padre' ? (<View>
                <Hijos
                    onSelectedChamaco={(index, grado, grupo, id) => this.onListItemPressed(index, grado, grupo, id)}
                />
            </View>) : null}
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                Elija el periodo a consultar
            </Text>
            <ModalSelector
                data={data}
                selectStyle={[styles.inputPicker, {borderColor: this.state.secondColor}]}
                cancelText='Cancelar'
                optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.8)}}
                selectTextStyle={{fontSize: responsiveFontSize(1.8)}}
                initValue={mess}
                onChange={option => this.onChange(option)}/>

            <Text style={[styles.main_title, {color: this.state.thirdColor, marginBottom: 5}]}>
                Seleccione una noticia
            </Text>
            {this.state.data !== null
                ? this.state.data.length !== 0
                    ? (
                        <FlatList
                            keyExtractor={this._keyExtractor}
                            data={this.state.data}
                            renderItem={({item}) => <Noticias {...item} />}
                        />
                    )
                    : null
                : null}
        </View>);
    }

	render() {
		return (<View style={[styles.container]}>
			<StatusBar
				backgroundColor={this.state.mainColor}
				barStyle='light-content'
			/>
			{this.state.admin === 'Admin' ? this.isAdmin() : null}
			{this.state.admin === 'Padre' || this.state.admin === 'Maestro' || this.state.admin === 'Alumno' ? this.isNotPapa() : null}
		</View>);
	}
}
