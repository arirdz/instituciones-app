import React from 'react';
import {
    AsyncStorage,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions';
import ModalSelector from 'react-native-modal-selector';
import styles from '../Tstyles';
import Hijos from './hijos';
import GradoyGrupo from './GradoyGrupo';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class newMessage extends React.Component {
    _keyExtractor = (item, index) => item.id;

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            userData: '',
            nombres: [],
            userbyrole: [],
            selectedIndex: 1,
            textInputValue: 'Administrador',
            id: 'Admin',
            materias: [],
            laMateria: []
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getRole();
        await this.getMaterias();
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    //+++++++++++++++++++++++++++++++usuarios
    async getUserdata() {
        let request = await fetch(
            this.state.uri +
            '/api/users/mensajes/' +
            this.state.id +
            '/' +
            this.state.grado +
            '/' +
            this.state.grupo,
            {
                method: 'GET',
                headers: {
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let dato = await request.json();
        await this.setState({nombres: dato});
    }

    async onListPressedName(index, user) {
        await this.setState({
            selectedIndexName: index,
            userData: user
        });
    }

    renderName(itemName, indexName) {
        let smallButtonStyles = [
            styles.listButtonNoticia,
            styles.listButtonSmall,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndexName === indexName) {
            smallButtonStyles.push({
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (
            <TouchableHighlight
                key={indexName}
                underlayColor={this.state.secondColor}
                style={smallButtonStyles}
                onPress={() => this.onListPressedName(indexName, itemName)}>
                <View style={[styles.listItem, {width: responsiveWidth(80)}]}>
                    <Text
                        numberOfLines={1}
                        style={[texto, {width: responsiveWidth(60)}]}>
                        {this.state.id === 'Admin'
                            ? itemName.name
                            : this.state.id === 'Padre'
                                ? itemName.hijos.name
                                : itemName.maestro.name}
                    </Text>
                    <Text
                        numberOfLines={1}
                        style={[
                            texto,
                            {
                                textAlign: 'center',
                                flexWrap: 'wrap',
                                flex: 2,
                                color: 'grey'
                            }
                        ]}>
                        {this.state.id === 'Maestro'
                            ? itemName.nombre.nombre
                            : this.state.id === 'Alumno' ? 'Alumno' : itemName.puesto}
                        {this.state.id === 'Padre' ? itemName.name : null}
                    </Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderNames() {
        let buttonsNames = [];
        this.state.nombres.forEach((itemName, indexName) => {
            buttonsNames.push(this.renderName(itemName, indexName));
        });
        return buttonsNames;
    }

    async onListItemPressed(index, grado, grupo) {
        await this.setState({
            selectedIndex: index,
            grado: grado,
            grupo: grupo
        });
    }

    async onListItemPressedName(index) {
        await this.setState({
            selectedIndexName: index
        });
    }

    async onChange(option) {
        await this.setState({textInputValue: option.label, id: option.id});
        await this.getUserdata();
    }

    async onListItemPressedGrado(index, grado) {
        await this.setState({
            selectedIndex: index,
            grado: grado
        });
    }

    async onListItemPressedGrupo(index, grupo) {
        await this.setState({
            selectedIndex: index,
            grupo: grupo
        });
        await this.getUserdata();
    }

    //++++++++++++++++++++materia alumno
    async getMaterias() {
        let materiapicker = await fetch(
            this.state.uri + '/api/get/materias/alumno',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let materiaspicker = await materiapicker.json();
        this.setState({materias: materiaspicker});
    }

    isAlumno() {
        const nombres = this.renderNames();
        let data1 = this.state.materias.map((item, i) => {
            return {
                grado: item.materia.materia[0].grado,
                key: i,
                label:
                item.materia.materia[0].nombre + ' ' + item.materia.materia[0].grado,
                materia: item.materia.id_materia
            };
        });
        return (
            <View>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione la materia
                </Text>
                <View style={[styles.asuntos]}>
                    <ModalSelector
                        data={data1}
                        selectStyle={[
                            styles.inputPicker,
                            {borderColor: this.state.secondColor}
                        ]}
                        initValue="Seleccione la materia"
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        onChange={option => this.onChange1(option)}
                    />
                </View>
                <View
                    style={{
                        width: responsiveWidth(94),
                        height: responsiveHeight(30),
                        marginTop: 35,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <View style={[styles.contAlumnos2, {borderWidth: 0}]}>
                        <ScrollView
                            style={[
                                styles.titulosContainer,
                                {borderColor: this.state.secondColor}
                            ]}
                            horizontal={false}
                            showsVerticalScrollIndicator={false}>
                            {nombres}
                        </ScrollView>
                    </View>
                    <TouchableHighlight
                        onPress={() =>
                            Actions.Messages({
                                data: this.state.userData.maestro
                            })
                        }
                        style={[
                            styles.bigButton,
                            {
                                backgroundColor: this.state.mainColor,
                                marginTop: responsiveHeight(10)
                            }
                        ]}>
                        <Text style={styles.textButton}>Enviar Mensaje</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    isPapa() {
        const nombres = this.renderNames();
        let dex = 0;
        const data = [
            {key: dex++, label: 'Maestro', id: 'Maestro'},
            {key: dex++, label: 'Administrativo', id: 'Admin'}
        ];
        return (
            <View style={styles.container}>
                <Hijos
                    onSelectedChamaco={(index, grado, grupo) =>
                        this.onListItemPressed(index, grado, grupo)
                    }
                />
                <View>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el destinatario
                    </Text>
                    <View style={[styles.asuntos]}>
                        <ModalSelector
                            data={data}
                            selectStyle={[
                                styles.inputPicker,
                                {borderColor: this.state.secondColor}
                            ]}
                            initValue="Lista de usuarios"
                            cancelText="Cancelar"
                            optionTextStyle={{color: this.state.thirdColor}}
                            onChange={option => this.onChange(option)}
                        />
                    </View>
                    <View
                        style={{
                            width: responsiveWidth(94),
                            ...ifIphoneX(
                                {
                                    height: responsiveHeight(44)
                                },
                                {
                                    height: responsiveHeight(38)
                                }
                            ),

                            marginTop: 15,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <View>
                            <ScrollView
                                style={[
                                    styles.titulosContainer,
                                    {borderColor: this.state.secondColor}
                                ]}
                                horizontal={false}
                                showsVerticalScrollIndicator={false}>
                                {nombres}
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <TouchableHighlight
                    onPress={() =>
                        Actions.Messages({
                            data: this.state.userData.maestro
                        })
                    }
                    style={[
                        styles.bigButton,
                        {
                            backgroundColor: this.state.mainColor
                        }
                    ]}>
                    <Text style={styles.textButton}>Enviar Mensaje</Text>
                </TouchableHighlight>
            </View>
        );
    }

    isAdmin() {
        const nombres = this.renderNames();
        let dex = 0;
        const data = [
            {key: dex++, label: 'Maestro', id: 'Maestro'},
            {key: dex++, label: 'Administrativo', id: 'Admin'},
            {key: dex++, label: 'Padre', id: 'Padre'}
        ];
        return (
            <View style={styles.container}>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione el destinatario
                </Text>
                <View style={[styles.asuntos]}>
                    <ModalSelector
                        data={data}
                        selectStyle={[
                            [styles.inputPicker, {borderColor: this.state.secondColor}],
                            {marginTop: 1}
                        ]}
                        initValue="Lista de usuarios"
                        cancelText="Cancelar"
                        optionTextStyle={{color: this.state.thirdColor}}
                        onChange={option => this.onChange(option)}
                    />
                </View>
                {this.state.id === 'Admin' || 'Maestro' ? (
                    <GradoyGrupo
                        onListItemPressedGrado={(index, grado) =>
                            this.onListItemPressedGrado(index, grado)
                        }
                        onListItemPressedGrupos={(index, grupo) =>
                            this.onListItemPressedGrupo(index, grupo)
                        }
                        todos={'0'}
                    />
                ) : null}
                <ScrollView
                    style={[
                        styles.titulosContainer,
                        {borderColor: this.state.secondColor, marginTop: 15}
                    ]}
                    horizontal={false}
                    showsVerticalScrollIndicator={false}>
                    {nombres}
                </ScrollView>
                <TouchableHighlight
                    onPress={() => Actions.Messages({data: this.state.userData})}
                    style={[
                        styles.bigButton,
                        {backgroundColor: this.state.mainColor, marginTop: 50}
                    ]}>
                    <Text style={styles.textButton}>Enviar Mensaje</Text>
                </TouchableHighlight>
            </View>
        );
    }

    render() {
        return (
            <View style={[styles.container, {height: responsiveHeight(100)}]}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {this.state.admin === 'Padre' ? this.isPapa() : null}
                {this.state.admin === 'Admin' ? this.isAdmin() : null}
                {this.state.admin === 'Alumno' ? this.isAlumno() : null}
                {this.state.admin === 'Maestro' ? this.isAdmin() : null}
            </View>
        );
    }
}
