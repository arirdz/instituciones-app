import React from "react";
import PropTypes from "prop-types";
import {AsyncStorage, Text, TouchableHighlight, View} from "react-native";
import styles from "../Tstyles";

export default class Hijos extends React.Component {
    static propTypes = {
        onSelectedChamaco: PropTypes.func.isRequired
    };

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            ninosPicker: [],
            uri: "",
            id: "",
            selectedIndex: 0
        };
        this.renderList = this.renderList.bind(this);
        this.onListItemPressed = this.onListItemPressed.bind(this);
    }

    componentWillMount() {
        this.getURL();
    }

    async getURL() {
        let uri = await AsyncStorage.getItem("uri");
        let maincolor = await AsyncStorage.getItem("mainColor");
        let secondColor = await AsyncStorage.getItem("secondColor");
        let thirdColor = await AsyncStorage.getItem("thirdColor");
        let fourthColor = await AsyncStorage.getItem("fourthColor");
        this.setState({
            uri: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        this.getHijos();
    }

    async getHijos() {
        let token = await AsyncStorage.getItem("token");
        let hijoPicker = await fetch(this.state.uri + "/api/user/hijos", {
            method: "GET",
            headers: {
                Authorization: "Bearer " + token
            }
        });
        let hijosPicker = await hijoPicker.json();
        this.setState({
            ninosPicker: hijosPicker.map(item => item)
        });
        this.setState({
            grado: this.state.ninosPicker[0].grado,
            grupo: this.state.ninosPicker[0].grupo,
            id: this.state.ninosPicker[0].id
        });
        await this.props.onSelectedChamaco(
            0,
            this.state.grado,
            this.state.grupo,
            this.state.id
        );
    }

    async onListItemPressed(index, grado, grupo, id) {
        await this.setState({
            selectedIndex: index,
            selGrad: grado,
            selGrup: grupo,
            id: id
        });
        await this.props.onSelectedChamaco(index, grado, grupo, id);
    }

    renderItem(listItem, index) {
        let bigButtonStyles = [
            styles.listButton,
            styles.listButtonBig,
            {borderColor: this.state.secondColor}
        ];
        let smallButtonStyles = [
            styles.listButton,
            styles.listButtonSmall,
            styles.btn6,
            {borderColor: this.state.secondColor}
        ];
        let texto = [styles.textoN];

        if (this.state.selectedIndex === index) {
            bigButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }

        return (
            <TouchableHighlight
                key={index}
                underlayColor={"transparent"}
                onPress={() =>
                    this.onListItemPressed(
                        index,
                        listItem.grado,
                        listItem.grupo,
                        listItem.id
                    )
                }
            >
                <View style={[styles.listItem]}>
                    <View style={bigButtonStyles}>
                        <Text style={texto}>{listItem.name}</Text>
                    </View>
                    <View style={smallButtonStyles}>
                        <Text style={texto}>{listItem.grado}</Text>
                    </View>
                    <View style={smallButtonStyles}>
                        <Text style={texto}>{listItem.grupo}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }

    renderList() {
        let buttons = [];
        this.state.ninosPicker.forEach((item, index) => {
            buttons.push(this.renderItem(item, index));
        });
        return buttons;
    }

    render() {
        const list = this.renderList();
        return (
            <View style={{}}>
                <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                    Seleccione el hijo correspondiente
                </Text>
                <View style={[styles.botones_N, {marginTop: 1}]}>{list}</View>
            </View>
        );
    }
}
