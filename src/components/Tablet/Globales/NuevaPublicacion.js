import React from 'react';
import {Alert, AsyncStorage, Keyboard, KeyboardAvoidingView, ScrollView, StatusBar, Text, TextInput, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import styles from '../Tstyles';
import GradoyGrupo from './GradoyGrupo';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';
import Entypo from 'react-native-vector-icons/Entypo';
import {Actions} from 'react-native-router-flux';
import Spinner from "react-native-loading-spinner-overlay";

export default class NuevaPublicacion extends React.Component {
    handleContenido = text => {
        this.setState({descripcion: text, resumen: text});
    };
    handleTitulo = text => {
        this.setState({titulo: text});
    };
    _changeWheelState = () => {
        this.setState({
            visible: !this.state.visible
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            offSet: 0,
            isModalSub: [],
            laSubOpcion: [],
            laOpcion: [],
            selectedIndexSub: [],
            elSub: [],
            elIndice: '',
            indice: '',
            btnPrueba: [{tipo: 'Múltiple'}, {tipo: 'Única'}, {tipo: 'Numérica'}, {tipo: 'Texto'}],
            btnSub: [{tipo: 'Múltiple'}, {tipo: 'Única'}, {tipo: 'Numérica'}, {tipo: 'Texto'}],
            elSubText: [],
            selectedIndexPrueba: 0,
            laPrueba: 'Múltiple',
            laSubResp: [],
            laResp: [],
            elText: [],
            losTexts: [0],
            losSubTexts: [0],
            subLel: [],
            titulo: 'Ejemplo',
            definido: true,
            personal: false,
            isModalPrueba: false,
            isModalPerson: false,
            descripcion: 'Se ha publicado un nuevo documento para ver el contenido de click <a href=https://www.google.com">aquí</a> Gracias!',
            resumen: '',
            selectedIndexGrados: -1,
            elGrado: 'Todos',
            elIdTaller: '',
            checkBoxBtns: [],
            checkBoxBtns2: [],
            checkBoxBtnsIndex: [],
            checkBoxBtnsIndex2: [],
            selected: '',
            selectedIndexGrupos: -1,
            elGrupo: 'Todos',
            selectedIndexLista: -1,
            elTaller: '',
            role: [{label: 'Padres', role: 'Padre'}, {label: 'Alumnos', role: 'Alumno'}, {
                label: 'Admón.', role: 'Admin'
            }, {label: 'Maestros', role: 'Maestro'}],
            role2: [],
            selectedIndexRole: -1,
            elRole: '',
            aux: 0,
            losRoles: '',
            losRoles2: '',
            tipoAviso: "1",
            visible:false
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async componentWillMount() {
        await this.getURL();
        await this.pushes();
    }

    async requestMultipart() {
        await this._changeWheelState();
        await this.pruebaRequest();

        let formData = new FormData();
        formData.append('new', JSON.stringify({
            'titulo': this.state.titulo,
            'descripcion': this.state.descripcion,
            'role': this.state.losRoles,
            'contestaran': this.state.losRoles2,
            'tipo_aviso': this.state.tipoAviso,
            'grado': this.state.elGrado,
            'grupo': this.state.elGrupo
        }));
        await fetch(this.state.uri + '/api/feed/create', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + this.state.token
            }, body: formData
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Noticia)', [{
                        text: 'Entendido', onPress: () => [this.setState({visible: false}), Actions.HomePage()]
                    }]);
                } else {
                    this.setState({idNoticia: responseJson.id});
                    Alert.alert('¡Felicidades!', 'Se ha publicado la noticia con éxito', [{
                        text: 'Entendido', onPress: () => [this.setState({visible: false}), Actions.HomePage()]
                    }]);
                }
            });
        if (this.state.idNoticia !== undefined) {
            await this.saveOpcion();
        }
    }

    async onListItemPressedLista(indexLista, taller, id) {
        await this.setState({
            selectedIndexLista: indexLista, elGrupo: taller, elGrado: id, tipoAviso: "2"
        });
    }

    async onListItemPressedGrupos(indexGrupo, grupo) {
        await this.setState({
            selectedIndexGrupos: indexGrupo, elGrupo: grupo, selectedIndexLista: -1, tipoAviso: "1"
        });
    }

    async onListItemPressedGrado(indexGrado, grado) {
        await this.setState({
            selectedIndexGrados: indexGrado, elGrado: grado, selectedIndexLista: -1, tipoAviso: "1"
        });
    }

    //++++++++++++++++++++++++++++++++++++ roles++++++++++++++++++++++++++++
    async onListPressedRole(itemRole, indexRole) {
        await this.setState({
            selectedIndexRole: indexRole, elRole: itemRole
        });
        let i = this.state.checkBoxBtns.indexOf(itemRole);
        if (i > -1) {
            this.state.checkBoxBtns.splice(i, 1);
            this.state.checkBoxBtnsIndex[indexRole] = 0;
        } else {
            this.state.checkBoxBtns.push(itemRole);
            this.state.checkBoxBtnsIndex[indexRole] = 1;
        }
        await this.setState({aux: 0});
    }

    renderRole(itemRole, indexRole) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn_2, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.checkBoxBtnsIndex[indexRole] === 1) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<View>
            <TouchableHighlight
                style={[smallButtonStyles]}
                underlayColor={'transparent'}
                onPress={() => this.onListPressedRole(itemRole, indexRole)}
            >
                <Text style={texto}>{itemRole.label}</Text>
            </TouchableHighlight>
        </View>);
    }

    renderRoles() {
        let btnRole = [];
        this.state.role.forEach((itemRole, indexRole) => {
            btnRole.push(this.renderRole(itemRole, indexRole));
        });
        return btnRole;
    }

    //++++++++++++++++++++++++++++++++++++ roles contesta ++++++++++++++++++++++++++++
    async onListPressedRole2(itemRole, indexRole) {
        await this.setState({
            selectedIndexRole2: indexRole, elRole2: itemRole
        });
        let i = this.state.checkBoxBtns2.indexOf(itemRole);
        if (i > -1) {
            this.state.checkBoxBtns2.splice(i, 1);
            this.state.checkBoxBtnsIndex2[indexRole] = 0;
        } else {
            this.state.checkBoxBtns2.push(itemRole);
            this.state.checkBoxBtnsIndex2[indexRole] = 1;
        }
        await this.setState({aux: 0});
    }

    renderRole2(itemRole, indexRole) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn_2, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.checkBoxBtnsIndex2[indexRole] === 1) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<View>
            <TouchableHighlight
                style={[smallButtonStyles]}
                underlayColor={'transparent'}
                onPress={() => this.onListPressedRole2(itemRole, indexRole)}>
                <Text style={texto}>{itemRole.label}</Text>
            </TouchableHighlight>
        </View>);
    }

    renderRoles2() {
        let btnRole = [];
        this.state.checkBoxBtns.forEach((itemRole, indexRole) => {
            btnRole.push(this.renderRole2(itemRole, indexRole));
        });
        return btnRole;
    }

    //+++++++++++++++++++++++++++++++++++++ render
    async predefinidos() {
        if (this.state.definido === false) {
            this.setState({definido: true, personal: false});
        } else {
            this.setState({definido: true, personal: false});
        }
    }

    async personalizados() {
        if (this.state.personal === false) {
            this.setState({
                personal: true, definido: false
            });
        } else {
            this.setState({
                personal: true, definido: false
            });
        }
    }

    //++++++++++++++++++++++++++++++++++++ Asd asd asd asd asd

    async saveOpcion() {
        let elObjeto = [];
        this.state.laResp.forEach((it, ix) => {
            if (((this.state.laPrueba === 'Múltiple') || (this.state.laPrueba === 'Única')) && ((this.state.elSub[ix] === 'Múltiple') || (this.state.elSub[ix] === 'Única'))) {
                let suvRespuestas = [];
                for (let i = 1; i <= this.state.losTexts.length; i++) {
                    let extra = [];
                    for (let j = 0; j < this.state.losSubTexts.length; j++) {
                        if (this.state.laSubResp[i + '' + j] !== '' && this.state.laSubResp[i + '' + j] !== undefined) {
                            extra.push(this.state.laSubResp[i + '' + j]);
                        }
                    }
                    suvRespuestas.push(extra);
                }
                elObjeto.push({
                    'tipo': this.state.laPrueba,
                    'respuesta': this.state.laResp[ix],
                    'subTipo': this.state.elSub[ix],
                    'subRespuestas': suvRespuestas[ix]
                });
            } else if (((this.state.laPrueba === 'Múltiple') || (this.state.laPrueba === 'Única')) && (this.state.elSub[ix] === 'Numérica')) {
                elObjeto.push({
                    'tipo': this.state.laPrueba, 'respuesta': this.state.laResp[ix], 'subTipo': this.state.elSub[ix]
                });
            } else if (((this.state.laPrueba === 'Múltiple') || (this.state.laPrueba === 'Única')) && (this.state.elSub[ix] === 'Texto')) {
                elObjeto.push({
                    'tipo': this.state.laPrueba, 'respuesta': this.state.laResp[ix], 'subTipo': this.state.elSub[ix]
                });
            } else {
                elObjeto.push({
                    'tipo': this.state.laPrueba, 'respuesta': this.state.laResp[ix], 'subTipo': 'ninguno'
                });
            }
        });
        if (this.state.laPrueba === 'Numérica') {
            elObjeto.push({
                'tipo': this.state.laPrueba
            });
        } else if (this.state.laPrueba === 'Texto') {
            elObjeto.push({
                'tipo': this.state.laPrueba
            });
        }

        elObjeto.forEach((item) => {
            let formData = new FormData();
            formData.append('new', JSON.stringify(item));
            fetch(this.state.uri + '/api/feed/create/respuestas/subrespuestas/' + this.state.idNoticia, {
                method: 'POST', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    Authorization: 'Bearer ' + this.state.token
                }, body: formData
            }).then(res => res.json())
                .then(responseJson => {

                });
        });
    }

    async pushes() {
        this.state.losTexts.forEach(() => {
            this.state.isModalSub.push(false);
        });
    }

    async isSubCarac(text, i) {
        this.state.elSubText[i] = text;
        this.setState({aux: 0});
    }

    async isCaracteres(text) {
        this.setState({elText: text});
    }

    //++++++++++++++++++++++++++++++++++++++++++++Respuesta
    async agregarOtro() {
        this.state.losTexts.length !== 4 ? this.state.losTexts.push(0) : null;
        await this.setState({aux: 0});
    }

    borrar(index) {
        this.state.losTexts.splice(index, 1);
        this.setState({aux: 0});
    }

    async openModal(i) {
        this.state.isModalSub[i] = true;
        await this.setState({aux: 0});
    }

    async closeModal(i) {
        this.state.isModalSub[i] = false;
        await this.setState({aux: 0});
    }

    isModal(i) {
        const subElec = this.renderSubs(i);
        const subTextInpt = this.subRespuestas(i);
        return (<View>
            <Modal
                isVisible={this.state.isModalSub[i]}
                backdropOpacity={0.8}
                animationIn={'bounceIn'}
                animationOut={'bounceOut'}
                animationInTiming={1000}
                animationOutTiming={1000}>
                <KeyboardAvoidingView
                    behavior={'padding'}
                    keyboardVerticalOffset={15}
                    style={[styles.container, {borderRadius: 6, flex: 0}]}
                >
                    <Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                        Seleccione el tipo de sub-respuesta
                    </Text>
                    <View
                        style={[styles.row, {height: responsiveHeight(7)}]}>
                        {subElec}
                    </View>
                    {this.state.selectedIndexSub[i] === 0 ? (<View
                        style={[styles.modalWidth, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                            }}>
                            Con esta selección el destinatario podrá varias de las opciones de respuesta
                        </Text>
                    </View>) : this.state.selectedIndexSub[i] === 1 ? (<View style={[styles.modalWidth, {
                        marginTop: 5, alignItems: 'center', borderBottomWidth: 1
                    }]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                            }}>
                            Con esta selección el destinatario podrá elegir solo una de las opciones de
                            respuesta
                        </Text>
                    </View>) : this.state.selectedIndexSub === 2 ? (<View style={[styles.modalWidth, {
                        marginTop: 5, alignItems: 'center', borderBottomWidth: 1
                    }]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                            }}>
                            Con esta selección el destinatario podrá capturar un número como respuesta
                        </Text>
                    </View>) : this.state.selectedIndexSub[i] === 3 ? (<View
                        style={[styles.modalWidth, {
                            marginTop: 5, alignItems: 'center', borderBottomWidth: 1
                        }]}>
                        <Text
                            style={{
                                fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                            }}>
                            Con esta selección el destinatario podrá capturar un texto como
                            respuesta
                        </Text>
                    </View>) : null}
                    {this.state.selectedIndexSub[i] === 0 || this.state.selectedIndexSub[i] === 1 ? (<ScrollView style={{marginTop: 15}}>
                        <View style={[styles.modalWidth, {alignItems: 'center'}]}>
                            <Text style={{marginBottom: 5}}>
                                Defina la sub-respuesta que habrá en la noticia
                            </Text>
                            {subTextInpt}
                            {this.state.losSubTexts.length !== 2 ? (<View style={[styles.modalWidth, {alignItems: 'center'}]}><TouchableOpacity
                                onPress={() => this.agregarOtroSub()}
                            >
                                <Entypo name='plus' size={35} color='#2b2b2b'/>
                            </TouchableOpacity>
                                <Text>Agregue otra sub-respuesta</Text></View>) : null}
                        </View>
                    </ScrollView>) : this.state.selectedIndexSub[i] === 2 ? (<ScrollView style={{marginTop: 15}}>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{marginTop: 4}}>
                                Máximo de 2 caracteres numéricos, ejemplo:
                            </Text>
                            <TextInput
                                keyboardType='numeric'
                                maxLength={2}
                                onChangeText={text => this.onChangeNum(text, i)}
                                placeholder={'ej.: del 1 al 99'}
                                returnKeyType='next'
                                underlineColorAndroid='transparent'
                                value={this.state.subLel[i]}
                                style={[styles.inputPicker, styles.btn11, {
                                    borderColor: this.state.secondColor, height: responsiveHeight(5), padding: 5,
                                    marginTop: 3
                                }]}
                            />
                        </View>
                    </ScrollView>) : this.state.selectedIndexSub[i] === 3 ? (<ScrollView>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{marginTop: 10}}>
                                Con esta selección el destinatario podrá capturar un texto como respuesta
                            </Text>
                            <Text
                                style={{
                                    fontStyle: 'italic', textAlign: 'center', marginTop: 10
                                }}>
                                ejemplo:
                            </Text>
                            <TextInput
                                keyboardType='default'
                                maxLength={120}
                                multiline={true}
                                onChangeText={(text) => this.isSubCarac(text, i)}
                                placeholder={'Responda de manera breve'}
                                returnKeyType='next'
                                value={this.state.elSubText[i]}
                                underlineColorAndroid='transparent'
                                style={[styles.inputPicker, styles.btn11, {
                                    borderColor: this.state.secondColor, height: responsiveHeight(9), padding: 5
                                }]}
                            />
                            <Text>{this.state.elSubText[i] === undefined ? '0' : this.state.elSubText[i].length}/120</Text>
                        </View>
                    </ScrollView>) : (<ScrollView style={{marginTop: 50}}>
                        <Text> "No hay ningún tipo de sub opción seleccionado"</Text>
                    </ScrollView>)}
                    <View style={[styles.modalWidth, styles.row]}>
                        <TouchableOpacity
                            style={[
                                styles.modalBigBtn,
                                {backgroundColor: '#fff', borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.closeModal(i)}
                        >
                            <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[
                                styles.modalBigBtn,
                                {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                            ]}
                            onPress={() => this.closeModal(i)}
                        >
                            <Text style={[styles.textW, {color: '#fff'}]}>Continuar</Text>
                        </TouchableOpacity>
                    </View>

                </KeyboardAvoidingView>
            </Modal>
        </View>);
    }

    respuesta(i) {
        const modal = this.isModal(i);
        return (<View style={[styles.row, {marginVertical: 5}]}>
            {modal}
            <Text style={[styles.btn5_l, {fontSize: responsiveFontSize(3), paddingBottom: 20}]}>{i + 1}</Text>
            <View>
                <TextInput
                    keyboardType='default'
                    maxLength={254}
                    multiline={true}
                    onChangeText={text => this.onChangeRes(text, i)}
                    placeholder={'ej.: , etc...'}
                    returnKeyType='next'
                    underlineColorAndroid='transparent'
                    value={this.state.laResp[i]}
                    style={[styles.inputPicker, styles.btn12, {
                        borderColor: this.state.secondColor, height: responsiveHeight(10), padding: 5
                    }]}
                />
                <View style={[styles.btn12, styles.row, {alignItems: 'flex-end'}]}>
                    <TouchableOpacity onPress={() => this.borrar(i)} style={{marginBottom: 7}}>
                        <Text style={[styles.textW, {color: 'red'}]}>Borrar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.inputPicker, styles.btn3, {
                            height: responsiveHeight(3.5), backgroundColor: this.state.secondColor, padding: 0
                        }]}
                        onPress={() => this.openModal(i)}
                    >
                        <Text style={styles.textoB}>Sub opción</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>);
    }

    respuestas() {
        let btnResp = [];
        for (let i = 0; i < this.state.losTexts.length; i++) {
            btnResp.push(this.respuesta(i));
        }
        return btnResp;
    }

    async onChangeRes(text, i) {
        this.state.laResp[i] = await text;
        await this.setState({aux: 0});
    }

    async onChangeNum(text, i) {
        this.state.subLel[i] = await text;
        await this.setState({aux: 0});
    }

    //++++++++++++++++++++++++++++++++++++++++++++SUB-RESPUESTA
    async agregarOtroSub() {
        this.state.losSubTexts.length !== 2 ? this.state.losSubTexts.push(0) : null;
        await this.setState({aux: 0});
    }

    async onChangeRes1(text, i, j) {
        this.state.laSubResp[i + '' + j] = await text;
        await this.setState({aux: 0});
    }

    borrarSub(index) {
        this.state.losSubTexts.splice(index, 1);
        this.setState({aux: 0});
    }

    subResp(i, j) {
        return (<View style={[styles.row, styles.btn10]}>
            <Text style={[styles.btn6_l, {fontSize: responsiveFontSize(2.5), marginBottom: 20}]}>
                {j + 1}
            </Text>
            <View style={{alignItems: 'center'}}>
                <TextInput
                    keyboardType='default'
                    maxLength={254}
                    multiline={true}
                    onChangeText={text => this.onChangeRes1(text, i, j)}
                    placeholder={'ej.:sub , etc...'}
                    underlineColorAndroid='transparent'
                    value={this.state.laSubResp[i + '' + j]}
                    style={[styles.inputPicker, styles.btn11, {
                        borderColor: this.state.secondColor, height: responsiveHeight(10), padding: 5
                    }]}/>
                <View style={[styles.btn11, styles.row, {alignItems: 'flex-end'}]}>
                    <TouchableOpacity onPress={() => this.borrarSub(j)} style={{marginBottom: 7}}>
                        <Text style={[styles.textW, {color: 'red'}]}>Borrar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>);
    }

    subRespuestas(i) {
        let iv = i + 1;
        let btnResp = [];
        for (let j = 0; j < this.state.losSubTexts.length; j++) {
            btnResp.push(this.subResp(iv, j));
        }
        return btnResp;
    }

    //++++++++++++++++++++++++++++++++++++++++++++checksBtns
    async onListPressedPrueba(itemPrueba, indexPrueba) {
        await this.setState({
            selectedIndexPrueba: indexPrueba, laPrueba: itemPrueba.tipo
        });
    }

    renderBtn(itemPrueba, indexPrueba) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn_2, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];
        if (this.state.selectedIndexPrueba === indexPrueba) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<View style={{alignItems: 'center'}}>
            <TouchableHighlight
                style={smallButtonStyles}
                underlayColor={'transparent'}
                onPress={() => this.onListPressedPrueba(itemPrueba, indexPrueba)}
            >
                <Text style={texto}>{itemPrueba.tipo}</Text>
            </TouchableHighlight>
        </View>);
    }

    renderBtns() {
        let elBtn = [];
        this.state.btnPrueba.forEach((itemPrueba, indexPrueba) => {
            elBtn.push(this.renderBtn(itemPrueba, indexPrueba));
        });
        return elBtn;
    }

    //++++++++++ subSeleccion ++++++++
    async onListPressedSub(itemSub, indexSub, i) {
        this.state.elSub[i] = itemSub.tipo;
        this.state.selectedIndexSub[i] = indexSub;
        await this.setState({indice: i});
    }

    renderSub(itemSub, indexSub, i) {
        let smallButtonStyles = [styles.listButton, styles.listButtonSmall, styles.btn5, {borderColor: this.state.secondColor}];
        let texto = [styles.textoN];

        if (this.state.selectedIndexSub[i] === indexSub) {
            smallButtonStyles.push(styles.listButtonSelected, {
                backgroundColor: this.state.secondColor
            });
            texto.push(styles.textoB);
        }
        return (<View style={{alignItems: 'center'}}>
            <TouchableHighlight
                style={smallButtonStyles}
                underlayColor={'transparent'}
                onPress={() => this.onListPressedSub(itemSub, indexSub, i)}>
                <Text style={texto}>{itemSub.tipo}</Text>
            </TouchableHighlight>
        </View>);
    }

    renderSubs(i) {
        let elBtnSub = [];
        this.state.btnSub.forEach((itemSub, indexSub) => {
            elBtnSub.push(this.renderSub(itemSub, indexSub, i));
        });
        return elBtnSub;
    }

    renderPersss() {
        const pruebaCheck = this.renderBtns();
        const textIput = this.respuestas();
        return (<View style={styles.container}>
            <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                Seleccione el tipo de respuesta
            </Text>
            <View style={[styles.row, {height: responsiveHeight(7)}]}>{pruebaCheck}</View>
            {this.state.selectedIndexPrueba === 0 ? (
                <View style={[styles.widthall, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                        }}>
                        Con esta selección el destinatario podrá elegir cualquiera de las opciones de
                        respuesta
                    </Text>
                </View>) : this.state.selectedIndexPrueba === 1 ? (
                <View style={[styles.widthall, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                        }}>
                        Con esta selección el destinatario podrá elegir solo una de las opciones de
                        respuesta
                    </Text>
                </View>) : this.state.selectedIndexPrueba === 2 ? (
                <View style={[styles.widthall, {marginTop: 5, alignItems: 'center', borderBottomWidth: 1}]}>
                    <Text
                        style={{
                            fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                        }}>
                        Con esta selección el destinatario podrá capturar un número como respuesta
                    </Text>
                </View>) : this.state.selectedIndexPrueba === 3 ? (<View
                style={[styles.widthall, {
                    marginTop: 5, alignItems: 'center', borderBottomWidth: 1
                }]}>
                <Text
                    style={{
                        fontSize: responsiveFontSize(1.5), fontStyle: 'italic', textAlign: 'center', marginBottom: 5
                    }}>
                    Con esta selección el destinatario podrá capturar un texto como respuesta
                </Text>
            </View>) : null}
            {this.state.selectedIndexPrueba === 0 || this.state.selectedIndexPrueba === 1 ? (<View style={{alignItems: 'center', width: responsiveWidth(94)}}>
                <Text style={{marginTop: 6, marginBottom: 10}}>
                    Defina las respuestas que habrá en la noticia
                </Text>
                {textIput}
                {this.state.losTexts.length !== 4 ? (<View style={[styles.widthall, {alignItems: 'center', marginTop: 5}]}>
                    <TouchableOpacity
                        onPress={() => this.agregarOtro()}>
                        <Entypo name='plus' size={40} color='#2b2b2b'/>
                    </TouchableOpacity>
                    <Text>Añadir otra opción de respuesta</Text>
                </View>) : null}

            </View>) : this.state.selectedIndexPrueba === 2 ? (<View style={{alignItems: 'center', width: responsiveWidth(94), height: responsiveHeight(10.5)}}>
                <Text style={{marginTop: 15}}>
                     Máximo de 2 caracteres numéricos, ejemplo:
                </Text>
                <TextInput
                    keyboardType='numeric'
                    maxLength={2}
                    onChangeText={text => (this.state.lel = text)}
                    placeholder={'ej.: del 1 al 99'}
                    returnKeyType='next'
                    underlineColorAndroid='transparent'
                    style={[styles.inputPicker, styles.btn11, {
                        borderColor: this.state.secondColor, height: responsiveHeight(5), padding: 5,
                        marginTop: 5
                    }]}
                />
            </View>) : this.state.selectedIndexPrueba === 3 ? (<View style={{alignItems: 'center', width: responsiveWidth(94), height: responsiveHeight(21)}}>
                <Text style={{marginTop: 10}}>
                    Este estilo de respuesta les llegará a los destinatarios y será respondido con un máximo
                    de 120 caracteres
                </Text>
                <Text
                    style={{
                        fontStyle: 'italic', textAlign: 'center', marginTop: 10
                    }}>
                    ejemplo:
                </Text>
                <TextInput
                    keyboardType='default'
                    maxLength={120}
                    multiline={true}
                    onChangeText={(text) => this.isCaracteres(text)}
                    placeholder={'Responda de manera breve'}
                    returnKeyType='next'
                    underlineColorAndroid='transparent'
                    style={[styles.inputPicker, styles.btn11, {
                        borderColor: this.state.secondColor, height: responsiveHeight(9.5), padding: 5
                    }]}
                />
                <Text>{this.state.elText.length}/120</Text>
            </View>) : null}
        </View>);
    }

    async pruebaRequest() {
        await this.setState({losRoles: ''});
        await this.state.checkBoxBtns.forEach((item) => {
            if (this.state.losRoles === '') {
                this.setState({losRoles: this.state.losRoles + item.role});
            } else {
                this.setState({losRoles: this.state.losRoles + ',' + item.role});
            }
        });

        await this.setState({losRoles2: ''});
        await this.state.checkBoxBtns2.forEach((item) => {
            if (this.state.losRoles2 === '') {
                this.setState({losRoles2: this.state.losRoles2 + item.role});
            } else {
                this.setState({losRoles2: this.state.losRoles2 + ',' + item.role});
            }
        });
    }

    //++++++++++++++++++++++++++++++++++++ Asd asd asd asd asd
    render() {
        const respPers = this.renderPersss();
        const losRoles = this.renderRoles();
        const losRoles2 = this.renderRoles2();
        let texto = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnCal = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.personal === true) {
            btnCal.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7), fontWeight: '700'
            });
        }
        let texto1 = [styles.textoN, {fontSize: responsiveFontSize(1.7), fontWeight: '700'}];
        let btnTem = [styles.exaCalif, {backgroundColor: this.state.fourthColor}];
        if (this.state.definido === true) {
            btnTem.push(styles.exaCalif, {backgroundColor: this.state.thirdColor});
            texto1.push(styles.textoB, {
                fontSize: responsiveFontSize(1.7), fontWeight: '700'
            });
        }
        return (<KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={40} style={[styles.container]}>
            <StatusBar
                backgroundColor={this.state.mainColor}
                barStyle='light-content'
            />
            <View style={styles.container}>
                <Spinner visible={this.state.visible} textContent='Procesando...'/>
                <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 50}}>
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Elija los Destinatarios
                    </Text>
                    <View style={[styles.widthall, {alignItems: 'center'}]}>
                        <View style={styles.buttonsRow}>{losRoles}</View>
                    </View>
                    {this.state.checkBoxBtnsIndex[0] === 1 || this.state.checkBoxBtnsIndex[1] === 1 ? (
                        <GradoyGrupo
                            onListItemPressedGrupos={(indexGrupo, grupo) => this.onListItemPressedGrupos(indexGrupo, grupo)}
                            onListItemPressedGrado={(indexGrado, grado) => this.onListItemPressedGrado(indexGrado, grado)}
                            onListItemPressedLista={(indexLista, taller, id) => this.onListItemPressedLista(indexLista, taller, id)}
                            listaVar={true}
                            todos={'1'}
                        />) : null}

                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Título
                    </Text>
                    <TextInput
                        keyboardType='default'
                        returnKeyType='next'
                        underlineColorAndroid='transparent'
                        style={[styles.inputTop, {borderColor: this.state.secondColor, marginTop: 3}]}
                        onChangeText={this.handleTitulo}
                        autoCapitalize='none'
                        autoCorrect={true}
                        defaultValue={this.state.titulo}
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Contenido
                    </Text>
                    <TextInput
                        keyboardType='default'
                        returnKeyType='next'
                        multiline={true}
                        underlineColorAndroid='transparent'
                        onChangeText={this.handleContenido}
                        defaultValue={this.state.descripcion}
                        onEndEditing={() => Keyboard.dismiss()}
                        style={[styles.inputContenido, {borderColor: this.state.secondColor, padding: 10, marginTop: 3}]}
                        autoCorrect={true}
                    />
                    <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                        Seleccione el estilo de respuesta
                    </Text>
                    <View style={[styles.widthall, {alignItems: 'center'}]}>
                        <View
                            style={[styles.rowsCalif, {
                                width: responsiveWidth(62.5), marginTop: 5, alignItems: 'center'
                            }]}>
                            <TouchableHighlight
                                underlayColor={'transparent'}
                                style={btnTem}
                                onPress={() => this.predefinidos()}>
                                <Text style={texto1}>Predefinida</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={'transparent'}
                                style={btnCal}
                                onPress={() => this.personalizados()}>
                                <Text style={texto}>Personalizada</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                    {this.state.personal === true ? respPers : null}
                    {this.state.checkBoxBtns.length > 0 ?
                        <View style={{height: 80}}><Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            Elija quien responderá la noticia:
                        </Text>
                            <View style={styles.buttonsRow}>{losRoles2}</View></View> : null}
                    <TouchableOpacity
                        style={[styles.bigButton, {backgroundColor: this.state.secondColor}]}
                        onPress={() => this.requestMultipart()}>
                        <Text style={styles.textButton}>Publicar</Text>
                    </TouchableOpacity>
                </ScrollView>

            </View>
        </KeyboardAvoidingView>);
    }
}
