import React from 'react';
import {Image, Platform, Text, TouchableOpacity, View} from 'react-native';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {MediaQuery} from 'react-native-responsive';
import myStyles from './../myStyles';

export default class Card extends React.Component {
    render() {
        const imagen = this.props.imagen;
        let height1 = null;
        let height2 = null;
        let height3 = null;
        if (this.props.cantidad === 3) {
            height1 = responsiveHeight(24.7);
            height2 = responsiveHeight(25.7);
            height3 = responsiveHeight(20.7);
        } else if (this.props.cantidad === 2) {
            height1 = responsiveHeight(37.6);
            height2 = responsiveHeight(39.3);
            height3 = responsiveHeight(36.7);
        } else if (this.props.cantidad === 7) {
            height1 = responsiveHeight(9.7);
            height2 = responsiveHeight(10.2);
            height3 = responsiveHeight(5.7);
        }
        let mainStyle = [];
        let mainStyle1 = [];
        if (Platform.OS !== 'ios') {
            mainStyle = {
                position: 'relative',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'transparent',
                borderRadius: 15,
                borderColor: '#22305d',
                borderWidth: this.props.bordeCard
            };
        }


        return (<View style={[{marginTop: 11}]}>
            <MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
                <View>
                    <TouchableOpacity onPress={Actions.cursos} style={[mainStyle, {
                        position: 'relative',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'transparent',
                        shadowColor: 'lightgrey',
                        shadowOpacity: 1,
                        shadowOffset: {width: 5, height: 5}
                    }]}>
                        <View
                            style={{
                                position: 'absolute',
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: this.props.color,
                                borderRadius: 15,
                                borderColor: '#22305d',
                                borderWidth: Platform.OS === 'ios' ? this.props.bordeCard : 0
                            }}>
                            <Image
                                style={{
                                    flex: 1, width: responsiveWidth(94), borderRadius: 15,

                                    ...ifIphoneX({
                                        height: Platform.OS === 'ios' ? height3 : height2
                                    }, {
                                        height: Platform.OS === 'ios' ? height3 : height2
                                    }), resizeMode: 'cover'
                                }}
                                source={imagen}
                            />
                        </View>
                        <View
                            style={{
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                                paddingBottom: 15,
                                width: Platform.OS === 'ios' ? responsiveWidth(100) : responsiveWidth(94),
                                borderRadius: 15, ...ifIphoneX({
                                    height: height1
                                }, {
                                    height: Platform.OS === 'ios' ? height3 : height2
                                })
                            }}>
                            <Text
                                style={[myStyles.cardTitle, {
                                    fontSize: 30,
                                    color: this.props.colorTXT, backgroundColor: this.props.fondoTXT, marginBottom: 0
                                }]}>
                                {this.props.titulo}
                            </Text>

                            {this.props.subtitulo === '' ? null : (
                                <Text
                                    style={{
                                        backgroundColor: this.props.fondoSubTXT,
                                        textAlign: 'center',
                                        fontSize: 10,
                                        color: 'white',
                                        fontWeight: '600',
                                        width: responsiveWidth(94)

                                    }}>
                                    {this.props.subtitulo}
                                </Text>)}

                        </View>
                    </TouchableOpacity>
                </View>
            </MediaQuery>
            <MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
                <View>
                    <TouchableOpacity onPress={Actions.cursos} style={[mainStyle, {
                        position: 'relative',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'transparent',
                        shadowColor: 'lightgrey',
                        shadowOpacity: 1,
                        shadowOffset: {width: 5, height: 5}
                    }]}>
                        <View
                            style={{
                                position: 'absolute',
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: this.props.color,
                                borderRadius: 15,
                                borderColor: '#22305d',
                                borderWidth: Platform.OS === 'ios' ? this.props.bordeCard : 0
                            }}>
                            <Image
                                style={{
                                    flex: 1, width: responsiveWidth(94), borderRadius: 15,

                                    ...ifIphoneX({
                                        height: Platform.OS === 'ios' ? height1 : height2
                                    }, {
                                        height: height2
                                    }), resizeMode: 'cover'
                                }}
                                source={imagen}
                            />
                        </View>
                        <View
                            style={{
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                                paddingBottom: 15,
                                width: Platform.OS === 'ios' ? responsiveWidth(100) : responsiveWidth(94),
                                borderRadius: 15, ...ifIphoneX({
                                    height: height1
                                }, {
                                    height: height2
                                })
                            }}>
                            <Text
                                style={[myStyles.cardTitle, {
                                    color: this.props.colorTXT, backgroundColor: this.props.fondoTXT, marginBottom: 0
                                }]}>
                                {this.props.titulo}
                            </Text>

                            {this.props.subtitulo === '' ? null : (
                                <Text
                                    style={{
                                        backgroundColor: this.props.fondoSubTXT,
                                        textAlign: 'center',
                                        fontSize: 14,
                                        color: 'white',
                                        fontWeight: '600',
                                        width: responsiveWidth(94)

                                    }}>
                                    {this.props.subtitulo}
                                </Text>)}

                        </View>
                    </TouchableOpacity>
                </View>
            </MediaQuery>
        </View>);
    }
}
