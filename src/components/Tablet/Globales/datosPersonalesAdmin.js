import React from 'react';
import {Alert, AsyncStorage, Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {responsiveWidth} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from '../Tstyles';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Fumi} from 'react-native-textinput-effects';
import MultiBotonRow from './MultiBotonRow';
import Modal from 'react-native-modal';

export default class DatosPersonalesAdmin extends React.Component {
   _showModal = () => this.setState({isModalVisible: true});
   _hideModal = () => this.setState({isModalVisible: false});
   constructor(props) {
	  super(props);
	  this.getURL();
	  this.state = {
		 data: [],
		 cososPicker: [],
		 datos: [],
		 datosV2: [],
		 isModalVisible: false,
		 botonSelected: 'Generales',
		 maincolor: '#fff',
		 secondColor: '#fff',
		 thirdColor: '#fff',
		 fourthColor: '#fff'
	  };
   }

   async getURL() {
	  let uri = await AsyncStorage.getItem('uri');
	  let token = await AsyncStorage.getItem('token');
	  let maincolor = await AsyncStorage.getItem('mainColor');
	  let secondColor = await AsyncStorage.getItem('secondColor');
	  let thirdColor = await AsyncStorage.getItem('thirdColor');
	  let fourthColor = await AsyncStorage.getItem('fourthColor');
	  this.setState({
		 uri: uri,
		 token: token,
		 mainColor: maincolor,
		 secondColor: secondColor,
		 thirdColor: thirdColor,
		 fourthColor: fourthColor
	  });
	  this.getRoles();
	  this.getUserdata();
	  this.getUserdataV2();
   }

   async botonSelected(indexSelected, itemSelected) {
	  await this.setState({
		 botonSelected: itemSelected, indexSelected: indexSelected
	  });
   }

   //+++++++++++++++++++++++++++++user data++++++++++++++++++++++++++++++++++++++
   async getUserdata() {
	  let uri = await AsyncStorage.getItem('uri');
	  let token = await AsyncStorage.getItem('token');
	  let request = await fetch(uri + '/api/user/data', {
		 method: 'GET', headers: {
			Authorization: 'Bearer ' + token
		 }
	  });
	  let dato = await request.json();
	  this.setState({datos: dato});
   }

   async getUserdataV2() {
	  let uri = await AsyncStorage.getItem('uri');
	  let token = await AsyncStorage.getItem('token');
	  let request2 = await fetch(uri + '/api/user/data/v2', {
		 method: 'GET', headers: {
			Authorization: 'Bearer ' + token
		 }
	  });
	  let dato2 = await request2.json();
	  this.setState({datosV2: dato2});
   }

	async requestMultipart() {
		let uri = await AsyncStorage.getItem('uri');
		let token = await AsyncStorage.getItem('token');
		let formData = new FormData();
		formData.append(
			'update',
			JSON.stringify({
				user_id: this.state.datosV2.id,
				nombre: this.state.datos.nombre,
				apellido_paterno: this.state.datos.apellido_paterno,
				apellido_materno: this.state.datos.apellido_materno,
				parentezco: this.state.datos.parentezco,
				calle: this.state.datos.calle,
				numero: this.state.datos.numero,
				colonia: this.state.datos.colonia,
				ciudad: this.state.datos.ciudad,
				estado: this.state.datos.estado,
				cp: this.state.datos.cp,
				nombre_alternativo1: this.state.datos.nombre_alternativo1,
				telefono_casa_alternativo1: this.state.datos
					.telefono_casa_alternativo1,
				telefono_celular_alternativo1: this.state.datos
					.telefono_celular_alternativo1,
				telefono_oficina_alternativo1: this.state.datos
					.telefono_oficina_alternativo1,
				email_alternativo1: this.state.datos.email_alternativo1,
				permiso_contacto_alternativo1: this.state.datos
					.permiso_contacto_alternativo1,
				parentesco_alternativo1: this.state.datos.parentesco_alternativo1,
				nombre_alternativo2: this.state.datos.nombre_alternativo2,
				telefono_casa_alternativo2: this.state.datos
					.telefono_casa_alternativo2,
				telefono_celular_alternativo2: this.state.datos
					.telefono_celular_alternativo2,
				telefono_oficina_alternativo2: this.state.datos
					.telefono_oficina_alternativo2,
				email_alternativo2: this.state.datos.email_alternativo2,
				permiso_contacto_alternativo2: this.state.datos
					.permiso_contacto_alternativo2,
				parentesco_alternativo2: this.state.datos.parentesco_alternativo2,
				telefono_casa_tutor: this.state.datos.telefono_casa_tutor,
				telefono_celular_tutor: this.state.datos.telefono_celular_tutor,
				telefono_oficina_tutor: this.state.datos.telefono_oficina_tutor,
				email_tutor: this.state.datos.email_tutor,
				permiso_contacto_tutor: this.state.datos.permiso_contacto_tutor,
				numero_emergencia: this.state.datos.numero_emergencia,
				estudios: this.state.datos.estudios,
				rfc: this.state.datos.rfc,
				curp: this.state.datos.curp
			})
		);
		await fetch(uri + '/api/user/update', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + token
			},
			body: formData
		}).then(res => res.json())
			.then(responseJson => {
				this.setState({elRequest: responseJson});
			});
		if (this.state.elRequest.error !== undefined) {
			Alert.alert('¡Ups!',
				'Ha ocurrido un error '
				+
				this.state.elRequest.error.status_code
				+
				' si el error continua pónganse en contacto con soporte'
				+
				'(Datos personales)',
				[{
					text: 'Entendido', onPress: () => Actions.drawer()
				}]);
		} else {
			Alert.alert('¡Felicidades!',
				'Se ha modificado tus datos personales',
				[{
					text: 'Entendido',
					onPress: () => Actions.drawer()
				}]);
		}
	}

   async getRoles() {
	  let uri = await AsyncStorage.getItem('uri');
	  let dataPicker = await fetch(uri + '/api/get/parentezcos');
	  datosPicker = await dataPicker.json();
	  this.setState({cososPicker: datosPicker});
   }

   async getRole() {
	  let role = await AsyncStorage.getItem('role');
	  this.setState({admin: role});
   }

   datosPersonales() {
	  return (<View style={styles.datosBasicos}>
		 <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
			Datos personales del maestro
		 </Text>
		 <Fumi
		   label={'Nombre(s)'}
		   iconClass={Ionicons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'md-person'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.nombre = text)}
		   value={this.state.datos.nombre}
		   keyboardType='default'
		   returnKeyType='next'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Apellido paterno'}
		   iconClass={Ionicons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'md-person'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.apellido_paterno = text)}
		   keyboardType='default'
		   value={this.state.datos.apellido_paterno}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Apellido materno'}
		   iconClass={Ionicons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'md-person'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.apellido_materno = text)}
		   keyboardType='default'
		   value={this.state.datos.apellido_materno}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
	  </View>);
   }

   domicilio() {
	  return (<View style={styles.datosBasicos}>
		 <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
			Domicilio del maestro
		 </Text>
		 <Fumi
		   label={'Calle/Avenida'}
		   iconClass={Entypo}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'address'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.calle = text)}
		   keyboardType='default'
		   value={this.state.datos.calle}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Número'}
		   iconClass={FontAwesome}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'hashtag'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.numero = text)}
		   keyboardType='default'
		   value={this.state.datos.numero}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Colonia'}
		   iconClass={MaterialIcons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'my-location'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.colonia = text)}
		   keyboardType='default'
		   value={this.state.datos.colonia}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Ciudad'}
		   iconClass={MaterialIcons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'location-city'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.ciudad = text)}
		   keyboardType='default'
		   value={this.state.datos.ciudad}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Estado'}
		   iconClass={Entypo}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'location'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.estado = text)}
		   keyboardType='default'
		   value={this.state.datos.estado}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Código postal'}
		   iconClass={FontAwesome}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'location-arrow'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.cp = text)}
		   keyboardType='default'
		   value={this.state.datos.cp}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
	  </View>);
   }

   contacto() {
	  return (<View style={styles.datosBasicos}>
		 <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
			Información de contacto del maestro
		 </Text>
		 <Text style={[styles.opcionsitas, {marginBottom: 1}]}>
			Teléfono de Emergencia:
		 </Text>
		 <View style={styles.datosBasicos}>
			<Fumi
			  label={'Escriba el número'}
			  iconClass={MaterialCommunityIcons}
			  style={[styles.inputDato, {borderColor: this.state.secondColor}]}
			  labelStyle={{color: this.state.mainColor}}
			  iconName={'heart-pulse'}
			  inputStyle={{color: this.state.secondColor}}
			  iconColor={this.state.mainColor}
			  iconSize={20}
			  onChangeText={text => (this.state.datos.numero_emergencia = text)}
			  keyboardType='phone-pad'
			  maxLength={10}
			  value={this.state.datos.numero_emergencia}
			  returnKeyType='next'
			  autoCapitalize='none'
			  autoCorrect={false}
			/>
		 </View>
		 <View>
			<Text style={[styles.opcionsitas, {marginTop: 1, marginBottom: 7}]}>
			   Escriba un número donde siempre podamos localizarlo
			</Text>
		 </View>
		 <Fumi
		   label={'Teléfono casa'}
		   iconClass={Entypo}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'old-phone'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   maxLength={10}
		   onChangeText={text => (this.state.datos.telefono_casa_tutor = text)}
		   keyboardType='phone-pad'
		   value={this.state.datos.telefono_casa_tutor}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Teléfono celular'}
		   iconClass={Ionicons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'md-phone-portrait'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   maxLength={10}
		   onChangeText={text => (this.state.datos.telefono_celular_tutor = text)}
		   keyboardType='phone-pad'
		   value={this.state.datos.telefono_celular_tutor}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Teléfono oficina'}
		   iconClass={MaterialCommunityIcons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'deskphone'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   maxLength={10}
		   onChangeText={text => (this.state.datos.telefono_oficina_tutor = text)}
		   keyboardType='phone-pad'
		   value={this.state.datos.telefono_oficina_tutor}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'Email'}
		   iconClass={Entypo}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'email'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.email_tutor = text)}
		   keyboardType='email-address'
		   value={this.state.datos.email_tutor}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
	  </View>);
   }

   profesional() {
	  return (<View style={styles.datosBasicos}>
		 <Text style={[styles.main_title, {color: this.statethirdColor}]}>
			Datos generales
		 </Text>
		 <Fumi
		   label={'Ultimo Grado de Estudios'}
		   iconClass={Ionicons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'ios-paper-outline'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.estudios = text)}
		   keyboardType='default'
		   value={this.state.datos.estudios}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'R.F.C.'}
		   iconClass={Ionicons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'ios-paper-outline'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.rfc = text)}
		   keyboardType='default'
		   value={this.state.datos.rfc}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
		 <Fumi
		   label={'C.U.R.P.'}
		   iconClass={Ionicons}
		   style={[styles.inputDato, {borderColor: this.state.secondColor}]}
		   labelStyle={{color: this.state.mainColor}}
		   iconName={'ios-paper-outline'}
		   inputStyle={{color: this.state.secondColor}}
		   iconColor={this.state.mainColor}
		   iconSize={20}
		   onChangeText={text => (this.state.datos.curp = text)}
		   keyboardType='default'
		   value={this.state.datos.curp}
		   returnKeyType='next'
		   autoCapitalize='none'
		   autoCorrect={false}
		 />
	  </View>);
   }

   render() {
	  return (<View style={styles.container}>
		 <Modal
		   isVisible={this.state.isModalVisible}
		   backdropOpacity={0.5}
		   animationIn={'bounceIn'}
		   animationOut={'bounceOut'}
		   animationInTiming={1000}
		   animationOutTiming={1000}
		 >
			<View style={styles.containerMod}>
			   <View style={styles.modalClaus}>
				  <Text style={styles.titleClausula}>
					 CLÁUSULA DE RESPONSABILIDAD PARA LA ACTUALIZACIÓN DE MIS DATOS
					 DE CONTACTO
				  </Text>
				  <Text style={styles.textClaus}>
					 Entiendo y acepto que es mi total responsabilidad mantener mis
					 datos de contacto actualizados. Deslindo a la institución de
					 cualquier responsabilidad derivada de la imposibilidad de
					 localizarme en el caso de una emergencia debido a que no hubiese
					 mantenido actualizada mi información de contacto. Al guardar sus
					 datos estará aceptando la presente cláusula de responsabilidad.
				  </Text>
				  <TouchableOpacity
					style={[styles.btnAceptClaus, {backgroundColor: this.state.mainColor}]}
					onPress={() => this.setState({isModalVisible: false})}>
					 <Text style={{color: 'white'}}>Aceptar</Text>
				  </TouchableOpacity>
			   </View>
			</View>
		 </Modal>

		 <ScrollView showsVerticalScrollIndicator={false}>
			<TouchableOpacity
			  onPress={this._showModal}
			  style={{alignItems: 'flex-end'}}>
			   <Image
				 style={[styles.imagen, {height: 20, width: 20, marginTop: 10}]}
				 source={require('../../images/icons8/icons8-attention.png')}
			   />
			</TouchableOpacity>

			<Text
			  style={[styles.main_title, {color: this.state.thirdColor, marginTop: 0}]}>
			   Seleccione la categoría
			</Text>
			<View style={styles.widthall}>
			   <MultiBotonRow
				 itemBtns={['Generales', 'Domicilio', 'Contacto', 'Profesional']}
				 onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
				 cantidad={4}
			   />
			</View>

			{this.state.botonSelected === 'Generales' ? this.datosPersonales() : null}
			{this.state.botonSelected === 'Contacto' ? this.contacto() : null}
			{this.state.botonSelected === 'Domicilio' ? this.domicilio() : null}
			{this.state.botonSelected === 'Profesional' ? this.profesional() : null}
			<TouchableOpacity
			  style={[styles.bigButton, {
				 backgroundColor: this.state.secondColor, width: responsiveWidth(94)
			  }]}
			  onPress={() => this.requestMultipart()}>
			   <Text style={styles.textButton}>Guardar</Text>
			</TouchableOpacity>
		 </ScrollView>
	  </View>);
   }
}
