import React from 'react';
import {Alert, AsyncStorage, ScrollView, StatusBar, Text, TouchableHighlight, View} from 'react-native';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import styles from '../Tstyles';
import Swipeable from 'react-native-swipeable';
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';
import MultiBotonRow from './MultiBotonRow';
import ModalSelector from 'react-native-modal-selector';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


export default class Notificaciones extends React.Component {
	_changeWheelState = () => {
		this.setState({
			visible: !this.state.visible
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			items: {},
			laSolicitud: '',
			solicitudes: [],
			types: [],
			type: '',
			elRequest: [],
			indexSelected: 0,
			data: [],
			visible: false,
			swipeable: null
		};
	}

	async componentWillMount() {
		await this.getURL();
	}

	async getURL() {
		this.setState({
			uri: await AsyncStorage.getItem('uri'),
			token: await AsyncStorage.getItem('token'),
			mainColor: await AsyncStorage.getItem('maincolor'),
			secondColor: await AsyncStorage.getItem('secondColor'),
			thirdColor: await AsyncStorage.getItem('thirdColor'),
			fourthColor: await AsyncStorage.getItem('fourthColor'),
			role: await AsyncStorage.getItem('role'),
			puesto: await AsyncStorage.getItem('puesto')
		});
	}

	//++++++ Modal +++++++++
	async getFilterMias() {
		await fetch(this.state.uri + '/api/filter1/notificaciones/' + this.state.type, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Notificaciones)', [{
						text: 'Entendido', onPress: () => [Actions.drawer(), this.setState({visible: false})]
					}]);
				} else {
					this.setState({elRequest: responseJson, visible: false});
				}
			});
	}

	async getFilterPorMi() {

		await fetch(this.state.uri + '/api/filter2/notificaciones/' + this.state.type, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Notificaciones)', [{
						text: 'Entendido', onPress: () => [Actions.drawer(), this.setState({visible: false})]
					}]);
				} else {
					this.setState({elRequest: responseJson, visible: false});
				}
			});
	}

	async onChange(option) {
		await this.setState({type: option.id});
		await this._changeWheelState();
		if (this.state.indexSelected === 0) {
			await this.getFilterMias();
		} else {
			await this.getFilterPorMi();
		}
	}

	renderNotificaciones() {
		let buttonsRespuestas = [];
		this.state.elRequest.forEach((it, i) => {
			buttonsRespuestas.push(this.notificaciones(it, i));
		});
		return buttonsRespuestas;
	}

	async ir(it) {
		if (this.state.type === 'noticias') {
			let request = await fetch(this.state.uri + '/api/get/post/by/notification/' + it.url, {
				method: 'GET', headers: {
					Authorization: 'Bearer ' + this.state.token
				}
			});
			let data = await request.json();
			await this.setState({data: []});
			await this.setState({data: data});
			await Actions.newsSingle({data: this.state.data});
		} else if (this.state.type === 'cita') {
			if (this.state.role === 'Padre') {
				await Actions.agendarCitas();
			} else if (this.state.role === 'Maestro') {
				await Actions.gestCitasMtro();
			} else if (this.state.role === 'Admin') {
				if (this.state.puesto === 'Director') {
					await Actions.gestCitasAdmin();
				} else if (this.state.puesto === 'SubDirector') {
					await Actions.gestCitasAdmin();
				} else if (this.state.puesto === 'Coordinador Academico') {
					await Actions.gestCitasAdmin();
				} else if (this.state.puesto === 'Secretaria') {
					await Actions.gestCitasAdmin();
				}
			}

		}
	}


	async delete(id, i) {
		await fetch(this.state.uri + '/api/delete/notificacion/' + id, {
			method: 'GET', headers: {
				Accept: 'application/json',
				'Content-Type': 'multipart/form-data',
				Authorization: 'Bearer ' + this.state.token
			}
		}).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert('¡Ups!', 'Ha ocurrido un error ' + responseJson.error.status_code + ' si el error continua pónganse en contacto con soporte (Borrar notificaciones)', [{
						text: 'Entendido'
					}]);
				} else {
					this.state.elRequest.splice(i, 1);
					this.state.swipeable.recenter();
					this.setState({aux: 0});
				}
			});
	}

	notificaciones(it, i) {
		let rightButtons = [];
		if (this.state.indexSelected === 0) {
			rightButtons = [<TouchableHighlight
				onPress={() => this.delete(it.id, i)}
				underlayColor={'transparent'}
				style={{
					backgroundColor: '#FC451E',
					borderRadius: 6,
					marginVertical: 2,
					width: responsiveWidth(19),
					height: responsiveHeight(10),
					alignItems: 'center',
					justifyContent: 'center'
				}}
			>
				<MaterialIcons style={{width: 30, height: 30}} color={'#fff'}
							   name='delete-forever' size={30}/>
			</TouchableHighlight>];
		}
		else {
			rightButtons = [<TouchableHighlight><Text>{''}</Text></TouchableHighlight>]
		}
		return (<Swipeable key={i + 'not'}
						   onRef={ref => this.swipe = ref}
						   onRightButtonsOpenRelease={(event, gestureState, swipe) => {
							   if (this.state.swipeable && this.state.swipeable !== swipe) {
								   this.state.swipeable.recenter();
							   }
							   this.setState({swipeable: swipe});
						   }}
						   onRightButtonsCloseRelease={() => this.setState({swipeable: null})}
						   rightButtons={rightButtons}
		>
			<View
				style={[styles.notificaciones, {borderColor: this.state.secondColor}]}>
				<View style={{
					width: responsiveWidth(19),
					height: responsiveHeight(10),
					alignItems: 'center',
					justifyContent: 'center'
				}}>
					<Ionicons style={{width: 35, height: 35}} color={this.state.mainColor}
							  name='ios-notifications-outline'
							  size={35}/>
				</View>
				<View style={{width: responsiveWidth(65)}}>
					{this.state.type === 'cita' ? (<Text style={styles.notifTitle}>
						Cita agendada
					</Text>) : null}
					{this.state.type === 'noticias' ? (<Text style={styles.notifTitle}>
						Nueva noticia
					</Text>) : null}
					{this.state.type === 'pagos' ? (<Text style={styles.notifTitle}>
						Nuevo pago
					</Text>) : null}
					{this.state.type === 'encuadre' ? (<Text style={styles.notifTitle}>
						Encuadre
					</Text>) : null}
					<Text style={styles.textInfo}>
						{it.data.substr(0, 70) + '...'}
					</Text>
				</View>
				<View style={{
					width: responsiveWidth(10), alignItems: 'center', justifyContent: 'center'
				}}>
					<SimpleLineIcons onPress={() => this.ir(it)} style={{width: 30, height: 30}}
									 color={this.state.mainColor}
									 name='arrow-right' size={30}/>
				</View>
			</View>
		</Swipeable>);
	}

	//+++++++ MultibotonRow +++++++
	async botonSelected(indexSelected, itemSelected) {
		await this.setState({elRequest: []});
		await this.setState({
			botonSelected: itemSelected, indexSelected: indexSelected
		});
		if (this.state.indexSelected === 0 && this.state.type !== '') {
			await this.getFilterMias();
		} else if (this.state.indexSelected === 1 && this.state.type !== '') {
			await this.getFilterPorMi();
		}

	}

	render() {
		let index = 0;
		const notificaciones = this.renderNotificaciones();
		const data = [{key: index++, label: 'Pagos', id: 'pagos'}, {
			key: index++, label: 'Noticias', id: 'noticias'
		}, {key: index++, label: 'Citas', id: 'cita'}, {key: index++, label: 'Encuadres', id: 'encuadre'}];


		return (<View style={styles.container}>
			<Spinner visible={this.state.visible} textContent='Cargando...'/>
			<StatusBar
				backgroundColor={this.state.mainColor}
				barStyle='light-content'
			/>
			<MultiBotonRow
				itemBtns={['Para mí', 'Hechas por mí']}
				onSelectedButton={(indexBtn, itemBtn) => this.botonSelected(indexBtn, itemBtn)}
				cantidad={2}
			/>
			<Text style={[styles.main_title, {color: this.state.thirdColor}]}>Seleccione el tipo</Text>
			<ModalSelector
				data={data}
				cancelText='Cancelar'
				optionTextStyle={{
					color: this.state.thirdColor
				}}
				selectStyle={[styles.inputPicker, {
					borderColor: this.state.secondColor, marginTop: 2
				}]}
				initValue='Seleccione una opción'
				onChange={option => this.onChange(option)}
			/>
			<Text style={[styles.main_title, {color: this.state.thirdColor}]}>
				Seleccione la notificación
			</Text>
			<ScrollView style={[styles.widthall, {marginTop: 5, marginBottom: 20}]}
						showsVerticalScrollIndicator={false}>
				{notificaciones}
			</ScrollView>
		</View>);
	}
}
