import React from 'react';
import {Alert, AsyncStorage} from 'react-native';

import {GiftedChat} from 'react-native-gifted-chat';

export default class mensajes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            data: [],
            message: [],
            idUser: []
        };
    }

    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            uri: uri,
            token: token,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
        await this.getUserdata();
        await this.getFeed();
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getFeed() {
        let request = await fetch(
            this.state.uri +
            '/api/mensajes/gcbuid/' +
            (this.props.user_id === undefined
                ? this.props.data.id
                : this.props.data.role === 'Alumno'
                    ? this.props.data.padre_id
                    : this.props.data.id),
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
        let data = await request.json();
        let dataMapeada = data.map(mensaje => {
            return {
                _id: mensaje.id,
                text: mensaje.message,
                createdAt: mensaje.created_at,
                user: {
                    _id: mensaje.sender.id,
                    name: mensaje.sender.name
                }
            };
        });
        this.setState({messages: dataMapeada.reverse()});
    }

    async getUserdata() {
        let request = await fetch(this.state.uri + '/api/user/data', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({idUser: responseJson});
				}
			});
    }

    async onSend(messages = []) {
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages)
        }));
        let sendMsj = await fetch(
            this.state.uri +
            '/api/mensajes/send/' +
            (this.props.data.id === undefined
                ? this.props.user_id
                : this.props.data.role === 'Alumno'
                    ? this.props.data.padre_id
                    : this.props.data.id) +
            '/' +
            messages[0].text,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + this.state.token
                }
            }
        );
    }

    render() {
        return (
            <GiftedChat
                messages={this.state.messages}
                placeholder="Escriba su mensaje"
                isAnimated={true}
                locale={'es'}
                timeFormat={'LTS'}
                onSend={messages => this.onSend(messages)}
                user={{
                    _id: this.state.idUser.user_id,
                    avatar:
                        'https://www.shareicon.net/data/512x512/2016/07/08/117367_logo_512x512.png'
                }}
            />
        );
    }
}
