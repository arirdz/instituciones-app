import React, {Component} from 'react';
import {
    AsyncStorage,
    FlatList,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from '../Tstyles';

export default class messagesList extends Component {
    _keyExtractor = item => item.id;

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            token: ''
        };
    }

    async componentWillMount() {
        await this.getURL();
    }

    async getURL() {
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        this.setState({
            fourthColor: fourthColor,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            uri: uri,
            token: token
        });
        await this.getFeed();
    }

    async getFeed() {
        let token = await AsyncStorage.getItem('token');
        let request = await fetch(this.state.uri + '/api/mensajes/hilos', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        let data = await request.json();
        this.setState({data: data});
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <TouchableOpacity
                        style={[styles.mList, {borderColor: this.state.fourthColor}]}
                        onPress={() => Actions.newMessage()}>
                        <Text style={styles.nombre}>Nuevo Mensaje</Text>
                        <Entypo name="plus" size={20} color="black"/>
                    </TouchableOpacity>
                    <FlatList
                        keyExtractor={this._keyExtractor}
                        data={this.state.data}
                        renderItem={({item}) => (
                            <TouchableOpacity
                                style={[styles.mList, {borderColor: this.state.fourthColor}]}
                                onPress={() => Actions.Messages({data: item.withUser})}>
                                <Text style={styles.nombre}>{item.withUser.name}</Text>
                                <MaterialIcons
                                    name="keyboard-arrow-right"
                                    size={20}
                                    color="black"
                                />
                            </TouchableOpacity>
                        )}
                    />
                </ScrollView>
            </View>
        );
    }
}
