import React from "react";
import {
    AsyncStorage,
    StatusBar,
    View,
    Alert,
    Linking,
    BackHandler, ImageBackground
} from "react-native";
import {Actions} from "react-native-router-flux";
import styles from "../../Tstyles";
import Padre from "./padre";
import Maestro from "./maestro";
import Coordacad from "./coordacad";
import Director from "../menu/director";
import Admin from "./admin";
import CordDiscipl from "./cordDiscipl";
import OneSignal from "react-native-onesignal";
import crossroads from "crossroads";
import Subdirector from "./subdirector";
import Prefecto from "./Prefecto";
import Alumno from "./Alumno";
import Medicos from './medico';

export default class HomePageT extends React.Component {
    _handleNotification = notification => {
        this.setState({notification: notification});
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            dato: [],
            puesto: "",
            Auth: "",
            notification: {},
            id: {}
        };
    }

    componentDidMount() {
        Linking.getInitialURL()
            .then(url => this.handleOpenURL({url}))
            .catch(console.error);
        Linking.addEventListener("url", this.handleOpenURL);
        BackHandler.addEventListener('hardwareBackPress', () => {
            try {
                Actions.pop();
                return true;
            }
            catch (err) {
                console.debug("Can't pop. Exiting the app...");
                return false;
            }
        });
    }

    handleOpenURL(event) {
        if (event.url && event.url.indexOf("ce://") === 0) {
        }
    }

    async getUserData() {
        let request = await fetch(this.state.uri + '/api/user/data', {
            method: 'GET', headers: {Authorization: 'Bearer ' + await AsyncStorage.getItem('token')}
        }).then(res => res.json())
			.then(responseJson => {
				if (responseJson.error !== undefined) {
					Alert.alert(
						'Error al cargar datos', 'Ha ocurrido un error ' +
						// responseJson.error.status_code +
						' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (cod. 1)',
						[{text: 'Entendido'}]
					);
				} else {
					this.setState({datos: responseJson});
				}
			});

        let request1 = await fetch(this.state.uri + '/api/payment/dos/comprobar/cliente', {
            method: 'GET', headers: {Authorization: 'Bearer ' + await AsyncStorage.getItem('token')}
        });
        let dato1 = await request1.json();
        this.setState({cliente: dato1});
        {
            this.state.datos.passwordUpdated === '0'
                ? Actions.replace('PwdRest')
                : this.state.cliente === 0
                ? await AsyncStorage.getItem('role') === 'Padre'
                    ? Actions.replace('rugt')
                    : null
                : null;
        }
    }

    async componentWillMount() {
        await this.getURL();
        await this.getRole();
        await this.getPuesto();
        OneSignal.init('02510db7-3e82-4308-a15d-f81486901635');
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
        OneSignal.configure();
        await this.getUserData()
    }

    componentWillUnmount() {
        Linking.removeEventListener("url", this.handleOpenURL);
        OneSignal.removeEventListener("received", this.onReceived);
        OneSignal.removeEventListener("opened", this.onOpened);
        OneSignal.removeEventListener("ids", this.onIds);
        BackHandler.removeEventListener('hardwareBackPress');
    }

    onReceived(notification) {
        console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
        // console.log("Data: ", openResult.notification.payload.additionalData);
        // console.log("isActive: ", openResult.notification.isAppInFocus);
        // console.log("URL: ", openResult.notification.payload.launchURL);
        // console.log("ID: ", openResult.notification.payload.additionalData.id);
        // console.log("openResult: ", openResult);
    }

    async onIds(device) {
        let uri = await AsyncStorage.getItem("uri");
        let token = await AsyncStorage.getItem("token");
        try {
            let formData = new FormData();
            formData.append(
                "PNToken",
                JSON.stringify({
                    PushNotificationsID: device.userId
                })
            );
            let request = await fetch(uri + "/api/user/register/device", {
                method: "POST",
                headers: {
                    "Content-Type": "multipart/form-data",
                    Authorization: "Bearer " + token
                },
                body: formData
            });
        } catch (e) {
            console.warn(e);
            Alert.alert("Error", "Intenta más tarde por favor", [
                {
                    text: "Entendido"
                }
            ]);
        }
    }

    /* End push notifications */

    async getURL() {
        let auth = await AsyncStorage.getItem("token");
        let uri = await AsyncStorage.getItem("uri");
        this.setState({
            uri: uri,
            Auth: auth
        });
    }

    async getRole() {
        let role = await AsyncStorage.getItem("role");
        this.setState({admin: role});
    }

    async getPuesto() {
        let puesto = await AsyncStorage.getItem("puesto");
        this.setState({puesto: puesto});
    }

    mensajes(id) {
        Actions.Messages({
            user_id: id
        });
    }

    menuses() {
        if (this.state.admin === "Padre") {
            return (<Padre/>);
        }
        if (this.state.admin === "Maestro") {
            return (<Maestro/>);
        }
		if (this.state.admin === "Medico") {
			return (<Medicos/>);
		}
        if (this.state.admin === "Alumno") {
            return (<Alumno/>);
        }
        if (this.state.admin === "Admin" && this.state.puesto === "Coordinador Academico") {
            return (<Coordacad/>);
        }
        if (this.state.admin === "Admin" && this.state.puesto === "Secretaria") {
            return (<Admin/>);
        }
        if (this.state.admin === "Admin" && this.state.puesto === "Director") {
            return (<Director/>);
        }
        if (this.state.admin === "Admin" && this.state.puesto === "Cordisi") {
            return (<CordDiscipl/>);
        }
        if (this.state.admin === "Admin" && this.state.puesto === "SubDirector") {
            return (<Subdirector/>);
        }
        if (this.state.admin === "Admin" && this.state.puesto === "Prefecto") {
            return (<Prefecto/>);
        }
    }

    render() {
        // URL schemes
        crossroads.addRoute("mensajes/{id}", id => this.mensajes(id));

        const a = this.menuses();
        return (
            <ImageBackground
                style={styles.container}
                imageStyle={{ resizeMode: 'stretch' }}
                source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
            >
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {this.state.data.passwordUpdated === 0 ? Actions.PwdRest() : null}
                {a}
            </ImageBackground>
        );
    }
}
