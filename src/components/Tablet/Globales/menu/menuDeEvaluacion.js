import React from 'react';
import {
    Alert,
    AsyncStorage,
    Image,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';
import styles from '../../Tstyles';

export default class menuDeEvaluacion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            dato: [],
            notification: {}
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getRole();
    }

    async getURL() {
        let auth = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            url: uri,
            Auth: auth,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }

    itsAdmin() {
        const comingSoon = () => {
            Alert.alert(
                '¡Bloqueado!',
                'Esta funcionalidad aun no esta disponible para su escuela',
                [
                    {
                        text: 'Entendido'
                    }
                ]
            );
        };
        return (
            <View>
                {/* para iphone */}
                <MediaQuery maxDeviceHeight={889612}>
                    <View>
                        <ScrollView
                            style={styles.menu}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.row_M}>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.supervisionClas()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-classroom.png')}
                                    />
                                    <Text style={styles.btnText}>Supervisión de clase</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.ppiDocente()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-google_classroom.png')}
                                    />
                                    <Text style={styles.btnText}>PPI docentes</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.row_M}>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.criteriosAdmin()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-windows_defragmenter.png')}
                                    />
                                    <Text style={styles.btnText}>Criterios administrativos</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.encAlumn()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-under_computer.png')}
                                    />
                                    <Text style={styles.btnText}>Encuestar alumnos</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </MediaQuery>
                {/* para ipad */}
                <MediaQuery maxDeviceWidth={1024} minDeviceHeight={897}>
                    <View>
                        <ScrollView
                            style={styles.menu3}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.row_M1}>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M1,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.supervisionClas()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-classroom.png')}
                                    />
                                    <Text style={styles.btnText1}>Supervisión de clase</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M1,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.ppiDocente()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-google_classroom.png')}
                                    />
                                    <Text style={styles.btnText1}>PPI docentes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M1,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.criteriosAdmin()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-windows_defragmenter.png')}
                                    />
                                    <Text style={styles.btnText1}>Criterios administrativos</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M1,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.encAlumn()}>
                                    <Image
                                        style={styles.imagen}
                                        source={require('../../../images/icons8/icons8-under_computer.png')}
                                    />
                                    <Text style={styles.btnText1}>Encuestar alumnos</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </MediaQuery>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {this.state.admin === 'Admin' ? this.itsAdmin() : null}
                {/* {this.state.admin === "Maestro" ? this.itsMaestro() : null} */}
            </View>
        );
    }
}
