import React from 'react';
import {Alert, AsyncStorage, Image, ImageBackground, ScrollView, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';
import styles from '../../Tstyles';

export default class menuDePagosDir extends React.Component {
   constructor(props) {
	  super(props);
	  this.state = {
		 data: [], dato: [], notification: {}
	  };
   }

   async componentWillMount() {
	  await this.getURL();
	  await this.getRole();
   }

   async getURL() {
	  let auth = await AsyncStorage.getItem('token');
	  let uri = await AsyncStorage.getItem('uri');
	  let maincolor = await AsyncStorage.getItem('mainColor');
	  let secondColor = await AsyncStorage.getItem('secondColor');
	  let thirdColor = await AsyncStorage.getItem('thirdColor');
	  let fourthColor = await AsyncStorage.getItem('fourthColor');
	  this.setState({
		 url: uri,
		 Auth: auth,
		 mainColor: maincolor,
		 secondColor: secondColor,
		 thirdColor: thirdColor,
		 fourthColor: fourthColor
	  });
   }

   async getRole() {
	  let role = await AsyncStorage.getItem('role');
	  this.setState({admin: role});
   }

   itsAdmin() {
	  const comingSoon = () => {
		 Alert.alert('¡Bloqueado!', 'Esta funcionalidad aun no esta disponible para su escuela', [{text: 'Entendido'}]);
	  };
	  return (<View>
		   {/*para iphone  */}
		   <MediaQuery maxDeviceHeight={896}>
			  <View>
				 <ScrollView
				   style={styles.menu}
				   showsVerticalScrollIndicator={false}>
					<View style={styles.row_M}>
					   <TouchableOpacity
						 style={[styles.btnContainer_M, {
							backgroundColor: this.state.fourthColor
						 }]}
						 onPress={() => Actions.crearCargo()}>
						  <Image
							style={styles.imagen}
							source={require('../../../images/icons8/icons8-bank_cards.png')}
						  />
						  <Text style={styles.btnText}>Crear un nuevo cargo</Text>
					   </TouchableOpacity>
					   <TouchableOpacity
						 style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor}]}
						 onPress={() => Actions.historialPagosDir()}>
						  <Image
							style={styles.imagen}
							source={require('../../../images/icons8/icons8-payment_history.png')}
						  />
						  <Text style={styles.btnText}>Historial de pagos</Text>
					   </TouchableOpacity>
					</View>
					<View style={styles.row_M}>
					   <TouchableOpacity
						 style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor}]}
						 onPress={() => Actions.consultarSaldosDir()}>
						  <Image
							style={styles.imagen}
							source={require('../../../images/icons8/icons8-accounting.png')}
						  />
						  <Text style={styles.btnText}>Consultar saldos</Text>
					   </TouchableOpacity>
					   <TouchableOpacity
						 style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor}]}
                         onPress={() => Actions.pagosBecasDir()}>
						  <Image
							style={styles.imagen}
							source={require('../../../images/icons8/icons8-wallet.png')}
						  />
						  <Text style={styles.btnText}>Asignación de becas</Text>
					   </TouchableOpacity>
					</View>

					{/*<View style={styles.row_M}>*/}
					   {/*<TouchableOpacity*/}
						 {/*style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor}]}*/}
						 {/*onPress={() => Actions.pagosConfiguracionDir()}>*/}
						  {/*<Image*/}
							{/*style={styles.imagen}*/}
							{/*source={require('../images/icons8/icons8-pos_terminal.png')}*/}
						  {/*/>*/}
						  {/*<Text style={styles.btnText}>Configuración de cobros</Text>*/}
					   {/*</TouchableOpacity>*/}
					{/*</View>*/}
				 </ScrollView>
			  </View>
		   </MediaQuery>
		</View>);
   }

   render() {
	  return (<ImageBackground
          style={styles.container}
          imageStyle={{ resizeMode: 'stretch' }}
          source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
      >
		   <StatusBar
			 backgroundColor={this.state.mainColor}
			 barStyle='light-content'
		   />
		   {this.state.admin === 'Admin' ? this.itsAdmin() : null}
		</ImageBackground>);
   }
}
