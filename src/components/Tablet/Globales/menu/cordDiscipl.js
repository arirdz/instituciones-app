import React from "react";
import {AsyncStorage, Image, ScrollView, Text, TouchableOpacity, View} from "react-native";
import styles from "../../Tstyles";
import {Actions} from "react-native-router-flux";

export default class CordDiscipl extends React.Component {
    static propTypes = {};

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            menu: []
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getMenus();

    }

    async getURL() {
        this.setState({
            auth: await AsyncStorage.getItem('token'),
            uri: await AsyncStorage.getItem('uri'),
            maincolor: await AsyncStorage.getItem('mainColor'),
            secondColor: await AsyncStorage.getItem('secondColor'),
            thirdColor: await AsyncStorage.getItem('thirdColor'),
            fourthColor: await AsyncStorage.getItem('fourthColor'),
            fase: await AsyncStorage.getItem('fase')
        });

    }

    getMenus() {
        fetch(this.state.uri + '/api/listas/menus/Admin/Cordisi/' + this.state.fase, {
            method: 'GET', headers: {
                'Content-Type': 'application/json', Authorization: 'Bearer ' + this.state.token
            }
        })
            .then(response => {
                return response.json();
            })
            .then(recurso => {
                this.setState({menu: recurso});
            });
    }

    renderMenus() {
        let btn = [];
        for (let i = 0; i < this.state.menu.length; i = i + 3) {
            btn.push(this.renderMenu(i));
        }
        return btn;
    }

    renderMenu(i) {
        return (<View style={styles.row_M} key={i + 'menu'}>
            <TouchableOpacity
                style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor}]}
                onPress={() => Actions.push(this.state.menu[i].mobile)}>
                <Image
                    style={styles.imagen}
                    source={{uri: this.state.menu[i].url}}/>
                <Text style={styles.btnText}>{this.state.menu[i].nombre}</Text>
            </TouchableOpacity>

            {this.state.menu.length !== i + 1 ?
                <TouchableOpacity
                    style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor}]}
                    onPress={() => Actions.push(this.state.menu[i + 1].mobile)}>
                    <Image
                        style={styles.imagen}
                        source={{uri: this.state.menu[i + 1].url}}/>
                    <Text style={styles.btnText}>{this.state.menu[i + 1].nombre}</Text>
                </TouchableOpacity> : null}

            {this.state.menu.length > i + 2 ?
                <TouchableOpacity
                    style={[styles.btnContainer_M, {backgroundColor: this.state.fourthColor}]}
                    onPress={() => Actions.push(this.state.menu[i + 2].mobile)}>
                    <Image
                        style={styles.imagen}
                        source={{uri: this.state.menu[i + 2].url}}/>
                    <Text style={styles.btnText}>{this.state.menu[i + 2].nombre}</Text>
                </TouchableOpacity> : null}
        </View>);
    }

    render() {
        const menu = this.renderMenus();
        return (
            <View>
                <ScrollView
                    style={styles.menu}
                    showsVerticalScrollIndicator={false}>
                    {menu}
                </ScrollView>
            </View>);
    }
}
