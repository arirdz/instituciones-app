import React from 'react';
import {
    Alert,
    AsyncStorage,
    Image,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';
import styles from '../../Tstyles';
import ciclosPeriodos from "../../Parametros/ciclosPeriodos";

export default class menuParam extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            dato: [],
            notification: {}
        };
    }

    async componentWillMount() {
        await this.getURL();
        await this.getRole();
    }

    async getURL() {
        let auth = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        this.setState({
            url: uri,
            Auth: auth,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor
        });
    }

    async getRole() {
        let role = await AsyncStorage.getItem('role');
        this.setState({admin: role});
    }


    itsAdmin() {
        const comingSoon = () => {
            Alert.alert(
                '¡Bloqueado!',
                'Esta funcionalidad aun no esta disponible para su escuela',
                [
                    {text: 'Entendido'}
                ]
            );
        };
        return (
            <View>
                {/*para iphone  */}
                <MediaQuery maxDeviceHeight={896}>
                    <View>
                        <ScrollView
                            style={styles.menu}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.row_M}>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                    onPress={() => Actions.horasClase()}>
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: "https://png.icons8.com/dusk/40/000000/timer.png"}}
                                    />
                                    <Text style={styles.btnText}>Definir horas de clase</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {
                                            backgroundColor: this.state.fourthColor
                                        }
                                    ]}
                                    onPress={() => Actions.ciclosPeriodos()}
                                >
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: "https://png.icons8.com/color/40/000000/date-span.png"}}
                                    />
                                    <Text style={styles.btnText}>Definir ciclos y periodos escolares</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.row_M}>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                    onPress={() => Actions.captMateria()}>
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: "https://png.icons8.com/color/40/000000/books.png"}}
                                    />
                                    <Text style={styles.btnText}>Definir las materias</Text>
                                </TouchableOpacity>
								<TouchableOpacity
									style={[
										styles.btnContainer_M,
										{backgroundColor: this.state.fourthColor}
									]}
									onPress={() => Actions.cantidadGrupos()}
								>
									<Image
										style={styles.imagen}
										source={{uri: "https://png.icons8.com/dusk/40/000000/google-groups.png"}}
									/>
									<Text style={styles.btnText}>Definir grupos</Text>
								</TouchableOpacity>
                            </View>
                            <View style={styles.row_M}>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                    onPress={() => Actions.captMaestro()}>
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: "https://png.icons8.com/dusk/40/000000/training.png"}}
                                    />
                                    <Text style={styles.btnText}>Alta maestros</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                    onPress={() => Actions.horarios()}>
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: "https://png.icons8.com/dusk/40/000000/timetable.png"}}
                                    />
                                    <Text style={styles.btnText}>Definir horarios{'\n'}de grupos</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.row_M}>

                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                    onPress={() => Actions.asuntosParam()}>
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: "https://png.icons8.com/dusk/40/000000/today.png"}}
                                    />
                                    <Text style={styles.btnText}>Definir asuntos{'\n'}para citas</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.btnContainer_M,
                                        {backgroundColor: this.state.fourthColor}
                                    ]}
                                    onPress={() => Actions.listaVar()}>
                                    <Image
                                        style={styles.imagen}
                                        source={{uri: "https://png.icons8.com/color/40/000000/add-list.png"}}
                                    />
                                    <Text style={styles.btnText}>Crear lista{'\n'}personalizada</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </MediaQuery>
                {/*  para ipad*/}
                {/*<MediaQuery maxDeviceWidth={1024} minDeviceHeight={897}>*/}
                {/*<View>*/}
                {/*<ScrollView*/}
                {/*style={styles.menu2}*/}
                {/*showsVerticalScrollIndicator={false}>*/}
                {/*<View style={styles.row_M1}>*/}
                {/*<TouchableOpacity*/}
                {/*style={[*/}
                {/*styles.btnContainer_M1,*/}
                {/*{*/}
                {/*backgroundColor: this.state.fourthColor*/}
                {/*}*/}
                {/*]}*/}
                {/*onPress={() => Actions.crearCargo()}>*/}
                {/*<Image*/}
                {/*style={styles.imagen}*/}
                {/*source={require('../images/icons8/icons8-bank_cards.png')}*/}
                {/*/>*/}
                {/*<Text style={styles.btnText1}>Crear un nuevo cargo</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity*/}
                {/*style={[*/}
                {/*styles.btnContainer_M1,*/}
                {/*{*/}
                {/*backgroundColor: this.state.fourthColor*/}
                {/*}*/}
                {/*]}*/}
                {/*onPress={() => Actions.historialPagos()}>*/}
                {/*<Image*/}
                {/*style={styles.imagen}*/}
                {/*source={require('../images/icons8/icons8-payment_history.png')}*/}
                {/*/>*/}
                {/*<Text style={styles.btnText1}>Historial de pagos</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity*/}
                {/*style={[*/}
                {/*styles.btnContainer_M1,*/}
                {/*{*/}
                {/*backgroundColor: this.state.fourthColor*/}
                {/*}*/}
                {/*]}*/}
                {/*onPress={() => Actions.consultaSaldos()}>*/}
                {/*<Image*/}
                {/*style={styles.imagen}*/}
                {/*source={require('../images/icons8/icons8-accounting.png')}*/}
                {/*/>*/}
                {/*<Text style={styles.btnText1}>Consultar saldos</Text>*/}
                {/*</TouchableOpacity>*/}

                {/*<TouchableOpacity*/}
                {/*style={[*/}
                {/*styles.btnContainer_M1,*/}
                {/*{*/}
                {/*backgroundColor: this.state.fourthColor*/}
                {/*}*/}
                {/*]}*/}
                {/*onPress={() => Actions.facturasGeneradas()}>*/}
                {/*<Image*/}
                {/*style={styles.imagen}*/}
                {/*source={require('../images/icons8/icons8-check.png')}*/}
                {/*/>*/}
                {/*<Text style={styles.btnText1}>Ver facturas generadas </Text>*/}
                {/*</TouchableOpacity>*/}
                {/*</View>*/}

                {/*<View style={styles.row_M3}>*/}
                {/*<TouchableOpacity*/}
                {/*style={[*/}
                {/*styles.btnContainer_M1,*/}
                {/*{*/}
                {/*backgroundColor: this.state.fourthColor*/}
                {/*}*/}
                {/*]}*/}
                {/*onPress={() => Actions.pagosConfiguracion()}>*/}
                {/*<Image*/}
                {/*style={styles.imagen}*/}
                {/*source={require('../images/icons8/icons8-pos_terminal.png')}*/}
                {/*/>*/}
                {/*<Text style={styles.btnText1}>Configuración de cobros</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity*/}
                {/*style={[*/}
                {/*styles.btnContainer_M1,*/}
                {/*{*/}
                {/*backgroundColor: this.state.fourthColor*/}
                {/*}*/}
                {/*]}*/}
                {/*onPress={() => Actions.pagosVencidos()}>*/}
                {/*<Image*/}
                {/*style={styles.imagen}*/}
                {/*source={require('../images/icons8/icons8-clear_shopping_cart.png')}*/}
                {/*/>*/}
                {/*<Text style={styles.btnText1}>Pagos vencidos </Text>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity*/}
                {/*style={styles.btnContainer_M1}*/}
                {/*onPress={comingSoon}>*/}
                {/*<Image*/}
                {/*style={styles.imagen}*/}
                {/*source={require('../images/icons8/icons8-wallet.png')}*/}
                {/*/>*/}
                {/*<Text style={styles.btnText1}>Asignación de becas</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*</View>*/}
                {/*</ScrollView>*/}
                {/*</View>*/}
                {/*</MediaQuery>*/}
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.mainColor}
                    barStyle="light-content"
                />
                {this.itsAdmin()}
            </View>
        );
    }
}
