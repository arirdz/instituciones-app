import {AsyncStorage, Image, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Actions} from 'react-native-router-flux';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';

const styles = StyleSheet.create({
    container: {
        ...ifIphoneX(
            {
                paddingTop: Platform.OS === 'ios' ? responsiveHeight(2.5) : 4
            },
            {
                paddingTop: Platform.OS === 'ios' ? 4 : 0
            }
        )
    },
    navTitle: {
        color: '#e5e4da',
        width: responsiveWidth(90),
        height: responsiveHeight(5),
        marginTop: Platform.OS === 'ios' ? 6 : 0,
        fontSize: responsiveFontSize(3.5),
        fontWeight: '800',
        marginLeft: responsiveWidth(4),
        textAlign: 'left'
    },
    navTitleT: {
        color: '#e5e4da',
        width: responsiveWidth(90),
        height: Platform.OS === 'ios' ? responsiveHeight(5) : responsiveHeight(5),
        ...ifIphoneX(
            {
                marginTop: Platform.OS === 'ios' ? -15 : 0
            },
            {
                marginTop: Platform.OS === 'ios' ? -3 : 0
            }
        ),
        fontSize: responsiveFontSize(3),
        fontWeight: '800',
        marginLeft: responsiveWidth(4),
        textAlign: 'left'
    },
    navTitle1: {
        color: '#e5e4da',
        width: responsiveWidth(90),
        height: Platform.OS === 'ios' ? responsiveHeight(7) : responsiveHeight(7),
        ...ifIphoneX(
            {
                marginTop: Platform.OS === 'ios' ? -15 : 0
            },
            {
                marginTop: Platform.OS === 'ios' ? 24 : 0
            }
        ),
        fontSize: responsiveFontSize(3.5),
        fontWeight: '800',
        marginLeft: responsiveWidth(1),
        textAlign: 'left'
    },
    navBarItem: {
        height: 0
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: responsiveWidth(100),
        height: responsiveHeight(6),
        paddingTop: responsiveHeight(3)
    },
    row2: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: responsiveWidth(100),
        height: responsiveHeight(7)
    }
});

export default class CustomNavBarT extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            escuelasDatos: []
        };
    }

    async getSchoolData() {
        let scid = await AsyncStorage.getItem('schoolId');
        let schoolData = await fetch(
            'https://controlescolar.pro/api/get/escuelas/datos/' + scid
        );
        let scData = await schoolData.json();
        this.setState({escuelasDatos: scData[0]});
        await AsyncStorage.setItem('fase', this.state.escuelasDatos.plan).then(mainColor => {
            this.setState({mainColor: mainColor});
        });
    }

    async componentWillMount() {
        await this.getSchoolData();
        await AsyncStorage.getItem('mainColor').then(mainColor => {
            this.setState({mainColor: mainColor});
        });
        await AsyncStorage.getItem('secondColor').then(secondColor => {
            this.setState({secondColor: secondColor});
        });
        await AsyncStorage.getItem('thirdColor').then(thirdColor => {
            this.setState({thirdColor: thirdColor});
        });
        await AsyncStorage.getItem('fourthColor').then(fourthColor => {
            this.setState({fourthColor: fourthColor});
        });
    }

    back() {
        return (
            <TouchableOpacity
                onPress={() => Actions.push('HomePageT')}
                style={[
                    styles.navBarItem,
                    {
                        marginTop: 10,
                        paddingTop: 15,
                        width: responsiveWidth(8),
                        height: 40
                    }
                ]}>
                <Image
                    style={{
                        width: 15,
                        height: 23,
                        margin: 0
                    }}
                    resizeMode="contain"
                    source={require('../../images/back.png')}
                />
            </TouchableOpacity>
        );
    }

    render() {
        return (<View>
                <View style={[styles.row, {backgroundColor: this.state.secondColor}]}>
                {Actions.currentScene === 'HomePageT' ? null : <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={[
                        {
                            width: responsiveWidth(6.5),
                            height: responsiveHeight(10),
                            justifyContent: 'center',
                            alignItems: 'flex-end'
                        }
                    ]}>
                    <Image
                        style={{
                            width: responsiveWidth(3),
                            height: responsiveHeight(5),
                        }}
                        resizeMode="contain"
                        source={require('../../images/back.png')}
                    />
                </TouchableOpacity>}
                <Text
                    numberOfLines={1}
                    style={[
                        styles.navTitle,
                        {
                            width: responsiveWidth(80),
                            color: this.state.fourthColor
                        }
                    ]}>
                </Text>
                <TouchableOpacity
                    onPress={() => Actions.drawerOpen()}
                    style={[
                        styles.navBarItem,
                        {
                            width: responsiveWidth(9.5),
                            height: responsiveHeight(10),
                            justifyContent: 'center',
                            alignItems: 'flex-start'
                        }
                    ]}>
                    <Image
                        style={{
                            width: responsiveWidth(6),
                            height: responsiveHeight(10),
                        }}
                        resizeMode="contain"
                        source={require('../../images/menu.png')}
                    />
                </TouchableOpacity>
            </View>
                <View style={[styles.row2, {backgroundColor: this.state.secondColor}]}>
                    <Text
                        numberOfLines={1}
                        style={[
                            styles.navTitle,
                            {
                                width: responsiveWidth(100),
                                color: this.state.fourthColor
                            }
                        ]}>
                        {this.props.title}
                    </Text>
                </View>
            </View>
        );
    }
}
