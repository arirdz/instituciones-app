import React from 'react';
import {StyleSheet} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export const fourthColor = '#e5e4da';
export const iconColor = 'white';
export const iconDesactivatedColor = 'white';
export const primaryColor = '#263239';
export const secondColor = '#126775';
export const thirdColor = '#75745a';

export default StyleSheet.create({

    // +++++++++++ A +++++++++++
    // +++++++++++ B +++++++++++
    bigButtonLogin: {
        alignItems: 'center',
        backgroundColor: secondColor,
        borderColor: secondColor,
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(7),
        justifyContent: 'center',
        marginTop: 10,
        width: responsiveWidth(63)
    },
    btncuatro: {
        width: responsiveWidth(15.5)
    },
    btnIzq: {
        alignItems: 'center', justifyContent: 'center', width: 100
    },
    btnCentro: {
        alignItems: 'center', borderRadius: 0, justifyContent: 'center'
    },
    btnDer: {
        alignItems: 'center', justifyContent: 'center'
    },
    btnText: {
        color: primaryColor,
        fontSize: responsiveFontSize(1.55),
        marginTop: 7,
        textAlign: 'center',
        width: responsiveWidth(25),
        lineHeight: responsiveHeight(1.8)
    },
    btnContainer_M: {
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: fourthColor,
        borderRadius: 6,
        padding: 0,
        marginVertical: 2,
        marginHorizontal: 7,
        height: responsiveHeight(13),
        width: responsiveWidth(30)
    },
    bottomMenuElement: {
        color: 'white', fontSize: responsiveFontSize(2), fontWeight: '100', textAlign: 'center'
    },
    btn1: {width: responsiveWidth(90.7)},
    btn1_2: {width: responsiveWidth(51)},
    btn2: {width: responsiveWidth(46.08)},
    btn2_2T: {width: responsiveWidth(24)},
    btn_2: {width: responsiveWidth(21)},
    btnDAte: {width: responsiveWidth(34)},
    btn3: {width: responsiveWidth(30.88)},
    btn3_ll: {width: responsiveWidth(8)},
    btn3_llT: {width: responsiveWidth(15)},
    btn3_lll: {width: responsiveWidth(15)},
    btn3T: {width: responsiveWidth(25)},
    btn3L: {width: responsiveWidth(28)},
    btn3_l: {width: responsiveWidth(0)},
    btn3_3: {width: responsiveWidth(15)},
    btn2_3: {width: responsiveWidth(60.82)},
    btn4: {width: responsiveWidth(22.58)},
    btn4_l: {width: responsiveWidth(7)},
    btn4_lT: {width: responsiveWidth(12)},
    btn3_4: {width: responsiveWidth(67.74)},
    btn5: {width: responsiveWidth(17.88)},
    btn5_l: {width: responsiveWidth(8)},
    btn5_lT: {width: responsiveWidth(13)},
    btn5_ll: {width: responsiveWidth(7)},
    btn5_3: {width: responsiveWidth(7)},
    btn5_4: {width: responsiveWidth(6)},
    btn2_5: {width: responsiveWidth(35.76)},
    btn3_5: {width: responsiveWidth(55.4)},
    btn3_5_l: {width: responsiveWidth(25.4)},
    btn4_5: {width: responsiveWidth(71.52)},
    btn6: {width: responsiveWidth(14.3)},
    btn6_l: {width: responsiveWidth(7)},
    btn6_ls: {width: responsiveWidth(10)},
    btn6_lsl: {width: responsiveWidth(1)},
    btn6_6: {width: responsiveWidth(7)},
    btn7: {width: responsiveWidth(62)},
    btn8: {width: responsiveWidth(40)},
    btn9: {width: responsiveWidth(89.8)},
    btn11: {width: responsiveWidth(70)},
    btn10: {width: responsiveWidth(79)},
    btn12: {width: responsiveWidth(85)},
    borderTop2: {
        height: responsiveHeight(19), width: responsiveWidth(68)
    },
    // +++++++++++ C +++++++++++
    container: {
        alignItems: 'center', backgroundColor: '#fff', flex: 1
    },
    // +++++++++++ D +++++++++++
    // +++++++++++ E +++++++++++
    // +++++++++++ F +++++++++++
    footer: {
        fontSize: 10, fontWeight: 'normal', textAlign: 'center'
    },
    footersito: {
        fontSize: 10, fontWeight: 'normal', textAlign: 'center'
    },
    // +++++++++++ G +++++++++++
    // +++++++++++ H +++++++++++
    // +++++++++++ I +++++++++++
    inputPicker: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#bdbdbd',
        borderRadius: 6,
        borderWidth: 1,
        justifyContent: 'center',
        padding: 10,
        width: responsiveWidth(63)
    },
    inputDismmis: {
        textAlign: 'left', width: responsiveWidth(54)
    },
    imagen: {
        height: responsiveHeight(5), resizeMode: 'contain', width: responsiveWidth(10)
    },
    // +++++++++++ J +++++++++++
    jelow: {
        fontSize: 30, fontWeight: 'bold', marginTop: 15
    },
    // +++++++++++ K +++++++++++
    // +++++++++++ L +++++++++++
    logo: {
        height: responsiveHeight(40), marginBottom: 5, resizeMode: 'contain', width: responsiveWidth(47)
    },
    login: {
        alignItems: 'center', height: responsiveHeight(45)
    },
    // +++++++++++ M +++++++++++
    main_title: {
        alignItems: 'flex-start',
        color: thirdColor,
        fontSize: responsiveFontSize(1.6),
        fontWeight: '700',
        marginBottom: 2,
        paddingTop: 15,
        width: responsiveWidth(94)
    },
    menu: {
        flexDirection: 'column',
        marginBottom: responsiveHeight(1),
        width: responsiveWidth(94)
    },
    modalWidth: {
        width: responsiveWidth(44)
    },
    modalBigBtn: {
        borderWidth: 2,
        borderRadius: 6,
        width: responsiveWidth(41),
        height: responsiveHeight(5),
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 6
    },
    // +++++++++++ N +++++++++++
    noEscuela: {
        color: primaryColor, fontSize: responsiveFontSize(1.5), fontWeight: 'normal', marginTop: 25
    },
    // +++++++++++ O +++++++++++
    // +++++++++++ P +++++++++++
    // +++++++++++ Q +++++++++++
    // +++++++++++ R +++++++++++
    row: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        width: responsiveWidth(63)
    },

    row_M: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveWidth(1,5),
        width: responsiveWidth(94),
    },
    // +++++++++++ S +++++++++++
    // +++++++++++ T +++++++++++
    textoN: {
        color: 'black', fontSize: responsiveFontSize(1.2)
    },
    textButton: {
        color: 'white', fontSize: responsiveFontSize(1.5), fontWeight: '700',

        textAlign: 'center'
    },
    textoB: {
        color: 'white', fontSize: responsiveFontSize(1.2)
    },
    txtCenter: {
        textAlign: 'center'
    },
    textInputV2: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        height: 50,
        paddingLeft: 15,
        width: responsiveWidth(63),
    },
    textInput: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        height: 50,
        marginVertical: 3,
        paddingLeft: 15,
        textAlign: 'left',
        width: responsiveWidth(63)
    },
    textW: {fontWeight: '700'},
    // +++++++++++ U +++++++++++
    user: {
        color: 'white',
        fontSize: responsiveFontSize(1.7),
        fontWeight: '700',
        marginTop: 10,
        textAlign: 'center',
        width: responsiveWidth(30)
    },
    // +++++++++++ V +++++++++++
    // +++++++++++ X +++++++++++
    // +++++++++++ Y +++++++++++
    // +++++++++++ Z +++++++++++
});