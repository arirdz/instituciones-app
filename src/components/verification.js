import React from 'react';
import {Alert, AsyncStorage, View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';
import styles from './styles';

export default class verification extends React.Component {
   static navigationOptions = {
	  header: null
   };
   _changeWheelState = () => {
	  this.setState({
		 visible: !this.state.visible
	  });
   };

   constructor(props) {
	  super(props);
	  this.state = {
		 uri: '', escuelasDatos: [], visible: true, textInputValue: '', cososPicker: [], datos: [], dato: []
	  };
   }

   async saveItem(item, selectedValue, token) {
	  try {
		 await AsyncStorage.setItem(item, selectedValue, token);
	  } catch (error) {
		 console.error('AsyncStorage error: ' + error.message);
	  }
   }

   async getURL() {
	  this.setState({
		 uri: await AsyncStorage.getItem('uri'),
		 token: await AsyncStorage.getItem('token')
	  });

   }

   async componentDidMount() {
	  await  this.getURL();
	  await this.getUserdata();
   }

   async getUserdata() {
	  let request = await fetch(this.state.uri + '/api/user/data', {
		 method: 'GET', headers: {Authorization: 'Bearer ' + this.state.token}
	  }).then(res => res.json())
		  .then(responseJson => {
			  if (responseJson.error !== undefined) {
				  Alert.alert(
					  'Error al cargar datos', 'Ha ocurrido un error  al tratar de cargar los datos si el error continua pónganse en contacto con soporte',
					  [{text: 'Entendido'}]
				  );
			  } else {
				  this.setState({datos: responseJson, visible: !this.state.visible});
			  }
		  });
	  {
		 this.state.datos.passwordUpdated !== '0' ? Actions.menuPrincipal() : Actions.replace('PwdRest');
	  }
   }

   render() {
	  return (<View style={styles.login}>
		   <Spinner visible={this.state.visible} textContent='Verificando...'/>
		</View>);
   }
}
