import React from 'react';
import {
    Alert,
    AsyncStorage,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from './styles';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import Communications from 'react-native-communications';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {MediaQuery} from 'react-native-responsive';
import Modal from 'react-native-modal';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ModalSelector from 'react-native-modal-selector';

export default class sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            escuelasDatos: [],
            schoolData: [],
            noCampus: [],
            configData: [],
            nivelEscuela: [],
            elId_Esc: [],
            selectIndxCampus: -1,
            elCampus: '',
            indxSelectLvl: -1,
            elNivel: '',
            aux: 0,
            modalInfo: false,
            isModalComent: false,
            elComentario: '',
            password: '',
            Passwordconfirmation: '',
            selected: -1
        };
    }

    static async userLogout() {
        try {
            await AsyncStorage.clear();
            Actions.replace('Bienvenida', {type: 'reset'});
        } catch (error) {
            console.log('Ocurrió un error ' + error);
        }
    }

    handlePassword = text => {
        this.setState({password: text});
    };

    handlePasswordconfirmation = text => {
        this.setState({Passwordconfirmation: text});
    };

    async requestMultipart(password, Passwordconfirmation) {
        let token = await AsyncStorage.getItem('token');
        let uri = await AsyncStorage.getItem('uri');
        if (password !== '' && Passwordconfirmation !== '') {
            if (password === Passwordconfirmation) {
                if (password.length >= 6) {
                    try {
                        let formData = new FormData();
                        formData.append('password', JSON.stringify({password: this.state.password}));
                        let request = await fetch(uri + '/api/password/update/first/login', {
                            method: 'POST', headers: {
                                Accept: 'application/json',
                                'Content-Type': 'multipart/form-data',
                                Authorization: 'Bearer ' + token
                            }, body: formData
                        });
                        Alert.alert('Contraseña actualizada', 'Se ha cambiado la contraseña con éxito', [{
                            text: 'Entendido',
                            onPress: () => this.closeModal1()
                        }]);
                    } catch (e) {
                        Alert.alert('Error en el cambio de contraseña', 'Intenta más tarde por favor', [{
                            text: 'Entendido'
                        }]);
                    }
                } else {
                    Alert.alert('Contraseña invalida', 'La contraseña es muy corta', [{
                        text: 'Entendido'
                    }]);
                }
            } else {
                Alert.alert('Contraseñas no coinciden', 'Las contraseñas no coinciden', [{
                    text: 'Entendido'
                }]);
            }
        } else {
            Alert.alert('Datos incompletos', 'Asegúrate de llenar los dos campos', [{
                text: 'Entendido'
            }]);
        }
    }

    async componentWillMount() {
        await this.getURL();
        if (this.state.puesto !== undefined
            && this.state.role !== undefined
            && this.state.id !== undefined
            && this.state.name !== undefined
            && this.state.token !== undefined) {
            await this.getSchoolData();
            await this.getSchoolCamp();
            await this.getUserdata();
        }
    }

    async getUserdata() {
        let uri = await AsyncStorage.getItem('uri');
        let token = await AsyncStorage.getItem('token');
        await fetch(uri + '/api/user/data/v2', {
            method: 'GET', headers: {
                Authorization: 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert(
                        'Error al cargar datos', 'Ha ocurrido un error ' +
                        // responseJson.error.status_code +
                        ' al tratar de cargar los datos si el error continua pónganse en contacto con soporte (Cod. 4)',
                        [{text: 'Entendido'}]
                    );
                } else {
                    this.setState({data: responseJson});
                }
            });
    }

    async cleanNotificationsID() {
        await fetch(this.state.url + '/api/user/clean/notifId/' + this.state.id, {
            method: 'GET', headers: {Authorization: 'Bearer ' + this.state.token}
        }).then(res => res.json())
            .then(responseJson => {
            });
    }

    async alertUserLogOut() {
        Alert.alert(
            'Cerrar sesión', '¿Esta seguro que desea cerrar esta sesión?',
            [
                {text: 'Sí', onPress: () => [this.cleanNotificationsID(), sidebar.userLogout()]},
                {text: 'No'}
            ]
        )
    }


    //+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-Pruebas de cambio de escuela
    async globalConfigs() {
        let getConfig = await fetch(this.state.url + '/api/global/config/values');
        let configSave = await getConfig.json();
        await this.setState({configData: configSave});
    }

    async userChangeEsc(uri, ide) {
        try {
            await AsyncStorage.clear();
            let ide2 = await ide.toString();
            await AsyncStorage.setItem('uri', uri);
            await AsyncStorage.setItem('schoolId', ide2);
            let mc1 = await this.state.configData[0].second_option.toString();
            let sc1 = await this.state.configData[1].second_option.toString();
            let tc1 = await this.state.configData[2].second_option.toString();
            let fc1 = await this.state.configData[3].second_option.toString();
            await AsyncStorage.setItem('mainColor', mc1);
            await AsyncStorage.setItem('secondColor', sc1);
            await AsyncStorage.setItem('thirdColor', tc1);
            await AsyncStorage.setItem('fourthColor', fc1);
            Actions.push('Authentication', {email_user: this.state.data.email});
        } catch (error) {
            console.warn('Ocurrió un error ' + error);
        }

    }


    async getURL() {
        let uri = await AsyncStorage.getItem('uri');
        let maincolor = await AsyncStorage.getItem('mainColor');
        let secondColor = await AsyncStorage.getItem('secondColor');
        let thirdColor = await AsyncStorage.getItem('thirdColor');
        let fourthColor = await AsyncStorage.getItem('fourthColor');
        let puesto = await AsyncStorage.getItem('puesto');
        let role = await AsyncStorage.getItem('role');
        let id = await AsyncStorage.getItem('id');
        let token = await AsyncStorage.getItem('token');
        let name = await AsyncStorage.getItem('name');
        this.setState({
            url: uri,
            mainColor: maincolor,
            secondColor: secondColor,
            thirdColor: thirdColor,
            fourthColor: fourthColor,
            token: token,
            puesto: puesto,
            role: role,
            id: id,
            name: name
        });
    }

    async getSchoolData() {
        let schoolId = await AsyncStorage.getItem('schoolId');
        await fetch(this.state.url + '/api/get/escuelas/datos/' + schoolId, {
            method: 'GET', headers: {Authorization: 'Bearer ' + this.state.token}
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({escuelasDatos: responseJson[0]});
                }
            });
    }

    async getSchoolCamp() {
        await fetch('https://gestion-dev.controlescolar.pro/api/api/get/escuelas/datos/' + 1, {
            method: 'GET', headers: {'Content-Type': 'application/json'}
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({schoolData: responseJson});
                    this.getCampusByEsc();
                }
            });
    }

    //+-+-+-+-+-+-+-+-+-+-+-+-+-+ Campus de escuelas
    async getCampusByEsc() {
        await fetch('https://gestion-dev.controlescolar.pro/api/get/campus/' + this.state.schoolData.institucion, {
            method: 'GET', headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                } else {
                    this.setState({noCampus: responseJson});
                    this.elCampus();
                }
            });
    }

    async elCampus() {
        this.state.noCampus.forEach((itm, i) => {
            if (itm.campus === this.state.schoolData.campus) {
                this.setState({
                    elCampus: itm.campus,
                    selectIndxCampus: i
                });
            }
        });
        await this.setState({aux: 0});
        await this.getNivelesByCamp();
    }

    async onSelectCampus(option) {
        await this.setState({
            selectIndxCampus: option.key,
            elCampus: option.label
        });
        await this.setState({aux: 0});
        await this.getNivelesByCamp();
    }


    rndCampus() {
        if (this.state.noCampus.length === 1) {
            return (
                <TouchableOpacity
                    style={[styles.btn3_5, {
                        borderWidth: 1,
                        backgroundColor: this.state.mainColor,
                        borderRadius: 6,
                        padding: 1
                    }]}
                >
                    <Text style={[
                        styles.bottomMenuElement,
                        styles.textW,
                        {color: '#fff'}
                    ]}
                    >
                        Campus único
                    </Text>
                </TouchableOpacity>
            );
        } else if (this.state.noCampus.length >= 2) {
            let data = this.state.noCampus.map((itm, i) => {
                return {
                    key: i, label: itm.campus
                }
            });
            return (
                <View>
                    <ModalSelector
                        cancelText='Cancelar'
                        data={data}
                        initValue={this.state.elCampus}
                        onChange={option => this.onSelectCampus(option)}
                        optionTextStyle={{color: this.state.thirdColor, fontSize: responsiveFontSize(1.7)}}
                        selectTextStyle={{fontSize: responsiveFontSize(1.7)}}
                        selectStyle={[styles.btn3_5, {
                            borderWidth: 1,
                            backgroundColor: '#fff',
                            borderRadius: 6,
                            paddingVertical: 4,
                        }]}
                    />
                </View>
            );
        }
    }

//+-+-+-+-+-+-+-++-+-+ niveles escolares
    async getNivelesByCamp() {
        if (this.state.schoolData.institucion !== null) {
            await fetch('https://gestion-dev.controlescolar.pro/api/api/get/niveles/' + this.state.schoolData.institucion + '/' + this.state.elCampus, {
                method: 'GET', headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {

                    } else {
                        this.setState({nivelEscuela: responseJson});
                        this.elNivelDeCamp();
                    }
                });
        }
    }

    async onSelectLvlEsc(indx, nivel) {
        await this.setState({
            indxSelectLvl: indx,
            elNivel: nivel.nivel,
            elId_Esc: nivel.id,
            url: nivel.url_escuela
        });
        await this.setState({aux: 0});
        await this.alertLogOut(indx);
    }

    async elNivelDeCamp() {
        this.setState({elNivel: '', indxSelectLvl: -1, selected: -1});
        this.state.nivelEscuela.forEach((itm, i) => {
            if (this.state.schoolData.nivel === itm.nivel) {
                this.setState({elNivel: itm.nivel, indxSelectLvl: i, selected: i});
            }
        });
        await this.setState({aux: 0});
    }

    async alertLogOut() {
        await this.globalConfigs();
        Alert.alert(
            'Cambio de escuela',
            'Esta a punto de cambiar de nivel escolar a  ' + this.state.elNivel.toUpperCase() + '\n¿Esta seguro?',
            [
                {text: 'Si', onPress: () => this.userChangeEsc(this.state.url, this.state.elId_Esc)},
                {text: 'No', onPress: () => this.setState({elNivel: '', indxSelectLvl: -1})}
            ]
        );
    }

    rndLvls() {
        let btnLvl = [];
        let indxLvl = this.state.indxSelectLvl;
        if (this.state.nivelEscuela.length !== 0) {
            this.state.nivelEscuela.forEach((itm, indx) => {
                if (this.state.selected !== indx) {
                    btnLvl.push(<TouchableOpacity
                        key={indx + 'lvl'}
                        style={[styles.btn3_5,
                            {
                                marginVertical: 2,
                                borderWidth: 1,
                                backgroundColor: indxLvl === indx ? this.state.mainColor : '#f1f1f1',
                                borderRadius: 6,
                                padding: 5,
                            }
                        ]}
                        onPress={() => this.onSelectLvlEsc(indx, itm)}
                    >
                        <Text style={[
                            styles.bottomMenuElement, styles.textW,
                            {
                                color: indxLvl === indx ? '#fff' : '#000',
                                textAlign: 'left',
                                lineHeight: responsiveHeight(1.7),
                                fontSize: responsiveFontSize(1.6)
                            }
                        ]}>
                            {itm.nombre_escuela} {'\n'}
                            <Text style={[
                                styles.bottomMenuElement,
                                styles.textW,
                                {
                                    fontWeight: '200', color: indxLvl === indx ? '#fff' : '#000',
                                    textAlign: 'left',
                                    fontSize: responsiveFontSize(1.6)
                                }]}
                            >
                                Nivel {itm.nivel}
                            </Text>
                        </Text>
                    </TouchableOpacity>)
                } else {
                    btnLvl.push(<View
                        key={indx + 'lvl'}
                        style={[styles.btn3_5,
                            {
                                marginVertical: 2,
                                borderWidth: 1,
                                backgroundColor: this.state.mainColor,
                                borderRadius: 6,
                                padding: 5,
                            }
                        ]}
                    >
                        <Text style={[
                            styles.bottomMenuElement, styles.textW,
                            {
                                color: '#f1f1f1',
                                textAlign: 'left',
                                lineHeight: responsiveHeight(1.7),
                                fontSize: responsiveFontSize(1.6)
                            }
                        ]}>
                            {itm.nombre_escuela} {'\n'}
                            <Text style={[
                                styles.bottomMenuElement,
                                styles.textW,
                                {
                                    fontWeight: '200', color: '#f1f1f1',
                                    textAlign: 'left',
                                    fontSize: responsiveFontSize(1.6)
                                }]}
                            >
                                Nivel {itm.nivel}
                            </Text>
                        </Text>
                    </View>)
                }
            });
        }
        return btnLvl;
    }

    //+-+-+-+-+-+--+-+-+-+-+-+-+-+Get Comentarios
    async postEnviarComentarios() {
        await fetch(this.state.url + '/api/get/enviar/comentario/' +
            this.state.id +
            '/' +
            this.state.elComentario +
            '/' +
            this.state.escuelasDatos.id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.state.token
            }
        }).then(res => res.json())
            .then(responseJson => {
                if (responseJson.error !== undefined) {
                    Alert.alert('Error al enviar comentario',
                        'Ha ocurrido un error '
                        +
                        this.state.elRequest.error.status_code
                        +
                        ' si el error continua pónganse en contacto con soporte'
                        +
                        '(Enviar comentarios)',
                        [{
                            text: 'Entendido', onPress: () => Actions.drawer()
                        }]);
                } else {
                    Alert.alert('Comentario enviado',
                        'Se ha comentado el encuadre con éxito',
                        [{
                            text: 'Entendido',
                            onPress: () => this.closeModal()
                        }]);
                }
            });
    }

    async openModal() {
        await this.setState({isModalComent: true});
    }

    async closeModal() {
        await this.setState({isModalComent: false});
    }

    async openModal1() {
        await this.setState({contraM: true});
    }

    async closeModal1() {
        await this.setState({contraM: false});
    }

    async openModalInfo(state) {
        await this.setState({modalInfo: state});
    }

    render() {
        return (<View
                style={[
                    styles.container,
                    {backgroundColor: this.state.secondColor}
                ]}>
                {/*+-+-+---++-+- cambio de contraseña+-+--+-+*/}
                <Modal
                    isVisible={this.state.contraM}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <KeyboardAvoidingView behavior='position'>
                        <View style={{height: responsiveHeight(40), alignItems: 'center'}}>
                            <View style={[styles.container, {
                                width: responsiveWidth(94),
                                borderRadius: 10,
                                alignItems: 'center',
                                flex: 0
                            }]}>
                                <Text
                                    style={[styles.main_title, styles.modalWidth, {
                                        color: this.state.thirdColor,
                                        textAlign: 'center'
                                    }]}>
                                    Ingrese su nueva contraseña
                                </Text>

                                {Platform.OS === 'ios' ? (
                                    <View
                                        style={[styles.textInputV2, styles.row, styles.modalWidth, {
                                            borderColor: this.state.secondColor,
                                            marginTop: 10
                                        }]}>
                                        <TextInput
                                            ref='email'
                                            placeholder='Contraseña nueva'
                                            secureTextEntry={true}
                                            returnKeyType='next'
                                            underlineColorAndroid='transparent'
                                            autoCapitalize='none'
                                            onChangeText={this.handlePassword}
                                            autoCorrect={false}
                                            style={[
                                                styles.inputDismmis,
                                                {width: responsiveWidth(70), textAlign: 'left'}
                                            ]}
                                            onSubmitEditing={() => this.refs.password.focus()}
                                        />
                                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                                            <MaterialCommunityIcons color={'grey'} name='keyboard-close' size={25}/>
                                        </TouchableWithoutFeedback>

                                    </View>) : (<TextInput
                                    ref='email'
                                    placeholder='Contraseña nueva'
                                    secureTextEntry={true}
                                    returnKeyType='next'
                                    underlineColorAndroid='transparent'
                                    autoCapitalize='none'
                                    onChangeText={this.handlePassword}
                                    autoCorrect={false}
                                    style={[
                                        styles.textInput,
                                        styles.modalWidth,
                                        {borderColor: this.state.secondColor, textAlign: 'left'}
                                    ]}
                                    onSubmitEditing={() => this.refs.password.focus()}
                                />)}
                                <TextInput
                                    ref='password'
                                    placeholder='Confirmar contraseña'
                                    secureTextEntry={true}
                                    underlineColorAndroid='transparent'
                                    onChangeText={this.handlePasswordconfirmation}
                                    returnKeyType='go'
                                    autoCapitalize='none'
                                    style={[
                                        styles.textInput,
                                        styles.modalWidth,
                                        {borderColor: this.state.secondColor, marginTop: 7, textAlign: 'left'}
                                    ]}
                                    onSubmitEditing={() => this.requestMultipart(this.state.password, this.state.Passwordconfirmation)}
                                />
                                <View style={[styles.modalWidth, styles.row]}>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                        ]}
                                        onPress={() => this.closeModal1()}
                                    >
                                        <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[
                                            styles.modalBigBtn,
                                            {
                                                backgroundColor: this.state.secondColor,
                                                borderColor: this.state.secondColor
                                            }
                                        ]}
                                        onPress={() => this.requestMultipart(this.state.password, this.state.Passwordconfirmation)}
                                    >
                                        <Text style={[styles.textW, {color: '#fff'}]}>Cambiar contraseña</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </View>
                    </KeyboardAvoidingView>

                </Modal>
                {/*-+-+-+-+-+-+-+-+-+Comentarios-+-+-+-+-+-+-*/}
                <Modal
                    isVisible={this.state.isModalComent}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <KeyboardAvoidingView behavior='position'>
                        <View style={[styles.container, {width: responsiveWidth(94), borderRadius: 10, flex: 0}]}>
                            <Text
                                style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                                Coméntanos tú experiencia con la App ¿Como podemos mejorar?
                            </Text>
                            <TextInput
                                keyboardType='default'
                                returnKeyType='next'
                                multiline={true}
                                maxLength={250}
                                placeholder='Sea lo mas detallado posible...'
                                underlineColorAndroid='transparent'
                                onChangeText={text => (this.state.elComentario = text)}
                                style={[
                                    styles.inputContenido,
                                    styles.modalWidth,
                                    {borderColor: this.state.secondColor, padding: 10}
                                ]}
                            />
                            <View style={[styles.modalWidth, styles.row]}>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: '#fff', borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => [this.closeModal(), Keyboard.dismiss()]}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>Cancelar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[
                                        styles.modalBigBtn,
                                        {backgroundColor: this.state.secondColor, borderColor: this.state.secondColor}
                                    ]}
                                    onPress={() => [this.postEnviarComentarios(), Keyboard.dismiss()]}
                                >
                                    <Text style={[styles.textW, {color: '#fff'}]}>Enviar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </Modal>
                {/*+-+-+-+-+-+-Info de la escuela+-+-+-+-+-+-*/}
                <Modal
                    isVisible={this.state.modalInfo}
                    backdropOpacity={0.8}
                    animationIn={'bounceIn'}
                    animationOut={'bounceOut'}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                >
                    <View style={[styles.container, {borderRadius: 10, flex: 0}]}>
                        <Text style={[styles.main_title, styles.modalWidth, {color: this.state.thirdColor}]}>
                            Datos de la escuela
                        </Text>
                        <View style={{flexDirection: 'row', marginTop: 20}}>
                            <Ionicons name='md-school' size={26} color='#000' style={{width: responsiveWidth(10)}}/>
                            <View>
                                <Text
                                    style={[
                                        styles.bottomMenuElement,
                                        {
                                            color: this.state.mainColor,
                                            width: responsiveWidth(55),
                                            textAlign: 'left',
                                            fontWeight: '400'
                                        }
                                    ]}
                                >
                                    {this.state.schoolData.nombre_escuela}
                                </Text>
                                <Text
                                    style={[
                                        styles.bottomMenuElement,
                                        {
                                            color: this.state.mainColor,
                                            fontSize: responsiveFontSize(1.7),
                                            textAlign: 'left'
                                        }
                                    ]}
                                >
                                    {'  ' + this.state.schoolData.rvoe}
                                </Text>
                            </View>
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                marginTop: 10
                            }}
                        >
                            <Ionicons name='ios-call' size={26} color='#000' style={{width: responsiveWidth(10)}}/>
                            <View style={{width: responsiveWidth(55), flexDirection: 'row'}}>
                                <TouchableOpacity
                                    onPress={() => Communications.phonecall(this.state.schoolData.telefono, true)}
                                >
                                    <Text
                                        style={[
                                            styles.bottomMenuElement,
                                            {
                                                color: this.state.mainColor,
                                                width: responsiveWidth(55),
                                                textAlign: 'left',
                                                fontWeight: '400'
                                            }
                                        ]}
                                    >
                                        {this.state.schoolData.telefono}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 10}}>
                            <Entypo name='address' size={26} color='#000' style={{width: responsiveWidth(10)}}/>
                            <Text
                                style={{
                                    fontSize: responsiveFontSize(2),
                                    color: this.state.mainColor,
                                    width: responsiveWidth(55),
                                    textAlign: 'left',
                                    fontWeight: '400'
                                }}
                            >
                                {this.state.schoolData.direccion_administrativa}
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={[
                                styles.modalBigBtn,
                                {
                                    backgroundColor: '#fff',
                                    borderColor: this.state.secondColor,
                                    marginTop: 20,
                                    marginBottom: 15,
                                }
                            ]}
                            onPress={() => this.openModalInfo(false)}
                        >
                            <Text style={[styles.textW, {color: this.state.secondColor}]}>Cerrar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <View>
                    <MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
                        <View style={{width: responsiveWidth(60), alignItems: 'center'}}>
                            <Text style={[styles.welcome, {
                                textAlign: 'center',
                                ...ifIphoneX({marginTop: responsiveHeight(2)}, {marginTop: responsiveHeight(0)}),
                                width: responsiveWidth(60)
                            }]}>
                                Iniciaste sesión como:
                            </Text>
                            <Text style={[styles.user, styles.btn2, {
                                marginBottom: 0,
                                height: responsiveHeight(3),
                                fontStyle: 'italic'
                            }]}>
                                {this.state.role === 'Padre' || this.state.role === 'Alumno' || this.state.role === 'Prefecto'
                                || this.state.role === 'Maestro' ?
                                    this.state.role : this.state.role === 'Medico' ?
                                        this.state.role + ' escolar' : null}
                                {this.state.role === 'Admin' ?
                                    this.state.puesto === 'Cordisi' ? 'Coordinador de Disciplina' : this.state.puesto :
                                    null}{': '}

                            </Text>
                            <Text style={[styles.user, {
                                marginTop: 0,
                                fontSize: responsiveFontSize(2),
                                width: responsiveWidth(60)
                            }]}>
                                {this.state.name}
                                {'\n'}
                            </Text>
                            <View style={{
                                borderBottomColor: '#fff',
                                borderBottomWidth: 1,
                                width: responsiveWidth(60),
                                marginVertical: 5
                            }}/>
                            <View style={[{
                                width: responsiveWidth(60),
                                ...ifIphoneX({paddingVertical: responsiveHeight(3)}, {paddingVertical: responsiveHeight(1)}),
                                paddingLeft: responsiveWidth(1.5),
                                alignItems: 'center'
                            }]}>
                                <TouchableOpacity
                                    style={[styles.btn2, {
                                        flexDirection: 'row',
                                        marginVertical: 5,
                                        width: responsiveWidth(50)

                                    }]}
                                    onPress={() => Actions.push('HomePage')}>
                                    <Ionicons name='ios-home' size={25} color='#ffff'/>
                                    <Text style={[styles.menuElementTitle, {marginHorizontal: 20, marginTop: 1}]}>
                                        Menú principal
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.btn2, {
                                        flexDirection: 'row',
                                        marginVertical: 5,
                                        width: responsiveWidth(50)
                                    }]}
                                    onPress={() => this.alertUserLogOut()}>
                                    <Ionicons name='ios-log-out' size={25} color='#ffff'/>
                                    <Text style={[styles.menuElementTitle, {marginHorizontal: 20, marginTop: 1}]}>
                                        Cerrar Sesión
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.btn2, {
                                        flexDirection: 'row',
                                        marginVertical: 5,
                                        width: responsiveWidth(50)
                                    }]}
                                    onPress={() => this.openModal()}
                                >
                                    <FontAwesome name='commenting' size={25} color='#ffff'/>
                                    <Text style={[styles.menuElementTitle, {marginHorizontal: 15, marginTop: 1}]}>
                                        Enviar Sugerencias
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.btn2, {
                                        flexDirection: 'row',
                                        marginVertical: 5,
                                        width: responsiveWidth(50)
                                    }]}
                                    onPress={() => this.openModal1()}
                                >
                                    <MaterialCommunityIcons name='textbox-password' size={25} color='#ffff'/>
                                    <Text style={[styles.menuElementTitle, {marginHorizontal: 15, marginTop: 1}]}>
                                        Cambiar Contraseña
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            {this.state.role === 'Padre' && this.state.escuelasDatos.gestion === 'Institucional' ?
                                <View style={{
                                    borderBottomColor: '#fff',
                                    borderBottomWidth: 1,
                                    alignItems: 'center',
                                    width: responsiveWidth(60),
                                }}/> : null}
                            {this.state.role === 'Padre' && this.state.escuelasDatos.gestion === 'Institucional' ?
                                <View style={{width: responsiveWidth(60), alignItems: 'center', marginVertical: 10}}>
                                    {/*Aqui va el cambio de escuela*/}
                                    <View style={{
                                        alignItems: 'center',
                                        paddingVertical: responsiveHeight(.5)
                                    }}>
                                        <Text style={{
                                            color: 'white',
                                            fontSize: responsiveFontSize(1.7),
                                            fontWeight: '700',
                                            textAlign: 'center'
                                        }}>
                                            Consultar otra escuela
                                        </Text>
                                        <Text style={{
                                            fontWeight: '200', color: '#f1f1f1',
                                            textAlign: 'left',
                                            marginTop: 5,
                                            width: responsiveWidth(54),
                                            fontSize: responsiveFontSize(1.4)
                                        }}
                                        >
                                            Seleccione un campus:
                                        </Text>
                                        <View style={[styles.btn3_5, {
                                            flexDirection: 'row',
                                            marginVertical: 5,
                                            justifyContent: 'space-between'
                                        }]}>
                                            {this.rndCampus()}
                                        </View>
                                        <Text style={{
                                            fontWeight: '200', color: '#f1f1f1',
                                            textAlign: 'left',
                                            marginTop: 5,
                                            width: responsiveWidth(54),
                                            fontSize: responsiveFontSize(1.4)
                                        }}
                                        >
                                            Seleccione una escuela:
                                        </Text>
                                        {this.rndLvls()}
                                    </View>
                                </View>
                                : null}
                            <View style={{
                                borderBottomColor: '#fff',
                                borderBottomWidth: 1,
                                width: responsiveWidth(60),
                                marginBottom: 5
                            }}
                            />
                            <View style={{
                                width: responsiveWidth(60), alignItems: 'center',
                                ...ifIphoneX({paddingVertical: responsiveHeight(2)}, {paddingVertical: responsiveHeight(.5)})
                            }}>
                                <Text
                                    style={{
                                        color: 'white',
                                        fontSize: responsiveFontSize(1.7),
                                        fontWeight: '700',
                                        textAlign: 'center'
                                    }}
                                >
                                    Información de contacto{'\n'}de la escuela actual
                                </Text>
                                <TouchableOpacity
                                    style={{
                                        borderRadius: 6,
                                        backgroundColor: this.state.fourthColor,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        paddingVertical: 10,
                                        marginTop: 10,
                                        paddingHorizontal: 30
                                    }}
                                    onPress={() => this.openModalInfo(true)}
                                >
                                    <Text style={[styles.textW, {color: this.state.secondColor}]}>
                                        Consultar
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </MediaQuery>
                    <MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
                        <View>
                            <View>
                                <View style={styles.borderBottom}>
                                    <Text style={[styles.welcome, {textAlign: 'center', width: responsiveWidth(67)}]}>
                                        Iniciaste sesión como:
                                    </Text>
                                    <Text style={[styles.user, styles.btn2, {
                                        marginBottom: 0,
                                        height: responsiveHeight(3),
                                        fontStyle: 'italic'
                                    }]}>
                                        {this.state.role === 'Padre' || this.state.role === 'Alumno' || this.state.role === 'Prefecto' || this.state.role === 'Maestro' ?
                                            this.state.role : null}
                                        {this.state.role === 'Admin' ?
                                            this.state.puesto === 'Cordisi' ? 'Coordinador de Disciplina' : this.state.puesto :
                                            null}{': '}

                                    </Text>
                                    <Text style={[styles.bottomMenuElement, {marginTop: 0}]}>
                                        {'  ' + this.state.escuelasDatos.rvoe}
                                    </Text>
                                </View>
                            </View>
                            <View style={{marginVertical: 5}}>
                                <View
                                    style={[styles.btn1_2, {flexDirection: 'row', width: responsiveWidth(60)}]}>
                                    <Ionicons name='ios-call' size={25} color='#ffff'/>
                                    <Text style={[
                                        styles.menuElementTitle,
                                        {
                                            marginHorizontal: 18,
                                            fontSize: responsiveFontSize(2),
                                            fontWeight: '700'
                                        }
                                    ]}
                                    >
                                        Teléfono:
                                    </Text>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                    <View style={{width: responsiveWidth(8)}}/>
                                    <TouchableOpacity
                                        style={[styles.btn3, {
                                            borderWidth: 1,
                                            backgroundColor: '#fff',
                                            borderRadius: 6,
                                            padding: 5
                                        }]}
                                        onPress={() => Communications.phonecall(this.state.escuelasDatos.telefono, true)}>
                                        <Text style={[styles.bottomMenuElement, {color: this.state.mainColor}]}>
                                            {this.state.escuelasDatos.telefono}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </MediaQuery>
                    <MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
                        <View>
                            <View style={styles.borderBottom}>
                                <Text style={[styles.welcome, {textAlign: 'center', width: responsiveWidth(67)}]}>
                                    Iniciaste sesión como:
                                </Text>
                                <Text style={[styles.user, styles.btn2, {
                                    marginBottom: 0,
                                    height: responsiveHeight(3),
                                    fontStyle: 'italic'
                                }]}>
                                    {this.state.role === 'Padre' || this.state.role === 'Alumno' || this.state.role === 'Maestro' ?
                                        this.state.role : null}
                                    {this.state.role === 'Admin' ?
                                        this.state.puesto === 'Cordisi' ? 'Coordinador de Disciplina' : this.state.puesto :
                                        null}{': '}

                                </Text>
                                <Text style={[styles.user, {
                                    marginTop: 0,
                                    fontSize: responsiveFontSize(1.8),
                                    width: responsiveWidth(45)
                                }]}>
                                    {this.state.name}
                                    {'\n'}
                                </Text>
                            </View>
                            <View style={[styles.borderTop, {height: responsiveHeight(25)}]}>
                                <View style={[styles.menuContainer]}>
                                    <TouchableOpacity
                                        style={[styles.btn2, {
                                            flexDirection: 'row',
                                            marginBottom: 5,
                                            width: responsiveWidth(60)
                                        }]}
                                        onPress={() => Actions.push('HomePage')}>
                                        <Ionicons name='ios-home' size={15} color='#ffff'/>
                                        <Text style={[styles.menuElementTitle, {marginHorizontal: 20, marginTop: 0}]}>
                                            Menú principal
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.btn2, {
                                            flexDirection: 'row',
                                            marginVertical: 5,
                                            width: responsiveWidth(60)
                                        }]}
                                        onPress={() => this.alertUserLogOut()}>
                                        <Ionicons name='ios-log-out' size={15} color='#ffff'/>
                                        <Text style={[styles.menuElementTitle, {marginHorizontal: 20, marginTop: 0}]}>
                                            Cerrar Sesión
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.btn2, {
                                            flexDirection: 'row',
                                            marginVertical: 5,
                                            width: responsiveWidth(60)
                                        }]}>
                                        <Entypo name='open-book' size={15} color='#ffff'/>

                                        <Text style={[styles.menuElementTitle, {marginHorizontal: 15, marginTop: 0}]}>
                                            Manual del usuario
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.btn2, {
                                            flexDirection: 'row',
                                            marginVertical: 5,
                                            width: responsiveWidth(60)
                                        }]}>
                                        <Entypo name='tools' size={15} color='#ffff'/>
                                        <Text style={[styles.menuElementTitle, {marginHorizontal: 15, marginTop: 0}]}>
                                            Soporte Técnico
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.btn2, {
                                            flexDirection: 'row',
                                            marginVertical: 5,
                                            width: responsiveWidth(60)
                                        }]}>
                                        <FontAwesome name='commenting' size={15} color='#ffff'/>
                                        <Text style={[styles.menuElementTitle, {marginHorizontal: 15, marginTop: 0}]}>
                                            Enviar Sugerencias
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/*Aqui va el cambio de escuela*/}
                            {this.state.role === 'Padre' && this.state.escuelasDatos.gestion === 'Institucional' ?
                                <View style={{
                                    borderBottomColor: '#fff',
                                    borderBottomWidth: 1,
                                    alignItems: 'center',
                                    width: responsiveWidth(60),
                                }}/> : null}
                            {this.state.role === 'Padre' && this.state.escuelasDatos.gestion === 'Institucional' ?
                                <View style={{width: responsiveWidth(60), alignItems: 'center', marginVertical: 10}}>
                                    {/*Aqui va el cambio de escuela*/}
                                    <View style={{
                                        alignItems: 'center',
                                        paddingVertical: responsiveHeight(.5)
                                    }}>
                                        <Text style={{
                                            color: 'white',
                                            fontSize: responsiveFontSize(1.7),
                                            fontWeight: '700',
                                            textAlign: 'center'
                                        }}>
                                            Cambio de escuela
                                        </Text>
                                        <View style={[styles.btn3_5, {
                                            flexDirection: 'row',
                                            marginVertical: 5,
                                            justifyContent: 'space-between'
                                        }]}>
                                            {this.rndCampus()}
                                        </View>
                                        {this.rndLvls()}
                                    </View>
                                </View>
                                : null}
                            <View
                                style={[styles.btn2_3, {borderTopWidth: 1, marginVertical: 12, borderColor: '#ffff'}]}/>
                            <View style={styles.borderTop2}>
                                <View style={[styles.menuContainer]}>
                                    <View style={{marginVertical: 5}}>
                                        <View
                                            style={[styles.btn1_2, {flexDirection: 'row', width: responsiveWidth(60)}]}>
                                            <Ionicons name='md-school' size={15} color='#ffff'/>
                                            <Text
                                                style={[
                                                    styles.menuElementTitle,
                                                    {
                                                        marginHorizontal: 18,
                                                        fontSize: responsiveFontSize(2),
                                                        fontWeight: '700'
                                                    }
                                                ]}
                                            >
                                                {this.state.escuelasDatos.nombre_escuela}
                                            </Text>
                                        </View>
                                        <View
                                            style={[styles.btn1_2, {flexDirection: 'row', width: responsiveWidth(60)}]}>
                                            <Text style={[styles.user, {
                                                height: responsiveHeight(0),
                                                width: responsiveWidth(7)
                                            }]}>

                                            </Text>
                                            <Text style={[styles.bottomMenuElement, {marginTop: 0}]}>
                                                {'  ' + this.state.escuelasDatos.rvoe}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{marginVertical: 5}}>
                                        <View
                                            style={[styles.btn1_2, {flexDirection: 'row', width: responsiveWidth(60)}]}>
                                            <Ionicons name='ios-call' size={15} color='#ffff'/>
                                            <Text style={[
                                                styles.menuElementTitle,
                                                {
                                                    marginHorizontal: 18,
                                                    fontSize: responsiveFontSize(2),
                                                    fontWeight: '700'
                                                }
                                            ]}
                                            >
                                                Teléfono:
                                            </Text>
                                        </View>
                                        <View style={{flexDirection: 'row'}}>
                                            <View style={{width: responsiveWidth(8)}}/>
                                            <TouchableOpacity
                                                style={[styles.btn3, {
                                                    borderWidth: 1,
                                                    backgroundColor: '#fff',
                                                    borderRadius: 6,
                                                    padding: 5
                                                }]}
                                                onPress={() => Communications.phonecall(this.state.escuelasDatos.telefono, true)}>
                                                <Text style={[styles.bottomMenuElement, {color: this.state.mainColor}]}>
                                                    {this.state.escuelasDatos.telefono}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </MediaQuery>
                </View>

            </View>
        );
    }
}
