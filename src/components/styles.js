import React from 'react';
import {StyleSheet} from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export const fourthColor = '#E6E6E6';
export const iconColor = 'white';
export const iconDesactivatedColor = 'white';
export const primaryColor = '#263238';
export const secondColor = '#116876';
export const thirdColor = '#75745A';

export default StyleSheet.create({

    // +++++ A ++++++ revisado y acomodado

    adminContainer: {
        alignItems: 'center', flexDirection: 'row', justifyContent: 'center', width: responsiveWidth(94)
    },
    agoText: {
        fontSize: responsiveFontSize(1.3), color: 'grey', textAlign: 'right'
    },
    ajustIcon: {
        width: responsiveWidth(60), height: responsiveHeight(4), alignItems: 'center', marginTop: -1, marginBottom: 10
    },
    ajustMovAlum: {
        paddingTop: 0, width: responsiveWidth(94), paddingHorizontal: 5
    },
    alignSwitch: {width: responsiveWidth(16), alignItems: 'center'},
    alignView: {
        alignItems: 'center', justifyContent: 'center', width: responsiveWidth(94)
    },
    asuntos: {
        alignItems: 'stretch', flexDirection: 'row', justifyContent: 'space-between', width: responsiveWidth(94)
    },

    // +++++ B ++++++

    bigButton: {
        alignItems: 'center',
        backgroundColor: secondColor,
        height: responsiveHeight(7),
        justifyContent: 'center',
        marginTop: 15,
        ...ifIphoneX({
            marginBottom: 40
        }, {
            marginBottom: 15
        }),
        borderRadius: 6,
        width: responsiveWidth(94)
    },
    bigButtonLogin: {
        alignItems: 'center',
        backgroundColor: secondColor,
        borderColor: secondColor,
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(7),
        justifyContent: 'center',
        marginTop: 10,
        width: responsiveWidth(94)
    },
    bigButtonTT: {
        alignItems: 'center',
        backgroundColor: secondColor,
        height: responsiveHeight(7),
        justifyContent: 'center',
        marginTop: 10,
        left: 10,
        width: responsiveWidth(40)
    },
    bigButtonTT2: {
        alignItems: 'center', backgroundColor: secondColor, height: responsiveHeight(5),

        marginTop: 10, width: responsiveWidth(40)
    },
    bigButtonTTT2: {
        alignItems: 'center', backgroundColor: secondColor, height: responsiveHeight(5),

        marginTop: 10, width: responsiveWidth(25)
    },
    blackText: {
        fontSize: responsiveFontSize(1.8), fontWeight: '700', textAlign: 'center'
    },
    blanco: {
        color: '#000', paddingVertical: 7
    },
    borderBottom: {
        alignItems: 'center',
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        height: responsiveHeight(18),
        marginVertical: 12,
        justifyContent: 'center',
        width: responsiveWidth(60)
    },
    bordRad: {
        alignItems: 'center', borderRadius: 8, textAlign: 'center'
    },
    boorderToop: {
        borderTopColor: 'white',
        borderTopWidth: 1,
        marginBottom: 10,
        height: responsiveHeight(10),
        width: responsiveWidth(66)
    },
    botonsito: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        marginHorizontal: 1.5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        width: responsiveWidth(15)
    },
    botones_N: {
        alignItems: 'stretch', marginTop: 10, width: responsiveWidth(94)
    },
    botones_NM: {
        alignItems: 'stretch', marginTop: 10, width: responsiveWidth(94)
    },
    botones_C: {
        alignItems: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        width: responsiveWidth(94)
    },
    botones_C5: {
        alignItems: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 130,
        width: responsiveWidth(65)
    },
    botones_C5T: {
        alignItems: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 130,
        width: responsiveWidth(42)
    },
    botonesRow: {
        flexDirection: 'row', width: responsiveWidth(94)
    },
    boton: {
        backgroundColor: 'white',
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        marginHorizontal: 2,
        marginTop: 5,
        padding: 12
    },
    botones_CG: {
        alignItems: 'stretch', marginTop: 10, width: responsiveWidth(94)
    },
    botones: {
        alignItems: 'stretch', flexDirection: 'row', width: responsiveWidth(95)
    },
    botones7: {
        alignItems: 'stretch', flexDirection: 'row', width: responsiveWidth(45)
    },
    botones_T: {
        alignItems: 'stretch', flexDirection: 'row', width: responsiveWidth(50)
    },
    botones_PP: {
        alignItems: 'stretch', marginTop: 10, width: responsiveWidth(94)
    },
    bottomMenuContainer: {
        width: responsiveWidth(30)
    },
    bottomMenuTitle: {
        color: 'white', fontSize: responsiveFontSize(2.5), fontWeight: '800', marginVertical: 10, textAlign: 'center'
    },
    bottomMenuElement: {
        color: 'white', fontSize: responsiveFontSize(2), fontWeight: '100', textAlign: 'center'
    },
    ButtonsRow: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
        width: responsiveWidth(93)
    },
    buttonsRow: {
        flex: 1, flexDirection: 'row'
    },
    buttonsRow2: {
        flexDirection: 'row'
    },
    buttonDetalle: {
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'center',
        marginTop: 5,
        width: responsiveWidth(40)
    },
    buttonEnterados: {
        alignItems: 'center',
        backgroundColor: 'skyblue',
        borderRadius: 6,
        height: 60,
        justifyContent: 'center',
        marginHorizontal: 4,
        width: responsiveWidth(46)
    },
    buttonNoEnterados: {
        alignItems: 'center',
        backgroundColor: 'red',
        borderRadius: 6,
        height: 60,
        justifyContent: 'center',
        marginHorizontal: 4,
        width: responsiveWidth(46)
    },
    borderTop: {
        height: responsiveHeight(38), width: responsiveWidth(68)
    },
    borderTop2: {
        height: responsiveHeight(19), width: responsiveWidth(68)
    },
    buttonArriba: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 6,
        borderWidth: 1,
        height: 40,
        justifyContent: 'center',
        marginBottom: 10,
        marginTop: 20,
        width: responsiveWidth(30)
    },
    buttonArribaSelected: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        height: 40,
        justifyContent: 'center',
        marginBottom: 10,
        marginTop: 20,
        width: responsiveWidth(30)
    },
    button_PW: {
        alignItems: 'center',
        backgroundColor: primaryColor,
        borderRadius: 4,
        height: 50,
        justifyContent: 'center',
        marginBottom: 5,
        marginTop: 5,
        width: responsiveWidth(94)
    },
    buttonSnd: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#13587a',
        borderRadius: 6,
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        marginBottom: 10,
        marginTop: 10,
        paddingLeft: 10,
        width: responsiveWidth(94)
    },
    buttonAceptar: {
        alignItems: 'center',
        backgroundColor: primaryColor,
        borderRadius: 6,
        height: responsiveHeight(2.5),
        justifyContent: 'center',
        width: responsiveWidth(33)
    },
    buttonMensaje: {
        alignItems: 'center',
        backgroundColor: '#3fa345',
        borderRadius: 6,
        height: responsiveHeight(2.5),
        justifyContent: 'center',
        marginLeft: 3,
        width: responsiveWidth(33)
    },
    buttonAtencion: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#EE6500',
        borderRadius: 6,
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        marginBottom: 5,
        marginTop: 5,
        width: responsiveWidth(46.5)
    },
    buttonAtencion_2: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#EE6500',
        borderRadius: 6,
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        marginBottom: 5,
        marginTop: 5,
        width: responsiveWidth(20)
    },
    buttonExpediente_2: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#DE0000',
        borderRadius: 6,
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        marginBottom: 5,
        marginLeft: 3,
        marginTop: 5,
        width: responsiveWidth(20)
    },
    buttonExpediente: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#DE0000',
        borderRadius: 6,
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        marginBottom: 5,
        marginLeft: 3,
        marginTop: 5,
        width: responsiveWidth(47)
    },
    buttonSnd_AC: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#13587a',
        borderRadius: 6,
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        marginBottom: 10,
        marginTop: 20,
        paddingLeft: 10,
        width: responsiveWidth(94)
    },
    buttonSnd_AD: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#13587a',
        borderRadius: 6,
        borderWidth: 1,
        height: 60,
        justifyContent: 'center',
        marginBottom: 10,
        paddingLeft: 10,
        width: responsiveWidth(94)
    },
    btnPago: {
        alignItems: 'center',
        borderRadius: 6,
        height: responsiveHeight(6),
        justifyContent: 'center',
        width: responsiveWidth(95),
        marginTop: 20
    },
    btntres: {
        width: responsiveWidth(31)
    },
    btnstatus: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        height: responsiveHeight(6),
        justifyContent: 'center',
        marginTop: 5,
        width: responsiveWidth(22)
    },
    btnlistadocT: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        height: responsiveHeight(6),
        justifyContent: 'center',
        marginTop: 5,
        textAlign: 'center',
        width: responsiveWidth(60)
    },
    btndos: {
        width: responsiveWidth(46.9)
    },
    btnUno: {
        width: responsiveWidth(92.8)
    },
    btncuatro: {
        width: responsiveWidth(23.2)
    },

    btncuatro6: {
        width: responsiveWidth(25)
    },
    btnIzq: {
        alignItems: 'center', justifyContent: 'center', width: 100
    },
    btnDer: {
        alignItems: 'center', justifyContent: 'center'
    },
    btnCentro: {
        alignItems: 'center', borderRadius: 0, justifyContent: 'center'
    },
    btnCentro1: {
        alignItems: 'center', borderRadius: 0, justifyContent: 'center'
    },
    btnContainer: {
        flexDirection: 'row', marginHorizontal: 3, width: responsiveWidth(94)
    },
    btn1: {width: responsiveWidth(90.7)},
    btn1_2: {width: responsiveWidth(51)},
    btn2: {width: responsiveWidth(46.08)},
    btn2_2T: {width: responsiveWidth(24)},
    btn_2: {width: responsiveWidth(21)},
    btnDAte: {width: responsiveWidth(34)},
    btn3: {width: responsiveWidth(30.88)},
    btn3_ll: {width: responsiveWidth(8)},
    btn3_llT: {width: responsiveWidth(15)},
    btn3_lll: {width: responsiveWidth(15)},
    btn3T: {width: responsiveWidth(25)},
    btn3L: {width: responsiveWidth(28)},
    btn3_l: {width: responsiveWidth(0)},
    btn3_3: {width: responsiveWidth(15)},
    btn2_3: {width: responsiveWidth(60.82)},
    btn4: {width: responsiveWidth(22.58)},
    btn4_l: {width: responsiveWidth(7)},
    btn4_lT: {width: responsiveWidth(12)},
    btn3_4: {width: responsiveWidth(67.74)},
    btn5: {width: responsiveWidth(17.88)},
    btn5_l: {width: responsiveWidth(8)},
    btn5_lT: {width: responsiveWidth(13)},
    btn5_ll: {width: responsiveWidth(7)},
    btn5_3: {width: responsiveWidth(7)},
    btn5_4: {width: responsiveWidth(6)},
    btn2_5: {width: responsiveWidth(35.76)},
    btn3_5: {width: responsiveWidth(55.4)},
    btn3_5_l: {width: responsiveWidth(25.4)},
    btn4_5: {width: responsiveWidth(71.52)},
    btn6: {width: responsiveWidth(14.3)},
    btn6_l: {width: responsiveWidth(7)},
    btn6_ls: {width: responsiveWidth(10)},
    btn6_lsl: {width: responsiveWidth(1)},
    btn6_6: {width: responsiveWidth(7)},
    btn7: {width: responsiveWidth(62)},
    btn8: {width: responsiveWidth(40)},
    btn9: {width: responsiveWidth(89.8)},
    btn11: {width: responsiveWidth(70)},
    btn10: {width: responsiveWidth(79)},
    btn12: {width: responsiveWidth(85)},
    btnContainer1: {
        alignItems: 'center',
        borderColor: secondColor,
        borderRadius: 6,
        height: responsiveHeight(12, 5),
        marginBottom: 10,
        padding: 10,
        width: responsiveWidth(44)
    },
    btnContainerDisabled: {
        alignItems: 'center',
        backgroundColor: 'lightgray',
        borderRadius: 6,
        justifyContent: 'center',
        height: responsiveHeight(13),
        padding: 7,
        width: responsiveWidth(46)
    },
    btnContainerDisabled1: {
        alignItems: 'center',
        backgroundColor: 'lightgray',
        borderRadius: 6,
        height: responsiveHeight(12, 5),
        padding: 10,
        width: responsiveWidth(20)
    },
    btnText1: {
        color: 'white', fontSize: responsiveFontSize(2), textAlign: 'center', width: responsiveWidth(27)
    },
    btnContainer_M: {
        alignItems: 'center',
        backgroundColor: fourthColor,
        borderRadius: 6,
        padding: 7,
        justifyContent: 'center',
        height: responsiveHeight(13),
        width: responsiveWidth(46)
    },
    btnContainer_M1: {
        alignItems: 'center',
        backgroundColor: fourthColor,
        borderRadius: 6,
        height: responsiveHeight(12, 5),
        padding: 10,
        width: responsiveWidth(20)
    },
    btnPorcent: {
        borderWidth: 1,
        backgroundColor: '#ffff',
        margin: 4
    },
    btnText: {
        color: primaryColor,
        fontSize: responsiveFontSize(1.75),
        marginTop: 5,
        textAlign: 'center',
        width: responsiveWidth(46)
    },
    btn: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#13587a',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(8),
        justifyContent: 'center',
        marginHorizontal: 4,
        marginTop: 10,
        width: responsiveWidth(47)
    },
    btnContainer_FDP: {
        alignItems: 'center',
        borderColor: 'black',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(10),
        padding: 0,
        width: responsiveWidth(44)
    },
    btnContainer_PA: {
        alignItems: 'center',
        borderColor: 'black',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(10),
        padding: 13,
        width: responsiveWidth(44)
    },
    btnText_PA: {
        color: 'black',
        fontSize: responsiveFontSize(1.8),
        paddingTop: 2,
        textAlign: 'center',
        width: responsiveWidth(27)
    },
    btn_asistencia: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: '#4f8ff7',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(7),
        marginTop: 5,
        width: responsiveWidth(30)
    },
    btn_gg: {
        backgroundColor: 'white',
        borderColor: 'black',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(3),
        width: responsiveWidth(10)
    },
    btn_imp: {
        marginTop: 0, width: responsiveWidth(60)
    },
    btn_blue: {
        alignItems: 'center',
        backgroundColor: '#5DADE2',
        borderRadius: 6,
        height: responsiveHeight(10),
        justifyContent: 'center',
        marginTop: 10,
        width: responsiveWidth(26)
    },
    btnPeriod: {
        alignItems: 'center',
        backgroundColor: secondColor,
        borderRadius: 6,
        height: responsiveHeight(4),
        justifyContent: 'center',
        width: responsiveWidth(50)
    },
    btnLista: {
        width: responsiveWidth(47),
        height: responsiveHeight(5),
        borderWidth: 1,
        borderRadius: 6,
        backgroundColor: 'white'
    },
    button: {
        alignItems: 'center',
        backgroundColor: primaryColor,
        borderColor: primaryColor,
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(10),
        justifyContent: 'center',
        width: responsiveWidth(40)
    },
    btnlistadoc: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        height: responsiveHeight(6),
        justifyContent: 'center',
        marginTop: 5,
        paddingHorizontal: 15,
        width: responsiveWidth(70)
    },
    btnstatusTT: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        height: responsiveHeight(6.2),
        justifyContent: 'center',
        marginTop: 5,
        width: responsiveWidth(12)
    },
    btnlistadocTT: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        height: responsiveHeight(6),
        justifyContent: 'center',
        marginTop: 5,

        width: responsiveWidth(32)
    },
    btnstatusT: {
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        height: responsiveHeight(6),
        justifyContent: 'flex-start',
        marginTop: 5,
        textAlign: 'center',
        width: responsiveWidth(32)
    },
    btnExam: {
        borderWidth: 1,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        width: responsiveWidth(21.5),
        height: responsiveHeight(6)
    },
    btnCalifs: {
        width: responsiveWidth(50),
        height: responsiveHeight(7),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    btnCalifsT: {
        width: responsiveWidth(25),
        height: responsiveHeight(7),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    btn_Exam: {
        borderWidth: 1,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        height: responsiveHeight(3),
        marginTop: 5,
        width: responsiveWidth(11)
    },
    btncitoEnc: {
        marginRight: 5, borderWidth: 1, borderRadius: 100, height: responsiveHeight(3), width: responsiveWidth(5)
    },
    btnShowMod: {
        width: responsiveWidth(47),
        height: responsiveHeight(4),
        borderWidth: 1,
        borderRadius: 6,
        backgroundColor: 'white'
    },
    btnEsp: {
        borderWidth: 1,
        height: responsiveHeight(5),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        width: responsiveWidth(50)
    },
    btnCita: {
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        marginLeft: 5,
        padding: 5
    },
    btnIconCita: {
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 3,
        marginLeft: 5
    },
    btncitoPors: {
        height: responsiveHeight(4),
        width: responsiveWidth(12),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6
    },
    btnAceptClaus: {
        alignItems: 'center',
        justifyContent: 'center',
        width: responsiveWidth(25),
        height: responsiveHeight(5),
        marginTop: 5,
        borderRadius: 6
    },
    btnSwip: {
        height: responsiveHeight(10),
        width: responsiveWidth(21),
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnModal: {
        borderBottomLeftRadius: 6, borderBottomRightRadius: 6
    },

    // +++++ C +++++
    cardCita: {
        borderWidth: 1,
        marginTop: 10,
        borderRadius: 6,
        width: responsiveWidth(94),
        backgroundColor: '#fff'
    },
    colInfo2: {
        borderRadius: 0,
        margin: 0,
        paddingVertical: 5,
        width: responsiveWidth(62),
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    container: {
        alignItems: 'center', backgroundColor: '#f9f9f9', flex: 1
    },
    containerMod: {
        flex: 1, alignItems: 'center', justifyContent: 'center'
    },
    columnaT: {
        alignItems: 'center', backgroundColor: '#fff', flex: 1, width: responsiveWidth(47)
    },
    campoTabla: {
        alignItems: 'center', borderLeftWidth: 1, justifyContent: 'center', padding: 8, width: responsiveWidth(26)
    },
    campoTablaT: {
        alignItems: 'center', borderLeftWidth: 1, justifyContent: 'center', padding: 8, width: responsiveWidth(12)
    },
    campoTabla1: {
        alignItems: 'center', borderLeftWidth: 1, justifyContent: 'center', padding: 6, width: responsiveWidth(22)
    },
    campoTablaG: {
        alignItems: 'center', justifyContent: 'center', padding: 8, width: responsiveWidth(94)
    },
    contentContainer: {
        alignItems: 'center'
    },
    centered: {
        alignItems: 'center', flexDirection: 'column', justifyContent: 'center', width: responsiveWidth(94)
    },
    checked: {
        alignItems: 'flex-start', flexDirection: 'column', marginVertical: 5, width: responsiveWidth(94)
    },
    centered_RT: {
        alignItems: 'center'
    },
    campoTabla1_RT: {
        alignItems: 'center', borderLeftWidth: 1, justifyContent: 'center', padding: 6, width: responsiveWidth(23.4)
    },
    columna: {
        alignItems: 'center', borderLeftWidth: 1, justifyContent: 'center', padding: 6, width: responsiveWidth(47)
    },
    col1: {
        width: responsiveWidth(25)
    },
    centeredTxt: {
        fontWeight: '700', textAlign: 'center'
    },
    contInas: {
        borderColor: 'gray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(40),
        marginTop: 5,
        width: responsiveWidth(80)
    },
    contAlumnos: {
        height: responsiveHeight(20), width: responsiveWidth(94)
    },
    contAlumnos_2: {
        height: responsiveHeight(25), width: responsiveWidth(50)
    },
    contTarea: {
        width: responsiveWidth(94), padding: 12
    },
    cargos: {
        alignItems: 'stretch', flexDirection: 'row', justifyContent: 'center', width: responsiveWidth(94)
    },
    cardContainer: {
        marginVertical: 10
    },
    containerCord: {
        flex: 1, height: responsiveHeight(20), width: responsiveWidth(94)
    },
    containerCordT: {
        flex: 1, height: responsiveHeight(40), width: responsiveWidth(44)
    },
    cartPed: {
        marginTop: 10, height: responsiveHeight(18), width: responsiveWidth(94), borderWidth: 1, borderRadius: 4
    },
    cantprend: {
        alignItems: 'center',
        borderBottomWidth: 1,
        height: responsiveHeight(10),
        paddingHorizontal: 5,
        width: responsiveWidth(46.5)
    },
    cuadroCalifs: {
        marginTop: 10,
        borderWidth: 1,
        borderRadius: 6,
        width: responsiveWidth(94),
        height: responsiveHeight(50),
        alignItems: 'center',
        paddingVertical: 5
    },
    cuadroCalifs2: {
        borderWidth: 1,
        borderRadius: 6,
        paddingHorizontal: 5,
        width: responsiveWidth(55),
        height: responsiveHeight(60),
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    cuadroCalifs3: {
        borderWidth: 1,
        borderRadius: 6,
        paddingHorizontal: 5,
        width: responsiveWidth(38),
        height: responsiveHeight(60),
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    calif_Exam: {
        borderWidth: 1, borderRadius: 6, width: responsiveWidth(45.5), height: responsiveHeight(15)
    },
    califList: {
        marginTop: 10, alignItems: 'center', justifyContent: 'center', width: responsiveWidth(94), marginBottom: 5
    },
    califPerCord: {
        width: responsiveWidth(37),
        height: responsiveHeight(5.5),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderLeftWidth: 0,
        borderBottomWidth: 0
    },
    califPerCord2: {
        width: responsiveWidth(37),
        height: responsiveHeight(30),
        alignItems: 'center',
        borderWidth: 1,
        borderLeftWidth: 0
    },
    conceptEst: {
        width: responsiveWidth(23), justifyContent: 'space-between'
    },
    cuadPorcent2: {
        alignItems: 'center', borderColor: 'black', height: responsiveHeight(4.5), justifyContent: 'center'
    },
    cuadroEstd: {
        marginTop: 7, height: responsiveHeight(4.5), alignItems: 'center', justifyContent: 'center', borderRadius: 6
    },
    cuadroess: {
        marginTop: 7,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        width: responsiveWidth(50),
        padding: 10,
        height: responsiveHeight(5)
    },
    cuadroEstd2: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(4.5),
        justifyContent: 'center',
        marginTop: 7
    },
    colInfo: {
        borderRadius: 0,
        margin: 0,
        paddingVertical: 5,
        width: responsiveWidth(32),
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    contAlumnos2: {
        height: responsiveHeight(20),
        width: responsiveWidth(94),
        marginTop: 10,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: 'black'
    },
    contAlumnos2T: {
        height: responsiveHeight(20),
        width: responsiveWidth(45),
        marginTop: 10,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: 'black'
    },
    contClausula: {
        fontSize: responsiveFontSize(2.2),
        width: responsiveWidth(69),
        height: responsiveHeight(45),
        marginTop: 15,
        textAlign: 'center',
        fontWeight: '300'
    },
    ciclosRen: {
        marginLeft: 134, width: responsiveWidth(47), flexDirection: 'row', justifyContent: 'space-between'
    },
    cardPago: {
        alignItems: 'center', borderBottomWidth: 1, borderTopWidth: 1
    },
    contIcon: {
        justifyContent: 'center', alignItems: 'center', height: responsiveHeight(10), width: responsiveWidth(12)
    },
    contPago: {
        height: responsiveHeight(5), width: responsiveWidth(82)
    },
    cardHoras: {
        borderWidth: 0.5, borderColor: 'lightgray', paddingLeft: 10, paddingRight: 5, marginTop: 5, borderRadius: 6
    },
    cardMat: {
        padding: 9, marginTop: 10, borderWidth: 0.5, borderRadius: 6, borderColor: 'lightgray'
    },
    cardEnc: {
        height: responsiveHeight(4.5),
        marginVertical: 2,
        borderWidth: 0.5,
        borderRadius: 6,
        borderColor: 'lightgray',
        paddingHorizontal: 20
    },

    // +++++ D +++++

    diasLab: {
        alignItems: 'center', borderTopWidth: 1, paddingHorizontal: 10, paddingVertical: 10, width: responsiveWidth(94)
    },
    disabled: {
        backgroundColor: 'lightgray'
    },
    descripcion: {
        marginLeft: 5,
        marginBottom: 20
    },
    datosContainer: {
        marginVertical: 10, width: responsiveWidth(94)
    },
    derecha: {
        width: responsiveWidth(40)
    },
    description: {
        alignItems: 'center', fontWeight: '300', textAlign: 'justify', width: responsiveWidth(90)
    },
    descrip: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderColor: 'gray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(5),
        width: responsiveWidth(45)
    },
    descripC: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'gray',
        borderRadius: 6,
        borderWidth: 1,
        width: responsiveWidth(55)
    },
    desc1: {
        borderColor: 'gray', borderRadius: 6, borderWidth: 1, height: responsiveHeight(5), width: responsiveWidth(15)
    },

    desc2: {
        borderColor: 'gray', borderRadius: 6, borderWidth: 1, height: responsiveHeight(5), width: responsiveWidth(30)
    },
    dispon: {
        alignItems: 'center',
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        height: responsiveHeight(4),
        justifyContent: 'center',
        width: responsiveWidth(46.5)
    },
    dropPed: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(5),
        justifyContent: 'center',
        width: responsiveWidth(30)
    },
    dropCalif: {
        width: responsiveWidth(45.5),
        height: responsiveHeight(7),
        borderRadius: 6,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },

    // +++++ E +++++

    enteradosTitulo: {
        alignItems: 'center',
        height: 30,
        justifyContent: 'center',
        marginHorizontal: 4,
        marginTop: 5,
        width: responsiveWidth(46)
    },
    enteradoBtnInside: {
        alignItems: 'center',
        backgroundColor: 'green',
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
        marginVertical: 10,
        width: responsiveWidth(94)
    },
    enteradoBtnOutside: {
        alignItems: 'center',
        backgroundColor: 'green',
        borderRadius: 6,
        height: 25,
        justifyContent: 'center',
        marginBottom: 5,
        marginLeft: 5,
        width: responsiveWidth(25)
    },
    emergenciasInput: {
        backgroundColor: '#fff',
        borderColor: 'red',
        borderRadius: 6,
        borderWidth: 1,
        fontSize: responsiveFontSize(3),
        height: 50,
        marginTop: 5,
        padding: 10,
        textAlign: 'center',
        width: responsiveWidth(94)
    },
    emptyDate: {
        flex: 1, height: 15, paddingTop: 30
    },
    emergenciasInput_AD: {
        backgroundColor: 'red',
        borderRadius: 6,
        height: responsiveHeight(5),
        marginTop: 10,
        padding: 5,
        width: responsiveWidth(94)
    },
    emergenciasInput_AD_T: {
        backgroundColor: 'red',
        borderRadius: 6,
        height: responsiveHeight(5),
        marginTop: 10,
        padding: 5,
        width: responsiveWidth(45)
    },
    emergenciasTit_T: {
        color: 'red',
        fontSize: responsiveFontSize(2),
        fontWeight: '600',
        marginTop: 10,
        textAlign: 'center',
        width: responsiveWidth(45)
    },
    emergenciasTit: {
        color: 'red',
        fontSize: responsiveFontSize(2),
        fontWeight: '600',
        marginTop: 10,
        textAlign: 'center',
        width: responsiveWidth(94)
    },
    etiquetas: {
        fontSize: responsiveFontSize(4), fontWeight: '200'
    },
    encuadre1: {
        width: responsiveWidth(10), borderWidth: 1, borderRadius: 6, margin: 2, paddingVertical: 10
    },
    encuadre2: {
        width: responsiveWidth(51), borderWidth: 1, borderRadius: 6, margin: 2, paddingVertical: 10
    },
    encuadre3: {
        width: responsiveWidth(30),
        height: responsiveHeight(5.5),
        borderWidth: 1,
        borderRadius: 6,
        margin: 2,
        paddingVertical: 10
    },
    exaCalif: {
        borderRadius: 6,
        height: responsiveHeight(4),
        width: responsiveWidth(30),
        alignItems: 'center',
        justifyContent: 'center'
    },
    exaCalif1: {
        borderRadius: 6,
        height: responsiveHeight(6),
        width: responsiveWidth(15),
        alignItems: 'center',
        justifyContent: 'center'
    },
    Encuesta: {
        marginTop: 10, width: responsiveWidth(94), height: responsiveHeight(10), borderWidth: 1, borderRadius: 6
    },

    // +++++ F +++++

    footer: {
        fontSize: 10, fontWeight: 'normal', textAlign: 'center'
    },
    footersito: {
        fontSize: 10, fontWeight: 'normal', textAlign: 'center'
    },
    form: {
        width: responsiveWidth(94)
    },
    formT: {
        width: responsiveWidth(50)
    },
    fecha: {
        color: 'gray', fontSize: 10, marginRight: 5, marginTop: 5
    },
    first_btn: {
        backgroundColor: '#047411',
        borderRadius: 6,
        height: responsiveHeight(4),
        marginTop: 5,
        width: responsiveWidth(13),
        justifyContent: 'center'
    },
    fourth_btn: {
        backgroundColor: '#f27d06',
        borderRadius: 6,
        height: responsiveHeight(4),
        marginTop: 5,
        width: responsiveWidth(13),
        justifyContent: 'center'
    },
    fiveth_btn: {
        backgroundColor: '#C0392B',
        borderRadius: 6,
        height: responsiveHeight(4),
        marginTop: 5,
        width: responsiveWidth(13),
        justifyContent: 'center'
    },
    fontUn: {fontSize: responsiveFontSize(1.65)},
    ftoContain: {
        marginTop: 10, height: responsiveHeight(20), width: responsiveWidth(30), borderWidth: 0.5, borderRadius: 4
    },

    // +++++ G +++++

    guardarCal: {
        alignItems: 'center',
        justifyContent: 'center',
        width: responsiveWidth(50),
        height: responsiveHeight(5),
        borderRadius: 6
    },
    grande: {
        alignItems: 'center', width: responsiveWidth(68)
    },
    grups: {
        alignItems: 'center', flexDirection: 'row', padding: 6, width: responsiveWidth(47)
    },
    gradoygrupo: {
        marginTop: 5
    },
    grupoTbCord: {
        marginTop: 5, width: responsiveWidth(94), flexDirection: 'row', justifyContent: 'space-between'
    },
    gradosTbCord: {
        width: responsiveWidth(5), justifyContent: 'space-between'
    },
    gradoPago: {
        justifyContent: 'center', height: responsiveHeight(5), width: responsiveWidth(20)
    },

    // +++++ H +++++

    head: {
        flexDirection: 'row', justifyContent: 'space-between'
    },
    headerRow: {
        alignItems: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        width: responsiveWidth(90)
    },

    // +++++ I +++++

    inputPicker: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#bdbdbd',
        borderRadius: 6,
        borderWidth: 1,
        justifyContent: 'center',
        padding: 10,
        width: responsiveWidth(94)
    },
    inputPicker6: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#bdbdbd',
        borderRadius: 6,
        borderWidth: 1,
        justifyContent: 'center',
        marginTop: 10,
        padding: 10,
        width: responsiveWidth(40)
    },
    inputPicker6T: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderColor: '#bdbdbd',
        borderRadius: 6,
        borderWidth: 1,
        justifyContent: 'center',
        marginTop: 10,
        padding: 10,
        width: responsiveWidth(40)
    },
    inputValor: {
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        height: responsiveHeight(5),
        textAlign: 'center',
        textAlignVertical: 'top',
        width: responsiveWidth(10)
    },
    inputComentarios: {
        backgroundColor: '#fff',
        borderRadius: 6,
        borderWidth: 1,
        paddingHorizontal: 10,
        height: responsiveHeight(35),
        textAlignVertical: 'top',
        width: responsiveWidth(84)
    },
    invUnifText: {
        marginTop: 15, fontWeight: '700', fontSize: responsiveFontSize(2.1), alignItems: 'flex-start'
    },
    input: {
        backgroundColor: '#fff',
        borderRadius: 2,
        height: 50,
        marginHorizontal: 10,
        marginVertical: 5,
        paddingVertical: 5,
        width: responsiveWidth(94)
    },
    inputContenido: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(25),
        marginTop: 10,
        textAlignVertical: 'top',
        width: responsiveWidth(94)
    },
    inputContenidoT: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(20),
        marginTop: 10,
        textAlignVertical: 'top',
        width: responsiveWidth(45)
    },
    inputContenidoTT: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(20),
        marginTop: 10,
        textAlignVertical: 'top',
        width: responsiveWidth(43)
    },
    inputTop: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        height: 40,
        marginTop: 5,
        padding: 10,
        width: responsiveWidth(94)
    },
    image: {
        marginTop: 10, width: responsiveWidth(94)
    },
    item: {
        backgroundColor: 'white', borderRadius: 6, flex: 1, marginRight: 10, marginTop: 17, padding: 10
    },
    izquierda: {
        marginHorizontal: 20, width: responsiveWidth(40)
    },
    info: {
        color: 'black', fontSize: responsiveFontSize(2), fontWeight: '300'
    },
    infoPago: {
        color: 'white', fontSize: responsiveFontSize(5), fontWeight: '600', textAlign: 'center'
    },
    infoPagoP: {
        color: 'white', fontSize: responsiveFontSize(3), fontWeight: '600', textAlign: 'center'
    },
    infoComision: {
        color: 'white', fontSize: responsiveFontSize(2), fontWeight: '600', textAlign: 'right'
    },
    inputRow: {
        borderRadius: 6, borderWidth: 1, height: responsiveHeight(5), width: responsiveWidth(20)
    },
    inputNoEditableLargo: {
        alignItems: 'center',
        backgroundColor: '#d3d3d3',
        borderColor: '#d3d3d3',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(5),
        justifyContent: 'center'
    },
    inputNoEditableCorto: {
        alignItems: 'center',
        backgroundColor: '#d3d3d3',
        borderColor: '#d3d3d3',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(5),
        justifyContent: 'center',
        width: responsiveWidth(30)
    },
    inputNoEditableText: {
        textAlign: 'center'
    },
    inputNoEditableTitle: {
        marginBottom: 3, marginTop: 10
    },
    inputNoEditableTitleSmall: {
        height: responsiveHeight(5), marginBottom: 5, textAlign: 'center', width: responsiveWidth(30)
    },
    infoReinsc: {
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(50),
        justifyContent: 'center',
        marginTop: 7,
        paddingHorizontal: 8,
        paddingVertical: 3,
        width: responsiveWidth(94)
    },
    indicadEst: {
        width: responsiveWidth(28), justifyContent: 'space-between', alignItems: 'center'
    },
    inputDato: {
        width: responsiveWidth(94),
        borderWidth: 1,
        borderRadius: 6,
        marginTop: 5,
        height: responsiveHeight(10),
        backgroundColor: 'white'
    },
    inputDismmis: {
        textAlign: 'center', width: responsiveWidth(80)
    },
    imagen: {
        height: 45, resizeMode: 'contain', width: 45
    },
    infoRt: {
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(8.5),
        justifyContent: 'center',
        marginTop: 7,
        paddingHorizontal: 8,
        paddingVertical: 3,
        width: responsiveWidth(94)
    },

    // +++++ J +++++

    jelow: {
        fontSize: 30, fontWeight: 'bold', marginTop: 10
    },

    // +++++ L +++++

    lessPadding: {
        paddingHorizontal: 23
    },
    logo: {
        height: responsiveHeight(40), marginBottom: 5, resizeMode: 'contain', width: responsiveWidth(70)
    },
    logoContainer: {
        marginTop: 200
    },
    listItem: {
        borderRadius: 6, flexDirection: 'row'
    },
    listButtonBig: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 6,
        borderWidth: 1,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    login: {
        alignItems: 'center', height: responsiveHeight(45)
    },
    listButtonNoticia: {
        alignItems: 'center',
        borderColor: 'lightgray',
        borderRadius: 6,
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 5
    },
    listButtonSmall: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'lightgray',
        justifyContent: 'center',
        height: responsiveHeight(3.5)
    },
    listButtonSmallT: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'lightgray',
        justifyContent: 'center',
        paddingVertical: 3.3
    },
    listButtonSmall3: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'lightgray',
        justifyContent: 'center',
        paddingVertical: 8.5
    },
    listButton: {
        alignItems: 'center',
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 2
    },
    listButton1: {
        alignItems: 'center',
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 2
    },
    listButtonSelected: {
        alignItems: 'center',
        backgroundColor: secondColor,
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    lista_asuntos: {
        height: responsiveHeight(20), width: responsiveWidth(94)
    },
    listButtonSmallRight: {
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
        paddingHorizontal: 25,
        paddingVertical: 5
    },
    listButtonSmallName: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'lightgray',
        borderRadius: 6,
        justifyContent: 'center',
        paddingHorizontal: 25,
        paddingVertical: 5
    },
    left_MP: {
        alignItems: 'flex-start', height: responsiveHeight(1.8), width: responsiveWidth(85)
    },
    left_M: {
        alignItems: 'center', height: 70, width: responsiveWidth(85)
    },
    label: {
        textAlign: 'center'
    },
    li: {
        fontSize: responsiveFontSize(3.5)
    },
    listButtonNames: {
        alignItems: 'center',
        borderColor: 'lightgray',
        borderRadius: 6,
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 2,
        padding: 10
    },
    listButtonSelectedDesc: {
        alignItems: 'center',
        backgroundColor: secondColor,
        flexDirection: 'row',
        height: responsiveHeight(4),
        justifyContent: 'center',
        paddingTop: 6,
        width: responsiveWidth(33)
    },
    ladodesc: {
        flexDirection: 'row', marginTop: 1, width: responsiveWidth(55)
    },
    ladovalor: {
        flexDirection: 'row', height: responsiveHeight(5), marginTop: 1, width: responsiveWidth(30)
    },
    letra: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'gray',
        borderRadius: 6,
        borderWidth: 1,
        width: responsiveWidth(10)
    },
    letraOculta: {
        backgroundColor: 'white', borderColor: 'white', borderRadius: 6, borderWidth: 1, width: responsiveWidth(10)
    },
    listButtonBott: {
        alignItems: 'center', flexDirection: 'row', justifyContent: 'center', width: responsiveWidth(30)
    },
    listButtonSelectedCenter: {
        alignItems: 'center',
        backgroundColor: secondColor,
        flexDirection: 'row',
        height: responsiveHeight(4),
        justifyContent: 'center',
        paddingTop: 6,
        width: responsiveWidth(33)
    },
    listButtonSelectedRight: {
        alignItems: 'center',
        backgroundColor: secondColor,
        flexDirection: 'row',
        height: responsiveHeight(4),
        justifyContent: 'center',
        paddingTop: 6,
        width: responsiveWidth(33)
    },
    listButtonAsunto: {
        alignItems: 'center',
        borderColor: 'lightgray',
        borderRadius: 6,
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 15
    },
    listaCalif: {
        alignItems: 'center', justifyContent: 'center', padding: 8, width: responsiveWidth(94)
    },

    // +++++ M +++++

    main_title: {
        alignItems: 'flex-start',
        color: thirdColor,
        fontSize: responsiveFontSize(2),
        fontWeight: '600',
        marginBottom: 2,
        marginTop: 15,
        width: responsiveWidth(94)
    },
    main_title7: {
        alignItems: 'flex-start',
        color: thirdColor,
        fontSize: responsiveFontSize(1.5),
        fontWeight: '700',
        marginBottom: 2,
        paddingTop: 15,
        width: responsiveWidth(47)
    },

    main_title_3: {
        alignItems: 'flex-start',
        color: thirdColor,
        fontSize: responsiveFontSize(1.4),
        fontWeight: '700',
        marginBottom: 2,
        paddingTop: 15,
        width: responsiveWidth(94)
    },
    main_title2: {
        alignItems: 'flex-start',
        color: thirdColor,
        fontSize: responsiveFontSize(1.4),
        fontWeight: '700',
        right: 30,
        marginBottom: 2,
        paddingTop: 15,
        width: responsiveWidth(94)
    },
    modalWidth: {
        width: responsiveWidth(84)
    },

    modalText: {
        fontSize: responsiveFontSize(2.4), fontWeight: '700', margin: 5
    },
    miniRow: {
        alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', width: responsiveWidth(35)
    },
    middleButton: {
        alignItems: 'center',
        backgroundColor: secondColor,
        height: responsiveHeight(7),
        justifyContent: 'center',
        marginTop: 10,
        width: responsiveWidth(50)
    },
    main: {
        color: primaryColor, fontSize: responsiveFontSize(2.3), fontWeight: '900', marginBottom: 10
    },
    menuContainer: {
        alignItems: 'center', height: responsiveHeight(20), width: responsiveWidth(75)
    },
    menuElement: {
        marginVertical: 6
    },
    menuElementTitle: {
        color: 'white', fontSize: responsiveFontSize(2.1), fontWeight: '300'
    },
    main_titleM: {
        alignItems: 'center',
        color: primaryColor,
        fontSize: responsiveFontSize(2.5),
        fontWeight: '600',
        paddingBottom: 15,
        paddingTop: 15,
        textAlign: 'left',
        width: responsiveWidth(94)
    },
    mList: {
        borderBottomWidth: 1,
        borderColor: primaryColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 20,
        width: responsiveWidth(96)
    },
    menu: {
        flexDirection: 'column',
        marginTop: responsiveWidth(1), ...ifIphoneX({marginBottom: responsiveHeight(1)}, {marginBottom: responsiveHeight(1)}),
        width: responsiveWidth(94)
    },
    menu1: {
        flexDirection: 'column', marginTop: responsiveWidth(7), marginBottom: 12, width: responsiveWidth(85)
    },
    menu2: {
        flexDirection: 'column', marginTop: responsiveWidth(15), marginBottom: 12, width: responsiveWidth(85)
    },
    menu3: {
        flexDirection: 'column', marginTop: responsiveWidth(21), marginBottom: 12, width: responsiveWidth(85)
    },
    miniInput: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        marginTop: 5,
        padding: 12,
        width: responsiveWidth(60)
    },
    main_CG: {
        alignItems: 'center',
        color: primaryColor,
        fontSize: responsiveFontSize(2.5),
        fontWeight: '600',
        paddingTop: 15,
        textAlign: 'left',
        width: responsiveWidth(94)
    },
    mainBottom: {
        alignItems: 'center',
        color: primaryColor,
        fontSize: responsiveFontSize(2.5),
        fontWeight: '600',
        textAlign: 'left',
        width: responsiveWidth(94)
    },
    mainFirst: {
        alignItems: 'center', color: primaryColor, fontSize: responsiveFontSize(2.5), fontWeight: '600', marginTop: 10
    },
    miniRowPedN: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        width: responsiveWidth(20)
    },
    maestro_asist: {
        width: responsiveWidth(94),
        height: responsiveHeight(30),
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 6
    },
    miniRowCord: {
        alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', width: responsiveWidth(24)
    },
    materiaCord: {
        width: responsiveWidth(60),
        height: responsiveHeight(10),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 6
    },
    materiaCord22: {
        width: responsiveWidth(30),
        height: responsiveHeight(10),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 6
    },
    materiaCord2: {
        width: responsiveWidth(57), height: responsiveHeight(30), paddingHorizontal: 5, borderWidth: 1
    },
    modalWidthEsp: {
        width: responsiveWidth(84),
        alignItems: 'center',
        borderTopWidth: .25,
        borderColor: '#ababab'
    },
    modalCalif: {
        width: responsiveWidth(85),
        height: responsiveHeight(60),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        backgroundColor: 'white'
    },
    modalDAtos: {
        width: responsiveWidth(42), alignItems: 'center', marginTop: 10, justifyContent: 'center'
    },
    modalClaus: {
        width: responsiveWidth(80),
        height: responsiveHeight(70),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        backgroundColor: 'white'
    },

    // +++++ N +++++

    noEscuela: {
        color: primaryColor, fontSize: responsiveFontSize(2), fontWeight: 'normal'
    },

    num: {
        fontSize: responsiveFontSize(5), fontWeight: '600'
    },
    noticias: {
        alignItems: 'center', justifyContent: 'center', marginVertical: 10
    },
    nombres: {
        flexDirection: 'row'
    },
    noticia: {
        backgroundColor: '#f2f2f2',
        borderColor: '#f2f2f2',
        borderRadius: 6,
        borderWidth: 1,
        elevation: 2,
        flex: 2,
        flexDirection: 'column',
        marginBottom: 15,
        width: responsiveWidth(94)
    },
    notificaciones: {
        borderTopWidth: .5,
        height: 70,
        width: responsiveWidth(94),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fff'
    },
    notificacionesV2: {
        marginTop: 10,
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        height: responsiveHeight(15),
        width: responsiveWidth(94)
    },
    noEnteradoBtnInside: {
        alignItems: 'center',
        backgroundColor: 'red',
        borderRadius: 6,
        height: 50,
        justifyContent: 'center',
        marginVertical: 10,
        width: responsiveWidth(94)
    },
    noEnterado: {
        alignItems: 'center',
        backgroundColor: 'red',
        borderRadius: 6,
        height: 25,
        justifyContent: 'center',
        marginLeft: 5,
        marginVertical: 10,
        width: responsiveWidth(25)
    },
    noticia_NC: {
        borderBottomWidth: 0.5,
        borderColor: 'lightgray',
        borderTopWidth: 0.5,
        flex: 1,
        paddingTop: 14,
        width: responsiveWidth(94)
    },
    notifTitle: {
        fontSize: responsiveFontSize(1.8), fontWeight: '800'
    },
    nombresContainer: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(17),
        padding: 10,
        width: responsiveWidth(94)
    },
    nombresContainer7: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(17),
        padding: 10,
        width: responsiveWidth(45)
    },
    nombresContainer_T: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(17),
        padding: 10,
        width: responsiveWidth(44)
    },
    nombPrend: {
        alignItems: 'center',
        borderBottomWidth: 1,
        borderTopLeftRadius: 4,
        height: responsiveHeight(4),
        justifyContent: 'center',
        width: responsiveWidth(46.5)
    },
    nombre: {
        color: '#000', fontSize: responsiveFontSize(2.3)
    },

    // +++++ O +++++

    opcionsitas: {
        color: 'red',
        fontSize: responsiveFontSize(1.5),
        fontWeight: '800',
        justifyContent: 'center',
        marginTop: 10,
        textAlign: 'center',
        width: responsiveWidth(94)
    },
    opcionsitasT: {
        color: 'red',
        fontSize: responsiveFontSize(1.2),
        fontWeight: '800',
        justifyContent: 'center',
        marginTop: 10,
        textAlign: 'center',
        width: responsiveWidth(50)
    },
    onlyWBorderBtn: {
        alignItems: 'center',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(10),
        justifyContent: 'center',
        width: responsiveWidth(46)
    },

    // +++++ P +++++

    padresContainer: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(25),
        marginBottom: 20,
        marginTop: 10,
        width: responsiveWidth(94)
    },
    pocerntTbCord: {
        width: responsiveWidth(11), justifyContent: 'space-between', alignItems: 'center'
    },
    porcentNum: {
        alignItems: 'center', height: responsiveHeight(10), justifyContent: 'center', width: responsiveWidth(30)
    },
    porcentNum22: {
        alignItems: 'center', height: responsiveHeight(10), justifyContent: 'center', width: responsiveWidth(15)
    },
    porcentNum2: {
        alignItems: 'center',
        borderColor: 'black',
        borderLeftWidth: 1,
        height: responsiveHeight(4.5),
        justifyContent: 'center'
    },
    pickerParam1: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(5),
        width: responsiveWidth(30)
    },
    pickerParam2: {
        alignItems: 'center', justifyContent: 'center', height: responsiveHeight(5), width: responsiveWidth(46)
    },
    pickerPago: {
        borderRadius: 6, height: responsiveHeight(5), width: responsiveWidth(65)
    },
    pendiente: {
        alignItems: 'center', backgroundColor: 'red', borderRadius: 6, height: 30, justifyContent: 'center'
    },
    precioPrend: {
        alignItems: 'center', height: responsiveHeight(4), justifyContent: 'center', width: responsiveWidth(46.5)
    },

    // +++++ R +++++
    rowBtnGpo: {
        width: responsiveWidth(22),
        paddingHorizontal: 4,
        borderRadius: 6,
        alignItems: 'center',
        marginHorizontal: 2
    },
    rowTabla: {
        borderBottomWidth: .5, borderColor: 'white', flexDirection: 'row', width: responsiveWidth(94)
    },
    rowTablaAj: {
        borderTopWidth: 1, flexDirection: 'row', width: responsiveWidth(61)
    },
    rowTablaT: {
        borderBottomWidth: 1, borderColor: 'white', borderRadius: 6, flexDirection: 'row', width: responsiveWidth(47)
    },
    row_RT: {
        flexDirection: 'row', justifyContent: 'center', width: responsiveWidth(100)
    },
    row_M: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveWidth(2),
        width: responsiveWidth(94)
    },
    row_M1: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveWidth(1),
        width: responsiveWidth(83)
    },
    row_M3: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveWidth(1),
        width: responsiveWidth(63.5)
    },
    row_M2: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: responsiveWidth(1),
        width: responsiveWidth(41)
    },
    row_C: {
        flexDirection: 'row', paddingTop: 5, width: responsiveWidth(94)
    },
    rowPago: {
        backgroundColor: primaryColor,
        borderColor: primaryColor,
        borderRadius: 6,
        borderWidth: 1,
        flexDirection: 'column',
        marginVertical: 10,
        padding: 30,
        width: responsiveWidth(94)
    },
    row_FDP: {
        flexDirection: 'row', justifyContent: 'space-between', marginTop: responsiveWidth(2), width: responsiveWidth(90)
    },
    row: {
        alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'
    },

    row_l: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        width: responsiveWidth(47)
    },
    row11: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        width: responsiveWidth(100)
    },
    row9: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        width: responsiveWidth(80)
    },
    row8: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        width: responsiveWidth(45)
    },
    row8_T: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        width: responsiveWidth(45)
    },
    row_T: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        width: responsiveWidth(50)
    },
    row_factura: {
        alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'flex-end'
    },
    row_v2: {
        alignItems: 'center', flexDirection: 'row', paddingTop: 5, width: responsiveWidth(94)
    },
    rounded: {
        color: 'white', fontWeight: '700', textAlign: 'center'
    },
    row_PC: {
        alignItems: 'center',
        borderTopWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 0,
        paddingVertical: 10,
        width: responsiveWidth(94)
    },
    rowText: {
        alignItems: 'center', width: responsiveWidth(70)
    },
    row_PA: {
        flexDirection: 'row', justifyContent: 'space-between', marginTop: responsiveWidth(2), width: responsiveWidth(90)
    },
    row_PCA: {
        alignItems: 'center',
        borderTopWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10,
        paddingVertical: 10,
        width: responsiveWidth(94)
    },
    row_asistencia: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: responsiveWidth(94)
    },
    row_gradoGrupo: {
        justifyContent: 'space-between', marginBottom: 10, marginTop: 10, width: responsiveWidth(94)
    },
    rows_tit: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,
        width: responsiveWidth(94)
    },
    red_btn: {
        backgroundColor: 'red',
        borderRadius: 6,
        height: responsiveHeight(4),
        marginTop: 5,
        width: responsiveWidth(13),
        alignItems: 'center',
        justifyContent: 'center'
    },
    realizado: {
        alignItems: 'center', backgroundColor: primaryColor, borderRadius: 6, height: 30, justifyContent: 'center'
    },
    row_v3: {
        alignItems: 'center', flexDirection: 'row', width: responsiveWidth(94)
    },
    rowsCalif: {
        flexDirection: 'row', justifyContent: 'space-between', width: responsiveWidth(94)
    },
    rowInfo: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 0,
        marginTop: 2,
        width: responsiveWidth(94)
    },
    rowEnc: {
        flexDirection: 'row', height: responsiveHeight(5)
    },
    rowPPI: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        height: responsiveHeight(5),
        width: responsiveWidth(94)
    },
    rowEsp: {
        flexDirection: 'row',
        height: responsiveHeight(9),
        width: responsiveWidth(94),
        justifyContent: 'space-between',
        borderTopWidth: 1,
        alignItems: 'center',
        borderColor: '#ababab'
    },
    rowsPlan: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: responsiveWidth(94),
        borderBottomWidth: 1.5,
        height: responsiveHeight(5),
        alignItems: 'center',
        paddingHorizontal: 10
    },
    rowDuracion: {
        height: responsiveHeight(7),
        width: responsiveWidth(94),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    row_rT: {
        alignItems: 'center', flexDirection: 'column'
    },
    row10: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 6,
        height: responsiveHeight(15),
        width: responsiveWidth(45)
    },
    requerido: {
        alignItems: 'center', backgroundColor: 'black', borderRadius: 6, height: 30, justifyContent: 'center'
    },

    // +++++ S +++++

    smallButton: {
        alignItems: 'center',
        backgroundColor: secondColor,
        borderColor: secondColor,
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(7),
        justifyContent: 'center',
        marginTop: 10,
        width: responsiveWidth(31)
    },
    subTitle: {
        alignItems: 'center',
        color: primaryColor,
        fontSize: responsiveFontSize(1.5),
        fontWeight: '300',
        paddingBottom: 10,
        paddingTop: 10,
        textAlign: 'center',
        width: responsiveWidth(40)
    },
    sinEnterarTitulo: {
        alignItems: 'center',
        height: 30,
        justifyContent: 'center',
        marginHorizontal: 4,
        marginTop: 5,
        width: responsiveWidth(46)
    },
    subTituloMain: {
        color: 'black',
        fontSize: responsiveFontSize(2.2),
        fontWeight: '600',
        marginHorizontal: 30,
        width: responsiveWidth(60)
    },
    subTituloBottom: {
        color: 'black', fontSize: responsiveFontSize(1.8), fontWeight: '600', marginTop: 5, width: responsiveWidth(94)
    },
    selector: {
        marginVertical: 10
    },
    subTitleMain_PP: {
        fontWeight: '700', marginLeft: 10, marginRight: 5
    },
    sec_btn: {
        backgroundColor: '#08a51b',
        borderRadius: 6,
        height: responsiveHeight(4),
        marginTop: 5,
        width: responsiveWidth(13),
        justifyContent: 'center'
    },
    subTitleMain: {
        fontWeight: '400', marginBottom: 3, marginTop: 7
    },
    subTitulo: {
        color: 'black', fontSize: responsiveFontSize(1.8), fontWeight: '600', marginTop: 6, width: responsiveWidth(94)
    },
    subTituloT: {
        color: 'black', fontSize: responsiveFontSize(1.4), fontWeight: '600', marginTop: 6, width: responsiveWidth(94)
    },
    subContainer: {
        height: responsiveHeight(75), marginTop: 10, width: responsiveWidth(95)
    },
    subRow: {
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginTop: 10,
        width: responsiveWidth(32)
    },
    solRev: {
        height: responsiveHeight(30),
        width: responsiveWidth(75),
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        paddingHorizontal: 15
    },
    switch_P: {
        alignItems: 'center', flexDirection: 'row', width: responsiveWidth(25), justifyContent: 'space-between'
    },
    swipSol: {
        height: responsiveHeight(15),
        width: responsiveWidth(21),
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'lightgray',
        borderLeftWidth: 1
    },
    subtitCalif: {
        fontSize: responsiveFontSize(1.75),
        width: responsiveWidth(69),
        marginBottom: 10,
        textAlign: 'center',
        fontWeight: '800'
    },
    selectedTitulo: {
        color: 'black', fontSize: responsiveFontSize(2.5), fontWeight: '700'
    },

    // +++++ T +++++

    txtCenter: {
        textAlign: 'center'
    },
    textoB: {
        color: 'white', fontSize: responsiveFontSize(1.7)
    },

    textoN: {
        color: 'black', fontSize: responsiveFontSize(1.7)
    },

    textInput: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        ...ifIphoneX({height: responsiveHeight(6)}, {height: responsiveHeight(6)}),
        marginVertical: 3,
        ...ifIphoneX({padding: 15}, {paddingHorizontal: 15}),
        textAlign: 'center',
        alignItems:'center',
        width: responsiveWidth(94)
    },
    textInputV2: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        ...ifIphoneX({height: responsiveHeight(6)}, {height: responsiveHeight(6)}),
        marginVertical: 3,
        ...ifIphoneX({padding: 15}, {paddingHorizontal: 15}),
        width: responsiveWidth(94)
    },
    textInputV3: {
        backgroundColor: '#fff',
        borderColor: '#ccc',
        borderRadius: 6,
        borderWidth: 1,
        ...ifIphoneX({height: responsiveHeight(6)}, {height: responsiveHeight(6)}),
        marginVertical: 3,
        ...ifIphoneX({padding: 15}, {paddingHorizontal: 15, paddingTop:10}),
        textAlign: 'center',
        alignItems:'center',
        width: responsiveWidth(94)
    },
    textButton: {
        color: 'white', fontSize: responsiveFontSize(2), fontWeight: '700', textAlign: 'center'
    },

    text: {
        color: 'white', fontSize: 20
    },
    textButtonT2: {
        color: 'white', fontSize: responsiveFontSize(1.2), fontWeight: '700',

        textAlign: 'center'
    },
    title: {
        alignItems: 'center',
        color: primaryColor,
        fontSize: responsiveFontSize(2.5),
        fontWeight: '600',
        paddingBottom: 15,
        paddingTop: 15,
        textAlign: 'left',
        width: responsiveWidth(94)
    },
    tablaEnc: {
        borderLeftWidth: 1, borderTopLeftRadius: 6, borderBottomLeftRadius: 6, borderBottomWidth: 1, borderTopWidth: 1
    },
    tablaEnv: {
        borderRightWidth: 1,
        borderTopRightRadius: 6,
        borderBottomRightRadius: 6,
        borderBottomWidth: 1,
        borderTopWidth: 1
    },
    tabla: {
        borderBottomWidth: 1, borderRadius: 6, borderWidth: 1
    },
    titulosC: {
        alignItems: 'center', borderLeftWidth: 1, justifyContent: 'center', padding: 8, width: responsiveWidth(23.5)
    },
    titulo_DPA: {
        color: primaryColor, height: 20, marginTop: 5, width: responsiveWidth(94)
    },
    text_VE: {
        color: 'white', fontSize: responsiveFontSize(3), fontWeight: '700'
    },
    textEnterados: {
        color: 'green', fontSize: responsiveFontSize(2)
    },
    textNoEnterados: {
        color: 'red', fontSize: responsiveFontSize(2)
    },
    tituloBox: {
        color: 'black', fontSize: responsiveFontSize(2)
    },
    tituloBoxNombres: {
        fontSize: responsiveFontSize(2.5), paddingLeft: 13.5, paddingVertical: 5
    },
    titBtnGyG: {
        color: 'black', fontSize: responsiveFontSize(1.5), fontWeight: '600'
    },
    tituloNoticia: {
        textAlign: 'center', width: responsiveWidth(80)
    },
    tipoEscuela: {
        color: 'white', fontSize: responsiveFontSize(2.25), fontWeight: '500', marginTop: 10, textAlign: 'center'
    },
    title1: {
        color: primaryColor,
        fontSize: responsiveFontSize(2.5),
        fontWeight: '600',
        paddingBottom: 15,
        paddingTop: 15,
        textAlign: 'center',
        width: responsiveWidth(94)
    },
    titBtn: {
        fontSize: responsiveFontSize(1.7), fontWeight: '400', marginBottom: 5, textAlign: 'center'
    },
    titBtnT: {
        fontSize: responsiveFontSize(1.3), fontWeight: '400', marginBottom: 5, textAlign: 'center'
    },
    titArriba: {
        fontSize: responsiveFontSize(1.5), fontWeight: '600'
    },
    titArribaSelected: {
        color: '#fff', fontSize: responsiveFontSize(1.5), fontWeight: '600'
    },
    titulo_noticia: {
        color: primaryColor,
        fontSize: responsiveFontSize(2.5),
        fontWeight: '700',
        marginBottom: 0,
        marginLeft: 5,
        marginTop: 5,
        width: responsiveWidth(73)
    },
    textEnterado: {
        color: 'white', fontSize: responsiveFontSize(1.6), fontWeight: '500', textAlign: 'center'
    },
    titulo_N: {
        height: 20, marginBottom: 15, marginTop: 5, width: responsiveWidth(94)
    },
    text_N: {
        color: '#13587a'
    },
    textInfo: {
        fontSize: responsiveFontSize(1.5)
    },
    theButtons: {
        flexDirection: 'row', width: responsiveWidth(94)
    },
    titBtn_GCM: {
        color: 'white', fontSize: responsiveFontSize(1.5), fontWeight: '600'
    },
    titBtn_DPA: {
        color: 'white', fontSize: responsiveFontSize(2.5), fontWeight: '600'
    },
    titulo: {
        color: primaryColor,
        fontSize: responsiveFontSize(2.5),
        fontWeight: '700',
        marginVertical: 10,
        width: responsiveWidth(94)
    },
    title_CC: {
        alignItems: 'center', fontWeight: '800', justifyContent: 'center'
    },
    text_CC: {
        alignItems: 'center',
        fontSize: responsiveFontSize(1.75),
        fontWeight: '600',
        justifyContent: 'center',
        width: responsiveWidth(94)
    },
    titBtnE: {
        color: '#DE0000', fontSize: responsiveFontSize(2), fontWeight: '600', textAlign: 'center'
    },
    titBtnE_2: {
        color: '#DE0000', fontSize: responsiveFontSize(0.8), fontWeight: '600', textAlign: 'center'
    },
    titBtnA: {
        color: '#EE6500', fontSize: responsiveFontSize(2), fontWeight: '600', textAlign: 'center'
    },
    titBtnA_2: {
        color: '#EE6500', fontSize: responsiveFontSize(0.8), fontWeight: '600', textAlign: 'center'
    },
    tituloData: {
        color: 'black',
        fontSize: responsiveFontSize(1.8),
        fontWeight: '600',
        marginBottom: 5,
        marginTop: 10,
        width: responsiveWidth(94)
    },
    tituloData_T: {
        color: 'black',
        fontSize: responsiveFontSize(1.4),
        fontWeight: '600',
        marginBottom: 5,
        marginTop: 10,
        width: responsiveWidth(94)
    },
    tituloEmergencias: {
        color: 'white', fontSize: responsiveFontSize(2.75), textAlign: 'center'
    },
    textData: {
        fontSize: responsiveFontSize(1.7), fontWeight: '200', marginVertical: 2
    },
    textData_T: {
        fontSize: responsiveFontSize(1.3), fontWeight: '200', marginVertical: 2
    },
    tituloPago: {
        color: 'white', fontSize: responsiveFontSize(2), textAlign: 'left'
    },
    titulo_RT: {
        color: 'black', fontSize: responsiveFontSize(3), fontWeight: '600', marginVertical: 10
    },
    titulo_PV: {
        height: 20, marginBottom: 15, marginTop: 5, width: responsiveWidth(94)
    },
    textParam: {
        width: responsiveWidth(70), fontSize: responsiveFontSize(2.25), fontWeight: '700'
    },
    titleLong: {
        alignItems: 'center',
        color: primaryColor,
        fontSize: responsiveFontSize(2),
        fontWeight: '600',
        paddingBottom: 10,
        paddingTop: 15,
        textAlign: 'left',
        width: responsiveWidth(94)
    },
    txt_btn: {
        color: '#4f8ff7', paddingVertical: 4.5, textAlign: 'center'
    },
    third_btn: {
        backgroundColor: '#ece31c',
        borderRadius: 6,
        height: responsiveHeight(4),
        marginTop: 5,
        width: responsiveWidth(13),
        justifyContent: 'center'
    },
    txt_botons: {
        color: 'white', textAlign: 'center'
    },
    title2: {
        alignItems: 'center',
        color: primaryColor,
        fontSize: responsiveFontSize(1.8),
        fontWeight: '700',
        paddingBottom: 15,
        paddingTop: 15,
        textAlign: 'left',
        width: responsiveWidth(95)
    },
    titulosContainer: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(20),
        padding: 5,
        width: responsiveWidth(94)
    },
    titulosContainer_2: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(20),
        padding: 5,
        width: responsiveWidth(42)
    },
    titulosContainer_2T: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(20),
        padding: 5,
        width: responsiveWidth(40)
    },
    titulosContainer_3: {
        borderColor: 'lightgray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(20),
        padding: 5,
        width: responsiveWidth(45)
    },
    textCenter: {
        textAlign: 'center'
    },
    titlePPI: {
        color: thirdColor, fontSize: responsiveFontSize(2.4), fontWeight: '700', marginRight: 3
    },
    truContain: {
        borderRadius: 6, borderWidth: 1
    },
    textGrupo: {
        fontWeight: '500', textAlign: 'center'
    },
    titleClausula: {
        fontSize: responsiveFontSize(1.75), width: responsiveWidth(69), textAlign: 'center', fontWeight: '800'
    },
    textClaus: {
        fontSize: responsiveFontSize(2.2),
        width: responsiveWidth(65),
        height: responsiveHeight(45),
        marginTop: 15,
        textAlign: 'center',
        fontWeight: '300'
    },
    textGris: {
        fontSize: responsiveFontSize(1), color: 'grey'
    },
    textPago: {
        fontWeight: '700', fontSize: responsiveFontSize(1.6), marginTop: 1
    },

    tallaPrend: {
        alignItems: 'center', borderBottomWidth: 1, height: responsiveHeight(10), width: responsiveWidth(46.5)
    },
    textW: {fontWeight: '700'},
    triangleCorner: {
        position: 'absolute',
        width: responsiveWidth(94),
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: responsiveWidth(93),
        borderTopWidth: responsiveHeight(309),
        borderRightColor: 'transparent',
        borderRadius: 6
    },
    triangleCorner1: {
        position: 'absolute',
        width: responsiveWidth(20),
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: responsiveWidth(33),
        borderTopWidth: responsiveHeight(103),
        borderRightColor: 'transparent',
        borderRadius: 6
    },
    triangleCornerLayer: {
        position: 'absolute',
        width: responsiveWidth(80),
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: responsiveWidth(92),
        borderTopWidth: responsiveHeight(308),
        borderRightColor: 'transparent',
        borderRadius: 6
    },
    cuadroArriba: {
        position: 'absolute', top: 0, backgroundColor: 'transparent'
    },

    // +++++ U +++++

    user: {
        color: 'white',
        fontSize: responsiveFontSize(1.7),
        fontWeight: '700',
        marginTop: 10,
        textAlign: 'center',
        width: responsiveWidth(30)
    },

    // +++++ V +++++

    ver_lista: {
        alignItems: 'center',
        borderColor: 'gray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(4),
        width: responsiveWidth(30),
        justifyContent: 'center'
    },
    ver_listaT: {
        alignItems: 'center',
        borderColor: 'gray',
        borderRadius: 6,
        borderWidth: 1,
        height: responsiveHeight(4),
        width: responsiveWidth(15),
        justifyContent: 'center'
    },
    valor: {
        backgroundColor: 'white', borderColor: 'gray', borderRadius: 6, borderWidth: 1, width: responsiveWidth(30)
    },
    viewStepper: {
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
    },
    varDato1: {
        width: responsiveWidth(44), alignItems: 'flex-end', borderBottomWidth: 0.5
    },
    varDato2: {
        width: responsiveWidth(40), alignItems: 'flex-start', borderBottomWidth: 0.5, justifyContent: 'center'
    },

    // +++++ W +++++

    welcome: {
        color: 'white', fontSize: responsiveFontSize(2.5), fontWeight: '200', paddingTop: 30
    },
    widthall: {width: responsiveWidth(94)},
    widthParm: {width: responsiveWidth(94), marginVertical: 8},
    //+-+-+-+-+-+-+-+-+--+-+-new style
    mediumBtn: {
        borderWidth: .5,
        borderRadius: 6,
        width: responsiveWidth(46),
        height: responsiveHeight(7),
        alignItems: 'center',
        justifyContent: 'center',
        padding: 6
    },
    modalBtnMed: {
        borderWidth: .5,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        width: responsiveWidth(41),
        height: responsiveHeight(7),
        padding: 6
    },
    bigBtn: {
        borderWidth: .5,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        width: responsiveWidth(94),
        height: responsiveHeight(7),
        padding: 6
    },
    modalBigBtn: {
        borderWidth: 2,
        borderRadius: 6,
        width: responsiveWidth(41),
        height: responsiveHeight(5),
        ...ifIphoneX({marginVertical: 8}, {marginVertical: 20}),
        alignItems: 'center',
        justifyContent: 'center',
        padding: 6
    }
});
