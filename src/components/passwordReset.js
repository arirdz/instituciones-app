import React from 'react';
import {
   Alert, AsyncStorage, Image, Keyboard, KeyboardAvoidingView, Platform, StatusBar, Text, TextInput, TouchableOpacity,
   TouchableWithoutFeedback, View
} from 'react-native';
import {responsiveHeight} from 'react-native-responsive-dimensions';
import {Actions} from 'react-native-router-flux';
import styles from './styles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

let moment = require('moment');
require('moment/locale/es');
moment.locale('es');

export default class PwdRest extends React.Component {
   handlePassword = text => {
	  this.setState({password: text});
   };
   handlePasswordconfirmation = text => {
	  this.setState({Passwordconfirmation: text});
   };

   constructor(props) {
	  super(props);
	  this.state = {
		 password: '', Passwordconfirmation: '', uri: '', escuelasDatos: []
	  };

	  const widthByPercent = (percentage, containerWidth = wWidth) => {
		 const value = percentage * containerWidth / 100;
		 return Math.round(value);
	  };
	  const regex = {
		 digitsPattern: /\d/,
		 lettersPattern: /[a-zA-Z]/,
		 lowerCasePattern: /[a-z]/,
		 upperCasePattern: /[A-Z]/,
		 wordsPattern: /\w/,
		 symbolsPattern: /\W/
	  };
   }

   async componentWillMount() {
	  await this.getURL();
	  await this.getSchoolData();
   }

   async getSchoolData() {
	  let scid = await AsyncStorage.getItem('schoolId');
	  let schoolData = await fetch('https://controlescolar.pro/api/get/escuelas/datos/' + scid);
	  scData = await schoolData.json();
	  this.setState({escuelasDatos: scData[0]});
   }

   async getURL() {
	  this.setState({
		 uri: await AsyncStorage.getItem('uri'),
		 token: await AsyncStorage.getItem('token'),
		 mainColor: await AsyncStorage.getItem('mainColor'),
		 secondColor: await AsyncStorage.getItem('secondColor'),
		 thirdColor: await AsyncStorage.getItem('thirdColor'),
		 fourthColor: await AsyncStorage.getItem('fourthColor'),
		 role: await AsyncStorage.getItem('role')
	  });
   }

   render() {
	  return (<KeyboardAvoidingView behavior='position' style={styles.container}>
		 <StatusBar
		   backgroundColor={this.state.mainColor}
		   barStyle='light-content'/>
		 <View
		   style={{
			  alignItems: 'center', marginTop: responsiveHeight(4.5)
		   }}
		 >
			<Image
			  style={styles.logo}
			  source={{
				 uri: 'https://controlescolar.pro/images/' + this.state.escuelasDatos.nombre_corto + '.png'
			  }}
			/>
		 </View>
		 <View style={styles.login}>
			<Text style={[styles.jelow, {color: this.state.mainColor}]}>Cambio de contraseña</Text>
			{Platform.OS === 'ios' ? (
			  <View style={[styles.textInputV2, styles.row, {borderColor: this.state.secondColor}]}>
				 <TextInput
				   ref='email'
				   placeholder='Contraseña nueva'
				   secureTextEntry={true}
				   returnKeyType='next'
				   underlineColorAndroid='transparent'
				   autoCapitalize='none'
				   onChangeText={this.handlePassword}
				   autoCorrect={false}
				   style={styles.inputDismmis}
				   onSubmitEditing={() => this.refs.password.focus()}
				 />
				 <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
					<MaterialCommunityIcons color={'grey'} name='keyboard-close' size={25}/>
				 </TouchableWithoutFeedback>

			  </View>) : (<TextInput
			  ref='email'
			  placeholder='Contraseña nueva'
			  secureTextEntry={true}
			  returnKeyType='next'
			  underlineColorAndroid='transparent'
			  autoCapitalize='none'
			  onChangeText={this.handlePassword}
			  autoCorrect={false}
			  style={[styles.textInput, {borderColor: this.state.secondColor}]}
			  onSubmitEditing={() => this.refs.password.focus()}
			/>)}
			<TextInput
			  ref='password'
			  placeholder='Confirmar contraseña'
			  secureTextEntry={true}
			  underlineColorAndroid='transparent'
			  onChangeText={this.handlePasswordconfirmation}
			  returnKeyType='go'
			  autoCapitalize='none'
			  style={[styles.textInput, {borderColor: this.state.secondColor}]}
			  onSubmitEditing={() => this.requestMultipart(this.state.password, this.state.Passwordconfirmation)}
			/>
			<TouchableOpacity
			  onPress={() => this.requestMultipart(this.state.password, this.state.Passwordconfirmation)}
			  style={[styles.bigButtonLogin, {
				 backgroundColor: this.state.mainColor, borderColor: this.state.mainColor
			  }]}>
			   <Text style={styles.textButton}>Cambiar contraseña</Text>
			</TouchableOpacity>
		 </View>
		 <View style={{height: responsiveHeight(10)}}>
			<Text style={styles.footer}>Todos los derechos reservados.</Text>
			<Text style={styles.footersito}>
			   Control Escolar Profesional &copy;2017 - {moment().year()}
			</Text>
		 </View>
	  </KeyboardAvoidingView>);
   }

   async requestMultipart(password, Passwordconfirmation) {
	  let token = await AsyncStorage.getItem('token');
	  if (password !== '' && Passwordconfirmation !== '') {
		 if (password === Passwordconfirmation) {
			if (password.length >= 6) {
			   try {
				  let formData = new FormData();
				  formData.append('password', JSON.stringify({password: this.state.password}));
				  let request = await fetch(this.state.uri + '/api/password/update/first/login', {
					 method: 'POST', headers: {
						Accept: 'application/json',
						'Content-Type': 'multipart/form-data',
						Authorization: 'Bearer ' + token
					 }, body: formData
				  });
				  Alert.alert('Contraseña actualizada', 'Se ha cambiado la contraseña con éxito', [{
					 text: 'Entendido', onPress: () => this.state.role === 'Padre' ? Actions.reset('drawer') : Actions.reset('drawer')
				  }]);
			   } catch (e) {
				  Alert.alert('Error en el cambio de contraseña', 'Intenta más tarde por favor', [{
					 text: 'Entendido'
				  }]);
			   }
			} else {
			   Alert.alert('Contraseña invalida', 'La contaseña es muy corta', [{
				  text: 'Entendido'
			   }]);
			}
		 } else {
			Alert.alert('Contraseñas no coinciden', 'Las contraseñas no coinciden', [{
			   text: 'Entendido'
			}]);
		 }
	  } else {
		 Alert.alert('Datos incompletos', 'Asegúrate de llenar los dos campos', [{
			text: 'Entendido'
		 }]);
	  }
   }
}