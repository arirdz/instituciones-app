import {AsyncStorage, Platform, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Actions} from 'react-native-router-flux';
import {MediaQuery} from 'react-native-responsive';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {responsiveFontSize, responsiveHeight} from 'react-native-responsive-dimensions';

const styles = StyleSheet.create({
    container: {
        ...ifIphoneX({
            paddingTop: Platform.OS === 'ios' ? responsiveHeight(2.5) : 4
        }, {
            paddingTop: Platform.OS === 'ios' ? 4 : 0
        })
    }, navTitle: {
        color: '#e5e4da', height: Platform.OS === 'ios' ? responsiveHeight(5) : responsiveHeight(5), ...ifIphoneX({
            marginTop: Platform.OS === 'ios' ? 10 : 0
        }, {
            marginTop: Platform.OS === 'ios' ? 6 : 0
        }), fontSize: responsiveFontSize(2.7), fontWeight: '800', textAlign: 'left'
    }
});

export default class NavBarDos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            escuelasDatos: []
        };
    }

    async getSchoolData() {
        let scid = await AsyncStorage.getItem('schoolId');
        let schoolData = await fetch('https://controlescolar.pro/api/get/escuelas/datos/' + scid);
        let scData = await schoolData.json();
        this.setState({escuelasDatos: scData[0]});
    }

    async componentWillMount() {
        await this.getSchoolData();
        await AsyncStorage.getItem('mainColor').then(mainColor => {
            this.setState({mainColor: mainColor});
        });
        await AsyncStorage.getItem('secondColor').then(secondColor => {
            this.setState({secondColor: secondColor});
        });
        await AsyncStorage.getItem('thirdColor').then(thirdColor => {
            this.setState({thirdColor: thirdColor});
        });
        await AsyncStorage.getItem('fourthColor').then(fourthColor => {
            this.setState({fourthColor: fourthColor});
        });
    }

    _renderMiddle() {
        return (<View>
            <MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
                <View>
                    <View style={[styles.container, {
                        ...ifIphoneX({paddingTop: 17}, {paddingTop: Platform.OS === 'ios' ? 15 : 5})
                    }]}>
                        <Text
                            numberOfLines={1}
                            style={[styles.navTitle, {
                                color: '#F2F2F2', textAlign: 'center'
                            }]}>
                            {this.props.title}
                        </Text>
                    </View>
                </View>
            </MediaQuery>
            <MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
                <View>
                    <View style={[styles.container, {
                        ...ifIphoneX({paddingTop: 17}, {paddingTop: Platform.OS === 'ios' ? 22 : 5})
                    }]}>
                        <Text
                            numberOfLines={1}
                            style={[styles.navTitle, {
                                color: '#F2F2F2', textAlign: 'center'
                            }]}>
                            {this.props.title}
                        </Text>
                    </View>
                </View>
            </MediaQuery>

            <MediaQuery maxDeviceWidth={1024} minDeviceHeight={897}>
                <View>
                    <View style={[styles.container, {paddingTop: 35}]}>
                        <Text
                            numberOfLines={1}
                            style={[styles.navTitle, {
                                color: this.state.fourthColor
                            }]}>
                            {this.props.title}
                        </Text>
                    </View>
                </View>
            </MediaQuery>
        </View>);
    }

    render() {
        let dinamicStyle = {};
        if (Actions.currentScene === 'customNavBar') {
            dinamicStyle = {
                backgroundColor: '#116876', ...ifIphoneX({
                    height: responsiveHeight(10)
                }, {height: responsiveHeight(10)})
            };
        } else {
            dinamicStyle = {
                backgroundColor: '#116876', ...ifIphoneX({
                    height: responsiveHeight(10)
                }, {height: Platform.OS === 'ios' ? responsiveHeight(10) : responsiveHeight(6)})
            };
        }

        return (<View style={[styles.container, dinamicStyle]}>
            {this._renderMiddle()}
        </View>);
    }
}
