import React from 'react';
import {Alert, Image, Platform, StatusBar, Text, TouchableOpacity, View, KeyboardAvoidingView} from 'react-native';
import styles from '../styles';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {MediaQuery} from 'react-native-responsive';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {Actions} from "react-native-router-flux";
import Fumi from "react-native-textinput-effects/lib/Fumi";
import Ionicons from "react-native-vector-icons/Ionicons";
import Spinner from "react-native-loading-spinner-overlay";


export default class infoCE extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            escuelasDatos: [],
            uri: 'https://controlescolar.pro',
            mainColor: '#263238',
            secondColor: '#116876',
            thirdColor: '#75745A',
            fourthColor: '#E6E6E6',
            visible: false,
        }
    }

    async componentWillMount() {

    }

    async requestMultipart() {
        if (this.state.cct === '') {
            Alert.alert('CCT/RVOE vacío', 'Por favor ingrese el CCT para poder solicitar el registro de su escuela', [{text: 'Enterado'}]);
        } else if (this.state.cct.length !== 10) {
            Alert.alert('CCT/RVOE inválido', 'Debe contener 10 caracteres', [{text: 'Enterado'}]);
        } else {
            await this.setState({visible: true})
            await fetch('http://127.0.0.1:8000/api/get/escuela/' + this.state.cct, {
                method: 'GET', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                }
            }).then(res => res.json())
                .then(responseJson => {
                    if (responseJson.error !== undefined) {
                        this.setState({visible: false})
                        console.log(responseJson)
                    } else {
                        console.log(responseJson)
                        if (responseJson.length === 0) {
                            Alert.alert('Error', 'El CCT/RVOE es incorrecto, por favor verifíquelo ', [{
                                text: 'Enterado',
                                onPress: () => this.setState({visible: false})
                            }]);
                        } else {
                            if (responseJson[0] === 'Registrado') {
                                Alert.alert('Atención', 'Ya existe una solicitud con este CCT', [{
                                    text: 'OK',
                                    onPress: () => [this.setState({visible: false})]
                                }]);
                            } else {
                                this.setState({escuela: responseJson})
                                if (this.state.escuela[0].nombre_control === 'PÚBLICO') {
                                    Alert.alert('Felicidades', '¡Hemos encontrado su escuela!', [{
                                        text: 'OK',
                                        onPress: () => [this.setState({visible: false}), Actions.InformacionPublicas({laEscuela: this.state.escuela[0]})]
                                    }]);
                                } else if (this.state.escuela[0].nombre_control === 'PRIVADO') {
                                    Alert.alert('Felicidades', '¡Hemos encontrado su escuela!', [{
                                        text: 'OK',
                                        onPress: () => [this.setState({visible: false}), Actions.InformacionPrivadas({laEscuela: this.state.escuela[0]})]
                                    }]);
                                }
                            }
                        }
                    }
                });
        }
    }


    render() {
        return (
            <KeyboardAvoidingView
                keyboardVerticalOffset={200}
                behavior={'padding'}
                style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.secondColor}
                    barStyle={'light-content'}
                />
                <MediaQuery minDeviceHeight={50} maxDeviceHeight={500}>
                    <View
                        style={{
                            width: responsiveWidth(100), ...ifIphoneX({
                                height: responsiveHeight(80)
                            }, {
                                height: Platform.OS === 'ios' ? responsiveHeight(85) : responsiveHeight(85)
                            }),
                            flexDirection: 'column',
                            backgroundColor: 'white',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <View
                            style={{
                                width: responsiveWidth(94),
                                height: responsiveHeight(40),
                                alignItems: 'center',
                                justifyContent: 'center',
                                shadowColor: 'lightgrey',
                            }}>
                        </View>
                        <View
                            style={{
                                width: responsiveWidth(94),
                                alignItems: 'center',
                                justifyContent: 'center',
                                margin: 10,
                                marginTop: 20
                            }}>
                            {this.state.hasToken ?
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        color: this.state.secondColor,
                                        backgroundColor: 'transparent',
                                        fontWeight: '900',
                                        fontSize: responsiveFontSize(4)
                                    }}>
                                    Recomiendanos
                                </Text> :
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        color: this.state.secondColor,
                                        backgroundColor: 'transparent',
                                        fontWeight: '900',
                                        fontSize: responsiveFontSize(4)
                                    }}>
                                    ¡Quiero ser Liceo!
                                </Text>}
                        </View>
                        <View style={{
                            width: responsiveWidth(94),
                            height: responsiveHeight(10),
                            alignItems: 'center',
                            justifyContent: 'center',
                            margin: 10
                        }}>
                            <Text style={{textAlign: 'justify'}}>
                                Ahora podrás realizar, desde nuestra App y de una manera muy sencilla, el proceso de
                                <Text style={{fontWeight: '800'}}>{' '}preinscripción</Text> o <Text
                                style={{fontWeight: '800'}}>{' '}transferencia</Text> a nuestra escuela en un solo tap:
                            </Text>
                        </View>
                        <TouchableOpacity
                            // onPress={() => this.functionAction()}
                            style={[
                                styles.bigButton,
                                {
                                    backgroundColor: this.state.secondColor,
                                    borderRadius: 8,
                                    width: responsiveWidth(94),
                                    margin: 20,
                                    shadowColor: 'lightgrey',
                                    shadowOpacity: 1,
                                    shadowOffset: {width: 5, height: 5}
                                }
                            ]}
                        >
                            <Text style={styles.textButton}>Conocer proceso de admisión</Text>
                        </TouchableOpacity>
                    </View>
                </MediaQuery>
                <MediaQuery minDeviceHeight={500} maxDeviceHeight={896}>
                    <View
                        style={{
                            // width: responsiveWidth(100), ...ifIphoneX({
                            // 	height: responsiveHeight(80)
                            // }, {
                            // 	height: Platform.OS === 'ios' ? responsiveHeight(85) : responsiveHeight(85)
                            // }),
                            flexDirection: 'column',
                            backgroundColor: 'white',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <Spinner visible={this.state.visible} textContent='Buscando escuela, espero por favor...'/>
                        <View
                            style={{
                                width: responsiveWidth(100),
                                height: responsiveHeight(30),
                                marginBottom: 20,
                                alignItems: 'center',
                                justifyContent: 'center',
                                shadowColor: 'lightgrey',
                            }}>
                            <Image
                                style={{
                                    flex: 1, width: responsiveWidth(100), borderRadius: 15, resizeMode: 'cover'

                                }}
                                source={require('../../images/background.jpg')}
                            />
                        </View>
                        <View
                            style={{
                                width: responsiveWidth(94),
                                alignItems: 'center',
                                justifyContent: 'center',
                                margin: 10,
                                marginTop: 0
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    color: this.state.secondColor,
                                    backgroundColor: 'transparent',
                                    fontWeight: '900',
                                    fontSize: responsiveFontSize(4)
                                }}>
                                ¡Quiero registrar mi escuela!
                            </Text>
                        </View>
                        <View style={{
                            width: responsiveWidth(94),
                            alignItems: 'center',
                            paddingHorizontal: 10,
                            justifyContent: 'center',
                            margin: 10
                        }}>
                            <Text style={{textAlign: 'justify'}}>
                                Con nuestra app, mejorará aún más la imagen que su escuela proyecta en el mercado,
                                dándole un valor agregado único. Para iniciar, indíquenos cuál es el CCT o RVOE de su
                                escuela:
                            </Text>
                        </View>
                        <Text style={[styles.main_title, {color: this.state.thirdColor}]}>
                            ¿Cuál es el CCT/RVOE de la escuela?
                        </Text>
                        <Fumi
                            label={'CCT/RVOE'}
                            iconClass={Ionicons}
                            style={[styles.inputDato, {borderColor: this.state.secondColor}]}
                            labelStyle={{color: this.state.mainColor}}
                            iconName={'md-person'}
                            inputStyle={{color: this.state.secondColor}}
                            iconColor={this.state.mainColor}
                            iconSize={20}
                            onChangeText={text => (this.state.cct = text)}
                            keyboardType='default'
                            value={this.state.cct}
                            returnKeyType='next'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                        <TouchableOpacity
                            onPress={() => this.requestMultipart()}
                            style={[
                                styles.bigButton,
                                {
                                    backgroundColor: this.state.secondColor,
                                    borderRadius: 8,
                                    width: responsiveWidth(94),
                                    margin: 20,
                                    shadowColor: 'lightgrey',
                                    shadowOpacity: 1,
                                    shadowOffset: {width: 5, height: 5}
                                }
                            ]}
                        >
                            <Text style={styles.textButton}>Enviar</Text>
                        </TouchableOpacity>
                    </View>
                </MediaQuery>
            </KeyboardAvoidingView>
        );
    }
}
