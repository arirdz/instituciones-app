import React from 'react';
import {
	Alert, AsyncStorage, BackHandler, Image, ImageBackground, Linking, Platform, ScrollView, StatusBar, Text, View
} from 'react-native';
import styles from '../styles';
import OneSignal from 'react-native-onesignal';
import {Actions} from 'react-native-router-flux';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {TouchableOpacity} from '../Mobile/Preinscripcion/subirDoctos';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default class HomeCE extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uri: 'https://controlescolar.pro',
			mainColor: '#263238',
			secondColor: '#116876',
			thirdColor: '#75745A',
			fourthColor: '#E6E6E6'
		};
	}

	async click() {
	}

	async componentWillMount() {
		await this.getURL();
		OneSignal.init('02510db7-3e82-4308-a15d-f81486901635');
		OneSignal.addEventListener('received', this.onReceived);
		OneSignal.addEventListener('opened', this.onOpened);
		OneSignal.addEventListener('ids', this.onIds);
		Linking.addEventListener('url', this.handleOpenURL);
		OneSignal.inFocusDisplaying(2);
		OneSignal.configure();
	}

	componentWillUnmount() {
		Linking.removeEventListener('url', this.handleOpenURL);
		OneSignal.removeEventListener('received', this.onReceived);
		OneSignal.removeEventListener('opened', this.onOpened);
		OneSignal.removeEventListener('ids', this.onIds);
		BackHandler.removeEventListener('hardwareBackPress');
	}

	async getURL() {
		await this.setState({
			uri: 'https://controlescolar.pro',
			mainColor: '#263238',
			secondColor: '#116876',
			thirdColor: '#75745A',
			fourthColor: '#E6E6E6',
		});
		await AsyncStorage.setItem('mainColor', '#263238');
		await AsyncStorage.setItem('secondColor', '#116876');
		await AsyncStorage.setItem('thirdColor', '#75745A');
		await AsyncStorage.setItem('fourthColor', '#E6E6E6');
	}

	async onOpened(openResult) {
		let tutor = await AsyncStorage.getItem('idTutor');
		let id_school = openResult.notification.payload.additionalData.id_escuela;
		if (openResult.notification.payload.launchURL === 'ce://aspirantesTutor') {
			try {
				if (tutor !== null) {
					Actions.push('aspirantesTutor', {idschool: id_school});
				} else {
					Actions.push('loginPreinscrip', {idschool: id_school});
				}
			} catch (error) {
				console.error('AsyncStorage error: ' + error.message);
			}
		}
	}

	async blogCard() {
		Alert.alert('Función no disponible por el momento', '');
	}

	render() {
		let p = Platform.OS === 'ios';
		let heightActa = [{...ifIphoneX({height: responsiveHeight(19)}, {height: p ? responsiveHeight(20) : responsiveHeight(21)})}];
		return (<ImageBackground
			style={styles.container}
			imageStyle={{resizeMode: 'stretch'}}
			source={{uri: 'https://controlescolar.pro/images/fondos/fondologin.jpg'}}
		>
			<StatusBar
				backgroundColor={this.state.secondColor}
				barStyle={'light-content'}
			/>
			<ScrollView
				style={{marginVertical: 15}}
				overScrollMode='always'
				showsVerticalScrollIndicator={false}
			>
				<View
					style={[
						styles.widthall,
						styles.row,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 6,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<Image
						style={[heightActa, {
							width: responsiveWidth(30),
							resizeMode: 'cover'
						}]}
						imageStyle={{borderRadius: 10}}
						source={require('../../images/tecEduc1.jpg')}
					/>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{
									color: this.state.thirdColor,
									width: responsiveWidth(55),
									textAlign: 'left',
									marginTop: 5,
									lineHeight: 15
								}
							]}
						>
							Tecnología aplicada a la educación
						</Text>
						<Text
							style={{
								fontSize: responsiveFontSize(1.5),
								textAlign: 'left',
								lineHeight: 11.8,
								marginTop: 3
							}}
						>
							En los últimos años, la gestión escolar ha evolucionado a la par de la tecnología. ¿Te
							interesa
							saber cómo puedes aprovechar la tecnología en tu escuela?
						</Text>
						<View style={{
							borderWidth: .5,
							width: responsiveWidth(55),
							marginVertical: 7,
							borderColor: 'grey'
						}}/>
						<Text
							style={{width: responsiveWidth(55), textAlign: 'center'}}
							onPress={() => this.blogCard()}
						>
							Leer artículo completo
						</Text>
					</View>
				</View>
				<View
					style={[
						styles.widthall,
						styles.row,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 6,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<Image
						style={[heightActa, {
							width: responsiveWidth(30),
							resizeMode: 'cover'
						}]}
						imageStyle={{borderRadius: 10}}
						source={require('../../images/InovEduc1.jpg')}
					/>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{
									color: this.state.thirdColor,
									width: responsiveWidth(55),
									textAlign: 'left',
									marginTop: 5,
									lineHeight: 15
								}
							]}
						>
							La innovación en materia educativa
						</Text>
						<Text style={{
							fontSize: responsiveFontSize(1.5),
							textAlign: 'left',
							lineHeight: 11.8,
							marginTop: 3
						}}>
							Existe una frase famosa que dice: 'Reinventarse o morir'. Para una escuela, la innovación
							resulta indispensable para mejorar el nivel educativo y ofrecer un servicio de calidad
							continuo.
						</Text>
						<View style={{
							borderWidth: .5,
							width: responsiveWidth(55),
							marginVertical: 7,
							borderColor: 'grey'
						}}/>
						<Text
							style={{width: responsiveWidth(55), textAlign: 'center'}}
							onPress={() => this.blogCard()}
						>
							Leer artículo completo
						</Text>
					</View>
				</View>
				<View
					style={[
						styles.widthall,
						styles.row,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 6,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<Image
						style={[heightActa, {
							width: responsiveWidth(30),
							resizeMode: 'cover'
						}]}
						imageStyle={{borderRadius: 10}}
						source={require('../../images/EficOrg1.jpg')}
					/>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{
									color: this.state.thirdColor,
									width: responsiveWidth(55),
									textAlign: 'left',
									marginTop: 5,
									lineHeight: 15
								}
							]}
						>
							La eficiencia en la administración escolar
						</Text>
						<Text style={{
							fontSize: responsiveFontSize(1.5),
							textAlign: 'left',
							lineHeight: 11.8,
							marginTop: 3
						}}>
							Una escuela eficiente es una escuela que destaca por sus resultados. Disminuir el tiempo que
							el personal realiza en sus actividades crea espacios para crecer e innovar.
						</Text>
						<View style={{
							borderWidth: .5,
							width: responsiveWidth(55),
							marginVertical: 7,
							borderColor: 'grey'
						}}/>
						<Text
							style={{width: responsiveWidth(55), textAlign: 'center'}}
							onPress={() => this.blogCard()}
						>
							Leer artículo completo
						</Text>
					</View>
				</View>
				<View
					style={[
						styles.widthall,
						styles.row,
						{
							backgroundColor: this.state.fourthColor,
							marginVertical: 6,
							borderRadius: 10,
							alignItems: 'center'
						}
					]}
				>
					<Image
						style={[heightActa, {
							width: responsiveWidth(30),
							resizeMode: 'cover'
						}]}
						imageStyle={{borderRadius: 10}}
						source={require('../../images/teamwork2.jpg')}
					/>
					<View style={{width: responsiveWidth(64), paddingHorizontal: 10}}>
						<Text
							style={[styles.main_title,
								{
									color: this.state.thirdColor,
									width: responsiveWidth(55),
									textAlign: 'left',
									marginTop: 5,
									lineHeight: 15
								}
							]}
						>
							La relación entre productividad y felicidad
						</Text>
						<Text style={{
							fontSize: responsiveFontSize(1.5),
							textAlign: 'left',
							lineHeight: 11.8,
							marginTop: 3
						}}>
							Equipos más productivos siempre son más felices. Dale a tus maestros las herramientas que
							necesitan para que su único trabajo sea crear mejores clases para sus alumnos.
						</Text>
						<View style={{
							borderWidth: .5,
							width: responsiveWidth(55),
							marginVertical: 7,
							borderColor: 'grey'
						}}/>
						<Text
							style={{width: responsiveWidth(55), textAlign: 'center'}}
							onPress={() => this.blogCard()}
						>
							Leer artículo completo
						</Text>
					</View>
				</View>
			</ScrollView>
		</ImageBackground>);
	}
}
    