import React from 'react';
import {ActivityIndicator, AsyncStorage, Platform, Text} from 'react-native';
import {Actions, Router, Scene} from 'react-native-router-flux';
import {responsiveWidth} from 'react-native-responsive-dimensions';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import codePush from 'react-native-code-push';
// +++++++++++++++++++++++++++++++++++++++++General++++++++++++++++++++++++++++
import Login from './src/components/login';
import Bienvenida from './src/components/Bienvenida';
import BienvenidaT from './src/components/Tablet/BienvenidaT';
import HomePage from './src/components/Mobile/Globales/menu/menu';
import HomePageT from './src/components/Tablet/Globales/menu/menu';
import News from './src/components/Mobile/Globales/news';
import newsSingle from './src/components/Mobile/Globales/newsSingle';
import editNew from './src/components/Mobile/Globales/editNew';
import sidebar from './src/components/sidebar';
import sideBarT from './src/components/Tablet/sidebarT';
import Mensajes from './src/components/Mobile/Globales/mensajes';
import messagesList from './src/components/Mobile/Globales/messagesList';
import newMessage from './src/components/Mobile/Globales/newMessage';
import MenuPagos from './src/components/Mobile/Globales/menu/menuDePagos';
import menuDePagosDir from './src/components/Mobile/Globales/menu/menuDePagosDir';
import crearCargo from './src/components/Mobile/Payments/directivos/crearCargo';
import cargosGenerados from './src/components/Mobile/Payments/directivos/cargosGenerados';
import PwdRest from './src/components/passwordReset';
import navbar from './src/components/navbar';
import navBarT from './src/components/Tablet/navBarT';
import calendario from './src/components/Mobile/Globales/calendar';
import menuDeEvaluacion from './src/components/Mobile/Globales/menu/menuDeEvaluacion';
import asuntosParam from './src/components/Mobile/Parametros/asuntosParam';
import respuestaPers from './src/components/Mobile/Globales/respuestaPers';
import menuParam from './src/components/Mobile/Globales/menu/menuParam';
import Notificaciones from './src/components/Mobile/Globales/Notificaciones';
import PasarLista from './src/components/Mobile/CoordAcad/PasarLista';
import galeria from './src/components/Mobile/Globales/Galeria';
import CGaleria from './src/components/Mobile/Globales/CGaleria';
import reciboPreinscrip from './src/components/Mobile/Alumno/reciboPreinscrip';
//+-+-+-+-+--+-+-+-++-+-+-+-+-+-+-+-Preinscripción-+-+-+-+-+-++-+-+-+-+-++-+-+-+-+
import preinscrip from './src/components/Mobile/Preinscripcion/preinscrip';
import loginPreinscrip from './src/components/Mobile/Preinscripcion/loginPreinscrip';
import aspirantesTutor from './src/components/Mobile/Preinscripcion/aspirantesTutor';
import citaPreinscrip from './src/components/Mobile/Preinscripcion/citaPreinscrip';
import subirDoctos from './src/components/Mobile/Preinscripcion/subirDoctos';
import pagoPreinscrip from './src/components/Mobile/Preinscripcion/pagoPreinscrip';
import pagoInscrip from './src/components/Mobile/Preinscripcion/pagoInscrip';
import pagoTarjetaP from './src/components/Mobile/Preinscripcion/pagoTarjetaP';
import pagoEfectivoP from './src/components/Mobile/Preinscripcion/pagoEfectivoP';
import diaInmersion from './src/components/Mobile/Preinscripcion/diaInmersion';
// +++++++++++++++++++++++++++++++++++++++++Payments++++++++++++++++++++++++++++++
import reciboTienda from './src/components/Mobile/Payments/reciboTienda';
import reciboTransferencia from './src/components/Mobile/Payments/reciboTransferencia';
import formadepagoquenofunciona from './src/components/Mobile/Payments/formasDePago';
// +++++++++++++++++++++++++++++++++++++++++Padres++++++++++++++++++++++++++++++++
import datosPersonales from './src/components/Mobile/Globales/datosPersonales';
import agendarCitas from './src/components/Mobile/Padres/agendarCitas';
import verReportes from './src/components/Mobile/Padres/verReportes';
import consultarDocumentos from './src/components/Mobile/Padres/consultarDocumentos';
import realizarTramite from './src/components/Mobile/Padres/realizarTramite';
import constEstForm from './src/components/Mobile/Padres/constEstForm';
import constEstSimp from './src/components/Mobile/Padres/constEstSimp';
import solBeca from './src/components/Mobile/Padres/solBeca';
import solicitudReinscrip from './src/components/Mobile/Padres/solicitudReinscrip';
import cartRenvBeca from './src/components/Mobile/Padres/cartRenvBeca';
import uniformes from './src/components/Mobile/Padres/uniformes';
import pedidoNuevo from './src/components/Mobile/Padres/pedidoNuevo';
import confirmarUniformes from './src/components/Mobile/Padres/confirmarUniformes';
import verCalif from './src/components/Mobile/Padres/verCalif';
import califsPadre from './src/components/Mobile/Padres/califsPadre';
import encuadrePadre from './src/components/Mobile/Padres/encuadrePadre';
import menuEscPadre from './src/components/Mobile/Padres/menuEscPadre';
import menuFacturas from './src/components/Mobile/Globales/menu/menufacturas';
import aspirantePendiente from './src/components/Mobile/Padres/aspirantePendiente';
import newAsp from './src/components/Mobile/Padres/newAsp';
import calendarEvent from './src/components/Mobile/Padres/calendarEvent';
//+++++++++++++++++++++++++++++++++Alumno+++++++++++++++++++++++++++++++++++++++
import boletasAlum from './src/components/Mobile/Alumno/boletasAlum';
import verCalifAlum from './src/components/Mobile/Alumno/verCalifAlum';
import examRecAlumn from './src/components/Mobile/Alumno/examRecAlumn';
import encuadresAlmn from './src/components/Mobile/Alumno/encuadresAlmn';
import tareaAlumn from './src/components/Mobile/Alumno/tareaAlumn';
import datosPersonalesAlumn from './src/components/Mobile/Alumno/datosPersonalesAlumn';
import verReportesAlumn from './src/components/Mobile/Alumno/verReportesAlumn';
// +++++++++++++++++++++++++++++++++++++++++Padres-Payments+++++++++++++++++++++++
import pagoTarjeta from './src/components/Mobile/Payments/padres/pagoTarjeta';
import GestionarPagos from './src/components/Mobile/Payments/padres/GestionarPagos';
import proximosPagos from './src/components/Mobile/Payments/padres/proximosPagos';
import registroDeUsuarioGateway from './src/components/Mobile/Payments/padres/registroUsuarioGateway';
import historialPagosPadre from './src/components/Mobile/Payments/padres/historialPagos';
import pagosVencidosPadrre from './src/components/Mobile/Payments/padres/pagosVencidos';
import pagosConfiguracionPadre from './src/components/Mobile/Payments/padres/pagosConfiguracion';
import pagarAhora from './src/components/Mobile/Payments/padres/pagarAhora';
import listaFacturas from './src/components/Mobile/Payments/padres/listaFacturas';
import gestionarTarjetas from './src/components/Mobile/Payments/padres/gestionarTarjetas';
import datosFiscales from './src/components/Mobile/Payments/padres/datosFiscales';
import DetalleFacturacion from './src/components/Mobile/Payments/padres/DetalleFacturacion';
import CrearFactura from './src/components/Mobile/Payments/padres/CrearFactura';
// +++++++++++++++++++++++++++++++++++++++++Maestros++++++++++++++++++++++++++++++
import gestCitasMtro from './src/components/Mobile/Maestros/gestCitasMtro';
import conductaM from './src/components/Mobile/Maestros/conductaM';
import moduloTareas from './src/components/Mobile/Maestros/moduloTareas';
import nuevaTarea from './src/components/Mobile/Maestros/nuevaTarea';
import espacios from './src/components/Mobile/Maestros/espacios';
import datosPersonalesM from './src/components/Mobile/Maestros/datosPersonalesM';
import consultarDocumentosM from './src/components/Mobile/Maestros/consultarDocumentosM';
import encuadre from './src/components/Mobile/Maestros/encuadre';
import regAsistencias from './src/components/Mobile/Maestros/regAsistencias';
import evidenciasClases from './src/components/Mobile/Maestros/evidenciasClases';
import calendarioM from './src/components/Mobile/Maestros/calendarioM';
import examenes from './src/components/Mobile/Maestros/examenes';
import modRecuperacion from './src/components/Mobile/Maestros/modRecuperacion';
import modCalificaciones from './src/components/Mobile/Maestros/modCalificaciones';
import iEstadisticos from './src/components/Mobile/Maestros/iEstadisticos';
import gestExam from './src/components/Mobile/Maestros/gestExam';
import correccionEncPt2 from './src/components/Mobile/Maestros/correccionEncPt2';
import captCalifcMtro from './src/components/Mobile/Maestros/captCalifcMtro';
//+-+-+-+-+-+-+-+-+-+-++-+-+-+Medico+-+-+-+-+-+-+-+-+-+-+-+-+-
import gestCitasMed from './src/components/Mobile/Medico/gestCitasMed';
//++++++++++++++++++++++++++++++++++director+++++++++++++++++++++++++++++++++++
import gestDocumentosDir from './src/components/Mobile/director/gestDocumentosDir';
import datosPersonalesDir from './src/components/Mobile/director/datosPersonalesDir';
import eventosCordDir from './src/components/Mobile/director/eventosCordDir';
import indEstadDir from './src/components/Mobile/director/indEstadDir';
import conductaDir from './src/components/Mobile/director/conductaDir';
import administrarDatosDir from './src/components/Mobile/director/administrarDatosDir';
import planAnualTrabDir from './src/components/Mobile/director/planAnualTrabDir';
import califDirec from './src/components/Mobile/director/califDirec';
import boletasDir from './src/components/Mobile/director/boletasDir';
import newAspirantes from './src/components/Mobile/director/newAspirantes';
import verDocumentos from './src/components/Mobile/director/verDocumentos';
import inscripAdmin from './src/components/Mobile/director/inscripAdmin';
import MiServicio from './src/components/Mobile/director/MiServicio';
import ConsultarServicio from './src/components/Mobile/director/ConsultarServicio';
import PagarServicio from './src/components/Mobile/director/PagarServicio';
import FacturarServicio from './src/components/Mobile/director/FacturarServicio';
import GestionarTarjetas from './src/components/Mobile/director/GestionarTarjetas';
import captCalifAdmin from './src/components/Mobile/director/captCalifAdmin';
import MiOrganizacion from './src/components/Mobile/director/MiOrganizacion';
import detalleFacturaServicio from './src/components/Mobile/director/detalleFacturaServicio';
import DetalleFacturacionDirector from './src/components/Mobile/director/DetalleFacturacion';
//++++++++++++++++++++++++++++++++++++subdirector++++++++++++++++++++++++++++++
import eventosCordSub from './src/components/Mobile/subdirector/eventosCordSub';
import indEstadSub from './src/components/Mobile/subdirector/indEstadSub';
import gestDocumentosSub from './src/components/Mobile/subdirector/gestDocumentosSub';
import conductaSub from './src/components/Mobile/subdirector/conductaSub';
import califSubDir from './src/components/Mobile/subdirector/califSubDir';
//++++++++++++++++++++++++++++++++++++prefecto+++++++++++++++++++++++++++++++++
import consultarDocsPref from './src/components/Mobile/prefecto/consultarDocsPref';
import conductaPre from './src/components/Mobile/prefecto/conductaPre';
import planAnualTrabPref from './src/components/Mobile/prefecto/planAnualTrabPref';
// +++++++++++++++++++++++++++++++++++++++++Admin++++++++++++++++++++++++++++++++
import gestEspAdmin from './src/components/Mobile/Admin/gestEspAdmin';
import datosPersonalesAdmin from './src/components/Mobile/Globales/datosPersonalesAdmin';
import gestReportes from './src/components/Mobile/Admin/gestReportes';
import gestEspacios from './src/components/Mobile/Admin/gestEspacios';
import gestDocumentosAdmin from './src/components/Mobile/Admin/gestDocumentosAdmin';
import califAdmin from './src/components/Mobile/Admin/califAdmin';
import invUnif from './src/components/Mobile/Admin/invUnif';
import gestCitasAdmin from './src/components/Mobile/Admin/gestCitasAdmin';
import gestMenuEsc from './src/components/Mobile/Admin/gestMenuEsc';
import editarMenuEsc from './src/components/Mobile/Admin/editarMenuEsc';
import CalendarCorDir from './src/components/Mobile/Admin/CalendarCordDir';
import planAnualTrabSubDir from './src/components/Mobile/Admin/planAnualTrabSubDir';
// +++++++++++++++++++++++++++++++++++++Admin-Payments+++++++++++++++++++++
import pagosVencidos from './src/components/Mobile/Payments/admin/pagosVencidos';
import pagosBecas from './src/components/Mobile/Payments/admin/pagosBecas';
import pagosConfiguracion from './src/components/Mobile/Payments/admin/pagosConfiguracion';
import facturasGeneradas from './src/components/Mobile/Payments/admin/facturasGeneradas';
import consultarSaldos from './src/components/Mobile/Payments/admin/consultarSaldos';
import historialPagos from './src/components/Mobile/Payments/admin/historialPagos';
import estadoCuenta from './src/components/Mobile/Payments/directivos/estadoCuenta';
// +++++++++++++++++++++++++++++++++++++direc-Payments+++++++++++++++++++++
import pagosVencidosDir from './src/components/Mobile/Payments/directivos/pagosVencidosDir';
import pagosBecasDir from './src/components/Mobile/Payments/directivos/pagosBecasDir';
import pagosConfiguracionDir from './src/components/Mobile/Payments/directivos/pagosConfiguracionDir';
import facturasGeneradasDir from './src/components/Mobile/Payments/directivos/facturasGeneradasDir';
import DetalleFacturaDirector from './src/components/Mobile/Payments/directivos/DetalleFacturaDirector';
import consultarSaldosDir from './src/components/Mobile/Payments/directivos/consultarSaldosDir';
import historialPagosDir from './src/components/Mobile/Payments/directivos/historialPagosDir';
import registroDeUsuarioGateway2 from './src/components/Mobile/Payments/directivos/registroUsuarioGateway2';
import pagoTarjetaServicio from './src/components/Mobile/Payments/directivos/pagoTarjetaServicio'
// +++++++++++++++++++++++++++++++++++++++++menu de evaluacion+++++++++++
import encAlumn from './src/components/Mobile/Evaluacion/encAlumn';
import criteriosAdmin from './src/components/Mobile/Evaluacion/criteriosAdmin';
import ppiDocente from './src/components/Mobile/Evaluacion/ppiDocente';
import supervisionClas from './src/components/Mobile/Evaluacion/supervisionClas';
import aplicEnc from './src/components/Mobile/Evaluacion/aplicEnc';
//+++++++++++++++++++++++++++++++++++++++Coordinar academico++++++++++++++++
import Asistencias from './src/components/Mobile/CoordAcad/Asistencias';
import Encuadres from './src/components/Mobile/CoordAcad/Encuadres';
import Calificaciones from './src/components/Mobile/CoordAcad/Calificaciones';
import gestDocumentosCord from './src/components/Mobile/CoordAcad/gestDocumentosCord';
import boletasCord from './src/components/Mobile/CoordAcad/boletasCord';
import indEstadCord from './src/components/Mobile/CoordAcad/indEstadCord';
import movAlumCord from './src/components/Mobile/CoordAcad/movAlumCord';
import examRecupCord from './src/components/Mobile/CoordAcad/examRecupCord';
import gestEspCord from './src/components/Mobile/CoordAcad/gestEspCord';
import eventosCord from './src/components/Mobile/CoordAcad/eventosCord';
import planAnualTrab from './src/components/Mobile/CoordAcad/planAnualTrab';
import editarEvento from './src/components/Mobile/CoordAcad/editarEvento';
//++++++++++++++++++++++++++CoordDisi+++++++++++++
import conductaCordDis from './src/components/Mobile/CoordDiscipl/conductaCordDis';
import gestDocumentosCorDis from './src/components/Mobile/CoordDiscipl/gestDocumentosCorDis';
import indEstadDisci from './src/components/Mobile/CoordDiscipl/indEstadDisci';
import planAnualTrabCorDis from './src/components/Mobile/CoordDiscipl/planAnualTrabCorDis';
import paramDisc from './src/components/Mobile/CoordDiscipl/paramDisc';
//+++++++++++++++++++++++++++++++++++menu de parametros++++++++++++++++++++
import horasClase from './src/components/Mobile/Parametros/horasClase';
import horarios from './src/components/Mobile/Parametros/horarios';
import captMaestro from './src/components/Mobile/Parametros/captMaestro';
import captMateria from './src/components/Mobile/Parametros/captMateria';
import listaVar from './src/components/Mobile/Parametros/listaVar';
import editarListPt2 from './src/components/Mobile/Parametros/editarListPt2';
import limiteSalon from './src/components/Mobile/Parametros/limiteSalon';
import ciclosPeriodos from './src/components/Mobile/Parametros/ciclosPeriodos';
import cantidadGrupos from './src/components/Mobile/Parametros/cantidadGrupos';
import ListaDocumentos from './src/components/Mobile/director/ListaDocumentos';
//++++++++++++++++++++++++++++++++++++++TabBarView++++++++++++++++++++++++++++
import HomeCE from './src/components/TabBarViews/HomeCE';
import infoCE from './src/components/TabBarViews/infoCE';
import NavBarDos from './src/components/TabBarViews/NavBarDos';
//++++++++++++++++++++++++++++++++++++++RegistroEscuelas++++++++++++++++++++++++++++
import Solicitud from './src/components/Mobile/Registro/Solicitud';
import AgregarInformacionPrivadas from './src/components/Mobile/Registro/Privadas/AgregarInformacion';
import AgregarInformacionPublicas from './src/components/Mobile/Registro/Publicas/AgregarInformacion';
import Planes from './src/components/Mobile/Registro/Privadas/Planes';
import altaCiudadesCE from './src/components/Mobile/Registro/AltaCiudadesCE';
import Comentario from './src/components/Mobile/Registro/Comentario';
import YaRegistro from './src/components/Mobile/Registro/YaRegistro';

class App extends React.Component {
	constructor() {
		super();
		this.state = {
			hasToken: false, assetsAreLoaded: false, isLoaded: false, hasIdTutor: false, changeIcon: false, ya_solicito: false
		};
		if (Text.defaultProps == null) Text.defaultProps = {};
		Text.defaultProps.allowFontScaling = false;
	}

	async componentWillMount() {
		await AsyncStorage.getItem('token').then(token => {
			this.setState({
				hasToken: token !== null
			});
		});
		await AsyncStorage.getItem('idTutor').then(id => {
			this.setState({
				hasIdTutor: id !== null
			});
		});
        await AsyncStorage.getItem('ya_registro').then(id => {
        	console.log(AsyncStorage.getItem('ya_registro'))
            this.setState({
                ya_registro: id !== null
            });
        });
	}

	componentDidMount() {
		/*getToken*/
		AsyncStorage.getItem('token').then(token => {
			this.setState({
				hasToken: token !== null, isLoaded: true
			});
		});
		AsyncStorage.getItem('idTutor').then(id => {
			this.setState({
				hasIdTutor: id !== null, isLoaded: true
			});
		});
        AsyncStorage.getItem('ya_registro').then(id => {
            this.setState({
                ya_registro: id !== null, isLoaded: true
            });
        });
	}

	render() {
		const icono1 = () => (<FontAwesome style={{width: 25, height: 25}} color={'#fff'} name='home' size={25}/>);
		const icono2 = () => (
			this.state.hasToken ?
				<FontAwesome style={{width: 25, height: 25}} color={'#fff'} name='th' size={25}/> :
				<FontAwesome style={{width: 25, height: 25}} color={'#fff'} name='sign-in' size={25}/>);
		const icono3 = () => (
			<MaterialCommunityIcons style={{width: 25, height: 25}} color={'#fff'} name='book-open-page-variant'
									size={25}/>);
		if (!this.state.isLoaded) {
			return <ActivityIndicator/>;
		} else {
			return (<Router uriPrefix={'controlescolar.pro'}>
				<Scene key='root' cardStyle={{backgroundColor: '#116876'}}>
					<Scene
						initial={true}
						key='tabbar'
						showLabel={false}
						tabs
						tabBarStyle={{backgroundColor: '#116876'}}
						hideNavBar={true}
						inactiveTintColor={'#116876'}
						activeBackgroundColor={'#23A199'}
						inactiveBackgroundColor={'#116876'}>
						<Scene
							component={HomeCE}
							hideNavBar={false}
							key='HomeCE'
							icon={icono1}
							title='Blog de noticias'
							showLabel={false}
							gestureEnabled={false}
							panHandlers={null}
							navBar={NavBarDos}
						/>
						<Scene icon={icono2} key='all' hideNavBar={false} initial={this.state.hasToken}>
							<Scene
								key='drawer'
								drawer
								initial={this.state.hasToken}
								drawerPosition='right'
								drawerImage={require('./src/images/menu.png')}
								contentComponent={Platform.isPad ? sideBarT : sidebar}
								hideNavBar
								drawerWidth={Platform.isPad ? responsiveWidth(50) : responsiveWidth(65)}
							>
								{Platform.isPad ?
									<Scene key='menuPrincipal'>
										<Scene
											component={HomePageT}
											hideNavBar={false}
											initial={this.state.hasToken}
											key='HomePageT'
											navBar={navBarT}
											title='Menú principal'
										/>
										<Scene
											component={News}
											hideNavBar={false}
											key='News'
											title='Últimas noticias'
											navBar={navBarT}
											back={true}
										/>
										<Scene
											component={datosPersonales}
											hideNavBar={false}
											back={true}
											navBar={navBarT}
											key='datosPersonales'
											title='Actualizar mi información'
										/>
									</Scene>
									:
									<Scene key='menuPrincipal'>
										<Scene
											component={HomePage}
											hideNavBar={false}
											initial={this.state.hasToken}
											key='HomePage'
											navBar={navbar}
											title='Menú principal'
										/>
                                        <Scene
                                            component={MiServicio}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='MiServicio'
                                            title='Mi Servicio'
                                        />
                                        <Scene
                                            component={ConsultarServicio}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='ConsultarServicio'
                                            title='Estatus del Servicio'
                                        />
                                        <Scene
                                            component={GestionarTarjetas}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='GestionarTarjetas'
                                            title='Gestionar Tarjetas'
                                        />
                                        <Scene
                                            component={PagarServicio}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='PagarServicio'
                                            title='Pagar Servicio'
                                        />
                                        <Scene
                                            component={FacturarServicio}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='FacturarServicio'
                                            title='Facturar Servicio'
                                        />
                                        <Scene
                                            component={detalleFacturaServicio}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='detalleFacturaServicio'
                                            title='Detalle de la Factura'
                                        />
										<Scene
											component={constEstForm}
											hideNavBar={false}
											key='constEstForm'
											navBar={navbar}
											title='Constancia de estudios formal'
										/>
										<Scene
											component={constEstSimp}
											hideNavBar={false}
											key='constEstSimp'
											navBar={navbar}
											title='Constancia de estudios simple'
										/>
										<Scene
											component={News}
											hideNavBar={false}
											key='News'
											title='Últimas noticias'
											navBar={navbar}
											back={true}
										/>
										<Scene
											component={Notificaciones}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='Notificaciones'
											title='Registro de actividad'
										/>
										<Scene
											component={solBeca}
											hideNavBar={false}
											key='solBeca'
											navBar={navbar}
											title='Solicitar beca'
										/>
										<Scene
											component={solicitudReinscrip}
											hideNavBar={false}
											key='solicitudReinscrip'
											navBar={navbar}
											title='Solicitar reinscripción'
										/>
										<Scene
											component={cartRenvBeca}
											hideNavBar={false}
											key='cartRenvBeca'
											navBar={navbar}
											title='Carta de renovación de beca'
										/>
										<Scene
											component={pedidoNuevo}
											hideNavBar={false}
											key='pedidoNuevo'
											navBar={navbar}
											title='Pedido nuevo'
										/>
										<Scene
											component={gestEspAdmin}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestEspAdmin'
											title='Gestión de espacios'
										/>
										<Scene
											component={consultarDocumentos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='consultarDocumentos'
											title='Consultar documentos'
										/>
										<Scene
											component={uniformes}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='uniformes'
											title='Pedidos de uniformes'
										/>
										<Scene
											component={realizarTramite}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='realizarTramite'
											title='Realizar un trámite'
										/>
										<Scene
											component={calendario}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='calendario'
											title='Calendario de actividades'
										/>
										<Scene
											component={consultarDocumentosM}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='consultarDocumentosM'
											title='Consultar documentos'
										/>
										<Scene
											component={gestDocumentosAdmin}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestDocumentosAdmin'
											title='Gestión de documentos'
										/>
										<Scene
											component={califAdmin}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='califAdmin'
											title='Calificaciones'
										/>
										<Scene
											component={invUnif}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='invUnif'
											title='Inventario de uniformes'
										/>
										<Scene
											component={gestCitasAdmin}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestCitasAdmin'
											title='Gestionar citas'
										/>
										<Scene
											component={gestMenuEsc}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestMenuEsc'
											title='Menú escolar'
										/>
										<Scene
											component={editarMenuEsc}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='editarMenuEsc'
											title='Editar menú escolar'
										/>
										<Scene
											component={CalendarCorDir}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='CalendarCorDir'
											title='Calendario escolar'
										/>
										<Scene
											component={gestDocumentosCord}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestDocumentosCord'
											title='Gestionar documentos'
										/>

										<Scene
											component={datosPersonales}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='datosPersonales'
											title='Actualizar mi información'
										/>
										<Scene
											component={indEstadCord}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='indEstadCord'
											title='Indicadores estadísticos'
										/>

										<Scene
											component={movAlumCord}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='movAlumCord'
											title='Movimiento de alumnos'
										/>
										<Scene
											component={examRecupCord}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='examRecupCord'
											title='Exámenes de recuperación'
										/>
										<Scene
											component={PasarLista}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='PasarLista'
											title='Pasar lista'
										/>
										<Scene
											component={gestEspCord}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestEspCord'
											title='Gestionar espacios'
										/>
										<Scene
											component={eventosCord}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='eventosCord'
											title='Programar eventos'
										/>
										<Scene
											component={planAnualTrab}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='planAnualTrab'
											title='Plan anual de trabajo'
										/>
										<Scene
											component={editarEvento}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='editarEvento'
											title='Editar evento'
										/>
										<Scene
											component={menuFacturas}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='menuFacturas'
											title='Menú de facturas'
										/>
										<Scene
											component={conductaCordDis}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='conductaCordDis'
											title='Informe de conducta'
										/>
										<Scene
											component={gestDocumentosCorDis}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestDocumentosCorDis'
											title='Gestionar documentos'
										/>
										<Scene
											component={planAnualTrabCorDis}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='planAnualTrabCorDis'
											title='Plan anual de trabajo'
										/>
										<Scene
											component={paramDisc}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='paramDisc'
											title='Parámetros'
										/>
										<Scene
											component={indEstadDisci}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='indEstadDisci'
											title='Estadísticas de disciplina'
										/>

										<Scene
											component={boletasCord}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='boletasCord'
											title='Boletas'
										/>
										<Scene
											component={datosPersonalesAdmin}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='datosPersonalesAdmin'
											title='Actualiza tu información'
										/>
										<Scene
											component={datosPersonalesM}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='datosPersonalesM'
											title='Actualiza tu información'
										/>
										<Scene
											component={newsSingle}
											hideNavBar={false}
											key='newsSingle'
											title='Estás leyendo'
											back={true}
											navBar={navbar}
										/>
                                        <Scene
                                            component={editNew}
                                            hideNavBar={false}
                                            key='editNew'
                                            title='Editar noticia'
                                            back={true}
                                            navBar={navbar}
                                        />
										<Scene
											component={agendarCitas}
											hideNavBar={false}
											key='agendarCitas'
											title='Agendar una cita'
											back={true}
											navBar={navbar}
										/>
										<Scene
											component={gestCitasMtro}
											hideNavBar={false}
											key='gestCitasMtro'
											back={true}
											navBar={navbar}
											title='Gestionar citas'
										/>
										<Scene
											component={gestCitasMed}
											hideNavBar={false}
											key='gestCitasMed'
											back={true}
											navBar={navbar}
											title='Gestionar citas'
										/>
										<Scene
											component={gestExam}
											hideNavBar={false}
											key='gestExam'
											back={true}
											navBar={navbar}
											title='Gestión de exámenes'
										/>
										<Scene
											component={gestDocumentosDir}
											hideNavBar={false}
											key='gestDocumentosDir'
											back={true}
											navBar={navbar}
											title='Documentos'
										/>
										<Scene
											component={ListaDocumentos}
											hideNavBar={false}
											key='ListaDocumentos'
											back={true}
											navBar={navbar}
											title='Lista Documentos'
										/>
										<Scene
											component={eventosCordDir}
											hideNavBar={false}
											key='eventosCordDir'
											back={true}
											navBar={navbar}
											title='Programar eventos'
										/>
										<Scene
											component={indEstadDir}
											hideNavBar={false}
											key='indEstadDir'
											back={true}
											navBar={navbar}
											title='Indicadores estadísticos'
										/>
										<Scene
											component={conductaDir}
											hideNavBar={false}
											key='conductaDir'
											back={true}
											navBar={navbar}
											title='Crear reporte de conducta'
										/>
										<Scene
											component={administrarDatosDir}
											hideNavBar={false}
											key='administrarDatosDir'
											back={true}
											navBar={navbar}
											title='Administrar datos'
										/>
										<Scene
											component={planAnualTrabDir}
											hideNavBar={false}
											key='planAnualTrabDir'
											back={true}
											navBar={navbar}
											title='Plan anual de trabajo'
										/>
										<Scene
											component={califDirec}
											hideNavBar={false}
											key='califDirec'
											back={true}
											navBar={navbar}
											title='Calificaciones'
										/>
										<Scene
											component={captCalifAdmin}
											hideNavBar={false}
											key='captCalifAdmin'
											back={true}
											navBar={navbar}
											title='Calificaciones'
										/>
										<Scene
											component={boletasDir}
											hideNavBar={false}
											key='boletasDir'
											back={true}
											navBar={navbar}
											title='Boletas'
										/>
										<Scene
											component={newAspirantes}
											hideNavBar={false}
											key='newAspirantes'
											back={true}
											navBar={navbar}
											title='Aspirantes'
										/>
										<Scene
											component={verDocumentos}
											hideNavBar={false}
											key='verDocumentos'
											back={true}
											navBar={navbar}
											title='Documentos del aspirante'
										/>
										<Scene
											component={inscripAdmin}
											hideNavBar={false}
											key='inscripAdmin'
											back={true}
											navBar={navbar}
											title='Inscripciones'
										/>
										<Scene
											component={eventosCordSub}
											hideNavBar={false}
											key='eventosCordSub'
											back={true}
											navBar={navbar}
											title='Programar eventos'
										/>
										<Scene
											component={indEstadSub}
											hideNavBar={false}
											key='indEstadSub'
											back={true}
											navBar={navbar}
											title='Indicadores estadísticos'
										/>

										<Scene
											component={conductaSub}
											hideNavBar={false}
											key='conductaSub'
											back={true}
											navBar={navbar}
											title='Crear reporte de conducta'
										/>

										<Scene
											component={gestDocumentosSub}
											hideNavBar={false}
											key='gestDocumentosSub'
											back={true}
											navBar={navbar}
											title='Documentos'
										/>

										<Scene
											component={planAnualTrabSubDir}
											hideNavBar={false}
											key='planAnualTrabSubDir'
											back={true}
											navBar={navbar}
											title='Plan anual de trabajo'
										/>
										<Scene
											component={califSubDir}
											hideNavBar={false}
											key='califSubDir'
											back={true}
											navBar={navbar}
											title='Calificaciones'
										/>
										<Scene
											component={consultarDocsPref}
											hideNavBar={false}
											key='consultarDocsPref'
											back={true}
											navBar={navbar}
											title='Consultar documentos'
										/>
										<Scene
											component={conductaPre}
											hideNavBar={false}
											key='conductaPre'
											back={true}
											navBar={navbar}
											title='Crear reporte de conducta'
										/>
										<Scene
											component={planAnualTrabPref}
											hideNavBar={false}
											key='planAnualTrabPref'
											back={true}
											navBar={navbar}
											title='Plan anual de trabajo'
										/>
										<Scene
											component={datosPersonalesDir}
											hideNavBar={false}
											key='datosPersonalesDir'
											back={true}
											navBar={navbar}
											title='Gestión de Datos'
										/>
										<Scene
											component={Mensajes}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='Messages'
											title='Enviar un mensaje'
										/>
										<Scene
											component={messagesList}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='messagesList'
											title='Conversaciones recientes'
										/>
										<Scene
											component={respuestaPers}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='respuestaPers'
											title='Definir respuesta'
										/>
										<Scene
											component={newMessage}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='newMessage'
											title='Conversaciones recientes'
										/>
										<Scene
											component={conductaM}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='conductaM'
											title='Crear reporte de conducta'
										/>

										<Scene
											component={espacios}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='espacios'
											title='Apartar espacios'
										/>
										<Scene
											component={gestEspacios}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestEspacios'
											title='Gestionar espacios'
										/>
										<Scene
											component={verReportes}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='verReportes'
											title='Reporte de conducta'
										/>

										<Scene
											component={confirmarUniformes}
											hideNavBar={false}
											key='confirmarUniformes'
											navBar={navbar}
											title='Confirmar pedido'
										/>
										<Scene
											component={verCalif}
											hideNavBar={false}
											key='verCalif'
											navBar={navbar}
											title='Calificaciones'
										/>
										<Scene
											component={califsPadre}
											hideNavBar={false}
											key='califsPadre'
											navBar={navbar}
											title='Calificaciones'
										/>
										<Scene
											component={aspirantePendiente}
											hideNavBar={false}
											key='aspirantePendiente'
											navBar={navbar}
											title='Inscripciones'
										/>
										<Scene
											component={newAsp}
											hideNavBar={false}
											key='newAsp'
											navBar={navbar}
											title='Nuevo aspirante'
										/>
										<Scene
											component={calendarEvent}
											hideNavBar={false}
											key='calendarEvent'
											navBar={navbar}
											title='Calendario de actividades'
										/>
										<Scene
											component={encuadrePadre}
											hideNavBar={false}
											key='encuadrePadre'
											back={true}
											navBar={navbar}
											title='Encuadres'
										/>
										<Scene
											component={menuEscPadre}
											hideNavBar={false}
											key='menuEscPadre'
											back={true}
											navBar={navbar}
											title='Menú escolar'
										/>
										<Scene
											component={galeria}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='galeria'
											title='Galería privada'
										/>
										<Scene
											component={CGaleria}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='CGaleria'
											title='Galería privada'
										/>
										<Scene
											component={reciboPreinscrip}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='reciboPreinscrip'
											title='Recibo de pago'
										/>
										<Scene
											component={boletasAlum}
											hideNavBar={false}
											key='boletasAlum'
											navBar={navbar}
											title='Boletas'
										/>
										<Scene
											component={verCalifAlum}
											hideNavBar={false}
											key='verCalifAlum'
											navBar={navbar}
											title='Ver calificaciones'
										/>
										<Scene
											component={examRecAlumn}
											hideNavBar={false}
											key='examRecAlumn'
											navBar={navbar}
											title='Examenes de recuperación'
										/>
										<Scene
											component={encuadresAlmn}
											hideNavBar={false}
											back={true}
											key='encuadresAlmn'
											navBar={navbar}
											title='Encuadres'
										/>
										<Scene
											component={tareaAlumn}
											hideNavBar={false}
											key='tareaAlumn'
											navBar={navbar}
											title='Consultar tareas'
										/>
										<Scene
											component={datosPersonalesAlumn}
											hideNavBar={false}
											key='datosPersonalesAlumn'
											navBar={navbar}
											title='Actualizar mi información'
										/>
										<Scene
											component={verReportesAlumn}
											hideNavBar={false}
											key='verReportesAlumn'
											navBar={navbar}
											title='Informe de conducta'
										/>
										<Scene
											component={gestReportes}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestReportes'
											title='Informe de conducta'
										/>

										<Scene
											back={true}
											navBar={navbar}
											component={nuevaTarea}
											hideNavBar={false}
											key='nuevaTarea'
											onBack={() => Actions.push('drawer')}
											title='Publicar tarea'
										/>

										<Scene
											back={true}
											component={moduloTareas}
											hideNavBar={false}
											key='moduloTareas'
											navBar={navbar}
											title='Revisar y registrar tareas'
										/>
										<Scene
											back={true}
											component={encuadre}
											hideNavBar={false}
											key='encuadre'
											navBar={navbar}
											title='Encuadre de evaluación'
										/>
										<Scene
											back={true}
											component={correccionEncPt2}
											hideNavBar={false}
											key='correccionEncPt2'
											navBar={navbar}
											title='Corrección de encuadre'
										/>
										<Scene
											back={true}
											component={captCalifcMtro}
											hideNavBar={false}
											key='captCalifcMtro'
											navBar={navbar}
											title='Calificaciones'
										/>
										<Scene
											back={true}
											component={evidenciasClases}
											hideNavBar={false}
											key='evidenciasClases'
											navBar={navbar}
											title='Evidencias de clases'
										/>
										<Scene
											back={true}
											component={iEstadisticos}
											hideNavBar={false}
											key='iEstadisticos'
											navBar={navbar}
											title='Indicadores estadísticos'
										/>
										<Scene
											back={true}
											component={calendarioM}
											hideNavBar={false}
											key='calendarioM'
											navBar={navbar}
											title='Calendario de actividades'
										/>
										<Scene
											back={true}
											component={examenes}
											hideNavBar={false}
											key='examenes'
											navBar={navbar}
											title='Resultados de exámenes'
										/>
										<Scene
											back={true}
											component={modRecuperacion}
											hideNavBar={false}
											key='modRecuperacion'
											navBar={navbar}
											title='Exámenes de recuperación'
										/>

										<Scene
											back={true}
											component={modCalificaciones}
											hideNavBar={false}
											key='modCalificaciones'
											navBar={navbar}
											title='Ver calificaciones'
										/>
										<Scene
											back={true}
											component={regAsistencias}
											hideNavBar={false}
											key='regAsistencias'
											navBar={navbar}
											title='Gestión de asistencias'
										/>
										<Scene
											component={PwdRest}
											back={false}
											navBar={navbar}
											hideNavBar={true}
											key='PwdRest'
											title='PwdReset'
											gestureEnabled={false}
											panHandlers={null}
										/>
										<Scene
											component={registroDeUsuarioGateway}
											hideNavBar={true}
											back={false}
											navBar={navbar}
											key='rugt'
											gestureEnabled={false}
											title='Datos de pago'
											panHandlers={null}
										/>
                                        <Scene
                                            component={registroDeUsuarioGateway2}
                                            hideNavBar={true}
                                            back={false}
                                            navBar={navbar}
                                            key='rugt2'
                                            gestureEnabled={false}
                                            title='Datos de pago'
                                            panHandlers={null}
                                        />
										<Scene
											component={Asistencias}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='Asistencias'
											title='Informe de asistencia'
										/>
										<Scene
											component={menuDePagosDir}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='menuDePagosDir'
											title='Menú de pagos'
										/>
										<Scene
											component={cargosGenerados}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='cargosGenerados'
											title='Cargos generados anteriormente'
										/>
										<Scene
											component={pagosVencidosDir}
											hideNavBar={false}
											path={'/vencidos/:id'}
											back={true}
											navBar={navbar}
											key='pagosVencidosDir'
											title='Pagos vencidos'
										/>
										<Scene
											component={historialPagosDir}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='historialPagosDir'
											title='Historial pagos'
										/>
										<Scene
											component={estadoCuenta}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='estadoCuenta'
											title='Estado de cuenta'
										/>
										<Scene
											component={consultarSaldosDir}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='consultarSaldosDir'
											title='Consultar saldos'
										/>
										<Scene
											component={pagosConfiguracionDir}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagosConfiguracionDir'
											title='Configuración de cobros'
										/>
										<Scene
											component={pagosBecasDir}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagosBecasDir'
											title='Asignación de becas'
										/>
										<Scene
											component={facturasGeneradasDir}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='facturasGeneradasDir'
											title='Facturas'
										/>
										<Scene
											component={DetalleFacturaDirector}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='DetalleFacturaDirector'
											title='Detalle de la factura'
										/>
                                        <Scene
                                            component={DetalleFacturacionDirector}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='DetalleFacturacionDirector'
                                            title='Detalle de la factura'
                                        />
                                        <Scene
                                            component={MiOrganizacion}
                                            hideNavBar={false}
                                            back={true}
                                            navBar={navbar}
                                            key='MiOrganizacion'
                                            title='Mi Organización'
                                        />
										<Scene
											component={MenuPagos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='menuDePagos'
											title='Menú de pagos'
											gestureEnabled={false}
											panHandlers={null}
										/>
										<Scene
											component={crearCargo}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='crearCargo'
											title='Nuevo cargo'
										/>
										<Scene
											component={cargosGenerados}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='cargosGenerados'
											title='Cargos generados anteriormente'
										/>
										<Scene
											component={proximosPagos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='proximosPagos'
											title='Proximos pagos'
										/>
										<Scene
											component={formadepagoquenofunciona}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='formadepagoquenofunciona'
											title='Elija una forma de pago'
										/>
										<Scene
											component={pagosVencidos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagosVencidos'
											title='Pagos vencidos'
										/>
										<Scene
											component={GestionarPagos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='GestionarPagos'
											title='Gestionar pagos'
										/>
										<Scene
											component={historialPagos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='historialPagos'
											title='Historial pagos'
										/>
										<Scene
											component={consultarSaldos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='consultaSaldos'
											title='Consultar saldos'
										/>
										<Scene
											component={pagosConfiguracion}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagosConfiguracion'
											title='Configuración de cobros'
										/>
										<Scene
											component={pagosBecas}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagosBecas'
											title='Pagos becas'
										/>
										<Scene
											component={facturasGeneradas}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='facturasGeneradas'
											title='Facturas generadas'
										/>
										<Scene
											component={historialPagosPadre}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='historialPagosPaps'
											title='Historial de pagos'
										/>
										<Scene
											component={pagosVencidosPadrre}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagosVencidosPaps'
											title='Pagos vencidos'
										/>
										<Scene
											component={pagosConfiguracionPadre}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagosConfiguracionPaps'
											title='Configuración de pagos'
										/>
										<Scene
											component={pagarAhora}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagarAhoraPaps'
											title='Pagar ahora'
										/>
										<Scene
											component={listaFacturas}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='listaFactraPaps'
											title='Historial de facturas'
										/>
										<Scene
											component={gestionarTarjetas}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='gestionarTarjetas'
											title='Gestionar tarjetas'
										/>
										<Scene
											component={datosFiscales}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='datosFiscales'
											title='Mis datos fiscales'
										/>
										<Scene
											component={DetalleFacturacion}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='DetalleFacturacion'
											title='Detalle de Factura'
										/>
										<Scene
											component={CrearFactura}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='CrearFactura'
											title='Crear factura'
										/>
										<Scene
											component={pagoTarjeta}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagoTarjetas'
											title='Confirmar pago'
										/>
										<Scene
											component={pagoTarjetaServicio}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='pagoTarjetaServicio'
											title='Confirmar Pago'
										/>
										<Scene
											component={menuParam}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='menuParam'
											title='Parámetros'
										/>
										<Scene
											component={horasClase}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='horasClase'
											title='Definir horario'
										/>
										<Scene
											component={captMateria}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='captMateria'
											title='Capturar materias'
										/>
										<Scene
											component={captMaestro}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='captMaestro'
											title='Alta maestros'
										/>
										<Scene
											component={horarios}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='horarios'
											title='Definir horario de clase'
										/>
										<Scene
											component={editarListPt2}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='editarListPt2'
											title='Editar lista'
										/>
										<Scene
											component={limiteSalon}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='limiteSalon'
											title='Capacidad de salon'
										/>
										<Scene
											component={ciclosPeriodos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='ciclosPeriodos'
											title='Ciclos y periodos escolares'
										/>
										<Scene
											component={cantidadGrupos}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='cantidadGrupos'
											title='Definir grupos'
										/>
										<Scene
											component={listaVar}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='listaVar'
											title='Lista personalizada'
										/>
										<Scene
											component={asuntosParam}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='asuntosParam'
											title='Definir asuntos'
										/>
										<Scene
											component={Encuadres}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='Encuadres'
											title='Encuadres'
										/>
										<Scene
											component={Calificaciones}
											hideNavBar={false}
											back={true}
											navBar={navbar}
											key='Calificaciones'
											title='Calificaciones'
										/>
										<Scene key='menuDeEvaluacion'>
											<Scene
												component={menuDeEvaluacion}
												hideNavBar={false}
												back={true}
												navBar={navbar}
												key='menuDeEvaluacion'
												title='Menú de Evaluación'
											/>
											<Scene
												component={aplicEnc}
												hideNavBar={false}
												back={true}
												navBar={navbar}
												key='aplicEnc'
												title='Aplicar encuesta'
											/>
											<Scene
												component={encAlumn}
												hideNavBar={false}
												back={true}
												navBar={navbar}
												key='encAlumn'
												title='Encuesta alumnos'
											/>
											<Scene
												component={supervisionClas}
												hideNavBar={false}
												back={true}
												navBar={navbar}
												key='supervisionClas'
												title='Supervisión de clase'
											/>
											<Scene
												component={ppiDocente}
												hideNavBar={false}
												back={true}
												navBar={navbar}
												key='ppiDocente'
												title='PPI Docentes'
											/>
											<Scene
												component={criteriosAdmin}
												hideNavBar={false}
												back={true}
												navBar={navbar}
												key='criteriosAdmin'
												title='Criterios administrativos'
											/>
										</Scene>
									</Scene>
								}
							</Scene>
							{Platform.isPad ?
								<Scene
									component={BienvenidaT}
									initial={!this.state.hasToken}
									key='Bienvenida'
									hideNavBar={true}
								/> : <Scene
									component={Bienvenida}
									initial={!this.state.hasToken}
									key='Bienvenida'
									hideNavBar={true}
								/>}
							<Scene
								component={Login}
								key='Authentication'
								hideNavBar={true}
								title='Inicio de sesión'
							/>
							<Scene
								component={preinscrip}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='preinscrip'
								title='¡Quiero ser parte!'
							/>
							<Scene
								component={reciboTienda}
								hideNavBar={false}
								back={true}
								navBar={navbar}
								key='reciboTienda'
								title='Pago en tienda'
							/>
							<Scene
								component={reciboTransferencia}
								hideNavBar={false}
								back={true}
								navBar={navbar}
								key='reciboTransferencia'
								title='Transferencia'
							/>
							<Scene
								component={loginPreinscrip}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='loginPreinscrip'
								title='Continuar registro'
							/>
							<Scene
								component={aspirantesTutor}
								initial={this.state.hasIdTutor}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='aspirantesTutor'
								title='Continuar registro'
							/>
							<Scene
								component={citaPreinscrip}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='citaPreinscrip'
								title='Agendar cita'
							/>
							<Scene
								component={subirDoctos}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='subirDoctos'
								title='Subir documentos'
							/>
							<Scene
								component={pagoPreinscrip}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='pagoPreinscrip'
								title='Pago preinscripción'
							/>
							<Scene
								component={pagoInscrip}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='pagoInscrip'
								title='Pago inscripción'
							/>
							<Scene
								component={pagoTarjetaP}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='pagoTarjetaP'
								title='Pago con tarjeta'
							/>
							<Scene
								component={pagoEfectivoP}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='pagoEfectivoP'
								title='Pago en tienda'
							/>
							<Scene
								component={diaInmersion}
								hideNavBar={false}
								navBar={navbar}
								back={true}
								key='diaInmersion'
								title='Dia de inmersión'
							/>
						</Scene>
                        <Scene icon={icono3} key='registrate' hideNavBar={false}>
                            <Scene
								initial={!this.state.ya_registro}
                                component={infoCE}
                                hideNavBar={false}
                                key='infoCE'
                                title='Regístrate'
                                showLabel={false}
                                gestureEnabled={false}
                                panHandlers={null}
                                navBar={NavBarDos}
                            />
                            <Scene
                                initial={this.state.ya_registro}
                                component={YaRegistro}
                                hideNavBar={false}
                                key='YaRegistro'
                                title='Regístrate'
                                showLabel={false}
                                gestureEnabled={false}
                                panHandlers={null}
                                navBar={NavBarDos}
                            />
                            <Scene
                                component={Solicitud}
                                hideNavBar={false}
                                navBar={navbar}
                                back={true}
                                key='SolicitudEsc'
                                title='Solicitud de Registro'
                            />
                            <Scene
                                component={AgregarInformacionPrivadas}
                                hideNavBar={false}
                                navBar={navbar}
                                back={true}
                                key='InformacionPrivadas'
                                title='Agregar Información'
                            />
                            <Scene
                                component={AgregarInformacionPublicas}
                                hideNavBar={false}
                                navBar={navbar}
                                back={true}
                                key='InformacionPublicas'
                                title='Agregar Información'
                            />
                            <Scene
                                component={Planes}
                                hideNavBar={false}
                                navBar={navbar}
                                back={true}
                                key='Planes'
                                title='Seleccionar Plan'
                            />
                            <Scene
                                component={altaCiudadesCE}
                                hideNavBar={false}
                                navBar={navbar}
                                back={true}
                                key='altaCiudadesCE'
                                title='Alta de ciudad'
                            />
                            <Scene
                                component={Comentario}
                                hideNavBar={false}
                                navBar={navbar}
                                back={true}
                                key='Comentario'
                                title='Agregar un comentario'
                            />
						</Scene>
					</Scene>
				</Scene>
			</Router>);
		}
	}
}


let MyApp = codePush({checkFrequency: codePush.CheckFrequency.ON_APP_RESUME})(App);
export default MyApp;
